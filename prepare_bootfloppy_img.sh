#!/bin/sh

sudo losetup /dev/loop0 floppy.img
sudo losetup -o 154112 /dev/loop1 floppy.img
sudo mount /dev/loop1 mnt/bootfloppy

sudo grub-install --root-directory=`pwd`/mnt/bootfloppy --allow-floppy --target=i386-pc --install-modules="normal part_msdos ext2 multiboot biosdisk gzio" --modules="normal part_msdos ext2 multiboot biosdisk gzio" --locales="" --themes=none --fonts=ascii /dev/loop0

sudo umount mnt/bootfloppy
sudo losetup -d /dev/loop1
sudo losetup -d /dev/loop0

