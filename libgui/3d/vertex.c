#include <libgui/libgui.h>
#include <math.h>
#include <stdlib.h>


// Projection

Point vertex_project_ortho(Vertex v) {
    Point newp = {  // because 2D > 3D
        .x = (int) v.x,
        .y = (int) v.y,
    };
    return newp;
}


Point *multi_vertex_project_ortho(Vertex *v, size_t num) {
    Point *points = (Point *) malloc(sizeof(Point) * num);
    for (size_t i=0; i<num; i++) {
        Point *curr = points + i;
        curr->x = v[i].x;
        curr->y = v[i].y;
    }
    return points;
}


// Rotation
// TODO euler z

Vertex vertex_rotate_euler_x(Vertex v, double alpha) {
    Vertex new_vertex;
    double co = cos(alpha);
    double si = sin(alpha);
    
    new_vertex.x = v.x;
    new_vertex.y = v.y * co - v.z * si;
    new_vertex.z = v.y * si + v.z * co;
    
    return new_vertex;
}

Vertex vertex_rotate_euler_y(Vertex v, double alpha) {
    Vertex new_vertex;
    double co = cos(alpha);
    double si = sin(alpha);
    
    new_vertex.y = v.y;
    new_vertex.z = v.z * co - v.x * si;
    new_vertex.x = v.z * si + v.x * co;
    
    return new_vertex;
}

void multi_vertex_rotate_euler_x(Vertex *v, size_t num, double alpha) {
    double co = cos(alpha);
    double si = sin(alpha);
    double temp;
    
    for (size_t i=0; i<num; i++) {
        temp = v[i].y;
        v[i].y = v[i].y * co - v[i].z * si;
        v[i].z = temp * si + v[i].z * co;
    }
}

void multi_vertex_rotate_euler_y(Vertex *v, size_t num, double alpha) {
    double co = cos(alpha);
    double si = sin(alpha);
    double temp;
    
    for (size_t i=0; i<num; i++) {
        temp = v[i].z;
        v[i].z = v[i].z * co - v[i].x * si;
        v[i].x = temp * si + v[i].x * co;
    }
}

// translation

Vertex vertex_translate(Vertex a, Vertex b) {
    Vertex c = {
        .x = a.x + b.x,
        .y = a.y + b.y,
        .z = a.z + b.z,
    };
    return c;
}

Vertex vertex_invert(Vertex a) {
    Vertex c = {
        .x = -a.x,
        .y = -a.y,
        .z = -a.z,
    };
    return c;
}

void multi_vertex_translate(Vertex *v, size_t num, Vertex delta) {
    for (size_t i=0; i<num; i++) {
        v[i].x += delta.x;
        v[i].y += delta.y;
        v[i].z += delta.z;
    }
}

// scaling

Vertex vertex_scale(Vertex a, Vertex b) {
    Vertex c = {
        .x = a.x * b.x,
        .y = a.y * b.y,
        .z = a.z * b.z,
    };
    return c;
}

void multi_vertex_scale(Vertex *v, size_t num, Vertex rho) {
    for (size_t i=0; i<num; i++) {
        v[i].x *= rho.x;
        v[i].y *= rho.y;
        v[i].z *= rho.z;
    }
}

// misc

Vertex multi_vertex_weight_center(Vertex *v, size_t num) {
    Vertex g = {.x = 0.0, .y = 0.0, .z = 0.0};
    
    for (size_t i=0; i<num; i++) {
        g.x += v[i].x;
        g.y += v[i].y;
        g.z += v[i].z;
    }
    
    g.x /= (double) num;
    g.y /= (double) num;
    g.z /= (double) num;
    
    return g;
}

void multi_vertex_bounds(Vertex *v, size_t num, Vertex *center, Vertex *halfbox) {
    Vertex min = {.x = 0.0, .y = 0.0, .z = 0.0};
    Vertex max = {.x = 0.0, .y = 0.0, .z = 0.0};
    
    for (size_t i=0; i<num; i++) {
        if (v[i].x > max.x) max.x = v[i].x;
        else if (v[i].x < min.x) min.x = v[i].x;
        
        if (v[i].y > max.y) max.y = v[i].y;
        else if (v[i].y < min.y) min.y = v[i].y;
        
        if (v[i].z > max.z) max.z = v[i].z;
        else if (v[i].z < min.z) min.z = v[i].z;
    }
    
    center->x = (max.x + min.x) / 2.0;
    center->y = (max.y + min.y) / 2.0;
    center->z = (max.z + min.z) / 2.0;
    
    halfbox->x = max.x - center->x;
    halfbox->y = max.y - center->y;
    halfbox->z = max.z - center->z;
}
