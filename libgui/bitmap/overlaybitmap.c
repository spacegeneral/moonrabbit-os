#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <geom.h>
#include <libgui/libgui.h>


void overlay_bitmap(Bitmap *dest, Bitmap *overlay, Color24 *colorkey) {
    assert(overlay->geom.w <= dest->geom.w);
    assert(overlay->geom.h <= dest->geom.h);
    
    int *pixel_from = overlay->colordata;
    int *pixel_to = PIXELAT(dest, overlay->geom.x, overlay->geom.y);
    
    int carriage_return = overlay->geom.w;
    int carriage_return_bytes = carriage_return * COLOR24BPP;
    
    int pitch = dest->geom.w;
    
    if (colorkey == NULL) {
        for (int y=0; y<overlay->geom.h; y++) {
            memcpy(pixel_to, pixel_from, carriage_return_bytes);
            pixel_from += carriage_return;
            pixel_to += pitch;
        }
    } else {
        for (int y=0; y<overlay->geom.h; y++) {
            for (int x=0; x<overlay->geom.w; x++) {
                if (*pixel_from != *colorkey) {
                    *pixel_to = *pixel_from;
                }
                pixel_from++;
                pixel_to++;
            }
            pixel_to = pixel_to + pitch - carriage_return;
        }
    }
}


void safe_overlay_bitmap(Bitmap *dest, Bitmap *overlay, Color24 *colorkey) {
    Rect safe_overlap_area;
    
    int num_intersection = rect_intersection(overlay->geom, dest->geom, &safe_overlap_area);
    if (num_intersection == 0) return;
    
    if (safe_overlap_area.x < 0) safe_overlap_area.x = 0;
    if (safe_overlap_area.y < 0) safe_overlap_area.y = 0;
    
    area_overlay_bitmap(dest, overlay, &safe_overlap_area, colorkey);
}

void area_overlay_bitmap(Bitmap *dest, Bitmap *overlay, Rect *area, Color24 *colorkey) {
    int overlay_x_displace = overlay->geom.w - area->w;
    int overlay_y_displace = overlay->geom.h - area->h;
    
    int *pixel_from = overlay->colordata;
    int *pixel_to = PIXELAT(dest, area->x, area->y);
    
    int carriage_return_outer = overlay->geom.w;
    int pitch_outer = dest->geom.w;
    
    int carriage_return_inner = area->w;
    int carriage_return_inner_bytes = carriage_return_inner * COLOR24BPP;
    int pitch_inner = overlay->geom.w;
    
    if (colorkey == NULL) {
        for (int y=0; y<area->h; y++) {
            memcpy(pixel_to, pixel_from, carriage_return_inner_bytes);
            pixel_to += pitch_outer;
            pixel_from += pitch_inner;
        }
    } else {
        for (int y=0; y<area->h; y++) {
            for (int x=0; x<area->w; x++) {
                if (*pixel_from != *colorkey) {
                    *pixel_to = *pixel_from;
                }
                pixel_from++;
                pixel_to++;
            }
            pixel_to = pixel_to + pitch_outer - carriage_return_outer + overlay_x_displace;
            pixel_from = pixel_from + pitch_inner - carriage_return_inner;
        }
    }
}
