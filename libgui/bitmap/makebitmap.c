#include <stdlib.h>
#include <libgui/libgui.h>


Bitmap make_bitmap(Rect geom, Color24 fill) {
    Bitmap newbitmap;
    newbitmap.geom = geom;
    
    newbitmap.colordata = (int*) malloc(sizeof(int) * geom.w * geom.h);
    
    int *pixel = newbitmap.colordata;
    for (int y=0; y<geom.h; y++) {
        for (int x=0; x<geom.w; x++) {
            *pixel = (int) fill;
            pixel++;
        }
    }
    
    return newbitmap;
}


Bitmap make_bitmap_from_existing(Rect geom, Color24 fill, int *colordata) {
    Bitmap newbitmap;
    newbitmap.geom = geom;
    
    newbitmap.colordata = colordata;
    
    int *pixel = newbitmap.colordata;
    for (int y=0; y<geom.h; y++) {
        for (int x=0; x<geom.w; x++) {
            *pixel = (int) fill;
            pixel++;
        }
    }
    
    return newbitmap;
}
