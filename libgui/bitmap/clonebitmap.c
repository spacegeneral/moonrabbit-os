#include <stdlib.h>
#include <string.h>
#include <libgui/libgui.h>


Bitmap clone_bitmap(Bitmap *original) {
    Bitmap newbitmap;
    newbitmap.geom = original->geom;
    
    newbitmap.colordata = (int*) malloc(sizeof(int) * original->geom.w * original->geom.h);
    
    memcpy(newbitmap.colordata, original->colordata, sizeof(int) * original->geom.w * original->geom.h);
    
    return newbitmap;
}
