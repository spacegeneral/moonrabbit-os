#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <libgui/libgui.h>


Bitmap crop_bitmap(Bitmap *source, Rect croparea) {
    assert(croparea.x+croparea.w <= source->geom.w);
    assert(croparea.y+croparea.h <= source->geom.h);
    Bitmap newbitmap;
    newbitmap.geom = croparea;
    
    newbitmap.colordata = (int*) malloc(sizeof(int) * croparea.w * croparea.h);
    
    int *pixel_from = PIXELAT(source, croparea.x, croparea.y);
    int *pixel_to = newbitmap.colordata;
    
    int carriage_return = croparea.w;
    int carriage_return_bytes = carriage_return * COLOR24BPP;
    
    int pitch = source->geom.w;
    
    for (int y=0; y<croparea.h; y++) {
        memcpy(pixel_to, pixel_from, carriage_return_bytes);
        pixel_from += pitch;
        pixel_to += carriage_return;
    }
    
    return newbitmap;
}

Bitmap safe_crop_bitmap(Bitmap *source, Rect croparea) {
    Rect safe_croparea;
    
    int num_intersection = rect_intersection(croparea, source->geom, &safe_croparea);
    
    if (num_intersection == 1) {
        if (safe_croparea.x < 0) safe_croparea.x = 0;
        if (safe_croparea.y < 0) safe_croparea.y = 0;
        
        return crop_bitmap(source, safe_croparea);
    }
    
    return *source;
}
