#include <stdlib.h>
#include <libgui/libgui.h>


void relocate_bitmap(Bitmap *dest, Point coord) {
    dest->geom.x = coord.x;
    dest->geom.y = coord.y;
}


Bitmap shallow_relocated_copy(Bitmap *source, Point coord) {
    Bitmap newbitmap;
    newbitmap.colordata = source->colordata;
    newbitmap.geom = source->geom;
    relocate_bitmap(&newbitmap, coord);
    return newbitmap;
}
