#include <stdlib.h>
#include <stdio.h>
#include <yakumo.h>
#include <unicode.h>
#include <libgui/libgui.h>


#define TERM_CELL(numrow, numcol) (term->cells + (term->num_rows * numcol) + numrow)


SoftTerminal SoftTerminal_init(Bitmap *screen, Rect geom, BitmapFont font, Color24 color, Color24 background, int zoomlevel, bool with_background) {
    SoftTerminal newterm;
    
    newterm.screen = screen;
    newterm.geom = geom;
    newterm.font = font;
    newterm.color = color;
    newterm.background = background;
    newterm.zoomlevel = zoomlevel;
    utf8_decoder_init(&(newterm.decoder));
    newterm.num_rows = geom.h / (CHAR_HEIGHT_UNIT << zoomlevel);
    newterm.num_cols = geom.w / (CHAR_WIDTH_UNIT << zoomlevel);
    newterm.cells = (SoftTerminalCell*) calloc(sizeof(SoftTerminalCell) * newterm.num_rows * newterm.num_cols, 1);
    newterm.with_background = with_background;
    newterm.scroll_top = 0;
    newterm.scroll_bottom = newterm.num_rows;
    newterm.writing = WD_BOOK;
    newterm.flags = 0;
    
    newterm.cursor.x = 0;
    newterm.cursor.y = 0;
    
    return newterm;
}

void SoftTerminal_resize(SoftTerminal *term, Rect geom) {
    size_t old_num_rows, old_num_cols;
    old_num_rows = term->num_rows;
    old_num_cols = term->num_cols;
    term->geom = geom;
    term->num_rows = geom.h / (CHAR_HEIGHT_UNIT << term->zoomlevel);
    term->num_cols = geom.w / (CHAR_WIDTH_UNIT << term->zoomlevel);
    if (term->cursor.x >= term->num_cols) {
        term->cursor.x = term->num_cols - 1;
    }
    if (term->cursor.y >= term->num_rows) {
        term->cursor.y = term->num_rows - 1;
    }
    term->scroll_bottom = term->num_rows;
    
    // resize also the virtual text cells
    debug_printf("%lux%lu -> %lux%lu\r\n", old_num_cols, old_num_rows, term->num_cols, term->num_rows);
    SoftTerminalCell* newcells = (SoftTerminalCell*) calloc(sizeof(SoftTerminalCell) * term->num_rows * term->num_cols, 1);
    size_t transition_rowlength = min(term->num_cols, old_num_cols);
    size_t transition_collength =min(term->num_rows, old_num_rows);
    for (size_t y=0; y<transition_collength; y++) {
        for (size_t x=0; x<transition_rowlength; x++) {
            //debug_printf("%lux%lu\r\n", x, y);
            newcells[(transition_collength * x) + y] = term->cells[(old_num_rows * x) + y];
        }
    }
    free(term->cells);
    term->cells = newcells;
}

size_t SoftTerminal_copy_selection(SoftTerminal *term, char* destination, size_t max_length) {
    //TODO conform to writing-direction
    size_t length_written = 0;
    for (size_t y=0; y<term->num_rows; y++) {
        for (size_t x=0; x<term->num_cols; x++) {
            length_written += snprintf(destination, max_length-length_written, "%s", TERM_CELL(y, x)->text_bytes);
        }
    }
    return length_written;
}


//TODO: side-scrolling


#define NEWLINE_BOOK() \
    term->cursor.x = 0; \
    term->cursor.y += 1<<term->zoomlevel; \
    if (term->cursor.y >= term->num_rows) { \
        SoftTerminal_scroll_up(term, 1); \
        term->cursor.y -= 1<<term->zoomlevel; \
    }
    
#define NEWLINE_RABBI() \
    term->cursor.x = term->num_cols-1; \
    term->cursor.y += 1<<term->zoomlevel; \
    if (term->cursor.y >= term->num_rows) { \
        SoftTerminal_scroll_up(term, 1); \
        term->cursor.y -= 1<<term->zoomlevel; \
    }
    
#define NEWLINE_TEGAMI() \
    term->cursor.y = 0; \
    term->cursor.x -= 1<<term->zoomlevel; \
    if (term->cursor.x < 0) { \
        SoftTerminal_scroll_left(term, 1); \
        term->cursor.x += 1<<term->zoomlevel; \
        term->cursor.x=term->num_cols-1; /* TODO temp fix only, because SoftTerminal_scroll_left is a stub */ \
    }

#define NEWLINE_LEMURIA() \
    term->cursor.y = term->num_rows-1; \
    term->cursor.x += 1<<term->zoomlevel; \
    if (term->cursor.x >= term->num_cols) { \
        SoftTerminal_scroll_right(term, 1); \
        term->cursor.x -= 1<<term->zoomlevel; \
        term->cursor.x=0; /* TODO temp fix only, because SoftTerminal_scroll_right is a stub */ \
    }
    
#define NEWLINE() \
    switch (term->writing) { \
        case WD_BOOK: \
            NEWLINE_BOOK() \
        break; \
        case WD_RABBI: \
            NEWLINE_RABBI() \
        break; \
        case WD_TEGAMI: \
            NEWLINE_TEGAMI() \
        break; \
        case WD_LEMURIA: \
            NEWLINE_LEMURIA() \
        break; \
        default: break; \
    }


#define BACKSPACE() \
    switch (term->writing) { \
        case WD_BOOK: \
            if (term->cursor.x-1 >= 0) term->cursor.x -= 1<<term->zoomlevel; \
        break; \
        case WD_RABBI: \
            if (term->cursor.x+1 < term->num_cols) term->cursor.x += 1<<term->zoomlevel; \
        break; \
        case WD_TEGAMI: \
            if (term->cursor.y-1 >= 0) term->cursor.y -= 1<<term->zoomlevel; \
        break; \
        case WD_LEMURIA: \
            if (term->cursor.y+1 < term->num_rows) term->cursor.y += 1<<term->zoomlevel; \
        break; \
        default: break; \
    }

#define CARRIAGE_RETURN() \
    switch (term->writing) { \
        case WD_BOOK: \
            term->cursor.x = 0; \
        break; \
        case WD_RABBI: \
            term->cursor.x = term->num_cols-1; \
        break; \
        case WD_TEGAMI: \
            term->cursor.y = 0; \
        break; \
        case WD_LEMURIA: \
            term->cursor.y = term->num_cols-1; \
        break; \
        default: break; \
    }
    
#define PRE_ADVANCE() \
    switch (term->writing) { \
        case WD_RABBI: \
            for (int a=0; a<advance; a++) { \
                term->cursor.x -= 1<<term->zoomlevel; \
                if (term->cursor.x < 0) { \
                    NEWLINE_RABBI() \
                } \
            } \
        break; \
        case WD_LEMURIA: \
            term->cursor.y -= 1<<term->zoomlevel; \
            if (term->cursor.y < 0) { \
                NEWLINE_LEMURIA() \
            } \
        break; \
        default: break; \
    }
    
#define POST_ADVANCE() \
    switch (term->writing) { \
        case WD_BOOK: \
            for (int a=0; a<advance; a++) { \
                term->cursor.x += 1<<term->zoomlevel; \
                if (term->cursor.x >= term->num_cols) { \
                    NEWLINE_BOOK() \
                } \
            } \
        break; \
        case WD_RABBI: \
            term->cursor.x -= 1<<term->zoomlevel; \
            if (term->cursor.x < 0) { \
                NEWLINE_RABBI() \
            } \
        break; \
        case WD_TEGAMI: \
            term->cursor.y += 1<<term->zoomlevel; \
            if (term->cursor.y >= term->num_rows) { \
                NEWLINE_TEGAMI() \
            } \
        break; \
        case WD_LEMURIA: \
            term->cursor.y -= 1<<term->zoomlevel; \
            if (term->cursor.y < 0) { \
                NEWLINE_LEMURIA() \
            } \
        break; \
        default: break; \
    }
    
#define ROTATE_SKIPBACK() \
{ \
    int tmp; \
    switch (term->writing) { \
        case WD_BOOK: \
        break; \
        case WD_RABBI: \
            delta_x = -delta_x; \
        break; \
        case WD_TEGAMI: \
            tmp = delta_y; \
            delta_y = delta_x; \
            delta_x = -tmp; \
        break; \
        case WD_LEMURIA: \
            tmp = delta_y; \
            delta_y = -delta_x; \
            delta_x = tmp; \
        break; \
        default: break; \
    } \
}


static int calc_tabdelta(SoftTerminal *term) {
    int stoppos;
    int deltapos;
    
    switch (term->writing) {
        case WD_BOOK:
            if (term->cursor.x % TAB_SPAN == 0) {
                stoppos = term->cursor.x + TAB_SPAN;
            } else {
                stoppos = ((term->cursor.x / TAB_SPAN) + 1) * TAB_SPAN;
            }
            deltapos = stoppos - term->cursor.x;
        break;
        case WD_RABBI:
            if (term->cursor.x % TAB_SPAN == 0) {
                stoppos = term->cursor.x - TAB_SPAN;
            } else {
                stoppos = ((term->cursor.x / TAB_SPAN) - 1) * TAB_SPAN;
            }
            deltapos = stoppos - term->cursor.x;
        break;
        case WD_TEGAMI:
            if (term->cursor.y % TAB_SPAN == 0) {
                stoppos = term->cursor.y + TAB_SPAN;
            } else {
                stoppos = ((term->cursor.y / TAB_SPAN) + 1) * TAB_SPAN;
            }
            deltapos = stoppos - term->cursor.y;
        break;
        case WD_LEMURIA:
            if (term->cursor.y % TAB_SPAN == 0) {
                stoppos = term->cursor.y - TAB_SPAN;
            } else {
                stoppos = ((term->cursor.y / TAB_SPAN) - 1) * TAB_SPAN;
            }
            deltapos = stoppos - term->cursor.y;
        break;
        
        default:
            deltapos = 0;
    }
    
    deltapos = clamp_pos(deltapos);
    return deltapos;
}


static void SoftTerminal_put_cell(SoftTerminal *term, int codepoint, int row, int col) {
    sprintf(TERM_CELL(row,col)->text_bytes, "%s", utf8_codepoint_to_atom_bytes(codepoint));
    TERM_CELL(row,col)->selected = (term->flags & TERM_FLAG_SELECTED);
}

static void SoftTerminal_blank_cell(SoftTerminal *term, int row, int col) {
    memset(TERM_CELL(row,col)->text_bytes, 0, SOFT_TERM_CELL_BYTES);
    TERM_CELL(row,col)->selected = false;
}

static void SoftTerminal_clear_all_cells(SoftTerminal *term) {
    memset(term->cells, 0, sizeof(SoftTerminalCell) * term->num_rows * term->num_cols);
}


void SoftTerminal_put_codepoint(SoftTerminal *term, int codepoint) {
    if (codepoint == '\n') {
        
        NEWLINE()
        
    } else if (codepoint == '\t') {
        
        int deltax = calc_tabdelta(term);
        for (int dx=0; dx<deltax; dx++) {
            SoftTerminal_put_codepoint(term, ' ');
        }
        
    } else if (codepoint == '\b') {
        
        BACKSPACE()
        
        fill_codepoint_block(term->screen, 
                             term->font, 
                             term->geom.x + term->cursor.x * (CHAR_WIDTH_UNIT),
                             term->geom.y + term->cursor.y * (CHAR_HEIGHT_UNIT),
                             term->zoomlevel,
                             codepoint, 
                             term->background);
        SoftTerminal_blank_cell(term, term->cursor.y, term->cursor.x);
        
    } else if (codepoint == '\r') {
        
        CARRIAGE_RETURN()
        
    } else {
        
        int color = term->color;
        int background_color = term->background;
        bool with_background = term->with_background;
        
        if (term->flags & TERM_FLAG_SELECTED) {
            with_background = true;
            color = term->background;
            background_color = term->color;
        }
        
        int skipback_factor = combining_factor(codepoint);
        
        if (skipback_factor == NOT_COMBINING) {
            int advance = (codepoint >= term->font.upper_range ? 0 : term->font.codepoints[codepoint].dimension);
            
            PRE_ADVANCE()
            
            advance = draw_codepoint_anywhere(term->screen, 
                                              term->font, 
                                              term->geom.x + term->cursor.x * (CHAR_WIDTH_UNIT),
                                              term->geom.y + term->cursor.y * (CHAR_HEIGHT_UNIT),
                                              term->zoomlevel,
                                              codepoint, 
                                              term->flags,
                                              color, 
                                              background_color, 
                                              with_background) + 1;
            SoftTerminal_put_cell(term, codepoint, term->cursor.y, term->cursor.x);
            
            POST_ADVANCE()
            
        } else {
            // compute skipback coordinates in WD_BOOK style, and then rotate them appropriately
            int delta_x = (term->cursor.x * CHAR_WIDTH_UNIT) + (skipback_factor << term->zoomlevel);
            int delta_y = term->cursor.y * (CHAR_HEIGHT_UNIT);
            ROTATE_SKIPBACK();
            
            int start_x = delta_x + term->geom.x;
            int start_y = delta_y + term->geom.y;
            
            if (start_x < 0) {
                start_x = (term->num_cols * (CHAR_WIDTH_UNIT << term->zoomlevel)) + start_x;
                start_y -= CHAR_HEIGHT_UNIT << term->zoomlevel;
                if (start_y < 0) start_y = 0;
            }
            
            draw_codepoint_anywhere(term->screen, 
                                    term->font, 
                                    start_x,
                                    start_y,
                                    term->zoomlevel,
                                    codepoint, 
                                    term->flags,
                                    term->color, 
                                    term->background, 
                                    false);
            SoftTerminal_put_cell(term, codepoint, delta_y / CHAR_HEIGHT_UNIT, delta_x / CHAR_WIDTH_UNIT);
        }
        
    }
}

void SoftTerminal_putchar(SoftTerminal *term, char character) {
    int codepoint = utf8_decoder_feed(&(term->decoder), character);
    if (codepoint > 0) {
        SoftTerminal_put_codepoint(term, process_codepoint(term->flags, codepoint, term->font.upper_range));
    }
}


static void SoftTerminal_printf_interface(char character, void *term) {
    SoftTerminal_putchar((SoftTerminal*) term, character);
}

void SoftTerminal_vprintf(SoftTerminal *term, const char *fmt, va_list va) {
    fctvprintf(&SoftTerminal_printf_interface, (void*) term, fmt, va);
}

void SoftTerminal_printf(SoftTerminal *term, const char *fmt, ...) {
    va_list va;
    va_start(va, fmt);
    SoftTerminal_vprintf(term, fmt, va);
    va_end(va);
}

void SoftTerminal_clear(SoftTerminal *term) {
    draw_rectangle_area(term->screen, term->geom, term->background);
    utf8_decoder_init(&(term->decoder));
    SoftTerminal_clear_all_cells(term);
}

void SoftTerminal_scroll_down(SoftTerminal *term, int num_cols) {
    int row_height = CHAR_HEIGHT_UNIT << term->zoomlevel;
    int numpixels = num_cols * row_height;
    
    // move all the pixels down by numpixels
    for (int yb = (term->scroll_bottom * row_height) + term->geom.y - 1;
             yb >= (term->scroll_top * row_height + numpixels) + term->geom.y; 
             yb--) {
        for (int x=term->geom.x; x<term->geom.w+term->geom.x; x++) {
            *PIXELAT(term->screen, x, yb) = *PIXELAT(term->screen, x, yb - numpixels);
        }
    }
    
    // move all the cells down by num_cols
    memmove(TERM_CELL(term->scroll_top + num_cols, 0), 
            TERM_CELL(term->scroll_top, 0), 
            (term->num_rows - num_cols) * sizeof(SoftTerminalCell) * term->num_cols);
    
    // clear all pixel in the empty area created at the top
    Rect topbar = {
        .x = term->geom.x,
        .y = term->geom.y,
        .w = term->geom.w,
        .h = numpixels,
    };
    draw_rectangle_area(term->screen, topbar, term->background);
    
    // clear all cells in the empty area created at the top
    for (int x=0; x<term->num_cols; x++) {
        memset(TERM_CELL(term->scroll_top, x), 0, sizeof(SoftTerminalCell));
    }
}

void SoftTerminal_scroll_up(SoftTerminal *term, int num_cols) {
    int row_height = CHAR_HEIGHT_UNIT << term->zoomlevel;
    int numpixels = num_cols * row_height;
    
    // move all the pixels up by numpixels
    for (int yb = (term->scroll_top * row_height) + term->geom.y; 
             yb < (term->scroll_bottom * row_height - numpixels) + term->geom.y; 
             yb++) {
        for (int x=term->geom.x; x<term->geom.w+term->geom.x; x++) {
            *PIXELAT(term->screen, x, yb) = *PIXELAT(term->screen, x, yb + numpixels);
        }
    }
    
    // move all the cells up by num_cols
    memmove(TERM_CELL(term->scroll_top, 0), 
            TERM_CELL(term->scroll_top + num_cols, 0), 
            (term->num_rows - num_cols) * sizeof(SoftTerminalCell) * term->num_cols);
    
    // clear all cells in the empty area created at the bottom
    Rect botbar = {
        .x = term->geom.x,
        .y = term->geom.y + (term->scroll_bottom * row_height) - numpixels,
        .w = term->geom.w,
        .h = numpixels,
    };
    draw_rectangle_area(term->screen, botbar, term->background);
    
    // clear all cells in the empty area created at the bottom
    for (int x=0; x<term->num_cols; x++) {
        memset(TERM_CELL(term->scroll_bottom - num_cols, x), 0, sizeof(SoftTerminalCell));
    }
}

void SoftTerminal_scroll_left(SoftTerminal *term, int num_rows) {
    //TODO
}

void SoftTerminal_scroll_right(SoftTerminal *term, int num_rows) {
    //TODO
}

void SoftTerminal_set_scroll_range(SoftTerminal *term, int scroll_top, int scroll_bottom) {
    if (scroll_bottom <= scroll_top) return;
    if (scroll_bottom > term->num_rows || scroll_bottom < 0) return;
    if (scroll_top > term->num_rows || scroll_top < 0) return;
    
    term->scroll_top = scroll_top * (CHAR_HEIGHT_UNIT << term->zoomlevel);
    term->scroll_bottom = scroll_bottom * (CHAR_HEIGHT_UNIT << term->zoomlevel);
}

void SoftTerminal_gotoxy(SoftTerminal *term, int x, int y) {
    if (x>=0 && x<term->num_cols && y>=0 && y<term->num_rows) {
        term->cursor.x = x;
        term->cursor.y = y;
    }
}

void SoftTerminal_select_at(SoftTerminal *term, int x, int y) {
    if (x>=0 && x<term->num_cols && y>=0 && y<term->num_rows) {
        if (TERM_CELL(y,x)->selected) return;
        
        int backup_x = term->cursor.x;
        int backup_y = term->cursor.y;
        unsigned backup_flags = term->flags;
        
        term->cursor.x = x;
        term->cursor.y = y;
        term->flags = term->flags | TERM_FLAG_SELECTED;
        
        SoftTerminal_printf(term, "%s", TERM_CELL(y,x)->text_bytes);
        
        term->cursor.x = backup_x;
        term->cursor.y = backup_y;
        term->flags = backup_flags;
    }
}

void SoftTerminal_unselect_at(SoftTerminal *term, int x, int y) {
    if (x>=0 && x<term->num_cols && y>=0 && y<term->num_rows) {
        if (!(TERM_CELL(y,x)->selected)) return;
        
        int backup_x = term->cursor.x;
        int backup_y = term->cursor.y;
        unsigned backup_flags = term->flags;
        
        term->cursor.x = x;
        term->cursor.y = y;
        term->flags = term->flags & ~(TERM_FLAG_SELECTED);
        
        SoftTerminal_printf(term, "%s", TERM_CELL(y,x)->text_bytes);
        
        term->cursor.x = backup_x;
        term->cursor.y = backup_y;
        term->flags = backup_flags;
    }
}

void SoftTerminal_unselect_all(SoftTerminal *term) {
    for (int x=0; x<term->num_cols; x++) {
        for (int y=0; y<term->num_rows; y++) {
            SoftTerminal_unselect_at(term, x, y);
        }
    }
}

void SoftTerminal_set_writing_direction(SoftTerminal *term, WritingDirection direction) {
    term->writing = direction;
}
