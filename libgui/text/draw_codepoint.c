#include <stdlib.h>
#include <stdio.h>
#include <libgui/libgui.h>


static BitmapCodePoint undefined_character = {
    .dimension = 0,
    .bitmap = {0x00, 0x00, 0x00, 0x7E, 0x66, 0x5A, 0x5A, 0x7A, 0x76, 0x76, 0x7E, 0x76, 0x76, 0x7E, 0x00, 0x00},
};



#define PUTPIXEL_COLOR24(pixel, color) \
{ \
    (pixel)[0] = color & 255; \
    (pixel)[1] = (color >> 8) & 255; \
    (pixel)[2] = (color >> 16) & 255; \
}


int draw_codepoint_anywhere(Bitmap *dest, 
                            BitmapFont font, 
                            int start_x, 
                            int start_y, 
                            int scale_log2,
                            int codepoint, 
                            unsigned flags,
                            Color24 color, 
                            Color24 background, 
                            bool with_background) {
    BitmapCodePoint chardata = (codepoint >= font.upper_range ? undefined_character : font.codepoints[codepoint]);
    int char_width, char_width_power, char_height;
    
    switch (chardata.dimension) {
        case CHAR_8x16:
            char_width = 8;
            char_width_power = 3;
            char_height = 16;
            break;
        case CHAR_16x16:
            char_width = 16;
            char_width_power = 4;
            char_height = 16;
            break;
        default:
            return chardata.dimension;
    }
    
    char_width <<= scale_log2;
    char_width_power += scale_log2;
    char_height <<= scale_log2;
    
    int ypace_power = char_width_power - 3 - scale_log2;
    int carriage_return = COLOR24BPP << char_width_power;
    int pitch = COLOR24BPP * dest->geom.w;
    
    unsigned char *screenpos = (unsigned char *)dest->colordata;
    screenpos += (start_y * pitch) + (start_x * COLOR24BPP);
    
    if (!with_background) {
        for (int y = 0; y < char_height; y++) {
            for (int x = 0; x < char_width; x++) {
                int ysel = y >> scale_log2;
                int xsel = x >> scale_log2;
                char row = chardata.bitmap[(ysel << ypace_power) + (xsel >> 3)];
                if (row & (0b10000000 >> (xsel % 8))) PUTPIXEL_COLOR24(screenpos, color)
                screenpos += COLOR24BPP;
            }
            screenpos = screenpos + pitch - carriage_return;
        }
    } else {
        for (int y = 0; y < char_height; y++) {
            for (int x = 0; x < char_width; x++) {
                int ysel = y >> scale_log2;
                int xsel = x >> scale_log2;
                char row = chardata.bitmap[(ysel << ypace_power) + (xsel >> 3)];
                if (row & (0b10000000 >> (xsel % 8))) PUTPIXEL_COLOR24(screenpos, color)
                else PUTPIXEL_COLOR24(screenpos, background)
                screenpos += COLOR24BPP;
            }
            screenpos = screenpos + pitch - carriage_return;
        }
    }
    
    if (flags & TERM_FLAG_UNDERLINE) {
        screenpos = ((unsigned char *)dest->colordata) + ((start_y + char_height - 1) * pitch) + (start_x * COLOR24BPP);
        for (size_t x = 0; x < char_width; x++) {
            PUTPIXEL_COLOR24(screenpos, color);
            screenpos += COLOR24BPP;
        }
    }
    
    
    return chardata.dimension;
}


int fill_codepoint_block(Bitmap *dest, 
                         BitmapFont font, 
                         int start_x, 
                         int start_y, 
                         int scale_log2,
                         int codepoint, 
                         Color24 color) {
    int char_width, char_width_power, char_height, area;
    BitmapCodePoint chardata = (codepoint >= font.upper_range ? undefined_character : font.codepoints[codepoint]);
    
    switch (chardata.dimension) {
        case CHAR_8x16:
            char_width = 8;
            char_width_power = 3;
            char_height = 16;
            area = 128;
            break;
        case CHAR_16x16:
            char_width = 16;
            char_width_power = 4;
            char_height = 16;
            area = 256;
            break;
        default:
            return chardata.dimension;
    }
    
    char_width <<= scale_log2;
    char_width_power += scale_log2;
    char_height <<= scale_log2;
    
    #pragma GCC unroll 8
    for (int index=0; index<area; index++) {
        int x = index % char_width;
        int y = index >> char_width_power;
        *PIXELAT(dest, x + start_x, y + start_y) = color;
    }
    
    return chardata.dimension;
}
