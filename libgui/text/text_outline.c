#include <libgui/text.h>
#include <string.h>


Rect get_text_outline_linear(const char* text, BitmapFont *font) {
    Utf8Decoder decoder;
    utf8_decoder_init(&decoder);
    
    Rect outline = {
        .x = 0,
        .y = 0,
        .w = 0,
        .h = CHAR_HEIGHT_UNIT,
    };
    long l_accumulator = 0;
    
    for (size_t str_pos = 0; str_pos < strlen(text); str_pos++) {
        int codepoint = utf8_decoder_feed(&decoder, text[str_pos]);
        if (codepoint < 0) continue;
        if (codepoint > font->upper_range) continue;
        int skipback_factor = combining_factor(codepoint);
        if (skipback_factor == NOT_COMBINING) {
            l_accumulator += (long) (get_codepoint_width_units(font->codepoints + codepoint) * CHAR_WIDTH_UNIT);
        } else {
            l_accumulator += (long) skipback_factor;
        }
    }
    if (l_accumulator > 0) outline.w = l_accumulator;
    return outline;
}

Rect get_text_outline_width_limited(const char* text, BitmapFont *font, long w_limit) {
    Utf8Decoder decoder;
    utf8_decoder_init(&decoder);
    
    Rect outline = {
        .x = 0,
        .y = 0,
        .w = 0,
        .h = CHAR_HEIGHT_UNIT,
    };
    long l_accumulator = 0;
    long max_length = 0;
    long l_ldelta = 0;
    
    for (size_t str_pos = 0; str_pos < strlen(text); str_pos++) {
        int codepoint = utf8_decoder_feed(&decoder, text[str_pos]);
        if (codepoint < 0) continue;
        if (codepoint > font->upper_range) continue;
        int skipback_factor = combining_factor(codepoint);
        if (skipback_factor == NOT_COMBINING) {
            l_ldelta = (long) (get_codepoint_width_units(font->codepoints + codepoint) * CHAR_WIDTH_UNIT);
        } else {
            l_ldelta = (long) skipback_factor;
        }
        
        if (l_accumulator + l_ldelta > w_limit) {
            outline.h += CHAR_HEIGHT_UNIT;
            l_accumulator = l_ldelta;
        } else {
            l_accumulator += l_ldelta;
            if (l_accumulator > max_length) max_length = l_accumulator;
        }
    }
    outline.w = max_length;
    return outline;
}
