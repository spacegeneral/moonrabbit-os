#include <stdlib.h>
#include <stdio.h>
#include <libgui/libgui.h>
#include <sys/share.h>



BitmapFont load_lbf_font(char *path) {
    BitmapFont newfont = {
        .upper_range = 0,
        .codepoints = NULL,
        .shared = false
    };
    
    unsigned long long file_size;
    uint8_t* memarea;
    if (load_shared_file(path, &file_size, 0, (uintptr_t*)&memarea) == 0) {
        newfont.upper_range = (file_size - sizeof(BitmapFontHeader)) / sizeof(BitmapCodePoint);
        newfont.codepoints = (BitmapCodePoint*) (memarea + sizeof(BitmapFontHeader));
        newfont.shared = true;
        return newfont;
    }
    
    FILE *fp = fopen(path, "rb");
    if (fp == NULL) return newfont;
    
    BitmapFontHeader header;
    if (fread(&header, sizeof(BitmapFontHeader), 1, fp) != 1) {
        fclose(fp);
        return newfont;
    }
    
    BitmapCodePoint *custom_bitmap_font = (BitmapCodePoint*) malloc(sizeof(BitmapCodePoint) * header.upper_range);
    if (fread(custom_bitmap_font, sizeof(BitmapCodePoint), header.upper_range, fp) < header.upper_range) {
        free(custom_bitmap_font);
        fclose(fp);
        return newfont;
    }
    
    newfont.upper_range = header.upper_range;
    newfont.codepoints = custom_bitmap_font;
    
    fclose(fp);
    return newfont;
}

void destroy_bitmap_font(BitmapFont font) {
    if (!font.shared) free(font.codepoints);
}
