#ifndef _LIBGUI_HELPERS_H
#define _LIBGUI_HELPERS_H


#include <libgui/domusclient.h>


bool dialog_yesno(const char * mesg, const char *yeslabel, const char *nolabel, BitmapFont font);
bool dialog_ok(const char * mesg, const char *oklabel, BitmapFont font);


#endif
