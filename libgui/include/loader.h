#ifndef _LIBGUI_LOADER_H
#define _LIBGUI_LOADER_H


#include <graphics.h>


typedef enum ImgLoaderError {
    IMG_LOADER_SUCCESS = 0,
    IMG_LOADER_FILE_NOT_FOUND,
    IMG_LOADER_JPEG_DECODE_ERR,
    IMG_LOADER_PNG_DECODE_ERR,
    IMG_LOADER_UNSUPPORTED_FORMAT,
    IMG_LOADER_BAD_PARAMETER,
    
} ImgLoaderError;


int load_jpeg_image(Bitmap *dest, const char *filename, float scale);
int load_png_image(Bitmap *dest, const char *filename, float scale);
int load_any_image(Bitmap *dest, const char *filename, float scale);

int load_jpeg_image_with_size(Bitmap *dest, const char *filename, int show_width, int show_height);
int load_png_image_with_size(Bitmap *dest, const char *filename, int show_width, int show_height);
int load_any_image_with_size(Bitmap *dest, const char *filename, int show_width, int show_height);

#endif
 
