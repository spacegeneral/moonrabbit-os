#ifndef _LIBGUI_3D_H
#define _LIBGUI_3D_H


typedef struct Vertex {
    double x;
    double y;
    double z;
} Vertex;


Point vertex_project_ortho(Vertex v);
Point *multi_vertex_project_ortho(Vertex *v, size_t num);

Vertex vertex_rotate_euler_x(Vertex v, double alpha);
Vertex vertex_rotate_euler_y(Vertex v, double alpha);
void multi_vertex_rotate_euler_x(Vertex *v, size_t num, double alpha);
void multi_vertex_rotate_euler_y(Vertex *v, size_t num, double alpha);

Vertex vertex_translate(Vertex a, Vertex b);
Vertex vertex_invert(Vertex a);
void multi_vertex_translate(Vertex *v, size_t num, Vertex delta);

Vertex vertex_scale(Vertex a, Vertex b);
void multi_vertex_scale(Vertex *v, size_t num, Vertex rho);

Vertex multi_vertex_weight_center(Vertex *v, size_t num);
void multi_vertex_bounds(Vertex *v, size_t num, Vertex *center, Vertex *halfbox);

#endif
