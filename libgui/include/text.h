#ifndef _LIBGUI_TEXT_H
#define _LIBGUI_TEXT_H


#include <graphics.h>
#include <stdbool.h>
#include <stdarg.h>
#include <utf8decoder.h>
#include <unicode.h>
#include <libgui/color.h>
#include <sys/term.h>


#define TAB_SPAN 4
#define SOFT_TERM_CELL_BYTES 20


typedef struct BitmapFont {
    size_t upper_range;
    BitmapCodePoint *codepoints;
    bool shared;
} BitmapFont;


typedef struct SoftTerminalCell {
    char text_bytes[SOFT_TERM_CELL_BYTES];
    bool selected;
} SoftTerminalCell;


typedef struct SoftTerminal {
    Bitmap* screen;
    SoftTerminalCell* cells;
    Rect geom;
    Point cursor;
    WritingDirection writing;
    int num_rows;
    int num_cols;
    int zoomlevel;
    Color24 color;
    Color24 background;
    BitmapFont font;
    Utf8Decoder decoder;
    bool with_background;
    int scroll_top;
    int scroll_bottom;
    unsigned flags;
} SoftTerminal;


Rect get_text_outline_linear(const char* text, BitmapFont *font);
Rect get_text_outline_width_limited(const char* text, BitmapFont *font, long w_limit);


SoftTerminal SoftTerminal_init(Bitmap *screen, Rect geom, BitmapFont font, Color24 color, Color24 background, int zoomlevel, bool with_background);
void SoftTerminal_put_codepoint(SoftTerminal *term, int codepoint);
void SoftTerminal_putchar(SoftTerminal *term, char character);
void SoftTerminal_vprintf(SoftTerminal *term, const char *fmt, va_list va);
void SoftTerminal_printf(SoftTerminal *term, const char *fmt, ...);
void SoftTerminal_clear(SoftTerminal *term);
void SoftTerminal_toggle_cursor(SoftTerminal *term, bool onoff);
void SoftTerminal_scroll_down(SoftTerminal *term, int num_cols);
void SoftTerminal_scroll_up(SoftTerminal *term, int num_cols);
void SoftTerminal_scroll_left(SoftTerminal *term, int num_rows); //TODO: stub
void SoftTerminal_scroll_right(SoftTerminal *term, int num_rows); //TODO: stub
void SoftTerminal_set_scroll_range(SoftTerminal *term, int scroll_top, int scroll_bottom);
void SoftTerminal_gotoxy(SoftTerminal *term, int x, int y);
void SoftTerminal_resize(SoftTerminal *term, Rect geom);
void SoftTerminal_set_writing_direction(SoftTerminal *term, WritingDirection direction);

size_t SoftTerminal_copy_selection(SoftTerminal *term, char* destination, size_t max_length);
void SoftTerminal_select_at(SoftTerminal *term, int x, int y);
void SoftTerminal_unselect_at(SoftTerminal *term, int x, int y);
void SoftTerminal_unselect_all(SoftTerminal *term);


BitmapFont load_lbf_font(char *path);
void destroy_bitmap_font(BitmapFont font);

int draw_codepoint_anywhere(Bitmap *dest, 
                            BitmapFont font, 
                            int start_x, 
                            int start_y, 
                            int scale_log2,
                            int codepoint, 
                            unsigned flags,
                            Color24 color, 
                            Color24 background, 
                            bool with_background);

int fill_codepoint_block(Bitmap *dest, 
                         BitmapFont font, 
                         int start_x, 
                         int start_y, 
                         int scale_log2,
                         int codepoint, 
                         Color24 color);

#endif
