#ifndef _LIBGUI_SHAPES_H
#define _LIBGUI_SHAPES_H


#include <graphics.h>
#include <libgui/color.h>


void draw_rectangle(Bitmap *bitmap, Rect area, Color24 line);
void draw_rectangle_area(Bitmap *bitmap, Rect area, Color24 fill);
void safe_draw_rectangle(Bitmap *bitmap, Rect area, Color24 line);
void safe_draw_rectangle_area(Bitmap *bitmap, Rect area, Color24 fill);

void draw_line(Bitmap *bitmap, Point a, Point b, Color24 color);
void draw_thick_line(Bitmap *bitmap, Point a, Point b, int thickness, Color24 color);
void safe_draw_line(Bitmap *bitmap, Point a, Point b, Color24 color);
void safe_draw_thick_line(Bitmap *bitmap, Point a, Point b, int thickness, Color24 color);

void draw_circle(Bitmap *bitmap, Point center, int radius, Color24 color);
void draw_circle_area(Bitmap *bitmap, Point center, int radius, Color24 color);
void safe_draw_circle(Bitmap *bitmap, Point center, int radius, Color24 color);
void safe_draw_circle_area(Bitmap *bitmap, Point center, int radius, Color24 color);


#endif
