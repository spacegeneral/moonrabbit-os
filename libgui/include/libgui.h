#ifndef _LIBGUI_LIBGUI_H
#define _LIBGUI_LIBGUI_H


#include <rice.h>
#include <libgui/bitmap.h>
#include <libgui/color.h>
#include <libgui/shapes.h>
#include <libgui/text.h>
#include <libgui/msg.h>
#include <libgui/domusclient.h>
#include <libgui/loader.h>
#include <libgui/helpers.h>
#include <libgui/3d.h>


#endif
