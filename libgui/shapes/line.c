#include <libgui/libgui.h>


void draw_line_bresenham_low(Bitmap *bitmap, Point a, Point b, Color24 color) {
    int dx = b.x - a.x;
    int dy = b.y - a.y;
    int xi = 1;
    int yi = 1;
    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }
    int D = 2*dy - dx;
    int y = a.y;
    
    int *pixel = PIXELAT(bitmap, a.x, y);
    int y_px_inc = bitmap->geom.w*yi;
    
    for (int x=a.x; x<b.x; x+=xi) {
        *pixel = color;
        
        if (D > 0) {
            pixel += y_px_inc;
            D = D - 2*dx;
        }
        D = D + 2*dy;
        
        pixel += xi;
    }
}

void safe_draw_line_bresenham_low(Bitmap *bitmap, Point a, Point b, Color24 color) {
    int dx = b.x - a.x;
    int dy = b.y - a.y;
    int xi = 1;
    int yi = 1;
    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }
    int D = 2*dy - dx;
    int y = a.y;
    
    int *pixel = PIXELAT(bitmap, a.x, y);
    int y_px_inc = bitmap->geom.w*yi;
    
    for (int x=a.x; x<b.x; x+=xi) {
        if (x >= 0 && x<bitmap->geom.w && y >= 0 && y<bitmap->geom.h) {
            *pixel = color;
        }
        
        if (D > 0) {
            pixel += y_px_inc;
            y += yi;
            D = D - 2*dx;
        }
        D = D + 2*dy;
        
        pixel += xi;
    }
}


void draw_line_bresenham_high(Bitmap *bitmap, Point a, Point b, Color24 color) {
    int dx = b.x - a.x;
    int dy = b.y - a.y;
    int xi = 1;
    int yi = 1;
    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }
    int D = 2*dx - dy;
    int x = a.x;
    
    int *pixel = PIXELAT(bitmap, x, a.y);
    int y_px_inc = bitmap->geom.w*yi;
    
    for (int y=a.y; y<b.y; y+=yi) {
        *pixel = color;
        
        if (D > 0) {
            pixel += xi;
            D = D - 2*dy;
        }
        D = D + 2*dx;
        
        pixel += y_px_inc;
    }
}

void safe_draw_line_bresenham_high(Bitmap *bitmap, Point a, Point b, Color24 color) {
    int dx = b.x - a.x;
    int dy = b.y - a.y;
    int xi = 1;
    int yi = 1;
    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }
    int D = 2*dx - dy;
    int x = a.x;
    
    int *pixel = PIXELAT(bitmap, x, a.y);
    int y_px_inc = bitmap->geom.w*yi;
    
    for (int y=a.y; y<b.y; y+=yi) {
        if (x >= 0 && x<bitmap->geom.w && y >= 0 && y<bitmap->geom.h) {
            *pixel = color;
        }
        
        if (D > 0) {
            pixel += xi;
            x += xi;
            D = D - 2*dy;
        }
        D = D + 2*dx;
        
        pixel += y_px_inc;
    }
}


// Bresenham's algorithm
void draw_line(Bitmap *bitmap, Point a, Point b, Color24 color) {
    if (abs(b.y - a.y) < abs(b.x - a.x)) {
        if (a.x > b.x) {
            draw_line_bresenham_low(bitmap, b, a, color);
        } else {
            draw_line_bresenham_low(bitmap, a, b, color);
        }
    } else {
        if (a.y > b.y) {
            draw_line_bresenham_high(bitmap, b, a, color);
        } else {
            draw_line_bresenham_high(bitmap, a, b, color);
        }
    }
}

void safe_draw_line(Bitmap *bitmap, Point a, Point b, Color24 color) {
    if (abs(b.y - a.y) < abs(b.x - a.x)) {
        if (a.x > b.x) {
            safe_draw_line_bresenham_low(bitmap, b, a, color);
        } else {
            safe_draw_line_bresenham_low(bitmap, a, b, color);
        }
    } else {
        if (a.y > b.y) {
            safe_draw_line_bresenham_high(bitmap, b, a, color);
        } else {
            safe_draw_line_bresenham_high(bitmap, a, b, color);
        }
    }
}


void draw_thick_line(Bitmap *bitmap, Point a, Point b, int thickness, Color24 color) {
    int stepx = 1;
    int stepy;
    
    if (thickness <= 1) {
        draw_line(bitmap, a, b, color);
        return;
    }
    
    double deltax = (double)b.x - (double)a.x;
    double deltay = (double)b.y - (double)a.y;

    if (b.x == a.x) stepy = 1;
    else stepy = ((deltay / deltax) > 0 ? -1 : 1);
    //stepy = ((b.y - a.y) > 0 ? -1 : 1);
    for (int l=0; l<thickness; l++) {
        Point new_a = {.x = a.x - (thickness*stepx)/2 + l*stepx, 
                       .y = a.y - (thickness*stepy)/2 + l*stepy};
        Point new_b = {.x = b.x - (thickness*stepx)/2 + l*stepx, 
                       .y = b.y - (thickness*stepy)/2 + l*stepy};
        draw_line(bitmap, new_a, new_b, color);
    }
    for (int l=0; l<thickness; l++) {
        Point new_a = {.x = a.x - (thickness*stepx)/2 + l*stepx, 
                       .y = a.y - (thickness*stepy)/2 + l*stepy + 1};
        Point new_b = {.x = b.x - (thickness*stepx)/2 + l*stepx, 
                       .y = b.y - (thickness*stepy)/2 + l*stepy + 1};
        draw_line(bitmap, new_a, new_b, color);
    }
}

void safe_draw_thick_line(Bitmap *bitmap, Point a, Point b, int thickness, Color24 color) {
    int stepx = 1;
    int stepy;
    
    if (thickness <= 1) {
        draw_line(bitmap, a, b, color);
        return;
    }
    
    double deltax = (double)b.x - (double)a.x;
    double deltay = (double)b.y - (double)a.y;

    if (b.x == a.x) stepy = 1;
    else stepy = ((deltay / deltax) > 0 ? -1 : 1);
    //stepy = ((b.y - a.y) > 0 ? -1 : 1);
    for (int l=0; l<thickness; l++) {
        Point new_a = {.x = a.x - (thickness*stepx)/2 + l*stepx, 
                       .y = a.y - (thickness*stepy)/2 + l*stepy};
        Point new_b = {.x = b.x - (thickness*stepx)/2 + l*stepx, 
                       .y = b.y - (thickness*stepy)/2 + l*stepy};
        safe_draw_line(bitmap, new_a, new_b, color);
    }
    for (int l=0; l<thickness; l++) {
        Point new_a = {.x = a.x - (thickness*stepx)/2 + l*stepx, 
                       .y = a.y - (thickness*stepy)/2 + l*stepy + 1};
        Point new_b = {.x = b.x - (thickness*stepx)/2 + l*stepx, 
                       .y = b.y - (thickness*stepy)/2 + l*stepy + 1};
        safe_draw_line(bitmap, new_a, new_b, color);
    }
}
