#include <libgui/libgui.h>



void draw_rectangle(Bitmap *bitmap, Rect area, Color24 line) {
    // draw horizontal lines
    int *pixel_upper = PIXELAT(bitmap, area.x, area.y);
    int *pixel_lower = PIXELAT(bitmap, area.x, area.y + area.h);
    for (int x=0; x<area.w; x++) {
        *pixel_upper = line;
        *pixel_lower = line;
        pixel_upper++;
        pixel_lower++;
    }
    
    //draw vertical lines
    int *pixel_left = PIXELAT(bitmap, area.x, area.y);
    int *pixel_right = PIXELAT(bitmap, area.x + area.w, area.y);
    int pitch = bitmap->geom.w;
    for (int y=0; y<area.h; y++) {
        *pixel_left = line;
        *pixel_right = line;
        pixel_left += pitch;
        pixel_right += pitch;
    }
}

void draw_rectangle_area(Bitmap *bitmap, Rect area, Color24 fill) {
    int *ppixel = PIXELAT(bitmap, area.x, area.y);
    int carriage_return = area.w;
    int pitch = bitmap->geom.w;
    for (int y=0; y<area.h; y++) {
        for (int x=0; x<area.w; x++) {
            *ppixel = fill;
            ppixel++;
        }
        ppixel = ppixel + pitch - carriage_return;
    }
    
}

void safe_draw_rectangle(Bitmap *bitmap, Rect area, Color24 line) {
    Rect safe_rectarea;
    
    int num_intersection = rect_intersection(area, bitmap->geom, &safe_rectarea);
    if (num_intersection == 1) {
        draw_rectangle(bitmap, safe_rectarea, line);
    }
}

void safe_draw_rectangle_area(Bitmap *bitmap, Rect area, Color24 fill) {
    Rect safe_rectarea;
    
    int num_intersection = rect_intersection(area, bitmap->geom, &safe_rectarea);
    if (num_intersection == 1) {
        draw_rectangle_area(bitmap, safe_rectarea, fill);
    }
}
