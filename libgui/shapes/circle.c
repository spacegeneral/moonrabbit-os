#include <libgui/libgui.h>


#define SAFE_PIXELASSIGN(bmp, coordx, coordy, newcolor, mem_start, mem_end) \
{ \
    int *pixeladdr = PIXELAT(bmp, coordx, coordy); \
    if (pixeladdr >= mem_start && pixeladdr < mem_end) { \
        *pixeladdr = newcolor; \
    } \
}

void draw_circle(Bitmap *bitmap, Point center, int radius, Color24 color) {
    int x = radius-1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (radius << 1);

    while (x >= y) {
        *PIXELAT(bitmap, center.x + x, center.y + y) = color;
        *PIXELAT(bitmap, center.x + y, center.y + x) = color;
        *PIXELAT(bitmap, center.x - y, center.y + x) = color;
        *PIXELAT(bitmap, center.x - x, center.y + y) = color;
        *PIXELAT(bitmap, center.x - x, center.y - y) = color;
        *PIXELAT(bitmap, center.x - y, center.y - x) = color;
        *PIXELAT(bitmap, center.x + y, center.y - x) = color;
        *PIXELAT(bitmap, center.x + x, center.y - y) = color;

        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }
        
        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (radius << 1);
        }
    }
}

void safe_draw_circle(Bitmap *bitmap, Point center, int radius, Color24 color) {
    int x = radius-1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (radius << 1);
    
    int *mem_start = bitmap->colordata;
    int *mem_end = mem_start + (bitmap->geom.w * bitmap->geom.h);

    while (x >= y) {
        SAFE_PIXELASSIGN(bitmap, center.x + x, center.y + y, color, mem_start, mem_end);
        SAFE_PIXELASSIGN(bitmap, center.x + y, center.y + x, color, mem_start, mem_end);
        SAFE_PIXELASSIGN(bitmap, center.x - y, center.y + x, color, mem_start, mem_end);
        SAFE_PIXELASSIGN(bitmap, center.x - x, center.y + y, color, mem_start, mem_end);
        SAFE_PIXELASSIGN(bitmap, center.x - x, center.y - y, color, mem_start, mem_end);
        SAFE_PIXELASSIGN(bitmap, center.x - y, center.y - x, color, mem_start, mem_end);
        SAFE_PIXELASSIGN(bitmap, center.x + y, center.y - x, color, mem_start, mem_end);
        SAFE_PIXELASSIGN(bitmap, center.x + x, center.y - y, color, mem_start, mem_end);

        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }
        
        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (radius << 1);
        }
    }
}

#define CONNECTING_LINE(bitmap, ax, ay, bx, by, color) \
{ \
    Point a = {.x = ax, .y = ay}; \
    Point b = {.x = bx, .y = by}; \
    draw_line(bitmap, a, b, color); \
}

void draw_circle_area(Bitmap *bitmap, Point center, int radius, Color24 color) {
    int x = radius-1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (radius << 1);

    while (x >= y) {
        CONNECTING_LINE(bitmap, center.x + x, center.y + y, center.x - x, center.y + y, color);
        CONNECTING_LINE(bitmap, center.x + y, center.y + x, center.x - y, center.y + x, color);
        CONNECTING_LINE(bitmap, center.x - y, center.y - x, center.x + y, center.y - x, color);
        CONNECTING_LINE(bitmap, center.x + x, center.y - y, center.x - x, center.y - y, color);
        
        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }
        
        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (radius << 1);
        }
    }
}

#define CONNECTING_LINE_SAFE(bitmap, ax, ay, bx, by, color) \
{ \
    Point a = {.x = ax, .y = ay}; \
    Point b = {.x = bx, .y = by}; \
    safe_draw_line(bitmap, a, b, color); \
}

void safe_draw_circle_area(Bitmap *bitmap, Point center, int radius, Color24 color) {
    int x = radius-1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (radius << 1);

    while (x >= y) {
        CONNECTING_LINE_SAFE(bitmap, center.x + x, center.y + y, center.x - x, center.y + y, color);
        CONNECTING_LINE_SAFE(bitmap, center.x + y, center.y + x, center.x - y, center.y + x, color);
        CONNECTING_LINE_SAFE(bitmap, center.x - y, center.y - x, center.x + y, center.y - x, color);
        CONNECTING_LINE_SAFE(bitmap, center.x + x, center.y - y, center.x - x, center.y - y, color);

        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }
        
        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (radius << 1);
        }
    }
}
