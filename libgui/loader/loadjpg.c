#include <stdlib.h>
#include <stdio.h>
#include <libgui/libgui.h>
#include <libgui/nanojpeg.h>


int load_jpeg_image_with_size(Bitmap *dest, const char *filename, int show_width, int show_height) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        return IMG_LOADER_FILE_NOT_FOUND;
    }
    
    if (show_width < 0 && show_height < 0) return IMG_LOADER_BAD_PARAMETER;
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    unsigned char *loaded_file = (unsigned char*) malloc(sizeof(unsigned char) * filesize);
    fread(loaded_file, 1, filesize, fp);
    fclose(fp);
    
    njInit();
    
    if (njDecode(loaded_file, filesize) == 0) {
        /* do stuff with image */
        unsigned int width = njGetWidth();
        unsigned int height = njGetHeight();
        unsigned int bpp = 3;
             
        unsigned char *pngbuf = njGetImage();
        
        if (show_width < 0) {
            show_width = (int)(((float)width / (float)height) * (float)show_height);
        } else if (show_height < 0) {
            show_height = (int)(((float)height / (float)width) * (float)show_width);
        }
        
        Rect geom = {
            .x = dest->geom.x,
            .y = dest->geom.y,
            .w = show_width,
            .h = show_height
        };
        
        *dest = make_bitmap(geom, 0);
        
        float scale_x = ((float)show_width) / ((float)width);
        float scale_y = ((float)show_height) / ((float)height);
        
        unsigned char *targetpix;
        for (unsigned y=0; y<show_height; y++) {
            for (unsigned x=0; x<show_width; x++) {
                unsigned from_x = (unsigned)((float)x / scale_x);
                unsigned from_y = (unsigned)((float)y / scale_y);
                
                int *currpix = PIXELAT(dest, x, y);
                
                targetpix = pngbuf + (from_y * width * bpp) + (from_x * bpp);
                
                *currpix = 0;
                *currpix |= ((int)(targetpix[0])) << 16;
                *currpix |= ((int)(targetpix[1])) << 8;
                *currpix |= ((int)(targetpix[2]));
            }
        }
        
        free(loaded_file);
        njDone();
    } else {
        free(loaded_file);
        return IMG_LOADER_JPEG_DECODE_ERR;
    }
    
    return IMG_LOADER_SUCCESS;
}


int load_jpeg_image(Bitmap *dest, const char *filename, float scale) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        return IMG_LOADER_FILE_NOT_FOUND;
    }
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    unsigned char *loaded_file = (unsigned char*) malloc(sizeof(unsigned char) * filesize);
    fread(loaded_file, 1, filesize, fp);
    fclose(fp);
    
    njInit();
    
    if (njDecode(loaded_file, filesize) == 0) {
        /* do stuff with image */
        unsigned int width = njGetWidth();
        unsigned int height = njGetHeight();
        unsigned int bpp = 3;
        
        int show_width = (int)(((float)width) * scale);
        int show_height = (int)(((float)height) * scale);
             
        unsigned char *pngbuf = njGetImage();
        
        Rect geom = {
            .x = dest->geom.x,
            .y = dest->geom.y,
            .w = show_width,
            .h = show_height
        };
        
        *dest = make_bitmap(geom, 0);
        
        unsigned char *targetpix;
        for (unsigned y=0; y<show_height; y++) {
            for (unsigned x=0; x<show_width; x++) {
                unsigned from_x = (unsigned)((float)x / scale);
                unsigned from_y = (unsigned)((float)y / scale);
                
                int *currpix = PIXELAT(dest, x, y);
                
                targetpix = pngbuf + (from_y * width * bpp) + (from_x * bpp);
                
                *currpix = 0;
                *currpix |= ((int)(targetpix[0])) << 16;
                *currpix |= ((int)(targetpix[1])) << 8;
                *currpix |= ((int)(targetpix[2]));
            }
        }
        
        free(loaded_file);
        njDone();
    } else {
        return IMG_LOADER_JPEG_DECODE_ERR;
    }
    
    return IMG_LOADER_SUCCESS;
}
