#include <stdlib.h>
#include <stdio.h>
#include <libgui/libgui.h>
#include <libgui/upng.h>


int load_png_image_with_size(Bitmap *dest, const char *filename, int show_width, int show_height) {
    upng_t* image = upng_new_from_file(filename);
    if (image == NULL) {
        return IMG_LOADER_FILE_NOT_FOUND;
    }
    
    if (show_width < 0 && show_height < 0) return IMG_LOADER_BAD_PARAMETER;
    
    upng_decode(image);
    if (upng_get_error(image) == UPNG_EOK) {
        /* do stuff with image */
        unsigned int width = upng_get_width(image);
        unsigned int height = upng_get_height(image);
        unsigned int bpp = upng_get_components(image);
             
        unsigned char *pngbuf = upng_get_buffer(image);
        
        if (show_width < 0) {
            show_width = (int)(((float)width / (float)height) * (float)show_height);
        } else if (show_height < 0) {
            show_height = (int)(((float)height / (float)width) * (float)show_width);
        }
        
        Rect geom = {
            .x = dest->geom.x,
            .y = dest->geom.y,
            .w = show_width,
            .h = show_height
        };
        
        *dest = make_bitmap(geom, 0);
        
        float scale_x = ((float)show_width) / ((float)width);
        float scale_y = ((float)show_height) / ((float)height);
        
        unsigned char *targetpix;
        for (unsigned y=0; y<show_height; y++) {
            for (unsigned x=0; x<show_width; x++) {
                unsigned from_x = (unsigned)((float)x / scale_x);
                unsigned from_y = (unsigned)((float)y / scale_y);
                
                int *currpix = PIXELAT(dest, x, y);
                
                targetpix = pngbuf + (from_y * width * bpp) + (from_x * bpp);
                
                *currpix = 0;
                *currpix |= ((int)(targetpix[0])) << 16;
                *currpix |= ((int)(targetpix[1])) << 8;
                *currpix |= ((int)(targetpix[2]));
            }
        }

        upng_free(image);
    } else {
        return IMG_LOADER_PNG_DECODE_ERR;
    }
    
    return IMG_LOADER_SUCCESS;
}


int load_png_image(Bitmap *dest, const char *filename, float scale) {
    upng_t* image = upng_new_from_file(filename);
    if (image == NULL) {
        return IMG_LOADER_FILE_NOT_FOUND;
    }
    
    upng_decode(image);
    if (upng_get_error(image) == UPNG_EOK) {
        /* do stuff with image */
        unsigned int width = upng_get_width(image);
        unsigned int height = upng_get_height(image);
        unsigned int bpp = upng_get_components(image);
        
        int show_width = (int)(((float)width) * scale);
        int show_height = (int)(((float)height) * scale);
             
        unsigned char *pngbuf = upng_get_buffer(image);
        
        Rect geom = {
            .x = dest->geom.x,
            .y = dest->geom.y,
            .w = show_width,
            .h = show_height
        };
        
        *dest = make_bitmap(geom, 0);
        
        unsigned char *targetpix;
        for (unsigned y=0; y<show_height; y++) {
            for (unsigned x=0; x<show_width; x++) {
                unsigned from_x = (unsigned)((float)x / scale);
                unsigned from_y = (unsigned)((float)y / scale);
                
                int *currpix = PIXELAT(dest, x, y);
                
                targetpix = pngbuf + (from_y * width * bpp) + (from_x * bpp);
                
                *currpix = 0;
                *currpix |= ((int)(targetpix[0])) << 16;
                *currpix |= ((int)(targetpix[1])) << 8;
                *currpix |= ((int)(targetpix[2]));
            }
        }

        upng_free(image);
    } else {
        return IMG_LOADER_PNG_DECODE_ERR;
    }
    
    return IMG_LOADER_SUCCESS;
}
