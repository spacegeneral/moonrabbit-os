#include <string.h>
#include <libgui/libgui.h>


int load_any_image_with_size(Bitmap *dest, const char *filename, int show_width, int show_height) {
    if (endswith(filename, "jpg") || endswith(filename, "jpeg")) {
        return load_jpeg_image_with_size(dest, filename, show_width, show_height);
    }
    else if (endswith(filename, "png")) {
        return load_png_image_with_size(dest, filename, show_width, show_height);
    }
    
    return IMG_LOADER_UNSUPPORTED_FORMAT;
}


int load_any_image(Bitmap *dest, const char *filename, float scale) {
    if (endswith(filename, "jpg") || endswith(filename, "jpeg")) {
        return load_jpeg_image(dest, filename, scale);
    }
    else if (endswith(filename, "png")) {
        return load_png_image(dest, filename, scale);
    }
    
    return IMG_LOADER_UNSUPPORTED_FORMAT;
}
