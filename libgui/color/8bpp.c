#include <libgui/color.h>


Color24 make_color_8bpp(int red, int green, int blue) {
    return ((red & 0xFF) << 16)  \
         | ((green & 0xFF) << 8) \
         | ((blue & 0xFF));
}
