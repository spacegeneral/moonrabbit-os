#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/serv.h>
#include <sys/proc.h>
#include <libgui/libgui.h>


bool OpenDoor_close(OpenDoor *door) {
    LibguiMsgKilldoor mesg;
    unsigned int request_id;
    ServStatus reqstatus;
    
    mesg.door_id = door->door_id;
    
    // send request
    do {
        reqstatus = service_request(KILLDOOR_SERVICE, &mesg, &request_id);
        if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) return false;
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    // await result
    do {
        reqstatus = service_poll(KILLDOOR_SERVICE, request_id);
        //TODO error conditions
        if (reqstatus != SERV_SUCCESS && reqstatus != SERV_PLEASE_WAIT) {
            return false;
        }
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    
    door->status = mesg.status;
    
    ARRAY_FOREACH(curr, door->buttons, {
        DoorButton *button = (DoorButton*) curr;
        free(button);
    });
    
    array_destroy(door->buttons);
    destroy_bitmap(&(door->face));
    
    // domus will retire the topics we were subscribed to
    
    return mesg.status == DOORS_OK;
}
