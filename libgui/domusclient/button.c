#include <libgui/libgui.h>
#include <stdlib.h>


DoorButton *DoorButton_create_advanced(OpenDoor *parent, Rect area, int border, Color24 border_color) {
    DoorButton *new_button = (DoorButton*) malloc(sizeof(DoorButton));
    
    new_button->area_rel = area;
    new_button->border = border;
    new_button->border_color = border_color;
    new_button->onclick = NULL;
    new_button->parent = parent;
    
    array_add(parent->buttons, (void*)new_button);
    
    DoorButton_redraw(new_button);
    
    return new_button;
}

DoorButton *DoorButton_create(OpenDoor *parent, Rect area) {
    return DoorButton_create_advanced(parent, area, 1, COLOR24_BUTTON);
}

void DoorButton_redraw(DoorButton *button) {
    Rect area = {
        .x = button->area_rel.x,
        .y = button->area_rel.y,
        .w = button->area_rel.w,
        .h = button->area_rel.h,
    };
    draw_rectangle(&(button->parent->face), area, button->border_color);
}

void DoorButton_destroy(DoorButton *button) {
    array_remove(button->parent, button, NULL);
    free(button);
}

