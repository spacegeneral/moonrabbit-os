#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/serv.h>
#include <sys/proc.h>
#include <libgui/libgui.h>


bool OpenDoor_update(OpenDoor *door, bool force_redraw) {
    LibguiMsgUpdatedoorFace mesg;
    char topicname[4096];
    snprintf(topicname, 4096, UPDATEDOOR_FACE_SERVICE_TEMPLATE, door->door_id);
    
    mesg.face = door->face;
    mesg.redraw = force_redraw;
    
    unsigned int request_id;
    ServStatus reqstatus;
    
    // send request
    do {
        reqstatus = service_request(topicname, &mesg, &request_id);
        if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) return false;
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    // await result
    do {
        reqstatus = service_poll(topicname, request_id);
        //TODO error conditions
        if (reqstatus != SERV_SUCCESS && reqstatus != SERV_PLEASE_WAIT) return false;
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    
    door->status = mesg.status;
    
    return (mesg.status == DOORS_OK);
} 
