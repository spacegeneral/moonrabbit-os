#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/serv.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <libgui/libgui.h>


bool open_door_advanced(OpenDoor *door, 
                        Rect area, 
                        const char *name, 
                        int border, 
                        Color24 bg_color, 
                        Color24 border_color, 
                        Color24 *transparency_colorkey, 
                        unsigned int parent_door_id, 
                        bool glued) {
    LibguiMsgMakedoor mesg;
    
    // prepare request
    door->face = make_bitmap(area, bg_color);
    mesg.face = door->face;
    
    strncpy(door->name, name, MAX_DOORNAME);
    strncpy(mesg.name, name, MAX_DOORNAME);
    
    door->border = border;
    mesg.border = border;
    
    door->has_focus = true; // just assume we have focus for now
    
    door->glued = glued;
    mesg.glued = glued;
    
    door->border_color = border_color;
    mesg.border_color = border_color;
    
    door->transparency_colorkey = transparency_colorkey;
    mesg.transparency_colorkey = transparency_colorkey;
    
    mesg.parent_door_id = parent_door_id;
    
    unsigned int request_id;
    ServStatus reqstatus;
    
    // send request
    do {
        reqstatus = service_request(MAKEDOOR_SERVICE, &mesg, &request_id);
        if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) {
            door->status = -reqstatus;
            return false;
        }
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    // await result
    do {
        reqstatus = service_poll(MAKEDOOR_SERVICE, request_id);
        if (reqstatus != SERV_SUCCESS && reqstatus != SERV_PLEASE_WAIT) {
            door->status = -reqstatus - 10;
            return false;
        }
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    
    
    door->door_id = mesg.door_id;
    door->status = mesg.status;
    door->mouse_callback = NULL;
    door->transform_callback = NULL;
    door->stdin_callback = NULL;
    door->keycode_callback = NULL;
    
    array_new(&(door->buttons));
    
    char topicname[4096];
    snprintf(topicname, 4096, DOOR_EVENT_MOUSE_TOPIC_TEMPLATE, door->door_id);
    subscribe(topicname);
    snprintf(topicname, 4096, DOOR_EVENT_STDIN_TOPIC_TEMPLATE, door->door_id);
    subscribe(topicname);
    snprintf(topicname, 4096, DOOR_EVENT_KEYCODE_TOPIC_TEMPLATE, door->door_id);
    subscribe(topicname);
    snprintf(topicname, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, door->door_id);
    subscribe(topicname);
    
    return true;
}

bool open_main_door_advanced(OpenDoor *door, Rect area, const char *name, int border, Color24 border_color, Color24 *transparency_colorkey, bool glued) {
    return open_door_advanced(door, area, name, border, COLOR24_WIN_BGROUND, border_color, transparency_colorkey, 0, glued);
}


bool open_main_door(OpenDoor *door, Rect area, const char *name) {
    return open_main_door_advanced(door, area, name, 1, COLOR24_WIN_BORDER, NULL, false);
}





