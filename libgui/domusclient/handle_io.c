#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mq.h>
#include <sys/proc.h>
#include <libgui/libgui.h>


void OpenDoor_handle_io(OpenDoor *door) {
    char topicname[4096];
    MQStatus poll_status;
    LibguiMsgMouseEvent mouse_event;
    LibguiMsgTransformEvent transform;
    char key;
    KeyEvent keycode;
    
    //transform
    snprintf(topicname, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, door->door_id);
    do {
        poll_status = poll(topicname, &transform);
        if (poll_status == MQ_SUCCESS) {
            Rect before_transformed;
            before_transformed = door->face.geom;
            door->face.geom.x = transform.geom.x;
            door->face.geom.y = transform.geom.y;
            door->has_focus = transform.has_focus;
            // handling resizing is complicated; let the callback handle this
            if (door->transform_callback != NULL) {
                door->transform_callback(&(transform.geom), &before_transformed);
            }
        }
    } while (poll_status == MQ_SUCCESS);
    
    //mouse
    snprintf(topicname, 4096, DOOR_EVENT_MOUSE_TOPIC_TEMPLATE, door->door_id);
    do {
        poll_status = poll(topicname, &mouse_event);
        if (poll_status == MQ_SUCCESS) {
            
            if (mouse_event.is_click) {
                Point clickpos = {
                    .x = mouse_event.x,
                    .y = mouse_event.y,
                };
                ARRAY_FOREACH(curr, door->buttons, {
                    DoorButton *button = (DoorButton*) curr;
                    if (rect_contains(button->area_rel, clickpos)) {
                        if (button->onclick != NULL) button->onclick();
                    }
                });
            }
            
            if (door->mouse_callback != NULL) {
                door->mouse_callback(&mouse_event);
            }
        }
    } while (poll_status == MQ_SUCCESS);
    
    //stdin
    snprintf(topicname, 4096, DOOR_EVENT_STDIN_TOPIC_TEMPLATE, door->door_id);
    do {
        poll_status = poll(topicname, &key);
        if (poll_status == MQ_SUCCESS) {
            if (door->stdin_callback != NULL) {
                door->stdin_callback(&key);
            }
        }
    } while (poll_status == MQ_SUCCESS);
    
    //keycode
    snprintf(topicname, 4096, DOOR_EVENT_KEYCODE_TOPIC_TEMPLATE, door->door_id);
    do {
        poll_status = poll(topicname, &keycode);
        if (poll_status == MQ_SUCCESS) {
            if (door->keycode_callback != NULL) {
                door->keycode_callback(&keycode);
            }
        }
    } while (poll_status == MQ_SUCCESS);
}
