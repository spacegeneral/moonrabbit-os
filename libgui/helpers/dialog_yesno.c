#include <libgui/libgui.h>
#include <sys/term.h>
#include <sys/proc.h>
#include <unicode.h>
#include <yakumo.h>
#include <stdio.h>

#include "dialog_common.h"


static bool dialog_blocking;
static bool dialog_result;


static void click_yes() {
    dialog_blocking = false;
    dialog_result = true;
}


static void click_no() {
    dialog_blocking = false;
    dialog_result = false;
}

static void keycode_callback(KeyEvent *keycode) {
    if (!keycode->is_pressed) return;
    switch (keycode->keycode) {
        case KEY_Escape:
            dialog_blocking = false;
            dialog_result = false;
            break;
    }
}


bool dialog_yesno(const char* mesg, const char *yeslabel, const char *nolabel, BitmapFont font) {
    OpenDoor dialog_door;
    DoorButton *yes_btn, *no_btn;
    SoftTerminal msgtext, yestext, notext;
    
    Rect msgbound = get_text_outline_width_limited(mesg, &font, DIALOG_MAX_INNER_WIDTH);
    TermInfo ti = getterminfo();
    
    int full_width = msgbound.w + 2*DIALOG_BORDER;
    int full_height = msgbound.h + 3*DIALOG_BORDER + DIALOG_BUTTON_HEIGHT;
    
    Rect winarea = {
        .x = max(ti.screensize.w/2 - full_width/2, 0),
        .y = max(ti.screensize.h/2 - full_height/2, 0),
        .w = full_width,
        .h = full_height,
    };
    Rect textarea = {
        .x = DIALOG_BORDER,
        .y = DIALOG_BORDER,
        .w = msgbound.w,
        .h = msgbound.h,
    };
    
    if (!open_main_door(&dialog_door, winarea, "Dialog")) return false;
    dialog_door.keycode_callback = &keycode_callback;
    
    msgtext = SoftTerminal_init(&(dialog_door.face), textarea, font, COLOR24_TEXT, 0, 0, false);
    SoftTerminal_printf(&msgtext, mesg);
    
    
    Rect yes_btn_area_rel = {
        .x = (winarea.w / 4) - (DIALOG_BUTTON_WIDTH/2),
        .y = DIALOG_BORDER*2 + msgbound.h,
        .w = DIALOG_BUTTON_WIDTH,
        .h = DIALOG_BUTTON_HEIGHT,
    };
    Rect yes_text_area = {
        .x = yes_btn_area_rel.x + DIALOG_BUTTON_MARGIN,
        .y = yes_btn_area_rel.y + DIALOG_BUTTON_MARGIN,
        .w = DIALOG_BUTTON_WIDTH - (2*DIALOG_BUTTON_MARGIN),
        .h = DIALOG_BUTTON_HEIGHT - (2*DIALOG_BUTTON_MARGIN),
    };
    yes_btn = DoorButton_create(&dialog_door, yes_btn_area_rel);
    yes_btn->onclick = &click_yes;
    yestext = SoftTerminal_init(&(dialog_door.face), yes_text_area, font, COLOR24_BUTTON, 0, 0, false);
    SoftTerminal_printf(&yestext, yeslabel);
    DoorButton_redraw(yes_btn);
    
    Rect no_btn_area_rel = {
        .x = ((winarea.w*3) / 4) - (DIALOG_BUTTON_WIDTH/2),
        .y = DIALOG_BORDER*2 + msgbound.h,
        .w = DIALOG_BUTTON_WIDTH,
        .h = DIALOG_BUTTON_HEIGHT,
    };
    Rect no_text_area = {
        .x = no_btn_area_rel.x + DIALOG_BUTTON_MARGIN,
        .y = no_btn_area_rel.y + DIALOG_BUTTON_MARGIN,
        .w = DIALOG_BUTTON_WIDTH - (2*DIALOG_BUTTON_MARGIN),
        .h = DIALOG_BUTTON_HEIGHT - (2*DIALOG_BUTTON_MARGIN),
    };
    no_btn = DoorButton_create(&dialog_door, no_btn_area_rel);
    no_btn->onclick = &click_no;
    notext = SoftTerminal_init(&(dialog_door.face), no_text_area, font, COLOR24_BUTTON, 0, 0, false);
    SoftTerminal_printf(&notext, nolabel);
    DoorButton_redraw(no_btn);
    
    OpenDoor_update(&dialog_door, true);
    
    dialog_blocking = true;
    while (dialog_blocking) {
        OpenDoor_handle_io(&dialog_door);
        yield();
    }
    
    OpenDoor_close(&dialog_door);
    
    return dialog_result;
}
