#ifndef DIALOG_COMMON_H
#define DIALOG_COMMON_H


#define DIALOG_BORDER 5
#define DIALOG_MAX_INNER_WIDTH 400
#define DIALOG_MIN_INNER_WIDTH 200
#define DIALOG_BUTTON_HEIGHT 30
#define DIALOG_BUTTON_MARGIN ((DIALOG_BUTTON_HEIGHT - CHAR_HEIGHT_UNIT) / 2)
#define DIALOG_BUTTON_WIDTH 80

#endif
