#!/bin/sh

set -e

if [[ "$1" ]]
  then
  if [[ "$1" = "floppy" ]]
    then
    . ./img.sh
    qemu-system-$(./target-triplet-to-arch.sh $HOST) -boot a -fda floppy.img -soundhw pcspk,sb16 -m 1G -netdev user,id=u1 -device rtl8139,netdev=u1 -object filter-dump,id=f1,netdev=u1,file=netdump.dat
    exit
  fi
  if [[ "$1" = "monster" ]]
    then
    . ./config.sh
    qemu-system-$(./target-triplet-to-arch.sh $HOST) -boot c -drive if=ide,media=disk,format=raw,file=frankendisk.bin -soundhw pcspk -m 256M
    exit
  fi
  if [[ "$1" = "twin2" ]]
    then
    . ./config.sh
    qemu-system-$(./target-triplet-to-arch.sh $HOST) -boot d -cdrom moonrabbit_os.iso -drive if=ide,media=disk,format=raw,file=testdisk.raw.bak -soundhw pcspk -m 256M -netdev socket,id=u1,connect=:1234 -device rtl8139,netdev=u1
    exit
  fi
  if [[ "$1" = "twin1" ]]
    then
    . ./config.sh
    qemu-system-$(./target-triplet-to-arch.sh $HOST) -boot d -cdrom moonrabbit_os.iso -drive if=ide,media=disk,format=raw,file=testdisk.raw -soundhw pcspk -m 256M -netdev socket,id=u1,listen=:1234 -device rtl8139,netdev=u1 -object filter-dump,id=f1,netdev=u1,file=netdump.dat
    exit
  fi
fi

. ./iso.sh
qemu-system-$(./target-triplet-to-arch.sh $HOST) -boot d -cdrom moonrabbit_os.iso -drive if=ide,media=disk,format=raw,file=testdisk.raw -soundhw pcspk,sb16 -m 1G -netdev user,id=u1 -device rtl8139,netdev=u1 -object filter-dump,id=f1,netdev=u1,file=netdump.dat

#qemu-system-$(./target-triplet-to-arch.sh $HOST) -boot d -cdrom moonrabbit_os.iso -drive if=ide,media=disk,format=raw,file=testdisk.raw -soundhw pcspk -m 256M  -netdev bridge,id=u1 -device rtl8139,netdev=u1 -object filter-dump,id=f1,netdev=u1,file=netdump.dat
