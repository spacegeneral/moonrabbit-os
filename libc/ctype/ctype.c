#include <ctype.h>


int isalpha(int c) {
    if (c >= 'A' && c <= 'Z') return 1;
    if (c >= 'a' && c <= 'z') return 1;
    return 0;
}

int isspace(int c) {
    if (c == ' ') return 1;
    if (c == '\t') return 1;
    if (c == '\n') return 1;
    if (c == '\v') return 1;
    if (c == '\f') return 1;
    if (c == '\r') return 1;
    
    return 0;
}

int isdigit(int c) {
    if (c >= '0' && c <= '9') return 1;
    return 0;
}

int isxdigit(int c) {
    if (c >= 'A' && c <= 'F') return 1;
    if (c >= 'a' && c <= 'f') return 1;
    if (c >= '0' && c <= '9') return 1;
    return 0;
}

int isalnum(int c) {
    return isdigit(c) || isalpha(c);
}

int isupper(int c) {
    if (c >= 'A' && c <= 'Z') return 1;
    return 0;
}

int islower(int c) {
    if (c >= 'a' && c <= 'z') return 1;
    return 0;
}

int isprint(int c) {
    if (c >= ' ' && c <= '~') return 1;
    return 0;
}

int isgraph(int c) {
    return (c != ' ') && isprint(c);
}

int iscntrl(int c) {
    if (c >= 0 && c <= 31) return 1;
    return 0;
}

int ispunct(int c) {
    return (!isalnum(c)) && isgraph(c);
}



int toupper(int c) {
    if (c >= 'a' && c <= 'z') return c + 'A' - 'a';
    return c;
}

int tolower(int c) {
    if (c >= 'A' && c <= 'Z') return c + 'a' - 'A';
    return c;
}



