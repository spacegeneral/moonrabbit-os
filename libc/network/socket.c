#include <string.h>
#include <stdlib.h>
#include <yakumo.h>

#include <sys/socket.h>
#include <sys/serv.h>
#include <sys/mq.h>
#include <sys/proc.h>
#include <sys/errno.h>



bool sockaddr_equals(const sockaddr *a, const sockaddr *b) {
    return (a->port == b->port) && ip_equals(&(a->addr), &(b->addr));
}

void format_sockaddr(char *str, const sockaddr *sock) {
    char ipstr[IP_STRING_LENGTH];
    format_ip(ipstr, &(sock->addr));
    sprintf(str, "%s:%d", ipstr, sock->port);
}


#define SS_SERV_REQ_ERRORCHECK() if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) {request->status = SERV_REQ_ERRORS + reqstatus; return;}
#define SS_SERV_POLL_ERRORCHECK() if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) {request->status = SERV_REQ_ERRORS + reqstatus; return;}


void make_sockservice_request(SocketControlServiceMessage *request) {
    unsigned int request_id;
    ServStatus reqstatus;
    // send request
    do {
        reqstatus = service_request(DEFAULT_SOCKET_CONTROL_SERVICE, request, &request_id);
        SS_SERV_REQ_ERRORCHECK();
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    // await result
    do {
        reqstatus = service_poll(DEFAULT_SOCKET_CONTROL_SERVICE, request_id);
        SS_SERV_POLL_ERRORCHECK();
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
}



int socket_get_free_number(unsigned int *number) {
    SocketControlServiceMessage request;
    request.verb = sock_get_free_number;
    make_sockservice_request(&request);
    errno = request.status;
    *number = request.msg.get_free_number.free_number;
    return request.status;
}

int socket_setup_with_number(unsigned int number, SocketType type, SocketProtocol protocol) {
    SocketControlServiceMessage request;
    request.verb = sock_setup_socket_with_number;
    request.msg.setup_socket_with_number.number = number;
    request.msg.setup_socket_with_number.type = type;
    request.msg.setup_socket_with_number.protocol = protocol;
    make_sockservice_request(&request);
    errno = request.status;
    return request.status;
}

int socket_teardown_with_number(unsigned int number) {
    SocketControlServiceMessage request;
    request.verb = sock_teardown_socket_with_number;
    request.msg.teardown_socket_with_number.number = number;
    make_sockservice_request(&request);
    errno = request.status;
    return request.status;
}

int socket_target(unsigned int number, sockaddr *address, bool use_port, bool use_addr) {
    SocketControlServiceMessage request;
    request.verb = sock_target_socket;
    request.msg.target_socket.number = number;
    request.msg.target_socket.use_port = use_port;
    request.msg.target_socket.use_addr = use_addr;
    request.msg.target_socket.sock_addr = *address;
    make_sockservice_request(&request);
    errno = request.status;
    return request.status;
}

int socket_source(unsigned int number, sockaddr *address, bool use_port, bool use_addr) {
    SocketControlServiceMessage request;
    request.verb = sock_source_socket;
    request.msg.source_socket.number = number;
    request.msg.source_socket.use_port = use_port;
    request.msg.source_socket.use_addr = use_addr;
    request.msg.source_socket.sock_addr = *address;
    make_sockservice_request(&request);
    errno = request.status;
    return request.status;
}

int socket_filter(unsigned int number, sockaddr *address, bool use_port, bool use_addr) {
    SocketControlServiceMessage request;
    request.verb = sock_filter_socket;
    request.msg.filter_socket.number = number;
    request.msg.filter_socket.use_port = use_port;
    request.msg.filter_socket.use_addr = use_addr;
    request.msg.filter_socket.sock_addr = *address;
    make_sockservice_request(&request);
    errno = request.status;
    return request.status;
}

int socket_flush(unsigned int number) {
    SocketControlServiceMessage request;
    request.verb = sock_flush_socket_explicit;
    request.msg.flush_socket_explicit.number = number;
    make_sockservice_request(&request);
    errno = request.status;
    return request.status;
}



socket_t *socket(SocketType type, SocketProtocol protocol) {
    unsigned int number;
    int errnum = socket_get_free_number(&number);
    if (errnum != 0) return NULL;
    
    socket_t *newsock = (socket_t*) malloc(sizeof(socket_t));
    newsock->number = number;
    newsock->errno = 0;
    newsock->type = type;
    newsock->protocol = protocol;
    snprintf(newsock->recv_topic, 4096, DEFAULT_SOCKET_RECV_FMT_TOPIC, number);
    snprintf(newsock->send_topic, 4096, DEFAULT_SOCKET_SEND_FMT_TOPIC, number);
    
    int errup = socket_setup_with_number(number, type, protocol);
    if (errup != 0) {
        free(newsock);
        return NULL;
    }
    
    MQStatus setup_status;
    setup_status = subscribe(newsock->recv_topic);
    if (setup_status != MQ_SUCCESS) {
        free(newsock);
        errno = MQ_ADVERT_ERRORS + setup_status;
        return NULL;
    }
    
    return newsock;
}

int socket_close(socket_t *sock) {
    unsubscribe(sock->recv_topic);
    int closerr = socket_teardown_with_number(sock->number);
    if (closerr != 0) return closerr;
    free(sock);
    return 0;
}

size_t send(socket_t *sock, void *buffer, size_t length) {
    uint8_t *buff8 = (uint8_t*) buffer;
    size_t sent = 0;
    MQStatus pub_status;
    
    if (sock->type == SOCK_DGRAM) {  // send in datagram mode
        size_t channel_width = (sock->protocol == IPPROTO_UDP ? sizeof(SockUDPDatagram) : sizeof(SockTCPDatagram));
        size_t data_chunk = channel_width - sizeof(SockANYDatagram) - 1;
        SockANYDatagram *datagram = (SockANYDatagram*) malloc(channel_width);
        
        size_t bytes_left = length;
        while (bytes_left > 0) {
            // build the datagram
            size_t bytes_sent = min(bytes_left, data_chunk);
            // ignore the "from" field when sending, the driver will take care of it
            datagram->data_length = bytes_sent;
            memcpy(&(datagram->data[0]), buff8 + sent, bytes_sent);
            
            // send the datagram
            pub_status = publish(sock->send_topic, datagram);
            if (pub_status != MQ_SUCCESS) {
                errno = sock->errno = MQ_PUB_ERRORS + pub_status;
                free(datagram);
                return sent;
            }
            
            sent += bytes_sent;
            bytes_left -= bytes_sent;
        }
        
        free(datagram);
    } else {  // send in stream mode
        size_t bytes_left = length;
        size_t bytes_written = 0;
        do {
            pub_status = publish_multi(sock->send_topic, (void*)(buff8 + bytes_written), bytes_left, &bytes_written);
            bytes_left -= bytes_written;
            sent = length - bytes_left;
            if (pub_status != MQ_SUCCESS) {
                errno = sock->errno = MQ_PUB_ERRORS + pub_status;
                return sent;
            }
            if (pub_status == MQ_CONTINUES) yield();
        } while (pub_status == MQ_CONTINUES);
    }
    errno = sock->errno = 0;
    return sent;
}

size_t sendto(socket_t *sock, void *buffer, size_t length, sockaddr *dest_addr) {
    int errtarget = socket_target(sock->number, dest_addr, true, true);
    if (errtarget != 0) {
        errno = sock->errno = errtarget;
        return 0;
    }
    return send(sock, buffer, length);
}

size_t recv(socket_t *sock, void *buffer, size_t length) {
    size_t received = 0;
    MQStatus poll_status;
    
    if (sock->type == SOCK_DGRAM) {  // receive a datagram
        size_t channel_width = (sock->protocol == IPPROTO_UDP ? sizeof(SockUDPDatagram) : sizeof(SockTCPDatagram));
        SockANYDatagram *datagram = (SockANYDatagram*) malloc(channel_width);
        
        do {
            poll_status = poll(sock->recv_topic, datagram);
            if (poll_status != MQ_SUCCESS && poll_status != MQ_NO_MESSAGES) {
                errno = sock->errno = MQ_POLL_ERRORS + poll_status;
                free(datagram);
                return received;
            }
            if (poll_status == MQ_NO_MESSAGES) yield();
        } while (poll_status == MQ_NO_MESSAGES);
        
        size_t data_length = min(length, datagram->data_length);
        
        memcpy(buffer, &(datagram->data[0]), data_length);
        
        received = data_length;
        
        free(datagram);
    } else {  // receive streaming data
        do {
            do {
                uint8_t *buff8 = (uint8_t*) buffer;
                poll_status = poll(sock->recv_topic, buff8 + received);
                if (poll_status != MQ_SUCCESS && poll_status != MQ_NO_MESSAGES) {
                    errno = sock->errno = MQ_POLL_ERRORS + poll_status;
                    return received;
                }
                if (poll_status == MQ_SUCCESS) received++;
                if (received >= length) {
                    errno = sock->errno = 0;
                    return received;
                }
            } while (poll_status == MQ_SUCCESS);
            
            if (poll_status == MQ_NO_MESSAGES && received < length) yield();
        } while (poll_status == MQ_NO_MESSAGES && received < length);
    }
    
    errno = sock->errno = 0;
    return received;
}

size_t recvfrom(socket_t *sock, void *buffer, size_t length, sockaddr *dest_addr) {
    if (sock->type == SOCK_DGRAM) {
        size_t received = 0;
        MQStatus poll_status;
        
        size_t channel_width = (sock->protocol == IPPROTO_UDP ? sizeof(SockUDPDatagram) : sizeof(SockTCPDatagram));
        SockANYDatagram *datagram = (SockANYDatagram*) malloc(channel_width);
        
        do {
            poll_status = poll(sock->recv_topic, datagram);
            if ((poll_status != MQ_SUCCESS) && (poll_status != MQ_NO_MESSAGES)) {
                errno = sock->errno = MQ_POLL_ERRORS + poll_status;
                free(datagram);
                return received;
            }
            if (poll_status == MQ_NO_MESSAGES) yield();
        } while (poll_status == MQ_NO_MESSAGES);
        
        size_t data_length = min(length, datagram->data_length);
        
        memcpy(buffer, &(datagram->data[0]), data_length);
        
        received = data_length;
        *dest_addr = datagram->from;
        
        free(datagram);
        
        return received;
    } else {  // streaming mode
        return recv(sock, buffer, length);
    }
}
