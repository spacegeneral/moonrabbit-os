#include <string.h>
#include <assert.h>
#include <sys/network.h>


void format_mac(char *str, const unsigned char *mac) {
    sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

void format_ip(char *str, const IpAddress *ip) {
    if (ip->version == 6) {
        sprintf(str, "%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d", ip->address[0], ip->address[1], ip->address[2], ip->address[3], ip->address[4], ip->address[5], ip->address[6], ip->address[7], ip->address[8], ip->address[9], ip->address[10], ip->address[11], ip->address[12], ip->address[13], ip->address[14], ip->address[15]);
    } else {
        sprintf(str, "%d.%d.%d.%d", ip->address[0], ip->address[1], ip->address[2], ip->address[3]);
    }
}


static bool parse_addr_any(char *str, unsigned char *address, int num_pieces, char sep, int base) {
    char *base_str = str;
    int pieces_done = 0;
    long piece;
    
    while (pieces_done < num_pieces) {
        char *dot = strchr(base_str, sep);
        if (dot == NULL && pieces_done < num_pieces-1) return false;
        else if (dot != NULL) {
            char backup = dot[0];
            dot[0] = '\0';
            piece = strtol(base_str, NULL, base);
            dot[0] = backup;
        }
        else {
            piece = strtol(base_str, NULL, base);
        }
        
        if (piece < 0 || piece >= 256) return false;
        
        base_str = dot + 1;
        address[pieces_done++] = piece;
    }
    
    return true;
}

bool parse_ip(char *str, IpAddress *ip) {
    if (strchr(str, '.') != NULL) {
        ip->version = 4;
        return parse_addr_any(str, ip->address, 4, '.', 10);
    } else if (strchr(str, ':') != NULL) {
        ip->version = 6;
        return parse_addr_any(str, ip->address, 16, ':', 10);
    }
    return false;
}

bool parse_mac(char *str, unsigned char *mac) {
    if (strchr(str, ':') != NULL) {
        return parse_addr_any(str, mac, 6, ':', 16);
    }
    return false;
}

bool ip_equals(const IpAddress *a, const IpAddress *b) {
    if (a->version != b->version) return false;
    
    if (a->version == 4) return (memcmp(a->address, b->address, IPv4_ADDRESS_LENGTH) == 0);
    if (a->version == 6) return (memcmp(a->address, b->address, IPv6_ADDRESS_LENGTH) == 0);
    return false;
}

bool mac_equals(const unsigned char *a, const unsigned char *b) {
    return memcmp(a, b, MAC_ADDRESS_LENGTH) == 0;
}
