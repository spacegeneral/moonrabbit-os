#include <time.h>

#if defined(__is_libk)
#include <syscall.h>
#else
#include <os.h>
#endif


unsigned int k_sleep_milli(unsigned int milliseconds) {
    SyscallMsleepParam param;
    param.milliseconds = milliseconds;
#if defined(__is_libk)
    msleep_implementation(&param, 0);
#else
    invoke_syscall(SyscallMsleep, &param);
#endif
    return param.result;
}


#if defined(__is_libc)

unsigned int u_sleep_milli(unsigned int milliseconds) {
    clock_t started = clock();
#define TIME_PASSED ((unsigned)(clock() - started))
    while (TIME_PASSED < milliseconds) yield();
    return TIME_PASSED;
#undef TIME_PASSED
}


#endif
