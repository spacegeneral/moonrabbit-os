#include <time.h>


int gettimeofday(struct timeval *restrict tp, void *restrict tzp) {
    tp->tv_sec = time(NULL);
    tp->tv_usec = 0;
    return 0;
}
