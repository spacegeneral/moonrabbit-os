#include <stdio.h>
#include <time.h>



char *asctime_r(const struct tm *timeptr, char *buf) {
    static const char wday_name[DAYSPERWEEK][4] = {
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    static const char mon_name[MONSPERYEAR][4] = {
            "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

    sprintf(buf, "%s %s %2d %02d:%02d:%02d %d",
            wday_name[timeptr->tm_wday],
            mon_name[timeptr->tm_mon],
            timeptr->tm_mday, 
            timeptr->tm_hour,
            timeptr->tm_min, 
            timeptr->tm_sec,
            YEAR_BASE + timeptr->tm_year);
    return buf;
}

char *asctime(const struct tm *timeptr) {
    static char result[26];
    return asctime_r(timeptr, result);
}


char *ctime(const time_t *timer) {
    return asctime(localtime(timer));
}

char *ctime_r(const time_t *timep, char *buf) {
    return asctime_r(localtime(timep), buf);
}




