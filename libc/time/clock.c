#include <time.h>

#if defined(__is_libk)
#include <syscall.h>
#else
#include <os.h>
#endif

clock_t clock() {
    clock_t result;
    SyscallClockParam param;
    param.result = &result;
#if defined(__is_libk)
    clock_implementation(&param, 0);
#else
    invoke_syscall(SyscallClock, &param);
#endif
    return result/* - time_started*/;
}
