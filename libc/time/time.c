#include <time.h>

#if defined(__is_libk)
#include <syscall.h>
#else
#include <os.h>
#endif

time_t time(time_t *tloc) {
    time_t result;
#if defined(__is_libk)
    gettime_implementation(&result, 0);
#else
    invoke_syscall(SyscallGettime, &result);
#endif
    if (tloc != NULL) (*tloc) = result;
    return result;
}

