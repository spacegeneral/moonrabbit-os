#include <time.h>


struct tm *localtime(const time_t *timep) {
    return gmtime(timep);
}

struct tm *localtime_r(const time_t *timep, struct tm *result) {
    return gmtime_r(timep, result);
}

