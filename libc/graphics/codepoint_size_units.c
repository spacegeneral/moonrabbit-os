#include <graphics.h>

size_t get_codepoint_width_units(BitmapCodePoint *cp) {
    switch (cp->dimension) {
        case CHAR_8x16: return 1;
        case CHAR_16x16: return 2;
        
        default: return 1;
    }
}

size_t get_codepoint_height_units(BitmapCodePoint *cp) {
    switch (cp->dimension) {
        default: return 1;
    }
} 
