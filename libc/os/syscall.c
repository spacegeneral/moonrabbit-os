#include <os.h>
#include <stdint.h>

#include <stdio.h>

void __attribute__((optimize("O0"))) invoke_syscall(unsigned int code, void *param_ptr) {
    uint32_t code_32 = code;
    uint32_t ptr_32 = (uint32_t)param_ptr;
    asm ("movl %0, %%eax;   \t\n\
          movl %1, %%ebx   \t\n\
          movl %%esp, %%ecx   \t\n\
          int $0x93"
            :
            :"r"(code_32), "r"(ptr_32)
            :"%eax", "%ebx", "%ecx"
            );
}
