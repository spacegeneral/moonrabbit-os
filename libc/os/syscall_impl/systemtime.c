#include <syscall.h>
#include <os.h>
#include <stdio.h>
#include <time.h>
#include <kernel/wallclock.h>
#include <interrupt/timing.h>


void gettime_implementation(void *param_ptr, unsigned int ident) {
    time_t *result = (time_t*) param_ptr;
    
    get_system_time(result);
}

void clock_implementation(void *param_ptr, unsigned int ident) {
    SyscallClockParam *param = (SyscallClockParam*) param_ptr;
    
    *(param->result) = (clock_t) system_clock_millisec();
}

void msleep_implementation(void *param_ptr, unsigned int ident) {
    SyscallMsleepParam *param = (SyscallMsleepParam*) param_ptr;
    
    wait_milliseconds(param->milliseconds);
    param->result = 0;
}
