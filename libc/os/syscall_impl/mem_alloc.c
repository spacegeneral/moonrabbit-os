#include <stdio.h>
#include <kernel/core.h>
#include <syscall.h>
#include <os.h>
#include <mm/mm.h>
#include <mm/paging.h>
#include <task/task.h>
#include <security/identify.h>
#include <sys/errno.h>


void malloc_implementation(void *param_ptr, unsigned int ident) {
    SyscallMallocParam *param = (SyscallMallocParam*) param_ptr;
    LOCK(mem_allocator_lock);
    size_t num_pages = fit_into_pages(param->size);
#ifdef MALLOC_GUARD
    size_t extra_pages = (num_pages >= MALLOC_GUARD_THRESHOLD ? 2 : 0);
    void *memarea = allocate_pages(num_pages + extra_pages, 0);
    uint8_t* area_bytes = (uint8_t*)memarea;
#else
    void *memarea = allocate_pages(num_pages, 0);
#endif
    if (memarea != NULL) {
#ifdef MALLOC_GUARD
        if (extra_pages > 0) {
            cascade_set_guard_pages((void*)area_bytes, 1);
            cascade_set_guard_pages((void*)(area_bytes + (num_pages+1)*PAGE_SIZE), 1);
        }
#endif
        transfer_mem_ownership(memarea, ident);
        set_memarea_tracking_data(memarea, param->filename, param->linenum);
    } else {
        if (ident == 0) {
            printf("Kernelspace malloc(%lu) failure at %s:%u\n", param->size, param->filename, param->linenum);
            panic();
        } else {
            #ifdef ADVANCED_DEBUG
                debug_printf("Userland malloc(%lu) failure at %s:%u\r\n", param->size, param->filename, param->linenum);
            #endif
        }
    }
    UNLOCK(mem_allocator_lock);
#ifdef MALLOC_GUARD
    if (extra_pages > 0) {
        param->result = (void*)(area_bytes + PAGE_SIZE);
    } else {
        param->result = memarea;
    }
#else
    param->result = memarea;
#endif
}


static void mem_exception(void *memarea, unsigned int ident, char *action_desc) {
    if (ident == 0) {
        printf("Kernel tried to %s invalid memarea %p\n", action_desc, memarea);
        panic();
    } else {
        printf("User tried to %s invalid memarea %p\n", action_desc, memarea);
        Task *troubled_task = get_current_task();
        IdentifiedAgent *troubled_agent = get_agent(troubled_task->owner_id);
        printf("The error occurred inside task %d,\nOwned by %s the %s %s\n",
            troubled_task->task_id,
            troubled_agent->human_name,
            phisical_domain_class_name[troubled_agent->phys_race_attrib],
            data_domain_class_name[troubled_agent->data_class_attrib]
        );
        unsigned int next_task = troubled_task->parent_task;
        printf("The task will be terminated, and control transferred to its parent %d\n", next_task);
        
        if (troubled_task->return_code_here != NULL)
            *(troubled_task->return_code_here) = TASK_KILLED_BECAUSE_EXCEPTION;
        
        purge_task(troubled_task->task_id);
        
        iswitch_task(next_task);
    }
}


void free_implementation(void *param_ptr, unsigned int ident) {
    LOCK(mem_allocator_lock);
    size_t mem_size;
    bool status;
    void* memarea = find_address_owner(param_ptr);
#ifdef MALLOC_GUARD
    status = get_memarea_length(memarea, &mem_size);
    if (!status) {
        UNLOCK(mem_allocator_lock);
        mem_exception(memarea, ident, "free");
        return;
    }
    if (mem_size / PAGE_SIZE >= MALLOC_GUARD_THRESHOLD) cascade_clear_guard_pages(memarea, mem_size / PAGE_SIZE);
#endif
    status = free_memarea(memarea);
    transfer_mem_ownership(memarea, 0);
    UNLOCK(mem_allocator_lock);
    if (!status) {
        mem_exception(memarea, ident, "free");
    }
}


void mlength_implementation(void *param_ptr, unsigned int ident) {
    SyscallMlengthParam *param = (SyscallMlengthParam*) param_ptr;
    LOCK(mem_allocator_lock);
    size_t mem_size;
    void* memarea = find_address_owner(param->memarea);
    bool status = get_memarea_length(memarea, &mem_size);
    UNLOCK(mem_allocator_lock);
    if (!status) {
        mem_exception(memarea, ident, "get the length of");
    }
#ifdef MALLOC_GUARD
    size_t extra_pages = ((mem_size / PAGE_SIZE) >= MALLOC_GUARD_THRESHOLD ? 2 : 0);
    param->result = mem_size - (extra_pages * PAGE_SIZE);
#else
    param->result = mem_size;
#endif
}

void gift_memarea_implementation(void *param_ptr, unsigned int ident) {
    SyscallGiftMemareaParam *param = (SyscallGiftMemareaParam*) param_ptr;
    LOCK(mem_allocator_lock);
    void* memarea = find_address_owner(param->memarea);
    unsigned int previous_owner_uid = get_memarea_tag(memarea);
    
    if ((previous_owner_uid != ident) && (ident != 0)) {
        param->result = SECURITY_DENIED;
        
    } else {
        
        if (param->new_owner_id == 0) {  // giving a memarea to kernel frees it
            bool status = free_memarea(param_ptr);
            transfer_mem_ownership(param_ptr, 0);
            UNLOCK(mem_allocator_lock);
            param->result = 0;
            if (!status) {
                param->result = INTERNAL_MM_ERROR;
                mem_exception(param_ptr, ident, "free");
            }
        }
        if (!transfer_mem_ownership(memarea, param->new_owner_id)) {
            UNLOCK(mem_allocator_lock);
            param->result = INTERNAL_MM_ERROR;
            mem_exception(param_ptr, ident, "gift_memarea");
        } else {
            UNLOCK(mem_allocator_lock);
            param->result = 0;
        }
    }
}
