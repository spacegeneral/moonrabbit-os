#include <syscall.h>
#include <os.h>
#include <errno.h>
#include <stdlib.h>
#include <kernel/tty.h>
#include <security/defaults.h>
#include <security/authorize.h>
#include <security/identify.h>


void direct_video_command_implementation(void *param_ptr, unsigned int ident) {
    DirectVideoCommandParam *param = (DirectVideoCommandParam*) param_ptr;
    
    if (!(authorize_agent_id(DEFAULT_DIRECTVIDEO_RACE, DEFAULT_DIRECTVIDEO_CLASS, 
                             ident))) {
        param->status = SECURITY_DENIED;
        return;
    }
    
    param->status = CALL_IMPL(video, terminal, direct_video_command, param->verb, param->argument, param->result, ident);
}
