#include <syscall.h>
#include <os.h>
#include <stdio.h>
#include <kernel/init.h>
#include <kernel/log.h>
#include <builtins.h>
#include <data/vfs.h>
#include <data/virtual_file.h>
#include <data/fs/internalfs.h>
#include <data/path.h>
#include <mm/mm.h>
#include <security/identify.h>
#include <sys/errno.h>



static unsigned int mount_fs_id_gen = 0;


unsigned int generate_fs_id(char *fsname) {
    char path[4096];
    
    unsigned int fsid = 0;
    
    while (true) {
        snprintf(path, 4096, "%s,%s%d", OS_MOUNT_PATH, fsname, fsid);
        int exists;
        internalfs_composites.file_exists(get_agent(0), path, &exists);
        if (!exists) break;
        
        fsid++;
    }
    
    return fsid;
}



void get_mount_info_implementation(void *param_ptr, unsigned int ident) {
    SyscallGetMountInfoParam *param = (SyscallGetMountInfoParam*) param_ptr;
    
    FileDescriptor *mount;
    char explicit_path[4096];
    resolve_mount_explicit_path(param->pathname, &mount, explicit_path);
    
    if (mount != NULL) {
        if (param->explicit != NULL) {
            // rewrite the pathname to accomodate for symbolic links
            char *mountpath;
            afull_file_path(mount, &mountpath);
            sprintf(param->explicit, "%s%s", mountpath, param->pathname + strlen(explicit_path));
            free(mountpath);
            //printf("explicit path: %s\n", param->explicit);
        }
        snprintf(param->additional, MAX_FS_DRIVER_NAME_LENGTH, "%s", mount->driver);
    } else {
        if (param->explicit != NULL) {
            sprintf(param->explicit, param->pathname);
            //printf("explicit path: %s\n", param->explicit);
        }
        snprintf(param->additional, MAX_FS_DRIVER_NAME_LENGTH, "%s", ROOTFS_DRIVER_NAME);
    }
    
    param->result = 0;
}

void mount_implementation(void *param_ptr, unsigned int ident) {
    SyscallMountParam *param = (SyscallMountParam*) param_ptr;
    
    IdentifiedAgent *agent = get_agent(ident);
    
    FileDescriptor* mountfile;
    FileDescriptor* dest_folder;
    
    char name[MAX_FILENAME_LENGTH];
    snprintf(name, MAX_FILENAME_LENGTH, "%s%d", param->fsname, generate_fs_id(param->fsname));
    
    // get the descriptor of the folder containing the mount points, fail if
    // we lack the proper permissions
    resolve_path(OS_MOUNT_PATH, &dest_folder);
    if (!authorize_file(dest_folder, agent->phys_race_attrib, agent->data_class_attrib)) {
        param->result = SECURITY_DENIED;
        return;
    }
    
    FileMetaData meta = {
        .race_attrib = agent->phys_race_attrib,
        .class_attrib = agent->data_class_attrib,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    
    // create a new file and overwrite its driver string
    virtual_fs_primitives.create_file(&mountfile, name, meta);
    strncpy(mountfile->driver, name, MAX_FS_DRIVER_NAME_LENGTH);
    mountfile->access_strategy = MOUNT_ALIENFS;
    
    // insert the file into the directory
    if (!file_method(dest_folder)->insert_into_directory(dest_folder, mountfile)) {
        param->result = INTERNAL_FS_ERROR;
        virtualfs_remove_file(&mountfile);
        return;
    }
    snprintf(param->mountpoint, 4096, "%s,%s", OS_MOUNT_PATH, name);
    snprintf(param->drivertopic, MAX_FS_DRIVER_NAME_LENGTH, "%s", name);
    
    param->result = 0;
}

void unmount_implementation(void *param_ptr, unsigned int ident) {
    SyscallUnMountParam *param = (SyscallUnMountParam*) param_ptr;
    
    IdentifiedAgent *agent = get_agent(ident);
    
    FileDescriptor *targetfile;
    
    if (!resolve_path(param->mountpoint, &targetfile)) {
        param->result = INTERNAL_FS_ERROR;
        return;
    }
    
    if (targetfile->access_strategy != MOUNT_ALIENFS) {
        param->result = NOT_MOUNTED;
        return;
    }
    
    snprintf(param->drivertopic, MAX_FS_DRIVER_NAME_LENGTH, "%s", targetfile->driver);
    
    internalfs_composites.remove(agent, param->mountpoint, &(param->result));
}

void scan_media_implementation(void *param_ptr, unsigned int ident) {
    int *result = (int*) param_ptr;
    DirEnt *entries = NULL;
    size_t numentries;
    
    *result = -1;
    
    char fullname[2048];
    if (listdir(OS_PART_PATH, &entries, &numentries)) return;

    for (size_t e=0; e<numentries; e++) {
        snprintf(fullname, 2048, "%s,%s", OS_PART_PATH, entries[e].name);
        if (remove(fullname) != 0) {
            klog("scan_media: cannot remove %s\n", fullname);
        }
    }
    free(entries);
    if (!scan_partitions(true)) return;
    
    (*result) = 0;
}


