#include <syscall.h>
#include <os.h>
#include <stdio.h>
#include <ipc/interface.h>
#include <sys/serv.h>
#include <task/task.h>
#include <security/authorize.h>


void advertise_service_implementation(void *param_ptr, unsigned int ident) {
    SyscallAdvertiseServiceParam *param = (SyscallAdvertiseServiceParam*) param_ptr;
    
    if (!authorize_agent_id(param->phys_race_attrib, param->data_class_attrib, ident)) {
        param->result = SERV_AUTH_PROBLEM;
        return;
    }
    
    param->result = CALL_IMPL(ipc, service, advertise_service, 
                                        param->topic_name,
                                        param->frame_length,
                                        param->phys_race_attrib, 
                                        param->data_class_attrib);
}

void retire_service_implementation(void *param_ptr, unsigned int ident) {
    SyscallRetireServiceParam *param = (SyscallRetireServiceParam*) param_ptr;
    
    param->result = CALL_IMPL(ipc, service, retire_service, 
                                            param->topic_name,
                                            ident);
}

void accept_service_implementation(void *param_ptr, unsigned int ident) {
    SyscallAcceptServiceParam *param = (SyscallAcceptServiceParam*) param_ptr;
    unsigned int task_id = get_current_task()->task_id;
    
    param->result = CALL_IMPL(ipc, service, accept, 
                                            param->topic_name,
                                            param->request,
                                            param->requestor_task_id,
                                            param->request_id,
                                            task_id);
}

void complete_service_implementation(void *param_ptr, unsigned int ident) {
    SyscallMessageServiceIDParam *param = (SyscallMessageServiceIDParam*) param_ptr;
    
    param->result = CALL_IMPL(ipc, service, server_complete, 
                                            param->topic_name,
                                            param->request_id,
                                            ident);
}

void poll_service_implementation(void *param_ptr, unsigned int ident) {
    SyscallMessageServiceIDParam *param = (SyscallMessageServiceIDParam*) param_ptr;
    unsigned int task_id = get_current_task()->task_id;
    
    param->result = CALL_IMPL(ipc, service, poll_response, 
                                            param->topic_name,
                                            param->request_id,
                                            task_id);
}

void request_service_implementation(void *param_ptr, unsigned int ident) {
    SyscallRequestServiceParam *param = (SyscallRequestServiceParam*) param_ptr;
    unsigned int task_id = get_current_task()->task_id;
    //printf("request service (%s) ", param->topic_name);
    param->result = CALL_IMPL(ipc, service, request, 
                                            param->topic_name,
                                            param->message,
                                            param->request_id,
                                            task_id);
    //printf("result: %d\n", param->result);
}

