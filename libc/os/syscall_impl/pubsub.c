#include <syscall.h>
#include <os.h>
#include <stdio.h>
#include <ipc/interface.h>
#include <sys/mq.h>
#include <task/task.h>
#include <security/authorize.h>


void advertise_channel_implementation(void *param_ptr, unsigned int ident) {
    SyscallAdvertiseChannelParam *param = (SyscallAdvertiseChannelParam*) param_ptr;
    
    if (!authorize_agent_id(param->phys_race_attrib, param->data_class_attrib, ident)) {
        param->result = MQ_AUTH_PROBLEM;
        return;
    }
    
    param->result = CALL_IMPL(ipc, pubsub, advertise_channel, 
                                        param->topic_name,
                                        param->max_queue_length,
                                        param->message_length,
                                        param->phys_race_attrib, 
                                        param->data_class_attrib, 
                                        param->policy);
}

void retire_channel_implementation(void *param_ptr, unsigned int ident) {
    SyscallTopicParam *param = (SyscallTopicParam*) param_ptr;
    
    param->result = CALL_IMPL(ipc, pubsub, retire_channel, 
                                        param->topic_name,
                                        ident);
}

void subscribe_implementation(void *param_ptr, unsigned int ident) {
    SyscallTopicParam *param = (SyscallTopicParam*) param_ptr;
    // when this is invoked by libk, use INVALID_TASK_ID as a "dummy task id" assigned to the kernel.
    // the ipc.pubsub system only used task ids to identify subscribers, therefore passing
    // INVALID_TASK_ID is safe in this particular case.
    unsigned int task_id = (ident != 0 ? get_current_task()->task_id : INVALID_TASK_ID);
    
    param->result = CALL_IMPL(ipc, pubsub, subscribe, 
                                        param->topic_name,
                                        task_id);
}

void unsubscribe_implementation(void *param_ptr, unsigned int ident) {
    SyscallTopicParam *param = (SyscallTopicParam*) param_ptr;
    unsigned int task_id = (ident != 0 ? get_current_task()->task_id : INVALID_TASK_ID);
    
    param->result = CALL_IMPL(ipc, pubsub, unsubscribe, 
                                        param->topic_name,
                                        task_id);
}

void publish_implementation(void *param_ptr, unsigned int ident) {
    SyscallPubPollParam *param = (SyscallPubPollParam*) param_ptr;
    
    param->result = CALL_IMPL(ipc, pubsub, publish, 
                                        param->topic_name,
                                        param->message,
                                        ident);
}

void publish_multi_implementation(void *param_ptr, unsigned int ident) {
    SyscallPublishMultiParam *param = (SyscallPublishMultiParam*) param_ptr;
    
    param->result = CALL_IMPL(ipc, pubsub, publish_multi, 
                                        param->topic_name,
                                        param->message,
                                        ident,
                                        param->num_messages,
                                        param->num_written);
}

void poll_implementation(void *param_ptr, unsigned int ident) {
    SyscallPubPollParam *param = (SyscallPubPollParam*) param_ptr;
    unsigned int task_id = (ident != 0 ? get_current_task()->task_id : INVALID_TASK_ID);
    
    param->result = CALL_IMPL(ipc, pubsub, poll, 
                                        param->topic_name,
                                        task_id,
                                        param->message);
}

void channel_stats_implementation(void *param_ptr, unsigned int ident) {
    SyscallChannelStatsParam *param = (SyscallChannelStatsParam*) param_ptr;
    
    param->result = CALL_IMPL(ipc, pubsub, stats, 
                                        param->topic_name,
                                        param->stats);
}
