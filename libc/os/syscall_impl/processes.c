#include <syscall.h>
#include <os.h>
#include <stdio.h>
#include <string.h>
#include <sys/proc.h>
#include <sys/errno.h>
#include <stdlib.h>
#include <builtins.h>
#include <mm/paging.h>
#include <mm/mm.h>
#include <security/identify.h>
#include <security/authorize.h>
#include <security/hardware.h>
#include <data/path.h>
#include <task/task.h>
#include <task/sched.h>
#include <kernel/core.h>


int prepare_to_launch_task_file(char *program_filename, 
                                char **copy_env,
                                PhisicalDomainClass phys_race_attrib,
                                DataDomainClass data_class_attrib,
                                IdentifiedAgent *requester,
                                unsigned int *new_taskid,
                                int *return_code_here) {
    Task *current_task = get_current_task();
    // check if the syscall issuer is allowed to access the requested security level
    if (!(authorize_agent_id(phys_race_attrib, data_class_attrib, 
                             requester->uid))) {
        return LAUNCH_NOT_AUTHORIZED;
    }
    // load the elf code in memory
    unsigned char* rawelf = userland_loader(program_filename);
    if (rawelf == NULL) {
        return LAUNCHELF_ERR_FOPEN;
    }
    
    char *progname = strrchr(program_filename, DIR_SEPARATOR);
    if (progname == NULL) progname = program_filename;
    else progname++;
    
    HalfElfLoaderReturn loaded_elf = half_elf_loader(program_filename, progname);
    if (loaded_elf.status != LAUNCHELF_OK) {
        free(rawelf);
        return loaded_elf.status;
    }
    free(rawelf);
    
    // spawn a new agent
    unsigned int new_process_ident = add_new_agent(phys_race_attrib, 
                                                   data_class_attrib, 
                                                   "superuser", 
                                                   true);
    // create (or reuse) a page management object for the new agent
    PageManagementObject *process_page_mgr = new_page_management_entry(phys_race_attrib, data_class_attrib);
    
    snprintf(get_agent(new_process_ident)->human_name, MAX_AGENT_NAME, "superuser-%d-%s", new_process_ident, (progname != NULL ? progname+1 : program_filename));
    
    // assigne the elf memory to the new agent
    transfer_mem_ownership(loaded_elf.memarea, new_process_ident);
    
    // create a stack for the new process
    void *new_stack = malloc(PROC_DEFAULT_STACKSIZE);
    ucpuint_t new_stack_top = ((ucpuint_t)new_stack) + PROC_DEFAULT_STACKSIZE - 16;
    transfer_mem_ownership(new_stack, new_process_ident);
    
    // create an environment block for the new process
    char **new_env = (char**) malloc(sizeof(char*));
    new_env[0] = NULL;
    clone_env(&new_env, copy_env);
    transfer_mem_ownership(new_env, new_process_ident);
    for (size_t e=0; new_env[e] != NULL; e++) {
        transfer_mem_ownership(new_env[e], new_process_ident);
    }
    
    // create a new task
    Task *newtask = new_task(
        loaded_elf.entrypoint, 
        loaded_elf.memarea,
        new_stack_top, 
        new_stack,
        (ucpuint_t)new_env,
        get_cr3_from_page_mgr_object(process_page_mgr), 
        0, 
        new_process_ident,
        current_task->task_id,
        return_code_here,
        progname,
        loaded_elf.debug_info
    );
    
    char stackname[256];
    snprintf(stackname, 256, "STACK-main");
    TaskDebuginfo_append_mem_section(newtask->debug_info, stackname, (uintptr_t)new_stack, PROC_DEFAULT_STACKSIZE);
    
    (*new_taskid) = newtask->task_id;
    return 0;
}


int prepare_to_launch_task_rawelf(unsigned char* rawelf, 
                                  char *progname,
                                  char **copy_env,
                                  PhisicalDomainClass phys_race_attrib,
                                  DataDomainClass data_class_attrib,
                                  IdentifiedAgent *requester,
                                  unsigned int *new_taskid,
                                  int *return_code_here) {
    Task *current_task = get_current_task();
    // check if the syscall issuer is allowed to access the requested security level
    if (!(authorize_agent_id(phys_race_attrib, data_class_attrib, 
                             requester->uid))) {
        return LAUNCH_NOT_AUTHORIZED;
    }
    
    HalfElfLoaderReturn loaded_elf = half_elf_loader(rawelf, progname);
    if (loaded_elf.status != LAUNCHELF_OK) {
        free(rawelf);
        return loaded_elf.status;
    }
    
    // spawn a new agent
    unsigned int new_process_ident = add_new_agent(phys_race_attrib, 
                                                   data_class_attrib, 
                                                   "user", 
                                                   true);
    // create (or reuse) a page management object for the new agent
    PageManagementObject *process_page_mgr = new_page_management_entry(phys_race_attrib, data_class_attrib);
    
    snprintf(get_agent(new_process_ident)->human_name, MAX_AGENT_NAME, "user-%d", new_process_ident);
    
    // assigne the elf memory to the new agent
    transfer_mem_ownership(loaded_elf.memarea, new_process_ident);
    
    // create a stack for the new process
    void *new_stack = malloc(PROC_DEFAULT_STACKSIZE);
    ucpuint_t new_stack_top = ((ucpuint_t)new_stack) + PROC_DEFAULT_STACKSIZE - 16;
    transfer_mem_ownership(new_stack, new_process_ident);
    
    // create an environment block for the new process
    char **new_env = (char**) malloc(sizeof(char*));
    new_env[0] = NULL;
    clone_env(&new_env, copy_env);
    transfer_mem_ownership(new_env, new_process_ident);
    for (size_t e=0; new_env[e] != NULL; e++) {
        transfer_mem_ownership(new_env[e], new_process_ident);
    }
    
    // create a new task
    Task *newtask = new_task(
        loaded_elf.entrypoint, 
        loaded_elf.memarea,
        new_stack_top, 
        new_stack,
        (ucpuint_t)new_env,
        get_cr3_from_page_mgr_object(process_page_mgr), 
        0, 
        new_process_ident,
        current_task->task_id,
        return_code_here,
        progname,
        loaded_elf.debug_info
    );
    
    char stackname[256];
    snprintf(stackname, 256, "STACK-main");
    TaskDebuginfo_append_mem_section(newtask->debug_info, stackname, (uintptr_t)new_stack, PROC_DEFAULT_STACKSIZE);
    
    (*new_taskid) = newtask->task_id;
    free(rawelf);
    return 0;
}

int hotswap_task_executable(unsigned char* ready_elf, 
                            uintptr_t entrypoint,
                            TaskDebuginfo *user_dbginfo,
                            char *progname,
                            char **copy_env,
                            IdentifiedAgent *requester) {
    Task *current_task = get_current_task();
    
    // the "new" task will have these parameters copied from the former incarnation
    unsigned int process_ident = requester->uid;
    int required_task_id = current_task->task_id;
    int required_parent_id = current_task->parent_task;
    int *required_return_code_here = current_task->return_code_here;
    
    // clone the user-provided debug info, before they are destroyed by reset_task()
    TaskDebuginfo *kernel_dbginfo;
    TaskDebuginfo_clone(&kernel_dbginfo, user_dbginfo);
    
    // create a new (copied) environment. Make sure to copy it before it's destroyed by reset_task()
    char **new_env = (char**) malloc(sizeof(char*));
    new_env[0] = NULL;
    clone_env(&new_env, copy_env);
    
    transfer_mem_ownership(ready_elf, 0);  // save the loaded code from destruction
    
    // free resources used by the former task
    reset_task(required_task_id);
    
    transfer_mem_ownership(ready_elf, process_ident);  // assign the elf memory to the "new" agent 
    
    // assign the new environment to the agent
    transfer_mem_ownership(new_env, process_ident);
    for (size_t e=0; new_env[e] != NULL; e++) {
        transfer_mem_ownership(new_env[e], process_ident);
    }
    
    // create a new stack
    void *new_stack = malloc(PROC_DEFAULT_STACKSIZE);
    ucpuint_t new_stack_top = ((ucpuint_t)new_stack) + PROC_DEFAULT_STACKSIZE - 16;
    transfer_mem_ownership(new_stack, process_ident);
    
    // create a new task with the same id, parent, owner agent and return code pointer
    Task *newtask = new_task_with_id(
        required_task_id,
        entrypoint, 
        ready_elf,
        new_stack_top, 
        new_stack,
        (ucpuint_t)new_env,
        get_cr3_from_page_mgr_object(get_page_management_entry_owned_by(requester)), 
        0, 
        process_ident,
        required_parent_id,
        required_return_code_here,
        progname,
        kernel_dbginfo
    );
    
    char stackname[256];
    snprintf(stackname, 256, "STACK-main");
    TaskDebuginfo_append_mem_section(newtask->debug_info, stackname, (uintptr_t)new_stack, PROC_DEFAULT_STACKSIZE);
    
    return 0;
}



int make_subtask(ucpuint_t entrypoint,
                 ucpuint_t existing_env,
                 IdentifiedAgent *requester,
                 unsigned int *new_subtaskid,
                 int *return_code_here) {
    Task *current_task = get_current_task();

    unsigned int current_task_ident = requester->uid;

    // create a stack for the new thread
    void *new_stack = malloc(PROC_DEFAULT_STACKSIZE);
    ucpuint_t new_stack_top = ((ucpuint_t)new_stack) + PROC_DEFAULT_STACKSIZE - 16;
    transfer_mem_ownership(new_stack, current_task_ident);
    
    char clonename[MAX_TASKNAME_LENGTH];
    snprintf(clonename, MAX_TASKNAME_LENGTH, "%s-clone", current_task->name);
    
    // create a new task
    Task *newtask = new_task(
        entrypoint, 
        current_task->main_memarea,
        new_stack_top, 
        new_stack,
        existing_env,
        get_cr3_from_page_mgr_object(get_page_management_entry_owned_by(requester)), 
        0, 
        current_task_ident,
        current_task->task_id,
        return_code_here,
        clonename,
        current_task->debug_info
    );
    
    char stackname[256];
    snprintf(stackname, 256, "STACK-thread-%d", newtask->task_id);
    TaskDebuginfo_append_mem_section(newtask->debug_info, stackname, (uintptr_t)new_stack, PROC_DEFAULT_STACKSIZE);
    
    (*new_subtaskid) = newtask->task_id;
    return 0;
}


void prelaunch_kernel_implementation(void *param_ptr, unsigned int ident) {
    SyscallPrelaunchKernelParam *param = (SyscallPrelaunchKernelParam*) param_ptr;
    
    IdentifiedAgent *requester = get_agent(ident);
    
    unsigned int newtask_id;
    param->result = prepare_to_launch_task_file(
        param->filename,
        param->copy_env,
        param->phys_race_attrib,
        param->data_class_attrib,
        requester,
        &newtask_id,
        param->return_code_here
    );
    if (param->return_code_here != NULL)
        *(param->return_code_here) = READY_TO_RUN;
    
    if (param->result == 0)
        *(param->new_taskid) = newtask_id;
}

void prelaunch_user_implementation(void *param_ptr, unsigned int ident) {
    SyscallPrelaunchUserParam *param = (SyscallPrelaunchUserParam*) param_ptr;
    
    IdentifiedAgent *requester = get_agent(ident);
    
    unsigned int newtask_id;
    param->result = prepare_to_launch_task_rawelf(
        param->raw_elf,
        param->program_name,
        param->copy_env,
        param->phys_race_attrib,
        param->data_class_attrib,
        requester,
        &newtask_id,
        param->return_code_here
    );
    if (param->return_code_here != NULL)
        *(param->return_code_here) = READY_TO_RUN;
    
    if (param->result == 0)
        *(param->new_taskid) = newtask_id;
}

void henshin_implementation(void *param_ptr, unsigned int ident) {
    SyscallHenshinParam *param = (SyscallHenshinParam*) param_ptr;
    
    IdentifiedAgent *requester = get_agent(ident);
    
    hotswap_task_executable(param->ready_elf, 
                            param->entrypoint,
                            param->dbginfo,
                            param->program_name,
                            param->copy_env,
                            requester);
    
    // jump explicitly to the current task, forgetting
    // the saved context
    iswitch_task_forget_current(get_current_task()->task_id);
    __builtin_unreachable();
}

void clone_implementation(void *param_ptr, unsigned int ident) {
    SyscallCloneParam *param = (SyscallCloneParam*) param_ptr;
    
    IdentifiedAgent *requester = get_agent(ident);
    
    unsigned int newtask_id;
    param->result = make_subtask(
        param->entrypoint,
        (ucpuint_t)param->copy_env,
        requester,
        &newtask_id,
        NULL
    );
    if (param->result == 0)
        *(param->new_taskid) = newtask_id;
}

void taskswitch_implementation(void *param_ptr, unsigned int ident) {
    SyscallTaskSwitchParam *param = (SyscallTaskSwitchParam*) param_ptr;
    (void)ident;
    Task *current_task = get_current_task();
    
    if (task_exists(param->newtask)) {
        
        if (current_task->return_code_here != NULL)
            *(current_task->return_code_here) = NOT_DONE_YET;
        
        param->result = 0;
        iswitch_task(param->newtask);
        __builtin_unreachable();
    } else {
        param->result = TASK_DOES_NOT_EXIST;
    }
}


void expirecc_implementation(void *param_ptr, unsigned int ident) {
    SyscallExpireCCParam *param = (SyscallExpireCCParam*) param_ptr;
    (void)ident;
    Task *current_task = get_current_task();
    
    enable_task_self_preempt(current_task, param->resume, param->expire);
}


void continuation_implementation(void *param_ptr, unsigned int ident) {
    (void)param_ptr;
    (void)ident;
    Task *current_task = get_current_task();
    
    iswitch_task_continuation(current_task->task_id);
}


void yield_implementation(void *param_ptr, unsigned int ident) {
    int *returncode = (int*) param_ptr;
    (void)ident;
    Task *current_task = get_current_task();
    
    if (task_exists(current_task->parent_task)) {
        
        if (current_task->return_code_here != NULL)
            *(current_task->return_code_here) = NOT_DONE_YET;
        
        (*returncode) = 0;
        iswitch_task(current_task->parent_task);
        __builtin_unreachable();
    } else {
        (*returncode) = TASK_DOES_NOT_EXIST;
    }
}

void ident_implementation(void *param_ptr, unsigned int ident) {
    SyscallIdentParam *param = (SyscallIdentParam*) param_ptr;
    Task *current = get_current_task();
    IdentifiedAgent *requester = get_agent(ident);
    param->result.request_ident = ident;
    param->result.taskid = current->task_id;
    param->result.phys_race_attrib = requester->phys_race_attrib;
    param->result.data_class_attrib = requester->data_class_attrib;
    param->result.is_user = requester->is_user;
    param->result.owner_ident = current->owner_id;
}


void exit_implementation(void *param_ptr, unsigned int ident) {
    int code = (int) param_ptr;
    Task *current = get_current_task();
    
    if (current->task_id == 0) {
        printf("Returned from init (code: %d)\n", code);
        panic();
    }

    unsigned int parent_task = current->parent_task;
    
    if (current->return_code_here != NULL)
        *(current->return_code_here) = code;
    
    purge_task(current->task_id);
    
    iswitch_task(parent_task);
}

void abort_implementation(void *param_ptr, unsigned int ident) {
    Task *current = get_current_task();
    
    if (current->task_id == 0) {
        printf("Aborted from init!\n");
        panic();
    }
    if (ident == 0) {
        printf("Aborted from kernel!\n");
        panic();
    }

    unsigned int parent_task = current->parent_task;
    
    if (current->return_code_here != NULL)
        *(current->return_code_here) = ABORTED;
    
    purge_task(current->task_id);
    
    iswitch_task(parent_task);
}

void adopt_implementation(void *param_ptr, unsigned int ident) {
    SyscallAdoptParam *param = (SyscallAdoptParam*) param_ptr;
    
    Task *orphan_task = get_task(param->child_task_id);
    if (orphan_task == NULL) {
        param->result = TASK_DOES_NOT_EXIST;
        return;
    }
    IdentifiedAgent *orphan_agent = get_agent(orphan_task->owner_id);
    
    if (!(authorize_agent_id(orphan_agent->phys_race_attrib, orphan_agent->data_class_attrib, ident))) {
        param->result = LAUNCH_NOT_AUTHORIZED;
        return;
    }
    
    if (orphan_task->parent_task != 0) {
        param->result = ADOPTION_NOT_ALLOWED;
        return;
    }
    
    Task *current = get_current_task();
    orphan_task->parent_task = current->task_id;
    param->result = 0;
}

void disown_implementation(void *param_ptr, unsigned int ident) {
    SyscallAdoptParam *param = (SyscallAdoptParam*) param_ptr;
    
    Task *orphan_task = get_task(param->child_task_id);
    if (orphan_task == NULL) {
        param->result = TASK_DOES_NOT_EXIST;
        return;
    }
    IdentifiedAgent *orphan_agent = get_agent(orphan_task->owner_id);
    
    if (!(authorize_agent_id(orphan_agent->phys_race_attrib, orphan_agent->data_class_attrib, ident))) {
        param->result = LAUNCH_NOT_AUTHORIZED;
        return;
    }
    
    Task *current = get_current_task();
    if (orphan_task->parent_task != current->task_id) {
        param->result = ADOPTION_NOT_ALLOWED;
        return;
    }
    
    orphan_task->parent_task = 0;
    param->result = 0;
}

void kill_implementation(void *param_ptr, unsigned int ident) {
    SyscallKillParam *param = (SyscallKillParam*) param_ptr;
    Task *morituri = get_task(param->task_id);
    
    if (morituri == NULL) {
        param->result = TASK_DOES_NOT_EXIST;
        return;
    }
    
    IdentifiedAgent *morituri_agent = get_agent(morituri->owner_id);
    
    if (!(authorize_agent_id(morituri_agent->phys_race_attrib, morituri_agent->data_class_attrib, ident))) {
        param->result = SECURITY_DENIED;
        return;
    }
    
    if (morituri->return_code_here != NULL)
        *(morituri->return_code_here) = TASK_KILLED;
    
    purge_task(morituri->task_id);
    
    param->result = 0;
}
