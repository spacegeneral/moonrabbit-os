#include <stdio.h>
#include <errno.h>

#if defined(__is_libk)
#include <data/vfs.h>
#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

int fclose_pure(FILE *stream) {
    FSServiceMessage request;
    request.service_name = fsservice_fclose;
    request.msg.fclose.stream = *stream;
#if defined(__is_libk)
    internalfs_service_handler((void*)&request, 0, 0);
#else
    make_fsservice_request(stream->driver, &request);
#endif
    errno = request.status;
    if (errno == 0) free(stream);
    return request.status;
}


int fclose_zlib(FILE *stream) {
    LibzOpenFileUserData *user_data = stream->interface.user_data;
    
    if (user_data->dirty) {
        fflush_zlib(stream);
    }
    
    free(user_data->memstore);
    free(user_data);
    return fclose_pure(stream);
}


int fclose(FILE *stream) {
    
    if (stream == stdin) return 0;
    if (stream == stdout) return 0;
    if (stream == stderr) return 0;
    
    return stream->interface.fclose(stream);
}
