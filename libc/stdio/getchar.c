#include <stdio.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/term.h>
#include <sys/keycodes.h>

#if defined(__is_libk)
#include <syscall.h>
#include <interrupt/timing.h>
#else
#include <os.h>
#endif


bool do_echo = true;


int getchar() {
    char ic;
    MQStatus poll_status = MQ_NO_MESSAGES; 
#if defined(__is_libk)
    subscribe(DEFAULT_STDIN_TOPIC);
    while (poll_status == MQ_NO_MESSAGES) {
        poll_status = poll(DEFAULT_STDIN_TOPIC, &ic);
        if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) {
            printf("fatal stdin error %d.\n", poll_status);
            return 0;
        }
        if (poll_status == MQ_NO_MESSAGES) halt();
    }
    unsubscribe(DEFAULT_STDIN_TOPIC);
#else
    subscribe(stdin_topic);
    while (poll_status == MQ_NO_MESSAGES) {
        poll_status = poll(stdin_topic, &ic);
        if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) {
            printf("fatal stdin error %d.\n", poll_status);
            exit(1);
        }
        if (poll_status == MQ_NO_MESSAGES) yield();
    }
    unsubscribe(stdin_topic);
#endif
    if (do_echo) {
        putchar((int)ic);
    }
    return (int)ic;
}


int getchar_nosub() {
    char ic;
    MQStatus poll_status = MQ_NO_MESSAGES; 
#if defined(__is_libk)
    while (poll_status == MQ_NO_MESSAGES) {
        poll_status = poll(DEFAULT_STDIN_TOPIC, &ic);
        if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) {
            printf("fatal stdin error %d.\n", poll_status);
            return 0;
        }
        if (poll_status == MQ_NO_MESSAGES) halt();
    }
#else
    while (poll_status == MQ_NO_MESSAGES) {
        poll_status = poll(stdin_topic, &ic);
        if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) {
            printf("fatal stdin error.\n");
            exit(1);
        }
        if (poll_status == MQ_NO_MESSAGES) yield();
    }
#endif
    if (do_echo) {
        putchar((int)ic);
    }
    return (int)ic;
}



KeyEvent getkey(bool ignore_released) {
    KeyEvent ic;
    ic.is_pressed = false;
    MQStatus poll_status = MQ_NO_MESSAGES; 
#if defined(__is_libk)
    subscribe(DEFAULT_KEYCODE_TOPIC);
    while (poll_status == MQ_NO_MESSAGES || (!ic.is_pressed && ignore_released)) {
        poll_status = poll(DEFAULT_KEYCODE_TOPIC, &ic);
        if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) {
            printf("fatal stdin error %d.\n", poll_status);
            return ic;
        }
        if (poll_status == MQ_NO_MESSAGES) halt();
    }
    unsubscribe(DEFAULT_KEYCODE_TOPIC);
#else
    subscribe(keycode_topic);
    while (poll_status == MQ_NO_MESSAGES || (!ic.is_pressed && ignore_released)) {
        poll_status = poll(keycode_topic, &ic);
        if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) {
            printf("fatal stdin error.\n");
            exit(1);
        }
        if (poll_status == MQ_NO_MESSAGES) yield();
    }
    unsubscribe(keycode_topic);
#endif
    return ic;
}

int getkcode() {
    return getkey(true).keycode;
}


int getkcode_async() {
    KeyEvent ic;
    MQStatus poll_status = MQ_NO_MESSAGES; 
#if defined(__is_libk)
    subscribe(DEFAULT_KEYCODE_TOPIC);
    poll_status = poll(DEFAULT_KEYCODE_TOPIC, &ic);
    halt();
    unsubscribe(DEFAULT_KEYCODE_TOPIC);
#else
    subscribe(keycode_topic);
    poll_status = poll(keycode_topic, &ic);
    yield();
    unsubscribe(keycode_topic);
#endif
    if (poll_status == MQ_SUCCESS) {
        return ic.keycode;
    }
    return -1;
}


int fgetc(FILE *stream) {
    if (stream == stdin) return getchar();
    else {
        unsigned char rc;
        if (fread(&rc, 1, 1, stream) != 1) return EOF;
        return (int) rc;
    }
}


int ungetc(int c, FILE *stream) { //TODO implement this
    return 0;
}
