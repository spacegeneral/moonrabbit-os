#include <stdio.h>
#include <errno.h>
#include <yakumo.h>

#if defined(__is_libk)
#include <data/vfs.h>
#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

size_t fread_pure(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    FSServiceMessage request;
    request.service_name = fsservice_fread;
    request.msg.fread.stream = *stream;
    size_t total_data = size * nmemb;
    size_t recv_data = 0;

    if (total_data < MAX_FILEIO_PAYLOAD) {
        request.msg.fread.size = size;
        request.msg.fread.nmemb = nmemb;
#if defined(__is_libk)
        internalfs_service_handler((void*)&request, 0, 0);
#else
        make_fsservice_request(stream->driver, &request);
#endif
        memcpy(ptr, (void*)request.msg.fread.payload, total_data);
        stream->seek_pos += request.msg.fread.result*size;
        errno = request.status;
        return request.msg.fread.result;
    } else {
        request.msg.fread.size = 1;
        request.msg.fread.nmemb = MAX_FILEIO_PAYLOAD;
        while (total_data >= MAX_FILEIO_PAYLOAD) {
#if defined(__is_libk)
            internalfs_service_handler((void*)&request, 0, 0);
#else
            make_fsservice_request(stream->driver, &request);
#endif
            memcpy((void*)(((uint8_t*)ptr) + recv_data), (void*)request.msg.fread.payload, MAX_FILEIO_PAYLOAD);
            total_data -= MAX_FILEIO_PAYLOAD;
            recv_data += request.msg.fread.result;
            stream->seek_pos += request.msg.fread.result;
            request.msg.fread.stream.seek_pos = stream->seek_pos;
            errno = request.status;
            if (errno != 0) return recv_data / size;
        }
        if (total_data > 0) {
            request.msg.fread.size = 1;
            request.msg.fread.nmemb = total_data;
#if defined(__is_libk)
            internalfs_service_handler((void*)&request, 0, 0);
#else
            make_fsservice_request(stream->driver, &request);
#endif
            memcpy((void*)(((uint8_t*)ptr) + recv_data), (void*)request.msg.fread.payload, total_data);
            recv_data += request.msg.fread.result;
            stream->seek_pos += request.msg.fread.result;
            errno = request.status;
        }
        return recv_data / size;
    }
}


size_t fread_zlib(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    LibzOpenFileUserData *user_data = stream->interface.user_data;
    
    size_t filesize = user_data->mem_length;
    uint8_t *source = user_data->memstore;
    if (source == NULL) return 0;
    uint8_t *destination = (uint8_t*) ptr;
    if (user_data->super_seek >= filesize) return 0;
    size_t amount = min(filesize, user_data->super_seek + (size*nmemb)) - user_data->super_seek;
    memcpy(destination, source + user_data->super_seek, amount);
    
    user_data->super_seek += amount;
    return amount / size;
}


size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    if (stream == stdin) {
        char *buff_view = (char*) ptr;
        for (size_t wb=0; wb<size*nmemb; wb++) {
            buff_view[wb] = (char) getchar();
        }
        return size*nmemb;
    }
    
    return stream->interface.fread(ptr, size, nmemb, stream);
}
