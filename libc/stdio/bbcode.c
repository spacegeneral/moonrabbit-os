#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bbcode.h>

#include <sys/term.h>
#include <rice.h>

#define BBCODE_PARSER_ESCAPE_CHAR '\\'

typedef enum BBCodeParserStage {
    BBCODE_PARSER_TAG,
    BBCODE_PARSER_OPEN_TAG,
    BBCODE_PARSER_ATTRIBUTE,
    BBCODE_PARSER_CONTENT,
    BBCODE_PARSER_CLOSE_TAG
    
} BBCodeParserStage;


typedef struct BBCodeParserState {
    OString current_tag;
    OString current_close_tag;
    OString current_attribute;
    OString current_content;
    size_t cursor;
    BBCodeParserStage stage;
    
} BBCodeParserState;


void BBCodeParserState_init(BBCodeParserState* state) {
    OString_init(&(state->current_tag));
    OString_init(&(state->current_close_tag));
    OString_init(&(state->current_attribute));
    OString_init(&(state->current_content));
    state->cursor = 0;
    state->stage = BBCODE_PARSER_CONTENT;
}



void parse_bbcode(char* code, 
                  parsed_content_callback callback_content, 
                  parsed_tag_open_callback callback_open, 
                  parsed_tag_close_callback callback_close) {
    BBCodeParserState state;
    BBCodeParserState_init(&state);
    
    size_t code_length = strlen(code);
    
    char old_char = '\0';
    bool escape_on = false;
    
    for (state.cursor = 0; state.cursor < code_length; state.cursor++) {
        
        char curr_char = code[state.cursor];
        
        if (curr_char == BBCODE_PARSER_ESCAPE_CHAR && !escape_on) {
            old_char = curr_char;
            escape_on = true;
            continue;
        }
        
        switch (state.stage) {      
            case BBCODE_PARSER_TAG:
            {
                if (curr_char == '/') {
                    state.stage = BBCODE_PARSER_CLOSE_TAG;
                } else {
                    state.stage = BBCODE_PARSER_OPEN_TAG;
                    OString_append(&(state.current_tag), curr_char);
                }
            }
            break;
            
            case BBCODE_PARSER_OPEN_TAG:
            {
                if (curr_char == '=' && !escape_on) {
                    state.stage = BBCODE_PARSER_ATTRIBUTE;
                    OString_clear(&(state.current_attribute));
                }
                else if (curr_char == ']' && !escape_on) {
                    state.stage = BBCODE_PARSER_CONTENT;
                    callback_open(state.current_tag.text, state.current_attribute.text);
                    OString_clear(&(state.current_attribute)); 
                    OString_clear(&(state.current_content)); 
                }
                else {
                    OString_append(&(state.current_tag), curr_char);
                }
            }
            break;
            
            case BBCODE_PARSER_ATTRIBUTE:
            {
                if (curr_char == ']' && !escape_on) {
                    state.stage = BBCODE_PARSER_CONTENT;
                    callback_open(state.current_tag.text, state.current_attribute.text);
                    OString_clear(&(state.current_attribute)); 
                    OString_clear(&(state.current_content)); 
                }
                else {
                    OString_append(&(state.current_attribute), curr_char);
                }
            }
            break;
            
            case BBCODE_PARSER_CONTENT:
            {
                if (curr_char == '[' && !escape_on) {
                    callback_content(state.current_tag.text, state.current_attribute.text, state.current_content.text);
                    state.stage = BBCODE_PARSER_TAG;
                    OString_clear(&(state.current_close_tag)); 
                    OString_clear(&(state.current_tag));
                    OString_clear(&(state.current_attribute));
                    OString_clear(&(state.current_content));
                }
                else {
                    OString_append(&(state.current_content), curr_char);
                }
            }
            break;
            
            case BBCODE_PARSER_CLOSE_TAG:
            {
                if (curr_char == ']' && !escape_on) {
                    state.stage = BBCODE_PARSER_CONTENT;
                
                    callback_close(state.current_close_tag.text);
                
                    OString_clear(&(state.current_content));
                    OString_clear(&(state.current_tag));
                    OString_clear(&(state.current_close_tag));
                    OString_clear(&(state.current_attribute));
                }
                else {
                    OString_append(&(state.current_close_tag), curr_char);
                }
            }
            break;
        }

        old_char = curr_char;
        escape_on = false;
    }
    
    if (state.current_content.length > 0) {
        callback_content(state.current_tag.text, state.current_attribute.text, state.current_content.text);
    }
}


void print_bbcode_content_function(char* tag_name, char* tag_attribute, char* tag_content) {
    printf("%s", tag_content);
}


static int saved_color = COLOR24_UNDEFINED;


void print_bbcode_open_tag_function(char* tag_name, char* tag_attribute) {
    if (strcmp(tag_name, "color") == 0) {
        TermInfo ti = getterminfo();
        saved_color = ti.foreground_color;
        setcolors(color_string_to_code(tag_attribute), COLOR24_UNDEFINED);
    }
    else if (strcmp(tag_name, "b") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_BOLD);
    }
    else if (strcmp(tag_name, "i") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_ITALICS);
    }
    else if (strcmp(tag_name, "u") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_UNDERLINE);
    }
    else if (strcmp(tag_name, "g") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_GOTHIC);
    }
    else if (strcmp(tag_name, "s") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_SCRIPT);
    }
    else if (strcmp(tag_name, "zoom") == 0) {
        setzoomlevel(atoi(tag_attribute));
    }
    else if (strcmp(tag_name, "tag") == 0) {
        puttag(tag_attribute);
    }
}

void print_bbcode_close_tag_function(char* tag_name) {
    if (strcmp(tag_name, "color") == 0) {
        setcolors(saved_color, COLOR24_UNDEFINED);
    }
    else if (strcmp(tag_name, "b") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_BOLD));
    }
    else if (strcmp(tag_name, "i") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_ITALICS));
    }
    else if (strcmp(tag_name, "u") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_UNDERLINE));
    }
    else if (strcmp(tag_name, "g") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_GOTHIC));
    }
    else if (strcmp(tag_name, "s") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_SCRIPT));
    }
    else if (strcmp(tag_name, "zoom") == 0) {
        setzoomlevel(0);
    }
    else if (strcmp(tag_name, "tag") == 0) {
        poptag();
    }
}




int color_string_to_code(char* color) {
    if (color[0] == '#') {
        return (int)strtol(color+1, NULL, 16);
    }
    
    if (strcmp(color, "normal") == 0) return COLOR24_TEXT;
    else if (strcmp(color, "highlight") == 0) return COLOR24_TEXT_HIGHLIGHT;
    else if (strcmp(color, "highlight2") == 0) return COLOR24_TEXT_HIGHLIGHT2;
    else if (strcmp(color, "highlight3") == 0) return COLOR24_TEXT_HIGHLIGHT3;
    else if (strcmp(color, "background") == 0) return COLOR24_TERM_BGROUND;
    else if (strcmp(color, "red") == 0) return 0x00FF0000;
    else if (strcmp(color, "green") == 0) return 0x0000FF00;
    else if (strcmp(color, "blue") == 0) return 0x000000FF;
    else if (strcmp(color, "cyan") == 0) return 0x0000FFFF;
    else if (strcmp(color, "magenta") == 0) return 0x00FF00FF;
    else if (strcmp(color, "yellow") == 0) return 0x00FFFF00;
    else if (strcmp(color, "white") == 0) return 0x00FFFFFF;
    else if (strcmp(color, "black") == 0) return 0x00000000;
    else if (strcmp(color, "gray") == 0) return 0x00808080;
    else if (strcmp(color, "orange") == 0) return 0x00ff6403;
    else if (strcmp(color, "brown") == 0) return 0x00562c05;
    
    
    return COLOR24_TEXT;
}