#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <yakumo.h>

#if defined(__is_libk)
#include <data/vfs.h>
#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif


size_t fwrite_pure(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
    FSServiceMessage request;
    request.service_name = fsservice_fwrite;
    request.msg.fwrite.stream = *stream;
    size_t total_data = size * nmemb;
    size_t sent_data = 0;
    if (total_data < MAX_FILEIO_PAYLOAD) {
        request.msg.fwrite.size = size;
        request.msg.fwrite.nmemb = nmemb;
        memcpy((void*)request.msg.fwrite.payload, ptr, total_data);
#if defined(__is_libk)
        internalfs_service_handler((void*)&request, 0, 0);
#else
        make_fsservice_request(stream->driver, &request);
#endif
        stream->seek_pos += request.msg.fwrite.result*size;
        errno = request.status;
        return request.msg.fwrite.result;
    } else {
        request.msg.fwrite.size = 1;
        request.msg.fwrite.nmemb = MAX_FILEIO_PAYLOAD;
        while (total_data >= MAX_FILEIO_PAYLOAD) {
            memcpy((void*)request.msg.fwrite.payload, (void*)(((uint8_t*)ptr) + sent_data), MAX_FILEIO_PAYLOAD);
#if defined(__is_libk)
            internalfs_service_handler((void*)&request, 0, 0);
#else
            make_fsservice_request(stream->driver, &request);
#endif
            total_data -= MAX_FILEIO_PAYLOAD;
            sent_data += request.msg.fwrite.result;
            stream->seek_pos += request.msg.fwrite.result;
            request.msg.fwrite.stream.seek_pos = stream->seek_pos;
            errno = request.status;
            if (errno != 0) return sent_data / size;
        }
        if (total_data > 0) {
            request.msg.fwrite.size = 1;
            request.msg.fwrite.nmemb = total_data;
            memcpy((void*)request.msg.fwrite.payload, (void*)(((uint8_t*)ptr) + sent_data), total_data);
#if defined(__is_libk)
            internalfs_service_handler((void*)&request, 0, 0);
#else
            make_fsservice_request(stream->driver, &request);
#endif
            sent_data += request.msg.fwrite.result;
            stream->seek_pos += request.msg.fwrite.result;
            request.msg.fwrite.stream.seek_pos = stream->seek_pos;
            errno = request.status;
        }
        return sent_data / size;
    }
}


size_t fwrite_zlib(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
    LibzOpenFileUserData *user_data = stream->interface.user_data;
    
    size_t datasize = user_data->super_seek + (size * nmemb);
    if (user_data->mem_length < datasize) {
        user_data->memstore = (uint8_t*) realloc(user_data->memstore, sizeof(uint8_t) * datasize);
        user_data->mem_length = datasize;
    }
    uint64_t filesize = user_data->mem_length;
    uint8_t *destination = user_data->memstore;
    uint8_t *source = (uint8_t*) ptr;
    size_t amount = min(filesize, user_data->super_seek + (size * nmemb)) - user_data->super_seek;
    memcpy(destination + user_data->super_seek, source, amount);
    
    user_data->super_seek += amount;
    user_data->dirty = true;
    return amount / size;
}


size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
    
    if (stream == stdout) {
        char *buff_view = (char*) ptr;
        for (size_t wb=0; wb<size*nmemb; wb++) {
            putchar(buff_view[wb]);
        }
        return size*nmemb;
    }
    
    return stream->interface.fwrite(ptr, size, nmemb, stream);
}
