#include <stdio.h>


int feof(FILE *stream) {
    long current_seek = ftell(stream);
    fseek(stream, 0, SEEK_END);
    int finished = (ftell(stream) == current_seek);
    fseek(stream, current_seek, SEEK_SET);
    return finished;
}

int ferror(FILE *stream) {  //TODO implement this
    return 0;
}

