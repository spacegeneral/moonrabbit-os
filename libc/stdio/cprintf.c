#include <stdio.h>
#include <sys/mq.h>



void topic_putc_interface(char c, void *p) {  // for cprintf
    if (p!=NULL) {
        char *topicname = (char*)p;
        publish(topicname, &c);
    }
    else putchar(c);
}


int cprintf(const char* topic, const char *fmt, ...) {
    va_list va;
    va_start(va, fmt);
    int ret = fctvprintf(&topic_putc_interface, (void*)topic, fmt, va);
    va_end(va);
    return ret;
}
