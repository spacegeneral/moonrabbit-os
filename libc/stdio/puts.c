#include <stdio.h>
#include <string.h>


int puts(const char* string) {
    printf("%s\n", string);
    return 1;
}

int fputs(const char *s, FILE *stream) {
    for (size_t c=0; c<strlen(s); c++) {
        if (fputc(s[c], stream) == EOF) return EOF; 
    }
    if (fputc('\n', stream) == EOF) return EOF; 
    return 1;
}
