#include <stdio.h>
#include <errno.h>

#if defined(__is_libk)

#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif


int fseek_pure(FILE *stream, long offset, int whence) {
    FSServiceMessage request;
    request.service_name = fsservice_fseek;
    request.msg.fseek.offset = offset;
    request.msg.fseek.whence = whence;
    request.msg.fseek.stream = *stream;
#if defined(__is_libk)
    internalfs_service_handler((void*)&request, 0, 0);
#else
    make_fsservice_request(stream->driver, &request);
#endif
    *stream = request.msg.fseek.stream;
    return request.status;
}


int fseek_zlib(FILE *stream, long offset, int whence) {
    LibzOpenFileUserData *user_data = stream->interface.user_data;
    
    long long file_size = (long long) (user_data->mem_length);
    if (whence == SEEK_SET) {
        if (offset < file_size) {
            user_data->super_seek = offset;
            return 0;
        }
    } else if (whence == SEEK_CUR) {
        if (user_data->super_seek + offset < file_size) {
            user_data->super_seek += offset;
            return 0;
        }
    } else if (whence == SEEK_END) {
        if (file_size - offset >= 0) {
            user_data->super_seek = file_size - offset;
            return 0;
        }
    }
    return -1;
}



int fseek(FILE *stream, long offset, int whence) {
    return stream->interface.fseek(stream, offset, whence);
}



int rewind(FILE *stream) {
    return fseek(stream, 0, SEEK_SET);
}
