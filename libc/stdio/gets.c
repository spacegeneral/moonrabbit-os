#include <stdio.h>
#include <sys/mq.h>


#define READLOOP_BODY(readfun) \
{ \
    newchar = readfun; \
    s[charpos] = (char) newchar; \
    if (newchar == '\b' && charpos > 0) charpos--; \
    else charpos++; \
}


char *gets(char *s) {
    size_t charpos = 0;
    int newchar;
    
#if defined(__is_libk)
    subscribe(DEFAULT_STDIN_TOPIC);
#else
    subscribe(stdin_topic);
#endif
    
    do {
        READLOOP_BODY(getchar_nosub());
    } while (newchar != '\0' && newchar != '\n' && newchar != '\r');
    
#if defined(__is_libk)
    unsubscribe(DEFAULT_STDIN_TOPIC);
#else
    unsubscribe(stdin_topic);
#endif
    
    s[charpos] = '\0';
    
    if (s[0] == '\0') return NULL;
    return s;
}


char *getsn(char *s, size_t limit) {
    size_t charpos = 0;
    int newchar;
    
#if defined(__is_libk)
    subscribe(DEFAULT_STDIN_TOPIC);
#else
    subscribe(stdin_topic);
#endif
    
    do {
        READLOOP_BODY(getchar_nosub());
    } while (newchar != '\0' && newchar != '\n' && newchar != '\r' && charpos < limit);
    
#if defined(__is_libk)
    unsubscribe(DEFAULT_STDIN_TOPIC);
#else
    unsubscribe(stdin_topic);
#endif
    
    s[charpos] = '\0';
    
    if (s[0] == '\0') return NULL;
    return s;
}

bool is_stop_char(char candidate, char *stopchars, size_t num_stopchars) {
    for (size_t i=0; i<num_stopchars; i++) {
        if (candidate == stopchars[i]) return true;
    }
    return false;
}

char *getsn_customstop(char *s, size_t limit, char *stopchars, size_t num_stopchars) {
    size_t charpos = 0;
    int newchar;
    
#if defined(__is_libk)
    subscribe(DEFAULT_STDIN_TOPIC);
#else
    subscribe(stdin_topic);
#endif
    
    do {
        READLOOP_BODY(getchar_nosub());
    } while (!is_stop_char(newchar, stopchars, num_stopchars) && charpos < limit);
    
#if defined(__is_libk)
    unsubscribe(DEFAULT_STDIN_TOPIC);
#else
    unsubscribe(stdin_topic);
#endif
    
    s[charpos] = '\0';
    
    if (s[0] == '\0') return NULL;
    return s;
}

char *fgets(char *s, int size, FILE *stream) {
    int charpos = 0;
    int newchar;
    do {
        newchar = fgetc(stream);
        s[charpos] = (char) newchar;
        charpos++;
    } while (newchar != '\0' && newchar != '\n' && newchar != EOF && charpos+1 < size);
    
    if (newchar == EOF) {
        charpos--;
    }
    
    s[charpos] = '\0';
    
    if (s[0] == '\0') return NULL;
    return s;
}
