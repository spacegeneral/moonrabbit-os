#include <stdio.h>
#include <sys/mq.h>

#if defined(__is_libk)
#include <syscall.h>
#include <kernel/tty.h>
#else
#include <os.h>
#endif

int putchar(int ic) {
#if defined(__is_libk)
    CALL_IMPL(video, terminal, putchar, ic);
#else
    TermOutMessage msg;
    msg.verb = Putchar;
    msg.msg.character.character = (char) ic;
    publish(termout_topic, &msg);
    char c = (char) ic;
    publish(stdout_topic, &c);
#endif
    return (unsigned) ic;
}

void _putchar(char character) {  // for printf
    putchar(character);
}


int fputc(int c, FILE *stream) {
    if (stream == stdout) return putchar(c);
    else if (stream == stderr) {
        char msg = (char)c;
        publish(DEFAULT_DEBUG_TOPIC, &msg);
        return putchar(c);
    } else {
        char wc = (char) c;
        if (fwrite(&wc, 1, 1, stream) != 1) {
            putchar(c);
            return EOF;
        }
        return (unsigned) c;
    }
}
