#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <yakumo.h>
#include <miniz/miniz.h>

#if defined(__is_libk)
#include <data/vfs.h>
#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

FILE *fopen_pure(const char *pathname, const char *mode) {
    FSServiceMessage request;
    request.service_name = fsservice_fopen;
    strncpy(request.msg.fopen.mode, mode, MAX_FOPEN_MODESTR_LENGTH);
#if defined(__is_libk)
    strncpy(request.msg.fopen.pathname, pathname, MAX_PATH_LENGTH);
    internalfs_service_handler((void*)&request, 0, 0);
#else
    char drivername[MAX_FS_DRIVER_NAME_LENGTH];
    char explicit_path[MAX_PATH_LENGTH];
    get_mount_info(pathname, drivername, explicit_path);
    strncpy(request.msg.fopen.pathname, explicit_path, MAX_PATH_LENGTH);
    make_fsservice_request(drivername, &request);
#endif
    errno = request.status;
    if (errno != 0) return NULL;
    FILE* newfile = malloc(sizeof(FILE));
    memcpy((void*)newfile, (void*)(&request.msg.fopen.result), sizeof(FILE));
    return newfile;
}


FILE *fopen_zlib(const char *pathname, const char *mode) {
    FILE *base_file = fopen_pure(pathname, mode);
    if (base_file == NULL) return NULL;
    
    fseek_pure(base_file, 0, SEEK_END);
    long orig_size = ftell_pure(base_file);
    fseek_pure(base_file, 0, SEEK_SET);
    
    LibzOpenFileUserData *user_data = (LibzOpenFileUserData*) malloc(sizeof(LibzOpenFileUserData));
    size_t uncompressed_size;
    void *memstore;  // uncompressed content here
    
    if (orig_size == 0) {  // empty / new file
        uncompressed_size = 0;
        memstore = malloc(sizeof(char) * 1);
    } else {
        char *full_content = (char*) malloc(sizeof(char) * orig_size);  // compressed content here
        
        if (fread_pure(full_content, 1, orig_size, base_file) != (size_t)orig_size) {  // read compressed content
            free(full_content);
            fclose_pure(base_file);
            return NULL;
        }
        
        // uncompress to memory
        memstore = tinfl_decompress_mem_to_heap((const void *) full_content, (size_t) orig_size, &uncompressed_size, TINFL_FLAG_PARSE_ZLIB_HEADER);
        if (memstore == NULL) {
            free(full_content);
            fclose_pure(base_file);
            debug_printf("decompress error for %s\r\n", pathname);
            return NULL;
        }
        
        free(full_content);
    }
    
    user_data->dirty = false;
    user_data->memstore = memstore;
    user_data->mem_length = uncompressed_size;
    user_data->super_seek = 0;
    base_file->interface.user_data = user_data;
    return base_file;
}


FILE *fopen(const char *pathname, const char *mode) {
    FILE_Interface new_interface;
    
    if (endswith(pathname, ".zz")) {
        new_interface.user_data = NULL;
        new_interface.fopen = &fopen_zlib;
        new_interface.fclose = &fclose_zlib;
        new_interface.fseek = &fseek_zlib;
        new_interface.fwrite = &fwrite_zlib;
        new_interface.fread = &fread_zlib;
        new_interface.fflush = &fflush_zlib;
        new_interface.ftell = &ftell_zlib;
    }
    else {
        new_interface.user_data = NULL;
        new_interface.fopen = &fopen_pure;
        new_interface.fclose = &fclose_pure;
        new_interface.fseek = &fseek_pure;
        new_interface.fwrite = &fwrite_pure;
        new_interface.fread = &fread_pure;
        new_interface.fflush = &fflush_pure;
        new_interface.ftell = &ftell_pure;
    }
    
    FILE *open_file = new_interface.fopen(pathname, mode);
    if (open_file != NULL) {
        FILE_Interface *old_interface = &(open_file->interface);
        new_interface.user_data = old_interface->user_data;
        open_file->interface = new_interface;
    }
    
    return open_file;
}

FILE *fopen_no_zlib(const char *pathname, const char *mode) {
    FILE_Interface new_interface;
    
    new_interface.user_data = NULL;
    new_interface.fopen = &fopen_pure;
    new_interface.fclose = &fclose_pure;
    new_interface.fseek = &fseek_pure;
    new_interface.fwrite = &fwrite_pure;
    new_interface.fread = &fread_pure;
    new_interface.fflush = &fflush_pure;
    new_interface.ftell = &ftell_pure;
    
    FILE *open_file = new_interface.fopen(pathname, mode);
    if (open_file != NULL) {
        FILE_Interface *old_interface = &(open_file->interface);
        new_interface.user_data = old_interface->user_data;
        open_file->interface = new_interface;
    }
    
    return open_file;
}
    
    
