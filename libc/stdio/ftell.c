#include <stdio.h>


long ftell_pure(FILE *stream) {
    return (long)stream->seek_pos;
}

long ftell_zlib(FILE *stream) {
    LibzOpenFileUserData *user_data = stream->interface.user_data;
    return (long)user_data->super_seek;
}

long ftell(FILE *stream) {
    return stream->interface.ftell(stream);
}
