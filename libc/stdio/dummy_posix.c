#include <stdio.h>
#include <fcntl.h>


int open(const char *path, int oflag, ...) {
    char mode[4] = {'\0'};
    int mode_ptr = 0;
    
    if (oflag & O_ACCMODE) {
        mode[mode_ptr++] = 'a';
    } else if (oflag & O_RDWR) {
        mode[mode_ptr++] = 'r';
        mode[mode_ptr++] = '+';
    } else if (oflag & O_WRONLY) {
        mode[mode_ptr++] = 'w';
    } else {
        mode[mode_ptr++] = 'r';
    }
    
    if (oflag & O_BINARY) {
        mode[mode_ptr++] = 'b';
    }
    
    FILE *fp = fopen(path, mode);
    if (fp == NULL) return -1;
    return (int) fp;
}

FILE* fdopen(int fildes, const char *mode) {
    return (FILE*)fildes;
}

int close(int fildes) {
    FILE *fp = (FILE*)fildes;
    return fclose(fp);
}

size_t read(int fildes, void *buffer, size_t length) {
    FILE *fp = (FILE*)fildes;
    return fread(buffer, 1, length, fp);
}

size_t write(int fildes, const void *buffer, size_t length) {
    FILE *fp = (FILE*)fildes;
    return fwrite(buffer, 1, length, fp);
}

int lseek(int fildes, long offset, int whence) {
    FILE *fp = (FILE*)fildes;
    return fseek(fp, offset, whence);
}
