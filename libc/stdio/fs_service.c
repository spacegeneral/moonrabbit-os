#include <stdio.h>
#include <sys/serv.h>
#include <sys/fs.h>
#include <sys/proc.h>
#include <time.h>
#include <errno.h>


#define FS_SERV_TIMEOUT 5000


#define FS_SERV_REQ_ERRORCHECK() if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) return(60 + reqstatus);
#define FS_SERV_POLL_ERRORCHECK() if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) return(70 + reqstatus);



static int make_fsservice_request_inner(char *driver, FSServiceMessage *request) {
    time_t start;
    char full_drivername[4096];
    snprintf(full_drivername, 4096, "/sys/fs/%s", driver);
    unsigned int request_id;
    ServStatus reqstatus;
    // send request
    start = clock_ms();
    do {
        reqstatus = service_request(full_drivername, request, &request_id);
        FS_SERV_REQ_ERRORCHECK();
        if (clock_ms() - start > FS_SERV_TIMEOUT) return FS_SERVICE_TIMEOUT;
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    // await result
    start = clock_ms();
    do {
        reqstatus = service_poll(full_drivername, request_id);
        FS_SERV_POLL_ERRORCHECK();
        if (clock_ms() - start > FS_SERV_TIMEOUT) return FS_SERVICE_TIMEOUT;
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    
    return 0;
}

void make_fsservice_request(char *driver, FSServiceMessage *request) {
    int attempts = 25;
    int status;
    do {
        status = make_fsservice_request_inner(driver, request);
        if (status == FS_SERVICE_TIMEOUT) {
            request->status = FS_SERVICE_TIMEOUT;
            return;
        }
        if (status != 0) yield();
    } while ((status != 0) && (attempts-- > 0));
}
