#include <stdio.h>
#include <geom.h>

#if defined(__is_libk)
#include <kernel/tty.h>
#else
#include <sys/serv.h>
#include <sys/mq.h>
#include <sys/term.h>
#include <sys/proc.h>
#endif



#define TERM_SERV_REQ_ERRORCHECK() if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) exit(60 + reqstatus);
#define TERM_SERV_POLL_ERRORCHECK() if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) exit(70 + reqstatus);



TermInfo getterminfo() {
    TermInfo ti;
#if defined(__is_libk)
    ti = CALL_IMPL(video, terminal, getinfo);
#else
    unsigned int request_id;
    ServStatus reqstatus;
    // send request
    do {
        reqstatus = service_request(terminfo_topic, &ti, &request_id);
        TERM_SERV_REQ_ERRORCHECK();
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    // await result
    do {
        reqstatus = service_poll(terminfo_topic, request_id);
        TERM_SERV_POLL_ERRORCHECK();
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
#endif
    return ti;
}


void cls() {
#if defined(__is_libk)
    CALL_IMPL(video, terminal, cls);
#else
    TermOutMessage msg;
    msg.verb = CLS;
    publish(termout_topic, &msg);
#endif
}


void setcursor(int x, int y) {
#if defined(__is_libk)
    CALL_IMPL(video, terminal, update_cursor, x, y);
    CALL_IMPL(video, terminal, gotoxy, x, y);
#else
    TermOutMessage msg;
    msg.verb = SetCursorPos;
    msg.msg.position.x = x;
    msg.msg.position.y = y;
    publish(termout_topic, &msg);
#endif
}

void togglecursor(int enabled) {
#if defined(__is_libk)
    if (enabled) {CALL_IMPL(video, terminal, enable_cursor);}
    else {CALL_IMPL(video, terminal, disable_cursor);}
#else
    TermOutMessage msg;
    msg.verb = ToggleCursor;
    msg.msg.toggle.enabled = enabled;
    publish(termout_topic, &msg);
#endif
}


void setcolors(int foreground, int background) {
#if defined(__is_libk)
    CALL_IMPL(video, terminal, set_colors, foreground, background);
#else
    TermOutMessage msg;
    msg.verb = SetColors;
    msg.msg.colors.foreground = foreground;
    msg.msg.colors.background = background;
    publish(termout_topic, &msg);
#endif
}


void setscrollrange(int scroll_top, int scroll_bottom) {
#if defined(__is_libk)
    CALL_IMPL(video, terminal, set_scroll_range, scroll_top, scroll_bottom);
#else
    TermOutMessage msg;
    msg.verb = SetScrollRange;
    msg.msg.range.start = scroll_top;
    msg.msg.range.end = scroll_bottom;
    publish(termout_topic, &msg);
#endif
}


void scrollreverse() {
#if defined(__is_libk)
    CALL_IMPL(video, terminal, scrolldown);
#else
    TermOutMessage msg;
    msg.verb = ScrollReverse;
    publish(termout_topic, &msg);
#endif
}


void setwritingdirection(WritingDirection direction) {
#if defined(__is_libk)
    CALL_IMPL(video, terminal, set_writing_direction, direction);
#else
    TermOutMessage msg;
    msg.verb = SetWritingDirection;
    msg.msg.writing.direction = direction;
    publish(termout_topic, &msg);
#endif
}


void settermflags(unsigned int flags) {
#if defined(__is_libk)
    CALL_IMPL(video, terminal, set_term_flags, flags);
#else
    TermOutMessage msg;
    msg.verb = SetTermFlags;
    msg.msg.flags.flags = flags;
    publish(termout_topic, &msg);
#endif
}

// not universally supported
void setzoomlevel(int level) {
#if defined(__is_libk)
    //CALL_IMPL(video, terminal, set_zoom_level, level);
#else
    TermOutMessage msg;
    msg.verb = SetZoomLevel;
    msg.msg.zoom.level = level;
    publish(termout_topic, &msg);
#endif
}


void display_bitmap(Bitmap *bitmap, int *colorkey) {
    int actual_colorkey;
    actual_colorkey = COLORKEY_NONE;
    if (colorkey != NULL) actual_colorkey = *colorkey;
#if defined(__is_libk)
    CALL_IMPL(video, terminal, display_bitmap, bitmap, actual_colorkey);
#else
    TermBitmapRequest req = {
        .bitmap = bitmap,
        .colorkey = actual_colorkey,
    };
    unsigned int request_id;
    ServStatus reqstatus;
    // send request
    do {
        reqstatus = service_request(termbitmap_topic, &req, &request_id);
        if (reqstatus == SERV_FULL) yield();
    } while (reqstatus == SERV_FULL);
    
    if (reqstatus != SERV_SUCCESS) return;
    // await result
    do {
        reqstatus = service_poll(termbitmap_topic, request_id);
        if (reqstatus == SERV_PLEASE_WAIT) yield();
    } while (reqstatus == SERV_PLEASE_WAIT);
#endif
}

void puttag(char* tagstr) {
#if defined(__is_libk)
    
#else
    TermOutMessage msg;
    msg.verb = Puttag;
    for (size_t i=0; i<strlen(tagstr)+1; i++) {
        msg.msg.tag.tag_character = tagstr[i];
        publish(termout_topic, &msg);
    }
#endif
}

void poptag() {
#if defined(__is_libk)
    
#else
    TermOutMessage msg;
    msg.verb = Puttag;
    msg.msg.tag.tag_character = '\0';
    publish(termout_topic, &msg);
#endif
}

