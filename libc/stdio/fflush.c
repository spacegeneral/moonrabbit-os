#include <stdio.h>
#include <errno.h>
#include <miniz/miniz.h>

#if defined(__is_libk)
#include <data/vfs.h>
#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

int fflush_pure(FILE *stream) {
    FSServiceMessage request;
    request.service_name = fsservice_fflush;
    request.msg.fflush.stream = *stream;
#if defined(__is_libk)
    internalfs_service_handler((void*)&request, 0, 0);
#else
    make_fsservice_request(stream->driver, &request);
#endif
    errno = request.status;
    return request.status;
}


static int tinfl_put_buf_func(const void* pBuf, int len, void *pUser) {
  return len == (int) fwrite_pure(pBuf, 1, len, (FILE*)pUser);
}

int fflush_zlib(FILE *stream) {
    LibzOpenFileUserData *user_data = stream->interface.user_data;
    
    fseek_pure(stream, 0, SEEK_SET);
    int status = tdefl_compress_mem_to_output(user_data->memstore, user_data->mem_length, &tinfl_put_buf_func, stream, TDEFL_WRITE_ZLIB_HEADER);
    if (!status) return EOF;
    
    fflush_pure(stream);
    
    user_data->dirty = false;
    return 0;
}


int fflush(FILE *stream) {
    
    if (stream == stdin) return 0;
    if (stream == stdout) return 0;
    if (stream == stderr) return 0;
    
    return stream->interface.fflush(stream);
}
