#include <stdio.h>
#include <sys/mq.h>



void file_putc_interface(char c, void *p) {
    if (p!=NULL) {
        FILE *fp = (FILE*) p;
        fputc(c, fp);
    }
    else putchar(c);
}


int fprintf(FILE *fp, const char *fmt, ...) {
    va_list va;
    va_start(va, fmt);
    int ret = fctvprintf(&file_putc_interface, (void*)fp, fmt, va);
    va_end(va);
    return ret;
}

int vfprintf(FILE *fp, const char *fmt, va_list va) {
    int ret = fctvprintf(&file_putc_interface, (void*)fp, fmt, va);
    return ret;
}
