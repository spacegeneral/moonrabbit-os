#include <stdio.h>
#include <stdlib.h>
#include <sys/mq.h>
#include <sys/proc.h>
#include <sys/default_topics.h>

char *stdin_topic = NULL;
char *keycode_topic = NULL;
char *stdout_topic = NULL;
char *termout_topic = NULL;
char *terminfo_topic = NULL;
char *termbitmap_topic = NULL;

char default_stdin_topic[] = DEFAULT_STDIN_TOPIC;
char default_keycode_topic[] = DEFAULT_KEYCODE_TOPIC;
char default_stdout_topic[] = DEFAULT_STDOUT_TOPIC;
char default_termout_topic[] = DEFAULT_TERMOUT_TOPIC;
char default_terminfo_topic[] = DEFAULT_TERMINFO_SERVICE;
char default_termbitmap_topic[] = DEFAULT_TERM_DRAWBITMAP_SERVICE;

#define APPLY_IO_OVERRIDE(iotopic) \
{ \
    snprintf(topicname, 4096, "%s_override", #iotopic); \
    char *topic_param = getenv(topicname); \
    if (topic_param != NULL) { \
        char *topic_param_cpy = (char*) malloc(sizeof(char) * (strlen(topic_param) + 1)); \
        snprintf(topic_param_cpy, strlen(topic_param) + 1, "%s", topic_param); \
        iotopic##_topic = topic_param_cpy; \
    } \
}

void setup_stdio(void) {
    char topicname[4096];
    
    stdin_topic = default_stdin_topic;
    keycode_topic = default_keycode_topic;
    stdout_topic = default_stdout_topic;
    termout_topic = default_termout_topic;
    terminfo_topic = default_terminfo_topic;
    termbitmap_topic = default_termbitmap_topic;
    
    APPLY_IO_OVERRIDE(terminfo);
    APPLY_IO_OVERRIDE(stdin);
    APPLY_IO_OVERRIDE(keycode);
    APPLY_IO_OVERRIDE(stdout);
    APPLY_IO_OVERRIDE(termout);
    APPLY_IO_OVERRIDE(termbitmap);
    
}

void teardown_stdio(void) {
}
