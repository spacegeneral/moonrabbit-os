#include <unicode.h>
#include <sys/term.h>


int process_codepoint(int flags, int codepoint, size_t top_codepoint_range) {
    int newcodepoint = codepoint;
    
    if ((flags & TERM_FLAG_BOLD) && (flags & TERM_FLAG_ITALICS)) {
        if (codepoint >= 'A' && codepoint <= 'Z') newcodepoint += (0x1D468 - 'A');
        else if (codepoint >= 'a' && codepoint <= 'z') newcodepoint += (0x1D482 - 'a');
        else if (codepoint >= '0' && codepoint <= '9') newcodepoint += (0x1D7CE - '0');
    }
    if ((flags & TERM_FLAG_BOLD) && (flags & TERM_FLAG_GOTHIC)) {
        if (codepoint >= 'A' && codepoint <= 'Z') newcodepoint += (0x1D57C - 'A');
        else if (codepoint >= 'a' && codepoint <= 'z') newcodepoint += (0x1D586 - 'a');
    }
    if ((flags & TERM_FLAG_BOLD) && (flags & TERM_FLAG_SCRIPT)) {
        if (codepoint >= 'A' && codepoint <= 'Z') newcodepoint += (0x1D4D0 - 'A');
        else if (codepoint >= 'a' && codepoint <= 'z') newcodepoint += (0x1D4EA - 'a');
    }
    else if (flags & TERM_FLAG_BOLD) {
        if (codepoint >= 'A' && codepoint <= 'Z') newcodepoint += (0x1D400 - 'A');
        else if (codepoint >= 'a' && codepoint <= 'z') newcodepoint += (0x1D41A - 'a');
        else if (codepoint >= '0' && codepoint <= '9') newcodepoint += (0x1D7CE - '0');
    }
    else if (flags & TERM_FLAG_ITALICS) {
        if (codepoint == 'h') newcodepoint = 0x210E;
        else if (codepoint >= 'A' && codepoint <= 'Z') newcodepoint += (0x1D434 - 'A');
        else if (codepoint >= 'a' && codepoint <= 'z') newcodepoint += (0x1D44E - 'a');
    }
    else if (flags & TERM_FLAG_GOTHIC) {
        if (codepoint == 'C') newcodepoint = 0x212D;
        else if (codepoint == 'H') newcodepoint = 0x210C;
        else if (codepoint == 'I') newcodepoint = 0x2111;
        else if (codepoint == 'R') newcodepoint = 0x211C;
        else if (codepoint == 'Z') newcodepoint = 0x2128;
        else if (codepoint == 'H') newcodepoint = 0x210C;
        else if (codepoint == 'H') newcodepoint = 0x210C;
        else if (codepoint == 'H') newcodepoint = 0x210C;
        else if (codepoint >= 'A' && codepoint <= 'Z') newcodepoint += (0x1D504 - 'A');
        else if (codepoint >= 'a' && codepoint <= 'z') newcodepoint += (0x1D51E - 'a');
    }
    else if (flags & TERM_FLAG_SCRIPT) {
        if (codepoint == 'B') newcodepoint = 0x212C;
        else if (codepoint == 'E') newcodepoint = 0x2130;
        else if (codepoint == 'F') newcodepoint = 0x2131;
        else if (codepoint == 'H') newcodepoint = 0x210B;
        else if (codepoint == 'L') newcodepoint = 0x2112;
        else if (codepoint == 'M') newcodepoint = 0x2133;
        else if (codepoint == 'R') newcodepoint = 0x211B;
        else if (codepoint == 'e') newcodepoint = 0x212F;
        else if (codepoint == 'g') newcodepoint = 0x210A;
        else if (codepoint == 'o') newcodepoint = 0x2134;       
        else if (codepoint >= 'A' && codepoint <= 'Z') newcodepoint += (0x1D49C - 'A');
        else if (codepoint >= 'a' && codepoint <= 'z') newcodepoint += (0x1D4B6 - 'a');
    }
    
    if ((unsigned)newcodepoint >= top_codepoint_range) newcodepoint = codepoint;
    
    return newcodepoint;
}