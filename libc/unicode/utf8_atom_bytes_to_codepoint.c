#include <unicode.h>
#include <sys/keycodes.h>


int utf8_atom_bytes_to_codepoint(char *atom) {
    Utf8Decoder decoder;
    utf8_decoder_init(&decoder);
    int codepoint = -1;
    for (size_t c=0; c<UTF8_BYTES_ATOM_LENGTH; c++) {
        codepoint = utf8_decoder_feed(&decoder, atom[c]);
        if (codepoint >= 0) break;
    }
    return codepoint;
}
