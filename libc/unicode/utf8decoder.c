/* utf8_decode.c */

/* 2016-04-05 */

/*
Copyright (c) 2005 JSON.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <utf8decoder.h>
#include <stdbool.h>
#include <stddef.h>

/*
    Very Strict UTF-8 Decoder

    UTF-8 is a multibyte character encoding of Unicode. A character can be
    represented by 1-4 bytes. The bit pattern of the first byte indicates the
    number of continuation bytes.

    Most UTF-8 decoders tend to be lenient, attempting to recover as much
    information as possible, even from badly encoded input. This UTF-8
    decoder is not lenient. It will reject input which does not include
    proper continuation bytes. It will reject aliases (or suboptimal
    codings). It will reject surrogates. (Surrogate encoding should only be
    used with UTF-16.)

    Code     Contination Minimum Maximum
    0xxxxxxx           0       0     127
    10xxxxxx       error
    110xxxxx           1     128    2047
    1110xxxx           2    2048   65535 excluding 55296 - 57343
    11110xxx           3   65536 1114111
    11111xxx       error
*/



/*
    Get the next byte. It returns UTF8_END if there are no more bytes.
*/
static int get(Utf8Decoder *decoder) {
    int c;
    if (decoder->index >= decoder->length) {
        return UTF8_END;
    }
    c = (int) decoder->buffer[decoder->index];
    decoder->index += 1;
    return c;
}


/*
    Get the 6-bit payload of the next continuation byte.
    Return UTF8_ERROR if it is not a contination byte.
*/
static int cont(Utf8Decoder *decoder) {
    int c = get(decoder);
    return ((c & 0xC0) == 0x80)
        ? (c & 0x3F)
        : UTF8_ERROR;
}


/*
    Extract the next character.
    Returns: the character (between 0 and 1114111)
         or  UTF8_END   (the end)
         or  UTF8_ERROR (error)
*/
int utf8_decoder_next(Utf8Decoder *decoder) {
    int c;  /* the first byte of the character */
    int c1; /* the first continuation character */
    int c2; /* the second continuation character */
    int c3; /* the third continuation character */
    int r;  /* the result */

    if (decoder->index >= decoder->length) {
        return decoder->index == decoder->length ? UTF8_END : UTF8_ERROR;
    }
    c = get(decoder);
/*
    Zero continuation (0 to 127)
*/
    if ((c & 0x80) == 0) {
        return c;
    }
/*
    One continuation (128 to 2047)
*/
    if ((c & 0xE0) == 0xC0) {
        c1 = cont(decoder);
        if (c1 >= 0) {
            r = ((c & 0x1F) << 6) | c1;
            if (r >= 128) {
                return r;
            }
        }

/*
    Two continuations (2048 to 55295 and 57344 to 65535)
*/
    } else if ((c & 0xF0) == 0xE0) {
        c1 = cont(decoder);
        c2 = cont(decoder);
        if ((c1 | c2) >= 0) {
            r = ((c & 0x0F) << 12) | (c1 << 6) | c2;
            if (r >= 2048 && (r < 55296 || r > 57343)) {
                return r;
            }
        }

/*
    Three continuations (65536 to 1114111)
*/
    } else if ((c & 0xF8) == 0xF0) {
        c1 = cont(decoder);
        c2 = cont(decoder);
        c3 = cont(decoder);
        if ((c1 | c2 | c3) >= 0) {
            r = ((c & 0x07) << 18) | (c1 << 12) | (c2 << 6) | c3;
            if (r >= 65536 && r <= 1114111) {
                return r;
            }
        }
    }
    return UTF8_ERROR;
}


/*
    Initialize the UTF-8 decoder.
*/
void utf8_decoder_init(Utf8Decoder *decoder) {
    decoder->index = 0;
    decoder->length = 0;
}

int utf8_decoder_feed(Utf8Decoder *decoder, char c) {
    decoder->buffer[decoder->length] = c;
    decoder->buffer[decoder->length + 1] = '\0';
    decoder->length++;
    decoder->index = 0;
    
    int success;
    bool decoded = false;
    
    while (!decoded) {
        success = utf8_decoder_next(decoder);
        
        if (success >= 0) {  // properly decoded
            decoder->index = 0;
            decoder->length = 0;
            decoded = true;
        } else if (decoder->length >= MAX_UTF8_SEQUENCE_LEN) {  // unfixable gibberish: pop the first byte and retry
            decoder->index = 0;
            decoder->length--;
            for (size_t i=0; i<UTF8DECODER_BUFFER_LEN-1; i++) {
                decoder->buffer[i] = decoder->buffer[i+1];
            }
            decoded = false;
        } else {  // partially decoded
            decoded = true;
        }
    }
    return success;
}
