#include <stdlib.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

__attribute__((__noreturn__))
void abort(void) {
#if defined(__is_libk)
    abort_implementation(NULL, 0);
#else
    invoke_syscall(SyscallAbort, NULL);
#endif
    __builtin_unreachable();
}
