#include <stddef.h>

#if defined(__is_libk)
#include <syscall.h>
#else
#include <os.h>
#endif



#if defined(__is_libk)

void free(void *ptr) {
    if (ptr == NULL) return;
    free_implementation(ptr, 0);
}



#else


void free_coarse(void *ptr) {
    if (ptr == NULL) return;
    invoke_syscall(SyscallFree, ptr);
}



#endif
