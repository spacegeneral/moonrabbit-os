#include <stdlib.h>
#include <string.h>


#if defined(__is_libk)

// In kernel space, all dynamic mem operations are coarse

void *rich_realloc(void *ptr, size_t size, char *filename, unsigned line) {
    if (ptr == NULL) {
        return rich_malloc(size, filename, line);
    }
    
    if (size == 0) {
        free(ptr);
        return NULL;
    }
    
    size_t original_size = mlength(ptr);
    size_t copied_size = size;

    void *newptr = rich_malloc(size, filename, line);
    
    if (original_size < size) copied_size = original_size;
    memcpy(newptr, ptr, copied_size);
    free(ptr);

    return newptr;
}


#else

// In user space, coarse dynamic mem operations are provided by libc,
// fine dynamic mem operations are provided by liballoc

void *rich_realloc_coarse(void *ptr, size_t size, char *filename, unsigned line) {
    if (ptr == NULL) {
        return rich_malloc_coarse(size, filename, line);
    }
    
    if (size == 0) {
        free_coarse(ptr);
        return NULL;
    }
    
    size_t original_size = mlength(ptr);
    size_t copied_size = size;

    void *newptr = rich_malloc_coarse(size, filename, line);
    
    if (original_size < size) copied_size = original_size;
    memcpy(newptr, ptr, copied_size);
    free_coarse(ptr);

    return newptr;
}

#endif


/*
void *realloc(void *ptr, size_t size) {
    if (ptr == NULL) {
        return malloc(size);
    }
    
    if (size == 0) {
        free(ptr);
        return NULL;
    }
    
    size_t original_size = mlength(ptr);
    size_t copied_size = size;

    void *newptr = malloc(size);
    
    if (original_size < size) copied_size = original_size;
    memcpy(newptr, ptr, copied_size);
    free(ptr);

    return newptr;
}
*/

