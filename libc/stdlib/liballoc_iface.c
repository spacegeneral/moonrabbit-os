#include <liballoc/liballoc.h>
#include <stdlib.h>

#define PAGE_SIZE 4096

int liballoc_lock() {
    return 0;
}

int liballoc_unlock() {
    return 0;
}

void* liballoc_alloc(size_t pages, char *filename, unsigned linenum) {
    return rich_malloc_coarse(pages * PAGE_SIZE, filename, linenum);
}


int liballoc_free(void* memarea, size_t pages) {
    free_coarse(memarea);
    return 0;
}
