#include <stdlib.h>


void mclean2(void ***ptr_to_2lvl_mem, size_t span) {
    void **_2lvl_mem = *ptr_to_2lvl_mem;
    for (size_t n=0; n<span; n++) {
        free(_2lvl_mem[n]);
    }
    free(_2lvl_mem);
    (*ptr_to_2lvl_mem) = NULL;
}
