#include <stdlib.h>
#include <string.h>
#include <sys/errno.h>



void clone_env(char ***to, char **from) {
    size_t e = 0;
    char *curr_env_var = from[e];
    
    while (curr_env_var != NULL) {
        char *equals = strchr(curr_env_var, '=');
        if (equals == NULL) return;
        
        char *value = equals+1;
        char *name = curr_env_var;
        equals[0] = '\0';
        setenv_r(name, value, 1, to);
        equals[0] = '=';
        
        curr_env_var = from[++e];
    }
}


int unsetenv_r(const char *name, char ***env) {
    size_t dead_index = getenv_index_r(name, env);
    if ((*env)[dead_index] == NULL) return 0;

    free_coarse((*env)[dead_index]);
    
    size_t env_len = getenv_length();
    (*env)[dead_index] = (*env)[env_len-1];
    (*env)[env_len-1] = NULL;
    
    (*env) = (char**) realloc_coarse((*env), env_len * sizeof(char*));
    
    return 0;
}

int setenv_r(const char *name, const char *value, int overwrite, char ***env) {
    char *found = getenv_r(name, env);
    if (found != NULL && !overwrite) return 0;
    if (found != NULL && overwrite) {
        size_t found_index = getenv_index_r(name, env);
        free_coarse((*env)[found_index]);
        
        char *newvar = (char*) malloc_coarse(sizeof(char) * (strlen(name)+2+strlen(value)));

        strcpy(newvar, name);
        newvar[strlen(name)] = '=';
        strcpy(newvar + strlen(name)+1, value);
        newvar[strlen(name)+1+strlen(value)] = '\0';
        
        (*env)[found_index] = newvar;
        return 0;
    }
    if (found == NULL) {
        char *newvar = (char*) malloc_coarse(sizeof(char) * (strlen(name)+2+strlen(value)));
        
        strcpy(newvar, name);
        newvar[strlen(name)] = '=';
        strcpy(newvar + strlen(name)+1, value);
        newvar[strlen(name)+1+strlen(value)] = '\0';
        
        size_t env_len = getenv_length_r(env);
        (*env) = (char**) realloc_coarse((*env), (env_len + 2) * sizeof(char*));
        
        (*env)[env_len] = newvar;
        (*env)[env_len+1] = NULL;
        return 0;
    }
    return -1;
}


int unsetenv(const char *name) {
    return unsetenv_r(name, &environ);
}

int setenv(const char *name, const char *value, int overwrite) {
    return setenv_r(name, value, overwrite, &environ);
}
