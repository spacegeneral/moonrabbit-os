#include <stdlib.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

size_t mlength(void *ptr) {
    SyscallMlengthParam param;
    param.memarea = ptr;
#if defined(__is_libk)
    mlength_implementation(&param, 0);
#else
    invoke_syscall(SyscallMlength, &param);
#endif
    return param.result;
}


