#include <stdlib.h>

static long holdrand;


void srand(unsigned int seed) {
    holdrand = (long)seed;
}


/* This algorithm is mentioned in the ISO C standard, here extended
   for 32 bits.  */
int rand_r(unsigned int *seed) {
    unsigned int next = *seed;
    int result;

    next *= 1103515245;
    next += 12345;
    result = (unsigned int) (next / 65536) % 2048;

    next *= 1103515245;
    next += 12345;
    result <<= 10;
    result ^= (unsigned int) (next / 65536) % 1024;

    next *= 1103515245;
    next += 12345;
    result <<= 10;
    result ^= (unsigned int) (next / 65536) % 1024;

    *seed = next;

    return result;
}


int rand(void) {
    return rand_r((unsigned int*)&holdrand);
}
