#include <stdio.h>
#include <stdlib.h>

void __assert (const char *msg, const char *file, int line) {
    printf("Assertion failed: %s\nIn program %s, line %d\n", msg, file, line);
}
