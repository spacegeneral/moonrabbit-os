#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>


size_t getenv_index_r(const char *name, char ***env) {
    size_t e = 0;
    size_t name_len = strlen(name);
    char *curr_env_var = (*env)[e];
    
    while (curr_env_var != NULL) {
        if (strncmp(curr_env_var, name, name_len) == 0) {
            if (curr_env_var[name_len] == '=') {
                break;
            }
        }
        
        curr_env_var = (*env)[++e];
    }
    return e;
}

size_t getenv_length_r(char ***env) {
    size_t e = 0;
    char *curr_env_var = (*env)[e];
    
    while (curr_env_var != NULL) {
        curr_env_var = (*env)[++e];
    }
    
    return e;
}

char *getenv_r(const char *name, char ***env) {
    size_t name_len = strlen(name);
    
    size_t var_index = getenv_index_r(name, env);
    char *curr_env_var = (*env)[var_index];
    
    if (curr_env_var == NULL) return NULL;
    
    return curr_env_var + name_len + 1;
}


size_t getenv_index(const char *name) {
    return getenv_index_r(name, &environ);
}

size_t getenv_length() {
    return getenv_length_r(&environ);
}

char *getenv(const char *name) {
    return getenv_r(name, &environ);
}


