#include <stdlib.h>
#include <string.h>

double atof(const char *nptr) {
    return strtod(nptr, NULL);
}
