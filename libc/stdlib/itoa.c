#include <stdlib.h>
#include <string.h>


char* itoa(int num, char* str, int base) {
    int i = 0;
    int is_negative = 0;
 
    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0) {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }
 
    // In standard itoa(), negative numbers are handled only with 
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10) {
        is_negative = 1;
        num = -num;
    }
 
    // Process individual digits
    while (num != 0) {
        int rem = num % base;
        str[i++] = ((rem > 9) ? (rem-10) + 'a' : rem + '0');
        num = num/base;
    }
 
    // If number is negative, append '-'
    if (is_negative)
        str[i++] = '-';
 
    str[i] = '\0'; // Append string terminator
 
    // Reverse the string
    strrev(str);
 
    return str;
}


char* ltoa(long num, char* str, int base) {
    int i = 0;
    int is_negative = 0;
 
    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0) {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }
 
    // In standard itoa(), negative numbers are handled only with 
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10) {
        is_negative = 1;
        num = -num;
    }
 
    // Process individual digits
    while (num != 0) {
        long rem = num % base;
        str[i++] = ((rem > 9) ? (rem-10) + 'a' : rem + '0');
        num = num/base;
    }
 
    // If number is negative, append '-'
    if (is_negative)
        str[i++] = '-';
 
    str[i] = '\0'; // Append string terminator
 
    // Reverse the string
    strrev(str);
 
    return str;
}


char* lltoa(long long num, char* str, int base) {
    int i = 0;
    int is_negative = 0;
 
    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0) {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }
 
    // In standard itoa(), negative numbers are handled only with 
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10) {
        is_negative = 1;
        num = -num;
    }
 
    // Process individual digits
    while (num != 0) {
        long long rem = num % base;
        str[i++] = ((rem > 9) ? (rem-10) + 'a' : rem + '0');
        num = num/base;
    }
 
    // If number is negative, append '-'
    if (is_negative)
        str[i++] = '-';
 
    str[i] = '\0'; // Append string terminator
 
    // Reverse the string
    strrev(str);
 
    return str;
}
