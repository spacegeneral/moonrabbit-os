#include <stddef.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif


#if defined(__is_libk)

/*
  In kernel world, all the malloc operations use the raw malloc
  (with 4KB page granularity)
 */
void *rich_malloc(size_t size, char *filename, unsigned line) {
    SyscallMallocParam param;
    param.size = size;
    param.filename = filename;
    param.linenum = line;
    malloc_implementation(&param, 0);
    return param.result;
}

#else

void *rich_malloc_coarse(size_t size, char *filename, unsigned line) {
    SyscallMallocParam param;
    param.size = size;
    param.filename = filename;
    param.linenum = line;
    invoke_syscall(SyscallMalloc, &param);
    return param.result;
}


#endif



/*
void *malloc(size_t size) {
    SyscallMallocParam param;
    param.size = size;
#if defined(__is_libk)
    malloc_implementation(&param, 0);
#else
    invoke_syscall(SyscallMalloc, &param);
#endif
    return param.result;
}
*/
