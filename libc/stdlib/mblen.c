#include <stdlib.h>
#include <stdbool.h>
#include <utf8decoder.h>


static Utf8Decoder _decoder;
static bool _decoder_initialized = false;


int mblen(const char *s, size_t n) {
    if (s == NULL) {
        utf8_decoder_init(&_decoder);
        return 1;
    } else {
        if (!_decoder_initialized) {
            utf8_decoder_init(&_decoder);
            _decoder_initialized = true;
        }
        for (size_t eat=0; eat<n; eat++) {
            int status = utf8_decoder_feed(&_decoder, s[eat]);
            if (status > 0) return eat+1;
            if (status == 0) return 0;
        }
        return -1;
    }
}
