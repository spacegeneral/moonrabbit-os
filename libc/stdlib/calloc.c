#include <stdlib.h>
#include <string.h>


#if defined(__is_libk)

void* rich_calloc(size_t nmemb, size_t size, char *filename, unsigned line) {
    void *p;
    size_t real_size = nmemb * size;
    
    p = rich_malloc(real_size, filename, line);
    memset(p, 0, real_size);
    
    return p;
}

#else

void* rich_calloc_coarse(size_t nmemb, size_t size, char *filename, unsigned line) {
    void *p;
    size_t real_size = nmemb * size;
    
    p = rich_malloc_coarse(real_size, filename, line);
    memset(p, 0, real_size);
    
    return p;
}

#endif

/*
void* calloc(size_t nmemb, size_t size) {
    void *p;
    size_t real_size = nmemb * size;
    
    p = malloc(real_size);
    memset(p, 0, real_size);
    
    return p;
}

*/
