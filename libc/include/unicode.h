#ifndef _UNICODE_H
#define _UNICODE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <utf8decoder.h>
#include <sys/keycodes.h>


#define CSUR 1
#define NOT_COMBINING (-333)


int combining_factor(int codepoint);
size_t strlen_utf8(const char * _s);
int utf8_atom_bytes_to_codepoint(char *atom);
char* utf8_codepoint_to_atom_bytes(unsigned codepoint);
int process_codepoint(int flags, int codepoint, size_t top_codepoint_range);




#endif
