#ifndef _YAKUMO_H
#define _YAKUMO_H

#include <stddef.h>


#define LOG_2_OF_10 3.3219280948


#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })


#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })


#define sgn(x) ((x) >= 0 ? 1 : -1)
#define abs(x) ((x) >= 0 ? x : -(x))
   
   
int spiralize(int index);
int qd_log2(int num);
int qd_log10(int num);


#define clamp_pos(val) (val < 0 ? 0 : val)


#endif 
