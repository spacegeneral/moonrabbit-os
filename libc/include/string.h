#ifndef _STRING_H
#define _STRING_H 1

#include <sys/cdefs.h>

#include <stddef.h>
#include <stdbool.h>
#include <objectstring.h>

#ifdef __cplusplus
extern "C" {
#endif

int memcmp(const void*, const void*, size_t);
void* memcpy(void* __restrict, const void* __restrict, size_t);
#if defined(__is_libk) || defined(__is_kernel)
void* simple_memcpy(void* restrict dstptr, const void* restrict srcptr, size_t size);
#endif
void* memmove(void*, const void*, size_t);
void* memset(void*, int, size_t);
void *memchr(const void *s, int c, size_t n);

size_t strlen(const char*);
char *strrev(char *);
int strcmp(const char*, const char*);
int strncmp(const char *s1, const char *s2, size_t n);
char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, size_t n);
char *strchr(const char *s, int c);
char *strrchr(const char *s, int c);
char *strcat(char *dest, const char *src);

int strcasecmp(const char *s1, const char *s2);
int strncasecmp(const char *s1, const char *s2, size_t n);

char *strstr(const char *haystack, const char *needle);

char *strtok(char *str, const char *delim);
char *strtok_r(char *str, const char *delim, char **saveptr);
size_t strspn(const char *s1, const char *s2);
size_t strcspn(const char *s1, const char *s2);
char *strpbrk(const char *s, const char *accept);

char *strdup(const char *s);

double strtod(const char *nptr, char **endptr);
long strtol(const char *nptr, char **endptr, int base);
long long strtoll(const char *nptr, char **endptr, int base);
unsigned long long strtoull(const char *nptr, char **endptr, int base);
unsigned long strtoul(const char *nptr, char **endptr, register int base);

// nonstandard:
float strtof(const char *nptr, char **endptr);
long double strtold(const char *nptr, char **endptr);

char* rstrip(char*);
size_t strsplit(char*, char, size_t);
char *strsplit_chunk_at(char*, size_t, size_t);

int strwrap(char * s, int w, char *** line_ret, int ** len_ret);

bool endswith(const char* base, const char* str);

#ifdef __cplusplus
}
#endif

#endif
