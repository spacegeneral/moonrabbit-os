#ifndef _SPECIO_H
#define _SPECIO_H 1

#include <sys/cdefs.h>
#include <sys/term.h>
#include <graphics.h>


#ifdef __cplusplus
extern "C" {
#endif

typedef enum DirectVideoCommandVerb {
    DVToggleGrabFB,
} DirectVideoCommandVerb;

#define DVC_TOGGLE_GRAB 1
#define DVC_TOGGLE_UNGRAB 0


int direct_video_command(int verb, int argument, void *result);

#ifdef __cplusplus
}
#endif

#endif
