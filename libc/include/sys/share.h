#ifndef _SYS_SHARE_H
#define _SYS_SHARE_H 1 


#include <sys/fs.h>


#define FOREACH_SHARESERVICE(SHARESERVICE) \
    SHARESERVICE(shareservice_give_area) \
    SHARESERVICE(shareservice_give_file) \
    SHARESERVICE(shareservice_free) \
    
    
    
typedef enum ShareServiceName {
    FOREACH_SHARESERVICE(GENERATE_ENUM)
} ShareServiceName;

static const char __attribute__((unused)) *share_service_name[] = {
    FOREACH_SHARESERVICE(GENERATE_STRING)
};


typedef struct __attribute__((packed)) ShareServiceMessage {
    ShareServiceName verb;
    char resource_name[MAX_PATH_LENGTH];
    unsigned long long size;
    unsigned flags;
    uintptr_t resource_pointer;
    int status;
} ShareServiceMessage;


int get_shared_area(char* name, unsigned long long *size, unsigned flags, uintptr_t* area);
int load_shared_file(char* name,  unsigned long long *size, unsigned flags, uintptr_t* area);
int free_shared_area(char* name);



#define SHARE_FLAG_MUST_EXIST   0b00000001
#define SHARE_FLAG_FORCE_RELOAD 0b00000010



#endif