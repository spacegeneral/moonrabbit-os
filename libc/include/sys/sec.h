#ifndef _SYS_SEC_H
#define _SYS_SEC_H

#include <enumtricks.h>
#include <stdlib.h>
#include <yakumo.h>


/** \defgroup LIBC_SYS_SEC Security clearance levels
 *
 * A series of enums which are used to represent 
 * security clearance levels within the Moonrabbit OS
 * Mandatory Access Control system.
 * 
 * In general, every resource and IdentifiedAgent has a 
 * specific security clearance level described by a tuple
 * (::PhisicalDomainClass, ::DataDomainClass)
 */

/** @{ */


#define NUM_SECURITY_CLEARANCE_LEVELS 9


#define FOREACH_PHISICALDOMAIN(PHISICALDOMAIN) \
    PHISICALDOMAIN(God) \
    PHISICALDOMAIN(Angel) \
    PHISICALDOMAIN(Dragon) \
    PHISICALDOMAIN(Elf) \
    PHISICALDOMAIN(Alien) \
    PHISICALDOMAIN(Human) \
    PHISICALDOMAIN(Dwarf) \
    PHISICALDOMAIN(Cat) \
    PHISICALDOMAIN(Ghost) \
    
    
/** This enum describes the clearance levels governing the interactions
 * between a system IdentifiedAgent and a resource which qualifies as
 * a hardware or software resource, such as a disk partition, a device
 * driver or a service.
 * The phisical domain clearance levels are named after fantasy races,
 * in order to be more easily remembered and compared.
 * (in many instances, the PhisicalDomainClass is called "race attribute" 
 * for simplicity).
 * The clearance levels range from "God" (the most privileged, reserved 
 * for the kernel itself) to "Ghost" (the least privileged)
 */
typedef enum PhisicalDomainClass {
    FOREACH_PHISICALDOMAIN(GENERATE_ENUM)
} PhisicalDomainClass;

/** Human-readable string representation of the various ::PhisicalDomainClass levels
 */
static const char __attribute__((unused)) *phisical_domain_class_name[] = {
    FOREACH_PHISICALDOMAIN(GENERATE_STRING)
};

#define FOREACH_DATADOMAIN(DATADOMAIN) \
    DATADOMAIN(Emperor) \
    DATADOMAIN(King) \
    DATADOMAIN(Princess) \
    DATADOMAIN(Knight) \
    DATADOMAIN(Priest) \
    DATADOMAIN(Citizen) \
    DATADOMAIN(Maid) \
    DATADOMAIN(Prisoner) \
    DATADOMAIN(Slave) \
    
/** This enum describes the clearance levels governing the interactions
 * between a system IdentifiedAgent and a resource which qualifies as
 * confidental data, such as personal documents and sensitive files.
 * The data domain clearance levels are named after fantasy social classes,
 * in order to be more easily remembered and compared.
 * (in many instances, the PhisicalDomainClass is called "class attribute" 
 * for simplicity).
 * The clearance levels range from "Emperor" (the most privileged, reserved
 * for the kernel itself) to "Slave" (the least privileged)
 */
typedef enum DataDomainClass {
    FOREACH_DATADOMAIN(GENERATE_ENUM)
} DataDomainClass;

/** Human-readable string representation of the various ::DataDomainClass levels
 */
static const char __attribute__((unused)) *data_domain_class_name[] = {
    FOREACH_DATADOMAIN(GENERATE_STRING)
};


// also works as a bit flag:
//  0b000 = 0  insecure
//  0b001 = 1  no write up enforced   ->   agents below the resource sec level cannot write to it
//  0b010 = 2  no read up enforced    ->   agents below the resource sec level cannot read it
//  0b011 = 3  fully enforced         ->   agents below the resource sec level cannot read/write
//  0b100 = 4  no data loss           ->   (orthogonal to the r/w policies) prevents situations
//                                         in which writing to a resource (i.e. message queue)
//                                         causes data loss (i.e. ringbuffe overrun)
#define FOREACH_SECPOLICY(SECPOLICY) \
    SECPOLICY(SecInsecure) \
    SECPOLICY(SecNoWriteUp) \
    SECPOLICY(SecNoReadUp) \
    SECPOLICY(SecFullyEnforced) \
    SECPOLICY(SecNoDataLoss) \
    
/** This enum describes a series of policies that control
 * how the clearance level limits are enforced.
 * Only certain resources (such as sistem-wide fifo queues) make use
 * of the security policies feature. 
 * - SecInsecure allows everyone to read and write a resource indistriminately
 * - SecNoWriteUp allow write access to a resource only for the agents having a clearance level
 * equal or higher to that of the resource
 * - SecNoReadUp allow read access to a resource only for the agents having a clearance level
 * equal or higher to that of the resource
 * - SecFullyEnforced is equivalend to SecNoWriteUp|SecNoReadUp, is the default behaviour for
 * all system resources in general
 * - SecNoDataLoss can be set orthogonally to the previous policies (i.e. SecFullyEnforced|SecNoDataLoss),
 * and is used to enable special precautions in order to prevent data loss in certain situations
 * (i.e. writing to a message queue which is already full)
 */
typedef enum SecurityPolicy {
    FOREACH_SECPOLICY(GENERATE_ENUM)
} SecurityPolicy;

/** Human-readable string representation of the various ::SecurityPolicy rules
 */
static const char __attribute__((unused)) *security_policy_name[] = {
    FOREACH_SECPOLICY(GENERATE_STRING)
};

/** This function parses a human readable string into its equivalent ::PhisicalDomainClass
 * enum value.
 * \return an int corresponding to a valid ::PhisicalDomainClass item, or -1 in case of failure
 */
int parse_race(char *race);

/** This function parses a human readable string into its equivalent ::DataDomainClass
 * enum value.
 * \return an int corresponding to a valid ::DataDomainClass item, or -1 in case of failure
 */
int parse_class(char *class);

/** This macro returns the safest (i.e. the most privileged) among two clearance levels
 */
#define safest(a, b) (min((a), (b)))

/** This macro returns the least safe (i.e. the least privileged) among two clearance levels
 */
#define least_safe(a, b) (max((a), (b)))

int negotiate_task_common_clearance(unsigned task1_id, unsigned task2_id, PhisicalDomainClass* race, DataDomainClass* class);

/** @} */

#endif 
