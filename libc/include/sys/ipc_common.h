#ifndef _SYS_IPC_COMMON_H
#define _SYS_IPC_COMMON_H

// Maximum length in bytes of a topic name, including the \0 terminator
#define MAX_TOPIC_LENGTH 4096

#endif
