#ifndef _SYS_MQ_H
#define _SYS_MQ_H

#include <sys/ipc_common.h>
#include <sys/sec.h>


/** \defgroup LIBC_MQ Message Queue libc API
 *
 * Interface which allow userspace application to
 * communicate with each other trough the OS-wide
 * publish/subscribe message queue system
 */

/** @{ */


/** Enum describing the outcome of an operation involving the
 * publish/subscribe message queue system
 */
typedef enum MQStatus {
    MQ_SUCCESS = 0,
    MQ_ALREADY_EXISTS,
    MQ_DOES_NOT_EXIST,
    MQ_NOT_SUBSCRIBED,
    MQ_NO_MESSAGES,
    MQ_AUTH_PROBLEM,
    MQ_QUEUE_FULL,
    MQ_CONTINUES,
    
} MQStatus;


/** Structure containing the description of a fifo-channel
 */
typedef struct ChannelStats {
    char name[MAX_TOPIC_LENGTH];
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    SecurityPolicy policy;
    unsigned int max_queue_length;
    unsigned int message_length;
    
} ChannelStats;


/** This function creates a new sistem-wide fifo channel with the specified
 * topic name (unique), maximum capacity and message length.
 * The security clearance of the new channel is the same as the invoker task,
 * using the SecFullyEnforced policy.
 * \return A ::MQStatus code describing the outcome of the operation
 */
MQStatus advertise_channel(char *topic_name, unsigned int max_queue_length, unsigned int message_length);

/** This function creates a new sistem-wide fifo channel with the specified
 * topic name (unique), maximum capacity and message length.
 * Furthermore, the caller must also specify the desired ::PhisicalDomainClass, ::DataDomainClass
 * and ::SecurityPolicy.
 * \return A ::MQStatus code describing the outcome of the operation
 */
MQStatus advertise_channel_advanced(char *topic_name, 
                                    unsigned int max_queue_length, 
                                    unsigned int message_length,
                                    PhisicalDomainClass phys_race_attrib,
                                    DataDomainClass data_class_attrib, 
                                    SecurityPolicy policy);

/** This function deletes an existing sistem-wide fifo channel 
 * given its unique topic name.
 * \return A ::MQStatus code describing the outcome of the operation
 */
MQStatus retire_channel(char *topic_name);

/** This function subscribes the invoker task to a fifo channel, 
 * given the unique topic name of the channel.
 * \return A ::MQStatus code describing the outcome of the operation
 */
MQStatus subscribe(char *topic_name);

/** This function unsubscribes the invoker task from a fifo channel, 
 * given the unique topic name of the channel.
 * \return A ::MQStatus code describing the outcome of the operation
 */
MQStatus unsubscribe(char *topic_name);

/** This function publishes a new message to a fifo channel,
 * identified by its unique topic name. The caller must only pass a
 * void pointer to the beginning of the message; If the length
 * of the underlying data does not match the message_length of the channel,
 * undefined behaviour may occur.
 * The invoker task does *NOT* need to be subscribed to a channel in order
 * to publish messages to it.
 * \return A ::MQStatus code describing the outcome of the operation
 */
MQStatus publish(char *topic_name, void *message);


MQStatus publish_multi(char *topic_name, void *message, size_t num_messages, size_t *num_written);


/** This function polls a fifo channel for new messages.
 * The fifo channel must be identified using its unique topic name.
 * The invoker task *MUST* be subscribed to the channel.
 * If a new message is indeed available, the content will be copied to
 * the memory area pointed by the parameter message, and the function 
 * will return MQ_SUCCESS.
 * If no new message is present, nothing will be written to the memory
 * area pointed by message, and the function will immediately return
 * MQ_NO_MESSAGES.
 * \return A ::MQStatus code describing the outcome of the operation
 */
MQStatus poll(char *topic_name, void *message);


/** This function fills a caller-provided ChannelStats data structure
 * with information about an existing fifo channel.
 * No authorization is enforced, meaning the stats of every message queue
 * are available to everyone.
 * \return A ::MQStatus code describing the outcome of the operation
 */
MQStatus channel_stats(char *topic_name, ChannelStats *stats);


/** @} */


#endif
