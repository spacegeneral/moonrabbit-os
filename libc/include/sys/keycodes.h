#ifndef _SYS_KEYCODES
#define _SYS_KEYCODES


#include <stdbool.h>


typedef enum keyCode {
    KEY_Escape=0, KEY_Num1, KEY_Num2, KEY_Num3, KEY_Num4, KEY_Num5,         // 0 ->
    KEY_Num6, KEY_Num7, KEY_Num8, KEY_Num9, KEY_Num0, KEY_Minus,            // 6 ->
    KEY_Equals, KEY_Backspace, KEY_Tab, KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T,  // 12 ->
    KEY_Y, KEY_U, KEY_I, KEY_O, KEY_P, KEY_LeftBracket, KEY_RightBracket,   // 20 ->
    KEY_Enter, KEY_LeftControl, KEY_A, KEY_S, KEY_D, KEY_F, KEY_G, KEY_H,   // 27 ->
    KEY_J, KEY_K, KEY_L, KEY_Semicolon, KEY_Apostrophe, KEY_Grave,          // 35 ->
    KEY_LeftShift, KEY_Backslash, KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N, // 41 ->
    KEY_M, KEY_Comma, KEY_Period, KEY_Slash, KEY_RightShift,                // 49 ->
    KEY_PadMultiply, // Also PrintScreen                                    // 54
    KEY_LeftAlt, KEY_Space, KEY_CapsLock, KEY_F1, KEY_F2, KEY_F3,           // 55 ->
    KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10,                // 61 ->
    KEY_NumLock, KEY_ScrollLock,                                            // 68 ->
    KEY_Home, // Also Pad7                                                  // 70
    KEY_Up, // Also Pad8                                                    // 71
    KEY_PageUp, // Also Pad9                                                // 72
    KEY_PadMinus,                                                           // 73
    KEY_Left, // Also Pad4                                                  // 74
    KEY_Pad5,                                                               // 75
    KEY_Right, // Also Pad6                                                 // 76
    KEY_PadPlus,                                                            // 77
    KEY_End, // Also Pad1                                                   // 78
    KEY_Down, // Also Pad2                                                  // 79
    KEY_PageDown, // Also Pad3                                              // 80
    KEY_Insert, // Also Pad0                                                // 81
    KEY_Delete, // Also PadDecimal                                          // 82
    KEY_NonUsBackslash, KEY_F11, KEY_F12, KEY_Pause,                        // 83 ->
    KEY_LeftGui, KEY_RightGui, KEY_Menu,                                    // 87 ->
    Pad1, KEY_Pad4, KEY_Pad7,
    KEY_None
} KeyCode;


#define KEY_PrintScreen KEY_PadMultiply
#define KEY_Pad7 KEY_Home
#define KEY_Pad8 KEY_Up
#define KEY_Pad9 KEY_PageUp
#define KEY_Pad4 KEY_Left
#define KEY_Pad6 KEY_Right
#define KEY_Pad1 KEY_End
#define KEY_Pad2 KEY_Down
#define KEY_Pad3 KEY_PageDown
#define KEY_Pad0 KEY_Insert
#define KEY_PadDecimal KEY_Delete


#define LSHIFT_DOWN_FLAG    0b0000000000000001
#define RSHIFT_DOWN_FLAG    0b0000000000000010
#define LALT_DOWN_FLAG      0b0000000000000100
#define LCTRL_DOWN_FLAG     0b0000000000001000
#define CAPSLOCK_DOWN_FLAG  0b0000000000010000



#define UTF8_BYTES_ATOM_LENGTH 6

typedef struct KeyEvent {
    KeyCode keycode;
    char utf8[UTF8_BYTES_ATOM_LENGTH];
    bool is_pressed;
    unsigned mod_flags;
    unsigned long long int timestamp;
} KeyEvent;


#endif
