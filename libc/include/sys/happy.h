#ifndef _SYS_HAPPY_H
#define _SYS_HAPPY_H 1 

#include <time.h>


#if !defined(__is_libk)

void happy(float happyness);

#endif


typedef struct HappyServiceMessage {
    unsigned int task_id;
    float happyness;
} HappyServiceMessage;


float errf_lin_pos(long error, long dc_limit);
float errf_lin_abs(long error, long dc_limit);


typedef struct FPSController {
    time_t time_mark;
    long target;
    long tolerance;
} FPSController;

FPSController FPSController_init(long target, long tolerance);
long FPSController_step(FPSController* fpscnt);


#define MIN_HAPPYNESS -1.0
#define MAX_HAPPYNESS 1.0


#endif 
