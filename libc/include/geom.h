#ifndef _GEOM_H
#define _GEOM_H

#include <stdbool.h>

typedef struct Rect {
    int x,y,w,h;
} Rect;

typedef struct Point {
    int x,y;
} Point;


Point point_delta(Point a, Point b);
Point point_add(Point a, Point b);

int rect_intersection(Rect a, Rect b, Rect *c);
Rect rect_delta(Rect a, Rect b);
Rect rect_add(Rect a, Rect b);
bool rect_is_zero(Rect a);
bool rect_contains(Rect a, Point p);


#endif
