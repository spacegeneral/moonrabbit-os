#include <yakumo.h>


int spiralize(int index) {
    int modulus = (index + 1) / 2;
    int negative = (index % 2) * -1;
    int positive = ((index + 1) % 2);
    
    return modulus * (positive + negative);
}

int qd_log2(int num) {
    int step = 0;
    while (num != 0) {
        num >>= 1;
        step++;
    }
    return step;
}


int qd_log10(int num) {
    double top = (double) qd_log2(num);
    return (int) (top / LOG_2_OF_10);
}
