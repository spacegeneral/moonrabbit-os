#include <string.h>

char *strpbrk(const char *s, const char *accept) {
    size_t candidates = strlen(accept);
    for (size_t i=0; i<candidates; i++) {
        char* found = strchr(s, accept[i]);
        if (found != NULL) return found;
    }
    return NULL;
}