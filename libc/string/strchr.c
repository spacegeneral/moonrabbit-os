#include <string.h>

char *strchr(const char *s, int c)
{
    while (*s != (char)c)
        if (!*s++)
            return 0;
    return (char *)s;
}

char *strrchr(const char *s, int c)
{
    const char* ret=NULL;
    do {
        if( *s == (char)c )
            ret=s;
    } while(*s++);
    return (char *)ret;
}
