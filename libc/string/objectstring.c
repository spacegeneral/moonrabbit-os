#include <stdlib.h>
#include <string.h>



void OString_init(OString* string) {
    string->capacity = OBJECTSTRING_BUMP_FACTOR;
    string->length = 0;
    string->text = calloc(string->capacity, sizeof(char));
}

void OString_destroy(OString* string) {
    string->capacity = 0;
    string->length = 0;
    free(string->text);
}

void OString_clear(OString* string) {
    string->capacity = OBJECTSTRING_BUMP_FACTOR;
    string->length = 0;
    free(string->text);
    string->text = calloc(string->capacity, sizeof(char));
}

void OString_append(OString* string, char character) {
    if (string->length+2 > string->capacity) {
        string->capacity += OBJECTSTRING_BUMP_FACTOR;
        string->text = realloc(string->text, string->capacity * sizeof(char));
    }
    string->text[string->length] = character;
    string->length++;
    string->text[string->length] = '\0';
}


void OString_extend(OString* string, char* text) {
    size_t newlength = string->length + strlen(text);
    if (newlength + 1 > string->capacity) {
        string->capacity = (((newlength + 1) / OBJECTSTRING_BUMP_FACTOR) + 1) * OBJECTSTRING_BUMP_FACTOR;
        string->text = realloc(string->text, string->capacity * sizeof(char));
    }
    memcpy(string->text + string->length, text, strlen(text));
    string->text[newlength] = '\0';
    string->length = newlength;
}

void OString_eat(OString* string, OString* food) {
    OString_extend(string, food->text);
    OString_destroy(food);
}