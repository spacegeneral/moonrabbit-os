#include <string.h>


bool endswith(const char* base, const char* str) {
    size_t lbase = strlen(base);
    size_t lstr = strlen(str);
    
    if (lbase >= lstr) {
        return (0 == memcmp(str, base + (lbase - lstr), lstr));
    }
    return 0;
}
