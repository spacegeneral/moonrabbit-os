#include <string.h>


char *strcasestr(const char *haystack, const char *needle) {
    const char *p = haystack;
    const size_t len = strlen(needle);

    for (; (p = strchr(p, *needle)) != 0; p++) {
        if (strncasecmp(p, needle, len) == 0) return (char *)p;
    }
    return NULL;
}
