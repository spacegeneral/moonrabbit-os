#include <string.h>




/*
 * Prepare a "splitted" string, by replacing instances of delim with '\0'.
 * str is modified by this function.
 * Returns the number of chunks created.
 * Repetitions of delim are considered as a single delim
 */
size_t strsplit(char *str, char delim, size_t length) {
    size_t chunks = 0;
    char sep = '\0';
    for (size_t c=0; c<length; c++) {
        if (str[c] == delim) {
            str[c] = sep;
            if (str[c+1] != delim) chunks++;
        }
    }
    return chunks+1;
}


char *strsplit_chunk_at(char *str, size_t maxlenght, size_t index) {
    size_t chunk = 0;
    char *start = str;
    char sep = '\0';
    for (size_t c=0; c<maxlenght; c++) {
        if (str[c] == sep) {
            if (index == chunk) break;
            else {
                if (str[c+1] != sep) {
                    chunk++;
                    start = str + c + 1;
                }
            }
        }
    }
    
    return start;
}
