#include <string.h>
#include <stdlib.h>


char *strdup(const char *s) {
    size_t slen = strlen(s);
    char *newstr = (char*) malloc(slen+1);
    if (newstr==NULL) return NULL;
    memcpy(newstr, s, slen+1);
    return newstr;
}

