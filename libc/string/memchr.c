#include <string.h>


void *memchr(const void *s, int c, size_t n) {
    unsigned char target = (unsigned char) c;
    unsigned char *scan = (unsigned char*) s;
    for (size_t p=0; p<n; p++) {
        if (scan[p] == target) return (void*)(scan + p);
    }
    return NULL;
}
