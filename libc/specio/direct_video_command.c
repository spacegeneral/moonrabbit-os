#include <specio.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

int direct_video_command(int verb, int argument, void *result) {
    DirectVideoCommandParam param;
    param.verb = verb;
    param.argument = argument;
    param.result = result;
#if defined(__is_libk)
    direct_video_command_implementation(&param, 0);
#else
    invoke_syscall(DirectVideoCommand, &param);
#endif
    return param.status;
}
