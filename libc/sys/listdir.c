#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#if defined(__is_libk)

#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

int listdir(const char *dirname, DirEnt **entries, size_t *numentries) {
    FSServiceMessage request;
    request.service_name = fsservice_count_directory_entries;
#if defined(__is_libk)
    strncpy(request.msg.count_directory_entries.pathname, dirname, MAX_PATH_LENGTH);
    internalfs_service_handler((void*)&request, 0, 0);
#else
    char drivername[MAX_FS_DRIVER_NAME_LENGTH];
    char explicit_path[MAX_PATH_LENGTH];
    get_mount_info(dirname, drivername, explicit_path);
    strncpy(request.msg.count_directory_entries.pathname, explicit_path, MAX_PATH_LENGTH);
    make_fsservice_request(drivername, &request);
#endif
    
    (*numentries) = request.msg.count_directory_entries.numentries;
    if ((*numentries) == 0) return request.status;
    
    (*entries) = malloc(sizeof(DirEnt) * (*numentries));
    request.service_name = fsservice_get_directory_entry_at;
#if defined(__is_libk)
    strncpy(request.msg.get_directory_entry_at.pathname, dirname, MAX_PATH_LENGTH);
#else
    strncpy(request.msg.count_directory_entries.pathname, explicit_path, MAX_PATH_LENGTH);
#endif
    for (size_t ne=0; ne<(*numentries); ne++) {
        request.msg.get_directory_entry_at.position = ne;
#if defined(__is_libk)
        internalfs_service_handler((void*)&request, 0, 0);
#else
        make_fsservice_request(drivername, &request);
        
#endif
        if (request.status != 0) return request.status;
        memcpy((*entries) + ne, &(request.msg.get_directory_entry_at.entry), sizeof(DirEnt));
    }
    return 0;
}
