#include <sys/proc.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

void expirecc(void* resume, unsigned long long expire_ms) {
    SyscallExpireCCParam param;
    param.resume = (uintptr_t)resume;
    param.expire = expire_ms;
#if defined(__is_libk)
    expirecc_implementation(&param, 0);
#else
    invoke_syscall(SyscallExpireCC, &param);
#endif
}

void continuation() {
#if defined(__is_libk)
    continuation_implementation(NULL, 0);
#else
    invoke_syscall(SyscallContinuation, NULL);
#endif
}