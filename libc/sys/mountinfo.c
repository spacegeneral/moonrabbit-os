#include <stdio.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

int get_mount_info(const char *pathname, char *additional_info, char *explicit_pathname) {
    SyscallGetMountInfoParam param;
    param.additional = additional_info;
    param.pathname = (char*)pathname;
    param.explicit = explicit_pathname;
#if defined(__is_libk)
    get_mount_info_implementation(&param, 0);
#else
    invoke_syscall(SyscallGetMountInfo, &param);
#endif
    return param.result;
}
