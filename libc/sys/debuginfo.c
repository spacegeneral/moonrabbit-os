#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/debuginfo.h>



void TaskSection_append_new(Array *toarray, const char* name, uintptr_t start, size_t length) {
    TaskSection *section = malloc_coarse(sizeof(TaskSection));
    snprintf(section->name, MAX_SECTIONNAME_LENGTH, "%s", name);
    section->start = start;
    section->length = length;
    array_add(toarray, section);
}



void TaskDebuginfo_destroy(TaskDebuginfo *debug_info) {
    if (debug_info->valid) {
        free_coarse(debug_info->mem_map);
    }
    
    debug_info->valid = false;
}


void TaskDebuginfo_append_mem_section(TaskDebuginfo *debug_info, const char *name, uintptr_t start, size_t length) {
    if (!debug_info->valid) return;
    
    debug_info->mem_map_length++;
    debug_info->mem_map = realloc_coarse(debug_info->mem_map, debug_info->mem_map_length * sizeof(TaskSection));
    
    strncpy(debug_info->mem_map[debug_info->mem_map_length-1].name, name, MAX_SECTIONNAME_LENGTH);
    debug_info->mem_map[debug_info->mem_map_length-1].start = start;
    debug_info->mem_map[debug_info->mem_map_length-1].length = length;
}


int TaskDebuginfo_find_section_by_start(TaskDebuginfo *debug_info, uintptr_t start) {
    for (unsigned e=0; e<debug_info->mem_map_length; e++) {
        if (debug_info->mem_map[e].start == start) return e;
    }
    return -1;
}


void TaskDebuginfo_remove_section_by_start(TaskDebuginfo *debug_info, uintptr_t start) {
    int section_id = TaskDebuginfo_find_section_by_start(debug_info, start);
    if (section_id < 0) return;
    for (size_t e=section_id; e<debug_info->mem_map_length-1; e++) {
        debug_info->mem_map[e] = debug_info->mem_map[e+1];
    }
    debug_info->mem_map_length--;
    debug_info->mem_map = realloc_coarse(debug_info->mem_map, debug_info->mem_map_length * sizeof(TaskSection));
}

void TaskDebuginfo_clone(TaskDebuginfo **clone, TaskDebuginfo *source) {
    *clone = malloc_coarse(sizeof(TaskDebuginfo));
    (*clone)->valid = source->valid;
    (*clone)->mem_map = malloc_coarse(source->mem_map_length * sizeof(TaskSection));
    (*clone)->mem_map_length = source->mem_map_length;
    memcpy((*clone)->mem_map, source->mem_map, source->mem_map_length * sizeof(TaskSection));
    (*clone)->stabs = source->stabs;
    (*clone)->stabstr = source->stabstr;
    (*clone)->num_stab_symbols = source->num_stab_symbols;
}
