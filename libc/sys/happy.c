#include <sys/happy.h>
#include <sys/mq.h>
#include <sys/proc.h>
#include <sys/default_topics.h>
#include <yakumo.h>
#include <stdio.h>



#if !defined(__is_libk)

void happy(float happyness) {
    HappyServiceMessage msg;
    ProcIdent identity = ident();
    msg.task_id = identity.taskid;
    if (happyness < MIN_HAPPYNESS) happyness = MIN_HAPPYNESS;
    else if (happyness > MAX_HAPPYNESS) happyness = MAX_HAPPYNESS;
    msg.happyness = happyness;
    publish(DEFAULT_HAPPY_TOPIC, &msg);
}

#endif


float errf_lin_pos(long error, long dc_limit) {
    if (error <= 0) return MAX_HAPPYNESS;
    else if (error >= dc_limit) return MIN_HAPPYNESS;
    else {
        float slope = (MIN_HAPPYNESS - MAX_HAPPYNESS) / ((float)dc_limit);
        return (((float)error) * slope) + MAX_HAPPYNESS;
    }
}

float errf_lin_abs(long error, long dc_limit) {
    return errf_lin_pos(abs(error), dc_limit);
}


FPSController FPSController_init(long target, long tolerance) {
    long target_period = 1000 / target;
    FPSController new = {
        .time_mark = clock_ms(),
        .target = target_period,
        .tolerance = target_period + (1000 / tolerance)
    };
    return new;
}

long FPSController_step(FPSController* fpscnt) {
    time_t now = clock_ms();
    long delta_t = (long)(now - fpscnt->time_mark);
    fpscnt->time_mark = now;
    long late_time = delta_t - fpscnt->target;
    happy(errf_lin_pos(late_time, fpscnt->tolerance));
    return late_time;
}

