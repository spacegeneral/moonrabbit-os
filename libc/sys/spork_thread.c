#include <sys/proc.h>
#include <sys/mq.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


int spork_thread(void *thread_entry_point) {
    unsigned int child_task_id;
    int status = clone(thread_entry_point, &child_task_id);
    if ((status < 0) && (status != READY_TO_RUN)) return status;
    
    int dstatus = disown(child_task_id);
    MQStatus qstatus = publish(DEFAULT_TASK_ADOPTION_TOPIC, &child_task_id);
    
    if ((qstatus != MQ_SUCCESS) || (dstatus != 0)) {
        printf("Warning: cannot put child process up for adoption.\n");
        return child_task_id;
    } else {
        return child_task_id;
    }
    
}
