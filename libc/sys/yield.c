#include <sys/proc.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

int yield() {
    int result;
#if defined(__is_libk)
    yield_implementation(&result, 0);
#else
    invoke_syscall(SyscallYield, &result);
#endif
    return result;
}

int task_switch(unsigned int taskid) {
    SyscallTaskSwitchParam param;
    param.newtask = taskid;
#if defined(__is_libk)
    taskswitch_implementation(&param, 0);
#else
    invoke_syscall(SyscallTaskSwitch, &param);
#endif
    return param.result;
}
