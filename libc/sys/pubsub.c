#include <sys/mq.h>
#include <sys/proc.h>
#include <os.h>
#include <stdlib.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

MQStatus advertise_channel_advanced(char *topic_name, 
                                    unsigned int max_queue_length, 
                                    unsigned int message_length,
                                    PhisicalDomainClass phys_race_attrib,
                                    DataDomainClass data_class_attrib, 
                                    SecurityPolicy policy) {
    SyscallAdvertiseChannelParam param;
    param.topic_name = topic_name;
    param.max_queue_length = max_queue_length;
    param.message_length = message_length;
    param.phys_race_attrib = phys_race_attrib;
    param.data_class_attrib = data_class_attrib;
    param.policy = policy;
#if defined(__is_libk)
    advertise_channel_implementation(&param, 0);
#else
    invoke_syscall(SyscallAdvertiseChannel, &param);
#endif
    return param.result;
}

MQStatus advertise_channel(char *topic_name, 
                           unsigned int max_queue_length, 
                           unsigned int message_length) {
    ProcIdent my_ident = ident();
    return advertise_channel_advanced(topic_name, max_queue_length, message_length, my_ident.phys_race_attrib, my_ident.data_class_attrib, SecFullyEnforced);
}

MQStatus retire_channel(char *topic_name) {
    SyscallTopicParam param;
    param.topic_name = topic_name;
#if defined(__is_libk)
    retire_channel_implementation(&param, 0);
#else
    invoke_syscall(SyscallRetireChannel, &param);
#endif
    return param.result;
}

MQStatus subscribe(char *topic_name) {
    SyscallTopicParam param;
    param.topic_name = topic_name;
#if defined(__is_libk)
    subscribe_implementation(&param, 0);
#else
    invoke_syscall(SyscallSubscribe, &param);
#endif
    return param.result;
}

MQStatus unsubscribe(char *topic_name) {
    SyscallTopicParam param;
    param.topic_name = topic_name;
#if defined(__is_libk)
    unsubscribe_implementation(&param, 0);
#else
    invoke_syscall(SyscallUnsubscribe, &param);
#endif
    return param.result;
}

static MQStatus inner_publish(char *topic_name, void *message) {
    SyscallPubPollParam param;
    param.topic_name = topic_name;
    param.message = message;
#if defined(__is_libk)
    publish_implementation(&param, 0);
#else
    invoke_syscall(SyscallPublish, &param);
#endif
    return param.result;
}

#if defined(__is_libk)
MQStatus publish(char *topic_name, void *message) {
    return inner_publish(topic_name, message);
}
#else
MQStatus publish(char *topic_name, void *message) {
    MQStatus status;
    size_t max_backoff = 10;  // if the queue is full, retry at most N times
    do {
        status = inner_publish(topic_name, message);
        if (status == MQ_QUEUE_FULL) {
            max_backoff--;
            yield();
        }
    } while ((status == MQ_QUEUE_FULL) && (max_backoff != 0));
    return status;
}
#endif


MQStatus publish_multi(char *topic_name, void *message, size_t num_messages, size_t *num_written) {
    SyscallPublishMultiParam param;
    param.topic_name = topic_name;
    param.message = message;
    param.num_messages = num_messages;
    param.num_written = num_written;
#if defined(__is_libk)
    publish_multi_implementation(&param, 0);
#else
    invoke_syscall(SyscallPublishMulti, &param);
#endif
    return param.result;
}


MQStatus poll(char *topic_name, void *message) {
    SyscallPubPollParam param;
    param.topic_name = topic_name;
    param.message = message;
#if defined(__is_libk)
    poll_implementation(&param, 0);
#else
    invoke_syscall(SyscallPoll, &param);
#endif
    return param.result;
}

MQStatus channel_stats(char *topic_name, ChannelStats *stats) {
    SyscallChannelStatsParam param;
    param.topic_name = topic_name;
    param.stats = stats;
    #if defined(__is_libk)
    channel_stats_implementation(&param, 0);
#else
    invoke_syscall(SyscallChannelStats, &param);
#endif
    return param.result;
}
