#include <sys/proc.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>



int spoon(const char *filename,
          PhisicalDomainClass phys_race_attrib,
          DataDomainClass data_class_attrib) {
    unsigned int child_task_id;
    volatile int return_code;
    int status = prelaunch(filename, 
                           phys_race_attrib, data_class_attrib,
                           &child_task_id, 
                           (int*)&return_code);
    if ((status < 0) && (status != READY_TO_RUN)) return status;
    
    while (true) {
        int swapstatus = task_switch(child_task_id);
        if (swapstatus != 0) {
            return (int)return_code;
        }
        if (return_code == NOT_DONE_YET) {
            yield();
        } else {
            return (int)return_code;
        }
    }
}

int epspoon(const char *filename) {
    ProcIdent my_ident = ident();
    return spoon(filename, my_ident.phys_race_attrib, my_ident.data_class_attrib);
}
