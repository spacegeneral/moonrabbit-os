#include <stdlib.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif


int scan_media() {
    int result;
#if defined(__is_libk)
    scan_media_implementation(&result, 0);
#else
    invoke_syscall(SyscallScanMedia, &result);
#endif
    return result;
}
