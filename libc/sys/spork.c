#include <sys/proc.h>
#include <sys/mq.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


int spork(const char *filename,
          PhisicalDomainClass phys_race_attrib,
          DataDomainClass data_class_attrib, 
          int *return_code_here) {
    unsigned int child_task_id;
    int status = prelaunch(filename, 
                           phys_race_attrib, data_class_attrib,
                           &child_task_id, 
                           return_code_here);
    if ((status < 0) && (status != READY_TO_RUN)) return status;
    
    int dstatus = disown(child_task_id);
    MQStatus qstatus = publish(DEFAULT_TASK_ADOPTION_TOPIC, &child_task_id);
    
    if ((qstatus != MQ_SUCCESS) || (dstatus != 0)) {
        printf("Warning: cannot put child process up for adoption. Falling back to spoon() behavior.\n");
        while (true) {
            int swapstatus = task_switch(child_task_id);
            if (swapstatus != 0) {
                return *return_code_here;
            }
            if ((*return_code_here) == NOT_DONE_YET) {
                yield();
            } else {
                return *return_code_here;
            }
        }
    } else {
        return child_task_id;
    }
    
}

int epspork(const char *filename, int *return_code_here) {
    ProcIdent my_ident = ident();
    return spork(filename, my_ident.phys_race_attrib, my_ident.data_class_attrib, return_code_here);
}
