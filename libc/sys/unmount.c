#include <stdlib.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#else
#include <stdio.h>
#endif


int pure_unmount(char *mountpoint, char *drivertopic) {
    SyscallUnMountParam param;
    param.mountpoint = mountpoint;
    param.drivertopic = drivertopic;
#if defined(__is_libk)
    unmount_implementation(&param, 0);
#else
    invoke_syscall(SyscallUnMount, &param);
#endif
    return param.result;
}

int unmount(char *mountpoint) {
    char drivertopic[MAX_TOPIC_LENGTH];
    int unmount_result = pure_unmount(mountpoint, drivertopic);
    if (unmount_result) return unmount_result;
    
#if defined(__is_libk)
    return 0;
#else
    FSServiceMessage request;
    request.service_name = fsservice_unsetmount;
    
    strncpy(request.msg.unsetmount.mountpoint, mountpoint, MAX_PATH_LENGTH);
    
    make_fsservice_request(drivertopic, &request);
    
    return request.status;
#endif
}
