#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <sys/loader.h>




unsigned char *userland_loader(const char* filename) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        return NULL;
    }
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    unsigned char *full_mem = (unsigned char*) malloc_coarse(sizeof(unsigned char) * (filesize));
    size_t nread = fread(full_mem, 1, filesize, fp);
    if (nread != (size_t)filesize) {
        fclose(fp);
        free_coarse(full_mem);
        return NULL;
    }
    
    fclose(fp);
    return full_mem;
}

