#include <stdlib.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif


int gift_memarea(void* memarea, unsigned int new_owner_id) {
    SyscallGiftMemareaParam param;
    param.memarea = memarea;
    param.new_owner_id = new_owner_id;
    param.result = 0;
#if defined(__is_libk)
    gift_memarea_implementation(&param, 0);
#else
    invoke_syscall(SyscallGiftMemarea, &param);
#endif
    return param.result;
}
