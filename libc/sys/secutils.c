#include <sys/sec.h>
#include <sys/proc.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>


int parse_race(char *race) {
    for (int c=0; c<NUM_SECURITY_CLEARANCE_LEVELS; c++) {
        if (strcmp(race, phisical_domain_class_name[c]) == 0) return c;
    }
    return -1;
}

int parse_class(char *class) {
    for (int c=0; c<NUM_SECURITY_CLEARANCE_LEVELS; c++) {
        if (strcmp(class, data_domain_class_name[c]) == 0) return c;
    }
    return -1;
}

int negotiate_task_common_clearance(unsigned task1_id, unsigned task2_id, PhisicalDomainClass* race, DataDomainClass* class) {
    FILE* fp = fopen("f,sys,dial,robot,tasks", "rb");
    if (fp == NULL) return FILE_DOES_NOT_EXIST;
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    TaskInfos* infos = (TaskInfos*) malloc(filesize);
    fread(infos, 1, filesize, fp);
    fclose(fp);
    
    PhisicalDomainClass race1, race2;
    DataDomainClass class1, class2;
    int num_found = 0;
    
    size_t num_tasks = filesize / sizeof(TaskInfos);
    for (size_t t=0; t<num_tasks; t++) {
        if (infos[t].task_id == task1_id) {
            num_found++;
            race1 = infos[t].race;
            class1 = infos[t].class;
        }
        if (infos[t].task_id == task2_id) {
            num_found++;
            race2 = infos[t].race;
            class2 = infos[t].class;
        }
        if (num_found >= 2) break;
    }
    
    (*class) = least_safe(class1, class2);
    (*race) = least_safe(race1, race2);
    
    free(infos);
    return 0;
}