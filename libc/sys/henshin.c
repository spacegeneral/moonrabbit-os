#include <sys/proc.h>
#include <os.h>
#include <stdlib.h>


#if !defined(__is_libk)

void henshin(unsigned char* ready_elf, uintptr_t entrypoint, TaskDebuginfo *dbginfo, char *program_name) {
    SyscallHenshinParam param;
    param.ready_elf = ready_elf;
    param.entrypoint = entrypoint;
    param.dbginfo = dbginfo;
    param.program_name = program_name;
    param.copy_env = environ;
    invoke_syscall(SyscallHenshin, &param);
    // if everything went OK, unreachable
} 


#endif
