#include <stdio.h>
#include <errno.h>

#if defined(__is_libk)

#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

bool file_exists(const char *pathname) {
    FSServiceMessage request;
    request.service_name = fsservice_file_exists;
#if defined(__is_libk)
    strncpy(request.msg.file_exists.pathname, pathname, MAX_PATH_LENGTH);
    internalfs_service_handler((void*)&request, 0, 0);
#else
    char drivername[MAX_FS_DRIVER_NAME_LENGTH];
    char explicit_path[MAX_PATH_LENGTH];
    get_mount_info(pathname, drivername, explicit_path);
    strncpy(request.msg.file_exists.pathname, explicit_path, MAX_PATH_LENGTH);
    make_fsservice_request(drivername, &request);
#endif
    errno = request.status;
    return request.msg.file_exists.result;
}
