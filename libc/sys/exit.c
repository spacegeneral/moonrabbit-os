#include <sys/proc.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

void exit(int code) {
#if defined(__is_libk)
    exit_implementation(code, 0);
#else
    invoke_syscall(SyscallExit, (void*)code);
#endif
    __builtin_unreachable();
}
