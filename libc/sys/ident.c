#include <sys/proc.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

ProcIdent ident() {
    SyscallIdentParam param;
#if defined(__is_libk)
    ident_implementation(&param, 0);
#else
    invoke_syscall(SyscallIdent, &param);
#endif
    return param.result;
}
