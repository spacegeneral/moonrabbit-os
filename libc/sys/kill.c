#include <sys/proc.h>
#include <os.h>
#include <stdlib.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

int kill(unsigned int taskid) {
    SyscallKillParam param;
    param.task_id = taskid;
#if defined(__is_libk)
    kill_implementation(&param, 0);
#else
    invoke_syscall(SyscallKill, &param);
#endif
    return param.result;
}
