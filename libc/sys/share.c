#include <stdlib.h>
#include <sys/share.h>
#include <sys/default_topics.h>
#include <sys/serv.h>
#include <errno.h>


#define SHARE_SERV_REQ_ERRORCHECK() if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) return(60 + reqstatus);
#define SHARE_SERV_POLL_ERRORCHECK() if (reqstatus == SERV_DOES_NOT_EXIST || reqstatus == SERV_AUTH_PROBLEM) return(70 + reqstatus);


#define SHARE_SERV_TIMEOUT 5000


static int make_shareservice_request(char* topic, void* request) {
    time_t start;
    unsigned int request_id;
    ServStatus reqstatus;
    // send request
    start = clock_ms();
    do {
        reqstatus = service_request(topic, request, &request_id);
        SHARE_SERV_REQ_ERRORCHECK();
        if (clock_ms() - start > SHARE_SERV_TIMEOUT) return SHARE_SERVICE_TIMEOUT;
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    // await result
    start = clock_ms();
    do {
        reqstatus = service_poll(topic, request_id);
        SHARE_SERV_POLL_ERRORCHECK();
        if (clock_ms() - start > SHARE_SERV_TIMEOUT) return SHARE_SERVICE_TIMEOUT;
        if (reqstatus != SERV_SUCCESS) yield();
    } while (reqstatus != SERV_SUCCESS);
    
    return 0;
}


int get_shared_area(char* name, unsigned long long *size, unsigned flags, uintptr_t* area) {
#if defined(__is_libk)
    return NOT_IMPLEMENTED;
#else
    ShareServiceMessage request;
    request.verb = shareservice_give_area;
    request.flags = flags;
    request.size = *size;
    strncpy(request.resource_name, name, MAX_PATH_LENGTH);
    int status = make_shareservice_request(DEFAULT_SHARE_SERVICE, &request);
    if (status != 0) {
        errno = status;
        return status;
    }
    (*area) = request.resource_pointer;
    (*size) = request.size;
    errno = request.status;
    return request.status;
#endif
}

int load_shared_file(char* name, unsigned long long *size, unsigned flags, uintptr_t* area) {
#if defined(__is_libk)
    return NOT_IMPLEMENTED;
#else
    ShareServiceMessage request;
    request.verb = shareservice_give_file;
    request.flags = flags;
    request.size = *size;
    strncpy(request.resource_name, name, MAX_PATH_LENGTH);
    int status = make_shareservice_request(DEFAULT_SHARE_SERVICE, &request);
    if (status != 0) {
        errno = status;
        return status;
    }
    (*area) = request.resource_pointer;
    (*size) = request.size;
    errno = request.status;
    return request.status;
#endif
}

int free_shared_area(char* name) {
#if defined(__is_libk)
    return NOT_IMPLEMENTED;
#else
    ShareServiceMessage request;
    request.verb = shareservice_free;
    strncpy(request.resource_name, name, MAX_PATH_LENGTH);
    int status = make_shareservice_request(DEFAULT_SHARE_SERVICE, &request);
    if (status != 0) {
        errno = status;
        return status;
    }
    errno = request.status;
    return request.status;
#endif
}