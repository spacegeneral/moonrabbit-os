#include <sys/audio.h>
#include <sys/default_topics.h>

#if defined(__is_libk)

#include <audio/beeper.h>

void beep(unsigned frequency, unsigned long long int duration) {
    system_beep((uint16_t)frequency, duration);
}

#define hifi_beep(frequency, duration) beep((frequency), (duration))

#else

#include <time.h>
#include <sys/mq.h>

void beep(unsigned frequency, unsigned long long int duration) {
    AudioEventMessage msg_on = {
        .frequency = frequency,
        .flags = AUDIO_EVENT_MSG_FLAG_NOTE_ON
    };
    AudioEventMessage msg_off = {0, 0};
    
    publish(DEFAULT_AUDIO_BEEPER_TOPIC, &msg_on);
    u_sleep_milli(duration);
    publish(DEFAULT_AUDIO_BEEPER_TOPIC, &msg_off);
}

void hifi_beep(unsigned frequency, unsigned long long int duration) {
    AudioEventMessage msg_on = {
        .frequency = frequency,
        .flags = AUDIO_EVENT_MSG_FLAG_NOTE_ON
    };
    AudioEventMessage msg_off = {0, 0};
    
    publish(DEFAULT_AUDIO_BEEPER_TOPIC, &msg_on);
    k_sleep_milli(duration);
    publish(DEFAULT_AUDIO_BEEPER_TOPIC, &msg_off);
}

#endif

void beep_on(unsigned frequency) {
    AudioEventMessage msg_on = {
        .frequency = frequency,
        .flags = AUDIO_EVENT_MSG_FLAG_NOTE_ON
    };
    
    publish(DEFAULT_AUDIO_BEEPER_TOPIC, &msg_on);
}

void beep_off() {
    AudioEventMessage msg_off = {0, 0};
    
    publish(DEFAULT_AUDIO_BEEPER_TOPIC, &msg_off);
}