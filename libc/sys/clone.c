#include <sys/proc.h>
#include <os.h>
#include <stdlib.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

int clone(void *thread_entry_point, unsigned int *newtask_id) {
    SyscallCloneParam param;
    param.entrypoint = (uint32_t)thread_entry_point;
    #if defined(__is_libk)
    param.copy_env = NULL;
    #else
    param.copy_env = environ;
    #endif
    param.new_taskid = newtask_id;
#if defined(__is_libk)
    clone_implementation(&param, 0);
#else
    invoke_syscall(SyscallClone, &param);
#endif
    return param.result;
}
