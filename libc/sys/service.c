#include <sys/serv.h>
#include <sys/proc.h>
#include <os.h>
#include <stdlib.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

ServStatus advertise_service_advanced(char *topic_name, 
                                      unsigned int frame_length,
                                      PhisicalDomainClass phys_race_attrib,
                                      DataDomainClass data_class_attrib) {
    SyscallAdvertiseServiceParam param;
    param.topic_name = topic_name;
    param.frame_length = frame_length;
    param.phys_race_attrib = phys_race_attrib;
    param.data_class_attrib = data_class_attrib;
#if defined(__is_libk)
    advertise_service_implementation(&param, 0);
#else
    invoke_syscall(SyscallAdvertiseService, &param);
#endif
    return param.result;
}

ServStatus advertise_service(char *topic_name, 
                             unsigned int frame_length) {
    ProcIdent my_ident = ident();
    return advertise_service_advanced(topic_name, frame_length, my_ident.phys_race_attrib, my_ident.data_class_attrib);
}

ServStatus retire_service(char *topic_name) {
    SyscallRetireServiceParam param;
    param.topic_name = topic_name;
#if defined(__is_libk)
    retire_service_implementation(&param, 0);
#else
    invoke_syscall(SyscallRetireService, &param);
#endif
    return param.result;
}

ServStatus service_accept(char *topic_name, 
                          void *request, 
                          unsigned int *requestor_task_id, 
                          unsigned int *request_id) {
    SyscallAcceptServiceParam param;
    param.topic_name = topic_name;
    param.request = request;
    param.requestor_task_id = requestor_task_id;
    param.request_id = request_id;
#if defined(__is_libk)
    accept_service_implementation(&param, 0);
#else
    invoke_syscall(SyscallAcceptService, &param);
#endif
    return param.result;
}

ServStatus service_complete(char *topic_name, 
                            unsigned int request_id) {
    SyscallMessageServiceIDParam param;
    param.topic_name = topic_name;
    param.request_id = request_id;
#if defined(__is_libk)
    complete_service_implementation(&param, 0);
#else
    invoke_syscall(SyscallCompleteService, &param);
#endif
    return param.result;
}

ServStatus service_poll(char *topic_name, 
                        unsigned int request_id) {
    SyscallMessageServiceIDParam param;
    param.topic_name = topic_name;
    param.request_id = request_id;
#if defined(__is_libk)
    poll_service_implementation(&param, 0);
#else
    invoke_syscall(SyscallPollService, &param);
#endif
    return param.result;
}

ServStatus service_request(char *topic_name, 
                           void *request,
                           unsigned int *request_id) {
    SyscallRequestServiceParam param;
    param.topic_name = topic_name;
    param.message = request;
    param.request_id = request_id;
#if defined(__is_libk)
    request_service_implementation(&param, 0);
#else
    invoke_syscall(SyscallRequestService, &param);
#endif
    return param.result;
}
            
