#include <stdlib.h>
#include <os.h>

#if defined(__is_libk)
#include <syscall.h>
#else
#include <stdio.h>
#endif


int pure_mount(char *mountpoint, char *fsname, char *drivertopic) {
    SyscallMountParam param;
    param.mountpoint = mountpoint;
    param.fsname = fsname;
    param.drivertopic = drivertopic;
#if defined(__is_libk)
    mount_implementation(&param, 0);
#else
    invoke_syscall(SyscallMount, &param);
#endif
    return param.result;
}

int mount(char *diskfile, char *mountpoint, char *fsname) {
    char drivertopic[MAX_TOPIC_LENGTH];
    int mount_result = pure_mount(mountpoint, fsname, drivertopic);
    if (mount_result) return mount_result;
    
#if defined(__is_libk)
    return 0;
#else
    FSServiceMessage request;
    request.service_name = fsservice_setmount;
    strncpy(request.msg.setmount.diskfile, diskfile, MAX_PATH_LENGTH);
    strncpy(request.msg.setmount.mountpoint, mountpoint, MAX_PATH_LENGTH);
    
    make_fsservice_request(drivertopic, &request);
    
    return request.status;
#endif
}
