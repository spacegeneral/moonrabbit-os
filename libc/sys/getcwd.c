#include <stdlib.h>
#include <string.h>
#include <sys/fs.h>


char *getcwd(char *buf, size_t size) {
    char *pwd = getenv("PWD");
    if (pwd == NULL) return NULL;
    if (strlen(pwd)+1 > size) return NULL;
    strcpy(buf, pwd);
    return buf;
}

char *getwd(char *buf) {
    char *pwd = getenv("PWD");
    if (pwd == NULL) return NULL;
    strcpy(buf, pwd);
    return buf;
}
