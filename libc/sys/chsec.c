#include <stdio.h>
#include <errno.h>

#if defined(__is_libk)

#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

int chsec(const char *pathname, unsigned newrace, unsigned newclass) {
    FSServiceMessage request;
    request.service_name = fsservice_chsec;
    request.msg.chsec.newrace = newrace;
    request.msg.chsec.newclass = newclass;
#if defined(__is_libk)
    strncpy(request.msg.chsec.pathname, pathname, MAX_PATH_LENGTH);
    internalfs_service_handler((void*)&request, 0, 0);
#else
    char drivername[MAX_FS_DRIVER_NAME_LENGTH];
    char explicit_path[MAX_PATH_LENGTH];
    get_mount_info(pathname, drivername, explicit_path);
    strncpy(request.msg.chsec.pathname, explicit_path, MAX_PATH_LENGTH);
    make_fsservice_request(drivername, &request);
#endif
    errno = request.status;
    return request.status;
}
