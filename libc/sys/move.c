#include <stdio.h>
#include <errno.h>

#if defined(__is_libk)

#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

static int inner_move(char *path_from, char *path_to) {
    FSServiceMessage request;
    request.service_name = fsservice_move;
    strncpy(request.msg.move.path_to, path_to, MAX_PATH_LENGTH);
#if defined(__is_libk)
    strncpy(request.msg.move.path_from, path_from, MAX_PATH_LENGTH);
    internalfs_service_handler((void*)&request, 0, 0);
#else
    char drivername[MAX_FS_DRIVER_NAME_LENGTH];
    char explicit_path[MAX_PATH_LENGTH];
    get_mount_info(path_from, drivername, explicit_path);
    strncpy(request.msg.move.path_from, explicit_path, MAX_PATH_LENGTH);
    make_fsservice_request(drivername, &request);
#endif
    errno = request.status;
    return request.status;
}

int move(char *path_from, char *path_to) {
    char drivername_from[MAX_FS_DRIVER_NAME_LENGTH];
    char drivername_to[MAX_FS_DRIVER_NAME_LENGTH];
    
    get_mount_info(path_from, drivername_from, NULL);
    get_mount_info(path_to, drivername_to, NULL);
    
    if (strcmp(drivername_from, drivername_to) == 0) {
        return inner_move(path_from, path_to);
    }
    
    errno = CANNOT_CROSS_FS_BOUNDARY;
    return errno;
}
