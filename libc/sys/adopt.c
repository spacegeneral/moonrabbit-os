#include <sys/proc.h>
#include <os.h>
#include <stdlib.h>

#if defined(__is_libk)
#include <syscall.h>
#endif

int adopt(unsigned int child_task_id) {
    SyscallAdoptParam param;
    param.child_task_id = child_task_id;
#if defined(__is_libk)
    adopt_implementation(&param, 0);
#else
    invoke_syscall(SyscallAdopt, &param);
#endif
    return param.result;
}

int disown(unsigned int child_task_id) {
    SyscallDisownParam param;
    param.child_task_id = child_task_id;
#if defined(__is_libk)
    disown_implementation(&param, 0);
#else
    invoke_syscall(SyscallDisown, &param);
#endif
    return param.result;
}
