#include <stdio.h>
#include <errno.h>

#if defined(__is_libk)

#else
#include <sys/serv.h>
#include <sys/proc.h>
#endif

int link(char *path_from, char *path_to) {
    FSServiceMessage request;
    request.service_name = fsservice_create_link;
    strncpy(request.msg.create_link.path_to, path_to, MAX_PATH_LENGTH);
#if defined(__is_libk)
    strncpy(request.msg.create_link.path_from, path_from, MAX_PATH_LENGTH);
    internalfs_service_handler((void*)&request, 0, 0);
#else
    char drivername[MAX_FS_DRIVER_NAME_LENGTH];
    char explicit_path[MAX_PATH_LENGTH];
    get_mount_info(path_from, drivername, explicit_path);
    strncpy(request.msg.create_link.path_from, explicit_path, MAX_PATH_LENGTH);
    make_fsservice_request(drivername, &request);
#endif
    errno = request.status;
    return request.status;
}
