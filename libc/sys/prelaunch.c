#include <sys/proc.h>
#include <os.h>
#include <stdlib.h>

#if defined(__is_libk)
#include <syscall.h>
#else
#include <string.h>
#endif



#if defined(__is_libk)

int prelaunch(const char *filename,
              PhisicalDomainClass phys_race_attrib,
              DataDomainClass data_class_attrib,
              unsigned int *newtask_id,
              int *return_code_here) {
    SyscallPrelaunchKernelParam param;
    param.filename = filename;
    param.copy_env = NULL;
    param.new_taskid = newtask_id;
    param.phys_race_attrib = phys_race_attrib;
    param.data_class_attrib = data_class_attrib;
    param.return_code_here = return_code_here;
    prelaunch_kernel_implementation(&param, 0);
    return param.result;
}

#else

static int prelaunch_call(unsigned char* raw_elf,
                          char *program_name,
                          PhisicalDomainClass phys_race_attrib,
                          DataDomainClass data_class_attrib,
                          unsigned int *newtask_id,
                          int *return_code_here) {
    SyscallPrelaunchUserParam param;
    param.raw_elf = raw_elf;
    param.program_name = program_name;
    param.copy_env = environ;
    param.new_taskid = newtask_id;
    param.phys_race_attrib = phys_race_attrib;
    param.data_class_attrib = data_class_attrib;
    param.return_code_here = return_code_here;
    invoke_syscall(SyscallPrelaunch, &param);
    return param.result;
}


int prelaunch(const char *filename,
              PhisicalDomainClass phys_race_attrib,
              DataDomainClass data_class_attrib,
              unsigned int *newtask_id,
              int *return_code_here) {
    // this function allocates a new memory area for the elf data; this memory will be
    // freed by the kernelspace loading logic
    unsigned char* raw_elf = userland_loader(filename);
    if (raw_elf == NULL) return LAUNCHELF_ERR_FOPEN;
    char *progname = strrchr(filename, DIR_SEPARATOR);
    if (progname == NULL) progname = (char*)filename;
    else progname++;
    return prelaunch_call(raw_elf, progname, phys_race_attrib, data_class_attrib, newtask_id, return_code_here);
}




#endif


/*int prelaunch(const char *filename,
              PhisicalDomainClass phys_race_attrib,
              DataDomainClass data_class_attrib,
              unsigned int *newtask_id,
              int *return_code_here) {
    SyscallPrelaunchParam param;
    param.filename = filename;
    #if defined(__is_libk)
    param.copy_env = NULL;
    #else
    param.copy_env = environ;
    #endif
    param.new_taskid = newtask_id;
    param.phys_race_attrib = phys_race_attrib;
    param.data_class_attrib = data_class_attrib;
    param.return_code_here = return_code_here;
#if defined(__is_libk)
    prelaunch_implementation(&param, 0);
#else
    invoke_syscall(SyscallPrelaunch, &param);
#endif
    return param.result;
}*/
