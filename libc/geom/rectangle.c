#include <geom.h>

int rect_intersection(Rect a, Rect b, Rect *c) {
    Rect leftmost = a;
    Rect rightmost = b;
    Rect bottomost = a;
    Rect topmost = b;
    if (b.x < a.x) {
        leftmost = b;
        rightmost = a;
    }
    if (b.y < a.y) {
        bottomost = b;
        topmost = a;
    }
    
    if (rightmost.x >= leftmost.x + leftmost.w) return 0;
    if (topmost.y >= bottomost.y + bottomost.h) return 0;
    
    
    c->x = rightmost.x;
    if (rightmost.x + rightmost.w > leftmost.x + leftmost.w) {
        c->w = leftmost.x + leftmost.w - rightmost.x;
    } else {
        c->w = rightmost.w;
    }
    
    c->y = topmost.y;
    if (topmost.y + topmost.h > bottomost.y + bottomost.h) {
        c->h = bottomost.y + bottomost.h - topmost.y;
    } else {
        c->h = topmost.h;
    }
    
    return 1;
}

Rect rect_delta(Rect a, Rect b) {
    Rect c = {
        .x = b.x - a.x,
        .y = b.y - a.y,
        .w = b.w - a.w,
        .h = b.h - a.h,
    };
    return c;
}

Rect rect_add(Rect a, Rect b) {
    Rect c = {
        .x = b.x + a.x,
        .y = b.y + a.y,
        .w = b.w + a.w,
        .h = b.h + a.h,
    };
    return c;
}

bool rect_is_zero(Rect a) {
    return (a.x == 0) && (a.y == 0) && (a.w == 0) && (a.h == 0);
}

bool rect_contains(Rect a, Point p) {
    return (p.x >= a.x) && (p.x < a.x+a.w) && (p.y >= a.y) && (p.y < a.y+a.h);
}

