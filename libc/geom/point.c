#include <geom.h>

Point point_delta(Point a, Point b) {
    Point c = {
        .x = b.x - a.x,
        .y = b.y - a.y,
    };
    return c;
}

Point point_add(Point a, Point b) {
    Point c = {
        .x = b.x + a.x,
        .y = b.y + a.y,
    };
    return c;
}
