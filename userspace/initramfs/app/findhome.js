let partpath = "f,metal,part";
let target_type = "Lunar-home";

function mount_home() {
    Fs.scan_media();
    let partition_names = Fs.listdir(partpath);

    for (let nid in partition_names) {
        let currname = partition_names[nid];
        let trailer = currname.slice(currname.length - target_type.length, currname.length);
        if (trailer === target_type) {
            print("Using home partition: " + partpath + "," + currname)
            let mountpoint = Fs.mount(partpath + "," + currname, "fat");
            if (mountpoint !== "undefined") {
                let linkstatus = Fs.link("f,def,home", mountpoint);
                if (linkstatus === 0) {
                    print("Home is ready.");
                    return true;
                } else {
                    print("Failed to link home.");
                    return false;
                }
            } else {
                print("Failed to mount partition.")
            }
        }
    }
    
    print("Could not find home.");
    return false;
}

mount_home();
