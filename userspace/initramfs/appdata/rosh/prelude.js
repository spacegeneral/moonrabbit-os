
let Sec = {
    God: 0,
    Angel: 1,
    Dragon: 2,
    Elf: 3,
    Alien: 4,
    Human: 5,
    Dward: 6,
    Cat: 7,
    Ghost: 8,
    
    Emperor: 0,
    King: 1,
    Princess: 2,
    Knight: 3,
    Priest: 4,
    Citizen: 5,
    Maid: 6,
    Prisoner: 7,
    Slave: 8,
};

let Point = {
    x: 0,
    y: 0,
    
    to_string: function() {
        return "Point(x=" + JSON.stringify(this.x) + ",y=" + JSON.stringify(this.y) + ")";
    },
    
    from_ptr: function(ptr) {
        let point_desc = ffi('void *rosh_get_point_descr()')();
        let point_blueprint = s2o(ptr, point_desc);
        let newpoint = Object.create(Point);
        newpoint.x = point_blueprint.x;
        newpoint.y = point_blueprint.y;
        return newpoint;
    },
};

let Rect = {
    x: 0,
    y: 0,
    w: 0,
    h: 0,
    
    to_string: function() {
        return "Rect(pos=" + JSON.stringify(this.x) + "," + JSON.stringify(this.y) + " size=" + JSON.stringify(this.w) + "x" + JSON.stringify(this.h) + ")";
    },
    
    from_ptr: function(ptr) {
        let rect_desc = ffi('void *rosh_get_rect_descr()')();
        let rect_blueprint = s2o(ptr, rect_desc);
        let newrect = Object.create(Rect);
        newrect.x = rect_blueprint.x;
        newrect.y = rect_blueprint.y;
        newrect.w = rect_blueprint.w;
        newrect.h = rect_blueprint.h;
        return newrect;
    },
};

let MemStats = {
    free: 0,
    total_available: 0,
    
    from_ptr: function(ptr) {
        let ms_desc = ffi('void *rosh_get_memstats_descr()')();
        let ms_blueprint = s2o(ptr, ms_desc);
        let newms = Object.create(MemStats);
        newms.free = ms_blueprint.free;
        newms.total_available = ms_blueprint.total_available;
        return newms;
    },
};

let File = {
    ptr: 0,
    
    seek_set: 0,
    seek_cur: 1,
    seek_end: 2,
    
    close: function() {
        ffi('void rosh_close_file(void*)')(this.ptr);
    },
    getsize: function() {
        return ffi('int rosh_get_filesize(void*)')(this.ptr);
    },
    
    fread: ffi('int rosh_fread(char *, int, int, void *)'),
    fwrite: ffi('int rosh_fwrite(char *, int, int, void *)'),
    // TODO: those functions where written with embedded microcontrollers in mind...
    read: function() {  // read the whole file
        let n = 0; let res = ''; let buf = 'xxxxxxxxxx'; // Should be > 5
        while ((n = File.fread(buf, 1, buf.length, this.ptr)) > 0) {
            res += buf.slice(0, n);
        }
        return res;
    },
    write: function(str) {
        let off = 0; let tot = str.length;
        while (off < tot) {
            let len = 5;  // Use light 5-byte strings for writing
            if (off + len > tot) len = tot - off;
            let n = File.fwrite(str.slice(off, off + len), 1, len, this.ptr);
            // if (n <= 0) break;
            off += n;
        }
        return off;
    },
    
    seek: function(offset, whence) {
        return ffi('int rosh_fseek(int, int, void*)')(offset, whence, this.ptr);
    },
    tell: function() {
        return ffi('int rosh_ftell(void*)')(this.ptr);
    },
};

let Fs = {
    file_exists: ffi('bool file_exists(char *)'),
    _begin_listdir: ffi('void* begin_listdir(char *)'),
    _finish_listdir: ffi('char *finish_listdir(int, void*)'),
    _copy_fileptr: ffi('bool rosh_copy(void*, void*)'),
    touch: ffi('int touch(char *)'),
    link: ffi('int link(char *, char *)'),
    mkdir: ffi('int mkdir(char *)'),
    remove: ffi('int remove(char *)'),
    move: ffi('int move(char *, char *)'),
    mount: ffi('char *rosh_mount(char *, char *)'),
    unmount: ffi('int unmount(char *)'),
    scan_media: ffi('int scan_media()'),
    listdir: function(pathname) {
        let entries = [];
        let stateptr = Fs._begin_listdir(pathname);
        if (Os.ptr_isnull(stateptr)) return entries;
        while (true) {
            let new_entry_name = Fs._finish_listdir(entries.length, stateptr);
            if (new_entry_name === "") return entries;
            entries.push(new_entry_name);
        }
    },
    open: function(pathname, mode) {
        let ptr = ffi('void* rosh_open_file(char *, char *)')(pathname, mode);
        if (Os.ptr_isnull(ptr)) return null;
        let f = Object.create(File);
        f.ptr = ptr;
        return f;
    },
    copy: function(sourcename, destname) {
        let sourcefile = Fs.open(sourcename, "r");
        if (sourcefile === null) return false;
        let destfile = Fs.open(destname, "w");
        if (destfile === null) return false;
        let success = Fs._copy_fileptr(sourcefile.ptr, destfile.ptr);
        sourcefile.close();
        destfile.close();
        return success;
    },
    chsec: ffi('int chsec(char *, int, int)')
};

let Env = {
    set: ffi('int setenv(char *, char *, int)'),
    get: ffi('char *rosh_getenv(char *)'),
    unset: ffi('int unsetenv(char *)'),
};

let Proc = {
    exit: ffi('void exit(int)'),
    spoon: ffi('int spoon(char *, int, int)'),
    exec: function(pathname, prace, pclass, args) {
        for (let aid in args) {
            let curr_arg = args[aid];
            Env.set(curr_arg[0], curr_arg[1], 1);
        }
        let return_code = Proc.spoon(pathname, prace, pclass);
        
        for (let aid in args) {
            let curr_arg = args[aid];
            Env.unset(curr_arg[0]);
        }
        
        return return_code;
    },
    kill: ffi('int kill(int)'),
    yield: ffi('void yield()'),
    task_switch: ffi('int task_switch(int)'),
    get_race: ffi('int rosh_get_race()'),
    get_class: ffi('int rosh_get_class()'),
    spork: ffi('int rosh_spork(char *, int, int)'),
    spawn: function(pathname, prace, pclass, args) {
        for (let aid in args) {
            let curr_arg = args[aid];
            Env.set(curr_arg[0], curr_arg[1], 1);
        }
        let child_pid = Proc.spork(pathname, prace, pclass);
        
        for (let aid in args) {
            let curr_arg = args[aid];
            Env.unset(curr_arg[0]);
        }
        
        return child_pid;
    },
    epspawn: function(pathname, args) {
        return Proc.spawn(pathname, Proc.get_race(), Proc.get_class(), args);
    },
    spawn_app: function(pathname, prace, pclass, args) {
        for (let aid in args) {
            let curr_arg = args[aid];
            Env.set(curr_arg[0], curr_arg[1], 1);
        }
        let child_pid = ffi('int rosh_spawn_app(char *, int, int)')(pathname, prace, pclass);
        
        for (let aid in args) {
            let curr_arg = args[aid];
            Env.unset(curr_arg[0]);
        }
        
        return child_pid;
    },
    epspawn_app: function(pathname, args) {
        return Proc.spawn_app(pathname, Proc.get_race(), Proc.get_class(), args);
    }
};

let Shell = {
    include: ffi('int rosh_include(char *)'),
    search_executable: ffi('char *search_executable(char *)'),
    search_script: ffi('char *search_script(char *)'),
    search_buildfile: ffi('char *search_buildfile(char *)'),
    search_cmd_any: ffi('char *search_cmd_any(char *)'),
};

let TermInfo = {
    termsize: Object.create(Rect),
    screensize: Object.create(Rect),
    cursor: Object.create(Point),
    cursor_enabled: true,
    support_bitmap: true,
    foreground_color: 0,
    background_color: 0,
    scroll_top: 0,
    scroll_bottom: 0,
    flags: 0,
    
    to_string: function() {
        return "TermInfo(termsize=" + this.termsize.to_string() + ",screensize=" + this.screensize.to_string() + ",cursor=" + this.cursor.to_string() + ",cursor_enabled=" + JSON.stringify(this.cursor_enabled) + ",support_bitmap=" + JSON.stringify(this.support_bitmap) + ",foreground_color=" + JSON.stringify(this.foreground_color) + ",background_color=" + JSON.stringify(this.background_color) + ",scroll_top=" + JSON.stringify(this.scroll_top) + ",scroll_bottom=" + JSON.stringify(this.scroll_bottom) + ",flags=" + JSON.stringify(this.flags) + ")";
    },
    
    from_ptr: function(ptr) {
        let tinfo_desc = ffi('void *rosh_get_terminfo_descr()')();
        let tinfo_blueprint = s2o(ptr, tinfo_desc);
        let newtinfo = Object.create(TermInfo);
        newtinfo.termsize = Rect.from_ptr(tinfo_blueprint.termsize);
        newtinfo.screensize = Rect.from_ptr(tinfo_blueprint.screensize);
        newtinfo.cursor = Point.from_ptr(tinfo_blueprint.cursor);
        newtinfo.cursor_enabled = tinfo_blueprint.cursor_enabled;
        newtinfo.support_bitmap = tinfo_blueprint.support_bitmap;
        newtinfo.foreground_color = tinfo_blueprint.foreground_color;
        newtinfo.background_color = tinfo_blueprint.background_color;
        newtinfo.scroll_top = tinfo_blueprint.scroll_top;
        newtinfo.scroll_bottom = tinfo_blueprint.scroll_bottom;
        newtinfo.flags = tinfo_blueprint.flags;
        return newtinfo;
    },
};

let Term = {
    get_terminfo: function() {
        let tinfo = ffi('void *rosh_terminfo()')();
        return TermInfo.from_ptr(tinfo);
    },
    
    _os_setcursor: ffi('void setcursor(int, int)'),
    gotoxy: function(point) {
        Term._os_setcursor(point.x, point.y);
    },
    
    get_key_code: ffi('int getkcode()'),
    
    _os_cls: ffi('void cls()'),
    cls: function() {
        Term._os_cls();
        Term._os_setcursor(0, 0);
    },
    debug_print: ffi('void rosh_debug_print(char*)'),
    printf: ffi('void rosh_printf(char*)'),
    printbb: ffi('void rosh_printbb(char*)'),
    put_image: ffi('int rosh_put_image(char*, double)'),
    put_image_with_size: ffi('int rosh_put_image_with_size(char*, double)'),
};

let Os = {
    get_memstats: function() {
        let ms = ffi('void *rosh_memstats()')();
        return MemStats.from_ptr(ms);
    },
    cpuid_vendor: ffi('char *rosh_cpuid_vendor()'),
    ptr_isnull: ffi('bool ptr_isnull(void*)'),
};


Shell.include("f,def,init,appdata,rosh,api_time.js");
Shell.include("f,def,init,appdata,rosh,api_math.js");
Shell.include("f,def,init,appdata,rosh,api_dataview.js");
