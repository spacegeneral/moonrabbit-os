let WeekDay = {
    Sunday: 0,
    Monday: 1,
    Tuesday: 2,
    Wednesday: 3,
    Thursday: 4,
    Friday: 5,
    Saturday: 6,
    
    value: 0,
    
    from_int: function(num) {
        let newday = Object.create(WeekDay);
        newday.value = num;
        if (newday.value < 0) newday.value = 0;
        if (newday.value > 6) newday.value = 6;
        return newday;
    },
    
    to_string: function() {
        let names = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        return names[this.value];
    },
    
    to_short_string: function() {
        let names = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        return names[this.value];
    },
};

let Month = {
    January: 0,
    February: 1,
    March: 2,
    April: 3,
    May: 4,
    June: 5,
    July: 6,
    August: 7,
    September: 8,
    October: 9,
    November: 10,
    December: 11,
    
    value: 0,
    
    from_int: function(num) {
        let newm = Object.create(Month);
        newm.value = num;
        if (newm.value < 0) newm.value = 0;
        if (newm.value > 11) newm.value = 11;
        return newm;
    },
    
    to_string: function() {
        let names = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return names[this.value];
    },
    
    to_short_string: function() {
        let names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return names[this.value];
    },
};

let DateTime = {
    seconds: 0,
    minutes: 0,
    hours: 0,
    mday: 1,
    yday: 1,
    weekday: 0,
    month: 0,
    year: 0,
    
    base_year: 1900,
    
    from_ptr: function(ptr) {
        let tm_desc = ffi('void *rosh_get_time_tm_descr()')();
        let tm_blueprint = s2o(ptr, tm_desc);
        let newtm = Object.create(DateTime);
        newtm.seconds = tm_blueprint.tm_sec;
        newtm.minutes = tm_blueprint.tm_min;
        newtm.hours = tm_blueprint.tm_hour;
        newtm.mday = tm_blueprint.tm_mday;
        newtm.yday = tm_blueprint.tm_yday;
        newtm.weekday = WeekDay.from_int(tm_blueprint.tm_wday);
        newtm.month = Month.from_int(tm_blueprint.tm_mon);
        newtm.year = tm_blueprint.tm_year + DateTime.base_year;
        
        return newtm;
    },
    
    _digit_to_string: function(digit) {
        let digitstr = JSON.stringify(digit);
        if (digitstr.length <= 1) digitstr = "0" + digitstr;
        return digitstr;
    },
    
    to_string: function() {
        return this.weekday.to_string() + ", " + JSON.stringify(this.mday) + " " + this.month.to_string() + " " + JSON.stringify(this.year) + ", " + DateTime._digit_to_string(this.hours) + ":" + DateTime._digit_to_string(this.minutes) + ":" + DateTime._digit_to_string(this.seconds);
    },
    
    to_short_string: function() {
        return this.weekday.to_short_string() + " " + this.month.to_short_string() + " " + JSON.stringify(this.mday) + " " + DateTime._digit_to_string(this.hours) + ":" + DateTime._digit_to_string(this.minutes) + ":" + DateTime._digit_to_string(this.seconds) + " " + JSON.stringify(this.year);
    },
};

let Time = {
    now: function() {
        let localtime = ffi('void *rosh_localtime()')();
        return DateTime.from_ptr(localtime);
    },
    user_sleep: ffi('void rosh_user_sleep(double)'),
};