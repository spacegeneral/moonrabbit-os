Env.unset("script");
Proc.epspawn(Shell.search_executable("fatdrv"), [["mount_race", "Human"], ["mount_class", "Citizen"]]);
Shell.include(Shell.search_script("findhome"));
let second_level_boot_script = "f,def,home,appdata,init,boot_time.js";
if (Fs.file_exists(second_level_boot_script)) {
    Proc.exec(Shell.search_executable("kosh"), Proc.get_race(), Proc.get_class(), [["script", second_level_boot_script]]);
    Env.set("PWD", "f,def,home", 1);
}
Proc.epspawn(Shell.search_executable("kosh"), []);
