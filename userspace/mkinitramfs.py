#!/bin/python

import os
import struct
import argparse
import zlib
import time


files = []



class FileType:
    File=0
    Directory=1
    Link=2
    Unsupported=3


class FileMetadata:
    def __init__(self):
        self.race_attrib = 5
        self.class_attrib = 5
        self.executable = 1
        self.writable = 1
        self.readable = 1
        self.creation_time = int(time.time())
        self.modification_time = int(time.time())
        
    def write(self, tofile):
        tofile.write(struct.pack("<iiiiiQQ",
                                 self.race_attrib,
                                 self.class_attrib,
                                 self.executable,
                                 self.writable,
                                 self.readable,
                                 self.creation_time,
                                 self.modification_time))


class FileDescriptor:
    def __init__(self, name, path, filetype):
        self.name = name
        self.driver = "rootfs"
        self.metadata = FileMetadata()
        self.parent = 0
        self.filetype = filetype
        self.access_strategy = 0
        self.data_address = 0
        
        self.childs_if_dir = []
        self.remember_data_address_seek = 0
        self.path = path
        
    def writename(self, tofile):
        for letter in self.name:
            tofile.write(struct.pack("<b", ord(letter)))
        for _ in range(256 - len(self.name)):
            tofile.write(struct.pack("<b", 0))
            
    def writedriver(self, tofile):
        for letter in self.driver:
            tofile.write(struct.pack("<b", ord(letter)))
        for _ in range(256 - len(self.driver)):
            tofile.write(struct.pack("<b", 0))
        
    def write_header(self, tofile):
        self.writename(tofile)
        self.writedriver(tofile)
        self.metadata.write(tofile)
        tofile.write(struct.pack("<I", 0))
        tofile.write(struct.pack("<i", self.filetype))
        tofile.write(struct.pack("<i", self.access_strategy))
        self.remember_data_address_seek = tofile.tell()
        tofile.write(struct.pack("<I", self.data_address))
        
    def write_content(self, tofile):
        self.data_address = tofile.tell()
        
        if self.filetype == FileType.File:
            with open(self.path, "rb") as fromfile:
                data_from = fromfile.read()
                
            compressed_data = zlib.compress(data_from, level=9)
            
            tofile.write(struct.pack("<I", len(data_from)))
            tofile.write(struct.pack("<I", len(compressed_data)))
            tofile.write(compressed_data)
            for _ in range(4 - (len(compressed_data) % 4)):
                tofile.write(struct.pack("<b", 0))
        
        elif self.filetype == FileType.Directory:
            global files
            tofile.write(struct.pack("<I", len(self.childs_if_dir)))
            for child in self.childs_if_dir:
                tofile.write(struct.pack("<I", 
                                         files.index(child)))
                
        elif self.filetype == FileType.Link:
            destlink = os.readlink(self.path)
            tofile.write(struct.pack("<I", len(destlink)))
            for letter in destlink:
                tofile.write(struct.pack("<b", ord(letter)))
            for _ in range(4 - (len(destlink) % 4)):
                tofile.write(struct.pack("<b", 0))
                
    def fix_header(self, tofile):
        tofile.seek(self.remember_data_address_seek)
        tofile.write(struct.pack("<I", self.data_address))


def get_filetype(path):
    if os.path.islink(path):
        return FileType.Link
    if os.path.isfile(path):
        return FileType.File
    if os.path.isdir(path):
        return FileType.Directory
    return FileType.Unsupported
    

def scan_file(path):
    name = os.path.basename(path)
    filetype = get_filetype(path)
    newentry = FileDescriptor(name, path, filetype)
    files.append(newentry)
    #print("added file %s, type=%d" % (name, filetype))
    
    if os.path.isdir(path):
        for subfile in os.listdir(path):
            newentry.childs_if_dir.append(scan_file(os.path.join(path, subfile)))
            
    return newentry


def write_super_header(tofile, files):
    tofile.write(struct.pack("I", 0xCABAAD10))
    tofile.write(struct.pack("I", 0x10))
    tofile.write(struct.pack("I", len(files)))
    for _ in range(125):
        tofile.write(struct.pack("I", 0))


def parsecli():
    parser = argparse.ArgumentParser(description="Create moonrabbit os initramfs")
    parser.add_argument('diskfile', metavar='diskfile', help='File to be overwritten with the initramfs', type=str)
    parser.add_argument('root', metavar='root', help='Path to the initramfs root directory', type=str)
    return parser.parse_args()


if __name__ == "__main__":
    cli = parsecli()
    scan_file(cli.root)
    with open(cli.diskfile, "rb+") as fp:
        write_super_header(fp, files)
        for file_entry in files:
            file_entry.write_header(fp)
        for file_entry in files:
            file_entry.write_content(fp)
        print("Total initramfs size: %.2f KB\n" % (fp.tell() / 1024,))
        for file_entry in files:
            file_entry.fix_header(fp)

    
