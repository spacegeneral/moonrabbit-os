Shell.include(Shell.search_script("verba.js"));


let test_set_ok = [
    "f,def,home,test,stage_1,valid,return_0",
    "f,def,home,test,stage_1,valid,return_2",
    "f,def,home,test,stage_1,valid,spaces",
    "f,def,home,test,stage_1,valid,multi_digit",
    "f,def,home,test,stage_1,valid,newlines",
    "f,def,home,test,stage_1,valid,no_newlines",
    
];
let test_set_fail = [
    "f,def,home,test,stage_1,invalid,missing_paren",
    "f,def,home,test,stage_1,invalid,missing_retval",
    "f,def,home,test,stage_1,invalid,no_brace",
    "f,def,home,test,stage_1,invalid,no_semicolon",
    "f,def,home,test,stage_1,invalid,no_space",
    "f,def,home,test,stage_1,invalid,wrong_case",
    
];



let tests_passed = 0;


for (let i=0; i<test_set_ok.length; i++) {
    let status = VerbaCompiler.compile_file(test_set_ok[i] + ".c", test_set_ok[i] + ".S");
    let statusmsg = "FAIL";
    if (status) {
        tests_passed++;
        statusmsg = "OK";
    }
    print("compiling " + test_set_ok[i] + ".c " + statusmsg);
}


for (let i=0; i<test_set_fail.length; i++) {
    let status = VerbaCompiler.compile_file(test_set_fail[i] + ".c", test_set_fail[i] + ".S");
    let statusmsg = "FAIL";
    if (!status) {
        tests_passed++;
        statusmsg = "OK";
    }
    print("compiling " + test_set_fail[i] + ".c " + statusmsg);
}

print("Test (passed/total): " + JSON.stringify(tests_passed) + " / " + JSON.stringify(test_set_ok.length + test_set_fail.length));
