Shell.include(Shell.search_script("verba,lexer.js"));
Shell.include(Shell.search_script("verba,parser.js"));


let VerbaCompiler = {
    compile_file: function(infile_name, outfile_name) {
        let infile = Fs.open(infile_name, "r");
        if (infile===null) {
            print("Could not open file", infile_name);
            return false;
        }
        
        let infile_source = infile.read();
        infile.close();
        
        let lexer = Object.create(VerbaLexer);
        lexer.lex(infile_source);
        
        print("lexer tokens:");
        print(lexer.to_string());
        
        let parser = Object.create(VerbaParser);
        parser.parse(lexer.tokens);
        if (parser.ast === null) return false;
        
        //print("AST:");
        //print(parser.to_string());
        
        let outfile = Fs.open(outfile_name, "w");
        if (infile===null) {
            print("Could not open file", outfile_name);
            return false;
        }
        outfile.write(parser.ast.emit_asm());
        outfile.close();
        
        return true;
    }
};
