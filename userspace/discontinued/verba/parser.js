let ASTNodeType = {
    PROGRAM:                0,
    FUNCTION:               1,
    RETURN:                 2,
    CONSTANT:               3,
    UNARY:                  4,

    
};


let ASTNode = {
    type: 0,
    value: "",
    children: [],
    
    to_string: function() {
        return "ASTNode()";
    },
    emit_asm: function() {
        return "nop\n";
    },
    
};




let VerbaParser = {
    ast: null,
    error: false,
    
    fail: function(message) {
        print(message);
        this.error = true;
        return null;
    },
    
    pop_token: function(tokens) {
        let poped = tokens.splice(0, 1);
        if (poped.length === 0) {
            return this.fail("ran out of tokens");
        }
        return poped[0];
    },
    
    to_string: function() {
        if (this.error) {
            return "Empty AST";
        } else if (this.ast === null) {
            return "Empty AST";
        } else {
            return this.ast.to_string();
        }
    },
    
    parse: function(tokens) {
        let program = this.parse_program(tokens);
        if (program === null) {return this.fail("invalid c program");}
        this.ast = program;
        return this.ast;
    },
    
    parse_program: function(tokens) {
        let program = Object.create(ASTNode);
        program.type = ASTNodeType.PROGRAM;
        program.functions = [];
        program.children = [];
        let cfunction = this.parse_function(tokens);
        if (cfunction === null) {return this.fail("invalid c function");}
        program.children.push(cfunction);
        program.functions.push(cfunction);
        program.to_string = function() {
            let str = "PROGRAM\n";
            for (let i=0; i<this.children.length; i++) {
                str = str + this.children[i].to_string();
            }
            return str;
        }
        program.emit_asm = function() {
            let str = "";
            for (let i=0; i<this.children.length; i++) {
                str = str + this.children[i].emit_asm();
            }
            return str;
        }
        return program;
    },
    
    parse_function: function(tokens) {
        let type = this.pop_token(tokens);
        if (type === null || type.type !== TokenType.KEYWORD_INT) {return this.fail("invalid function type");}
        let identifier = this.pop_token(tokens);
        if (identifier === null || identifier.type !== TokenType.IDENTIFIER) {return this.fail("invalid function identifier");}
        let open_paren = this.pop_token(tokens);
        if (open_paren === null || open_paren.type !== TokenType.OPEN_PAREN) {return this.fail("invalid function singnature (open paren)");}
        let closed_paren = this.pop_token(tokens);
        if (closed_paren === null || closed_paren.type !== TokenType.CLOSE_PAREN) {return this.fail("invalid function singnature (closed paren)");}
        let open_brace = this.pop_token(tokens);
        if (open_brace === null || open_brace.type !== TokenType.OPEN_BRACE) {return this.fail("invalid function body (open brace)");}
        let statement = this.parse_statement(tokens);
        if (statement === null) {return this.fail("invalid function body (statement)");}
        let closed_brace = this.pop_token(tokens);
        if (closed_brace === null || closed_brace.type !== TokenType.CLOSE_BRACE) {return this.fail("invalid function body (closed brace)");}
        
        let cfunction = Object.create(ASTNode);
        cfunction.type = ASTNodeType.FUNCTION;
        cfunction.children = [];
        cfunction.return_type = type;
        cfunction.identifier = identifier;
        cfunction.statements = [];
        cfunction.children.push(statement);
        cfunction.statements.push(statement);
        cfunction.to_string = function() {
            let str = "    FUNCTION <" + this.return_type.to_string() + "> " + this.identifier.value + " ()\n";
            for (let i=0; i<this.children.length; i++) {
                str = str + this.children[i].to_string();
            }
            return str;
        }
        cfunction.emit_asm = function() {
            let str = ".global _" + this.identifier.value + "\n" + this.identifier.value + ":\n";
            for (let i=0; i<this.children.length; i++) {
                str = str + this.children[i].emit_asm();
            }
            return str;
        }
        return cfunction;
    },
    
    parse_statement: function(tokens) {
        let initial_keyword = this.pop_token(tokens);
        if (initial_keyword === null || initial_keyword.type !== TokenType.KEYWORD_RETURN) {return this.fail("invalid statement begin");}
        let expression = this.parse_expression(tokens);
        if (expression === null) {return this.fail("invalid statement expression");}
        let semicolon = this.pop_token(tokens);
        if (semicolon === null || semicolon.type !== TokenType.SEMICOLON) {return this.fail("invalid statement (missing semicolon)");}
        
        let statement = Object.create(ASTNode);
        statement.children = [];
        
        if (initial_keyword.type === TokenType.KEYWORD_RETURN) {
            statement.type = ASTNodeType.RETURN;
            statement.to_string = function() {
                return "            RETURN " + this.children[0].to_string() + "\n";
            }
            statement.emit_asm = function() {
                let str = "";
                for (let i=0; i<this.children.length; i++) {
                    str = str + this.children[i].emit_asm();
                }
                return str + "ret\n";
            }
        }
        
        statement.children.push(expression);
        return statement;
    },
    
    parse_expression: function(tokens) {
        let expression = Object.create(ASTNode);
        expression.type = ASTNodeType.EXPRESSION;
        expression.children = [];
        
        let tok = this.pop_token(tokens);
        if (tok === null) {return this.fail("invalid token in expression");}
        
        if (tok.type === TokenType.CONSTANT) {
            expression.children.push(this.get_constant(tok));
            expression.to_string = function() {
                return this.children[0].to_string();
            }
            expression.emit_asm = function() {
                return this.children[0].emit_asm(;
            }
        }
        else {
            let unop = this.parse_unary_operator(tok);
            unop.children.push(parse_expression(tokens));
            expression.children.push(unop);
            expression.to_string = function() {
                return this.children[0].to_string();
            }
            expression.emit_asm = function() {
                return this.children[0].emit_asm();
            }
        }
        
        return expression;
    },
    
    parse_unary_operator: function(token) {
        let unop = Object.create(ASTNode);
        unop.children = [];
        unop.type = ASTNodeType.UNARY;
        unop.value = token;
        unop.to_string = function() {
            return this.value.value + this.children[0].to_string();
        }
        unop.emit_asm = function() {
            if (this.value.type === TokenType.ARITHMETIC_NEGATION) {
                let str = "";
                for (let i=0; i<this.children.length; i++) {
                    str = str + this.children[i].emit_asm();
                }
                return str + "neg %eax\n";
            }
            return "";
        }
    },
    
    get_constant: function(token) {
        let integer = Object.create(ASTNode);
        integer.children = [];
        integer.type = ASTNodeType.CONSTANT;
        integer.value = JSON.parse(token.value);
        integer.to_string = function() {
            return JSON.stringify(this.value);
        }
        integer.emit_asm = function() {
            return "movl " + JSON.stringify(this.value) + ", %eax\n";
        }
        return integer;
    },
    
};
