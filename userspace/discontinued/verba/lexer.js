
let TokenType = {
    IDENTIFIER:             0,
    OPEN_PAREN:             1,
    CLOSE_PAREN:            2,
    OPEN_BRACE:             3,
    CLOSE_BRACE:            4,
    CONSTANT:               5,
    SEMICOLON:              6,
    KEYWORD_INT:            7,
    KEYWORD_RETURN:         8,
    ARITHMETIC_NEGATION:    9,
    LOGICAL_NEGATION:       10,
    BITWISE_COMPLEMENT:     11,
    
    
    name: ["IDENTIFIER", "OPEN_PAREN", "CLOSE_PAREN", "OPEN_BRACE", "CLOSE_BRACE", 
           "CONSTANT", "SEMICOLON", "KEYWORD_INT", "KEYWORD_RETURN", "ARITHMETIC_NEGATION", 
           "LOGICAL_NEGATION", "BITWISE_COMPLEMENT"],
};

function isspace(character) {
    return " \t\n\v\f\r".indexOf(character) !== -1;
};

function isnumber(character) {
    return "0123456789".indexOf(character) !== -1;
};

let LexerToken = {
    type: 0,
    value: "",
    special_chars: "({});",
    consume: function(character) {
        this.value = this.value + character;
    },
    evaluate: function() {
        if (this.value === "(") {this.type = TokenType.OPEN_PAREN;}
        else if (this.value === ")") {this.type = TokenType.CLOSE_PAREN;}
        else if (this.value === "{") {this.type = TokenType.OPEN_BRACE;}
        else if (this.value === "}") {this.type = TokenType.CLOSE_BRACE;}
        else if (this.value === ";") {this.type = TokenType.SEMICOLON;}
        else if (this.value === "-") {this.type = TokenType.ARITHMETIC_NEGATION;}
        else if (this.value === "!") {this.type = TokenType.LOGICAL_NEGATION;}
        else if (this.value === "~") {this.type = TokenType.BITWISE_COMPLEMENT;}
        else if (this.value === "return") {this.type = TokenType.KEYWORD_RETURN;}
        else if (this.value === "int") {this.type = TokenType.KEYWORD_INT;}
        else if (isnumber(this.value[0])) {this.type = TokenType.CONSTANT;}
        else {this.type = TokenType.IDENTIFIER;}
        
    },
    lookahead: function(next_character) {
        if (this.value.length === 0) {return false;}
        if (isspace(next_character)) {return true;}
        if (LexerToken.special_chars.indexOf(this.value) !== -1) {return true;}
        if (LexerToken.special_chars.indexOf(next_character) !== -1) {return true;}
        return false;
    },
    
    to_string: function() {
        return TokenType.name[this.type] + "(" + this.value + ")";
    },
};




let VerbaLexer = {
    tokens: [],
    input_string: "",
    
    lex: function(str) {
        this.tokens = [];
        this.input_string = str;
        let current_token = Object.create(LexerToken);
        current_token.value = "";
        for (let i=0; i<this.input_string.length; i++) {
            let new_character = this.input_string[i];
            
            if (current_token.lookahead(new_character)) {
                current_token.evaluate();
                this.tokens.push(current_token);
                current_token = Object.create(LexerToken);
                current_token.value = "";
            }
            
            if (isspace(new_character)) continue;
            current_token.consume(new_character);
        }
        if (current_token.value.length > 0) {
            current_token.evaluate();
            this.tokens.push(current_token);
        }
    },
    
    to_string: function() {
        let str = "TokenList(";
        for (let i=0; i<this.tokens.length; i++) {
            str = str + this.tokens[i].to_string();
            if (i+1 < this.tokens.length) str = str + ", ";
        }
        return str + ")";
    }
};
