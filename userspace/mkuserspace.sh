#!/bin/bash

rm -r homemaster/compile/lib/*
rm -r homemaster/compile/include/*
rm -r homemaster/compile/src/*

set -e

cp -r --preserve=timestamps ../sysroot_user/usr/* homemaster/compile/
mkdir -p homemaster/compile/lib
cp -r --preserve=timestamps ../sysroot/usr/lib/* homemaster/compile/lib/
cp -r --preserve=timestamps ../tinycc/include/* homemaster/compile/include/

mkdir -p homemaster/compile/crt
echo "#include <stddef.h>" > homemaster/compile/include/stdint.h

find homemaster/compile/lib -type f -iname "*.a" -exec pigz -z -f {} \;

mkdir -p homemaster/compile/src
cp --preserve=timestamps src/{mofuterm.c,gordon.c,rosh.c,sha256sum.c,ush.c,wump.c,playmidi.c,udchat.c,xd.c,cc.c} homemaster/compile/src/
mkdir -p homemaster/compile/src/{modular,inih,ipdrv,midi,lamenes}
cp --preserve=timestamps src/door*.c homemaster/compile/src/
cp -r --preserve=timestamps src/modular/* homemaster/compile/src/modular/
cp -r --preserve=timestamps src/inih/* homemaster/compile/src/inih/
cp -r --preserve=timestamps src/ipdrv/* homemaster/compile/src/ipdrv/
cp -r --preserve=timestamps src/midi/* homemaster/compile/src/midi/
cp -r --preserve=timestamps src/lamenes/* homemaster/compile/src/lamenes/
mkdir -p homemaster/compile/src/mjs
cp --preserve=timestamps src/mjs/{mjs.h,mjs.c,config.h} homemaster/compile/src/mjs/
mkdir -p homemaster/compile/src/domus
cp --preserve=timestamps src/domus/{*.c,*.h} homemaster/compile/src/domus/
find homemaster/compile/src/ -not \( -name "*.c" -o -name "*.h" \) -type f -delete


. ./buildscript.sh

cp --preserve=timestamps crt0.o homemaster/compile/crt/

find homemaster -type f -iname "*.elf" -exec pigz -z -f {} \;

sudo losetup -o 1048576 /dev/loop1 testdisk.raw
sudo losetup -o 34603008 /dev/loop3 testdisk.raw
sudo mount -o uid=1000,gid=100 /dev/loop3 ./home

sudo python mkinitramfs.py /dev/loop1 initramfs
rm -rf ./home/*
cp -r --preserve=timestamps homemaster/* home/

du -sh ./home

sudo umount ./home

echo "Home checksum: `sudo sha256sum /dev/loop3`"

sudo losetup -d /dev/loop3
sudo losetup -d /dev/loop1
