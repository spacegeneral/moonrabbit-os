import socket
import struct
import argparse
import os
import logging
from pathlib import Path
from stat import *


MAX_PATH_LENGTH = 4096
MAX_FILEIO_PAYLOAD = MAX_PATH_LENGTH * 2
MAX_FILENAME_LENGTH = 256
MESSAGE_SIZE = 8532


fsservice_type = {
    "fsservice_create_file": 0,
    "fsservice_create_directory": 1,
    "fsservice_create_link": 2,
    "fsservice_remove": 3,
    "fsservice_count_directory_entries": 4,
    "fsservice_get_directory_entry_at": 5,
    "fsservice_file_exists": 6,
    "fsservice_move": 7,
    "fsservice_fopen": 8,
    "fsservice_fclose": 9,
    "fsservice_fflush": 10,
    "fsservice_fread": 11,
    "fsservice_fwrite": 12,
    "fsservice_fseek": 13,
    "fsservice_setmount": 14,
    "fsservice_unsetmount": 16,
    "fsservice_chsec": 17
}


NOT_A_DIRECTORY = 4
CANNOT_CREATE_FILE = 9
NOT_IMPLEMENTED = 10
CANNOT_CREATE_DIR = 11
CANNOT_REMOVE = 12
CANNOT_LIST_DIRECTORY = 27

mount_race = 5
mount_class = 5



def parsecli():
    parser = argparse.ArgumentParser(description="Remote server for the MoonrabbitOS filesystem protocol.")
    parser.add_argument('--host', help='IP address to listen on', type=str, default="0.0.0.0")
    parser.add_argument('--port', '-p', help='UDP port to listen on', type=int, default=5959)
    parser.add_argument('--root', '-r', help='Root folder to serve', type=str, default=os.getcwd())
    return parser.parse_args()


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


class MrFileserver:
    
    def __init__(self, cli):
        self.cli = cli
        
        self.mount_root = cli.root
        self.mount_base_path = ""
        
        self.log = logging.getLogger('mr_fileserver')
        FORMAT_CONS = '%(asctime)s %(name)-12s %(levelname)8s\t%(message)s'
        logging.basicConfig(level=logging.DEBUG, format=FORMAT_CONS)

    def translate_path(self, path):
        return path.replace(self.mount_base_path, self.mount_root).replace(",", "/").replace("//", "/")
    
    def trim_nulls(self, some_bytes):
        return some_bytes.split(b'\0',1)[0]
    
    def get_dirent(self, filename, filepath):
        response = b""
        response += struct.pack("<%ds" % MAX_FILENAME_LENGTH, filename.encode("utf-8"))  # 4356
        response += struct.pack("<I", mount_race)  # 4360
        response += struct.pack("<I", mount_class)  # 4364
        stats = os.stat(filepath)
        response += struct.pack("<I", bool(S_IXUSR & stats.st_mode))  # 4368
        response += struct.pack("<I", bool(S_IWUSR & stats.st_mode))  # 4372
        response += struct.pack("<I", bool(S_IRUSR & stats.st_mode))  # 4376
        response += struct.pack("<Q", int(stats.st_ctime))  # 4384
        response += struct.pack("<Q", int(stats.st_mtime))  # 4392
        size = 0
        filetype = 4
        if os.path.islink(filepath):
            filetype = 2
        elif os.path.isdir(filepath):
            filetype = 1
        elif os.path.isfile(filepath):
            filetype = 0
            size = stats.st_size
        response += struct.pack("<I", filetype)  # 4396
        response += struct.pack("<Q", size)  # 4404
        return response

    def parse_request(self, requestdata):
        verb = struct.unpack("I", requestdata[0:4])[0]
        msg = requestdata[4:MESSAGE_SIZE-4]
        # ignore the status int32
        status = 0
        response = b""
        response += struct.pack("I", verb)  # verb is copied
        
        self.log.debug("Request verb: %d" % verb)
        
        if verb == fsservice_type["fsservice_create_file"]:
            pathname = self.trim_nulls(struct.unpack("<%ds" % MAX_PATH_LENGTH, msg[0:MAX_PATH_LENGTH])[0]).decode("utf-8")
            try:
                Path(self.translate_path(pathname)).touch()
            except Exception:
                status = CANNOT_CREATE_FILE
            
        elif verb == fsservice_type["fsservice_create_directory"]:
            pathname = self.trim_nulls(struct.unpack("<%ds" % MAX_PATH_LENGTH, msg[0:MAX_PATH_LENGTH])[0]).decode("utf-8")
            try:
                os.mkdir(self.translate_path(pathname))
            except Exception:
                status = CANNOT_CREATE_DIR
            
        elif verb == fsservice_type["fsservice_create_link"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_remove"]:
            pathname = self.trim_nulls(struct.unpack("<%ds" % MAX_PATH_LENGTH, msg[0:MAX_PATH_LENGTH])[0]).decode("utf-8")
            translated_path = self.translate_path(pathname)
            try:
                if os.path.isfile(translated_path):
                    os.remove(translated_path)
                else:
                    os.rmdir(translated_path)
            except Exception:
                status = CANNOT_REMOVE
                
        elif verb == fsservice_type["fsservice_count_directory_entries"]:
            pathname = self.trim_nulls(struct.unpack("<%ds" % MAX_PATH_LENGTH, msg[0:MAX_PATH_LENGTH])[0]).decode("utf-8")
            lst = []
            try:
                self.log.debug("Enumerating entries in %s" % (self.translate_path(pathname),))
                lst = os.listdir(self.translate_path(pathname))
            except Exception:
                status = NOT_A_DIRECTORY
            response += msg[0:MAX_PATH_LENGTH]
            response += struct.pack("<L", len(lst))
            
        elif verb == fsservice_type["fsservice_get_directory_entry_at"]:
            pathname = self.trim_nulls(struct.unpack("<%ds" % MAX_PATH_LENGTH, msg[0:MAX_PATH_LENGTH])[0]).decode("utf-8")
            position = struct.unpack("<L", msg[MAX_PATH_LENGTH:MAX_PATH_LENGTH+4])[0]
            
            translated_path = self.translate_path(pathname)
            selected_filename = ""
            
            try:
                lst = os.listdir(translated_path)
                selected_filename = lst[position]
                response += msg[0:MAX_PATH_LENGTH]  # 4096
                response += struct.pack("<L", position)  # 4100
                selected_filepath = os.path.join(translated_path, selected_filename)
                response += self.get_dirent(selected_filename, selected_filepath)
            except Exception:
                status = CANNOT_LIST_DIRECTORY
            
        elif verb == fsservice_type["fsservice_file_exists"]:
            pathname = self.trim_nulls(struct.unpack("<%ds" % MAX_PATH_LENGTH, msg[0:MAX_PATH_LENGTH])[0]).decode("utf-8")
            translated_path = self.translate_path(pathname)
            self.log.debug("File '%s' (%s) exists? (%s)" % (translated_path, pathname, str(os.path.exists(translated_path))))
            response += msg[0:MAX_PATH_LENGTH]
            response += struct.pack("<i", os.path.exists(translated_path))
            
            
        elif verb == fsservice_type["fsservice_move"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_fopen"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_fclose"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_fflush"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_fread"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_fwrite"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_fseek"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_setmount"]:
            diskfile = self.trim_nulls(struct.unpack("<%ds" % MAX_PATH_LENGTH, msg[0:MAX_PATH_LENGTH])[0]).decode("utf-8")
            mountpoint = self.trim_nulls(struct.unpack("<%ds" % MAX_PATH_LENGTH, msg[MAX_PATH_LENGTH:MAX_FILEIO_PAYLOAD])[0]).decode("utf-8")
            self.mount_base_path = mountpoint
            self.log.info("Mounted to %s (ignoring diskfile path %s)" % (repr(mountpoint), diskfile))
            
        elif verb == fsservice_type["fsservice_unsetmount"]:
            status = NOT_IMPLEMENTED
            
        elif verb == fsservice_type["fsservice_chsec"]:
            status = NOT_IMPLEMENTED
        
        
        response += requestdata[len(response):MESSAGE_SIZE-4]
        response += struct.pack("<i", status)
        return response
    
    def serve(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((cli.host, cli.port))
        self.log.debug("Listening on %s:%d" % (cli.host, cli.port))
        accumulator = b""
    
        while True:
            (data, address) = sock.recvfrom(9001)
            accumulator += data
            #self.log.debug("Incoming data from address %s" % str(address))
            if len(accumulator) >= MESSAGE_SIZE:
                self.log.debug("Reconstructed request from address %s" % str(address))
                response = self.parse_request(accumulator[0:MESSAGE_SIZE])
                self.log.debug("Response size: %d\n", len(response))
                accumulator = accumulator[MESSAGE_SIZE:]
                for chunk in chunks(response, 512):
                    sock.sendto(chunk, address)


def main(cli):
    server = MrFileserver(cli)
    server.serve()


if __name__ == "__main__":
    cli = parsecli()
    main(cli)