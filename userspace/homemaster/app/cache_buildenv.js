Fs.mkdir("f,def,cache");
Fs.mkdir("f,def,cache,app");
Fs.mkdir("f,def,cache,compile");
Fs.mkdir("f,def,cache,compile,lib");
Fs.mkdir("f,def,cache,compile,include");
Fs.mkdir("f,def,cache,compile,crt");
Fs.mkdir("f,def,cache,compile,src");

let libs = Fs.listdir("f,def,home,compile,lib");
for (let i=0; i<libs.length; i++) {
    let success = Fs.copy("f,def,home,compile,lib," + libs[i], "f,def,cache,compile,lib," + libs[i]);
    if (!success) {
        print("Failed to copy " + "f,def,home,compile,lib," + libs[i] + " -> f,def,cache,compile,lib," + libs[i]);
        break;
    }
}

function copy_type_recursive(frompath, topath, extension) {
    let mixedfiles = Fs.listdir(frompath);
    for (let i=0; i<mixedfiles.length; i++) {
        let currname = mixedfiles[i];
        let slice_start = currname.length - extension.length;
        let slice_end = currname.length;
        let trailer = currname.slice(slice_start, slice_end);

        if (trailer === extension) {
            let success = Fs.copy(frompath + "," + currname, topath + "," + currname);
            if (!success) {
                print("Failed to copy " + frompath + "," + currname + " -> " + topath + "," + currname);
                break;
            }
        } else if (trailer[0] !== ".") {
            Fs.mkdir(topath + "," + currname);
            copy_type_recursive(frompath + "," + currname, topath + "," + currname, extension);
        }
    }
}


Fs.copy("f,def,home,compile,crt,crt0.o", "f,def,cache,compile,crt,crt0.o");
copy_type_recursive("f,def,home,compile,include", "f,def,cache,compile,include", ".h");
copy_type_recursive("f,def,home,compile,src", "f,def,cache,compile,src", ".c");
copy_type_recursive("f,def,home,compile,src", "f,def,cache,compile,src", ".h");
Fs.copy("f,def,home,app,cc.elf.zz", "f,def,cache,app,cc.elf");
