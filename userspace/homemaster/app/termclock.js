Term.cls();

let ti = Term.get_terminfo();

while (true) {
    Term.gotoxy(ti.cursor);
    let timestr = Time.now().to_string();
    let spacestr = "";
    for (let s=0; s<(ti.termsize.w - timestr.length) / 2; s++) spacestr = spacestr + " ";
    Term.printf(spacestr + timestr + "\n");
    
    let memstats = Os.get_memstats();
    let ram_mb_used = Math.round((memstats.total_available - memstats.free) / 1024);
    let ram_mb_total = Math.round(memstats.total_available / 1024);
    
    let memstr = "RAM usage: " + JSON.stringify(ram_mb_used) + "KB / " + JSON.stringify(ram_mb_total) + "KB";
    spacestr = "";
    for (let s=0; s<(ti.termsize.w - memstr.length) / 2; s++) spacestr = spacestr + " ";
    Term.printf(spacestr + memstr);
    
    Time.user_sleep(1.0);
}