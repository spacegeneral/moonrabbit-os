let terminfo = Term.get_terminfo();
let memstats = Os.get_memstats();
let ram_mb_used = Math.round((memstats.total_available - memstats.free) / 1048576);
let ram_mb_total = Math.round(memstats.total_available / 1048576);

print("        .++++++.");
print("     .++++ +++++++.");
print("    ++++ ++ ++++++++");
print("   ++++++ ++ ++++++++   OS: Moonrabbit OS");
print("  ++++++++  O *+++++++  Kernel: " + Env.get("KERNEL_VER"));
print("  ++++++++.  .W+++++++  CPU: " + Os.cpuid_vendor());
print("  ++++++++    ++++++++  Resolution: " + JSON.stringify(terminfo.screensize.w) + "x" + JSON.stringify(terminfo.screensize.h));
print("   ++++++    +. +++++   RAM: " + JSON.stringify(ram_mb_used) + "MB / " + JSON.stringify(ram_mb_total) + "MB");
print("    +++.    ++++++++");
print("     '+++++..  +++'");
print("        '++++++'");
