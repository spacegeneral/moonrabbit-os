let divider = 1;
if (Env.get("k") !== "") {divider = 1024;}
else if (Env.get("m") !== "") {divider = 1024*1024;}
else if (Env.get("g") !== "") {divider = 1024*1024*1024;}

let memstats = Os.get_memstats();
let ram_b_used = Math.round((memstats.total_available - memstats.free) / divider);
let ram_b_free = Math.round(memstats.free / divider);
let ram_b_total = Math.round(memstats.total_available / divider);

function spalign(txt, numspaces) {
    let header = "";
    for (let i=0; i<numspaces - txt.length; i++) {
        header += " ";
    }
    return header + txt;
}

Term.printf("              Total:         Used:         Free:\n");
Term.printf("Mem   " + spalign(JSON.stringify(ram_b_total), 14) + spalign(JSON.stringify(ram_b_used), 14) + spalign(JSON.stringify(ram_b_free),14) + "\n");