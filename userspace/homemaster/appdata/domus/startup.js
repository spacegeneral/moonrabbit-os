Env.unset("script");

let tinfo = Term.get_terminfo();
let border = 20;
let winwidth = (tinfo.screensize.w - (border*3)) / 2;

let sleeptime = 1.5;  // TODO: for some reason, spawning the tasks too fast crashes cc

Env.set("quiet", "1", 1);
Proc.epspawn_app("mofuterm", [["program", "ush"], ["x", JSON.stringify(border)], ["y", JSON.stringify(border)], ["width", JSON.stringify(winwidth)], ["height", JSON.stringify(tinfo.screensize.h - (border*2))]]);
Time.user_sleep(sleeptime);
Proc.spawn_app("mofuterm", Sec.Human, Sec.Citizen, [["x", JSON.stringify(winwidth + (border*2))], ["y", JSON.stringify(border)], ["width", JSON.stringify(winwidth)], ["height", "54"], ["program", "rosh"], ["script", "f,def,home,app,termclock.js"]]);
Time.user_sleep(sleeptime);
Proc.spawn_app("mofuterm", Sec.Human, Sec.Citizen, [["program", "ush"], ["x", JSON.stringify(winwidth + (border*2))], ["y", "94"], ["width", JSON.stringify(winwidth)], ["height", "280"]]);
Time.user_sleep(sleeptime);
Proc.epspawn_app("mofuterm", [["program", "ush"], ["x", JSON.stringify(winwidth + (border*2))], ["y", "394"], ["width", JSON.stringify(winwidth)], ["height", "354"]]);
Env.unset("quiet");