Term.printbb("Welcome to MoonrabbitOS!\n");
Term.printf("Type 'help'<enter> to get help.\n");
if (Shell.search_cmd_any("domus") !== "domus") {
    Term.printf("The graphical shell domus appears to be installed.\n");
}
Term.printbb("[color=highlight]DIRE WARNING[/color]: MoonrabbitOS is currently in active developmenent and is primarily aimed towards extraordinary users.\n");
Term.printf("If you happen to be a less-than-extraordinary person, please abandon the terminal now and notify a more qualified candidate.\n");
Term.printf("You can customize this startup script located at: f,def,home,appdata,init,boot_time.js\n");
Term.printf("Populating JIT cache...\n");
Fs.mkdir("f,def,cache");
Fs.chsec("f,def,cache", Sec.Human, Sec.Citizen);
Proc.spawn_app("sharedrv", Sec.Human, Sec.Citizen, [["debug", "1"]]);
Proc.spawn_app("cache_buildenv", Sec.Human, Sec.Citizen, []);