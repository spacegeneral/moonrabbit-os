#ifndef _LIBGUI_DOMUSCLIENT_H
#define _LIBGUI_DOMUSCLIENT_H


#include <libgui/msg.h>
#include <sys/keycodes.h>
#include <collections/array.h>


typedef void (*opendoor_mouse_callback) (LibguiMsgMouseEvent *event);
typedef void (*opendoor_transform_callback) (Rect *event, Rect *old);
typedef void (*opendoor_stdin_callback) (char *key);
typedef void (*opendoor_keycode_callback) (KeyEvent *keycode);


typedef struct OpenDoor {
    Bitmap face;
    char name[MAX_DOORNAME];
    int border;
    bool glued;
    bool has_focus;
    Color24 border_color;
    Color24 *transparency_colorkey;
    unsigned int door_id;
    DoorSystemStatus status;
    opendoor_mouse_callback mouse_callback;
    opendoor_transform_callback transform_callback;
    opendoor_stdin_callback stdin_callback;
    opendoor_keycode_callback keycode_callback;
    
    Array *buttons;
} OpenDoor;

typedef struct DoorButton {
    Rect area_rel;
    int border;
    Color24 border_color;
    OpenDoor *parent;
    void (*onclick) (void);
} DoorButton;


bool open_main_door_advanced(OpenDoor *door, Rect area, const char *name, int border, Color24 border_color, Color24 *transparency_colorkey, bool glued);
bool open_main_door(OpenDoor *door, Rect area, const char *name);
bool open_door_advanced(OpenDoor *door, Rect area, const char *name, int border, Color24 bg_color, Color24 border_color, Color24 *transparency_colorkey, unsigned int parent_door_id, bool glued);
bool OpenDoor_update(OpenDoor *door, bool force_redraw);
void OpenDoor_handle_io(OpenDoor *door);
bool OpenDoor_close(OpenDoor *door);


DoorButton *DoorButton_create_advanced(OpenDoor *parent, Rect area, int border, Color24 border_color);
DoorButton *DoorButton_create(OpenDoor *parent, Rect area);
void DoorButton_redraw(DoorButton *button);
void DoorButton_destroy(DoorButton *button);


#endif
