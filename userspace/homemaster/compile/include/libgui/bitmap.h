#ifndef _LIBGUI_BITMAP_H
#define _LIBGUI_BITMAP_H


#include <graphics.h>
#include <libgui/color.h>


#define PIXELAT(bitmap_ptr, coord_x, coord_y) ((bitmap_ptr)->colordata + ((coord_y) * (bitmap_ptr)->geom.w) + (coord_x))
#define PIXELAT_FAST(bitmap_ptr, coord_x, coord_y, log2_width) ((bitmap_ptr)->colordata + ((coord_y) << (log2_width)) + (coord_x))


Bitmap make_bitmap(Rect geom, Color24 fill);
Bitmap make_bitmap_from_existing(Rect geom, Color24 fill, int *colordata);
Bitmap clone_bitmap(Bitmap *original);
Bitmap crop_bitmap(Bitmap *source, Rect croparea);
void destroy_bitmap(Bitmap *delenda);
void overlay_bitmap(Bitmap *dest, Bitmap *overlay, Color24 *colorkey);
void area_overlay_bitmap(Bitmap *dest, Bitmap *overlay, Rect *area, Color24 *colorkey);
void relocate_bitmap(Bitmap *dest, Point coord);
Bitmap shallow_relocated_copy(Bitmap *source, Point coord);


Bitmap safe_crop_bitmap(Bitmap *source, Rect croparea);
void safe_overlay_bitmap(Bitmap *dest, Bitmap *overlay, Color24 *colorkey);


#endif
