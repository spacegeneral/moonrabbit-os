#ifndef _LIBGUI_MSG_H
#define _LIBGUI_MSG_H


#include <graphics.h>
#include <time.h>
#include <libgui/color.h>


#define MAX_DOORNAME 256


#define DOOR_TOPIC_PREFIX "/usr/doors/"
#define MAKEDOOR_SERVICE "/usr/doors/makedoor"
#define KILLDOOR_SERVICE "/usr/doors/killdoor"

#define UPDATEDOOR_FACE_SERVICE_TEMPLATE "/usr/doors/%d/update/face"
#define DOOR_EVENT_MOUSE_TOPIC_TEMPLATE "/usr/doors/%d/event/mouse"
#define DOOR_EVENT_STDIN_TOPIC_TEMPLATE "/usr/doors/%d/event/stdin"
#define DOOR_EVENT_KEYCODE_TOPIC_TEMPLATE "/usr/doors/%d/event/keycode"
#define DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE "/usr/doors/%d/transform"



typedef enum DoorSystemStatus {
    DOORS_OK,
    DOORS_ERR_SERV_CREATION,
    DOORS_ERR_TOPIC_CREATION,
    DOORS_ERR_INVALID_PARENT,
    DOORS_ERR_DOES_NOT_EXIST,
    
} DoorSystemStatus;


typedef struct LibguiMsgMakedoor {
    Bitmap face;
    char name[MAX_DOORNAME];
    int border;
    bool glued;
    Color24 border_color;
    Color24 *transparency_colorkey;
    unsigned int parent_door_id;
    // response
    unsigned int door_id;
    DoorSystemStatus status;
} LibguiMsgMakedoor;

typedef struct LibguiMsgKilldoor {
    unsigned int door_id;
    // response
    DoorSystemStatus status;
} LibguiMsgKilldoor;

typedef struct LibguiMsgUpdatedoorFace {
    Bitmap face;
    bool redraw;
    // response
    DoorSystemStatus status;
} LibguiMsgUpdatedoorFace;

typedef struct LibguiMsgMouseEvent {
    int x,y;
    bool btn_left, btn_mid, btn_right;
    bool is_enter;
    bool is_leave;
    bool is_click;
    bool is_dbclick;
    clock_t timestamp;
} LibguiMsgMouseEvent;

typedef struct LibguiMsgTransformEvent {
    Rect geom;
    bool has_focus;
} LibguiMsgTransformEvent;

#endif
