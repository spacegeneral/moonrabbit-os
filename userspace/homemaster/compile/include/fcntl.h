#ifndef _FCNTL_H
#define _FCNTL_H

#include <unistd.h>

/* open-only flags */
#define	O_RDONLY	0x0000		/* open for reading only */
#define	O_WRONLY	0x0001		/* open for writing only */
#define	O_RDWR		0x0002		/* open for reading and writing */
#define	O_ACCMODE	0x0003	/* mask for above modes */
#define O_BINARY    0x0004
#define O_CREAT     0x0008
#define O_TRUNC     0x0010

#endif
 
