#ifndef _SYS_TERM_H
#define _SYS_TERM_H

#include <stdbool.h>
#include <stddef.h>
#include <geom.h>
#include <graphics.h>

typedef enum WritingDirection {
    WD_BOOK,
    WD_RABBI,
    WD_TEGAMI,
    WD_LEMURIA,
} WritingDirection;


typedef struct TermInfo {
    Rect termsize;
    Rect screensize;
    Point cursor;
    WritingDirection writing;
    int cursor_enabled;
    int support_bitmap;
    int foreground_color;
    int background_color;
    int scroll_top;
    int scroll_bottom;
    unsigned int flags;
} TermInfo;

#define TERM_FLAG_UNDERLINE 0b00000000000000000000000000000001
#define TERM_FLAG_SELECTED  0b00000000000000000000000000000010

#define TERM_FLAG_BOLD      0b00000000000000000000000000000100
#define TERM_FLAG_ITALICS   0b00000000000000000000000000001000
#define TERM_FLAG_GOTHIC    0b00000000000000000000000000010000
#define TERM_FLAG_SCRIPT    0b00000000000000000000000000100000


#define TERM_FLAG_ZOOMABLE  0b00000000000000010000000000000000

#define TERM_FLAG_RW_MASK 0x0000FFFF
#define TERM_FLAG_RO_MASK 0xFFFF0000



typedef enum TermCommandVerb {
    SetCursorPos,
    ToggleCursor,
    SetColors,
    SetScrollRange,
    ScrollReverse,
    CLS,
    Putchar,
    Puttag,
    SetWritingDirection,
    SetTermFlags,
    SetZoomLevel,  // not all terms support this
} TermCommandVerb;


typedef struct TermOutMessage {
    TermCommandVerb verb;
    union {
        struct {
            int x;
            int y;
        } position;
        struct {
            int foreground;
            int background;
        } colors;
        struct {
            int start;
            int end;
        } range;
        struct {
            bool enabled;
        } toggle;
        struct {
            char character;
        } character;
        struct {
            char tag_character;
        } tag;
        struct {
            WritingDirection direction;
        } writing;
        struct {
            unsigned int flags;
        } flags;
        struct {
            int level;
        } zoom;
    } msg;
} TermOutMessage;


TermInfo getterminfo();


#define SETCOLOR_UNCHANGED (-1)


typedef struct TermBitmapRequest {
    Bitmap *bitmap;
    int colorkey;
    // response
    int status;
} TermBitmapRequest;


#define COLORKEY_NONE 0x9DC9C3

#define BITMAP_DISPLAY_OK 0
#define BITMAP_DISPLAY_UNSUPPORTED -1




#endif
 
