#ifndef _SYS_FS_H
#define _SYS_FS_H 1

#include <enumtricks.h>
#include <stdbool.h>
#include <stddef.h>
#include <time.h>

#define MAX_FILENAME_LENGTH 256
#define MAX_FS_DRIVER_NAME_LENGTH 256
#define OPENFILE_TOKEN_LEN 16
#define MAX_PATH_LENGTH 4096
#define MAX_FOPEN_MODESTR_LENGTH 16
#define MAX_FILEIO_PAYLOAD (MAX_PATH_LENGTH*2)

#define ROOTFS_DRIVER_NAME "rootfs"

#define DIR_SEPARATOR ','

#define EOF (-1)

typedef enum MountTypes {
    M_INTERNALFS = 4,
    M_INITRAMFS,
    M_ALIENFS,
    
} MountTypes;


#define FOREACH_FILETYPE(FILETYPE) \
    FILETYPE(File) \
    FILETYPE(Directory) \
    FILETYPE(Link) \
    FILETYPE(Unsupported) \
    
typedef enum FileType {
    FOREACH_FILETYPE(GENERATE_ENUM)
} FileType;

static __attribute__((unused)) const char* file_type_name[] = {
    FOREACH_FILETYPE(GENERATE_STRING)
};


typedef struct FILE_Interface {
    void *user_data;
    struct FILE*  (*fopen)  (const char *pathname, const char *mode);
    int    (*fclose) (struct FILE *stream);
    size_t (*fread)  (void *ptr, size_t size, size_t nmemb, struct FILE *stream);
    size_t (*fwrite) (const void *ptr, size_t size, size_t nmemb, struct FILE *stream);
    int    (*fflush) (struct FILE *stream);
    int    (*fseek)  (struct FILE *stream, long offset, int whence);
    long   (*ftell) (struct FILE *stream);
} FILE_Interface;


typedef struct LibzOpenFileUserData {
    void *memstore;
    size_t mem_length;
    size_t super_seek;
    bool dirty;
} LibzOpenFileUserData;


typedef struct FILE {
    void *reserved;
    size_t seek_pos;
    FILE_Interface interface;
    char driver[MAX_FS_DRIVER_NAME_LENGTH];
    unsigned char unique_token[OPENFILE_TOKEN_LEN];
    size_t id;
    unsigned int owner_agent_id;
} FILE;



typedef struct __attribute__((packed)) FileMetaData {
    unsigned race_attrib;
    unsigned class_attrib;
    unsigned executable;
    unsigned writable;
    unsigned readable;
    time_t creation_time;
    time_t modification_time;
} FileMetaData;

static FILE __attribute__((unused)) *stdin = (FILE*)0x1;
static FILE __attribute__((unused)) *stdout = (FILE*)0x2;
static FILE __attribute__((unused)) *stderr = (FILE*)0x2;


typedef struct __attribute__((packed)) DirEnt {
    char name[MAX_FILENAME_LENGTH];
    FileMetaData metadata;
    FileType filetype;
    unsigned long long filesize;
} DirEnt;


int listdir(const char *dirname, DirEnt **entries, size_t *numentries);
int mkdir(const char *pathname);
int touch(const char *pathname);
int link(char *path_from, char *path_to);
int remove(const char *pathname);
int chsec(const char *pathname, unsigned newrace, unsigned newclass);
bool file_exists(const char *pathname);
int move(char *path_from, char *path_to);
int mount(char *diskfile, char *mountpoint, char *fsname);
int unmount(char *mountpoint);
int get_mount_info(const char *pathname, char *additional_info, char *explicit_pathname);
int scan_media();
char *getcwd(char *buf, size_t size);
char *getwd(char *buf);


#define FOREACH_FSSERVICE(FSSERVICE) \
    FSSERVICE(fsservice_create_file) \
    FSSERVICE(fsservice_create_directory) \
    FSSERVICE(fsservice_create_link) \
    FSSERVICE(fsservice_remove) \
    FSSERVICE(fsservice_count_directory_entries) \
    FSSERVICE(fsservice_get_directory_entry_at) \
    FSSERVICE(fsservice_file_exists) \
    FSSERVICE(fsservice_move) \
    FSSERVICE(fsservice_fopen) \
    FSSERVICE(fsservice_fclose) \
    FSSERVICE(fsservice_fflush) \
    FSSERVICE(fsservice_fread) \
    FSSERVICE(fsservice_fwrite) \
    FSSERVICE(fsservice_fseek) \
    FSSERVICE(fsservice_setmount) \
    FSSERVICE(fsservice_unsetmount) \
    FSSERVICE(fsservice_chsec) \
    
    
typedef enum FSServiceName {
    FOREACH_FSSERVICE(GENERATE_ENUM)
} FSServiceName;

static const char __attribute__((unused)) *fs_service_name[] = {
    FOREACH_FSSERVICE(GENERATE_STRING)
};


typedef struct __attribute__((packed)) FSServiceMessage {
    FSServiceName service_name;
    
    union {
        
        struct {
            const char pathname[MAX_PATH_LENGTH];
        } create_file;
        
        struct {
            const char pathname[MAX_PATH_LENGTH];
        } create_directory;
        
        struct {
            char path_from[MAX_PATH_LENGTH];
            char path_to[MAX_PATH_LENGTH];
        } create_link;
        
        struct {
            const char pathname[MAX_PATH_LENGTH];
        } remove;
        
        struct {
            const char pathname[MAX_PATH_LENGTH];
            size_t numentries;
        } count_directory_entries;
        
        struct {
            const char pathname[MAX_PATH_LENGTH];
            size_t position;
            DirEnt entry;
        } get_directory_entry_at;
        
        struct {
            const char pathname[MAX_PATH_LENGTH];
            bool result;
        } file_exists;
        
        struct {
            char path_from[MAX_PATH_LENGTH];
            char path_to[MAX_PATH_LENGTH];
        } move;
        
        struct {
            const char pathname[MAX_PATH_LENGTH];
            const char mode[MAX_FOPEN_MODESTR_LENGTH];
            FILE result;
        } fopen;
        
        struct {
            FILE stream;
        } fclose;
        
        struct {
            FILE stream;
        } fflush;
        
        struct {
            uint8_t payload[MAX_FILEIO_PAYLOAD];
            size_t size; 
            size_t nmemb; 
            FILE stream;
            size_t result;
        } fread;
        
        struct {
            const uint8_t payload[MAX_FILEIO_PAYLOAD];
            size_t size; 
            size_t nmemb; 
            FILE stream;
            size_t result;
        } fwrite;
        
        struct {
            FILE stream;
            long offset;
            int whence;
        } fseek;
        
        struct {
            const char diskfile[MAX_PATH_LENGTH];
            const char mountpoint[MAX_PATH_LENGTH];
        } setmount;
        
        struct {
            const char mountpoint[MAX_PATH_LENGTH];
        } unsetmount;
        
        struct {
            const char pathname[MAX_PATH_LENGTH];
            unsigned newrace;
            unsigned newclass;
        } chsec;
        
    } msg;
    int status;
} FSServiceMessage;


void make_fsservice_request(char *driver, FSServiceMessage *request);


#endif
 
