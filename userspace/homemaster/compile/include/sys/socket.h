#ifndef _SYS_SOCKET_H
#define _SYS_SOCKET_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <enumtricks.h>
#include <sys/network.h>



typedef enum SocketType {
    SOCK_STREAM,
    SOCK_DGRAM,
    
} SocketType;


typedef enum SocketProtocol {
    IPPROTO_TCP,
    IPPROTO_UDP,
    
} SocketProtocol;


typedef struct sockaddr {
    IpAddress addr;
    int port;
} sockaddr;

bool sockaddr_equals(const sockaddr *a, const sockaddr *b);

#define SOCKADDR_STRING_LENGTH (IP_STRING_LENGTH + 10)
void format_sockaddr(char *str, const sockaddr *sock);


typedef struct SockUDPDatagram {
    sockaddr from;
    size_t data_length;
    unsigned char data[MAX_UDP_MTU];
} SockUDPDatagram;

typedef struct SockTCPDatagram {
    sockaddr from;
    size_t data_length;
    unsigned char data[MAX_UDP_MTU];
} SockTCPDatagram;

typedef struct SockANYDatagram {
    sockaddr from;
    size_t data_length;
    unsigned char data[1];
} SockANYDatagram;



static const sockaddr __attribute__((unused)) sockaddr_nofilter_v4 = {.port = 0, .addr = {.version = 4, .address = {0,0,0,0}}};
static const sockaddr __attribute__((unused)) sockaddr_nofilter_v6 = {.port = 0, .addr = {.version = 6, .address = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}}};


typedef enum SocketControlServiceVerb {
    sock_get_free_number,
    sock_setup_socket_with_number,
    sock_teardown_socket_with_number,
    sock_target_socket,
    sock_source_socket,
    sock_filter_socket,
    sock_flush_socket_explicit,
    
} SocketControlServiceVerb;

typedef struct SocketControlServiceMessage {
    SocketControlServiceVerb verb;
    
    union {
        struct {
            uint32_t free_number;
        } get_free_number;
        struct {
            uint32_t number;
            int type;
            int protocol;
        } setup_socket_with_number;
        struct {
            uint32_t number;
        } teardown_socket_with_number;
        struct {
            uint32_t number;
            sockaddr sock_addr;
            bool use_port;
            bool use_addr;
        } target_socket;
        struct {
            uint32_t number;
            sockaddr sock_addr;
            bool use_port;
            bool use_addr;
        } source_socket;
        struct {
            uint32_t number;
            sockaddr sock_addr;
            bool use_port;
            bool use_addr;
        } filter_socket;
        struct {
            uint32_t number;
        } flush_socket_explicit;
    } msg;
    
    int status;
} SocketControlServiceMessage;



typedef struct socket_t {
    unsigned int number;
    int errno;
    SocketType type;
    SocketProtocol protocol;
    char send_topic[4096];
    char recv_topic[4096];
} socket_t;

#define DEFAULT_SOCK_STREAM_BUFFER 16384
#define DEFAULT_SOCK_DGRAM_BUFFER 64



int socket_get_free_number(unsigned int *number);
int socket_setup_with_number(unsigned int number, SocketType type, SocketProtocol protocol);
int socket_teardown_with_number(unsigned int number);
int socket_target(unsigned int number, sockaddr *address, bool use_port, bool use_addr);
int socket_source(unsigned int number, sockaddr *address, bool use_port, bool use_addr);
int socket_filter(unsigned int number, sockaddr *address, bool use_port, bool use_addr);
int socket_flush(unsigned int number);



socket_t *socket(SocketType type, SocketProtocol protocol);
int socket_close(socket_t *sock);
size_t send(socket_t *sock, void *buffer, size_t length);
size_t sendto(socket_t *sock, void *buffer, size_t length, sockaddr *dest_addr);
size_t recv(socket_t *sock, void *buffer, size_t length);
//WARNING: in stream mode, data from different source will be merged into a single stream
//         and src_addr won't be set to anything.
//         Filtering can be used to target a stream socket towards a specific remote address.
size_t recvfrom(socket_t *sock, void *buffer, size_t length, sockaddr *src_addr);


#endif
 
