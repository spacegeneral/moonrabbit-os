#ifndef _SYS_AUDIO_H
#define _SYS_AUDIO_H


#include <stdint.h>


void beep(unsigned frequency, unsigned long long int duration);
void hifi_beep(unsigned frequency, unsigned long long int duration);
void beep_on(unsigned frequency);
void beep_off();


typedef struct AudioEventMessage {
    unsigned int frequency;
    unsigned int flags;
} AudioEventMessage;


#define AUDIO_EVENT_MSG_FLAG_NOTE_ON    0b00000001

#define AUDIO_PACKET_SIZE 64*1024  // 64K

/*
 * The default audio sink accepts 16bit signed PCM stereo input and plays it at a rate of 44100 Hz
 * 
 */


#endif
