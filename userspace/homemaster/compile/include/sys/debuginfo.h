#ifndef _SYS_DEBUGINFO_H
#define _SYS_DEBUGINFO_H


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <collections/array.h>


#define MAX_SECTIONNAME_LENGTH 256


#define STAB_N_SO 0x64
#define STAB_N_SLINE 0x44
#define STAB_N_FUN 0x24
#define STAB_N_UNDF 0x00



typedef struct TaskSection {
    char name[MAX_SECTIONNAME_LENGTH];
    uintptr_t start;
    size_t length;
} TaskSection;

void TaskSection_append_new(Array *toarray, const char* name, uintptr_t start, size_t length);


typedef struct StabSymbol {
    unsigned int n_strx;         /* index into string table of name */
    unsigned char n_type;         /* type of symbol */
    unsigned char n_other;        /* misc info (usually empty) */
    unsigned short n_desc;        /* description field */
    unsigned int n_value;        /* value of symbol */
} StabSymbol;


typedef struct TaskDebuginfo {
    TaskSection* mem_map;
    size_t mem_map_length;
    
    StabSymbol* stabs;
    char* stabstr;
    size_t num_stab_symbols;
    
    bool valid;
} TaskDebuginfo;


void TaskDebuginfo_destroy(TaskDebuginfo *debug_info);
void TaskDebuginfo_append_mem_section(TaskDebuginfo *debug_info, const char *name, uintptr_t start, size_t length);
int TaskDebuginfo_find_section_by_start(TaskDebuginfo *debug_info, uintptr_t start);
void TaskDebuginfo_remove_section_by_start(TaskDebuginfo *debug_info, uintptr_t start);
void TaskDebuginfo_clone(TaskDebuginfo **clone, TaskDebuginfo *source);

#endif
