#ifndef _SYS_MOUSE_H
#define _SYS_MOUSE_H


#include <stdbool.h>


#define PS2_MOUSE_ID 0


typedef struct MouseEvent {
    bool left_btn_down, middle_btn_down, right_btn_down;
    unsigned char pointer_id;
    int delta_x, delta_y;
    unsigned long long int timestamp;
} MouseEvent;



#endif
