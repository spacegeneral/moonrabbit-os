#ifndef _SYS_LOADER_H
#define _SYS_LOADER_H 1

#include <stddef.h>
#include <stdint.h>

// Loader status codes
#define LAUNCHELF_ERR_FOPEN (-10)
#define LAUNCHELF_ERR_INIT  (-20)
#define LAUNCHELF_ERR_LOAD  (-30)
#define LAUNCHELF_ERR_RELOC (-40)
#define LAUNCHELF_OK        0


unsigned char *userland_loader(const char* filename);


#endif
