#ifndef _LIBEDIT_CHARACTERLINE_H
#define _LIBEDIT_CHARACTERLINE_H


#include <stdbool.h>
#include <stddef.h>
#include <graphics.h>
#include <stdio.h>
#include <libgui/text.h>



#define CELLS_GROW_FACTOR 512


typedef struct CharacterCell {
    char utf8[6];
    int foreground_color;
    int background_color;
} CharacterCell;


typedef struct CharacterLine {
    CharacterCell *cells;
    size_t num_cells_avail;
    size_t num_cells_used;
    int cursor_x;
    bool ins_mode;
    int trailing_erase;
    int default_foreground_color;
    int default_background_color;
} CharacterLine;



void CharacterLine_init(CharacterLine *line);
void CharacterLine_grow(CharacterLine *line);
void CharacterLine_destroy(CharacterLine *line);
void CharacterLine_putchar(CharacterLine *line, KeyEvent *key);
void CharacterLine_backspace(CharacterLine *line);
void CharacterLine_delete(CharacterLine *line);
void CharacterLine_cursor_left(CharacterLine *line);
void CharacterLine_cursor_right(CharacterLine *line);
void CharacterLine_print(CharacterLine *line);
void CharacterLine_print_up_to_cursor(CharacterLine *line);
void CharacterLine_print_color(CharacterLine *line, int *current_foreground_color, int *current_background_color);
void CharacterLine_print_up_to_cursor_color(CharacterLine *line, int *current_foreground_color, int *current_background_color);
void CharacterLine_get_str(CharacterLine *line, char *str, int start_offset);
bool CharacterLine_save(CharacterLine *line, FILE *tofile);
void CharacterLine_digest(CharacterLine *line, char *str);
void CharacterLine_append_other(CharacterLine *line, CharacterLine *other, size_t offset);
void CharacterLine_replace_with(CharacterLine *line, char *str);
int CharacterLine_search(CharacterLine *line, char *str, int start_offset);
size_t CharacterLine_onscreen_length(CharacterLine *line, BitmapFont *font, int start_offset);
size_t CharacterLine_onscreen_height(CharacterLine *line, BitmapFont *font, int start_offset);


#endif
