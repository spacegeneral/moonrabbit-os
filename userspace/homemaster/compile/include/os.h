#ifndef _OS_H
#define _OS_H

#include <stddef.h>
#include <stdint.h>
#include <geom.h>
#include <graphics.h>
#include <time.h>
#include <sys/sec.h>
#include <sys/proc.h>
#include <sys/fs.h>
#include <sys/mq.h>
#include <sys/serv.h>
#include <enumtricks.h>


#define FOREACH_SYSCALL(SYSCALLFUN) \
    SYSCALLFUN(SyscallMalloc) \
    SYSCALLFUN(SyscallFree) \
    SYSCALLFUN(SyscallMlength) \
    SYSCALLFUN(SyscallGiftMemarea) \
    SYSCALLFUN(DirectVideoCommand) \
    SYSCALLFUN(SyscallGetMountInfo) \
    SYSCALLFUN(SyscallMount) \
    SYSCALLFUN(SyscallUnMount) \
    SYSCALLFUN(SyscallScanMedia) \
    SYSCALLFUN(SyscallIdent) \
    SYSCALLFUN(SyscallExit) \
    SYSCALLFUN(SyscallPrelaunch) \
    SYSCALLFUN(SyscallHenshin) \
    SYSCALLFUN(SyscallTaskSwitch) \
    SYSCALLFUN(SyscallExpireCC) \
    SYSCALLFUN(SyscallContinuation) \
    SYSCALLFUN(SyscallYield) \
    SYSCALLFUN(SyscallClone) \
    SYSCALLFUN(SyscallAbort) \
    SYSCALLFUN(SyscallDisown) \
    SYSCALLFUN(SyscallAdopt) \
    SYSCALLFUN(SyscallKill) \
    SYSCALLFUN(SyscallGettime) \
    SYSCALLFUN(SyscallMsleep) \
    SYSCALLFUN(SyscallClock) \
    SYSCALLFUN(SyscallBeep) \
    SYSCALLFUN(SyscallAdvertiseChannel) \
    SYSCALLFUN(SyscallRetireChannel) \
    SYSCALLFUN(SyscallSubscribe) \
    SYSCALLFUN(SyscallUnsubscribe) \
    SYSCALLFUN(SyscallPublish) \
    SYSCALLFUN(SyscallPublishMulti) \
    SYSCALLFUN(SyscallPoll) \
    SYSCALLFUN(SyscallChannelStats) \
    SYSCALLFUN(SyscallAdvertiseService) \
    SYSCALLFUN(SyscallRetireService) \
    SYSCALLFUN(SyscallAcceptService) \
    SYSCALLFUN(SyscallRequestService) \
    SYSCALLFUN(SyscallCompleteService) \
    SYSCALLFUN(SyscallPollService) \
    
    



typedef enum SyscallCode {
    FOREACH_SYSCALL(GENERATE_ENUM)
    NUM_AVAILABLE_SYSCALLS
} SyscallCode;

static const char __attribute__((unused)) *syscall_names[] = {
    FOREACH_SYSCALL(GENERATE_STRING)
};



typedef struct SyscallMallocParam {
    size_t size;
    char *filename;
    unsigned linenum;
    void *result;
} SyscallMallocParam;

typedef struct SyscallMlengthParam {
    void *memarea;
    size_t result;
} SyscallMlengthParam;

typedef struct SyscallGiftMemareaParam {
    void *memarea;
    unsigned new_owner_id;
    size_t result;
} SyscallGiftMemareaParam;

typedef struct DirectVideoCommandParam {
    int verb;
    int argument;
    void *result;
    int status;
} DirectVideoCommandParam;

typedef struct SyscallPrelaunchKernelParam {
    const char* filename;
    char **copy_env;
    unsigned int* new_taskid;
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    int *return_code_here;
    int result;
} SyscallPrelaunchKernelParam;

typedef struct SyscallPrelaunchUserParam {
    unsigned char* raw_elf;
    char *program_name;
    char **copy_env;
    unsigned int* new_taskid;
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    int *return_code_here;
    int result;
} SyscallPrelaunchUserParam;

typedef struct SyscallHenshinParam {
    unsigned char* ready_elf;
    uintptr_t entrypoint;
    TaskDebuginfo *dbginfo;
    char *program_name;
    char **copy_env;
} SyscallHenshinParam;

typedef struct SyscallExpireCCParam {
    uintptr_t resume;
    unsigned long long expire;
} SyscallExpireCCParam;

typedef struct SyscallCloneParam {
    uint32_t entrypoint;
    char **copy_env;
    unsigned int* new_taskid;
    int result;
} SyscallCloneParam;

typedef struct SyscallIdentParam {
    ProcIdent result;
} SyscallIdentParam;

typedef struct SyscallDisownParam {
    unsigned int child_task_id;
    int result;
} SyscallDisownParam;

typedef struct SyscallAdoptParam {
    unsigned int child_task_id;
    int result;
} SyscallAdoptParam;

typedef struct SyscallKillParam {
    unsigned int task_id;
    int result;
} SyscallKillParam;

typedef struct SyscallTaskSwitchParam {
    unsigned int newtask;
    int result;
} SyscallTaskSwitchParam;

typedef struct SyscallBeepParam {
    uint16_t frequency;
    unsigned long long int duration;
} SyscallBeepParam;

typedef struct SyscallGetMountInfoParam {
    char *pathname;
    char *additional;
    char *explicit;
    int result;
} SyscallGetMountInfoParam;

typedef struct SyscallMountParam {
    char *fsname;
    char *mountpoint;
    char *drivertopic;
    int result;
} SyscallMountParam;

typedef struct SyscallUnMountParam {
    char* mountpoint;
    char* drivertopic;
    int result;
} SyscallUnMountParam;

typedef struct SyscallAdvertiseChannelParam {
    char* topic_name;
    unsigned int max_queue_length;
    unsigned int message_length;
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    SecurityPolicy policy;
    MQStatus result;
} SyscallAdvertiseChannelParam;

typedef struct SyscallTopicParam {
    char* topic_name;
    MQStatus result;
} SyscallTopicParam;

typedef struct SyscallPubPollParam {
    char* topic_name;
    void* message;
    MQStatus result;
} SyscallPubPollParam;

typedef struct SyscallPublishMultiParam {
    char* topic_name;
    void* message;
    size_t num_messages;
    size_t* num_written;
    MQStatus result;
} SyscallPublishMultiParam;

typedef struct SyscallChannelStatsParam {
    char* topic_name;
    ChannelStats* stats;
    MQStatus result;
} SyscallChannelStatsParam;

typedef struct SyscallClockParam {
    clock_t* result;
} SyscallClockParam;

typedef struct SyscallMsleepParam {
    unsigned int milliseconds;
    unsigned int result;
} SyscallMsleepParam;

typedef struct SyscallAdvertiseServiceParam {
    char *topic_name;
    unsigned int frame_length;
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    ServStatus result;
} SyscallAdvertiseServiceParam;

typedef struct SyscallRetireServiceParam {
    char *topic_name;
    ServStatus result;
} SyscallRetireServiceParam;

typedef struct SyscallMessageServiceIDParam {
    char *topic_name;
    unsigned int request_id;
    ServStatus result;
} SyscallMessageServiceIDParam;

typedef struct SyscallRequestServiceParam {
    char *topic_name;
    void *message;
    unsigned int *request_id;
    ServStatus result;
} SyscallRequestServiceParam;

typedef struct SyscallAcceptServiceParam {
    char *topic_name;
    void *request;
    unsigned int *request_id;
    unsigned int *requestor_task_id;
    ServStatus result;
} SyscallAcceptServiceParam;



void invoke_syscall(unsigned int code, void *param_ptr);


#endif 
