#ifndef _STDLIB_H
#define _STDLIB_H 1

#include <sys/cdefs.h>
#include <stddef.h>
#include <liballoc/liballoc.h>

#ifdef __cplusplus
extern "C" {
#endif


extern char **environ;


__attribute__((__noreturn__))
void abort(void);
void exit(int code);

char* itoa(int num, char* str, int base);
char* lltoa(long long num, char* str, int base);
char* ltoa(long num, char* str, int base);

char atob(const char* nptr);
int atoi(const char *nptr);
long atol(const char *nptr);
long long atoll(const char *nptr);
int atoi_hex(const char* nptr);

double atof(const char *nptr);

#ifdef __FILENAME__
#define __MALLOC_FILE__ __FILENAME__
#else
#define __MALLOC_FILE__ __FILE__
#endif

#if defined(__is_libk) || defined(__is_kernel)

void *rich_malloc(size_t size, char *filename, unsigned line);
void *rich_realloc(void *ptr, size_t size, char *filename, unsigned line);
void *rich_calloc(size_t nmemb, size_t size, char *filename, unsigned line);

#define malloc(size) rich_malloc((size), __MALLOC_FILE__, __LINE__)
#define malloc_coarse(size) malloc((size))
#define realloc(ptr, size) rich_realloc((ptr), (size), __MALLOC_FILE__, __LINE__)
#define realloc_coarse(ptr, size) realloc((ptr), (size))
#define calloc(nmemb, size) rich_calloc((nmemb), (size), __MALLOC_FILE__, __LINE__)
void free(void *ptr);
#define free_coarse(ptr) free((ptr))

#else

void *rich_malloc_coarse(size_t size, char *filename, unsigned line);
void *rich_realloc_coarse(void *ptr, size_t size, char *filename, unsigned line);
void free_coarse(void *ptr);

#define malloc_coarse(size) rich_malloc_coarse((size), __MALLOC_FILE__, __LINE__)
#define malloc(size) liballoc_rich_malloc_fine((size), __MALLOC_FILE__, __LINE__)
#define realloc_coarse(ptr, size) rich_realloc_coarse((ptr), (size), __MALLOC_FILE__, __LINE__)
#define realloc(ptr, size) liballoc_rich_realloc_fine((ptr), (size), __MALLOC_FILE__, __LINE__)
#define calloc(nmemb, size) liballoc_rich_calloc_fine((nmemb), (size), __MALLOC_FILE__, __LINE__)
#define free(ptr) liballoc_free_fine((ptr))

#endif

/* return the size of the allocated memory area starting at ptr. 0 if error. */
size_t mlength(void *ptr);

void mclean2(void ***ptr_to_2lvl_mem, size_t span);

void qsort(void *base, size_t nel, size_t width, int (*comp)(const void *, const void *));

void srand(unsigned int seed);
int rand(void);
int rand_r(unsigned int *seed);

char *getenv(const char *name);
int setenv(const char *name, const char *value, int overwrite);
int unsetenv(const char *name);
size_t getenv_index(const char *name);
size_t getenv_length();

char *getenv_r(const char *name, char ***env);
int setenv_r(const char *name, const char *value, int overwrite, char ***env);
int unsetenv_r(const char *name, char ***env);
size_t getenv_index_r(const char *name, char ***env);
size_t getenv_length_r(char ***env);

void clone_env(char ***to, char **from);

int mblen(const char *s, size_t n);



#ifdef __cplusplus
}
#endif

#endif
