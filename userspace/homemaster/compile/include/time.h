#ifndef _TIME_H
#define _TIME_H 1

#include <sys/cdefs.h>
#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


#define SECSPERMIN      60L
#define MINSPERHOUR     60L
#define HOURSPERDAY     24L
#define SECSPERHOUR     (SECSPERMIN * MINSPERHOUR)
#define SECSPERDAY      (SECSPERHOUR * HOURSPERDAY)
#define DAYSPERWEEK     7
#define MONSPERYEAR     12
#define YEAR_BASE       1900
#define EPOCH_YEAR      1970
#define EPOCH_WDAY      4
#define EPOCH_YEARS_SINCE_LEAP 2
#define EPOCH_YEARS_SINCE_CENTURY 70
#define EPOCH_YEARS_SINCE_LEAP_CENTURY 370
#define isleap(y) ((((y) % 4) == 0 && ((y) % 100) != 0) || ((y) % 400) == 0)


#define CLOCKS_PER_SEC 1000
    

typedef uint64_t time_t;
typedef uint64_t clock_t;


struct timespec
{
    time_t tv_sec;
    long long tv_nsec;
};

struct timeval
{
    time_t tv_sec;
    long long tv_usec;
};

struct tm
{
    int tm_sec;   /* 0-60 */
    int tm_min;   /* 0-59 */
    int tm_hour;  /* 0-23 */
    int tm_mday;  /* 1-31 */
    int tm_mon;   /* 0-11 */
    int tm_year;  /* years since 1900 */
    int tm_wday;  /* 0-6 */
    int tm_yday;  /* 0-365 */
    int tm_isdst; /* >0 DST, 0 no DST, <0 information unavailable */
};


clock_t clock(void);
#define clock_ms() ((clock() * 1000) / CLOCKS_PER_SEC)


unsigned int k_sleep_milli(unsigned int milliseconds);
#ifndef __is_libk
unsigned int u_sleep_milli(unsigned int milliseconds);
#endif

/* Returns the difference between two calendar times in seconds. */
double difftime(time_t time1, time_t time0);

time_t mktime(struct tm *timeptr);

time_t time(time_t *timer);
int gettimeofday(struct timeval *restrict tp, void *restrict tzp);

char *asctime(const struct tm *timeptr);
char *asctime_r(const struct tm *tm, char *buf);

char *ctime(const time_t *timer);
char *ctime_r(const time_t *timep, char *buf);

struct tm *gmtime(const time_t *timer);
struct tm *gmtime_r(const time_t *timep, struct tm *result);

struct tm *localtime(const time_t *timer);
struct tm *localtime_r(const time_t *timep, struct tm *result);

size_t strftime(char *s, size_t maxsize, const char *format, const struct tm *timeptr);


extern clock_t time_started;


#ifdef __cplusplus
}
#endif

#endif 
