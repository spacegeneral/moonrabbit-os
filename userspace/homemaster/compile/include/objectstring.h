#ifndef _OBJECTSTRING_H
#define _OBJECTSTRING_H

#include <stddef.h>



#define OBJECTSTRING_BUMP_FACTOR 1024


typedef struct OString {
    char* text;
    size_t length;
    size_t capacity;
} OString;


void OString_init(OString* string);
void OString_destroy(OString* string);
void OString_clear(OString* string);
void OString_append(OString* string, char character);
void OString_extend(OString* string, char* text);
void OString_eat(OString* string, OString* food);



#endif
