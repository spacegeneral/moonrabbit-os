#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <sys/fs.h>
#include <sys/term.h>
#include <sys/keycodes.h>


static int TERMWIDTH;
static int TERMHEIGHT;

static int disp_width;
static int disp_height;

static int color_text;
static int color_bg;

static bool readonly;

#define SUBCMDLEN 1024

#define LINEAT(offset) ((offset) % disp_width == 0 ? (offset) / disp_width : ((offset) / disp_width) + 1)


char get_ascii(unsigned char character) {
    if (character >= ' ' && character <= '~') return character;
    return '.';
} 

void draw_line(long address, long line, long cursor, unsigned char *screen_buff, long filesize) {
    printf("%08X ", address*disp_width);
    
    for (size_t major_group=0; major_group<2; major_group++) {
        for (size_t minor_group=0; minor_group<8; minor_group++) {
            long index = line*disp_width + major_group*8 + minor_group;
            long absolute = address*disp_width + major_group*8 + minor_group;
            
            if (index == cursor) setcolors(color_bg, color_text);
            
            if (absolute < filesize) {
                printf("%02X", screen_buff[index]);
            } else {
                printf("  ");
            }
            
            if (index == cursor) setcolors(color_text, color_bg);
            
            if (minor_group+1<8) printf(" ");
        }
        if (major_group+1<2) printf("  ");
    }
    
    printf(" |");
    for (size_t charid=0; charid<2*8; charid++) {
        long index = line*disp_width + charid;
        long absolute = address*disp_width + charid;
        if (index == cursor) setcolors(color_bg, color_text);
        if (absolute < filesize) {
            printf("%c", get_ascii(screen_buff[index]));
        } else {
            printf(" ");
        }
        if (index == cursor) setcolors(color_text, color_bg);
    }
    printf("|\n");
}


#define REDRAW(forced) \
{ \
    togglecursor(false); \
    long relative_offset = current_offset - (current_line * disp_width); \
    setcursor(0, 0); \
    for (int h=0; h<disp_height; h++) { \
        draw_line(current_line + h, h, relative_offset, screen_ring, filesize); \
    } \
    togglecursor(true); \
}


#define OVERWRITE_DIGIT(digit) \
{ \
    long relative_offset = current_offset - (current_line * disp_width); \
    if (!readonly) { \
        screen_ring[relative_offset] = ((digit & 0xF) << (edit_highdigit ? 4 : 0)) | \
                                       (screen_ring[relative_offset] & (0xF << (edit_highdigit ? 0 : 4))); \
    } \
    edit_highdigit = !edit_highdigit; \
     \
    if (!readonly) { \
        fseek(fp, current_offset, SEEK_SET); \
        fwrite(screen_ring + relative_offset, 1, 1, fp); \
    } \
     \
    if (edit_highdigit) current_offset++; \
    if (current_offset >= filesize) { \
        current_offset--; \
    } \
}


void print_help() {
    printf("Hex eDitor (xd)\n");
    printf("Minimalist hex editor. The target file is kept open and only loaded one screen at\n");
    printf("a time, thus allowing manipulation of large files (but the overall responsiveness\n");
    printf("is penalized).\n");
    printf("    Parameters:    file=[path]    file to open\n");
    printf("                   pure           disable transparent decompression of .zz files\n");
    printf("                   readonly       prevent modifications to the file content\n");
    printf("    Controls:      arrow keys     move cursor\n");
    printf("                   G              Go to offset. Accepts decimal and hexadecimal\n");
    printf("                                  (0x-prefixed) parameters\n");
    printf("                   Escape         quit the program\n");
    printf("                   0-9A-F         overwrite the byte under the cursor.\n");
    printf("                                  WARNING: modifications are written instantly,\n");
    printf("                                  there is no \"save\" command!\n");
    
}


int main(void) {
    char subcommand[SUBCMDLEN];
    char *fname;
    
    if (getenv("help") != NULL) {
        print_help();
        exit(0);
    }
    
    char *file_param = getenv("file");
    
    if (file_param == NULL) {
        printf("File to read: ");
        getsn(subcommand, 1024);
        rstrip(subcommand);
        fname = subcommand;
    } else {
        fname = file_param;
    }
    
    readonly = (getenv("readonly") != NULL);
    
    
    TermInfo term = getterminfo();
    TERMWIDTH = term.termsize.w;
    TERMHEIGHT = term.termsize.h;
    color_text = term.foreground_color;
    color_bg = term.background_color;
    
    bool open_pure = (getenv("pure") != 0);
    
    FILE *fp;
    if (open_pure) fp = fopen_no_zlib(fname, "rw");
    else fp = fopen(fname, "rw");
    
    if (fp == NULL) {
        printf("Error: cannot open %s\n", fname);
        return 1;
    }
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    disp_height = TERMHEIGHT-1;
    disp_width = 2*8;
    unsigned long nlines = filesize/disp_width;
    if (nlines == 0) nlines = 1;
    
    unsigned char *screen_buff = (unsigned char*) malloc(sizeof(unsigned char) * disp_height * disp_width);
    unsigned char *screen_ring = screen_buff;
    
    long current_offset = 0;
    long previous_offset = 0;
    long current_line = 0;
    long previous_line = 5;
    
    bool edit_highdigit = true;
    bool force_redraw = true;
    
    cls();
    
    while (true) {
        // redraw
        long current_line = LINEAT(current_offset) - disp_height/2;
        if (nlines < disp_height) current_line = 0;
        else {
            if (current_line < 0) current_line = 0;
            if (current_line + disp_height > nlines) {
                current_line = nlines - disp_height + 1;
            }
        }
        
        fseek(fp, current_line*disp_width, SEEK_SET);
        fread(screen_ring, 1, disp_height*disp_width, fp);
        
        REDRAW(force_redraw);
        force_redraw = false;
            
        setcursor(0, disp_height+1);
        printf("cur: 0x%08X = %010d %02d%% ", current_offset, current_offset, (current_line*100) / nlines);
        
        int cmd = getkcode();
        
        previous_line = current_line;
        previous_offset = current_offset;
        
        switch (cmd) {
            case KEY_Up:
                if (current_offset-disp_width >= 0) {
                    current_offset -= disp_width;
                }
                edit_highdigit = true;
                break;
            case KEY_Down:
                if (current_offset+disp_width < filesize) {
                    current_offset += disp_width;
                }
                edit_highdigit = true;
                break;
            case KEY_Left:
                if (current_offset-1 >= 0) {
                    current_offset--;
                }
                edit_highdigit = true;
                break;
            case KEY_Right:
                if (current_offset+1 < filesize) {
                    current_offset++;
                }
                edit_highdigit = true;
                break;
            case KEY_G:
                printf("Jump to offset: ");
                getsn(subcommand, SUBCMDLEN);
                rstrip(subcommand);
                char *num_param = subcommand;
                if (memcmp(subcommand, "0x", 2) == 0) {
                    num_param = subcommand+2;
                    current_offset = atoi_hex(num_param);
                } else {
                    current_offset = atoi(num_param);
                }
                
                if (current_offset + (disp_height*disp_width) >= filesize) {
                    current_offset = filesize - (disp_height*disp_width);
                }
                if (current_offset < 0) current_offset = 0;
                edit_highdigit = true;
                force_redraw = true;
                break;
            case KEY_Escape:
                fclose(fp);
                printf("\n");
                exit(0);
            case KEY_A: OVERWRITE_DIGIT(0xA); break;
            case KEY_B: OVERWRITE_DIGIT(0xB); break;
            case KEY_C: OVERWRITE_DIGIT(0xC); break;
            case KEY_D: OVERWRITE_DIGIT(0xD); break;
            case KEY_E: OVERWRITE_DIGIT(0xE); break;
            case KEY_F: OVERWRITE_DIGIT(0xF); break;
            case KEY_Num0: OVERWRITE_DIGIT(0x0); break;
            case KEY_Num1: OVERWRITE_DIGIT(0x1); break;
            case KEY_Num2: OVERWRITE_DIGIT(0x2); break;
            case KEY_Num3: OVERWRITE_DIGIT(0x3); break;
            case KEY_Num4: OVERWRITE_DIGIT(0x4); break;
            case KEY_Num5: OVERWRITE_DIGIT(0x5); break;
            case KEY_Num6: OVERWRITE_DIGIT(0x6); break;
            case KEY_Num7: OVERWRITE_DIGIT(0x7); break;
            case KEY_Num8: OVERWRITE_DIGIT(0x8); break;
            case KEY_Num9: OVERWRITE_DIGIT(0x9); break;
        }
    }
    fclose(fp);
    printf("\n");
    return 0;
}
 
 
