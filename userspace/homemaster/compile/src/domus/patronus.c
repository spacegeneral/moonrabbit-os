#include <stdio.h>
#include <stdlib.h>
#include <sys/mq.h>
#include <sys/serv.h>
#include <sys/proc.h>

#include <libgui/libgui.h>

#include "../modular/modular.h"
#include "domus.h"


void patronus(void) {
    char topicname[4096];
    MQStatus poll_status;
    KeyEvent keycode;
    snprintf(topicname, 4096, DOOR_EVENT_KEYCODE_TOPIC_TEMPLATE, 0);
    
    if (config.show_welcome) {
        dialog_ok("Welcome to the Door Multiplexer System (DOMUS), graphical \
shell for the Moonrabbit Operating System. You can focus any \
open door by clicking the mouse. You can drag doors around \
holding the right mouse button. When the background door is \
focused, press F1 to spawn a soft terminal or Escape to \
return to the textual shell.", "Thanks", font);
    }
    
    if (file_exists(config.startup_script)) {
        setenv("quiet","1",1);
        setenv("script", config.startup_script, 1);
        epspawn_app(SHELL_APP);
        unsetenv("script");
        unsetenv("quiet");
    }
    
    subscribe(topicname);
    
    char mesg[2048];
    while (running) {
        poll_status = poll(topicname, &keycode);
        if (poll_status == MQ_SUCCESS) {
            if (focused_door->door_id == 0) {
                if (keycode.keycode == KEY_Escape && keycode.is_pressed) {
                    size_t numdoors = array_size(doors);
                    
                    if (numdoors <= 1) {
                        snprintf(mesg, 2048, "Do you really want to quit DOMUS?");  // min 51 char ? TODO fix it
                    } else {
                        snprintf(mesg, 2048, "Do you really want to quit DOMUS? %ld remaining door applications will be TERMINATED.", numdoors-1);
                    }
                    
                    bool answer = dialog_yesno(mesg, "yes", "no", font);
                    if (answer) {
                        running = false;
                    }
                }
            }
        }
        
        yield();
    }
    
    unsubscribe(topicname);
    
    exit(0);
}
