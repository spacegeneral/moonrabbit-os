#include <stdio.h>
#include <stdlib.h>
#include <sys/mq.h>
#include <sys/serv.h>
#include <sys/mouse.h>
#include <sys/proc.h>

#include <libgui/libgui.h>

#include "../modular/modular.h"
#include "domus.h"


Point prev_mouse_pos;
MouseEvent prev_mouse_event;
double mouse_speed_mult = 2.0;
clock_t last_leftbtn_down = 0;
clock_t last_click = 0;

Point move_pos_start;
Point move_origin_start;

Point resize_pos_start;
Point resize_origin_start;
bool resising_underway;


void handle_makedoor_commands() {
    LibguiMsgMakedoor request;
    unsigned int requestor_task_id;
    unsigned int request_id;
    ServStatus md_accept_status;
    do {
        md_accept_status = service_accept(MAKEDOOR_SERVICE, &request, &requestor_task_id, &request_id);
        if (md_accept_status == SERV_SUCCESS) {
            request.status = add_new_door(&request, &(request.door_id), requestor_task_id);
            restore_door_list_consistency();
            service_complete(MAKEDOOR_SERVICE, request_id);
            switch_cursor(&mouse_cursor_default);
        }
    } while (md_accept_status == SERV_SUCCESS);
}

void handle_killdoor_commands() {
    LibguiMsgKilldoor request;
    unsigned int requestor_task_id;
    unsigned int request_id;
    ServStatus kd_accept_status;
    do {
        kd_accept_status = service_accept(KILLDOOR_SERVICE, &request, &requestor_task_id, &request_id);
        if (kd_accept_status == SERV_SUCCESS) {
            request.status = remove_door(request.door_id);
            restore_door_list_consistency();
            cascade_redraw(get_door_by_id(0));
            service_complete(KILLDOOR_SERVICE, request_id);
        }
    } while (kd_accept_status == SERV_SUCCESS);
}



void handle_door_specific_commands(DoorEntry *door) {
    char topicname[4096];
    unsigned int requestor_task_id;
    unsigned int request_id;
    
    // update door
    snprintf(topicname, 4096, UPDATEDOOR_FACE_SERVICE_TEMPLATE, door->door_id);
    LibguiMsgUpdatedoorFace request_upd;
    ServStatus upd_accept_status;
    
    do {
        upd_accept_status = service_accept(topicname, &request_upd, &requestor_task_id, &request_id);
        if (upd_accept_status == SERV_SUCCESS) {
            // TODO handle mismatching task_id
            if (!rect_is_zero(rect_delta(door->face.geom, request_upd.face.geom))) {  // door was geometrically moved
                door->face = request_upd.face;
                DoorEntry *parent = get_door_by_id(door->parent_door_id);
                
                compute_global_cover_status();
                
                if (parent == focused_door) {
                    cascade_redraw(parent);
                } else {
                    cascade_redraw(get_common_ancestor(focused_door, parent));
                }
            }
            door->face = request_upd.face;
            if (request_upd.redraw) {
                if (door == focused_door) {
                    cascade_redraw(door);
                } else {
                    if (!door->covered) cascade_redraw(door);  //TODO compute visibility better
                }
            }
            request_upd.status = DOORS_OK;
            // TODO handle redraw parent for transparency
            
            service_complete(topicname, request_id);
        }
    } while (upd_accept_status == SERV_SUCCESS);
    
}


void handle_commands() {
    //if (!is_setup_complete()) return;
    handle_makedoor_commands();
    handle_killdoor_commands();
    ARRAY_FOREACH(current, doors, {
        handle_door_specific_commands((DoorEntry*) current);
    });
}


bool handle_input() {
    MouseEvent event;
    LibguiMsgMouseEvent mouse_msg;
    char key;
    KeyEvent keycode;
    char topicname[4096];
    bool door_transformed = false;
    bool must_redisplay = false;
    
    Rect resize_hotspot = focused_door->face.geom;
    resize_hotspot.x += resize_hotspot.w - RESIZE_HOTSPOT;
    resize_hotspot.y += resize_hotspot.h - RESIZE_HOTSPOT;
    
    MQStatus poll_status;
    snprintf(topicname, 4096, DOOR_EVENT_MOUSE_TOPIC_TEMPLATE, focused_door->door_id);
    do {
        poll_status = poll(DEFAULT_MOUSE_INPUT_TOPIC, &event);
        if (poll_status == MQ_SUCCESS) {
            do_redraw_cursor = true;
            must_redisplay = true;
            mouse_cursor.geom.x += (int) (mouse_speed_mult * ((double) event.delta_x));
            mouse_cursor.geom.y -= (int) (mouse_speed_mult * ((double) event.delta_y));
            if (mouse_cursor.geom.x < 0) mouse_cursor.geom.x = 0;
            if (mouse_cursor.geom.y < 0) mouse_cursor.geom.y = 0;
            if (mouse_cursor.geom.x >= screeninfo.screensize.w) mouse_cursor.geom.x = screeninfo.screensize.w-1;
            if (mouse_cursor.geom.y >= screeninfo.screensize.h) mouse_cursor.geom.y = screeninfo.screensize.h-1;
            Point curr_mouse_pos = {.x = mouse_cursor.geom.x, .y = mouse_cursor.geom.y};
            
            if ((event.left_btn_down && !prev_mouse_event.left_btn_down) || \
                (event.right_btn_down && !prev_mouse_event.right_btn_down) || \
                (event.middle_btn_down && !prev_mouse_event.middle_btn_down)) {  // transition to any button pressed
                
                DoorEntry *selected = get_topmost_at(curr_mouse_pos);
                if (selected != focused_door) {
                    shift_focus(focused_door, selected);
                    snprintf(topicname, 4096, DOOR_EVENT_MOUSE_TOPIC_TEMPLATE, focused_door->door_id);
                }
            }
            
            mouse_msg.x = mouse_cursor.geom.x - focused_door->face.geom.x;
            mouse_msg.y = mouse_cursor.geom.y - focused_door->face.geom.y;
            mouse_msg.btn_left = event.left_btn_down;
            mouse_msg.btn_mid = event.middle_btn_down;
            mouse_msg.btn_right = event.right_btn_down;
            mouse_msg.is_enter = false;
            mouse_msg.is_leave = false;
            mouse_msg.is_click = false;
            mouse_msg.is_dbclick = false;
            mouse_msg.timestamp = event.timestamp;
            
            if (rect_contains(focused_door->face.geom, curr_mouse_pos) && !rect_contains(focused_door->face.geom, prev_mouse_pos)) {
                mouse_msg.is_enter = true;
            } else if (!rect_contains(focused_door->face.geom, curr_mouse_pos) && rect_contains(focused_door->face.geom, prev_mouse_pos)) {
                mouse_msg.is_leave = true;
            }
            
            if (!event.left_btn_down && prev_mouse_event.left_btn_down) {  // transition to left button released
                if (event.timestamp - last_leftbtn_down < CLICK_DELTA_T) {
                    mouse_msg.is_click = true;
                }
                resising_underway = false;
            } else if (event.left_btn_down && !prev_mouse_event.left_btn_down) {  // transition to left button pressed
                last_leftbtn_down = event.timestamp;
                
                if (rect_contains(resize_hotspot, curr_mouse_pos)) {  // start resize
                    resize_pos_start = curr_mouse_pos;
                    resize_origin_start.x = focused_door->face.geom.w;
                    resize_origin_start.y = focused_door->face.geom.h;
                    resising_underway = true;
                }
            } else if (event.left_btn_down && prev_mouse_event.left_btn_down) {  // keep holding left button
                if (resising_underway && (curr_mouse_pos.x != prev_mouse_pos.x) && (curr_mouse_pos.y != prev_mouse_pos.y)) {  // keep resizing
                    Point new_size = {
                        .x = resize_origin_start.x + (curr_mouse_pos.x - resize_pos_start.x),
                        .y = resize_origin_start.y + (curr_mouse_pos.y - resize_pos_start.y),
                    };
                    resize_door(focused_door, new_size);
                    door_transformed = true;
                }
            }
            
            if (mouse_msg.is_click) {
                if (event.timestamp - last_click < DBCLICK_DELTA_T) {
                    mouse_msg.is_dbclick = true;
                } else {
                    last_click = event.timestamp;
                }
            }
            
            if (!focused_door->glued) {
                if (event.BTN_MOVEWIN && !prev_mouse_event.BTN_MOVEWIN) {  // transition to middle button pressed
                    move_pos_start = curr_mouse_pos;
                    move_origin_start.x = focused_door->face.geom.x;
                    move_origin_start.y = focused_door->face.geom.y;
                } else if (event.BTN_MOVEWIN && prev_mouse_event.BTN_MOVEWIN) {  // keep holding middle
                    Point new_origin = {
                        .x = move_origin_start.x + (curr_mouse_pos.x - move_pos_start.x),
                        .y = move_origin_start.y + (curr_mouse_pos.y - move_pos_start.y),
                    };
                    move_door(focused_door, new_origin);
                    door_transformed = true;
                } else if (!event.BTN_MOVEWIN && prev_mouse_event.BTN_MOVEWIN) {  // released middle
                    if (focused_door->door_id != 0) {
                        DoorEntry *parent = get_door_by_id(focused_door->parent_door_id);
                        
                        if (parent == focused_door) {
                            cascade_redraw(parent);
                        } else {
                            cascade_redraw(get_common_ancestor(focused_door, parent));
                        }
                    }
                }
            }
            
            if ((rect_contains(focused_door->face.geom, curr_mouse_pos) || mouse_msg.is_leave) && !door_transformed) {
                publish(topicname, (void*)&mouse_msg);
            }
            
            prev_mouse_event = event;
            prev_mouse_pos = curr_mouse_pos;
        }
    } while (poll_status == MQ_SUCCESS);
    
    // stdin
    snprintf(topicname, 4096, DOOR_EVENT_STDIN_TOPIC_TEMPLATE, focused_door->door_id);
    do {
        poll_status = poll(stdin_topic, &key);
        if (poll_status == MQ_SUCCESS) {
            publish(topicname, (void*)&key);
            /*if (key == 'a') {
                epspork("f,def,init,app,hello_gui.elf", &debug_only);
            }*/
        }
    } while (poll_status == MQ_SUCCESS);
    
    // keycode
    snprintf(topicname, 4096, DOOR_EVENT_KEYCODE_TOPIC_TEMPLATE, focused_door->door_id);
    do {
        poll_status = poll(keycode_topic, &keycode);
        if (poll_status == MQ_SUCCESS) {
            publish(topicname, (void*)&keycode);
            if (focused_door->door_id == 0 && keycode.keycode == KEY_F12 && keycode.is_pressed) {
                switch_cursor(&mouse_cursor_wait);
                setenv("quiet","1",1);
                epspawn_app(TERMAPP);
                unsetenv("quiet");
            }
            if (focused_door->door_id != 0 && keycode.keycode == KEY_F4 && (keycode.mod_flags & LALT_DOWN_FLAG) && keycode.is_pressed) {
                kill(focused_door->owner_task);
                remove_door(focused_door->door_id);
                return must_redisplay;
            }
            if (focused_door->door_id != 0 && keycode.keycode == KEY_F3 && (keycode.mod_flags & LALT_DOWN_FLAG) && keycode.is_pressed) {
                toggle_maximize_door(focused_door);
                return must_redisplay;
            }
        }
    } while (poll_status == MQ_SUCCESS);
    
    return must_redisplay;
}
