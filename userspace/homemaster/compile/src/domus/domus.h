#ifndef _DOMUS_H
#define _DOMUS_H

#include <sys/term.h>
#include <sys/mouse.h>
#include <collections/array.h>
#include <libgui/libgui.h>



#define DIRECT_FB_DRAW


#define DOMUS_RACE Dwarf
#define DOMUS_CLASS Maid

#define TERMAPP "mofuterm"
#define CONFIG_FILE_PATH "f,def,home,appdata,domus,config.ini"

#define SHELL_APP "kosh"


#define CLICK_DELTA_T 200
#define DBCLICK_DELTA_T 400

#define MINIMUM_DOOR_SIZE 4
#define RESIZE_HOTSPOT 20

#define BTN_MOVEWIN middle_btn_down
//#define BTN_MOVEWIN right_btn_down


typedef struct DOMUSConfig {
    char* wallpaper_path;
    char* cursor_normal_path;
    char* cursor_busy_path;
    char* bitmap_font_path;
    bool show_welcome;
    char* startup_script;
} DOMUSConfig;

extern DOMUSConfig config;

extern bool running;

extern TermInfo screeninfo;
extern Array* doors;

extern Bitmap mouse_cursor;
extern Bitmap mouse_cursor_default;
extern Bitmap mouse_cursor_wait;
extern MouseEvent prev_mouse_event;
extern Point prev_mouse_pos;
extern double mouse_speed_mult;

extern BitmapFont font;
extern bool do_redraw_cursor;


typedef struct DoorEntry {
    Bitmap face;
    Rect remember_size;
    char name[MAX_DOORNAME];
    int border;
    Color24 border_color;
    Color24* transparency_colorkey;
    unsigned int parent_door_id;
    unsigned int door_id;
    int owner_task;
    int zindex;
    bool redraw;
    bool glued;
    bool covered;
    Array* child_doors;
} DoorEntry;

extern DoorEntry* focused_door;



void cascade_redraw(DoorEntry* place);
void patronus(void);
void handle_commands();
bool handle_input();
DoorSystemStatus add_new_door(LibguiMsgMakedoor* request, unsigned int* new_door_id, int owner_task);
DoorSystemStatus remove_door(unsigned int door_id);
DoorEntry* get_door_by_id(unsigned int door_id);
DoorEntry* get_common_ancestor(DoorEntry* a, DoorEntry* b);
DoorEntry* get_topmost_at(Point position);
void move_door(DoorEntry* door, Point new_origin);
void resize_door(DoorEntry *door, Point new_size);
void toggle_maximize_door(DoorEntry *door);
void shift_focus(DoorEntry* from, DoorEntry* to);
void restore_door_list_consistency();
void switch_cursor(Bitmap* new_cursor);
void compute_global_cover_status();


#endif
