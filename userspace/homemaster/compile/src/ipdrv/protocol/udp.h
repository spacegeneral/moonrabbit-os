#ifndef _IPDRV_PROTO_UDP_H
#define _IPDRV_PROTO_UDP_H


#include <stdint.h>
#include "../shipyard.h"

typedef struct __attribute__((packed)) UDPHeader {
    uint16_t src_port;
    uint16_t dest_port;
    uint16_t length;
    uint16_t checksum;
} UDPHeader;


typedef struct __attribute__((packed)) UDPPseudoHeaderV4 {
    uint8_t src_ip[IPv4_ADDRESS_LENGTH];
    uint8_t dest_ip[IPv4_ADDRESS_LENGTH];
    uint8_t zeros;
    uint8_t protocol;
    uint16_t udp_length;
    UDPHeader udp_header;
} UDPPseudoHeaderV4;


typedef struct __attribute__((packed)) UDPPseudoHeaderV6 {
    uint8_t src_ip[IPv6_ADDRESS_LENGTH];
    uint8_t dest_ip[IPv6_ADDRESS_LENGTH];
    uint32_t udp_length;
    uint8_t zeros[3];
    uint8_t protocol;
    UDPHeader udp_header;
} UDPPseudoHeaderV6;


void update_udp_checksum(ShipyardBuffer* shipyard);


#endif
