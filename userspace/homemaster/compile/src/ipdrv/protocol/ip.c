#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <yakumo.h>
#include <sys/network.h>

#include "../shipyard.h"
#include "../endianness.h"
#include "ip.h"
#include "tcp.h"
#include "udp.h"
#include "ethernet.h"


uint16_t ip_checksum(void* vdata, size_t length) {
    // Cast the data pointer to one that can be indexed.
    char* data=(char*)vdata;

    // Initialise the accumulator.
    uint64_t acc=0xffff;

    // Handle any partial block at the start of the data.
    unsigned int offset=((uintptr_t)data)&3;
    if (offset) {
        size_t count=4-offset;
        if (count>length) count=length;
        uint32_t word=0;
        memcpy(offset+(char*)&word,data,count);
        acc+=get_bigendian_dword(word);
        data+=count;
        length-=count;
    }

    // Handle any complete 32-bit blocks.
    char* data_end=data+(length&~3);
    while (data!=data_end) {
        uint32_t word;
        memcpy(&word,data,4);
        acc+=get_bigendian_dword(word);
        data+=4;
    }
    length&=3;

    // Handle any partial block at the end of the data.
    if (length) {
        uint32_t word=0;
        memcpy(&word,data,length);
        acc+=get_bigendian_dword(word);
    }

    // Handle deferred carries.
    acc=(acc&0xffffffff)+(acc>>32);
    while (acc>>16) {
        acc=(acc&0xffff)+(acc>>16);
    }

    // If the data began at an odd byte address
    // then reverse the byte order to compensate.
    if (offset&1) {
        acc=((acc&0xff00)>>8)|((acc&0x00ff)<<8);
    }

    // Return the checksum in network byte order.
    uint16_t ret;
    put_bigendian_word(~acc, (uint8_t*)&ret);
    return ret;
}


void assemble_ipv4_armor(ShipyardBuffer* shipyard, IpAddress *src_ip, IpAddress *dest_ip) {
    assert(shipyard->transport_header != NULL);
    assert(src_ip->version == 4);
    assert(dest_ip->version == 4);
    
    size_t header_length = MIN_IPv4_HEADER_LENGTH;
    
    shipyard->network_header = shipyard->transport_header - header_length;
    shipyard->network_data = shipyard->transport_header;
    shipyard->network_total_length = shipyard->transport_total_length + header_length;
    shipyard->network_ethertype = EtherType_ipv4;
    
    IPV4Header *header = (IPV4Header*) shipyard->network_header;
    header->version_ihl = (4 << 4) | (((uint8_t)(header_length/4)) & 0xF);
    header->dscp_ecn = 0;
    put_bigendian_word((uint16_t)shipyard->network_total_length, (uint8_t*)&(header->total_length));
    put_bigendian_word((uint16_t)0xabcd, (uint8_t*)&(header->identification));  //TODO randomize
    header->flags_fragment_offset = 0;
    header->ttl = DEFAULT_IP_TTL;
    header->protocol = (uint8_t) shipyard->transport_protocol;
    header->header_checksum = 0;  // for now
    
    memcpy(header->dest_ip, dest_ip->address, IPv4_ADDRESS_LENGTH);
    memcpy(header->src_ip, src_ip->address, IPv4_ADDRESS_LENGTH);
    shipyard->dest_ip = *dest_ip;
    shipyard->src_ip = *src_ip;
    
    header->header_checksum = ip_checksum((void*)header, header_length);
    
    if (shipyard->transport_protocol == PROTO_TCP) {
        update_tcp_checksum(shipyard);
    } else if (shipyard->transport_protocol == PROTO_UDP) {
        update_udp_checksum(shipyard);
    }
}


bool try_disassemble_ipv4_armor(ShipyardBuffer* shipyard) {
    assert(shipyard->network_header != NULL);
    IPV4Header *header = (IPV4Header*) shipyard->network_header;
    
    uint8_t version = header->version_ihl >> 4;
    if (version != 4) return false;  // not ipv4
    size_t header_length = (header->version_ihl & 0xF) * 4;
    
    shipyard->network_data = shipyard->network_header + header_length;
    shipyard->network_total_length = get_bigendian_word(header->total_length);
    shipyard->transport_protocol = header->protocol;
    shipyard->transport_header = shipyard->network_data;
    
    memcpy(shipyard->dest_ip.address, header->dest_ip, IP_ADDRESS_LENGTH);
    memcpy(shipyard->src_ip.address, header->src_ip, IP_ADDRESS_LENGTH);
    shipyard->dest_ip.version = 4;
    shipyard->src_ip.version = 4;
    
    return true;
}


void assemble_ipv6_armor(ShipyardBuffer* shipyard, IpAddress *src_ip, IpAddress *dest_ip) {
    assert(shipyard->transport_header != NULL);
    assert(src_ip->version == 6);
    assert(dest_ip->version == 6);
    
    size_t header_length = IPv6_HEADER_LENGTH;
    
    shipyard->network_header = shipyard->transport_header - header_length;
    shipyard->network_data = shipyard->transport_header;
    shipyard->network_total_length = shipyard->transport_total_length + header_length;
    shipyard->network_ethertype = EtherType_ipv6;
    
    IPV6Header *header = (IPV6Header*) shipyard->network_header;
    header->version_tclass_flowlabel = (6 << 28);
    
    put_bigendian_word((uint16_t)shipyard->network_total_length, (uint8_t*)&(header->total_length));
    header->ttl = DEFAULT_IP_TTL;
    header->protocol = (uint8_t) shipyard->transport_protocol;
    
    memcpy(header->dest_ip, dest_ip->address, IPv6_ADDRESS_LENGTH);
    memcpy(header->src_ip, src_ip->address, IPv6_ADDRESS_LENGTH);
    shipyard->dest_ip = *dest_ip;
    shipyard->src_ip = *src_ip;
    
    if (shipyard->transport_protocol == PROTO_TCP) {
        update_tcp_checksum(shipyard);
    } else if (shipyard->transport_protocol == PROTO_UDP) {
        update_udp_checksum(shipyard);
    }
}


bool try_disassemble_ipv6_armor(ShipyardBuffer* shipyard) {
    assert(shipyard->network_header != NULL);
    IPV6Header *header = (IPV6Header*) shipyard->network_header;
    
    unsigned version = header->version_tclass_flowlabel >> 28;
    if (version != 6) return false;  // not ipv6
    size_t header_length = IPv6_HEADER_LENGTH;
    
    shipyard->network_data = shipyard->network_header + header_length;
    shipyard->network_total_length = get_bigendian_dword(header->total_length);
    shipyard->transport_protocol = header->protocol;
    shipyard->transport_header = shipyard->network_data;
    
    memcpy(shipyard->dest_ip.address, header->dest_ip, IP_ADDRESS_LENGTH);
    memcpy(shipyard->src_ip.address, header->src_ip, IP_ADDRESS_LENGTH);
    shipyard->dest_ip.version = 6;
    shipyard->src_ip.version = 6;
    
    return true;
}


void assemble_ip_armor(ShipyardBuffer* shipyard, IpAddress *src_ip, IpAddress *dest_ip) {
    assert(src_ip->version == dest_ip->version);
    if (src_ip->version == 6) {
        assemble_ipv6_armor(shipyard, src_ip, dest_ip);
    } else {
        assemble_ipv4_armor(shipyard, src_ip, dest_ip);
    }
}

size_t get_ip_header_length(ShipyardBuffer* shipyard) {
    if (shipyard->network_ethertype == EtherType_ipv6) return IPv6_HEADER_LENGTH;
    else {
        IPV4Header *header = (IPV4Header*) shipyard->network_header;
        size_t header_length = (header->version_ihl & 0xF) * 4;
        return header_length;
    }
    return 0;
}
