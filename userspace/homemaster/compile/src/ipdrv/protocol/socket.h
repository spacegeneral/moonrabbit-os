#ifndef _IPDRV_PROTO_SOCKET_H
#define _IPDRV_PROTO_SOCKET_H


#include <stdint.h>
#include <stdbool.h>
#include <sys/socket.h>

#include "tcp.h"


typedef struct SocketDescriptor {
    char rx_topic[4096];
    char tx_topic[4096];
    SocketType type;
    SocketProtocol protocol;
    void *connection_params;
    int sock_id;
    int endpoint_task_id;
} SocketDescriptor;


#define INVALID_SOCKED_ID (-1)


// for when we don't know which port to use
#define DEFAULT_SOCK_PORT 17

#define EPHEMERAL_PORT_RANGE_START 49152
#define EPHEMERAL_PORT_RANGE_END   65535


typedef struct UDPSocketConnectionParams {
    sockaddr source;
    sockaddr destination;
    sockaddr filter;
    sockaddr last_received_from;
} UDPSocketConnectionParams;


typedef struct TCPSocketConnectionParams {
    TCPConnectionStatus conn_status;
    sockaddr destination;
    sockaddr source;
    bool destination_specified;
    
    struct {  // send
        unsigned una;  // send unacknowledged
        unsigned nxt;  // send next
        unsigned wnd;  // send window
        unsigned iss;  // send initial sequence number
    } snd;
    
    struct {  // receive
        unsigned nxt;  // receive next
        unsigned wnd;  // receive window
        unsigned irs;  // receive initial sequence number
    } rcv;
    
} TCPSocketConnectionParams;



size_t get_socket_mtu(SocketDescriptor *sock);



#endif
