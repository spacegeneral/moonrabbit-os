#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <yakumo.h>
#include <sys/network.h>

#include "../shipyard.h"
#include "../endianness.h"
#include "icmp.h"
#include "ip.h"


static char default_payload[] = "nya";



void update_icmp_checksum(ICMPHeader *header, size_t data_length) {
    header->checksum = 0;
    header->checksum = ip_checksum((void*) header, sizeof(ICMPHeader) + data_length);
}



ICMPPacket *make_icmp_echo_request(int identifier, int sequence_number, int version, size_t payload_size) {
    ICMPPacket *packet = malloc(sizeof(ICMPHeader) + payload_size);
    
    if (version == 6) packet->header.type = ICMPv6_ECHO_REQUEST;
    else packet->header.type = ICMP_ECHO_REQUEST;
    packet->header.code = 0;
    
    put_bigendian_word((uint16_t)identifier, (uint8_t*)&(packet->header.rest.echo.identifier));
    put_bigendian_word((uint16_t)sequence_number, (uint8_t*)&(packet->header.rest.echo.sequence_number));
    
    size_t repeat = strlen(default_payload);
    for (size_t p=0; p<payload_size; p++) {
        packet->payload[p] = default_payload[p % repeat];
    }
    
    update_icmp_checksum((ICMPHeader*) packet, payload_size);
    //TODO checksum v6
    
    return packet;
}

ICMPPacket *make_icmp_echo_reply(int identifier, int sequence_number, int version, void* payload, size_t payload_size) {
    ICMPPacket *packet = malloc(sizeof(ICMPHeader) + payload_size);
    
    if (version == 6) packet->header.type = ICMPv6_ECHO_REPLY;
    else packet->header.type = ICMP_ECHO_REPLY;
    packet->header.code = 0;
    
    put_bigendian_word((uint16_t)identifier, (uint8_t*)&(packet->header.rest.echo.identifier));
    put_bigendian_word((uint16_t)sequence_number, (uint8_t*)&(packet->header.rest.echo.sequence_number));
    
    memcpy(packet->payload, payload, payload_size);
    
    update_icmp_checksum((ICMPHeader*) packet, payload_size);
    //TODO checksum v6
    
    return packet;
}


ICMPPacket *make_icmp_router_solicitation(int identifier, int sequence_number, int version) {
    ICMPPacket *packet = malloc(sizeof(ICMPHeader));
    
    if (version == 6) packet->header.type = ICMPv6_ROUTER_SOLICITATION;
    else packet->header.type = ICMP_ROUTER_SOLICITATION;
    packet->header.code = 0;
    
    packet->header.rest.router_solicitation_all_versions.reserved = 0;
    
    update_icmp_checksum((ICMPHeader*) packet, 0);
    //TODO checksum v6
    
    return packet;
}
