#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <yakumo.h>
#include <sys/network.h>

#include "../shipyard.h"
#include "../endianness.h"
#include "ethernet.h"


const uint32_t ethernet_polynomial_le = 0xedb88320U;

uint32_t ethernet_crc_le(uint8_t *data, size_t length, int foxes) {
    uint32_t crc = (foxes) ? 0xffffffff: 0;
    for (size_t c=0; c<length; c++) {
        unsigned char current_octet = *data++;
        int bit;
        for (bit = 8; --bit >= 0; current_octet >>= 1) {
            if ((crc ^ current_octet) & 1) {
                crc >>= 1;
                crc ^= ethernet_polynomial_le;
            } else {
                crc >>= 1;
            }
        }
    }
    return crc;
}



void assemble_ethernet_armor(ShipyardBuffer* shipyard, const uint8_t *src_mac, const uint8_t *dest_mac) {
    assert(shipyard->network_header != NULL);
    
    shipyard->datalink_header = shipyard->network_header - sizeof(EthernetHeader);
    shipyard->datalink_data = shipyard->network_header;
    size_t payload_length = max(MIN_ETHERNET_MTU, shipyard->network_total_length);
    shipyard->datalink_total_length = payload_length + sizeof(EthernetHeader) + FCS_LENGTH;
    uint32_t *crc_tail = (uint32_t*)(shipyard->datalink_header + shipyard->datalink_total_length - FCS_LENGTH);
    *crc_tail = 0;
    
    EthernetHeader *header = (EthernetHeader*) shipyard->datalink_header;
    memcpy(header->dest_mac, dest_mac, MAC_ADDRESS_LENGTH);
    memcpy(header->src_mac, src_mac, MAC_ADDRESS_LENGTH);
    memcpy(shipyard->src_mac, src_mac, MAC_ADDRESS_LENGTH);
    memcpy(shipyard->dest_mac, dest_mac, MAC_ADDRESS_LENGTH);
    put_bigendian_word((uint16_t)shipyard->network_ethertype, (uint8_t*)&(header->lt.ether_type));
    
    uint32_t crc32 = ethernet_crc_le(shipyard->datalink_header, shipyard->datalink_total_length-4, 1);
    *crc_tail = ~crc32;
}


bool try_disassemble_ethernet_armor(ShipyardBuffer* shipyard, size_t link_length) {
    assert(shipyard->datalink_header != NULL);
    
    shipyard->datalink_data = shipyard->datalink_header + sizeof(EthernetHeader);
    shipyard->datalink_total_length = link_length;
    
    EthernetHeader *header = (EthernetHeader*) shipyard->datalink_header;
    shipyard->network_ethertype = get_bigendian_word(header->lt.ether_type);
    shipyard->network_header = shipyard->datalink_data;
    
    memcpy(shipyard->src_mac, header->src_mac, MAC_ADDRESS_LENGTH);
    memcpy(shipyard->dest_mac, header->dest_mac, MAC_ADDRESS_LENGTH);
    
    uint32_t *crc_tail = (uint32_t*)(shipyard->datalink_header + shipyard->datalink_total_length - FCS_LENGTH);
    
    uint32_t local_crc32 = ethernet_crc_le(shipyard->datalink_header, shipyard->datalink_total_length-4, 1);
    
    // the crc32 will tell us if this is really an ethernet frame
    /*if ((~local_crc32) == (*crc_tail)) return true;
    return false;*/
    return true;
}
