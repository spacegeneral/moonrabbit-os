#ifndef _IPDRV_PROTO_ETHERNET_H
#define _IPDRV_PROTO_ETHERNET_H


#include <stdint.h>
#include <sys/network.h>


#define FCS_LENGTH 4


typedef struct __attribute__((packed)) EthernetHeader {
    uint8_t dest_mac[MAC_ADDRESS_LENGTH];
    uint8_t src_mac[MAC_ADDRESS_LENGTH];
    union {
        uint16_t data_length;  // <= 1500 (ethernet 802.3)
        uint16_t ether_type;   // > 1500  (ethernet II)  <-- use this one
    } lt;
} EthernetHeader;


uint32_t ethernet_crc_le(uint8_t *data, size_t length, int foxes);


#define EtherType_ipv4          0x0800
#define EtherType_ipv6          0x86DD
#define EtherType_arp           0x0806
#define EtherType_wake_on_lan   0x0842


#endif
