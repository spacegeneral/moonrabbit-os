#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <yakumo.h>
#include <sys/network.h>

#include "../shipyard.h"
#include "../endianness.h"
#include "udp.h"
#include "ip.h"



void assemble_udp_armor(ShipyardBuffer* shipyard, uint16_t src_port, uint16_t dest_port) {
    assert(shipyard->application_data != NULL);
    
    size_t header_length = sizeof(UDPHeader);
    
    shipyard->transport_header = shipyard->application_data - header_length;
    shipyard->transport_data = shipyard->application_data;
    shipyard->transport_total_length = shipyard->application_length + header_length;
    shipyard->transport_protocol = PROTO_UDP;
    
    UDPHeader *header = (UDPHeader*) shipyard->transport_header;
    
    put_bigendian_word(src_port, (uint8_t*)&(header->src_port));
    put_bigendian_word(dest_port, (uint8_t*)&(header->dest_port));
    shipyard->dest_port = dest_port;
    shipyard->src_port = src_port;
    
    put_bigendian_word(shipyard->transport_total_length, (uint8_t*)&(header->length));
    header->checksum = 0;
}

    
bool try_disassemble_udp_armor(ShipyardBuffer* shipyard) {
    assert(shipyard->transport_header != NULL);
    UDPHeader* header = (UDPHeader*) shipyard->transport_header;
    
    size_t header_length = sizeof(UDPHeader);
    
    shipyard->transport_data = shipyard->transport_header + header_length;
    shipyard->transport_total_length = get_bigendian_word(header->length);
    shipyard->application_data = shipyard->transport_data;
    shipyard->application_length = shipyard->transport_total_length - header_length;
    
    shipyard->dest_port = get_bigendian_word(header->dest_port);
    shipyard->src_port = get_bigendian_word(header->src_port);
    
    return true;
}


void update_udp_checksum(ShipyardBuffer* shipyard) {
    UDPHeader* header = (UDPHeader*) shipyard->transport_header;
    header->checksum = 0;
}
