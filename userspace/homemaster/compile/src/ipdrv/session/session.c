#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "session.h"
#include "../self_description.h"
#include "../tables.h"


#define ONEDAY (60*60*24)


bool save_session(const char *fname, NetSession *session) {
    FILE *fp = fopen(fname, "w");
    if (fp == NULL) return false;
    
    bool success = (fwrite(session, sizeof(NetSession), 1, fp) == 1);
    fclose(fp);
    
    return success;
}

bool load_session(const char *fname, NetSession *session) {
    FILE *fp = fopen(fname, "r");
    if (fp == NULL) return false;
    
    bool success = (fread(session, sizeof(NetSession), 1, fp) == 1);
    fclose(fp);
    
    return success;
}

bool restore_session(const char *fname) {
    NetSession session;
    if (!load_session(fname, &session)) return false;
    
    time_t now = time(NULL);
    if (now - session.created >= ONEDAY) return false;
    
    self_description.preferred_ip_version = session.lan_ip.version;
    self_description.lan_ip = session.lan_ip;
    self_description.gateway_ip = session.gateway_ip;
    arp_table_insert(session.gateway_mac, &(session.gateway_ip));
    
    return true;
}

bool remember_session(const char *fname) {
    NetSession session;
    
    session.lan_ip = self_description.lan_ip;
    session.gateway_ip = self_description.gateway_ip;
    memcpy(session.gateway_mac, arp_table_match_ip(&(self_description.gateway_ip)), MAC_ADDRESS_LENGTH);
    session.created = time(NULL);
    
    return save_session(fname, &session);
}
