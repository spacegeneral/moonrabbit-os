#ifndef _IPDRV_IPDRV_H
#define _IPDRV_IPDRV_H


#include <sys/socket.h>
#include "shipyard.h"
#include "protocol/socket.h"


//#define IPDRV_ADVANCED_DEBUG

#define DEFAULT_TIMEOUT 5000


typedef struct IPDRVConfig {
    char *gateway_address;
    char *gateway_mac;
    char *lan_ip;
    char *session_file;
    int nic_id;
    int preferred_ipv;
} IPDRVConfig;




void handle_sock_control_request(SocketControlServiceMessage *response, unsigned int requestor_task_id);
void transmit_from_sockets();
void flush_socket(SocketDescriptor *sock);
void terminate_sockets_by_taskid(int task_id);
void receive_into_socket(SocketDescriptor *sock, sockaddr *from, void *data_start, size_t data_length);

#endif
 
