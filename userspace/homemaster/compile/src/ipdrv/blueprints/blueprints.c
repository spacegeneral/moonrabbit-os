#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/socket.h>


#include "../self_description.h"
#include "../shipyard.h"
#include "../armor.h"
#include "../pilot.h"
#include "../tables.h"
#include "../protocol/arp.h"
#include "../protocol/icmp.h"
#include "../protocol/udp.h"
#include "../protocol/tcp.h"
#include "../protocol/dhcp.h"
#include "blueprints.h"



bool guard_unknown_mac(IpAddress *ip, long long int timeout) {
    char *gateway_mac;
    clock_t started = clock();
    while ((gateway_mac = arp_table_match_ip(ip)) == NULL) {
        if (clock() - started > timeout) {
            return false;
        }
        yield();
    }
    return true;
}


bool guard_gateway_mac(long long int timeout) {
    return guard_unknown_mac(&(self_description.gateway_ip), timeout);
}


void arp_as_needed(IpAddress *ip) {
    unsigned char *mac = arp_table_match_ip(ip);
    if (mac == NULL) issue_arp_probe(ip);
}


bool guard_dhcp_client_state(int state, long long int timeout) {
    char *gateway_mac;
    clock_t started = clock();
    
    while (self_description.dhcp_client_state.status != state) {
        if (clock() - started > timeout) {
            return false;
        }
        yield();
    }
    return true;
}


bool arp_claim_address(IpAddress *ip) {
    int attempts = 3;
    int timeout = 1000;
    for (int a=0; a<attempts; a++) {
        issue_arp_probe(ip);
        if (guard_unknown_mac(ip, timeout)) return false;
    }
    issue_arp_announcement(ip);
    return true;
}


void issue_arp_probe(IpAddress *ip) {
    if (ip->version != 4) return;
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    
    NetPacket *outbound;
    
    ARPFrame arp_probe = make_arp_probe(self_description.nic_manifest.mac, ip->address);
    insert_arp_pilot(&shipyard_request, &arp_probe);
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, bcast_mac);
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
    ShipyardBuffer_destroy(&shipyard_request);
}

void issue_arp_announcement(IpAddress *my_ip) {
    if (my_ip->version != 4) return;
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    
    NetPacket *outbound;
    
    ARPFrame arp_probe = make_arp_announcement(self_description.nic_manifest.mac, my_ip->address);
    insert_arp_pilot(&shipyard_request, &arp_probe);
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, bcast_mac);
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
    ShipyardBuffer_destroy(&shipyard_request);
}

void issue_arp_response(IpAddress *my_ip, ARPFrame *request) {
    if (my_ip->version != 4) return;
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    
    NetPacket *outbound;
    ARPFrame arp_response = make_arp_response(self_description.nic_manifest.mac, my_ip->address, request->sender_hw_address, request->sender_protocol_address);
    insert_arp_pilot(&shipyard_request, &arp_response);
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, request->sender_hw_address);
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
    ShipyardBuffer_destroy(&shipyard_request);
}


void issue_icmp_ping(IpAddress *ip, int id_num, int seq_num) {
    assert(ip->version == self_description.lan_ip.version);
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    NetPacket *outbound;
    
    ICMPPacket *ping = make_icmp_echo_request(id_num, seq_num, ip->version, DEFAULT_ICMP_ECHO_PAYLOAD_LENGTH);
    insert_icmp_pilot(&shipyard_request, ping, sizeof(ICMPHeader) + DEFAULT_ICMP_ECHO_PAYLOAD_LENGTH);
    
    assemble_ip_armor(&shipyard_request, &(self_description.lan_ip), ip);
    
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, link_level_routing(ip));
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
    free(ping);
    ShipyardBuffer_destroy(&shipyard_request);
}

void issue_icmp_pong(IpAddress *ip, int id_num, int seq_num, void *payload, size_t payload_size) {
    assert(ip->version == self_description.lan_ip.version);
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    NetPacket *outbound;
    
    ICMPPacket *pong = make_icmp_echo_reply(id_num, seq_num, ip->version, payload, payload_size);
    insert_icmp_pilot(&shipyard_request, pong, sizeof(ICMPHeader) + payload_size);
    
    assemble_ip_armor(&shipyard_request, &(self_description.lan_ip), ip);
    
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, link_level_routing(ip));
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
    free(pong);
    ShipyardBuffer_destroy(&shipyard_request);
}

void issue_udp_packet(sockaddr *from, sockaddr *to, void *data, size_t data_length) {
    assert(from->addr.version == to->addr.version);
    
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    NetPacket *outbound;
    
    insert_application_level_pilot(&shipyard_request, data, data_length);
    assemble_udp_armor(&shipyard_request, (uint16_t)from->port, (uint16_t)to->port);
    assemble_ip_armor(&shipyard_request, &(from->addr), &(to->addr));
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, link_level_routing(&(to->addr)));
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
#ifdef IPDRV_ADVANCED_DEBUG
    ShipyardBuffer_debug(&shipyard_request);
#endif
    
    ShipyardBuffer_destroy(&shipyard_request);
}

void issue_dhcp_discover(uint32_t transaction_id) {
    DHCPHeader *data;
    size_t data_length;
    build_dhcp_discovery(&data, &data_length, transaction_id);
    
    sockaddr from = {
        .port = 68,
        .addr = {
            .version = 4,
            .address = {0,0,0,0},
        },
    };
    sockaddr to = {
        .port = 67,
        .addr = {
            .version = 4,
            .address = {255,255,255,255},
        },
    };
    
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    NetPacket *outbound;
    
    insert_application_level_pilot(&shipyard_request, (uint8_t*)data, data_length);
    assemble_udp_armor(&shipyard_request, (uint16_t)from.port, (uint16_t)to.port);
    assemble_ip_armor(&shipyard_request, &(from.addr), &(to.addr));
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, bcast_mac);
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
    free(data);
    ShipyardBuffer_destroy(&shipyard_request);
}

void issue_dhcp_request(uint32_t transaction_id, IpAddress *dhcp_server_ip, IpAddress *offered) {
    DHCPHeader *data;
    size_t data_length;
    build_dhcp_request(&data, &data_length, transaction_id, dhcp_server_ip, offered);
    
    sockaddr from = {
        .port = 68,
        .addr = {
            .version = 4,
            .address = {0,0,0,0},
        },
    };
    sockaddr to = {
        .port = 67,
        .addr = {
            .version = 4,
            .address = {255,255,255,255},
        },
    };
    
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    NetPacket *outbound;
    
    insert_application_level_pilot(&shipyard_request, (uint8_t*)data, data_length);
    assemble_udp_armor(&shipyard_request, (uint16_t)from.port, (uint16_t)to.port);
    assemble_ip_armor(&shipyard_request, &(from.addr), &(to.addr));
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, bcast_mac);
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
    free(data);
    ShipyardBuffer_destroy(&shipyard_request);
}

void issue_tcp_control(sockaddr *from, sockaddr *to, uint16_t window_size, uint16_t flags, uint32_t sequence_number, uint32_t ack_number) {
    assert(to->addr.version == from->addr.version);
    
    ShipyardBuffer shipyard_request;
    ShipyardBuffer_initialize(&shipyard_request);
    NetPacket *outbound;
    
    TCPHeader *control = make_tcp_control_segment(&shipyard_request, from->port, to->port, window_size, flags, sequence_number, ack_number);
    insert_tcp_pilot(&shipyard_request, control, sizeof(TCPHeader));

    assemble_ip_armor(&shipyard_request, &(from->addr), &(to->addr));
    
    assemble_ethernet_armor(&shipyard_request, self_description.nic_manifest.mac, link_level_routing(&(to->addr)));
    ShipyardBuffer_launch(&shipyard_request, &outbound);
    publish(self_description.tx_topic, outbound);
    
    free(control);
    ShipyardBuffer_destroy(&shipyard_request);
}
