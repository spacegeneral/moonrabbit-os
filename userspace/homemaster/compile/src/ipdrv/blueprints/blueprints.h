#ifndef _IPDRV_BLUEPRINTS_BLUEPRINTS_H
#define _IPDRV_BLUEPRINTS_BLUEPRINTS_H


#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/network.h>

#include "../protocol/arp.h"



bool guard_gateway_mac(long long int timeout);
bool guard_unknown_mac(IpAddress *ip, long long int timeout);
bool guard_dhcp_client_state(int state, long long int timeout);
void arp_as_needed(IpAddress *ip);
bool arp_claim_address(IpAddress *ip);

void issue_arp_probe(IpAddress *ip);
void issue_arp_announcement(IpAddress *my_ip);
void issue_arp_response(IpAddress *my_ip, ARPFrame *request);
void issue_icmp_ping(IpAddress *ip, int id_num, int seq_num);
void issue_icmp_pong(IpAddress *ip, int id_num, int seq_num, void *payload, size_t payload_size);
void issue_udp_packet(sockaddr *from, sockaddr *to, void *data, size_t data_length);
void issue_dhcp_discover(uint32_t transaction_id);
void issue_dhcp_request(uint32_t transaction_id, IpAddress *dhcp_server_ip, IpAddress *offered);
void issue_tcp_control(sockaddr *from, sockaddr *to, uint16_t window_size, uint16_t flags, uint32_t sequence_number, uint32_t ack_number);

#endif
