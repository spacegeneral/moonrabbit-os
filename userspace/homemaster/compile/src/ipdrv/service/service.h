#ifndef _IPDRV_SERVICE_SERVICE_H
#define _IPDRV_SERVICE_SERVICE_H


#include <stdint.h>
#include <stddef.h>
#include "../shipyard.h"


typedef enum IPDRVServices {
    ARP_Service,
    ICMP_Service,
    UDP_Service,
    DHCP_Service,
    
    NUM_IPDRV_Services
} IPDRVServices;


typedef void (*ipdrv_service_callback) (ShipyardBuffer* shipyard, void *data_start, size_t data_length);

typedef struct IPDRVServiceTable {
    ipdrv_service_callback table[NUM_IPDRV_Services];
} IPDRVServiceTable;


void IPDRVServiceTable_dispatch(IPDRVServiceTable *table, int service, ShipyardBuffer* shipyard, void *data_start, size_t data_length);
void IPDRVServiceTable_init_empty(IPDRVServiceTable *table);





void arp_table_updater(ShipyardBuffer* shipyard, void *data_start, size_t data_length);
void icmp_receiver_and_ponger(ShipyardBuffer* shipyard, void *data_start, size_t data_length);
extern ipdrv_service_callback pong_callback;
void dhcp_receiver(ShipyardBuffer* shipyard, void *data_start, size_t data_length);
void udp_dispatcher(ShipyardBuffer* shipyard, void *data_start, size_t data_length);


#endif
