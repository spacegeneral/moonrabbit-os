#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "protocol/ethernet.h"
#include "protocol/ip.h"
#include "protocol/arp.h"
#include "protocol/icmp.h"
#include "protocol/tcp.h"
#include "pilot.h"


void insert_network_level_pilot(ShipyardBuffer* shipyard, uint8_t *payload, size_t length) {
    shipyard->network_header = shipyard->network_data = shipyard->buffer + DEFAULT_SHIPYARD_MARGIN + sizeof(EthernetHeader);
    shipyard->network_total_length = length;
    shipyard->network_ethertype = EtherType_ipv4; // default value
    memcpy(shipyard->network_data, payload, length);
}

void insert_tranport_level_pilot(ShipyardBuffer* shipyard, uint8_t *payload, size_t length) {
    shipyard->transport_header = shipyard->transport_data = shipyard->buffer + DEFAULT_SHIPYARD_MARGIN + sizeof(EthernetHeader) + IPv6_HEADER_LENGTH;
    shipyard->transport_total_length = length;
    shipyard->transport_protocol = PROTO_UDP; // default value
    memcpy(shipyard->transport_data, payload, length);
}

void insert_application_level_pilot(ShipyardBuffer* shipyard, uint8_t *payload, size_t length) {
    shipyard->application_data = shipyard->buffer + DEFAULT_SHIPYARD_MARGIN + sizeof(EthernetHeader) + IPv6_HEADER_LENGTH + sizeof(TCPHeader);
    shipyard->application_length = length;
    memcpy(shipyard->application_data, payload, length);
}




void insert_arp_pilot(ShipyardBuffer* shipyard, ARPFrame *payload) {
    insert_network_level_pilot(shipyard, (uint8_t*)payload, sizeof(ARPFrame));
    shipyard->network_ethertype = EtherType_arp;
}

void eject_arp_pilot(ShipyardBuffer* shipyard, ARPFrame **payload) {
    assert(shipyard->network_ethertype == EtherType_arp);
    shipyard->network_data = shipyard->network_header;
    shipyard->network_total_length = sizeof(ARPFrame);
    *payload = (ARPFrame*) shipyard->network_header;
}



void insert_icmp_pilot(ShipyardBuffer* shipyard, ICMPPacket *payload, size_t length_header_plus_data) {
    insert_tranport_level_pilot(shipyard, (uint8_t*)payload, length_header_plus_data);
    shipyard->transport_data = shipyard->transport_header + sizeof(ICMPHeader);
    shipyard->transport_protocol = PROTO_ICMP;
}

void eject_icmp_pilot(ShipyardBuffer* shipyard, ICMPPacket **payload, size_t length_header_plus_data) {
    assert(shipyard->transport_protocol == PROTO_ICMP);
    shipyard->transport_data = shipyard->transport_header + sizeof(ICMPHeader);
    shipyard->transport_total_length = length_header_plus_data;
    *payload = (ICMPPacket*) shipyard->transport_header;
}



void insert_tcp_pilot(ShipyardBuffer* shipyard, TCPHeader *payload, size_t length_header_plus_data) {
    insert_tranport_level_pilot(shipyard, (uint8_t*)payload, length_header_plus_data);
    shipyard->transport_data = shipyard->transport_header + sizeof(ICMPHeader);
    shipyard->transport_protocol = PROTO_TCP;
}

void eject_tcp_pilot(ShipyardBuffer* shipyard, TCPHeader **payload, size_t length_header_plus_data) {
    assert(shipyard->transport_protocol == PROTO_TCP);
    shipyard->transport_data = shipyard->transport_header + sizeof(TCPHeader);
    shipyard->transport_total_length = length_header_plus_data;
    *payload = (TCPHeader*) shipyard->transport_header;
}
