#ifndef _IPDRV_PILOT_H
#define _IPDRV_PILOT_H


#include <stdint.h>
#include "protocol/arp.h"
#include "protocol/icmp.h"
#include "protocol/tcp.h"
#include "shipyard.h"



void insert_network_level_pilot(ShipyardBuffer* shipyard, uint8_t *payload, size_t length);
void insert_tranport_level_pilot(ShipyardBuffer* shipyard, uint8_t *payload, size_t length);
void insert_application_level_pilot(ShipyardBuffer* shipyard, uint8_t *payload, size_t length);

void insert_arp_pilot(ShipyardBuffer* shipyard, ARPFrame *payload);
void eject_arp_pilot(ShipyardBuffer* shipyard, ARPFrame **payload);

void insert_icmp_pilot(ShipyardBuffer* shipyard, ICMPPacket *payload, size_t length_header_plus_data);
void eject_icmp_pilot(ShipyardBuffer* shipyard, ICMPPacket **payload, size_t length_header_plus_data);

void insert_tcp_pilot(ShipyardBuffer* shipyard, TCPHeader *payload, size_t length_header_plus_data);
void eject_tcp_pilot(ShipyardBuffer* shipyard, TCPHeader **payload, size_t length_header_plus_data);

#endif
 
