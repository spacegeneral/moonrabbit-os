#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/network.h>

#include "shipyard.h"
#include "protocol/ethernet.h"




void ShipyardBuffer_initialize(ShipyardBuffer* shipyard) {
    shipyard->buffer = malloc(DEFAULT_SHIPYARD_LENGTH);
    ShipyardBuffer_reset(shipyard);
}


void ShipyardBuffer_reset(ShipyardBuffer* shipyard) {
    memset(shipyard->buffer, 0, DEFAULT_SHIPYARD_LENGTH);
    
    shipyard->datalink_header = NULL;
    shipyard->datalink_data = NULL;
    shipyard->datalink_total_length = 0;
    
    shipyard->network_header = NULL;
    shipyard->network_data = NULL;
    shipyard->network_total_length = 0;
    
    shipyard->transport_header = NULL;
    shipyard->transport_data = NULL;
    shipyard->transport_total_length = 0;
    
    shipyard->application_data = NULL;
    shipyard->application_length = 0;
    
    shipyard->src_port = 0;
    shipyard->dest_port = 0;
}

void ShipyardBuffer_destroy(ShipyardBuffer* shipyard) {
    free(shipyard->buffer);
}


void ShipyardBuffer_launch(ShipyardBuffer* shipyard, NetPacket **packet_view) {
    assert(shipyard->datalink_header != NULL);
    assert(shipyard->datalink_total_length > 0);
    
    uint64_t *length_tag_ptr = (uint64_t*)(shipyard->datalink_header - sizeof(uint64_t));
    *length_tag_ptr = shipyard->datalink_total_length;
    *packet_view = (NetPacket*) length_tag_ptr;
}


void ShipyardBuffer_dock(ShipyardBuffer* shipyard, NetPacket **inbound_packet_view) {
    shipyard->datalink_header = shipyard->buffer + DEFAULT_SHIPYARD_MARGIN;
    *inbound_packet_view = (NetPacket*) (shipyard->datalink_header - sizeof(uint64_t));
}

void ShipyardBuffer_debug(ShipyardBuffer* shipyard) {
    char src_port_str[128];
    char dest_port_str[128];
    if (shipyard->src_port <= 0 || shipyard->src_port >= 65536) sprintf(src_port_str, "no port");
    else sprintf(src_port_str, "port %d", shipyard->src_port);
    if (shipyard->dest_port <= 0 || shipyard->dest_port >= 65536) sprintf(dest_port_str, "no port");
    else sprintf(dest_port_str, "port %d", shipyard->dest_port);
    
    char src_ip_str[IP_STRING_LENGTH];
    char dest_ip_str[IP_STRING_LENGTH];
    if (shipyard->network_ethertype == EtherType_ipv4 || shipyard->network_ethertype == EtherType_ipv6) format_ip(src_ip_str, &(shipyard->src_ip));
    else sprintf(src_ip_str, "no IP");
    if (shipyard->network_ethertype == EtherType_ipv4 || shipyard->network_ethertype == EtherType_ipv6) format_ip(dest_ip_str, &(shipyard->dest_ip));
    else sprintf(dest_ip_str, "no IP");
    
    char src_mac_str[MAC_STRING_LENGTH];
    char dest_mac_str[MAC_STRING_LENGTH];
    format_mac(src_mac_str, shipyard->src_mac);
    format_mac(dest_mac_str, shipyard->dest_mac);
    
    debug_printf("Begin dump of shipyard buffer data\r\n\
    Shipyard buffer                       @ %p [+0];\r\n\
        datalink_header                   @ %p [+%d];\r\n\
            network_header                @ %p [+%d];\r\n\
                transport_header          @ %p [+%d];\r\n\
                    application_data      @ %p [+%d];\r\n\
                    application_length    = %lu;\r\n\
                transport_length          = %lu;\r\n\
            network_length                = %lu;\r\n\
        datalink_length                   = %lu;\r\n\
    Ethertype:              0x%04x\r\n\
    Transport protocol:     %d\r\n\
    Source:                 %s, %s, ether %s\r\n\
    Destination:            %s, %s, ether %s\r\n\
End dump of shipyard buffer data\r\n", 
                                    shipyard->buffer, 
                                    shipyard->datalink_header, (unsigned)shipyard->datalink_header - (unsigned)shipyard->buffer, 
                                    shipyard->network_header, (unsigned)shipyard->network_header - (unsigned)shipyard->buffer, 
                                    shipyard->transport_header, (unsigned)shipyard->transport_header - (unsigned)shipyard->buffer, 
                                    shipyard->application_data, (unsigned)shipyard->application_data - (unsigned)shipyard->buffer,
                                    shipyard->application_length,
                                    shipyard->transport_total_length,
                                    shipyard->network_total_length,
                                    shipyard->datalink_total_length, 
                                    shipyard->network_ethertype, 
                                    shipyard->transport_protocol,
                                    src_port_str, src_ip_str, src_mac_str,
                                    dest_port_str, dest_ip_str, dest_mac_str);
}
