#include "endianness.h"


void put_bigendian_word(uint16_t number, uint8_t *dest) {
    dest[0] = number >> 8;
    dest[1] = number & 0xFF;
}

uint16_t get_bigendian_word(uint16_t number_be) {
    uint8_t *src = (uint8_t*) &number_be;
    return (((uint16_t) src[0]) << 8) \
         | (((uint16_t) src[1])     ) \
         ;
}

void put_bigendian_dword(uint32_t number, uint8_t *dest) {
    dest[0] = number >> 24;
    dest[1] = (number >> 16) & 0xFF;
    dest[2] = (number >> 8) & 0xFF;
    dest[3] = number & 0xFF;
    
}

uint32_t get_bigendian_dword(uint32_t number_be) {
    uint8_t *src = (uint8_t*) &number_be;
    return (((uint32_t) src[0]) << 24) \
         | (((uint32_t) src[1]) << 16) \
         | (((uint32_t) src[2]) << 8 ) \
         | (((uint32_t) src[3])      ) \
         ;
}
