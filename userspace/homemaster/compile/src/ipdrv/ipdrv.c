#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/serv.h>
#include <sys/socket.h>
#include <sys/errno.h>


#include "ipdrv.h"
#include "self_description.h"
#include "shipyard.h"
#include "armor.h"
#include "pilot.h"
#include "tables.h"
#include "disassemble.h"
#include "endianness.h"
#include "protocol/ip.h"
#include "protocol/arp.h"
#include "protocol/icmp.h"
#include "protocol/ethernet.h"
#include "blueprints/blueprints.h"
#include "session/session.h"


#include "../inih/ini.h"


#define SOCK_RACE Human
#define SOCK_CLASS Citizen

#define DHCP_MAX_ATTEMPTS 4

#define NIC_MANIFEST_FILE "f,sys,dial,robot,nics"
#define CONFIG_FILE "f,def,init,appdata,ipdrv,config.ini"
#define NIC_TX_TOPIC_FMT "%s/eth%d/tx"
#define NIC_RX_TOPIC_FMT "%s/eth%d/rx"



SelfDescription self_description;
int nic_id = 0; // used only for initialization!



bool setup_interface(int nic_id) {
    FILE *fp = fopen(NIC_MANIFEST_FILE, "rb");
    if (fp == NULL) return false;
    
    fseek(fp, 0, SEEK_END);
    long num_nics = ftell(fp) / sizeof(NetIfaceManifest);
    fseek(fp, 0, SEEK_SET);
    
    if (nic_id > num_nics) {
        fclose(fp);
        return false;
    }
    
    unsigned char *nicdata = (unsigned char*) malloc(num_nics * sizeof(NetIfaceManifest));
    if (fread(nicdata, sizeof(NetIfaceManifest), num_nics, fp) != num_nics) {
        free(nicdata);
        fclose(fp);
        return false;
    }
    
    fclose(fp);
    
    memcpy(&(self_description.nic_manifest), nicdata + sizeof(NetIfaceManifest) * nic_id, sizeof(NetIfaceManifest));
    free(nicdata);
    
    sprintf(self_description.tx_topic, NIC_TX_TOPIC_FMT, DEFAULT_NET_IFACE_BASE_TOPIC, self_description.nic_manifest.nic_id);
    sprintf(self_description.rx_topic, NIC_RX_TOPIC_FMT, DEFAULT_NET_IFACE_BASE_TOPIC, self_description.nic_manifest.nic_id);
    
    return true;
}



static int config_handler(void* user, const char* section, const char* name, const char* value) {
    IPDRVConfig* config = (IPDRVConfig*)user;

    #define MATCH(sname, nname) (strcmp(section, sname) == 0) && (strcmp(name, nname) == 0)
    if (MATCH("connection", "gateway_ip")) {
        config->gateway_address = strdup(value);
    } else if (MATCH("connection", "gateway_mac")) {
        config->gateway_mac = strdup(value);
    } else if (MATCH("connection", "preferred_ipv")) {
        config->preferred_ipv = atoi(value);
    } else if (MATCH("connection", "lan_ip")) {
        config->lan_ip = strdup(value);
    } else if (MATCH("interface", "nic_id")) {
        config->nic_id = atoi(value);
    } else if (MATCH("persistence", "session_file")) {
        config->session_file = strdup(value);
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

bool load_config() {
    IPDRVConfig config = {
        .gateway_address = NULL,
        .gateway_mac = NULL,
        .lan_ip = NULL,
        .nic_id = 0,
        .preferred_ipv = 4,
        .session_file = "",
    };
    
    if (ini_parse(CONFIG_FILE, &config_handler, &config) < 0) return false;
    
    if (config.gateway_address == NULL) return false;
    if (!parse_ip(config.gateway_address, &(self_description.gateway_ip))) return false;
    
    if (config.gateway_mac == NULL) return false;
    if (!parse_mac(config.gateway_mac, self_description.fallback_gateway_mac)) return false;
    
    if (config.lan_ip == NULL) return false;
    if (!parse_ip(config.lan_ip, &(self_description.lan_ip))) return false;
    
    nic_id = config.nic_id;
    self_description.preferred_ip_version = config.preferred_ipv;
    self_description.session_file_name = config.session_file;
    
    free(config.gateway_address);
    free(config.gateway_mac);
    
    return true;
}




static void arp_probe_applet(IpAddress *ip) {
    issue_arp_probe(ip);
    
    clock_t started = clock();
    while (true) {
        char *matched_mac = arp_table_match_ip(ip);
        if (matched_mac != NULL) {
            char macstr[MAC_STRING_LENGTH];
            format_mac(macstr, matched_mac);
            printf("Got matching MAC: %s\n", macstr);
            break;
        }
        if (clock() - started > 5000) {
            printf("ARP probe timed out.\n");
            break;
        }
        yield();
    }
}


static int pong_applet_responses_expected = 4;
static int pong_applet_seq_num;
static int pong_applet_id_num;
static int pong_applet_responses_count;
static clock_t pong_applet_start_time;
static void pong_applet_callback(ShipyardBuffer* shipyard, void *data_start, size_t data_length) {
    ICMPHeader *header = (ICMPHeader*) data_start;
    
    clock_t now = clock();
    char ipstr[IP_STRING_LENGTH];
    format_ip(ipstr, &(shipyard->src_ip));
    
    int response_seqnum = get_bigendian_word(header->rest.echo.sequence_number);
    int response_idnum = get_bigendian_word(header->rest.echo.identifier);
    int ttl = 0;
    if (shipyard->network_ethertype == EtherType_ipv4) ttl = ((IPV4Header*)(shipyard->network_header))->ttl;

    if (response_idnum == pong_applet_id_num && response_seqnum == pong_applet_seq_num) {
        printf("    %d(%d) bytes from %s: icmp_seq=%d ttl=%d time=%d ms\n", data_length - sizeof(ICMPHeader), data_length, ipstr, response_seqnum, ttl, now - pong_applet_start_time);
        
        pong_applet_responses_count++;
        pong_applet_seq_num++;
        if (pong_applet_seq_num >= pong_applet_responses_expected) return;
        
        pong_applet_start_time = clock();
        issue_icmp_ping(&(shipyard->src_ip), pong_applet_id_num, pong_applet_seq_num);
    }
    
}


static void icmp_ping_applet(IpAddress *ip) {
    pong_callback = &pong_applet_callback;
    pong_applet_id_num = rand() % 65536;
    pong_applet_seq_num = 1;
    pong_applet_responses_count = 0;
    pong_applet_start_time = clock();
    int pong_applet_tries = 0;
    char ipstr[IP_STRING_LENGTH];
    format_ip(ipstr, ip);
    printf("ICMP ping %s with %d(%d) bytes of data.\n", ipstr, DEFAULT_ICMP_ECHO_PAYLOAD_LENGTH, DEFAULT_ICMP_ECHO_PAYLOAD_LENGTH + sizeof(ICMPHeader));
    issue_icmp_ping(ip, pong_applet_id_num, pong_applet_seq_num);
    
    while (pong_applet_responses_count < pong_applet_responses_expected) {
        if (clock() - pong_applet_start_time > 5000) {
            printf("ICMP ping timed out.\n");
            
            if (pong_applet_tries >= pong_applet_responses_expected-1) {
                pong_callback = NULL;
                break;
            } else {
                pong_applet_tries++;
                pong_applet_id_num = rand() % 65536;
                pong_applet_seq_num = 1;
                pong_applet_start_time = clock();
                issue_icmp_ping(ip, pong_applet_id_num, pong_applet_seq_num);
            }
        }
        yield();
    }
    
    printf("Got %d/%d ping packets.\n", pong_applet_responses_count, pong_applet_responses_expected);
}




void mainloop() {
    IPDRVServiceTable services;
    IPDRVServiceTable_init_empty(&services);
    
    services.table[ARP_Service] = &arp_table_updater;
    services.table[ICMP_Service] = &icmp_receiver_and_ponger;
    services.table[DHCP_Service] = &dhcp_receiver;
    services.table[UDP_Service] = &udp_dispatcher;
    
    ShipyardBuffer shipyard_response;
    ShipyardBuffer_initialize(&shipyard_response);
    NetPacket *inbound;
    ShipyardBuffer_dock(&shipyard_response, &inbound);
    
    subscribe(DEFAULT_GRAVEYARD_TOPIC);
    subscribe(self_description.rx_topic);
    
    MQStatus poll_status;
    ServStatus accept_status;
#ifdef IPDRV_ADVANCED_DEBUG
    debug_printf("ipdrv: starting receiver thread\r\n");
#endif
    self_description.mainloop_running = true;
    while (self_description.mainloop_running) {
        // reset shipyard
        ShipyardBuffer_reset(&shipyard_response);
        ShipyardBuffer_dock(&shipyard_response, &inbound);
        
        // handle queued receptions
        do {
            poll_status = poll(self_description.rx_topic, inbound);
            if (poll_status == MQ_SUCCESS) {
#ifdef IPDRV_ADVANCED_DEBUG
                if (!disassemble_received_data(&shipyard_response, &services)) {
                    debug_printf("Failed to disassemble incoming frame!!\r\n");
                }
#else
                disassemble_received_data(&shipyard_response, &services);
#endif
            }
        } while (poll_status == MQ_SUCCESS);
        
        // handle socket queued transmission
        transmit_from_sockets();
        
        // handle socket control messages
        unsigned int requestor_task_id;
        unsigned int request_id;
        SocketControlServiceMessage sock_serv_req;
        do {
            accept_status = service_accept(DEFAULT_SOCKET_CONTROL_SERVICE, &sock_serv_req, &requestor_task_id, &request_id);
            if (accept_status == SERV_SUCCESS) {
                handle_sock_control_request(&sock_serv_req, requestor_task_id);
                service_complete(DEFAULT_SOCKET_CONTROL_SERVICE, request_id);
            }
        } while (accept_status == SERV_SUCCESS);
        
        // remove sockets owned by dead tasks
        int task_id;
        do {
            poll_status = poll(DEFAULT_GRAVEYARD_TOPIC, &task_id);
            if (poll_status == MQ_SUCCESS) {
                terminate_sockets_by_taskid(task_id);
            }
        } while (poll_status == MQ_SUCCESS);
        
        // switch to other task
        yield();
    }
#ifdef IPDRV_ADVANCED_DEBUG
    debug_printf("ipdrv: stopping receiver thread\r\n");
#endif
    unsubscribe(self_description.rx_topic);
    unsubscribe(DEFAULT_GRAVEYARD_TOPIC);
    ShipyardBuffer_destroy(&shipyard_response);
    exit(0);
}


int dhcploop() {
    int attempts = 0;
    while (true) {
        if (attempts >= DHCP_MAX_ATTEMPTS) {
            memcpy(self_description.dhcp_client_state.candidate_router_mac, bcast_mac, MAC_ADDRESS_LENGTH);
            printf("DHCP max attempts exceeded!\n");
            break;
        }
        attempts++;
        
        unsigned int transaction_id = rand();
        self_description.dhcp_client_state.current_transaction_id = transaction_id;
        self_description.dhcp_client_state.status = DHCP_CLIENT_DISCOVERY;
        self_description.dhcp_client_state.router = self_description.gateway_ip;
        issue_dhcp_discover(transaction_id);
        
        if (!guard_dhcp_client_state(DHCP_CLIENT_OFFERED, DEFAULT_TIMEOUT)) {
            if (self_description.verbose) {
                printf("DHCP offer timeout\n");
            }
            continue;
        }
        
        if (self_description.verbose) {
            char myipstr[MAC_STRING_LENGTH];
            char routeripstr[MAC_STRING_LENGTH];
            format_ip(myipstr, &(self_description.dhcp_client_state.offered_address));
            format_ip(routeripstr, &(self_description.dhcp_client_state.router));
            printf("DHCP offered IP: %s ; Router: %s\n", myipstr, routeripstr);
        }
        
        self_description.dhcp_client_state.status = DHCP_CLIENT_REQUEST;
        issue_dhcp_request(transaction_id, 
                           &(self_description.dhcp_client_state.next_server), 
                           &(self_description.dhcp_client_state.offered_address));
        
        if (!guard_dhcp_client_state(DHCP_CLIENT_ACKNOWLEDGED, DEFAULT_TIMEOUT)) {
            if (self_description.verbose) {
                printf("DHCP acknowledge timeout; going on anyway.\n");
            }
        }
        
        self_description.lan_ip = self_description.dhcp_client_state.offered_address;
        self_description.preferred_ip_version = 4;
        self_description.gateway_ip = self_description.dhcp_client_state.router;
        
        if (!arp_claim_address(&(self_description.lan_ip))) {
            if (self_description.verbose) {
                char myipstr[MAC_STRING_LENGTH];
                format_ip(myipstr, &(self_description.lan_ip));
                printf("Cannot claim address %s!\n", myipstr);
            }
        }
        
        break;
    }
    
    if (self_description.verbose) {
        char myipstr[IP_STRING_LENGTH];
        char routeripstr[IP_STRING_LENGTH];
        format_ip(myipstr, &(self_description.lan_ip));
        format_ip(routeripstr, &(self_description.gateway_ip));
        printf("DHCP configuration completed. Claimed IP: %s ; Using router: %s\n", myipstr, routeripstr);
    }
    
    self_description.dhcp_client_state.status = DHCP_CLIENT_READY;
    
    exit(0);
}


void die(int code) {
    self_description.mainloop_running = false;
    if (getenv("applet") == NULL) retire_service(DEFAULT_SOCKET_CONTROL_SERVICE);
    exit(code);
}

void setup_services() {
    ServStatus serv_creation_status;
    
    serv_creation_status = advertise_service_advanced(DEFAULT_SOCKET_CONTROL_SERVICE, sizeof(SocketControlServiceMessage), SOCK_RACE, SOCK_CLASS);
    if (serv_creation_status != SERV_SUCCESS && serv_creation_status != SERV_ALREADY_EXISTS) {
        printf("Error %d cannot create service %s\n", serv_creation_status, DEFAULT_SOCKET_CONTROL_SERVICE);
        die(6);
    }
}

void setup_nic() {
    if (getenv("nic") != NULL) {
        nic_id = atoi(getenv("nic"));
    }
    
    bool setup_success = setup_interface(nic_id);
    if (!setup_success) {
        printf("Could not find NIC %d\n", nic_id);
        die(1);
    }
    
    if (self_description.verbose) {
        char macstr[MAC_STRING_LENGTH];
        format_mac(macstr, self_description.nic_manifest.mac);
        printf("Using NIC eth%d, model %s, MAC: %s\n", self_description.nic_manifest.nic_id, self_description.nic_manifest.name, macstr);
    }
}

void setup_connection() {
    bool do_remember_session = false;
    // start the mainloop thread
    int mainloop_task = spork_thread(&mainloop);
    if (mainloop_task < 0) {
        printf("Could not spork main loop: code %d\n", mainloop_task);
        die(3);
    }
    
    while (!self_description.mainloop_running) yield();  // wait for the mainloop thread to be ready
    
    if (!restore_session(self_description.session_file_name)) {  // if we can't restore a saved session, create a new one
        int dhcploop_task = spork_thread(&dhcploop);  // start the dhcp client
        if (dhcploop_task < 0) {
            printf("Could not spork dhcp loop: code %d\n", dhcploop_task);
            die(3);
        }
        
        if (!guard_dhcp_client_state(DHCP_CLIENT_READY, DEFAULT_TIMEOUT*5)) {  // wait for the dhcp client to finish
            if (self_description.verbose) {
                printf("DHCP client timeout\n");
            }
        }
        
        do_remember_session = true;
    } else {
        if (self_description.verbose) {  // print data about the restored session
            char myipstr[IP_STRING_LENGTH];
            char routeripstr[IP_STRING_LENGTH];
            char routermacstr[MAC_STRING_LENGTH];
            format_ip(myipstr, &(self_description.lan_ip));
            format_ip(routeripstr, &(self_description.gateway_ip));
            format_mac(routermacstr, arp_table_match_ip(&(self_description.gateway_ip)));
            printf("Restoring saved session with IP: %s ; router: %s (MAC: %s)\n", myipstr, routeripstr, routermacstr);
        }
    }
    
    issue_arp_probe(&(self_description.gateway_ip));  // fetch the gateway's mac address
    
    if (!guard_gateway_mac(DEFAULT_TIMEOUT)) {  // if we didn't find out the gateway mac address, use a fallback one
        if (self_description.dhcp_client_state.status == DHCP_CLIENT_READY) {  // use the one suggested by the dhcp system
            if (self_description.verbose) {
                char macstr[MAC_STRING_LENGTH];
                format_mac(macstr, self_description.dhcp_client_state.candidate_router_mac);
                printf("Timeout acquiring gateway MAC address, using %s (suggested by DHCP).\n", macstr);
            }
            arp_table_insert(self_description.dhcp_client_state.candidate_router_mac, &(self_description.gateway_ip));
        } else {  // use the one set in the config file
            if (self_description.verbose) {
                char macstr[MAC_STRING_LENGTH];
                format_mac(macstr, self_description.fallback_gateway_mac);
                printf("Timeout acquiring gateway MAC address, using %s (set in config.ini).\n", macstr);
            }
            arp_table_insert(self_description.fallback_gateway_mac, &(self_description.gateway_ip));
        }
        
    } else if (self_description.verbose) {
        char macstr[MAC_STRING_LENGTH];
        format_mac(macstr, arp_table_match_ip(&(self_description.gateway_ip)));
        printf("ARP initialization complete, gateway MAC: %s\n", macstr);
    }
    
    if (do_remember_session) remember_session(self_description.session_file_name);  // store session to disk
}

void print_help() {
    printf("Internet Protocol driver.\n");
    printf("It can run either as a normal driver, or as a standalone network applet.\n");
    printf("Parameters:\n");
    printf("    verbose     (no value) generate a verbose output, useful for debugging.\n");
    printf("    nic         numeric identifier of the network card to use,\n");
    printf("                defaults to 0 (eth0); see f,sys,dial,human,nics\n");
    printf("                for a full list of the available network cards.\n");
    printf("    applet      run an applet and then exit.\n");
    printf("                available applets are \"arp_probe\" and \"ping\".\n");
    printf("Config file:\n");
    printf("    The only config file is located at %s\n", CONFIG_FILE);
    printf("Session file:\n");
    printf("    Some session variables, i.e. the dhcp lease, are written to\n");
    printf("    a session file. The session file location is specified in\n");
    printf("    the config file.\n");
    printf("Interface:\n");
    printf("    After a normal startup, the driver will operate a service\n");
    printf("    located at /usr/net/socket/control\n");
}

int main(void) {
    
    if (getenv("help") != NULL) {
        print_help();
        return 0;
    }
    
    self_description.mainloop_running = false;
    self_description.dhcp_client_state.status = DHCP_CLIENT_CONFUSED;
    self_description.verbose = (getenv("verbose") != NULL);
    
    srand(time(NULL));
    
    if (!load_config()) {
        printf("ipdrv: Error reading config file.\n");
        return 4;
    }
    
    setup_tables();
    setup_services();
    setup_nic();
    setup_connection();
    
    if (getenv("applet") != NULL) {
        if (strcmp(getenv("applet"), "arp_probe") == 0) {
            IpAddress ip;
            if (getenv("ip") == NULL) {
                printf("Provide a valid ipv4 address (\"ip=192.168.0.1\")\n");
                die(2);
            }
            if (!parse_ip(getenv("ip"), &ip)) {
                printf("Invalid ip address %s\n", getenv("ip"));
                die(2);
            }
            arp_probe_applet(&ip);
        }
        else if (strcmp(getenv("applet"), "ping") == 0) {
            IpAddress ip;
            if (getenv("ip") == NULL) {
                printf("Provide a valid ipv4 address (\"ip=192.168.0.1\")\n");
                die(2);
            }
            if (!parse_ip(getenv("ip"), &ip)) {
                printf("Invalid ip address %s\n", getenv("ip"));
                die(2);
            }
            arp_as_needed(&ip);
            guard_unknown_mac(&ip, DEFAULT_TIMEOUT);
            icmp_ping_applet(&ip);
        }
        else {
            printf("Applet %s not available.\n", getenv("applet"));
            die(2);
        }
        die(0);
    }
    else {
        if (self_description.verbose) {
            printf("Daemon mode enaged.\n");
        }
    }
    
    return 0;
}
 
