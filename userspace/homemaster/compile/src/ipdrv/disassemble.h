#ifndef _IPDRV_DISASSEMBLE_H
#define _IPDRV_DISASSEMBLE_H


#include <stdint.h>
#include "shipyard.h"
#include "service/service.h"


bool disassemble_received_data(ShipyardBuffer* shipyard, IPDRVServiceTable *handlers);



#endif
 
