#ifndef _IPDRV_SCHOPENHAUER_H
#define _IPDRV_SCHOPENHAUER_H

#include <stdbool.h>
#include <sys/network.h>

bool should_ping_back(IpAddress *ip);
bool should_arp_respond(unsigned char* mac);


#endif
 
