#ifndef _IPDRV_ARMOR_H
#define _IPDRV_ARMOR_H


#include <stdint.h>
#include "shipyard.h"



void assemble_ethernet_armor(ShipyardBuffer* shipyard, const uint8_t *src_mac, const uint8_t *dest_mac);
bool try_disassemble_ethernet_armor(ShipyardBuffer* shipyard, size_t link_length);

void assemble_ipv4_armor(ShipyardBuffer* shipyard, IpAddress *src_ip, IpAddress *dest_ip);
bool try_disassemble_ipv4_armor(ShipyardBuffer* shipyard);
void assemble_ipv6_armor(ShipyardBuffer* shipyard, IpAddress *src_ip, IpAddress *dest_ip);
bool try_disassemble_ipv6_armor(ShipyardBuffer* shipyard);
void assemble_ip_armor(ShipyardBuffer* shipyard, IpAddress *src_ip, IpAddress *dest_ip);

void assemble_udp_armor(ShipyardBuffer* shipyard, uint16_t src_port, uint16_t dest_port);
bool try_disassemble_udp_armor(ShipyardBuffer* shipyard);


#endif
 
