#include <stdlib.h>
#include <sys/network.h>

#include "closed.h"
#include "status_transitions.h"
#include "../blueprints/blueprints.h"
#include "../endianness.h"


void tcp_sock_closed_passive_open_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard) {
    state->destination_specified = false;
    init_tcb(state);
    tcp_machine_transition_to(state, TCPCONN_LISTEN);
}

void tcp_sock_closed_active_open_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard) {
    if (state->destination_specified) {
        tcp_machine_error(state, DESTINATION_SOCK_NOT_SPECIFIED);
    } else {  // perform an active open
        init_tcb(state);
        unsigned iss = generate_iss();
        state->snd.iss = iss;
        issue_tcp_control(&(state->source), &(state->destination), state->snd.wnd, 
                                                                   TCP_FLAG_SYN, 
                                                                   iss, 0);
        state->snd.una = iss;
        state->snd.nxt = iss+1;
        tcp_machine_transition_to(state, TCPCONN_SYN_SENT);
    }
}

void tcp_sock_closed_error_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard) {
    tcp_machine_error(state, CONNECTION_DOES_NOT_EXIST);
}

void tcp_sock_closed_recv_segment_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard) {
    TCPHeader *segm = (TCPHeader*)shipyard->transport_header;
    unsigned segm_length = shipyard->transport_total_length;
    if (segm->data_offset_and_flags & TCP_FLAG_ACK) {
        unsigned int segm_seq = get_bigendian_dword(segm->sequence_number);
        issue_tcp_control(&(state->source), &(state->destination), state->snd.wnd, 
                                                                   TCP_FLAG_RST|TCP_FLAG_ACK, 
                                                                   segm_seq + segm_length, 0);
    } else {
        unsigned int segm_ack = get_bigendian_dword(segm->ack_number);
        issue_tcp_control(&(state->source), &(state->destination), state->snd.wnd, 
                                                                   TCP_FLAG_RST, 
                                                                   segm_ack, 0);
    }
}
