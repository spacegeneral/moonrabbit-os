#include <stdlib.h>
#include <sys/network.h>

#include "syn_sent.h"
#include "status_transitions.h"
#include "../blueprints/blueprints.h"
#include "../endianness.h"


static void reset_conn(TCPSocketConnectionParams *state) {
    issue_tcp_control(&(state->source), &(state->destination), state->snd.wnd, 
                                                               TCP_FLAG_RST, 
                                                               state->snd.iss, 0);
}

static void send_synack(TCPSocketConnectionParams *state) {
    issue_tcp_control(&(state->source), &(state->destination), state->snd.wnd, 
                                                               TCP_FLAG_SYN|TCP_FLAG_ACK, 
                                                               state->snd.iss,
                                                               state->rcv.nxt);
    tcp_machine_transition_to(state, TCPCONN_SYN_RCVD);
}

void tcp_sock_syn_sent_recv_segment_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard) {
    TCPHeader *segm = (TCPHeader*)shipyard->transport_header;
    if (segm->data_offset_and_flags & TCP_FLAG_ACK) {  // ack is set
        if ((segm->ack_number <= state->snd.iss) || (segm->ack_number > state->snd.nxt)) {  // non-acceptable ack
            reset_conn(state);
            return;
        } else {  // but wait! there's more checks...
            if ((segm->ack_number >= state->snd.una) && (segm->ack_number <= state->snd.nxt)) {  // connection reset
                if (segm->data_offset_and_flags & TCP_FLAG_RST) {
                    tcp_machine_error(state, CONNECTION_RESET);
                    destroy_tcb(state);
                    tcp_machine_transition_to(state, TCPEVENT_CLOSE);
                    return;
                } else {
                    if (segm->data_offset_and_flags & TCP_FLAG_SYN) {  // syn is set
                        state->rcv.nxt = segm->sequence_number + 1;
                        state->rcv.irs = segm->sequence_number;
                        state->snd.una = segm->ack_number;
                        // TODO remove acked segments from rexmt queue
                        if (state->snd.una > state->snd.iss) {
                            issue_tcp_control(&(state->source), &(state->destination), state->snd.wnd, 
                                                                                       TCP_FLAG_ACK, 
                                                                                       state->snd.nxt,
                                                                                       state->rcv.nxt);
                            tcp_machine_transition_to(state, TCPCONN_ESTABLISHED);
                        } else {
                            send_synack(state);
                        }
                    }
                }
            } else {  // non-acceptable ack
                reset_conn(state);
                return;
            }
        }
        
    } else {  // ack not set
        if (segm->data_offset_and_flags & TCP_FLAG_RST) return;  // rst is set
        else {  // rst not set
            if (segm->data_offset_and_flags & TCP_FLAG_SYN) {  // syn is set
                state->rcv.nxt = segm->sequence_number + 1;
                state->rcv.irs = segm->sequence_number;
                send_synack(state);
            }
        }
    }
}

void tcp_sock_syn_sent_error_queuing_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard) {
    tcp_machine_error(state, QUEUING_DATA_DURING_ESTABLISHED);
}

void tcp_sock_syn_sent_error_aready_exists_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard) {
    tcp_machine_error(state, CONNECTION_ALREADY_EXISTS);
}