#ifndef _IPDRV_TCP_STATUS_TRANSITION_H
#define _IPDRV_TCP_STATUS_TRANSITION_H


#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/network.h>

#include "../protocol/socket.h"
#include "../shipyard.h"
#include "common.h"


typedef void (*tcp_machine_event) (TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);

extern tcp_machine_event tcp_machine_trigger_table[NUM_TCPCONN_STATUSES][NUM_TCPCONN_EVENTS];
//                                                 ^^^ current status    ^^^ received event

bool tcp_machine_transition_to(TCPSocketConnectionParams *state, TCPConnectionStatus to_status);
void setup_tcp_machine_tables();


bool tcp_machine_receive_event(TCPSocketConnectionParams *state, TCPConnectionEvents event, ShipyardBuffer *shipyard);
void tcp_machine_error(TCPSocketConnectionParams *state, int errorcode);


#endif
