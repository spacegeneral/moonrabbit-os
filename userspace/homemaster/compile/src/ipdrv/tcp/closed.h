#ifndef _IPDRV_TCP_CLOSED_H
#define _IPDRV_TCP_CLOSED_H


#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/network.h>

#include "../protocol/socket.h"
#include "../protocol/tcp.h"
#include "../shipyard.h"


void tcp_sock_closed_passive_open_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);
void tcp_sock_closed_active_open_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);
void tcp_sock_closed_recv_segment_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);


void tcp_sock_closed_error_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);


#endif
