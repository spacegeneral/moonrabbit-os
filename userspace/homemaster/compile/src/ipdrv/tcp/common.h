#ifndef _IPDRV_TCP_COMMON_H
#define _IPDRV_TCP_COMMON_H


#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/network.h>

#include "../protocol/socket.h"
#include "../protocol/tcp.h"
#include "../shipyard.h"


void init_tcb(TCPSocketConnectionParams *state);
void destroy_tcb(TCPSocketConnectionParams *state);

bool is_segment_seq_acceptable(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);


#endif
