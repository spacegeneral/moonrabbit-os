#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <libgui/libgui.h>
#include <sys/proc.h>
#include <sys/term.h>
#include <sys/mq.h>
#include <sys/keycodes.h>
#include <math.h>
#include <yakumo.h>


SoftTerminal info;
OpenDoor main_door;
bool running = true;

#define NOTCH_LEN 10.0
#define NOTCH_BORDER 1.0
#define HOUR_ARM_LEN 22.0
#define MIN_ARM_LEN 35.0
#define SEC_ARM_LEN 48.0

Color24 colorkey;

Color24 dial_color;
Color24 notch_color;
Color24 arms_color;

int dial_radius = 50;


#define memfmt "mem used/tot: %dK/%dK (%d%%)"
#define MEM_UPDATE_RATE_MS 1000

typedef struct Memstats {
    unsigned int free;
    unsigned int total_available;
} Memstats;


void mouse_callback(LibguiMsgMouseEvent *event) {
    SoftTerminal_clear(&info);
    SoftTerminal_gotoxy(&info, 0, 0);
    SoftTerminal_printf(&info, "mouse x,y: %d,%d\nbuttons (l|m|r): %s|%s|%s\nclick: %d\ndbclick: %d\nenter: %d\nleave: %d\ntime: %llu\n", 
                        event->x, event->y,
                        (event->btn_left ? "down" : "up"), (event->btn_mid ? "down" : "up"), (event->btn_right ? "down" : "up"), 
                        event->is_click, event->is_dbclick, event->is_enter, event->is_leave, 
                        event->timestamp);
    OpenDoor_update(&main_door, true);
}


void keycode_callback(KeyEvent *keycode) {
    switch (keycode->keycode) {
        case KEY_Escape:
            running = false;
            break;
    }
}


void drawface(Bitmap *bitmap, struct tm *local_time) {
    Point center = {
        .x = dial_radius,
        .y = dial_radius,
    };
    safe_draw_circle_area(bitmap, center, dial_radius, dial_color);
    
    double cx = (double) center.x;
    double cy = (double) center.y;
    
    // draw notches on the dial
    for (int step=0; step<12; step++) {
        double alpha = ((2.0*M_PI) / 12.0) * (double)step;
        
        Point inner = {
            .x = (int) (cx + ((double)dial_radius - NOTCH_BORDER - NOTCH_LEN) * cos(alpha)),
            .y = (int) (cy + ((double)dial_radius - NOTCH_BORDER - NOTCH_LEN) * sin(alpha)),
        };
        Point outer = {
            .x = (int) (cx + ((double)dial_radius - NOTCH_BORDER) * cos(alpha)),
            .y = (int) (cy + ((double)dial_radius - NOTCH_BORDER) * sin(alpha)),
        };
        
        draw_line(bitmap, inner, outer, notch_color);
    }
    
    double hour_a = ((4.0 * M_PI * (double)(local_time->tm_hour * 3600 + local_time->tm_min * 60 + local_time->tm_sec)) / (24.0 * 3600.0)) + M_PI + M_PI_2;
    if (hour_a >= 2*M_PI) hour_a -= 2*M_PI;
    Point outer_hour = {
        .x = (int) (cx + (HOUR_ARM_LEN * cos(hour_a))),
        .y = (int) (cy + (HOUR_ARM_LEN * sin(hour_a))),
    };
    draw_line(bitmap, center, outer_hour, arms_color);
    //draw_circle_area(bitmap, outer_hour, 3, arms_color);
    
    double min_a = ((2.0 * M_PI * (double)(local_time->tm_min * 60 + local_time->tm_sec)) / (3600.0)) + M_PI + M_PI_2;
    if (min_a >= 2*M_PI) min_a -= 2*M_PI;
    Point outer_min = {
        .x = (int) (cx + (MIN_ARM_LEN * cos(min_a))),
        .y = (int) (cy + (MIN_ARM_LEN * sin(min_a))),
    };
    draw_line(bitmap, center, outer_min, arms_color);
    
    double sec_a = ((2.0 * M_PI * (double)(local_time->tm_sec)) / (60.0)) + M_PI + M_PI_2;
    if (sec_a >= 2*M_PI) sec_a -= 2*M_PI;
    Point outer_sec = {
        .x = (int) (cx + (SEC_ARM_LEN * cos(sec_a))),
        .y = (int) (cy + (SEC_ARM_LEN * sin(sec_a))),
    };
    draw_line(bitmap, center, outer_sec, arms_color);
}


bool must_tick(struct tm *time_now, struct tm *time_then) {
    return time_now->tm_sec != time_then->tm_sec || time_now->tm_min != time_then->tm_min || time_now->tm_hour != time_then->tm_hour;
}

int main_clock(void) {
    BitmapFont font;
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    colorkey = make_color_8bpp(255, 0, 255);
    
    dial_color = make_color_8bpp(255, 255, 255);
    notch_color = make_color_8bpp(0, 0, 0);
    arms_color = make_color_8bpp(0, 0, 0);
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = ti.screensize.w - dial_radius*2 - 2,
        .y = ti.screensize.h - dial_radius*2 - 2,
        .w = dial_radius*2,
        .h = dial_radius*2,
    };
    bool open_success = open_main_door_advanced(&main_door, main_area, "Clock", 0, 0, &colorkey, false);
    if (!open_success) {
        printf("doorclock: Error opening main door\n");
        return 2;
    }
    
    Rect blank_area = {
        .x = 0,
        .y = 0,
        .w = main_area.w,
        .h = main_area.h,
    };
    draw_rectangle_area(&(main_door.face), blank_area, colorkey);
    
    //OpenDoor_update(&main_door, true);
    
    main_door.keycode_callback = &keycode_callback;
    
    time_t currtime;
    struct tm local_time;
    struct tm old_local_time;
    
    
    while (running) {
        currtime = time(NULL);
        localtime_r(&currtime, &local_time);
        
        if (must_tick(&local_time, &old_local_time)) {
            drawface(&(main_door.face), &local_time);
            OpenDoor_update(&main_door, true);
            
            old_local_time = local_time;
        }
        
        OpenDoor_handle_io(&main_door);
        yield();
    }
    
    OpenDoor_close(&main_door);
    destroy_bitmap_font(font);
    
    return 0;
}

int main_mem(void) {
    BitmapFont font;
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = ti.screensize.w - 250 - 1,
        .y = 0,
        .w = 250,
        .h = 50,
    };
    bool open_success = open_main_door(&main_door, main_area, "Mem-O-Meter");
    if (!open_success) {
        printf("doormem: Error opening main door\n");
        return 2;
    }
    
    main_door.keycode_callback = &keycode_callback;
    
    Rect label_pos = {.x = 5, .y = 5, .w = 245, .h = 40};
    SoftTerminal label = SoftTerminal_init(&(main_door.face), label_pos, font, make_color_8bpp(255, 0, 0), make_color_8bpp(90, 18, 68), 0, true);
    
    FILE *fp = fopen("f,sys,dial,robot,memstats","rb");
    if (fp == NULL) return 3;
    
    Memstats stats;
    Memstats old_stats;
    bool encounter_error = false;
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        
        rewind(fp);
        if (fread(&stats, sizeof(Memstats), 1, fp) != 1) {
            if (!encounter_error) {
                encounter_error = true;
                SoftTerminal_clear(&label);
                SoftTerminal_gotoxy(&label, 0, 0);
                SoftTerminal_printf(&label, "error");
                OpenDoor_update(&main_door, true);
            }
        }
        else {
            unsigned int used = stats.total_available - stats.free;
            unsigned int used_k = used/1024;
            unsigned int total_k = stats.total_available/1024;
            encounter_error = false;
            if (stats.free != old_stats.free) {
                SoftTerminal_clear(&label);
                SoftTerminal_gotoxy(&label, 0, 0);
                SoftTerminal_printf(&label, memfmt, used_k, total_k, (used_k*100)/total_k);
                OpenDoor_update(&main_door, true);
                old_stats = stats;
            }
        }
        
        u_sleep_milli(MEM_UPDATE_RATE_MS);
    }
    
    OpenDoor_close(&main_door);
    destroy_bitmap_font(font);
    fclose(fp);
    
    return 0;
}

int main_image(void) {
    char *imgname = getenv("file");
    if (imgname == NULL) {
        printf("Image file not specified.\n");
        exit(1);
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = 0,
        .y = 0,
        .w = 10,
        .h = 10,
    };
    bool open_success = open_main_door_advanced(&main_door, main_area, "Image viewer", 0, 0, NULL, false);
    if (!open_success) {
        printf("Error opening main door\n");
        return 2;
    }
    
    main_door.keycode_callback = &keycode_callback;
    
    destroy_bitmap(&(main_door.face));
    
    int error;
    float scale = 1.0;
    if (getenv("scale") != NULL) {
        scale = atof(getenv("scale"));
    }
    
    error = load_any_image(&(main_door.face), imgname, scale);

    if (error) {
        printf("Error %d loading image\n");
        running = false;
    } else {
        OpenDoor_update(&main_door, true);
    }
    
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        yield();
    }
    
    OpenDoor_close(&main_door);
    
    return 0;
}

int main_test(void) {
    BitmapFont font;
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = 5,
        .y = ti.screensize.h - 200 - 5,
        .w = 250,
        .h = 200,
    };
    bool open_success = open_main_door(&main_door, main_area, "Hello world!");
    if (!open_success) {
        printf("hello_gui: Error opening main door\n");
        return 2;
    }
    
    main_door.mouse_callback = &mouse_callback;
    main_door.keycode_callback = &keycode_callback;
    
    Rect label_pos = {.x = 5, .y = 5, .w = 245, .h = 40};
    SoftTerminal label = SoftTerminal_init(&(main_door.face), label_pos, font, make_color_8bpp(255, 0, 0), make_color_8bpp(90, 18, 68), 1, true);
    SoftTerminal_printf(&label, "Hello world!");
    
    Rect info_pos = {.x = 5, .y = 45, .w = 245, .h = 150};
    info = SoftTerminal_init(&(main_door.face), info_pos, font, make_color_8bpp(255, 255, 255), make_color_8bpp(90, 18, 68), 0, true);
    
    bool update_success = OpenDoor_update(&main_door, true);
    if (!update_success) {
        printf("Error updating main door\n");
        return 3;
    }
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        yield();
    }
    
    OpenDoor_close(&main_door);
    destroy_bitmap_font(font);
    
    return 0;
}

int main_fps(void) {
    BitmapFont font;
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = 0,
        .y = 0,
        .w = 84,
        .h = 64,
    };
    bool open_success = open_main_door(&main_door, main_area, "FPS-O-Meter");
    if (!open_success) {
        printf("doorfps: Error opening main door\n");
        return 2;
    }
    
    main_door.keycode_callback = &keycode_callback;
    
    Rect label_pos = {.x = 2, .y = 2, .w = 80, .h = 60};
    SoftTerminal label = SoftTerminal_init(&(main_door.face), label_pos, font, make_color_8bpp(255, 0, 0), make_color_8bpp(90, 18, 68), 1, true);
    
    int fps_now, fps_old;
    bool encounter_error = false;
    
    subscribe("/usr/idol/fps");
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        
        SoftTerminal_clear(&label);
        SoftTerminal_gotoxy(&label, 0, 0);
        
        MQStatus status = poll("/usr/idol/fps", &fps_now);
        
        if (status != MQ_SUCCESS && status != MQ_NO_MESSAGES) {
            if (!encounter_error) {
                encounter_error = true;
                SoftTerminal_clear(&label);
                SoftTerminal_gotoxy(&label, 0, 0);
                SoftTerminal_printf(&label, "error");
                OpenDoor_update(&main_door, true);
            }
        } else {
            encounter_error = false;
            if (fps_now != fps_old) {
                SoftTerminal_clear(&label);
                SoftTerminal_gotoxy(&label, 0, 0);
                SoftTerminal_printf(&label, "%d", fps_now);
                OpenDoor_update(&main_door, true);
            }
            fps_old = fps_now;
        }
        
        yield();
    }
    
    unsubscribe("/usr/idol/fps");
    
    OpenDoor_close(&main_door);
    destroy_bitmap_font(font);
    
    return 0;
}


typedef struct LorenzParams {
    Vertex coord;
    double a;
    double b;
    double c;
    double t;
} LorenzParams;


static void init_lorenz_params(LorenzParams *params) {
    params->coord.x = 1.0 / ((double)(rand() % 10) + 1);
    params->coord.y = 1.0 / ((double)(rand() % 10) + 1);
    params->coord.z = 1.0 / ((double)(rand() % 10) + 1);
    
    params->a = /*(double)(rand() % 10) + 5;*/ 10.0;
    params->b = /*(double)(rand() % 10) + 25;*/ 28.0;
    params->c = 8.0 / 3.0;
    
    params->t = 0.01;
}

static void step_lorenz_params(LorenzParams *params) {
    double xt = params->coord.x + params->t * params->a * (params->coord.y - params->coord.x);
    double yt = params->coord.y + params->t * (params->coord.x * (params->b - params->coord.z) - params->coord.y);
    double zt = params->coord.z + params->t * (params->coord.x * params->coord.y - params->c * params->coord.z);

    params->coord.x = xt;
    params->coord.y = yt;
    params->coord.z = zt;
}

static bool lorenz_mouse_hold = false;
LibguiMsgMouseEvent lorenz_prev_event;
LibguiMsgMouseEvent lorenz_hold_start;

double rei = 0.0;
double lorenz_drag_coeff;

void lorenz_mouse_callback(LibguiMsgMouseEvent *event) {
    if (event->btn_left && !lorenz_prev_event.btn_left) {
        lorenz_hold_start = *event;
    } else if (event->btn_left && lorenz_prev_event.btn_left) {
        lorenz_mouse_hold = true;
        lorenz_drag_coeff = rei + M_PI * ((double) (lorenz_hold_start.x - event->x)) * 0.004;
    } else if (!event->btn_left && lorenz_prev_event.btn_left) {
        lorenz_mouse_hold = false;
        rei = lorenz_drag_coeff;
    }
    
    lorenz_prev_event = *event;
}

int main_lorenz() {
    BitmapFont font;
    srand(time(NULL));
    
    int linecolor = make_color_8bpp(0, 255, 0);
    int bgroundcolor = make_color_8bpp(0, 0, 0);
    
    int width = 250, height = 250, iterations = 3600;
    if (getenv("width") != NULL) width = atoi(getenv("width"));
    if (getenv("height") != NULL) height = atoi(getenv("height"));
    if (getenv("iterations") != NULL) iterations = atoi(getenv("iterations"));
    
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = max(0, rand() % (ti.screensize.w - width)),
        .y = max(0, rand() % (ti.screensize.h - height)),
        .w = width,
        .h = height,
    };
    Rect blank_area = {
        .x = 0,
        .y = 0,
        .w = main_area.w,
        .h = main_area.h,
    };
    bool open_success = open_main_door(&main_door, main_area, "Lorenz attractor");
    if (!open_success) {
        printf("doorlorenz: Error opening main door\n");
        return 2;
    }
    
    main_door.keycode_callback = &keycode_callback;
    main_door.mouse_callback = &lorenz_mouse_callback;
    
    Vertex *vertices = (Vertex*) malloc(sizeof(Vertex) * (iterations + 10));
    Vertex *vertices_work_copy = (Vertex*) malloc(sizeof(Vertex) * (iterations + 10));
    size_t num_vertices = 0;
    LorenzParams lorenz_params;
    
    double scaling = ((double) width) / 250.0;
    Vertex scale3d = {
        .x = 3.8 * scaling,
        .y = 3.8 * scaling,
        .z = 3.8 * scaling,
    };
    
    Vertex center3d= {
        .x = (double) width/2,
        .y = (double) height/2,
        .z = 0,
    };
    
#define ADDVERTEX vertices[num_vertices++] = lorenz_params.coord
    
    init_lorenz_params(&lorenz_params);
    
    int iter_num = 0;
    double omega = M_PI / (60.0 * 8.0);
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        
        if (iter_num >= iterations) {
            iter_num = 0;
            num_vertices = 0;
            init_lorenz_params(&lorenz_params);
        }
        
        step_lorenz_params(&lorenz_params);
        ADDVERTEX;
        
        memcpy(vertices_work_copy, vertices, sizeof(Vertex) * num_vertices);
        multi_vertex_scale(vertices_work_copy, num_vertices, scale3d);
        double alpha = (lorenz_mouse_hold ? lorenz_drag_coeff : rei);
        multi_vertex_rotate_euler_y(vertices_work_copy, num_vertices, alpha);
        
        Vertex g = vertex_invert(
                        multi_vertex_weight_center(vertices_work_copy, 
                                                   num_vertices));
        Vertex center = vertex_translate(g, center3d);
        multi_vertex_translate(vertices_work_copy, num_vertices, center);
        
        draw_rectangle_area(&(main_door.face), blank_area, bgroundcolor);
        
        Point *disp_coord = multi_vertex_project_ortho(vertices_work_copy, num_vertices);
        for (size_t i=0; i<num_vertices-1; i++) {
            safe_draw_line(&(main_door.face), 
                           disp_coord[i], 
                           disp_coord[i+1], 
                           linecolor);
        }
        free(disp_coord);
        
        OpenDoor_update(&main_door, true);  // yield in here
        
        iter_num++;
        if (!lorenz_mouse_hold) rei += omega;
        if (rei >= 2*M_PI) rei -= 2*M_PI;
    }
    
    free(vertices);
    free(vertices_work_copy);
    
#undef ADDVERTEX
    
    OpenDoor_close(&main_door);
    
    return 0;
}


#define COMPLAIN printf("Please specify type=gadget to run.\n Possible gadgets are \"mem\", \"clock\", \"image\", \"test\", \"fps\", \"lorenz\"\n")

int main(void) {
    char *typestr = getenv("type");
    if (typestr == NULL) {
        COMPLAIN;
        return 100;
    } else {
        if (strcmp(typestr, "mem") == 0) return main_mem();
        if (strcmp(typestr, "clock") == 0) return main_clock();
        if (strcmp(typestr, "image") == 0) return main_image();
        if (strcmp(typestr, "test") == 0) return main_test();
        if (strcmp(typestr, "fps") == 0) return main_fps();
        if (strcmp(typestr, "lorenz") == 0) return main_lorenz();
        
        COMPLAIN;
        return 200;
    }
}
