#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <libgui/libgui.h>
#include <sys/proc.h>
#include <sys/term.h>
#include <sys/keycodes.h>


OpenDoor main_door;
bool running = true;

#define NOTCH_LEN 10.0
#define NOTCH_BORDER 1.0
#define HOUR_ARM_LEN 22.0
#define MIN_ARM_LEN 35.0
#define SEC_ARM_LEN 48.0

Color24 colorkey;

Color24 dial_color;
Color24 notch_color;
Color24 arms_color;

int dial_radius = 50;



void keycode_callback(KeyEvent *keycode) {
    switch (keycode->keycode) {
        case KEY_Escape:
            running = false;
            break;
    }
}


void drawface(Bitmap *bitmap, struct tm *local_time) {
    Point center = {
        .x = dial_radius,
        .y = dial_radius,
    };
    safe_draw_circle_area(bitmap, center, dial_radius, dial_color);
    
    double cx = (double) center.x;
    double cy = (double) center.y;
    
    // draw notches on the dial
    for (int step=0; step<12; step++) {
        double alpha = ((2.0*M_PI) / 12.0) * (double)step;
        
        Point inner = {
            .x = (int) (cx + ((double)dial_radius - NOTCH_BORDER - NOTCH_LEN) * cos(alpha)),
            .y = (int) (cy + ((double)dial_radius - NOTCH_BORDER - NOTCH_LEN) * sin(alpha)),
        };
        Point outer = {
            .x = (int) (cx + ((double)dial_radius - NOTCH_BORDER) * cos(alpha)),
            .y = (int) (cy + ((double)dial_radius - NOTCH_BORDER) * sin(alpha)),
        };
        
        draw_line(bitmap, inner, outer, notch_color);
    }
    
    // draw the hours arm
    double hour_a = ((4.0 * M_PI * (double)(local_time->tm_hour * 3600 + local_time->tm_min * 60 + local_time->tm_sec)) / (24.0 * 3600.0)) + M_PI + M_PI_2;
    if (hour_a >= 2*M_PI) hour_a -= 2*M_PI;
    Point outer_hour = {
        .x = (int) (cx + (HOUR_ARM_LEN * cos(hour_a))),
        .y = (int) (cy + (HOUR_ARM_LEN * sin(hour_a))),
    };
    draw_line(bitmap, center, outer_hour, arms_color);
    //draw_circle_area(bitmap, outer_hour, 3, arms_color);
    
    // draw the minutes arm
    double min_a = ((2.0 * M_PI * (double)(local_time->tm_min * 60 + local_time->tm_sec)) / (3600.0)) + M_PI + M_PI_2;
    if (min_a >= 2*M_PI) min_a -= 2*M_PI;
    Point outer_min = {
        .x = (int) (cx + (MIN_ARM_LEN * cos(min_a))),
        .y = (int) (cy + (MIN_ARM_LEN * sin(min_a))),
    };
    draw_line(bitmap, center, outer_min, arms_color);
    
    double sec_a = ((2.0 * M_PI * (double)(local_time->tm_sec)) / (60.0)) + M_PI + M_PI_2;
    if (sec_a >= 2*M_PI) sec_a -= 2*M_PI;
    Point outer_sec = {
        .x = (int) (cx + (SEC_ARM_LEN * cos(sec_a))),
        .y = (int) (cy + (SEC_ARM_LEN * sin(sec_a))),
    };
    draw_line(bitmap, center, outer_sec, arms_color);
}


bool must_tick(struct tm *time_now, struct tm *time_then) {
    return time_now->tm_sec != time_then->tm_sec || time_now->tm_min != time_then->tm_min || time_now->tm_hour != time_then->tm_hour;
}


int main(void) {   
    colorkey = make_color_8bpp(255, 0, 255);
    
    dial_color = make_color_8bpp(255, 255, 255);
    notch_color = make_color_8bpp(0, 0, 0);
    arms_color = make_color_8bpp(0, 0, 0);
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = ti.screensize.w - dial_radius*2 - 2,
        .y = ti.screensize.h - dial_radius*2 - 2,
        .w = dial_radius*2,
        .h = dial_radius*2,
    };
    bool open_success = open_main_door_advanced(&main_door, main_area, "Clock", 0, 0, &colorkey, false);
    if (!open_success) {
        printf("doorclock: Error opening main door\n");
        return 2;
    }
    
    Rect blank_area = {
        .x = 0,
        .y = 0,
        .w = main_area.w,
        .h = main_area.h,
    };
    draw_rectangle_area(&(main_door.face), blank_area, colorkey);
    
    //OpenDoor_update(&main_door, true);
    
    main_door.keycode_callback = &keycode_callback;
    
    time_t currtime;
    struct tm local_time;
    struct tm old_local_time;
    
    
    while (running) {
        currtime = time(NULL);
        localtime_r(&currtime, &local_time);
        
        if (must_tick(&local_time, &old_local_time)) {
            drawface(&(main_door.face), &local_time);
            OpenDoor_update(&main_door, true);
            
            old_local_time = local_time;
        }
        
        OpenDoor_handle_io(&main_door);
        yield();
    }
    
    OpenDoor_close(&main_door);
    
    return 0;
}
