#include <stdio.h>
#include <stdlib.h>
#include <libgui/libgui.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/keycodes.h>


SoftTerminal info;
OpenDoor main_door;
bool running = true;



void keycode_callback(KeyEvent *keycode) {
    switch (keycode->keycode) {
        case KEY_Escape:
            running = false;
            break;
    }
}


int main(void) {
    BitmapFont font;
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = 0,
        .y = 0,
        .w = 54,
        .h = 54,
    };
    bool open_success = open_main_door(&main_door, main_area, "FPS-O-Meter");
    if (!open_success) {
        printf("doorfps: Error opening main door\n");
        return 2;
    }
    
    main_door.keycode_callback = &keycode_callback;
    
    Rect label_pos = {.x = 2, .y = 2, .w = 50, .h = 50};
    SoftTerminal label = SoftTerminal_init(&(main_door.face), label_pos, font, make_color_8bpp(255, 0, 0), make_color_8bpp(90, 18, 68), 1, true);
    
    int fps_now, fps_old;
    bool encounter_error = false;
    
    subscribe("/usr/idol/fps");
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        
        SoftTerminal_clear(&label);
        SoftTerminal_gotoxy(&label, 0, 0);
        
        MQStatus status = poll("/usr/idol/fps", &fps_now);
        
        if (status != MQ_SUCCESS && status != MQ_NO_MESSAGES) {
            if (!encounter_error) {
                encounter_error = true;
                SoftTerminal_clear(&label);
                SoftTerminal_gotoxy(&label, 0, 0);
                SoftTerminal_printf(&label, "error");
                OpenDoor_update(&main_door, true);
            }
        } else {
            encounter_error = false;
            if (fps_now != fps_old) {
                SoftTerminal_clear(&label);
                SoftTerminal_gotoxy(&label, 0, 0);
                SoftTerminal_printf(&label, "%d", fps_now);
                OpenDoor_update(&main_door, true);
            }
            fps_old = fps_now;
        }
        
        yield();
    }
    
    unsubscribe("/usr/idol/fps");
    
    OpenDoor_close(&main_door);
    destroy_bitmap_font(font);
    
    return 0;
}
