#pragma once

#include <sys/proc.h>
#include <sys/keycodes.h>
#include <libgui/libgui.h>

extern OpenDoor main_door;
extern Rect main_area;
void keycode_callback(KeyEvent *keycode);
