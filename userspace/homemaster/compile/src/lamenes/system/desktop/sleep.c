#include "system/sleep.h"
#include "system/moonrabbitos.h"

#include <stdint.h>
#include <time.h>


#define MIN_USERSPACE_SLEEP 5

time_t prev_time;

void time_align_to(unsigned long long int delta) {
    if (delta >= MIN_USERSPACE_SLEEP) {
        unsigned long long int userspace_part = (delta / MIN_USERSPACE_SLEEP) * MIN_USERSPACE_SLEEP;
        unsigned long long int userspace_sleep_start = clock();
        u_sleep_milli(delta);
        unsigned long long int actual_userspace_sleeptime = clock() - userspace_sleep_start;
        if (actual_userspace_sleeptime >= delta) return;
        unsigned long long int time_left = delta - actual_userspace_sleeptime;
        k_sleep_milli(time_left);
    } else {
        k_sleep_milli(delta);
    }
}


void align_time_period(uint32_t time_ms) {
    time_t curr_time = clock();
    time_t delta = curr_time - prev_time;
    if (delta < time_ms) {
        time_align_to(time_ms - delta);
    }
    prev_time = curr_time;
}
