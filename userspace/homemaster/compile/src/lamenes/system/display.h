#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "lamenes.h"
#include "system/moonrabbitos.h"

typedef enum {
  DisplayTypePAL = 0,
  DisplayTypeNTSC,
} DisplayType;

void display_init(uint16_t width, uint16_t height, DisplayType display_type, bool fullscreen);
void display_lock(void);
//void display_set_pixel(uint16_t x, uint16_t y, uint8_t nes_color);
void display_update(void);
void display_unlock(void);


#define display_set_pixel(chosen_x, chosen_y, chosen_color)                     \
{                                                                               \
    int nes_color = (chosen_color);                                             \
    int mx = (chosen_x);                                                        \
    int my = (chosen_y);                                                        \
    if (mx < main_area.w && my < main_area.h && nes_color != 0) {               \
        int cR = palette[nes_color].r;                                          \
        int cG = palette[nes_color].g;                                          \
        int cB = palette[nes_color].b;                                          \
        Color24 color = make_color_8bpp(cR, cG, cB);                            \
        Color24* pixel = PIXELAT_FAST(&(main_door.face), mx, my, log2_width);   \
        *pixel = color;                                                         \
    }                                                                           \
}
