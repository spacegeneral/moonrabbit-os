#include <stdio.h>
#include <sys/proc.h>

#include "modular.h"


#define INTERPRETER "kosh"
#define COMPILER "cc"


typedef enum AppType {
    APP_ELF,
    APP_SCRIPT,
    APP_JIT,
} AppType;


int execute_routine(const char *fname, AppType apptype, int crace, int cclass) {
    int return_val;
    
    if (apptype == APP_SCRIPT && !file_exists(search_executable(INTERPRETER))) {
        printf("Interpreter \"%s\" not found.\n", INTERPRETER);
        return -1;
    }
    else if (apptype == APP_JIT && !file_exists(search_executable(COMPILER))) {
        printf("Compiler \"%s\" not found.\n", COMPILER);
        return -1;
    }
    
    switch (apptype) {
        case APP_ELF:
            return_val = spoon(search_executable(fname), crace, cclass);
        break;
        case APP_SCRIPT:
            setenv("script", search_script(fname), 1);
            return_val = spoon(search_executable(INTERPRETER), crace, cclass);
            unsetenv("script");
        break;
        case APP_JIT:
            setenv("build", search_buildfile(fname), 1);
            return_val = spoon(search_executable(COMPILER), crace, cclass);
            unsetenv("build");
        break;
    }
    
    return return_val;
}

int spawn_routine(const char *fname, AppType apptype, int crace, int cclass, int *returncode) {
    int return_val;
    
    if (apptype == APP_SCRIPT && !file_exists(search_executable(INTERPRETER))) {
        printf("Interpreter \"%s\" not found.\n", INTERPRETER);
        return -1;
    }
    else if (apptype == APP_JIT && !file_exists(search_executable(COMPILER))) {
        printf("Compiler \"%s\" not found.\n", COMPILER);
        return -1;
    }
    
    switch (apptype) {
        case APP_ELF:
            return_val = spork(search_executable(fname), crace, cclass, returncode);
        break;
        case APP_SCRIPT:
            setenv("script", search_script(fname), 1);
            return_val = spork(search_executable(INTERPRETER), crace, cclass, returncode);
            unsetenv("script");
        break;
        case APP_JIT:
            setenv("build", search_buildfile(fname), 1);
            return_val = spork(search_executable(COMPILER), crace, cclass, returncode);
            unsetenv("build");
        break;
    }
    
    return return_val;
}


int execute_app(const char* fname, int crace, int cclass) {
    if (file_exists(search_buildfile(fname))) {
        return execute_routine(fname, APP_JIT, crace, cclass);
        
    } else if (file_exists(search_script(fname))) {
        return execute_routine(fname, APP_SCRIPT, crace, cclass);
        
    } else if (file_exists(search_executable(fname))) {
        return execute_routine(fname, APP_ELF, crace, cclass);
    } 
    
    else {
        printf("Executable not found in path, pwd, home or init.\n");
    }
    return -1;
}

int epexecute_app(const char* fname) {
    ProcIdent identity = ident();
    int crace = identity.phys_race_attrib;
    int cclass = identity.data_class_attrib;
    return execute_app(fname, crace, cclass);
}

int spawn_app(const char* fname, int crace, int cclass, int *returncode) {
    if (file_exists(search_buildfile(fname))) {
        return spawn_routine(fname, APP_JIT, crace, cclass, returncode);
        
    } else if (file_exists(search_script(fname))) {
        return spawn_routine(fname, APP_SCRIPT, crace, cclass, returncode);
        
    } else if (file_exists(search_executable(fname))) {
        return spawn_routine(fname, APP_ELF, crace, cclass, returncode);
    } 
    
    else {
        printf("Executable not found in path, pwd, home or init.\n");
    }
    return -1;
}

int epspawn_app(const char* fname) {
    ProcIdent identity = ident();
    int crace = identity.phys_race_attrib;
    int cclass = identity.data_class_attrib;
    return spawn_app(fname, crace, cclass, NULL);
}

int epspawn_app_checkreturn(const char* fname, int *returncode) {
    ProcIdent identity = ident();
    int crace = identity.phys_race_attrib;
    int cclass = identity.data_class_attrib;
    return spawn_app(fname, crace, cclass, returncode);
}
