#include <rice.h>
#include <string.h>
#include <stdbool.h>

#include "modular.h"


#define NUM_USH_NONWORDS 20
char ush_non_words[NUM_USH_NONWORDS][6] = {
    " ",
    "\n",
    "<",
    ">",
    "(",
    ")",
    "{",
    "}",
    ";",
    ":",
    "+",
    "-",
    "*",
    "/",
    "=",
    "!",
    "\t"
};


#define NUM_HIGHLIGHTED_USH_KEYWORDS_A 35
#define MAX_USH_KEYWORD_A_LENGTH 12
char ush_keywords_a[NUM_HIGHLIGHTED_USH_KEYWORDS_A][MAX_USH_KEYWORD_A_LENGTH] = {
    "exit", 
    "help", 
    "?", 
    "cat", 
    "ident", 
    "terminfo", 
    "exec", 
    "x", 
    "spawn", 
    "s", 
    "w", 
    "crash", 
    "ls", 
    "dir", 
    "tree", 
    "date", 
    "touch", 
    "mv",
    "mkdir", 
    "rm", 
    "ktest", 
    "mtest", 
    "set", 
    "get", 
    "unset", 
    "kill", 
    "cls", 
    "shutdown", 
    "tx", 
    "send", 
    "netmon",
    "chan_stats",
    "togglewd",
    "chsec",
    "cd"
};


static void CharacterLine_recolor(CharacterLine *line, int start, int length, int foreground, int background) {
    for (int c=start; c<start+length; c++) {
        line->cells[c].foreground_color = foreground;
        line->cells[c].background_color = background;
    }
}


static bool CharacterCell_is_ush_non_word(CharacterCell *cell) {
    for (size_t kw=0; kw<NUM_USH_NONWORDS; kw++) {
        if (strcmp(ush_non_words[kw], cell->utf8) == 0) return true;
    }
    return false;
}


void CharacterLine_highlight_ush_syntax(CharacterLine *line) {
    if (line->num_cells_used <= 0) return;
    CharacterLine_recolor(line, 0, (int)line->num_cells_used, COLOR24_TEXT, SETCOLOR_UNCHANGED);
    // important symbols
#define MATCH_SYMBOL(smbl) (strcmp(line->cells[c].utf8, smbl) == 0)
    for (size_t c=0; c<line->num_cells_used; c++) {
        if (MATCH_SYMBOL("=") || MATCH_SYMBOL(",") || MATCH_SYMBOL(".")) {
            line->cells[c].foreground_color = COLOR24_TEXT_HIGHLIGHT2;
            line->cells[c].background_color = SETCOLOR_UNCHANGED;
        }
    }
    // keywords
    for (size_t kw=0; kw<NUM_HIGHLIGHTED_USH_KEYWORDS_A; kw++) {
        int found_start = 0;
        do {
            if (found_start >= line->num_cells_used) break;
            found_start = CharacterLine_search(line, ush_keywords_a[kw], found_start);
            if (found_start >= 0) {
                if ((found_start) > 0 && !CharacterCell_is_ush_non_word(&(line->cells[found_start-1]))) {found_start++; continue;}
                if ((found_start+strlen(ush_keywords_a[kw]) < line->num_cells_used) && !CharacterCell_is_ush_non_word(&(line->cells[found_start+strlen(ush_keywords_a[kw])]))) {found_start++; continue;}
                CharacterLine_recolor(line, found_start, strlen(ush_keywords_a[kw]), COLOR24_TEXT_HIGHLIGHT, SETCOLOR_UNCHANGED);
                found_start++;
            }
        } while (found_start >= 0);
    }
}
