#ifndef MODULAR_H
#define MODULAR_H

#include <libedit/characterline.h>


int ush_main(void);
int rosh_main(void);

// search the default locations (home,app init,app PATH PWD) for an executable elf
const char* search_executable(const char *name);
// search the default locations (home,app init,app PATH PWD) for a js script
const char* search_script(const char *name);
// search the default locations (home,app init,app PATH PWD) for a JIT buildfile
const char* search_buildfile(const char *name);
// search the default locations (home,app init,app PATH PWD) for a buildfile or a js script or an elf
const char* search_cmd_any(const char *name);

int execute_app(const char* fname, int crace, int cclass);
int epexecute_app(const char* fname);
int spawn_app(const char* fname, int crace, int cclass, int *returncode);
int epspawn_app(const char* fname);
int epspawn_app_checkreturn(const char* fname, int *returncode);

void CharacterLine_highlight_c_syntax(CharacterLine *line);
void CharacterLine_highlight_mjs_syntax(CharacterLine *line);
void CharacterLine_highlight_ush_syntax(CharacterLine *line);


#endif
