#include <rice.h>
#include <string.h>
#include <stdbool.h>

#include "modular.h"


#define NUM_MJS_NONWORDS 19
char mjs_non_words[NUM_MJS_NONWORDS][6] = {
    " ",
    "\n",
    ".",
    ",",
    "<",
    ">",
    "(",
    ")",
    "{",
    "}",
    ";",
    ":",
    "?",
    "+",
    "-",
    "*",
    "/",
    "=",
    "!"
    
};


#define NUM_HIGHLIGHTED_MJS_KEYWORDS_A 31
#define MAX_MJS_KEYWORD_A_LENGTH 12
char mjs_keywords_a[NUM_HIGHLIGHTED_MJS_KEYWORDS_A][MAX_MJS_KEYWORD_A_LENGTH] = {
    "break",
    "case",
    "catch",
    "continue",
    "debugger",
    "default",
    "delete",
    "do",
    "else",
    "false",
    "finally",
    "for",
    "function",
    "if",
    "in",
    "instanceof",
    "new",
    "null",
    "return",
    "switch",
    "this",
    "throw",
    "true",
    "try",
    "typeof",
    "var",
    "void",
    "while",
    "with",
    "let",
    "undefined"
};


static void CharacterLine_recolor(CharacterLine *line, int start, int length, int foreground, int background) {
    for (int c=start; c<start+length; c++) {
        line->cells[c].foreground_color = foreground;
        line->cells[c].background_color = background;
    }
}


static bool CharacterCell_is_mjs_non_word(CharacterCell *cell) {
    for (size_t kw=0; kw<NUM_MJS_NONWORDS; kw++) {
        if (strcmp(mjs_non_words[kw], cell->utf8) == 0) return true;
    }
    return false;
}


void CharacterLine_highlight_mjs_syntax(CharacterLine *line) {
    if (line->num_cells_used <= 0) return;
    CharacterLine_recolor(line, 0, (int)line->num_cells_used, COLOR24_TEXT, SETCOLOR_UNCHANGED);
    // keywords
    for (size_t kw=0; kw<NUM_HIGHLIGHTED_MJS_KEYWORDS_A; kw++) {
        int found_start = 0;
        do {
            found_start = CharacterLine_search(line, mjs_keywords_a[kw], found_start);
            if (found_start >= 0) {
                if ((found_start) > 0 && !CharacterCell_is_mjs_non_word(&(line->cells[found_start-1]))) {found_start++; continue;}
                if ((found_start+strlen(mjs_keywords_a[kw]) < line->num_cells_used) && !CharacterCell_is_mjs_non_word(&(line->cells[found_start+strlen(mjs_keywords_a[kw])]))) {found_start++; continue;}
                CharacterLine_recolor(line, found_start, strlen(mjs_keywords_a[kw]), COLOR24_TEXT_HIGHLIGHT, SETCOLOR_UNCHANGED);
                found_start++;
                if (found_start >= line->num_cells_used) break;
            }
        } while (found_start >= 0);
    }
    // string literals 1
    bool streak_on = false;
    for (size_t c=0; c<line->num_cells_used; c++) {
        if (strcmp(line->cells[c].utf8, "\"") == 0) {
            streak_on = !streak_on;
            line->cells[c].foreground_color = COLOR24_TEXT_HIGHLIGHT2;
            line->cells[c].background_color = SETCOLOR_UNCHANGED;
        } else if (streak_on) {
            line->cells[c].foreground_color = COLOR24_TEXT_HIGHLIGHT2;
            line->cells[c].background_color = SETCOLOR_UNCHANGED;
        }
    }
    // string literals 2
    streak_on = false;
    for (size_t c=0; c<line->num_cells_used; c++) {
        if (strcmp(line->cells[c].utf8, "'") == 0) {
            streak_on = !streak_on;
            line->cells[c].foreground_color = COLOR24_TEXT_HIGHLIGHT2;
            line->cells[c].background_color = SETCOLOR_UNCHANGED;
        } else if (streak_on) {
            line->cells[c].foreground_color = COLOR24_TEXT_HIGHLIGHT2;
            line->cells[c].background_color = SETCOLOR_UNCHANGED;
        }
    }
    // c++ style comments
    int cpp_comment_start=CharacterLine_search(line, "//", 0);
    if (cpp_comment_start >= 0) {
        CharacterLine_recolor(line, cpp_comment_start, line->num_cells_used-cpp_comment_start, COLOR24_TEXT_HIGHLIGHT3, SETCOLOR_UNCHANGED);
    }
}
