#include <rice.h>
#include <string.h>
#include <stdbool.h>

#include "modular.h"


#define NUM_C_NONWORDS 20
char c_non_words[NUM_C_NONWORDS][6] = {
    " ",
    "\n",
    ".",
    ",",
    "<",
    ">",
    "(",
    ")",
    "{",
    "}",
    ";",
    ":",
    "?",
    "+",
    "-",
    "*",
    "/",
    "=",
    "!",
    "\t"
};


#define NUM_HIGHLIGHTED_C_KEYWORDS_A 43
#define MAX_C_KEYWORD_A_LENGTH 12
char c_keywords_a[NUM_HIGHLIGHTED_C_KEYWORDS_A][MAX_C_KEYWORD_A_LENGTH] = {
    "auto", 
    "const", 
    "double", 
    "float", 
    "int", 
    "short", 
    "struct", 
    "unsigned", 
    "break", 
    "continue", 
    "else", 
    "for", 
    "long", 
    "signed", 
    "switch", 
    "void", 
    "case", 
    "default", 
    "enum", 
    "goto", 
    "register", 
    "sizeof", 
    "typedef", 
    "volatile", 
    "char", 
    "do", 
    "extern", 
    "if", 
    "return", 
    "static", 
    "union", 
    "while",
    "int8_t",
    "uint8_t",
    "int16_t",
    "uint16_t",
    "int32_t",
    "uint32_t",
    "int64_t",
    "uint64_t",
    "intptr_t",
    "uintptr_t",
    "time_t"
};


static void CharacterLine_recolor(CharacterLine *line, int start, int length, int foreground, int background) {
    for (int c=start; c<start+length; c++) {
        line->cells[c].foreground_color = foreground;
        line->cells[c].background_color = background;
    }
}


static bool CharacterCell_is_c_non_word(CharacterCell *cell) {
    for (size_t kw=0; kw<NUM_C_NONWORDS; kw++) {
        if (strcmp(c_non_words[kw], cell->utf8) == 0) return true;
    }
    return false;
}


void CharacterLine_highlight_c_syntax(CharacterLine *line) {
    if (line->num_cells_used <= 0) return;
    CharacterLine_recolor(line, 0, (int)line->num_cells_used, COLOR24_TEXT, SETCOLOR_UNCHANGED);
    // preprocessor directives
    if (strcmp(line->cells[0].utf8, "#") == 0) {
        CharacterLine_recolor(line, 0, (int)line->num_cells_used, COLOR24_TEXT_HIGHLIGHT2, SETCOLOR_UNCHANGED);
        return;
    }
    // keywords
    for (size_t kw=0; kw<NUM_HIGHLIGHTED_C_KEYWORDS_A; kw++) {
        int found_start = 0;
        do {
            found_start = CharacterLine_search(line, c_keywords_a[kw], found_start);
            if (found_start >= 0) {
                if ((found_start) > 0 && !CharacterCell_is_c_non_word(&(line->cells[found_start-1]))) {found_start++; continue;}
                if ((found_start+strlen(c_keywords_a[kw]) < line->num_cells_used) && !CharacterCell_is_c_non_word(&(line->cells[found_start+strlen(c_keywords_a[kw])]))) {found_start++; continue;}
                CharacterLine_recolor(line, found_start, strlen(c_keywords_a[kw]), COLOR24_TEXT_HIGHLIGHT, SETCOLOR_UNCHANGED);
                found_start++;
                if (found_start >= line->num_cells_used) break;
            }
        } while (found_start >= 0);
    }
    // string literals
    bool streak_on = false;
    for (size_t c=0; c<line->num_cells_used; c++) {
        if (strcmp(line->cells[c].utf8, "\"") == 0) {
            streak_on = !streak_on;
            line->cells[c].foreground_color = COLOR24_TEXT_HIGHLIGHT2;
            line->cells[c].background_color = SETCOLOR_UNCHANGED;
        } else if (streak_on) {
            line->cells[c].foreground_color = COLOR24_TEXT_HIGHLIGHT2;
            line->cells[c].background_color = SETCOLOR_UNCHANGED;
        }
    }
    // c++ style comments
    int cpp_comment_start=CharacterLine_search(line, "//", 0);
    if (cpp_comment_start >= 0) {
        CharacterLine_recolor(line, cpp_comment_start, line->num_cells_used-cpp_comment_start, COLOR24_TEXT_HIGHLIGHT3, SETCOLOR_UNCHANGED);
    }
}
