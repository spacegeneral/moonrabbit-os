#include <sys/fs.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXCOMMAND 2048


static const char* elf_searchlist[] = {
    "%s",
    "f,def,cache,app,%s",
    "f,def,cache,app,%s.elf",
    "f,def,home,app,%s",
    "f,def,home,app,%s.elf",
    "f,def,init,app,%s",
    "f,def,init,app,%s.elf",
    "%s.zz",
    "f,def,cache,app,%s.zz",
    "f,def,cache,app,%s.elf.zz",
    "f,def,home,app,%s.zz",
    "f,def,home,app,%s.elf.zz",
    "f,def,init,app,%s.zz",
    "f,def,init,app,%s.elf.zz",
};

static const char* elf_searchlist_special[] = {
    "%s,%s",
    "%s,%s.elf",
    "%s,%s.elf.zz",
};

static const char* script_searchlist[] = {
    "%s",
    "f,def,cache,app,%s",
    "f,def,cache,app,%s.js",
    "f,def,home,app,%s",
    "f,def,home,app,%s.js",
    "f,def,init,app,%s",
    "f,def,init,app,%s.js",
    "%s.zz",
    "f,def,cache,app,%s.zz",
    "f,def,cache,app,%s.js.zz",
    "f,def,home,app,%s.zz",
    "f,def,home,app,%s.js.zz",
    "f,def,init,app,%s.zz",
    "f,def,init,app,%s.js.zz",
};

static const char* script_searchlist_special[] = {
    "%s,%s",
    "%s,%s.js",
    "%s,%s.js.zz",
};

static const char* buildfile_searchlist[] = {
    "%s",
    "f,def,cache,app,%s",
    "f,def,cache,app,%s.jit",
    "f,def,home,app,%s",
    "f,def,home,app,%s.jit",
    "f,def,init,app,%s",
    "f,def,init,app,%s.jit",
    "%s.zz",
    "f,def,cache,app,%s.zz",
    "f,def,cache,app,%s.jit.zz",
    "f,def,home,app,%s.zz",
    "f,def,home,app,%s.jit.zz",
    "f,def,init,app,%s.zz",
    "f,def,init,app,%s.jit.zz",
};

static const char* buildfile_searchlist_special[] = {
    "%s,%s",
    "%s,%s.jit",
    "%s,%s.jit.zz",
};


const char* search_any(const char *name, const char** searchlist, size_t searchlist_length, const char** special_searchlist, size_t special_searchlist_length) {
    static char path[MAXCOMMAND];
    char *path_var;
    char *pwd_var;
    
    if ((path_var = getenv("PATH")) != NULL) {
        for (size_t n=0; n<special_searchlist_length; n++) {
            sprintf(path, special_searchlist[n], path_var, name);
            if (file_exists(path)) return path;
        }
    }
    
    if ((pwd_var = getenv("PWD")) != NULL) {
        for (size_t n=0; n<special_searchlist_length; n++) {
            sprintf(path, special_searchlist[n], pwd_var, name);
            if (file_exists(path)) return path;
        }
    }
    
    for (size_t n=0; n<searchlist_length; n++) {
        sprintf(path, searchlist[n], name);
        if (file_exists(path)) return path;
    }
    
    return name;
}


const char* search_executable(const char *name) {
    return search_any(name, elf_searchlist, sizeof(elf_searchlist) / sizeof(char*), elf_searchlist_special, sizeof(elf_searchlist_special) / sizeof(char*));
}

const char* search_script(const char *name) {
    return search_any(name, script_searchlist, sizeof(script_searchlist) / sizeof(char*), script_searchlist_special, sizeof(script_searchlist_special) / sizeof(char*));
}

const char* search_buildfile(const char *name) {
    return search_any(name, buildfile_searchlist, sizeof(buildfile_searchlist) / sizeof(char*), buildfile_searchlist_special, sizeof(buildfile_searchlist_special) / sizeof(char*));
}

const char* search_cmd_any(const char *name) {
    const char* result;
    result = search_buildfile(name);
    if (strcmp(result, name) != 0) return result;
    result = search_script(name);
    if (strcmp(result, name) != 0) return result;
    result = search_executable(name);
    return result;
}

