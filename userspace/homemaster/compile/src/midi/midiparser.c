#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "midiparser.h"


static uint16_t get_bigendian_word(uint16_t number_be) {
    uint8_t *src = (uint8_t*) &number_be;
    return (((uint16_t) src[0]) << 8) \
         | (((uint16_t) src[1])     ) \
         ;
}

static uint32_t get_bigendian_dword(uint32_t number_be) {
    uint8_t *src = (uint8_t*) &number_be;
    return (((uint32_t) src[0]) << 24) \
         | (((uint32_t) src[1]) << 16) \
         | (((uint32_t) src[2]) << 8 ) \
         | (((uint32_t) src[3])      ) \
         ;
}

static uint32_t to32bit(uint8_t c1, uint8_t c2, uint8_t c3, uint8_t c4) {
    uint32_t value = 0L;

    value = (c1 & 0xff);
    value = (value<<8) + (c2 & 0xff);
    value = (value<<8) + (c3 & 0xff);
    value = (value<<8) + (c4 & 0xff);
    return value;
}

static size_t readvarinum(uint8_t *buffer, long *ret_value) {
    long value;
    size_t nb = 0;
    uint8_t c;

    c = buffer[nb++];
    value = c;
    
    if (c & 0x80) {
        value &= 0x7f;
        do {
            c = buffer[nb++];
            value = (value << 7) + (c & 0x7f);
        } while (c & 0x80);
    }
    
    *ret_value = value;
    return nb;
}

static double mf_ticks2sec(MidiParser *parser, unsigned long ticks) {
    if (parser->divisions > 0) {
        return ((double) (((double)(ticks) * (double)(parser->tempo)) / ((double)(parser->divisions) * 1000000.0)));
    } else {
#define lowerbyte(x) ((unsigned char)(x & 0xff))
#define upperbyte(x) ((unsigned char)((x & 0xff00)>>8))
        double smpte_format, smpte_resolution;
        smpte_format = upperbyte(parser->divisions);
        smpte_resolution = lowerbyte(parser->divisions);
        return (double) ((double) ticks / (smpte_format * smpte_resolution * 1000000.0));
#undef lowerbyte
#undef upperbyte
    }
}


MidiParserStatus MidiParser_init(MidiParser *parser) {
    parser->midi_data = NULL;
    parser->data_length = 0;
    parser->current_byte = 0;
    
    parser->num_tracks = 0;
    parser->divisions = 0;
    parser->tempo = 500000; /* the default tempo is 120 beats/minute */
    
    parser->tick_accumulator = 0;
    
    parser->note_on_callback = NULL;
    parser->note_off_callback = NULL;
    parser->text_callback = NULL;
    
    return MIDI_OK;
}


static MidiParserStatus MidiParser_parse_midi_event(MidiParser *parser, uint8_t opcode, long event_time, unsigned tracknum) {
    uint8_t channel = opcode & 0x0f;
    uint8_t type =    opcode & 0xf0;
    
    if (type == MIDI_CHANNEL_PRESSURE || type == MIDI_PROGRAM_CHANGE) {
        //debug_printf("midi event %02x (%02x)\r\n", opcode, parser->midi_data[parser->current_byte]);
        parser->current_byte++;
    } else {
        //debug_printf("midi event %02x (%02x %02x)\r\n", opcode, parser->midi_data[parser->current_byte], parser->midi_data[parser->current_byte+1]);
        unsigned note = parser->midi_data[parser->current_byte];
        unsigned velocity = parser->midi_data[parser->current_byte+1];
        
        switch (type) {
            case MIDI_NOTE_ON:
                {
                    unsigned long long timestamp = (unsigned long long)(mf_ticks2sec(parser, event_time) * 1000.0);
                    if (velocity == 0) {
                        if (parser->note_off_callback) parser->note_off_callback(note, tracknum, (unsigned) channel, velocity, timestamp);
                    }
                    else {
                        if (parser->note_on_callback) parser->note_on_callback(note, tracknum, (unsigned) channel, velocity, timestamp);
                    }
                }
            break;
            case MIDI_NOTE_OFF:
                {
                    unsigned long long timestamp = (unsigned long long)(mf_ticks2sec(parser, event_time) * 1000.0);
                    if (parser->note_off_callback) parser->note_off_callback(note, tracknum, (unsigned) channel, velocity, timestamp);
                }
            break;
        }
        
        parser->current_byte += 2;
    }
    
    return MIDI_OK;
}


static void MidiParser_parse_meta_text(MidiParser *parser, long event_time, char *text, size_t length) {
    if (parser->text_callback != NULL) {
        char *temptext = malloc(length + 1);
        memset(temptext, 0, length + 1);
        memcpy(temptext, text, length);
        unsigned long long timestamp = (unsigned long long)(mf_ticks2sec(parser, event_time) * 1000.0);
        parser->text_callback(temptext, timestamp);
        free(temptext);
    }
}

static MidiParserStatus MidiParser_parse_meta_event(MidiParser *parser, long event_time) {
    size_t meta_pos = parser->current_byte;
    parser->current_byte++;
    uint8_t meta_type = parser->midi_data[parser->current_byte];
    parser->current_byte++;
    long meta_length;
    parser->current_byte += readvarinum(parser->midi_data + parser->current_byte, &meta_length);
    
    switch (meta_type) {
        case 0x01:	/* Text event */
        case 0x02:	/* Copyright notice */
        case 0x03:	/* Sequence/Track name */
        case 0x04:	/* Instrument name */
        //case 0x05:	/* Lyric */
        case 0x06:	/* Marker */
        case 0x07:	/* Cue point */
        case 0x08:
        case 0x09:
        case 0x0a:
        case 0x0b:
        case 0x0c:
        case 0x0d:
        case 0x0e:
        case 0x0f:
            MidiParser_parse_meta_text(parser, event_time, parser->midi_data + parser->current_byte, meta_length);
            break;
        case MIDI_META_SET_TEMPO:
            {
                parser->tempo = (long) to32bit(0, 
                                               parser->midi_data[parser->current_byte], 
                                               parser->midi_data[parser->current_byte+1], 
                                               parser->midi_data[parser->current_byte+2]);
            }
            break;
    }
    //debug_printf("meta event %02x [len: %ld] @ (time: %ld, byte: %lu)\r\n", meta_type, meta_length, event_time, meta_pos);
    parser->current_byte += (size_t) meta_length;
    return MIDI_OK;
}


static MidiParserStatus MidiParser_parse_sysex_event(MidiParser *parser, long event_time) {
    parser->current_byte++;
    while (parser->midi_data[parser->current_byte++] != MIDI_SYSEX_END_EVENT) {}
    parser->current_byte++;
    //debug_printf("sysex event\r\n");
    
    return MIDI_OK;
}


static MidiParserStatus MidiParser_parse_current_track(MidiParser *parser, unsigned tracknum) {
    MidiHeaderAny *track_header = (MidiHeaderAny*) (parser->midi_data + parser->current_byte);
    if (memcmp(track_header->string, "MTrk", 4) != 0) return MIDI_TRACK_MALFORMED;
    size_t track_length = get_bigendian_dword(track_header->length);
    parser->current_byte += sizeof(MidiHeaderAny);
    size_t track_begin_index = parser->current_byte;
    
    if (track_begin_index + track_length > parser->data_length) return MIDI_FILE_LENGTH_OVERRUN;
    
    //debug_printf("... Track %u:\r\n", tracknum);
    
    parser->tick_accumulator = 0;
    
    uint8_t running_status = 0;
    while (parser->current_byte < track_begin_index + track_length) {
        long event_time;
        size_t int_length = readvarinum(parser->midi_data + parser->current_byte, &event_time);
        size_t seq_start_at = parser->current_byte;
        parser->current_byte += int_length;
        
        parser->tick_accumulator += event_time;
        
        //debug_printf("... ... dt: %04ld (%02d B @ %06lu) ", event_time, int_length, seq_start_at);
        
        uint8_t event_type = parser->midi_data[parser->current_byte];
        if (event_type >= 0x80 && event_type <= 0xEF) {
            running_status = event_type;
        } else if (event_type & 0x80) {
            running_status = 0;
        }
        
        MidiParserStatus event_err;
        if (event_type == MIDI_META_EVENT) {
            event_err = MidiParser_parse_meta_event(parser, parser->tick_accumulator);
        } else if (event_type == MIDI_SYSEX_START_EVENT) {
            event_err = MidiParser_parse_sysex_event(parser, parser->tick_accumulator);
        } else if (event_type & IS_MIDI_EVENT_FLAG) {
            uint8_t opcode = parser->midi_data[parser->current_byte];
            parser->current_byte++;
            event_err = MidiParser_parse_midi_event(parser, opcode, parser->tick_accumulator, tracknum);
        } else {
            if (running_status != 0) {
                //debug_printf("R ");
                event_err = MidiParser_parse_midi_event(parser, running_status, parser->tick_accumulator, tracknum);
            } else {
                //debug_printf("ERR\r\n");
            }
        }
        
        if (event_err) return event_err;
    }
    
    return MIDI_OK;
}


MidiParserStatus MidiParser_parse(MidiParser *parser, unsigned char *midi_data, size_t data_length) {
    if (midi_data == NULL) return MIDI_HEADER_MALFORMED;
    
    parser->midi_data = midi_data;
    parser->data_length = data_length;
    
    MidiHeaderChunk *header_chunk = (MidiHeaderChunk*) parser->midi_data;
    if (memcmp(header_chunk->header.string, "MThd", 4) != 0) return MIDI_HEADER_MALFORMED;
    if (get_bigendian_dword(header_chunk->header.length) != 6) return MIDI_HEADER_MALFORMED;
    
    int format = get_bigendian_word(header_chunk->format);
    if (format == 2) return MIDI_UNSUPPORTED_FEATURE;
    else if (format != 0 && format != 1) return MIDI_HEADER_MALFORMED;
    parser->num_tracks = get_bigendian_word(header_chunk->tracks);
    parser->divisions = get_bigendian_word(header_chunk->divisions);
    
    parser->current_byte += sizeof(MidiHeaderChunk);
    
    for (unsigned t=0; t<parser->num_tracks; t++) {
        MidiParserStatus track_err = MidiParser_parse_current_track(parser, t);
        if (track_err) return track_err;
    }
    
    return MIDI_OK;
}

