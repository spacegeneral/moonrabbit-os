#!/bin/bash

mkdir -p initramfs/app
mkdir -p initramfs/appdata/wump
mkdir -p initramfs/osdata/
mkdir -p initramfs/drivers

cp init.elf initramfs/
cp ring.elf initramfs/
#cp help.txt initramfs/

#cp unifont.lbf initramfs/osdata/

cp dummydrv.elf initramfs/drivers/
cp fatdrv.elf initramfs/drivers/

cp ubershell.elf initramfs/app/ush.elf
cp rocketshell.elf initramfs/app/rosh.elf
cp mount.elf initramfs/app/
cp page.elf initramfs/app/
cp xd.elf initramfs/app/
cp uiclock.elf initramfs/app/
cp uimem.elf initramfs/app/
cp idol.elf initramfs/app/
cp sha256sum.elf initramfs/app/
cp patche.elf initramfs/app/
cp comfilter.elf initramfs/app/

cp pngviz.elf initramfs/app/
cp jpegviz.elf initramfs/app/
cp wump.elf initramfs/app/
cp gordon.elf initramfs/app/
cp 0paint.elf initramfs/app/
cp domus.elf initramfs/app/
cp hello_gui.elf initramfs/app/
cp mofuterm.elf initramfs/app/
cp doormem.elf initramfs/app/
cp doorimage.elf initramfs/app/
cp doorclock.elf initramfs/app/

cp pngviz.elf homemaster/app/
cp jpegviz.elf homemaster/app/
cp wump.elf homemaster/app/
cp gordon.elf homemaster/app/
cp 0paint.elf homemaster/app/
cp domus.elf homemaster/app/
cp hello_gui.elf homemaster/app/
cp mofuterm.elf homemaster/app/
cp doormem.elf homemaster/app/
cp doorimage.elf homemaster/app/
cp doorclock.elf initramfs/app/


cp -r homemaster/* home/
#cp homedisk.img initramfs/
