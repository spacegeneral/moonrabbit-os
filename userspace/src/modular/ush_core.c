#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <sys/proc.h>
#include <sys/fs.h>
#include <sys/keycodes.h>
#include <sys/mouse.h>
#include <sys/mq.h>
#include <sys/term.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <sys/happy.h>
#include <yakumo.h>
#include <rice.h>

#include <collections/array.h>
#include <libedit/readline.h>
#include <libedit/characterline.h>
#include "modular.h"


#define PROMPT "%s >- "
#define MAXCOMMAND 2048
#define DEFAULT_PWD "f"

static int wild_return_catcher;


#define INTERPRETER "kosh"


static void execute_routine(char *command, size_t command_length, size_t num_tokens) {
    char *fname = strsplit_chunk_at(command, command_length, 1);
    
    for (size_t nparam=2; nparam<num_tokens; nparam++) {
        char *curr_param = strsplit_chunk_at(command, command_length, nparam);
        char *equals = strchr(curr_param, '=');
        if (equals == NULL) {
            setenv(curr_param, "1", 1);
        } else { \
            char *value = equals+1;
            char *name = curr_param;
            equals[0] = '\0';
            setenv(name, value, 1);
            equals[0] = '=';
        }
    }

    ProcIdent identity = ident();
    int crace = identity.phys_race_attrib;
    int cclass = identity.data_class_attrib;

    char *crace_str = getenv("race");
    if (crace_str != NULL) {
        int parsed_crace = parse_race(crace_str);
        if (parsed_crace >= 0) crace = parsed_crace;
    }
    char *cclass_str = getenv("class");
    if (cclass_str != NULL) {
        int parsed_cclass = parse_class(cclass_str);
        if (parsed_cclass >= 0) cclass = parsed_cclass;
    }

    int return_val = execute_app(fname, crace, cclass);
    
    printf("Process returned %d\n", return_val);
    
    for (size_t nparam=2; nparam<num_tokens; nparam++) {
        char *curr_param = strsplit_chunk_at(command, command_length, nparam);
        char *equals = strchr(curr_param, '=');
        if (equals == NULL) {
            unsetenv(curr_param);
        } else {
            char *name = curr_param;
            equals[0] = '\0';
            unsetenv(name);
            equals[0] = '=';
        }
    }
}


static void spawn_routine(char *command, size_t command_length, size_t num_tokens) {
    char *fname = strsplit_chunk_at(command, command_length, 1);
    
    for (size_t nparam=2; nparam<num_tokens; nparam++) {
        char *curr_param = strsplit_chunk_at(command, command_length, nparam);
        char *equals = strchr(curr_param, '=');
        if (equals == NULL) {
            setenv(curr_param, "1", 1);
        } else {
            char *value = equals+1;
            char *name = curr_param;
            equals[0] = '\0';
            setenv(name, value, 1);
            equals[0] = '=';
        }
    }

    ProcIdent identity = ident();
    int crace = identity.phys_race_attrib;
    int cclass = identity.data_class_attrib;

    char *crace_str = getenv("race");
    if (crace_str != NULL) {
        int parsed_crace = parse_race(crace_str);
        if (parsed_crace >= 0) crace = parsed_crace;
    }
    char *cclass_str = getenv("class");
    if (cclass_str != NULL) {
        int parsed_cclass = parse_class(cclass_str);
        if (parsed_cclass >= 0) cclass = parsed_cclass;
    }

    int return_val = spawn_app(fname, crace, cclass, NULL);
    
    printf("Child task id: %d\n", return_val);

    for (size_t nparam=2; nparam<num_tokens; nparam++) {
        char *curr_param = strsplit_chunk_at(command, command_length, nparam);
        char *equals = strchr(curr_param, '=');
        if (equals == NULL) {
            unsetenv(curr_param);
        } else {
            char *name = curr_param;
            equals[0] = '\0';
            unsetenv(name);
            equals[0] = '=';
        }
    }
}

#define SPAWNMACRO \
{ \
    char *fname = strsplit_chunk_at(command, command_length, 1); \
    for (size_t nparam=2; nparam<num_tokens; nparam++) { \
        char *curr_param = strsplit_chunk_at(command, command_length, nparam); \
        char *equals = strchr(curr_param, '='); \
        if (equals == NULL) { \
            setenv(curr_param, "1", 1); \
        } else { \
            char *value = equals+1; \
            char *name = curr_param; \
            equals[0] = '\0'; \
            setenv(name, value, 1); \
            equals[0] = '='; \
        } \
    } \
     \
    ProcIdent identity = ident(); \
    int crace = identity.phys_race_attrib; \
    int cclass = identity.data_class_attrib; \
     \
    char *crace_str = getenv("race"); \
    if (crace_str != NULL) { \
        int parsed_crace = parse_race(crace_str); \
        if (parsed_crace >= 0) crace = parsed_crace; \
    } \
    char *cclass_str = getenv("class"); \
    if (cclass_str != NULL) { \
        int parsed_cclass = parse_class(cclass_str); \
        if (parsed_cclass >= 0) cclass = parsed_cclass; \
    } \
     \
    int child_pid = spork(search_executable(fname), crace, cclass, &wild_return_catcher); \
    printf("New process task id: %d\n", child_pid); \
     \
    for (size_t nparam=2; nparam<num_tokens; nparam++) { \
        char *curr_param = strsplit_chunk_at(command, command_length, nparam); \
        char *equals = strchr(curr_param, '='); \
        if (equals == NULL) { \
            unsetenv(curr_param); \
        } else { \
            char *name = curr_param; \
            equals[0] = '\0'; \
            unsetenv(name); \
            equals[0] = '='; \
        } \
    } \
}

static void tree(char *rootpath, char *prettyname, size_t depth) {
    DirEnt *entries;
    size_t num_entries;
    
    for (size_t i=0; i<depth*2; i++) printf(" ");
    printf("%s\n", prettyname);
    
    int error = listdir(rootpath, &entries, &num_entries);
    
    if (error == 0) {
        char *entry_path;
        //printf("");  // bug: doesn't work without this
        for (size_t n=0; n<num_entries; n++) {
            entry_path = (char*) malloc(sizeof(char) * (strlen(rootpath) + strlen(entries[n].name) + 2));
            sprintf(entry_path, "%s,%s", rootpath, entries[n].name);
            tree(entry_path, entries[n].name, depth+1);
            free(entry_path);
        }
        free(entries);
        yield();
    }
}


static bool parse_command(char *command_orig) {
    char command[MAXCOMMAND];
    char command_tmp[MAXCOMMAND];
    size_t command_length = strlen(command_orig);
    memcpy(command, command_orig, command_length+1);
    size_t num_tokens = strsplit(command, ' ', command_length);
    char *first_token = strsplit_chunk_at(command, command_length, 0);
    
    happy(0.0);
    
    if (strcmp(first_token, "exit") == 0) {
        exit(0);
    }
    
    if ((strcmp(first_token, "help") == 0) || (strcmp(first_token, "?") == 0)) {
        setenv("file", "f,def,init,help.txt", 1);
        setenv("script", search_script("page"), 1);
        epspoon(search_executable("kosh"));
        unsetenv("script");
        unsetenv("file");
    }
    
    else if (strcmp(first_token, "cat") == 0) {
        if (num_tokens == 2) {
            char *fname = strsplit_chunk_at(command, command_length, 1);
            FILE *fp = fopen(fname, "r");
            if (fp == NULL) {  // retry using relative path
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, fname);
                fp = fopen(command_tmp, "r");
            }
            if (fp == NULL) {
                printf("Cannot open file %s.\n", fname);
            } else {
                char dest[257];
                size_t read_bytes;
                do {
                    memset(dest, 0, 257);
                    read_bytes = fread(dest, 1, 256, fp);
                    //printf("(cat) read bytes: %d\n", read_bytes);
                    //for (size_t c=0; c<read_bytes; c++) printf("%02X ", dest[c]);
                    //printf("\n");
                    for (size_t c=0; c<read_bytes; c++) putchar(dest[c]);
                    // printf("\nUpdated seek: %lu\n", ftell(fp));
                    //msleep(1000);
                    yield();
                } while (!feof(fp) && (read_bytes == 256));

                int close_success = fclose(fp);
                if (close_success != 0) {
                    printf("Cannot close file %s.\n", fname);
                }
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "ident") == 0) {
        ProcIdent identity;
        identity = ident();
        printf("Request ident id=%d\n", identity.request_ident);
        printf("Task id=%d\n", identity.taskid);
        printf("Task owner ident id=%d\n", identity.owner_ident);
        printf("Process clearance is %s %s\n", 
            phisical_domain_class_name[identity.phys_race_attrib],
            data_domain_class_name[identity.data_class_attrib]
            );
        printf("Flag is_user=%s\n", (identity.is_user ? "true" : "false"));
    }
    
    else if (strcmp(first_token, "terminfo") == 0) {
        TermInfo ti = getterminfo();
        printf("Terminal size (x,y,w,h): %d,%d,%d,%d\n", ti.termsize.x, ti.termsize.y, ti.termsize.w, ti.termsize.h);
        printf("Screen size (x,y,w,h): %d,%d,%d,%d\n", ti.screensize.x, ti.screensize.y, ti.screensize.w, ti.screensize.h);
        printf("Cursor at (x,y): %d,%d, cursor is %s\n", ti.cursor.x, ti.cursor.y, (ti.cursor_enabled ? "enabled" : "disabled"));
        printf("Bitmap graphics is %s\n", (ti.support_bitmap ? "supported" : "unsupported"));
        printf("Color scheme (foreground,background): %d,%d\n", ti.foreground_color, ti.background_color);
        printf("Scroll limits (top,bottom): %d,%d\n", ti.scroll_top, ti.scroll_bottom);
    }
    
    else if (strcmp(first_token, "exec") == 0 || strcmp(first_token, "x") == 0) {
        if (num_tokens >= 2) {
             execute_routine(command, command_length, num_tokens);
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "spawn") == 0 || strcmp(first_token, "s") == 0) {
        if (num_tokens >= 2) {
            spawn_routine(command, command_length, num_tokens);
        } else {
            goto bad_command;
        }
    }
    
     else if (strcmp(first_token, "w") == 0) {
        if (num_tokens >= 2) {
            char* fname = strsplit_chunk_at(command, command_length, 1);
            if (!file_exists(fname)) {
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, fname);
                fname = command_tmp;
            }
            setenv("file", fname, 1);
            epspoon(search_executable("wip"));
            unsetenv("file");
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "crash") == 0) {
        asm volatile("cli");
    }
    
    else if ((strcmp(first_token, "ls") == 0) || (strcmp(first_token, "dir") == 0)) {
        char* dirname;
        if (num_tokens < 2) {
            snprintf(command_tmp, MAXCOMMAND, "%s", getenv("PWD"));
            dirname = command_tmp;
        } else {
            dirname = strsplit_chunk_at(command, command_length, 1);
            if (!file_exists(dirname)) {
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, dirname);
                dirname = command_tmp;
            }
        }
        
        DirEnt *entries;
        size_t num_entries;
        int error;
        if ((error = listdir(dirname, &entries, &num_entries))) {
            printf("Error listing %s: %d\n", dirname, error);
        } else {
            if (strcmp(first_token, "dir") == 0) {
                for (size_t n=0; n<num_entries; n++) {
                    printf("%s  ", asctime(localtime(&(entries[n].metadata.modification_time))));
                    printf("%6s %-8s  ", phisical_domain_class_name[entries[n].metadata.race_attrib], 
                                                data_domain_class_name[entries[n].metadata.class_attrib]);
                    if (entries[n].filetype == Directory) {
                        printf("<DIR>     ");
                        setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED);
                    } else if (entries[n].filetype == Link) {
                        printf("<SYMLINK> ");
                        setcolors(COLOR24_TEXT_HIGHLIGHT2, COLOR24_UNDEFINED);
                    } else printf("%8llu  ", entries[n].filesize);
                    
                    printf("%s\n", entries[n].name);
                    setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
                }
            } else {
                for (size_t n=0; n<num_entries; n++) {
                    if (entries[n].filetype == Directory) {
                        setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED);
                    } else if (entries[n].filetype == Link) {
                        setcolors(COLOR24_TEXT_HIGHLIGHT2, COLOR24_UNDEFINED);
                    }
                    printf("%s\n", entries[n].name);
                    setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
                }
            }
            free(entries);
        }
    }
    
    else if (strcmp(first_token, "cd") == 0) {
        if (num_tokens == 2) {
            char *dirname = strsplit_chunk_at(command, command_length, 1);
            if (strcmp(dirname, "..") == 0) {
                snprintf(command_tmp, MAXCOMMAND, "%s", getenv("PWD"));
                char* last_sep = strrchr(command_tmp, DIR_SEPARATOR);
                if (last_sep != NULL) {
                    last_sep[0] = 0;
                    if (!file_exists(command_tmp)) {
                        printf("The specified location does not exist!\n");
                    } else {
                        setenv("PWD", command_tmp, 1);
                    }
                }
            }
            else if (!file_exists(dirname)) {
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, dirname);
                if (!file_exists(command_tmp)) {
                    printf("The specified location does not exist!\n");
                } else {
                    setenv("PWD", command_tmp, 1);
                }
            }
            else {
                setenv("PWD", dirname, 1);
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "tree") == 0) {
        if (num_tokens == 2) {
            char *dirname = strsplit_chunk_at(command, command_length, 1);
            if (!file_exists(dirname)) {
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, dirname);
                dirname = command_tmp;
            }
            tree(dirname, dirname, 0);
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "date") == 0) {
        time_t currtime = time(NULL);
        printf("%s\n", ctime(&currtime));
    }
    
    else if (strcmp(first_token, "touch") == 0) {
        if (num_tokens == 2) {
            char *filename = strsplit_chunk_at(command, command_length, 1);
            if (filename[0] != 'f') {
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, filename);
                filename = command_tmp;
            }
            int error;
            if ((error = touch(filename))) {
                printf("Error touching %s: %d\n", filename, error);
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "mkdir") == 0) {
        if (num_tokens == 2) {
            char *filename = strsplit_chunk_at(command, command_length, 1);
            if (filename[0] != 'f') {
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, filename);
                filename = command_tmp;
            }
            int error;
            if ((error = mkdir(filename))) {
                printf("Error creating dir %s: %d\n", filename, error);
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "rm") == 0) {
        if (num_tokens == 2) {
            char *filename = strsplit_chunk_at(command, command_length, 1);
            if (filename[0] != 'f') {
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, filename);
                filename = command_tmp;
            }
            int error;
            if ((error = remove(filename))) {
                printf("Error removing file %s\n", filename);
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "mv") == 0) {
        if (num_tokens == 3) {
            char *filename_from = strsplit_chunk_at(command, command_length, 1);
            char *filename_to = strsplit_chunk_at(command, command_length, 2);
            int error;
            if ((error = move(filename_from, filename_to))) {
                printf("Error moving file %s -> %s: %d\n", filename_from, filename_to, error);
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "ktest") == 0) {
        KeyEvent key;
        MQStatus poll_status;
        subscribe(keycode_topic);
        do {
            poll_status = poll(keycode_topic, &key);
            if (poll_status == MQ_SUCCESS) {
                if (key.is_pressed) {
                    printf("Pressed key %d\n", key.keycode);
                    if (key.keycode == KEY_Escape) break;
                } else {
                    printf("Released key %d\n", key.keycode);
                }
            } else if (poll_status != MQ_NO_MESSAGES) {
                printf("Keycode channel error %d\n", poll_status);
                break;
            } else {
                yield();
            }
        } while (true);
        
        unsubscribe(keycode_topic);
    }
    
    else if (strcmp(first_token, "mtest") == 0) {
        bool looping = true;
        MouseEvent event;
        subscribe(DEFAULT_MOUSE_INPUT_TOPIC);
        while (looping) {
            MQStatus poll_status = poll(DEFAULT_MOUSE_INPUT_TOPIC, &event);
            if (poll_status == MQ_SUCCESS) {
                printf("Mouse event at %llu\n  Delta x:y = %d:%d\n  Buttons (l|m|r) = %d|%d|%d\n", event.timestamp, event.delta_x, event.delta_y, event.left_btn_down, event.middle_btn_down, event.right_btn_down);
                looping = !event.right_btn_down;
            }
        }
        unsubscribe(DEFAULT_MOUSE_INPUT_TOPIC);
    }
    
    else if (strcmp(first_token, "set") == 0) {
        if (num_tokens >= 2) {
            for (size_t tk=1; tk<num_tokens; tk++) {
                char *option = strsplit_chunk_at(command, command_length, tk);
                
                char *equals = strchr(option, '=');
                if (equals == NULL) goto bad_command;
                char *value = equals+1;
                char *name = option;
                equals[0] = '\0';
                printf("\"%s\" = \"%s\"\n", name, value);
                setenv(name, value, 1);
                equals[0] = '=';
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "get") == 0) {
        if (num_tokens == 2) {
            char *name = strsplit_chunk_at(command, command_length, 1);
            char *value = getenv(name);
            if (value == NULL) {
                printf("\"%s\" not set\n", name);
            } else {
                printf("%s\n", value);
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "unset") == 0) {
        if (num_tokens == 2) {
            char *name = strsplit_chunk_at(command, command_length, 1);
            unsetenv(name);
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "kill") == 0) {
        if (num_tokens == 2) {
            char *taskid_str = strsplit_chunk_at(command, command_length, 1);
            unsigned int taskid = atoi(taskid_str);
            int error = kill(taskid);
            if (error != 0) {
                printf("Error %d while killing task %d\n", error, taskid);
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "cls") == 0) {
        cls();
        setcursor(0, 0);
    }
    
    else if (strcmp(first_token, "scan_media") == 0) {
        int status = scan_media();
        if (status != 0) {
            printf("Failed to scan media\n");
        }
    }
    
    else if (strcmp(first_token, "shutdown") == 0) {
        int msg;
        publish(DEFAULT_ACPI_POWEROFF_TOPIC, &msg);
    }
    
    else if (strcmp(first_token, "tx") == 0) {
        if (num_tokens == 2) {
            char *msg_str = strsplit_chunk_at(command, command_length, 1);
            size_t msg_len = strlen(msg_str) + 1;
            
            NetPacket msg;
            msg.length = msg_len;
            memcpy(msg.data, msg_str, msg_len);
            publish("/sys/net/iface/eth0/tx", &msg);
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "send") == 0) {
        if (num_tokens == 2) {
            char *msg_str = strsplit_chunk_at(command, command_length, 1);
            size_t msg_len = strlen(msg_str) + 1;
            
            socket_t *sock = socket(SOCK_DGRAM, IPPROTO_UDP);
            if (sock == NULL) {
                printf("Error calling socket(SOCK_STREAM, IPPROTO_UDP): %d\n", errno);
            } else {
                sockaddr dest = {
                    .port = 17,
                    .addr = {.version = 4, 
                             .address = {255, 255, 255, 255}},
                };
                size_t sent = sendto(sock, msg_str, msg_len, &dest);
                yield();
                if (sock->errno != 0) {
                    printf("Sock send error %d\n", sock->errno);
                } else {
                    printf("Sent %lu bytes\n", sent);
                }
                int closerr = socket_close(sock);
                if (closerr != 0) {
                    printf("Sock close error %d\n", closerr);
                }
            }
            
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "netmon") == 0) {
        bool looping = true;
        NetPacket received;
        subscribe("/sys/net/iface/eth0/rx");
        while (looping) {
            MQStatus poll_status = poll("/sys/net/iface/eth0/rx", &received);
            if (poll_status == MQ_SUCCESS) {
                printf("received %llu B:\n", received.length);
                for (size_t i=0; i<received.length; i++) {
                    printf("0x%02x ", received.data[i]);
                }
                printf("\n");
            }
        }
        unsubscribe("/sys/net/iface/eth0/rx");
    }
    
    else if (strcmp(first_token, "chan_stats") == 0) {
        if (num_tokens == 2) {
            char *topic_name = strsplit_chunk_at(command, command_length, 1);
            ChannelStats stats;
            MQStatus stats_status = channel_stats(topic_name, &stats);
            if (stats_status == MQ_SUCCESS) {
                printf("Stats of channel %s:\n", stats.name);
                printf("    Race: %d (%s)\n", stats.phys_race_attrib, phisical_domain_class_name[stats.phys_race_attrib]);
                printf("    Class: %d (%s)\n", stats.data_class_attrib, data_domain_class_name[stats.data_class_attrib]);
                printf("    Security policy: 0x%x\n", stats.policy);
                printf("    Max queue length: %u\n", stats.max_queue_length);
                printf("    Message length: %u\n", stats.message_length);
            } else {
                printf("Error %d\n", stats_status);
            }
        } else {
            goto bad_command;
        }
    }
    
    else if (strcmp(first_token, "togglewd") == 0) {
        TermInfo ti = getterminfo();
        switch (ti.writing) {
            case WD_BOOK:
                printf("Switching text direction to WD_RABBI\n");
                setwritingdirection(WD_RABBI);
                break;
            case WD_RABBI:
                printf("Switching text direction to WD_TEGAMI\n");
                setwritingdirection(WD_TEGAMI);
                break;
            case WD_TEGAMI:
                printf("Switching text direction to WD_LEMURIA\n");
                setwritingdirection(WD_LEMURIA);
                break;
            default:
                printf("Switching text direction to WD_BOOK\n");
                setwritingdirection(WD_BOOK);
                break;
        }
    }
    
    else if (strcmp(first_token, "chsec") == 0) {
        if (num_tokens == 4) {
            char *filename = strsplit_chunk_at(command, command_length, 1);
            if (filename[0] != 'f') {
                snprintf(command_tmp, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, filename);
                filename = command_tmp;
            }
            
            ProcIdent identity = ident();
            int crace = identity.phys_race_attrib;
            int cclass = identity.data_class_attrib;

            char *crace_str = strsplit_chunk_at(command, command_length, 2);
            int parsed_crace = parse_race(crace_str);
            if (parsed_crace >= 0) crace = parsed_crace;
            else {
                printf("Cannot parse race %s\n", crace_str);
                goto bad_command;
            }
            char *cclass_str = strsplit_chunk_at(command, command_length, 3);
            int parsed_cclass = parse_class(cclass_str);
            if (parsed_cclass >= 0) cclass = parsed_cclass;
            else {
                printf("Cannot parse class %s\n", cclass_str);
                goto bad_command;
            }
            
            int error;
            if ((error = chsec(filename, crace, cclass))) {
                printf("Error changing security attributes: %d\n", error);
            }
        } else {
            goto bad_command;
        }
    }
    
    else {
        bad_command:
        printf("Command not understood.\n");
        return false;
    }
    
    return true;
}


static Array *history;
static unsigned history_rewind = 0;
static bool history_changed = false;


static bool readline_ush_custom_handle_key(CharacterLine *line, KeyEvent *key) {
    unsigned history_length = array_size(history);
    
    CharacterLine_highlight_ush_syntax(line);
    
    switch (key->keycode) {
        case KEY_Up:
            history_changed = true;
            if (history_length > 0) {
                history_rewind++;
                return true;
            }
        case KEY_Down:
            history_changed = true;
            if (history_length > 0) {
                history_rewind--;
                return true;
            }
    }
    
    return false;
}

int ush_main(void) {
    char *command = (char*) malloc(sizeof(char) * MAXCOMMAND);
    command[0] = '\0';
    
    TermInfo ti = getterminfo();
    
    printbb("Welcome to [color=#%06X]ubershell[/color] v0.2!\n", COLOR24_TEXT_HIGHLIGHT);
    
    if (getenv("PWD") == NULL) {
        setenv("PWD", DEFAULT_PWD, 1);
    }
    
    array_new(&history);

    while (true) {
        ti = getterminfo();
        history_changed = false;
        printf(PROMPT, getenv("PWD"));
        readline_editline_custom(command, &readline_ush_custom_handle_key);
        
        unsigned history_length = array_size(history);
        
        if (!history_changed) {
            printf("\n");
            bool command_valid = parse_command(command);
            bool isduplicate = false;
            if (history_length > 0) {
                char *historical_cmd;
                array_get_at(history, history_length - 1, (void*)&historical_cmd);
                isduplicate = (strcmp(command, historical_cmd) == 0);
            }
            if (!isduplicate) {
                char *hist_entry = (char*) malloc(sizeof(char) * (strlen(command) + 2));
                strcpy(hist_entry, command);
                array_add(history, hist_entry);
            }
            
            history_rewind = 0;
            command[0] = '\0';
        }
        else {
            printf("\r");
            
            if (history_rewind > history_length) history_rewind = 0;
            if (history_rewind < 0) history_rewind += history_length;
            
            if (history_rewind > 0) {
                char *historical_cmd;
                array_get_at(history, history_length - history_rewind, (void*)&historical_cmd);
                for (int b = 0; b < ti.termsize.w - 1; b++) printf(" ");
                printf("\r");
                strcpy(command, historical_cmd);
            } else {
                for (int b = 0; b < ti.termsize.w - 1; b++) printf(" ");
                printf("\r");
                command[0] = '\0';
            }
        }
    }
}

