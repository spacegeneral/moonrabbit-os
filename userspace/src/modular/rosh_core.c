#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <bbcode.h>
#include <yakumo.h>
#include <sys/proc.h>
#include <sys/fs.h>
#include <sys/term.h>
#include <sys/keycodes.h>

#include <time.h>
#include <sys/happy.h>

#include <collections/array.h>
#include <libedit/readline.h>
#include <libedit/characterline.h>
#include <libgui/libgui.h>

#include "../mjs/mjs.h"
#include "modular.h"


/*
 * Functions for memory introspection.
 * These are supposed to be FFI-ed and used from the JS environment.
 */
void *mjs_mem_to_ptr(unsigned int val);
void *mjs_mem_get_ptr(void *base, int offset);
void mjs_mem_set_ptr(void *ptr, void *val);
double mjs_mem_get_dbl(void *ptr);
void mjs_mem_set_dbl(void *ptr, double val);
double mjs_mem_get_uint(void *ptr, int size, int bigendian);
double mjs_mem_get_int(void *ptr, int size, int bigendian);
void mjs_mem_set_uint(void *ptr, unsigned int val, int size, int bigendian);
void mjs_mem_set_int(void *ptr, int val, int size, int bigendian);




#define PROMPT ">- "
#define PRELUDE "f,def,init,appdata,rosh,prelude.js"
#define MAXCOMMAND 2048
static bool debug;

typedef struct listdir_state {
    DirEnt *list_entries;
    size_t list_num_entries;
} listdir_state;


struct mjs *mjs;

#define ERRORCHECK(status) \
{ \
    if (status != MJS_OK) mjs_print_error(mjs, stdout, NULL, 1); \
}



static bool ptr_isnull(void *ptr) {
    return ptr == NULL;
}

static void* begin_listdir(char *path) {
    int error;
    DirEnt *list_entries;
    size_t list_num_entries;
    listdir_state *newstate = malloc(sizeof(listdir_state));
    if ((error = listdir(path, &list_entries, &list_num_entries))) {
        free(newstate);
        return NULL;
    }
    newstate->list_entries = list_entries;
    newstate->list_num_entries = list_num_entries;
    return newstate;
}

static char *finish_listdir(int curr_ent, void *state) {
    listdir_state *currstate = (listdir_state*) state;
    
    if (curr_ent >= currstate->list_num_entries) {
        free(currstate->list_entries);
        free(currstate);
        return "";
    }
    return currstate->list_entries[curr_ent].name;
}

static char* rosh_getenv(const char *name) {
    char* result = getenv(name);
    if (result == NULL) return "";
    return result;
}

static void* rosh_open_file(char *path, char *mode) {
    FILE *fp = fopen(path, mode);
    return (void*)fp;
}

static void rosh_close_file(void *fileptr) {
    FILE *fp = (FILE*) fileptr;
    fclose(fp);
}

static int rosh_get_filesize(void *fileptr) {
    FILE *fp = (FILE*) fileptr;
    int oldseek = ftell(fp);
    fseek(fp, 0, SEEK_END);
    int filesize = ftell(fp);
    fseek(fp, oldseek, SEEK_SET);
    
    return filesize;
}

static int rosh_fseek(int offset, int whence, void *fileptr) {
    FILE *fp = (FILE*) fileptr;
    return fseek(fp, offset, whence);
}

static int rosh_ftell(void *fileptr) {
    FILE *fp = (FILE*) fileptr;
    return ftell(fp);
}

static int rosh_fread(char *buf, int size, int nmemb, void *fileptr) {
    FILE *fp = (FILE*) fileptr;
    return (int) fread(buf, size, nmemb, fp);
}

static int rosh_fwrite(char *buf, int size, int nmemb, void *fileptr) {
    FILE *fp = (FILE*) fileptr;
    return (int) fwrite(buf, size, nmemb, fp);
}


static bool rosh_file_exists(char *name) {
    return file_exists(name);
}

static bool rosh_copy(void *fileptr_from, void *fileptr_to) {
    FILE *fp_from = (FILE*) fileptr_from;
    FILE *fp_to = (FILE*) fileptr_to;
    size_t nread, nwrite;
    char* buff = malloc(65536);
    if (buff == NULL) return false;
    
    time_t started_copy = clock_ms();
    
    while (!feof(fp_from)) {
        nread = fread(buff, 1, 65536, fp_from);
        nwrite = fwrite(buff, 1, nread, fp_to);
        
        long happy_err = (long)(clock_ms() - started_copy);
        started_copy = clock_ms();
        happy(errf_lin_pos(happy_err, 300));
        
        if (nwrite != nread) {
            free(buff);
            return false;
        }
    }
    
    free(buff);
    return true;
}


static char *rosh_mount(char *diskfile, char *filesystem) {
    static char mountpoint[MAXCOMMAND] = "undefined";
    mount(diskfile, mountpoint, filesystem);
    return mountpoint;
}

static int rosh_spork(char *filename, int phys_race_attrib, int data_class_attrib) {
    return spork(filename, phys_race_attrib, data_class_attrib, NULL);
}

static int rosh_spawn_app(char *filename, int phys_race_attrib, int data_class_attrib) {
    return spawn_app(filename, phys_race_attrib, data_class_attrib, NULL);
}

static int rosh_get_race() {
    ProcIdent identity = ident();
    return identity.phys_race_attrib;
}

static int rosh_get_class() {
    ProcIdent identity = ident();
    return identity.data_class_attrib;
}


static int rosh_include(char *filename) {
    mjs_err_t status = mjs_exec_file(mjs, filename, NULL);
    ERRORCHECK(status);
    return status;
}


static void rosh_debug_print(char *message) {
    debug_printf("%s\r\n", message);
}


static void rosh_printf(char *message) {
    printf("%s", message);
}

static int rosh_put_image(char* filename, double scale) {
    Bitmap image;
    TermInfo ti = getterminfo();
    int load_status = load_any_image(&image, filename, (float)scale);
    if (load_status != IMG_LOADER_SUCCESS) return load_status;
    image.geom.x = ti.cursor.x * max((ti.screensize.w / ti.termsize.w), 1);
    image.geom.y = ti.cursor.y * max((ti.screensize.h / ti.termsize.h), 1);
    display_bitmap(&image, NULL);
    yield();
    destroy_bitmap(&image);
    return 0;
}
static int rosh_put_image_with_size(char* filename, int width, int height) {
    Bitmap image;
    TermInfo ti = getterminfo();
    int load_status = load_any_image_with_size(&image, filename, width, height);
    if (load_status != IMG_LOADER_SUCCESS) return load_status;
    image.geom.x = ti.cursor.x * max((ti.screensize.w / ti.termsize.w), 1);
    image.geom.y = ti.cursor.y * max((ti.screensize.h / ti.termsize.h), 1);
    display_bitmap(&image, NULL);
    yield();
    destroy_bitmap(&image);
    return 0;
}




static bool rosh_bb_inside_image = false;
static int rosh_saved_color = COLOR24_UNDEFINED;

static void rosh_print_bbcode_content_function(char* tag_name, char* tag_attribute, char* tag_content) {
    if (!rosh_bb_inside_image) {
        printf("%s", tag_content);
    }
}

static void rosh_print_bbcode_open_tag_function(char* tag_name, char* tag_attribute) {
    if (strcmp(tag_name, "color") == 0) {
        TermInfo ti = getterminfo();
        rosh_saved_color = ti.foreground_color;
        setcolors(color_string_to_code(tag_attribute), COLOR24_UNDEFINED);
    }
    else if (strcmp(tag_name, "b") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_BOLD);
    }
    else if (strcmp(tag_name, "i") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_ITALICS);
    }
    else if (strcmp(tag_name, "u") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_UNDERLINE);
    }
    else if (strcmp(tag_name, "g") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_GOTHIC);
    }
    else if (strcmp(tag_name, "s") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags | TERM_FLAG_SCRIPT);
    }
    else if (strcmp(tag_name, "zoom") == 0) {
        setzoomlevel(atoi(tag_attribute));
    }
    else if (strcmp(tag_name, "tag") == 0) {
        puttag(tag_attribute);
    }
    else if (strcmp(tag_name, "image") == 0) {
        char* image_params = strdup(tag_attribute);
        size_t image_params_length = strlen(image_params);
        size_t num_tokens = strsplit(image_params, ';', image_params_length);
        
        char* img_fname;
        char tmp_retry_filename[4096];
        
        if (num_tokens >= 1) {
            img_fname = strsplit_chunk_at(image_params, image_params_length, 0);
            if (!file_exists(img_fname)) {
                snprintf(tmp_retry_filename, MAXCOMMAND, "%s%c%s", getenv("PWD"), DIR_SEPARATOR, img_fname);
                img_fname = tmp_retry_filename;
            }
        }
        if (num_tokens == 2) {
            rosh_put_image(img_fname, (double)atof(strsplit_chunk_at(image_params, image_params_length, 1)));
        } else if (num_tokens == 3) {
            debug_printf("<%s><%s><%s>\r\n", strsplit_chunk_at(image_params, image_params_length, 0), strsplit_chunk_at(image_params, image_params_length, 1), strsplit_chunk_at(image_params, image_params_length, 2));
            rosh_put_image_with_size(img_fname, atoi(strsplit_chunk_at(image_params, image_params_length, 1)), 
                                                atoi(strsplit_chunk_at(image_params, image_params_length, 2)));
        } else {
            rosh_put_image(img_fname, 1.0);
        }
        
        free(image_params);
        rosh_bb_inside_image = true;
    }
}

static void rosh_print_bbcode_close_tag_function(char* tag_name) {
    if (strcmp(tag_name, "color") == 0) {
        setcolors(rosh_saved_color, COLOR24_UNDEFINED);
    }
    else if (strcmp(tag_name, "b") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_BOLD));
    }
    else if (strcmp(tag_name, "i") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_ITALICS));
    }
    else if (strcmp(tag_name, "u") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_UNDERLINE));
    }
    else if (strcmp(tag_name, "g") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_GOTHIC));
    }
    else if (strcmp(tag_name, "s") == 0) {
        TermInfo ti = getterminfo();
        settermflags(ti.flags & ~(TERM_FLAG_SCRIPT));
    }
    else if (strcmp(tag_name, "zoom") == 0) {
        setzoomlevel(0);
    }
    else if (strcmp(tag_name, "tag") == 0) {
        poptag();
    }
    else if (strcmp(tag_name, "image") == 0) {
        rosh_bb_inside_image = false;
    }
}


static void rosh_printbb(char *message) {
    parse_bbcode(message, &rosh_print_bbcode_content_function,
                          &rosh_print_bbcode_open_tag_function,
                          &rosh_print_bbcode_close_tag_function);
}



typedef struct RoshPseudoTermInfo {
    void *termsize;
    void *screensize;
    void *cursor;
    bool cursor_enabled;
    bool support_bitmap;
    int foreground_color;
    int background_color;
    int scroll_top;
    int scroll_bottom;
    unsigned int flags;
} RoshPseudoTermInfo;

static void* rosh_terminfo() {
    static RoshPseudoTermInfo rosh_ti;
    static TermInfo real_ti;
    real_ti = getterminfo();
    rosh_ti.termsize = (void*)&(real_ti.termsize);
    rosh_ti.screensize = (void*)&(real_ti.screensize);
    rosh_ti.cursor = (void*)&(real_ti.cursor);
    rosh_ti.cursor_enabled = real_ti.cursor_enabled;
    rosh_ti.support_bitmap = real_ti.support_bitmap;
    rosh_ti.foreground_color = real_ti.foreground_color;
    rosh_ti.background_color = real_ti.background_color;
    rosh_ti.scroll_top = real_ti.scroll_top;
    rosh_ti.scroll_bottom = real_ti.scroll_bottom;
    rosh_ti.flags = real_ti.flags;
    
    return (void*)&rosh_ti;
}


typedef struct Memstats {
    unsigned int free;
    unsigned int total_available;
} Memstats;

static void* rosh_memstats() {
    static Memstats stats;

    FILE *fp = fopen("f,sys,dial,robot,memstats","rb");
    if (fp == NULL) return NULL;
    
    if (fread(&stats, sizeof(Memstats), 1, fp) != 1) {
        fclose(fp);
        return NULL;
    }
    fclose(fp);
    
    return (void*)&stats;
}

void cpuid(unsigned int index, int regs[4]) {
    asm volatile(
        "cpuid            \n\t"
        : "=a"(regs[0]), [ebx] "=b"(regs[1]), "=c"(regs[2]), "=d"(regs[3])
        : "a"(index));
}


char *rosh_cpuid_vendor() {
    static char str[12];
    int intview[4];
    
    cpuid(0, intview);
    
    int i = 0;
    int o = 0;
    for (i=4; i<8; i++) {
        str[o++] = ((char *)intview)[i];
    }
    for (i=12; i<16; i++) {
        str[o++] = ((char *)intview)[i];
    }
    for (i=8; i<12; i++) {
        str[o++] = ((char *)intview)[i];
    }
    
    str[12] = '\0';
    return str;
}

static void* rosh_localtime() {
    time_t currtime = time(NULL);
    return (void*) localtime(&currtime);
}

void rosh_user_sleep(double seconds) {
    unsigned int milliseconds = (unsigned int)(seconds * 1000.0);
    u_sleep_milli(milliseconds);
}

static struct mjs_c_struct_member *rosh_get_terminfo_descr() {
    static struct mjs_c_struct_member terminfo_struct_descr[] = {
        {"termsize", offsetof(RoshPseudoTermInfo, termsize), MJS_FFI_CTYPE_VOID_PTR},
        {"screensize", offsetof(RoshPseudoTermInfo, screensize), MJS_FFI_CTYPE_VOID_PTR},
        {"cursor", offsetof(RoshPseudoTermInfo, cursor), MJS_FFI_CTYPE_VOID_PTR},
        {"cursor_enabled", offsetof(RoshPseudoTermInfo, cursor_enabled), MJS_FFI_CTYPE_BOOL},
        {"support_bitmap", offsetof(RoshPseudoTermInfo, support_bitmap), MJS_FFI_CTYPE_BOOL},
        {"foreground_color", offsetof(RoshPseudoTermInfo, foreground_color), MJS_FFI_CTYPE_INT},
        {"background_color", offsetof(RoshPseudoTermInfo, background_color), MJS_FFI_CTYPE_INT},
        {"scroll_top", offsetof(RoshPseudoTermInfo, scroll_top), MJS_FFI_CTYPE_INT},
        {"scroll_bottom", offsetof(RoshPseudoTermInfo, scroll_bottom), MJS_FFI_CTYPE_INT},
        {"flags", offsetof(RoshPseudoTermInfo, flags), MJS_FFI_CTYPE_INT},
        {NULL, 0, MJS_FFI_CTYPE_NONE},
    };
    return terminfo_struct_descr;
};

static struct mjs_c_struct_member *rosh_get_point_descr() {
    static struct mjs_c_struct_member point_struct_descr[] = {
        {"x", offsetof(Point, x), MJS_FFI_CTYPE_INT},
        {"y", offsetof(Point, y), MJS_FFI_CTYPE_INT},
        {NULL, 0, MJS_FFI_CTYPE_NONE},
    };
    return point_struct_descr;
};

static struct mjs_c_struct_member *rosh_get_rect_descr() {
    static struct mjs_c_struct_member rect_struct_descr[] = {
        {"x", offsetof(Rect, x), MJS_FFI_CTYPE_INT},
        {"y", offsetof(Rect, y), MJS_FFI_CTYPE_INT},
        {"w", offsetof(Rect, w), MJS_FFI_CTYPE_INT},
        {"h", offsetof(Rect, h), MJS_FFI_CTYPE_INT},
        {NULL, 0, MJS_FFI_CTYPE_NONE},
    };
    return rect_struct_descr;
};

static struct mjs_c_struct_member *rosh_get_memstats_descr() {
    static struct mjs_c_struct_member memstats_struct_descr[] = {
        {"free", offsetof(Memstats, free), MJS_FFI_CTYPE_INT},
        {"total_available", offsetof(Memstats, total_available), MJS_FFI_CTYPE_INT},
        {NULL, 0, MJS_FFI_CTYPE_NONE},
    };
    return memstats_struct_descr;
};

static struct mjs_c_struct_member *rosh_get_time_tm_descr() {
    static struct mjs_c_struct_member time_tm_struct_descr[] = {
        {"tm_sec", offsetof(struct tm, tm_sec), MJS_FFI_CTYPE_INT},
        {"tm_min", offsetof(struct tm, tm_min), MJS_FFI_CTYPE_INT},
        {"tm_hour", offsetof(struct tm, tm_hour), MJS_FFI_CTYPE_INT},
        {"tm_mday", offsetof(struct tm, tm_mday), MJS_FFI_CTYPE_INT},
        {"tm_mon", offsetof(struct tm, tm_mon), MJS_FFI_CTYPE_INT},
        {"tm_year", offsetof(struct tm, tm_year), MJS_FFI_CTYPE_INT},
        {"tm_wday", offsetof(struct tm, tm_wday), MJS_FFI_CTYPE_INT},
        {"tm_yday", offsetof(struct tm, tm_yday), MJS_FFI_CTYPE_INT},
        
        {NULL, 0, MJS_FFI_CTYPE_NONE},
    };
    return time_tm_struct_descr;
};


static void *rocket_dlsym(void *handle, const char *name) {
    if (debug) printf("dlsym \"%s\"\n", name);
    if (strcmp(name, "floor") == 0) return &floor;
    if (strcmp(name, "ceil") == 0) return &ceil;
    if (strcmp(name, "round") == 0) return &round;
    if (strcmp(name, "fmax") == 0) return &fmax;
    if (strcmp(name, "fmin") == 0) return &fmin;
    if (strcmp(name, "fabs") == 0) return &fabs;
    if (strcmp(name, "sqrt") == 0) return &sqrt;
    if (strcmp(name, "exp") == 0) return &exp;
    if (strcmp(name, "log") == 0) return &log;
    if (strcmp(name, "pow") == 0) return &pow;
    if (strcmp(name, "sin") == 0) return &sin;
    if (strcmp(name, "cos") == 0) return &cos;
    if (strcmp(name, "rand") == 0) return &rand;
    
    if (strcmp(name, "mjs_mem_to_ptr") == 0) return &mjs_mem_to_ptr;
    if (strcmp(name, "mjs_mem_get_ptr") == 0) return &mjs_mem_get_ptr;
    if (strcmp(name, "mjs_mem_set_ptr") == 0) return &mjs_mem_set_ptr;
    if (strcmp(name, "mjs_mem_get_dbl") == 0) return &mjs_mem_get_dbl;
    if (strcmp(name, "mjs_mem_set_dbl") == 0) return &mjs_mem_set_dbl;
    if (strcmp(name, "mjs_mem_get_uint") == 0) return &mjs_mem_get_uint;
    if (strcmp(name, "mjs_mem_get_int") == 0) return &mjs_mem_get_int;
    if (strcmp(name, "mjs_mem_set_uint") == 0) return &mjs_mem_set_uint;
    if (strcmp(name, "mjs_mem_set_int") == 0) return &mjs_mem_set_int;
    
    if (strcmp(name, "ptr_isnull") == 0) return &ptr_isnull;
    if (strcmp(name, "file_exists") == 0) return &rosh_file_exists;
    if (strcmp(name, "search_executable") == 0) return &search_executable;
    if (strcmp(name, "search_script") == 0) return &search_script;
    if (strcmp(name, "search_buildfile") == 0) return &search_buildfile;
    if (strcmp(name, "search_cmd_any") == 0) return &search_cmd_any;
    if (strcmp(name, "setenv") == 0) return &setenv;
    if (strcmp(name, "unsetenv") == 0) return &unsetenv;
    if (strcmp(name, "rosh_getenv") == 0) return &rosh_getenv;
    if (strcmp(name, "exit") == 0) return &exit;
    if (strcmp(name, "spoon") == 0) return &spoon;
    if (strcmp(name, "kill") == 0) return &kill;
    if (strcmp(name, "rosh_spork") == 0) return &rosh_spork;
    if (strcmp(name, "rosh_spawn_app") == 0) return &rosh_spawn_app;
    if (strcmp(name, "yield") == 0) return &yield;
    if (strcmp(name, "task_switch") == 0) return &task_switch;
    if (strcmp(name, "rosh_get_race") == 0) return &rosh_get_race;
    if (strcmp(name, "rosh_get_class") == 0) return &rosh_get_class;
    if (strcmp(name, "begin_listdir") == 0) return &begin_listdir;
    if (strcmp(name, "finish_listdir") == 0) return &finish_listdir;
    if (strcmp(name, "rosh_open_file") == 0) return &rosh_open_file;
    if (strcmp(name, "rosh_close_file") == 0) return &rosh_close_file;
    if (strcmp(name, "rosh_get_filesize") == 0) return &rosh_get_filesize;
    if (strcmp(name, "rosh_fread") == 0) return &rosh_fread;
    if (strcmp(name, "rosh_fwrite") == 0) return &rosh_fwrite;
    if (strcmp(name, "rosh_fseek") == 0) return &rosh_fseek;
    if (strcmp(name, "rosh_ftell") == 0) return &rosh_ftell;
    if (strcmp(name, "touch") == 0) return &touch;
    if (strcmp(name, "mkdir") == 0) return &mkdir;
    if (strcmp(name, "link") == 0) return &link;
    if (strcmp(name, "remove") == 0) return &remove;
    if (strcmp(name, "rosh_copy") == 0) return &rosh_copy;
    if (strcmp(name, "move") == 0) return &move;
    if (strcmp(name, "scan_media") == 0) return &scan_media;
    if (strcmp(name, "rosh_mount") == 0) return &rosh_mount;
    if (strcmp(name, "unmount") == 0) return &unmount;
    if (strcmp(name, "rosh_include") == 0) return &rosh_include;
    if (strcmp(name, "rosh_get_terminfo_descr") == 0) return &rosh_get_terminfo_descr;
    if (strcmp(name, "rosh_get_point_descr") == 0) return &rosh_get_point_descr;
    if (strcmp(name, "rosh_get_rect_descr") == 0) return &rosh_get_rect_descr;
    if (strcmp(name, "rosh_get_memstats_descr") == 0) return &rosh_get_memstats_descr;
    if (strcmp(name, "rosh_terminfo") == 0) return &rosh_terminfo;
    if (strcmp(name, "rosh_memstats") == 0) return &rosh_memstats;
    if (strcmp(name, "rosh_cpuid_vendor") == 0) return &rosh_cpuid_vendor;
    if (strcmp(name, "rosh_debug_print") == 0) return &rosh_debug_print;
    if (strcmp(name, "rosh_printf") == 0) return &rosh_printf;
    if (strcmp(name, "rosh_printbb") == 0) return &rosh_printbb;
    if (strcmp(name, "rosh_put_image") == 0) return &rosh_put_image;
    if (strcmp(name, "rosh_put_image_with_size") == 0) return &rosh_put_image_with_size;
    if (strcmp(name, "rosh_get_time_tm_descr") == 0) return &rosh_get_time_tm_descr;
    if (strcmp(name, "rosh_localtime") == 0) return &rosh_localtime;
    if (strcmp(name, "setcursor") == 0) return &setcursor;
    if (strcmp(name, "getkcode") == 0) return &getkcode;
    if (strcmp(name, "rosh_user_sleep") == 0) return &rosh_user_sleep;
    if (strcmp(name, "cls") == 0) return &cls;
    if (strcmp(name, "chsec") == 0) return &chsec;
    
    return NULL;
}




static Array *history;
static unsigned history_rewind = 0;
static bool history_changed = false;


static bool readline_rosh_custom_handle_key(CharacterLine *line, KeyEvent *key) {
    unsigned history_length = array_size(history);
    
    CharacterLine_highlight_mjs_syntax(line);
    
    switch (key->keycode) {
        case KEY_Up:
            history_changed = true;
            if (history_length > 0) {
                history_rewind++;
                return true;
            }
        case KEY_Down:
            history_changed = true;
            if (history_length > 0) {
                history_rewind--;
                return true;
            }
    }
    
    return false;
}


static int rep_loop(struct mjs *mjs) {
    TermInfo ti = getterminfo();
    array_new(&history);
    char *command = (char*) malloc(sizeof(char) * MAXCOMMAND);
    command[0] = '\0';
    mjs_err_t status = 0;
    
    while (true) {
        ti = getterminfo();
        history_changed = false;
        printf("%s", PROMPT);
        readline_editline_custom(command, &readline_rosh_custom_handle_key);
        
        if (strcmp(command, "exit") == 0) {break;}
        
        unsigned history_length = array_size(history);
        
        if (!history_changed) {
            printf("\n");
            status = mjs_exec(mjs, command, NULL);
            ERRORCHECK(status);
            bool isduplicate = false;
            if (history_length > 0) {
                char *historical_cmd;
                array_get_at(history, history_length - 1, (void*)&historical_cmd);
                isduplicate = (strcmp(command, historical_cmd) == 0);
            }
            if (!isduplicate) {
                char *hist_entry = (char*) malloc(sizeof(char) * (strlen(command) + 1));
                strcpy(hist_entry, command);
                array_add(history, hist_entry);
            }
            
            history_rewind = 0;
            command[0] = '\0';
        }
        else {
            printf("\r");
            
            if (history_rewind > history_length) history_rewind = 0;
            if (history_rewind < 0) history_rewind += history_length;
            
            if (history_rewind > 0) {
                char *historical_cmd;
                array_get_at(history, history_length - history_rewind, (void*)&historical_cmd);
                for (int b = 0; b < ti.termsize.w - 1; b++) printf(" ");
                printf("\r");
                strcpy(command, historical_cmd);
            } else {
                for (int b = 0; b < ti.termsize.w - 1; b++) printf(" ");
                printf("\r");
                command[0] = '\0';
            }
        }
    }
    array_destroy(history);
    free(command);
    printf("\n");
    return status;
}


void resume_handler() {
    expirecc(&resume_handler, 15);
    continuation();
}


int rosh_main(void) {
    srand(time(NULL));
    
    debug = (getenv("debug") != NULL);
    
    if (getenv("nopreempt") == NULL) {
        expirecc(&resume_handler, 20);
    }
    
    mjs = mjs_create();
    mjs_set_ffi_resolver(mjs, &rocket_dlsym);
    
    mjs_err_t status = mjs_exec_file(mjs, PRELUDE, NULL);
    ERRORCHECK(status);
    
    char *file_param = getenv("script");
    if (file_param != NULL) {  // non-interactive
        mjs_err_t status = mjs_exec_file(mjs, file_param, NULL);
        ERRORCHECK(status);
        return (int)status;
    } else {  // interactive
        printf("Welcome to rocketshell v0.1!\n");
        return rep_loop(mjs);
    }
    
    return 0;
}
 
