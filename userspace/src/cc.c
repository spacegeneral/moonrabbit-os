#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/proc.h>
#include <sys/fs.h>
#include <libtcc.h>
#include <sys/happy.h>

#include "inih/ini.h"


char cached_lib_base[] = "f,def,cache,compile";
char cached_include_base[] = "f,def,cache,compile,include";
char cached_source_base[] = "f,def,cache,compile,src";
char uncached_lib_base[] = "f,def,home,compile";
char uncached_include_base[] = "f,def,home,compile,include";
char uncached_source_base[] = "f,def,home,compile,src";


long long expiration_quantum = 20;


char compile_symbols[2][6][12] = {
    {"@", "I", "H", "C", "A", "*"}, 
    {"🚂", "🏃", "🍟", "⚛", "🖇", "🚀"}
};
int symbol_selector_1;

enum symbol_selector_2 {
    SYMBOL_START,
    SYMBOL_INIT,
    SYMBOL_INCLUDE,
    SYMBOL_COMPILE,
    SYMBOL_LINK,
    SYMBOL_LAUNCH
};


char *lib_base;
char *include_base;
char *source_base;

bool quiet = false;

TermInfo initial_ti;


typedef struct JITFile {
    char *name;
    char *description;
    char *author;
    char *compile_units;
    char *extra_includes;
    char *extra_libs;
    char *flags;
} JITFile;

JITFile buildconfig = {"", "", "", "", "", "", ""};


static int config_handler(void* user, const char* section, const char* name, const char* value) {
    JITFile* config = (JITFile*)user;

    #define MATCH(sname, nname) (strcmp(section, sname) == 0) && (strcmp(name, nname) == 0)
    if (MATCH("manifest", "name")) {
        config->name = strdup(value);
    } else if (MATCH("manifest", "description")) {
        config->description = strdup(value);
    } else if (MATCH("manifest", "author")) {
        config->author = strdup(value);
    } else if (MATCH("compile", "units")) {
        config->compile_units = strdup(value);
    } else if (MATCH("compile", "extra_include_paths")) {
        config->extra_includes = strdup(value);
    } else if (MATCH("compile", "extra_libs")) {
        config->extra_libs = strdup(value);
    } else if (MATCH("compile", "flags")) {
        config->flags = strdup(value);
    } 
    else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}


int add_source_file(TCCState *s, const char *basename) {
    char fullname[4096];
    snprintf(fullname, 4096, "%s,%s", source_base, basename);
    return tcc_add_file(s, fullname);
}

int add_lib_file(TCCState *s, const char *basename) {
    char fullname[4096];
    snprintf(fullname, 4096, "%s,lib,%s", lib_base, basename);
    return tcc_add_file(s, fullname);
}


/*void set_defines(TCCState *s, char *tokenized_define_string, size_t tokenized_define_string_length, size_t num_tokens) {
    for (size_t d=0; d<num_tokens; d++) {
        char *curr_define = strsplit_chunk_at(tokenized_define_string, tokenized_define_string_length, d);
        char *equals = strchr(curr_define, '=');
        if (equals == NULL) {
            tcc_define_symbol(s, curr_define, "1");
        } else { \
            char *value = equals+1;
            char *name = curr_define;
            equals[0] = '\0';
            tcc_define_symbol(s, name, value);
            equals[0] = '=';
        }
    }
}*/


void setup_paths(TCCState* s) {
    if (getenv("CC_CACHEONLY")) {
        lib_base = cached_lib_base;
        tcc_set_lib_path(s, cached_lib_base);
        include_base = cached_include_base;
        tcc_add_sysinclude_path(s, cached_include_base);
        source_base = cached_source_base;
        tcc_add_file(s, "f,def,cache,compile,crt,crt0.o");
    }
    else {
        if (!file_exists(cached_lib_base)) {
            lib_base = uncached_lib_base;
            tcc_set_lib_path(s, uncached_lib_base);
        } else {
            lib_base = cached_lib_base;
            tcc_set_lib_path(s, cached_lib_base);
        }
        
        if (!file_exists(cached_include_base)) {
            include_base = uncached_include_base;
            tcc_add_sysinclude_path(s, uncached_include_base);
        } else {
            include_base = cached_include_base;
            tcc_add_sysinclude_path(s, cached_include_base);
        }
        
        if (!file_exists(cached_source_base)) {
            source_base = uncached_source_base;
        } else {
            source_base = cached_source_base;
        }
        
        if (file_exists("f,def,cache,compile,crt,crt0.o")) tcc_add_file(s, "f,def,cache,compile,crt,crt0.o");
        else tcc_add_file(s, "f,def,home,compile,crt,crt0.o");
    }
}


void return_to_shell(int code) {
    setwritingdirection(initial_ti.writing);
    exit(code);
}


#define LOCKFILE_NAME "f,def,cache,cc.lock"
void fs_lock() {
    while (file_exists(LOCKFILE_NAME)) {
        yield();
    }
    FILE* fp = fopen(LOCKFILE_NAME, "w");
    if (fp != NULL) fclose(fp);
}

void fs_unlock() {
    remove(LOCKFILE_NAME);
}


void compile_and_run() {
    TCCState *s;
    s = tcc_new();
    if (!s) {
        setwritingdirection(initial_ti.writing);
        printf("\r\nCould not create tcc state\n");
        return_to_shell(3);
    }
    debug_printf("CC init TCCState\r\n");
    
    char *flags = malloc(4096);
    snprintf(flags, 4096, "%s %s", "-static -g", buildconfig.flags);
    tcc_set_options(s, flags);
    tcc_set_output_type(s, TCC_OUTPUT_MEMORY);
    
    setup_paths(s);
    
    debug_printf("CC setup_paths\r\n");
    
    if (!quiet) {
        setwritingdirection(WD_RABBI);
        printf("%s ", compile_symbols[symbol_selector_1][SYMBOL_INCLUDE]);
        setwritingdirection(WD_BOOK);
    }
    
    size_t includestr_length = strlen(buildconfig.extra_includes);
    if (includestr_length > 0) {
        size_t num_includes = strsplit(buildconfig.extra_includes, ' ', includestr_length);
        for (size_t i=0; i<num_includes; i++) {
            char *path_to_add = strsplit_chunk_at(buildconfig.extra_includes, includestr_length, i);
            if (tcc_add_include_path(s, path_to_add) < 0) {
                setwritingdirection(initial_ti.writing);
                printf("\r\nFailed to add include path %s\n", path_to_add);
                return_to_shell(4);
            }
        }
    }
    
    debug_printf("CC tcc_add_include_path\r\n");
    
    if (!quiet) {
        setwritingdirection(WD_RABBI);
        printf("%s ", compile_symbols[symbol_selector_1][SYMBOL_COMPILE]);
        setwritingdirection(WD_BOOK);
    }
    
    happy(0.4);
    
    size_t sourcestr_length = strlen(buildconfig.compile_units);
    if (sourcestr_length > 0) {
        size_t num_sources = strsplit(buildconfig.compile_units, ' ', sourcestr_length);
        for (size_t u=0; u<num_sources; u++) {
            char *source_to_add = strsplit_chunk_at(buildconfig.compile_units, sourcestr_length, u);
            if (add_source_file(s, source_to_add) < 0) {
                setwritingdirection(initial_ti.writing);
                printf("\r\nFailed to add source file %s\n", source_to_add);
                return_to_shell(5);
            }
        }
    } else {
        setwritingdirection(initial_ti.writing);
        printf("\r\nThe build file does not specify any source!\n");
        return_to_shell(6);
    }
    
    happy(0.6);
    
    debug_printf("CC add_source_file\r\n");
    
    if (!quiet) {
        setwritingdirection(WD_RABBI);
        printf("%s ", compile_symbols[symbol_selector_1][SYMBOL_LINK]);
        setwritingdirection(WD_BOOK);
    }
    
    size_t libstr_length = strlen(buildconfig.extra_libs);
    if (libstr_length > 0) {
        size_t num_libs = strsplit(buildconfig.extra_libs, ' ', libstr_length);
        for (size_t l=0; l<num_libs; l++) {
            char *lib_to_add = strsplit_chunk_at(buildconfig.extra_libs, libstr_length, l);
            if (add_lib_file(s, lib_to_add) < 0) {
                setwritingdirection(initial_ti.writing);
                printf("\r\nFailed to add library %s\n", lib_to_add);
                return_to_shell(7);
            }
        }
    }
    
    happy(0.8);
    
    debug_printf("CC add_lib_file\r\n");
        
    int memsize = tcc_relocate(s, NULL);
    debug_printf("CC get tcc_relocate memsize\r\n");
    void *memarea = malloc_coarse(memsize);
    if (memarea == NULL) {
        setwritingdirection(initial_ti.writing);
        printf("\r\nCould not allocate %d bytes\n", memsize);
        return_to_shell(8);
    }
    
    // relocate the code
    if (tcc_relocate(s, memarea) < 0) {
        setwritingdirection(initial_ti.writing);
        printf("\r\nFailed to relocate code\n");
        return_to_shell(7);
    }
    
    debug_printf("CC tcc_relocate\r\n");

    // get entry symbol
    int (*func)(void);
    func = tcc_get_symbol(s, "_start");
    
    TaskDebuginfo *info;
    tcc_extract_debuginfo(s, &info);
    
    if (!quiet) {
        setwritingdirection(WD_RABBI);
        printf("%s ", compile_symbols[symbol_selector_1][SYMBOL_LAUNCH]);
    
        setwritingdirection(initial_ti.writing);
        printf("\r\n");
    }
    
    tcc_delete(s);
    free(flags);
    
    //uintptr_t textaddr = text_header.sh_offset + (uintptr_t) memarea;
    debug_printf("CC Loaded %s @%p [%lu B]; entrypoint @ %p; (%lu symbols)\r\n", buildconfig.name, memarea, memsize, func, info->num_stab_symbols);
    
    //fs_unlock();
    
    happy(0.0);
    
    henshin(memarea, (uintptr_t)func, info, buildconfig.name);
    
    // unreachable if everything goes ok
    
    setwritingdirection(initial_ti.writing);
    printf("\r\nThere was an unexpected error (return from henshin).\n");
    
    
}


char* load_file(char* fname) {
    FILE* fp;
    fp = fopen(fname, "r");
    
    debug_printf("CC fopen(%s)\r\n", fname);

    if (fp != NULL) {
        fseek(fp, 0, SEEK_END);
        long filesize = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        
        debug_printf("CC filesize=%d\r\n", filesize);
        
        char *full_text = (char*) malloc(sizeof(char) * (filesize + 1));
        debug_printf("CC full_text=%p\r\n", full_text);
        if (full_text == NULL) {
            fclose(fp);
            return NULL;
        }
        size_t nread = fread(full_text, 1, filesize, fp);
        debug_printf("CC nread=%lu\r\n", nread);
        full_text[filesize] = '\0';
        fclose(fp);
        
        if (nread != filesize) return NULL;
        
        return full_text;
    }
    
    return NULL;
}




void resume_handler(void) {
    expirecc(&resume_handler, (unsigned long long)expiration_quantum);
    continuation();
}


int main(void) {
    TCCState *s;
    initial_ti = getterminfo();
    symbol_selector_1 = (initial_ti.support_bitmap ? 1 : 0);
    
    debug_printf("CC started\r\n");
    
    char *fname = getenv("build");
    if (fname == NULL) {
        printf("Please specify build=build_file\n");
        return_to_shell(1);
    }
    
    debug_printf("CC getenv(build)\r\n");
    
    if (getenv("quiet") != NULL) {
        quiet = true;
        unsetenv("quiet");
    }
    
    debug_printf("CC getenv(quiet)\r\n");
    
    if (getenv("quantum") != NULL) {
        expiration_quantum = atoll(getenv("quantum"));
        if (expiration_quantum <= 0) expiration_quantum = 1;
    }
    debug_printf("CC getenv(quantum)\r\n");
    expirecc(&resume_handler, (unsigned long long)expiration_quantum);
    debug_printf("CC expirecc\r\n");
    
    if (!quiet) {
        setwritingdirection(WD_RABBI);
        printf("\r");
        printf("%s ", compile_symbols[symbol_selector_1][SYMBOL_START]);
        setwritingdirection(WD_BOOK);
    }
    
    // for some reason, running multiple instances of cc at once causes weird crashes.
    // TODO investigate this
    //fs_lock();
    
    happy(0.1);
    
    debug_printf("CC loading build file...\r\n");
    
    char* ini_string = load_file(fname);
    if (ini_string == NULL) {
        setwritingdirection(initial_ti.writing);
        printf("\r\nCould not open build file %s\n", fname);
        return_to_shell(2);
    }
    
    happy(0.2);
    
    debug_printf("CC loaded build file\r\n");
    
    if (ini_parse_string(ini_string, &config_handler, &buildconfig) < 0) {
        setwritingdirection(initial_ti.writing);
        printf("\r\nCould not parse build file %s\n", fname);
        return_to_shell(2);
    }
    
    free(ini_string);
    
    debug_printf("CC parsed build file\r\n");
    
    if (!quiet) {
        setwritingdirection(WD_RABBI);
        printf("%s ", compile_symbols[symbol_selector_1][SYMBOL_INIT]);
        setwritingdirection(WD_BOOK);
    }
    
    compile_and_run();
    
    return 0;
}
 
