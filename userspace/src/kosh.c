#include "modular/modular.h"
#include <stdlib.h>

int main(void) {
    if ((getenv("js") != NULL) || (getenv("script") != NULL)) {
        return rosh_main();
    }
    else {
        return ush_main();
    }
}

