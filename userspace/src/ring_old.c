#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <sys/proc.h>
#include <sys/mq.h>


#define SUBCMDLEN 1024
#define EMPTY_GEM_SLOT (-1)
static char default_ring[] = "f,def,init,appdata,init,init.ring";
#define BOOT_TIME_SHELL "f,def,init,app,kosh.elf"
#define BOOT_TIME_SHELL_SCRIPT "f,def,init,appdata,init,boot_time.js"

static bool debug;
static bool sauron;
ProcIdent identity;

int discarded_return_code;


void setup_capture_channel() {
    MQStatus status = advertise_channel_advanced(DEFAULT_TASK_ADOPTION_TOPIC, 1024, sizeof(int), Human, Citizen, SecFullyEnforced);
    if (status != MQ_ALREADY_EXISTS && status != MQ_SUCCESS) {
        printf("Warning: cannot create topic %s\n", DEFAULT_TASK_ADOPTION_TOPIC);
    }
    MQStatus status2 = subscribe(DEFAULT_TASK_ADOPTION_TOPIC);
    if (status2 != MQ_SUCCESS) {
        printf("Warning: cannot subscribe to topic %s\n", DEFAULT_TASK_ADOPTION_TOPIC);
    }
}


void hot_task_insertion(int **ring_task_ids, size_t *num_entries, int task_id_to_insert) {
    for (size_t entry_id=0; entry_id<*num_entries; entry_id++) {
        if ((*ring_task_ids)[entry_id] == EMPTY_GEM_SLOT) {
            (*ring_task_ids)[entry_id] = task_id_to_insert;
            return;
        }
    }
    
    *ring_task_ids = (int*) realloc(*ring_task_ids, sizeof(int) * ((*num_entries) + 1));
    (*ring_task_ids)[*num_entries] = task_id_to_insert;
    (*num_entries)++;
}


void boot_time_shell(int** ring_task_ids, size_t* num_gems) {
    setenv("script", BOOT_TIME_SHELL_SCRIPT, 1);
    int boot_task_id;
    int status = prelaunch(BOOT_TIME_SHELL, 
                           identity.phys_race_attrib, identity.data_class_attrib,
                           &boot_task_id, 
                           &discarded_return_code);
    if (status != 0) {
        printf("Warning: ring could not launch boot time shell \"%s\"\n", BOOT_TIME_SHELL);
    } else {
        hot_task_insertion(ring_task_ids, num_gems, boot_task_id);
    }
}


int main(void) {
    char* fname;
    char* file_param = getenv("file");
    char subcommand[SUBCMDLEN];
    sauron = false;
    debug = false;
       
    if (file_param == NULL) {  // no params: run in init mode
        fname = default_ring;
        sauron = true;
    } else {
        fname = file_param;
    }
    
    
    
    
    /*FILE* fph = fopen("f,def,init,hobbit.txt", "r");
    fseek(fph, 0, SEEK_END);
    long filesizeh = ftell(fph);
    fseek(fph, 0, SEEK_SET);
    char *full_texth = (char*) malloc(sizeof(char) * (filesizeh + 1));
    fread(full_texth, 1, filesizeh, fph);
    fclose(fph);
    
    debug_printf("%s\r\n", full_texth);*/
    
    
    
    FILE* fp = fopen(fname, "r");
    if (fp == NULL) {
        printf("Error: cannot open ringfile %s\n", fname);
        return 1;
    }
    unsetenv("file");
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    char *full_text = (char*) malloc(sizeof(char) * (filesize + 1));
    fread(full_text, 1, filesize, fp);
    full_text[filesize] = '\0';
    fclose(fp);
    
    identity = ident();  // needed later
    
    // split the ringfile text into lines
    size_t num_entries = (strsplit(full_text, '\n', filesize)-1);
    int* ring_task_ids = (int*) malloc(sizeof(int) * num_entries);
    
    size_t num_gems = 0;
    
    // each line defines a program, try to prelaunch it
    for (size_t entry_id=0; entry_id<num_entries; entry_id++) {
        char *gem_name = strsplit_chunk_at(full_text, filesize, entry_id);
        rstrip(gem_name);
        if (strlen(gem_name) == 0) continue;
        if (debug) printf("gem %d: %s\n", num_gems, gem_name);
        int status = prelaunch(gem_name, 
                               identity.phys_race_attrib, identity.data_class_attrib,
                               ring_task_ids + num_gems, 
                               &discarded_return_code);
        if (status != 0) {
            printf("Warning: ring could not launch gem %s (error: %d)\n", gem_name, status);
        } else {
            num_gems++;
        }
    }
    
    free(full_text);
    
    if (num_gems == 0) {
        printf("Error: no gems on ring!\n");
        return 2;
    }
    
    if (sauron) {
        setup_capture_channel();
        boot_time_shell(&ring_task_ids, &num_gems);
    }
    
    // The actual ring part
    size_t gems_alive = num_gems;
    while (gems_alive > 0) {
        for (size_t gemid=0; gemid<num_gems; gemid++) {
            int current_taskid = ring_task_ids[gemid];
            if (current_taskid == EMPTY_GEM_SLOT) continue;
            
            int swapstatus = task_switch(current_taskid); // control switched to child task
            
            // child task yielded
            
            if (swapstatus != 0) {
                gems_alive--;
                ring_task_ids[gemid] = EMPTY_GEM_SLOT;
            }
        }
        if (sauron) {
            // before switching, see if there is a task to adopt
            int adopted_task_id;
            MQStatus poll_status = poll(DEFAULT_TASK_ADOPTION_TOPIC, &adopted_task_id);
            if (poll_status == MQ_SUCCESS) {
                if (adopt(adopted_task_id) == 0) {
                    hot_task_insertion(&ring_task_ids, &num_gems, adopted_task_id);
                    gems_alive++;
                }
            }
        }
        if (!sauron) yield();
    }

    free(ring_task_ids);
    return 0;
}
 
