#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <sys/audio.h>
#include <sys/mq.h>
#include <sys/keycodes.h>

#include "midi/midiparser.h"

#define MIDI_NOTE_TUNING_A 440.0
//#define MIDI_NOTE_TUNING_A 1400
#define MIDI_FREQUENCY_TO_NOTE(freq) (69 + 12*log2(freq) - 12*log(MIDI_NOTE_TUNING_A))


#define MIN_USERSPACE_SLEEP 50


unsigned midi_note_to_frequency(unsigned note) {
    double freq = ((MIDI_NOTE_TUNING_A / 32.0) * pow(2.0, ((((double)note) - 9.0) / 12.0)));
    return (unsigned) freq;
}


unsigned long long int started_playing;



#define NOTE_WAIT_MAX 100000
#define NUM_CHANNELS_USED 16
#define IGNORED_CHANNEL 9
typedef struct MidiChannel {
    unsigned id;
    size_t size;
    double mean;
    double variance;
} MidiChannel;
static MidiChannel channels[NUM_MIDI_CHANNELS];
unsigned channel_mask = 0;
static int cmpchannel(const void *p1, const void *p2) {
    MidiChannel *a = (MidiChannel*) p1;
    MidiChannel *b = (MidiChannel*) p2;
    
    if (a->variance > b->variance) return -1;
    else if (a->variance == b->variance) return 0;
    return 1;
}


#define NOTE_ARRAY_BUMP_FACTOR 256
typedef struct MidiNote {
    unsigned note;
    unsigned channel;
    bool is_on;
    unsigned long long int timestamp;
} MidiNote;
static MidiNote *notes;
size_t num_notes;
size_t max_notes;
static int cmpnotes(const void *p1, const void *p2) {
    MidiNote *a = (MidiNote*) p1;
    MidiNote *b = (MidiNote*) p2;
    
    unsigned long long int causality_a = (a->is_on ? 1 : 0);
    unsigned long long int causality_b = (b->is_on ? 1 : 0);
    
    if (a->timestamp+causality_a > b->timestamp+causality_b) return 1;
    else if (a->timestamp+causality_a == b->timestamp+causality_b) return 0;
    return -1;
}


#define INVALID_NOTE 999
unsigned active_notes_by_channel[NUM_MIDI_CHANNELS];


void text_event_handler(const char* text, unsigned long long int timestamp) {
    printf("%s\n", text);
}


static void beep_toggle(unsigned frequency, bool toggle) {
    AudioEventMessage msg = {
        .frequency = (toggle ? frequency : 0),
        .flags = (toggle ? AUDIO_EVENT_MSG_FLAG_NOTE_ON : 0),
    };
    publish(DEFAULT_AUDIO_BEEPER_TOPIC, &msg);
}


void time_align_to(unsigned long long int timestamp) {
    clock_t sys_timestamp = clock() - started_playing;
    unsigned long long int delta = timestamp - sys_timestamp;
    if (delta < NOTE_WAIT_MAX && delta > 0) {
        if (delta >= MIN_USERSPACE_SLEEP) {
            unsigned long long int userspace_part = (delta / MIN_USERSPACE_SLEEP) * MIN_USERSPACE_SLEEP;
            unsigned long long int userspace_sleep_start = clock();
            u_sleep_milli(delta);
            unsigned long long int actual_userspace_sleeptime = clock() - userspace_sleep_start;
            if (actual_userspace_sleeptime >= delta) return;
            unsigned long long int time_left = delta - actual_userspace_sleeptime;
            k_sleep_milli(time_left);
        } else {
            k_sleep_milli(delta);
        }
    }
}


void catch_escape() {
    KeyEvent key;
    if (poll(keycode_topic, &key) == MQ_SUCCESS) {
        if (key.keycode == KEY_Escape) {
            printf("Stopping...");
            beep_toggle(0, false);
            exit(0);
        }
    }
}




void activate_note(MidiNote note) {
    if (active_notes_by_channel[note.channel] != INVALID_NOTE) {
        /*printf("Error: channel %u already in use (%u), cannot play note %u\n", note.channel, active_notes_by_channel[note.channel], note.note);
        beep_toggle(0, false);
        exit(3);*/
        return;
    }
    active_notes_by_channel[note.channel] = note.note;
}

void deactivate_note(MidiNote note) {
    if (active_notes_by_channel[note.channel] != note.note) {
        /*printf("Error: channel %u playing note %u, but midi tries to stop note %u\n", note.channel, active_notes_by_channel[note.channel], note.note);
        beep_toggle(0, false);
        exit(4);*/
        return;
    }
    active_notes_by_channel[note.channel] = INVALID_NOTE;
}

void play_current_note(MidiChannel *priority_list) {
    unsigned priority_note = 0;
    bool found = false;
    for (size_t c = 0; c<NUM_MIDI_CHANNELS; c++) {
        unsigned chan_id = priority_list[c].id;
        if (active_notes_by_channel[chan_id] == INVALID_NOTE) continue;
        if (active_notes_by_channel[chan_id] > priority_note) {
            priority_note = active_notes_by_channel[chan_id];
            found = true;
        }
    }
    if (found) {
        //debug_printf("Playing note %u, tone %u\r\n", priority_note, midi_note_to_frequency(priority_note));
        beep_toggle(midi_note_to_frequency(priority_note), true);
    } else {
        beep_toggle(0, false);
    }
}




void note_event_on(unsigned note, unsigned track, unsigned channel, unsigned velocity, unsigned long long int timestamp) {
    if (((1 << channel) & channel_mask) == 0) return;
    
    if (num_notes+1 > max_notes) {
        max_notes += NOTE_ARRAY_BUMP_FACTOR;
        notes = realloc(notes, sizeof(MidiNote) * max_notes);
    }
    notes[num_notes].note = note;
    notes[num_notes].channel = channel;
    notes[num_notes].is_on = true;
    notes[num_notes].timestamp = timestamp;
    num_notes++;
    
    //debug_printf("++ Note on %u  track %u  channel %u  velocity %u  timestamp %llu\r\n", note, track, channel, velocity, timestamp);
}

void note_event_on_first_pass(unsigned note, unsigned track, unsigned channel, unsigned velocity, unsigned long long int timestamp) {
    if (channel == IGNORED_CHANNEL) return;
    channels[channel].size++;
    channels[channel].mean += (double) note;
}

void note_event_off(unsigned note, unsigned track, unsigned channel, unsigned velocity, unsigned long long int timestamp) {
    if (((1 << channel) & channel_mask) == 0) return;
    
    if (num_notes+1 > max_notes) {
        max_notes += NOTE_ARRAY_BUMP_FACTOR;
        notes = realloc(notes, sizeof(MidiNote) * max_notes);
    }
    notes[num_notes].note = note;
    notes[num_notes].channel = channel;
    notes[num_notes].is_on = false;
    notes[num_notes].timestamp = timestamp;
    num_notes++;
    
    //debug_printf("-- Note off %u  track %u  channel %u  velocity %u  timestamp %llu\r\n", note, track, channel, velocity, timestamp);
}


MidiParser first_pass(unsigned char *full_mid, long filesize) {
    MidiParser parser;
    int parse_err;
    if ((parse_err = MidiParser_init(&parser))) {
        printf("Failed to init midi parser (%d)\n", parse_err);
        exit(parse_err);
    }
    
    parser.note_on_callback = &note_event_on_first_pass;
    
    if ((parse_err = MidiParser_parse(&parser, full_mid, (size_t) filesize))) {
        printf("Failed to parse file: error %d at byte %lu\n", parse_err, parser.current_byte);
        exit(parse_err);
    }
    
    return parser;
}


void second_pass(unsigned char *full_mid, long filesize) {
    MidiParser parser;
    int parse_err;
    if ((parse_err = MidiParser_init(&parser))) {
        printf("Failed to init midi parser (%d)\n", parse_err);
        exit(parse_err);
    }
    
    parser.text_callback = &text_event_handler;
    parser.note_on_callback = &note_event_on;
    parser.note_off_callback = &note_event_off;
    
    started_playing = clock();
    if ((parse_err = MidiParser_parse(&parser, full_mid, (size_t) filesize))) {
        printf("Failed to parse file: error %d at byte %lu\n", parse_err, parser.current_byte);
        exit(parse_err);
    }
}



void compute_channel_variance(unsigned channel_num) {
    channels[channel_num].variance = 0.0;
    double mean = channels[channel_num].mean;
    double size = (double)channels[channel_num].size;
    
    for (size_t nt=0; nt<num_notes; nt++) {
        if (notes[nt].channel != channel_num) continue;
        double note = (double) notes[nt].note;
        channels[channel_num].variance += ((note - mean) * (note - mean)) / (size - 1);
    }
}

void finalize_channel_stats() {
    for (size_t c=0; c<NUM_MIDI_CHANNELS; c++) {
        if (channels[c].size == 0) continue;
        channels[c].mean = channels[c].mean / ((double) channels[c].size);
        compute_channel_variance(c);
    }
}


void print_help() {
    printf("Play a given midi file using the pc's internal beeper.\n");
    printf("The file is analyzed before it's played, in order to insure\n");
    printf("a good rendition on the single-voice pc speaker.\n");
    printf("Parameters:\n");
    printf("    file        path to the midi file to play.\n");
    
}


int main(void) {
    if (getenv("help") != NULL) {
        print_help();
        return 0;
    }
    
    char *fname = getenv("file");
    if (fname == NULL) {
        printf("Please specify file=input.mid\n");
        return 1;
    }
    
    FILE *fp = fopen(fname, "r");
    if (fp == NULL) {
        printf("Error: cannot open %s\n", fname);
        return 2;
    }
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    unsigned char *full_mid = (unsigned char*) malloc(sizeof(unsigned char) * filesize);
    fread(full_mid, 1, filesize, fp);
    fclose(fp);
    
    for (size_t c=0; c<NUM_MIDI_CHANNELS; c++) {
        channels[c].id = c;
        channels[c].size = 0;
        channels[c].mean = 0.0;
        channels[c].variance = 0.0;
    }
    
    max_notes = NOTE_ARRAY_BUMP_FACTOR;
    notes = malloc(sizeof(MidiNote) * max_notes);
    num_notes = 0;
    
    for (size_t c=0; c<NUM_MIDI_CHANNELS; c++) active_notes_by_channel[c] = INVALID_NOTE;
    
    subscribe(keycode_topic);
    
    MidiParser first_parsing = first_pass(full_mid, filesize);
    
    for (size_t nc=0; nc<NUM_CHANNELS_USED; nc++) {
        channel_mask |= (1 << channels[nc].id);
    }
    channel_mask &= ~(1 << IGNORED_CHANNEL);
    
    second_pass(full_mid, filesize);
    finalize_channel_stats();
    qsort(channels, NUM_MIDI_CHANNELS, sizeof(MidiChannel), cmpchannel);
    for (unsigned c=0; c<NUM_MIDI_CHANNELS; c++) {
        if (channels[c].size > 0) printf("Channel %02u:  size %lu  mean %f  variance %f\n", channels[c].id, channels[c].size, channels[c].mean, channels[c].variance);
    }
    
    printf("Tracks: %u, Tempo: %ld, Divisions: %ld, Note events: %lu, Channel mask: 0x%04x\n", first_parsing.num_tracks, first_parsing.tempo, first_parsing.divisions, num_notes, channel_mask);
    
    qsort(notes, num_notes, sizeof(MidiNote), cmpnotes);
    
    /*for (size_t nt=0; nt<num_notes; nt++) {
        debug_printf("Note %s %u  channel %u  timestamp %llu\r\n", (notes[nt].is_on ? "on" : "off"), notes[nt].note, notes[nt].channel, notes[nt].timestamp);
    }*/
    
    for (size_t n=0; n<num_notes; n++) {
        catch_escape();
        
        if (notes[n].is_on) {
            activate_note(notes[n]);
        } else {
            deactivate_note(notes[n]);
        }
        
        time_align_to(notes[n].timestamp);
        play_current_note(channels);
    }
    
    beep_toggle(0, false);
    free(notes);
    free(full_mid);
    return 0;
}
