#ifndef _MIDIPARSER_H
#define _MIDIPARSER_H

#include <stddef.h>
#include <stdint.h>


#define NUM_MIDI_CHANNELS 16


typedef void (*note_event) (unsigned note, unsigned track, unsigned channel, unsigned velocity, unsigned long long int timestamp);
typedef void (*text_event) (const char* text, unsigned long long int timestamp);


typedef struct MidiParser {
    unsigned char *midi_data;
    size_t data_length;
    size_t current_byte;
    
    unsigned num_tracks;
    long divisions;
    long tempo;
    
    unsigned long tick_accumulator;
    
    note_event note_on_callback;
    note_event note_off_callback;
    text_event text_callback;
} MidiParser;


typedef struct __attribute__((packed)) MidiHeaderAny {
    char string[4];
    uint32_t length;
} MidiHeaderAny;

typedef struct __attribute__((packed)) MidiHeaderChunk {
    MidiHeaderAny header;
    uint16_t format;
    uint16_t tracks;
    uint16_t divisions;
} MidiHeaderChunk;



typedef enum MidiParserStatus {
    MIDI_OK = 0,
    MIDI_UNSUPPORTED_FEATURE,
    MIDI_HEADER_MALFORMED,
    MIDI_TRACK_MALFORMED,
    MIDI_NO_TRACKS,
    MIDI_FILE_LENGTH_OVERRUN,
    
    
} MidiParserStatus;



#define MIDI_META_EVENT         0xFF
#define MIDI_SYSEX_START_EVENT  0xF0
#define MIDI_SYSEX_END_EVENT    0xF7
#define IS_MIDI_EVENT_FLAG      0x80


#define MIDI_META_SET_TEMPO     0x51

#define MIDI_NOTE_OFF           0x80
#define MIDI_NOTE_ON            0x90
#define MIDI_POLY_PRESSURE      0xa0
#define MIDI_CONTROL_CHANGE     0xb0
#define MIDI_PROGRAM_CHANGE     0xc0
#define MIDI_CHANNEL_PRESSURE   0xd0
#define MIDI_PITCH_WHEEL        0xe0




MidiParserStatus MidiParser_init(MidiParser *parser);
MidiParserStatus MidiParser_parse(MidiParser *parser, unsigned char *midi_data, size_t data_length);


#endif
