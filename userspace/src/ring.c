#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/happy.h>

#include "genetics.h"

#define SUBCMDLEN 1024
static char default_ring[] = "f,def,init,appdata,init,init.ring";
#define BOOT_TIME_SHELL "f,def,init,app,kosh.elf"
#define BOOT_TIME_SHELL_SCRIPT "f,def,init,appdata,init,boot_time.js"

static bool debug;
static bool sauron;
ProcIdent identity;

int discarded_return_code;



typedef struct TaskListEntry {
    unsigned int task_id;
    unsigned int task_alias;
    float happyness;
    bool is_dead;
    
    float tmp_tally;
    float tmp_multiplicity;
} TaskListEntry;
// implicit assumption: if a task does not publish happyness data for a period, we assume it's happy same as before

TaskListEntry* tasklist = NULL;
size_t num_tasks = 0;

typedef struct Gene {
    unsigned int task_id;
    bool is_idle;
    float happyness;
} Gene;

typedef struct Chromosome {
    float fitness;
    Gene genes[0];
} Chromosome;

Chromosome* dna = NULL;
Chromosome* tmp_dna = NULL;
size_t population = 0;

#define CHROMOSOME_LENGTH ((num_tasks * GENES_PER_TASK) + EXTRA_GENES)
#define CHROMOSOME_SIZE_BYTES (sizeof(float) + (CHROMOSOME_LENGTH * sizeof(Gene)))
#define CHROMOSOME_CROSSOVER_POINT_BYTES(atgene) (sizeof(float) + (atgene * sizeof(Gene)))
#define CHROMOSOME(id) ((Chromosome*)(((uint8_t*)dna) + ((id)*CHROMOSOME_SIZE_BYTES)))
#define TMPCHROMOSOME(id) ((Chromosome*)(((uint8_t*)tmp_dna) + ((id)*CHROMOSOME_SIZE_BYTES)))


#define FOREACH_CHROMOSOME(current, body) { \
    for (size_t chromo_iter=0; chromo_iter<POPULATION; chromo_iter++) { \
        Chromosome* current = CHROMOSOME(chromo_iter); \
        body \
    } \
}


TaskListEntry* get_task_entry(unsigned task_id) {
    for (size_t t=0; t<num_tasks; t++) {
        if (tasklist[t].task_id == task_id) return tasklist + t;
    }
    for (size_t t=0; t<num_tasks; t++) {
        if ((tasklist[t].task_alias == task_id) && (tasklist[t].task_alias != 0)) return tasklist + t;
    }
    return NULL;
}

size_t get_task_entry_index(unsigned task_id) {
    for (size_t t=0; t<num_tasks; t++) {
        if (tasklist[t].task_id == task_id) return t;
    }
    return (size_t)(-1);
}



size_t Chromosome_variety_score(Chromosome* chromo) {
    bool* found = calloc(sizeof(bool) * num_tasks, 1);
    size_t score = 0;
    
    for (size_t geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {
        Gene* curr = chromo->genes + geneid;
        if (curr->is_idle) continue;
        
        size_t task_idx = get_task_entry_index(curr->task_id);
        if (!found[task_idx]) {
            found[task_idx] = true;
            score++;
        }
    }
    
    free(found);
    return score;
}

int Chromosome_cmp(const void * a, const void * b) {
    Chromosome *alpha = (Chromosome*) a;  // that's why we all love C
    Chromosome *beta = (Chromosome*) b;
    if (alpha->fitness > beta->fitness) return -1;
    if (alpha->fitness < beta->fitness) return 1;
    return 0;
}


void debug_chromosome(Chromosome* chromo) {
    debug_printf(") Fit:%.04f [", chromo->fitness);
    Gene* curr;
    for (size_t geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {
        curr = chromo->genes + geneid;
        
        if (curr->is_idle) debug_printf("ZZ");
        else debug_printf("%02d", curr->task_id);
        
        if (geneid+1<CHROMOSOME_LENGTH) debug_printf("|");
    }
    
    debug_printf("]\r\n");
}


void Gene_mutate(Gene* gene) {
    if (num_tasks == 0) return;
    
    if (gene->is_idle) {
        int dice = rand() % num_tasks;
        gene->task_id = tasklist[dice].task_id;
        gene->happyness = 0.0;
        gene->is_idle = false;
    } else {
        int dice = rand() % (num_tasks*IDLE_MUTATION_PENALTY + 1);
        if (dice >= num_tasks*IDLE_MUTATION_PENALTY) {
            gene->is_idle = true;
        }
        else {
            gene->task_id = tasklist[dice / IDLE_MUTATION_PENALTY].task_id;
            gene->happyness = 0.0;
            gene->is_idle = false;
        }
    }
}

void Gene_probab_mutate(Gene* gene) {
    int mut_chance = rand() % 10000;
    if (mut_chance >= (int)(MUTATION_PROBABILITY*10000)) return;
    
    if (debug) debug_printf("        mutation!\r\n");
    
    Gene_mutate(gene);
}



void setup_capture_channel() {
    MQStatus status = advertise_channel_advanced(DEFAULT_TASK_ADOPTION_TOPIC, 1024, sizeof(int), Human, Citizen, SecFullyEnforced);
    if (status != MQ_ALREADY_EXISTS && status != MQ_SUCCESS) {
        printf("Warning: cannot create topic %s\n", DEFAULT_TASK_ADOPTION_TOPIC);
    }
    MQStatus status2 = subscribe(DEFAULT_TASK_ADOPTION_TOPIC);
    if (status2 != MQ_SUCCESS) {
        printf("Warning: cannot subscribe to topic %s\n", DEFAULT_TASK_ADOPTION_TOPIC);
    }
}

void setup_happy_channel() {
    MQStatus status = advertise_channel_advanced(DEFAULT_HAPPY_TOPIC, 1024, sizeof(HappyServiceMessage), Cat, Prisoner, SecFullyEnforced);
    if (status != MQ_ALREADY_EXISTS && status != MQ_SUCCESS) {
        printf("Warning: cannot create topic %s\n", DEFAULT_HAPPY_TOPIC);
    }
}


void hot_task_insertion(int task_id) {
#define NEWCHROMOSOME_LENGTH (((num_tasks+1) * GENES_PER_TASK) + EXTRA_GENES)
#define NEWCHROMOSOME_SIZE_BYTES (sizeof(float) + (NEWCHROMOSOME_LENGTH * sizeof(Gene)))
#define NEWCHROMOSOME(id) ((Chromosome*)(((uint8_t*)newdna) + ((id)*NEWCHROMOSOME_SIZE_BYTES)))
    
    Chromosome* newdna = calloc(POPULATION * NEWCHROMOSOME_SIZE_BYTES, 1);
    
    FOREACH_CHROMOSOME(chromo, {
        memcpy(NEWCHROMOSOME(chromo_iter), chromo, NEWCHROMOSOME_SIZE_BYTES);
        
        NEWCHROMOSOME(chromo_iter)->genes[(num_tasks * GENES_PER_TASK) + EXTRA_GENES + 0].task_id = task_id;
        NEWCHROMOSOME(chromo_iter)->genes[(num_tasks * GENES_PER_TASK) + EXTRA_GENES + 0].is_idle = false;
        NEWCHROMOSOME(chromo_iter)->genes[(num_tasks * GENES_PER_TASK) + EXTRA_GENES + 0].happyness = 0.0;
        
        for (size_t p=1; p<GENES_PER_TASK; p++) {
            NEWCHROMOSOME(chromo_iter)->genes[(num_tasks * GENES_PER_TASK) + EXTRA_GENES + p].is_idle = true;
            Gene_mutate(NEWCHROMOSOME(chromo_iter)->genes + EXTRA_GENES + (num_tasks * GENES_PER_TASK) + p);
        }
        
    });
    
#undef NEWCHROMOSOME
#undef NEWCHROMOSOME_LENGTH
#undef NEWCHROMOSOME_SIZE_BYTES
    
    free(dna);
    dna = newdna;
    
    tasklist = (TaskListEntry*) realloc(tasklist, sizeof(TaskListEntry) * (num_tasks + 1));
    tasklist[num_tasks].task_id = task_id;
    tasklist[num_tasks].task_alias = 0;
    tasklist[num_tasks].happyness = 0.0;
    tasklist[num_tasks].is_dead = false;
    
    num_tasks++;
    
    free(tmp_dna);
    tmp_dna = (Chromosome*) calloc(POPULATION * CHROMOSOME_SIZE_BYTES, 1);
}

void hot_task_deletion(int task_id) {
#define NEWCHROMOSOME_LENGTH (((num_tasks-1) * GENES_PER_TASK) + EXTRA_GENES)
#define NEWCHROMOSOME_SIZE_BYTES (sizeof(float) + (NEWCHROMOSOME_LENGTH * sizeof(Gene)))
#define NEWCHROMOSOME(id) ((Chromosome*)(((uint8_t*)newdna) + ((id)*NEWCHROMOSOME_SIZE_BYTES)))
    
    Chromosome* newdna = calloc(POPULATION * NEWCHROMOSOME_SIZE_BYTES, 1);
    
    // replaces genes carrying the dead task with idle genes
    FOREACH_CHROMOSOME(chromo, {
        for (size_t geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {
            Gene* curr = chromo->genes + geneid;
            if (curr->task_id == task_id) curr->is_idle = true;
        }
    });
    
    // blindly trim the end of all chromosomes by GENES_PER_TASK, hoping that the system is capable of 
    // evolving to compensate the damage
    FOREACH_CHROMOSOME(chromo, {
        memcpy(NEWCHROMOSOME(chromo_iter), chromo, NEWCHROMOSOME_SIZE_BYTES);
    });
    
#undef NEWCHROMOSOME
#undef NEWCHROMOSOME_LENGTH
#undef NEWCHROMOSOME_SIZE_BYTES
    
    free(dna);
    dna = newdna;
    
    size_t dead_task_idx = get_task_entry_index(task_id);
    for (size_t t=dead_task_idx; t<num_tasks-1; t++) {
        tasklist[t] = tasklist[t+1];
    }
    
    tasklist = (TaskListEntry*) realloc(tasklist, sizeof(TaskListEntry) * (num_tasks - 1));
    
    num_tasks--;
    
    free(tmp_dna);
    tmp_dna = (Chromosome*) calloc(POPULATION * CHROMOSOME_SIZE_BYTES, 1);
    
    FOREACH_CHROMOSOME(chromo, {
        for (size_t geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {
            if (chromo->genes[geneid].is_idle) {
                Gene_mutate(chromo->genes + geneid);
            }
        }
    });
}


void boot_time_shell() {
    setenv("script", BOOT_TIME_SHELL_SCRIPT, 1);
    int boot_task_id;
    int status = prelaunch(BOOT_TIME_SHELL, 
                           identity.phys_race_attrib, identity.data_class_attrib,
                           &boot_task_id, 
                           &discarded_return_code);
    if (status != 0) {
        printf("Warning: ring could not launch boot time shell \"%s\"\n", BOOT_TIME_SHELL);
    } else {
        hot_task_insertion(boot_task_id);
    }
}


void init_dna() {
    dna = (Chromosome*) calloc(POPULATION * CHROMOSOME_SIZE_BYTES, 1);
    tmp_dna = (Chromosome*) calloc(POPULATION * CHROMOSOME_SIZE_BYTES, 1);
    
    FOREACH_CHROMOSOME(chromo, {
        chromo->fitness = 0.0;
        for (size_t geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {
            Gene* curr = chromo->genes + geneid;
            if (geneid < num_tasks) {
                curr->task_id = geneid;
                curr->is_idle = false;
            } else {
                curr->is_idle = true;
            }
        }
    });
}

void init_tasklist_from_ringfile(FILE* fp) {
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    char *full_text = (char*) malloc(sizeof(char) * (filesize + 1));
    fread(full_text, 1, filesize, fp);
    full_text[filesize] = '\0';
    
    // split the ringfile text into lines
    size_t num_entries = (strsplit(full_text, '\n', filesize)-1);
    tasklist = (TaskListEntry*) malloc(sizeof(TaskListEntry) * num_entries);
    num_tasks = 0;
    
    // each line defines a program, try to prelaunch it
    for (size_t entry_id=0; entry_id<num_entries; entry_id++) {
        char *launch_task_name = strsplit_chunk_at(full_text, filesize, entry_id);
        rstrip(launch_task_name);
        if (strlen(launch_task_name) == 0) continue;
        if (debug) printf("task %d: %s\n", num_tasks, launch_task_name);
        int status = prelaunch(launch_task_name, 
                               identity.phys_race_attrib, identity.data_class_attrib,
                               &((tasklist + num_tasks)->task_id), 
                               &discarded_return_code);
        if (status != 0) {
            printf("Warning: init could not launch task %s (error: %d)\n", launch_task_name, status);
        } else {
            (tasklist + num_tasks)->happyness = 0.0;
            (tasklist + num_tasks)->is_dead = false;
            num_tasks++;
        }
    }
    
    free(full_text);
    
    init_dna();
}



bool find_dead_task_in_list(unsigned* task_id) {
    for (size_t t=0; t<num_tasks; t++) {
        if (tasklist[t].is_dead) {
            (*task_id) = tasklist[t].task_id;
            return true;
        }
    }
    return false;
}

void cleanup_routine() {
    unsigned dead_task;
    bool found_dead = false;
    do {
        found_dead = find_dead_task_in_list(&dead_task);
        if (found_dead) hot_task_deletion(dead_task);
    } while (found_dead);
}


void idle_routine() {
    k_sleep_milli(QUANTUM_OF_SLEEP_MS);
}



unsigned find_parent_of_task(unsigned task_id, TaskInfos* infos, size_t num_task_records) {
    debug_printf("    ");
    for (size_t t=0; t<num_task_records; t++) {
        debug_printf("%u ", infos[t].task_id);
        if (infos[t].task_id == task_id) return infos[t].parent_task;
    }
    debug_printf("?\r\n");
    return 0;
}

unsigned find_ancestor_of_task(unsigned task_id) {
    debug_printf("finding ancestor of %u\r\n", task_id);
    FILE* fp = fopen("f,sys,dial,robot,tasks", "rb");
    if (fp == NULL) return 0;
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    TaskInfos* infos = (TaskInfos*) malloc(filesize);
    fread(infos, 1, filesize, fp);
    fclose(fp);
    size_t num_task_records = filesize / sizeof(TaskInfos);
    
    FILE* fp2 = fopen("f,test", "wb");
    fwrite(infos, 1, filesize, fp2);
    fclose(fp2);
    
    debug_printf("  %u task records\r\n", num_task_records);
    
    unsigned curr_task = task_id;
    unsigned ancestor = 0;
    do {
        ancestor = find_parent_of_task(curr_task, infos, num_task_records);
        debug_printf("    %u > %u\r\n", ancestor, curr_task);
        if (ancestor == 0) {
            free(infos);
            if (task_id == curr_task) return 0;
            return curr_task;
        }
        curr_task = ancestor;
    } while (ancestor != 0);
    
    free(infos);
    return 0;
}


void fetch_happyness_data() {
    MQStatus status;
    HappyServiceMessage msg;
    
    do {
        status = poll(DEFAULT_HAPPY_TOPIC, &msg);
        if (status == MQ_SUCCESS) {
            TaskListEntry* task = get_task_entry(msg.task_id);
            if (task == NULL) {
                unsigned ancestor = find_ancestor_of_task(msg.task_id);
                if (ancestor != 0) {
                    task = get_task_entry(ancestor);
                    task->task_alias = msg.task_id;
                    debug_printf("############# %u -> %u\r\n", msg.task_id, ancestor);
                }
            }
            if (task != NULL) {
                task->happyness = msg.happyness;
                //if (debug) debug_printf("task %u posted happyness=%.4f\r\n", msg.task_id, msg.happyness);
            }
        }
    } while (status == MQ_SUCCESS);
}


void compute_fitness(Chromosome* chromo) {
    float accum = 0.0;
    
    for (size_t t=0; t<num_tasks; t++) {
        tasklist[t].tmp_tally = 0.0;
        tasklist[t].tmp_multiplicity = 0.0;
    }
    
    for (size_t geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {
        Gene* curr = chromo->genes + geneid;
        if (curr->is_idle) {
            accum += IDLE_PAYOFF;
        } else {
            TaskListEntry* task = get_task_entry(curr->task_id);
            task->tmp_tally += curr->happyness;
            task->tmp_multiplicity += 1.0;
        }
        curr->happyness = 0.0;
    }
    
    for (size_t t=0; t<num_tasks; t++) {
        accum += (tasklist[t].tmp_tally / (tasklist[t].tmp_multiplicity + 0.000001)) * (1.0 - MULTIPLICITY) + (tasklist[t].tmp_tally * MULTIPLICITY);
    }
    
    float penalty = ((float)(num_tasks - Chromosome_variety_score(chromo))) * MISSING_TASK_PENALTY;
    accum += penalty;
    
    chromo->fitness = accum;
}


void roulette_select(Chromosome** a, Chromosome** b) {
    int total_fitness = 0;
    float lowest_fitness = 0;
    
    FOREACH_CHROMOSOME(chromo, {
        if (chromo->fitness < lowest_fitness) lowest_fitness = chromo->fitness;
    });
    
    FOREACH_CHROMOSOME(chromo, {
        total_fitness += ((int)((chromo->fitness - lowest_fitness) * 10000.0) + 10000);
    });
    
    int dice = rand() % total_fitness;
    
    int lower_bound = 0;
    int upper_bound = 0;
    
    FOREACH_CHROMOSOME(chromo, {
        upper_bound += ((int)((chromo->fitness - lowest_fitness) * 10000.0) + 10000);
        if (dice >= lower_bound && dice < upper_bound) {
            (*a) = chromo;
            if (debug) debug_printf("A=%u ", chromo_iter);
            break;
        }
        lower_bound = upper_bound;
    });
    
    bool normal_exit = false;
    for (size_t retry=0; retry<10; retry++) {
        dice = rand() % total_fitness;
        lower_bound = 0;
        upper_bound = 0;
        FOREACH_CHROMOSOME(chromo, {
            upper_bound += ((int)((chromo->fitness - lowest_fitness) * 10000.0) + 10000);
            if (dice >= lower_bound && dice < upper_bound) {
                if (chromo == (*a)) {
                    normal_exit = false;
                } else {
                    normal_exit = true;
                    if (debug) debug_printf("B=%u ", chromo_iter);
                }
                (*b) = chromo;
                break;
            }
            lower_bound = upper_bound;
        });
        if (normal_exit) break;
    }
}


void genetic_selection_routine() {
    size_t new_pop_count = 0;
    
    // sort the dna for firtness, reverse order
    if (debug) debug_printf("    Sorting dna...\r\n");
    qsort((void*)dna, POPULATION, CHROMOSOME_SIZE_BYTES, &Chromosome_cmp);
    
    //debug_population();
    
    debug_printf("fittest: "); debug_chromosome(CHROMOSOME(0));
    
    // apply elitism
    if (debug) debug_printf("    Applying elitism factor %d\r\n", ELITISM);
    for (size_t elit=0; elit<ELITISM; elit++) {
        memcpy(TMPCHROMOSOME(new_pop_count), CHROMOSOME(new_pop_count), CHROMOSOME_SIZE_BYTES);
        new_pop_count++;
    }
    
    // apply roulette selection
    while (new_pop_count < POPULATION) {
        if (debug) debug_printf("    Applying crossover... ");
        Chromosome* one;
        Chromosome* two;
        roulette_select(&one, &two);
        int crossover_point = rand() % CHROMOSOME_LENGTH;
        
        if (debug) debug_printf("Point=%d\r\n", crossover_point);
        
        for (int geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {
            if (geneid < crossover_point) {
                TMPCHROMOSOME(new_pop_count  )->genes[geneid] = one->genes[geneid];
                TMPCHROMOSOME(new_pop_count+1)->genes[geneid] = two->genes[geneid];
            } else {
                TMPCHROMOSOME(new_pop_count  )->genes[geneid] = two->genes[geneid];
                TMPCHROMOSOME(new_pop_count+1)->genes[geneid] = one->genes[geneid];
            }
        }
        
        new_pop_count += 2;
    }
    
    Chromosome* swap;
    swap = dna;
    dna = tmp_dna;
    tmp_dna = swap;
    
    // apply mutations
    if (debug) debug_printf("    Applying mutations...\r\n");
    FOREACH_CHROMOSOME(chromo, {
        for (size_t geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {
            Gene_probab_mutate(chromo->genes + geneid);
        }
    });
}


void adoption_routine() {
    // see if there is a task to adopt
    int adopted_task_id;
    MQStatus poll_status = poll(DEFAULT_TASK_ADOPTION_TOPIC, &adopted_task_id);
    if (poll_status == MQ_SUCCESS) {
        if (adopt(adopted_task_id) == 0) {
            hot_task_insertion((unsigned)adopted_task_id);
        }
    }
}


void debug_population() {
    debug_printf("---- Population at time %llu: ----\r\n", clock_ms());
    FOREACH_CHROMOSOME(chromo, {
        debug_printf("%02u", chromo_iter);
        debug_chromosome(chromo);
    });
    
    debug_printf("\r\n");
}


void init_loop() {   
    while (num_tasks > 0) {
        
        FOREACH_CHROMOSOME(chromo, {  // iterate over chromosomes (schedules)
            for (size_t geneid=0; geneid<CHROMOSOME_LENGTH; geneid++) {  // iterate over genes (scheduled tasks)
                Gene* curr = chromo->genes + geneid;
                if (curr->is_idle) {
                    if (debug) debug_printf("IDLE at chr#%lu gene#%lu\r\n", chromo_iter, geneid);
                    idle_routine();
                }
                else {
                    if (debug) debug_printf("task %u at chr#%lu gene#%lu", curr->task_id, chromo_iter, geneid);
                    if (get_task_entry(curr->task_id)->is_dead) {
                        if (debug) debug_printf(" ... which is dead, idling.\r\n");
                        idle_routine();
                    }
                    else {
                        int swapstatus = task_switch(curr->task_id); // control switched to child task
                        if (debug) debug_printf(" ... which returned status: %d\r\n", swapstatus);
                        if (swapstatus != 0) {
                            get_task_entry(curr->task_id)->is_dead = true; // remember that the current task is dead
                        } else {
                            fetch_happyness_data();
                            curr->happyness = get_task_entry(curr->task_id)->happyness;
                        }
                    }
                }
            }
            if (debug) debug_printf("evaluating fitness of chromosome #%lu\r\n", chromo_iter);
            compute_fitness(chromo);
        });
        
        cleanup_routine();
        
        if (debug) debug_printf("running genetic selection... \r\n");
        genetic_selection_routine();
        if (debug) {
            debug_printf("done, population:\r\n");
            debug_population();
        }
        
        adoption_routine();
    }
}



int main(void) {
    char* fname = default_ring;
    char subcommand[SUBCMDLEN];
    debug = false;
       
    identity = ident();  // needed later
    
    FILE* fp = fopen(fname, "r");
    if (fp == NULL) {
        printf("Error: cannot open ringfile %s\n", fname);
        return 1;
    }
    init_tasklist_from_ringfile(fp);
    fclose(fp);
    
    srand(time(NULL));
    
    setup_capture_channel();
    setup_happy_channel();
    
    boot_time_shell();
    subscribe(DEFAULT_HAPPY_TOPIC);
    
    if (debug) {
        debug_population();
        debug_printf("Starting init loop\r\n");
    }
    
    init_loop();

    return 0;  // return from init, very bad
}
 
