.section .data

.section .text

.global _start
_start:
    mov %esp, %ebp
    mov $0x0, %edi  # argc
    mov $0x0, %esi  # argv
    mov %eax, (environ)
    call setup_stdio
    call setup_clock
    call main
    
    push %eax
    call exit
    
.size _start, . - _start
