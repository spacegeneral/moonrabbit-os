#include <stdio.h>
#include <stdlib.h>
#include <libgui/libgui.h>
#include <sys/proc.h>
#include <sys/term.h>
#include <sys/keycodes.h>


SoftTerminal info;
BitmapFont font;
OpenDoor main_door;
bool running = true;
bool button_pressed = false;


void mouse_callback(LibguiMsgMouseEvent *event) {
    SoftTerminal_clear(&info);
    SoftTerminal_gotoxy(&info, 0, 0);
    SoftTerminal_printf(&info, "mouse x,y: %d,%d\nbuttons (l|m|r): %s|%s|%s\nclick: %d\ndbclick: %d\nenter: %d\nleave: %d\ntime: %llu\ndialog clicked: %s\n", 
                        event->x, event->y,
                        (event->btn_left ? "down" : "up"), (event->btn_mid ? "down" : "up"), (event->btn_right ? "down" : "up"), 
                        event->is_click, event->is_dbclick, event->is_enter, event->is_leave, 
                        event->timestamp, 
                        (button_pressed ? "yes" : "no"));
    OpenDoor_update(&main_door, true);
}


void keycode_callback(KeyEvent *keycode) {
    switch (keycode->keycode) {
        case KEY_Escape:
            running = false;
            break;
        case KEY_F2:
            {
                button_pressed = dialog_yesno("Please click one of the following buttons:", "yes", "no", font);
            }
            break;
    }
}


int main(void) {
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = 5,
        .y = ti.screensize.h - 200 - 5,
        .w = 250,
        .h = 200,
    };
    bool open_success = open_main_door(&main_door, main_area, "Hello world!");
    if (!open_success) {
        printf("hello_gui: Error opening main door\n");
        return 2;
    }
    
    main_door.mouse_callback = &mouse_callback;
    main_door.keycode_callback = &keycode_callback;
    
    Rect label_pos = {.x = 5, .y = 5, .w = 245, .h = 40};
    SoftTerminal label = SoftTerminal_init(&(main_door.face), label_pos, font, make_color_8bpp(255, 0, 0), make_color_8bpp(90, 18, 68), 1, true);
    SoftTerminal_printf(&label, "Hello world!");
    
    Rect info_pos = {.x = 5, .y = 45, .w = 245, .h = 150};
    info = SoftTerminal_init(&(main_door.face), info_pos, font, make_color_8bpp(255, 255, 255), make_color_8bpp(90, 18, 68), 0, true);
    
    bool update_success = OpenDoor_update(&main_door, true);
    if (!update_success) {
        printf("Error updating main door\n");
        return 3;
    }
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        yield();
    }
    
    OpenDoor_close(&main_door);
    destroy_bitmap_font(font);
    
    return 0;
}
