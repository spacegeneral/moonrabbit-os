#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fs.h>

#define MAXCOMMAND 2048

int main(void) {
    char subcommand[MAXCOMMAND];
    
    char fname[MAXCOMMAND] = {0};
    char mountname[MAXCOMMAND] = "undefined";
    char fsname[MAXCOMMAND] = {0};
    
    char *file_param = getenv("disk");
    if (file_param == NULL) {
        printf("Partition to mount: ");
        getsn(subcommand, MAXCOMMAND);
        rstrip(subcommand);
        strcpy(fname, subcommand);
    } else {
        strcpy(fname, file_param);
    }
    
    char *fs_param = getenv("fs");
    if (fs_param == NULL) {
        printf("Filesystem: ");
        getsn(subcommand, MAXCOMMAND);
        rstrip(subcommand);
        strcpy(fsname, subcommand);
    } else {
        strcpy(fsname, fs_param);
    }
    
    int failure = mount(fname, mountname, fsname);
    if (failure) {
        printf("error %d trying to mount %s partition %s to %s\n", failure, fsname, fname, mountname);
        return failure;
    } else {
        printf("mounted at %s\n", mountname);
    }
    return 0;
}

