#pragma once

#include <stdint.h>
#include <time.h>

extern time_t prev_time;
void align_time_period(uint32_t time_ms);
