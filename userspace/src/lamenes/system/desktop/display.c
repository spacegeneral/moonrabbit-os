#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "lamenes.h"
#include "palette.h"

#include "system/display.h"

OpenDoor main_door;
Rect main_area;

void display_init(uint16_t width, uint16_t height, DisplayType display_type, bool fullscreen) {
    main_area.x = 0;
    main_area.y = 0;
    main_area.w = width;
    main_area.h = height;
    
    bool open_success = open_main_door(&main_door, main_area, "lamenes");
    if (!open_success) {
        printf("lamenes: Error opening main door\n");
        exit(2);
    }

    main_door.keycode_callback = &keycode_callback;
 
    OpenDoor_update(&main_door, true);
}

void display_lock(void) {
}

/*void display_set_pixel(uint16_t x, uint16_t y, uint8_t nes_color) {
  if (x >= main_area.w) {
    #ifdef PPU_DEBUG
    printf("[%d]: warning attempt to draw outside the screen! [x = %d]\n", debug_cnt, x);
    #endif

    return;
  }

  if (y >= main_area.h) {
    #ifdef PPU_DEBUG
    printf("[%d]: warning attempt to draw outside the screen! [y = %d]\n", debug_cnt, y);
    #endif

    return;
  }

  int cR = palette[nes_color].r;
  int cG = palette[nes_color].g;
  int cB = palette[nes_color].b;

  if(nes_color == 0)
    return;

  Color24 color = make_color_8bpp(cR, cG, cB);
  Color24* pixel = PIXELAT(&(main_door.face), x, y);
  *pixel = color;
}*/

void display_update(void) {
  uint8_t nes_color = ppu_memory[0x3f00];

  /* update the screen */
  OpenDoor_update(&main_door, true);  // yield in here

  /* clear the surface and set it to nes_color 0x10 */
  Rect blank_area = {
        .x = 0,
        .y = 0,
        .w = main_area.w,
        .h = main_area.h,
    };
  draw_rectangle_area(&(main_door.face), blank_area, make_color_8bpp(palette[nes_color].r, palette[nes_color].g, palette[nes_color].b));
}

void display_unlock() {
}
