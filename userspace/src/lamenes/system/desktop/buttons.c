#include "system/buttons.h"
#include "system/moonrabbitos.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "lame6502/lame6502.h"
#include "lame6502/debugger.h"

#include "lamenes.h"
#include "input.h"

void toggle_input(int num, bool status) {
    if (status) {
        set_input((uint8_t)num);
    } else {
        clear_input((uint8_t)num);
    }
}

void keycode_callback(KeyEvent *keycode) {
    //debug_printf("Got key %d, status %d\r\n", keycode->keycode, keycode->is_pressed);
    switch (keycode->keycode) {
        case KEY_Escape:
            quit_emulation();
            break;
        case KEY_Enter: // start
            toggle_input(5, keycode->is_pressed); break;
        case KEY_Apostrophe: // select
            toggle_input(6, keycode->is_pressed); break;
        case KEY_O: // B
            toggle_input(8, keycode->is_pressed); break;
        case KEY_P:
            toggle_input(7, keycode->is_pressed); break;
        case KEY_W: // up
            toggle_input(2, keycode->is_pressed); break;
        case KEY_A:  // left
            toggle_input(3, keycode->is_pressed); break;
        case KEY_S:  // down
            toggle_input(1, keycode->is_pressed); break;
        case KEY_D: // right
            toggle_input(4, keycode->is_pressed); break;
        case KEY_H:
            if (!keycode->is_pressed) break;
            if(pause_emulation) {
                printf("[*] LameNES continue emulation!\n");
                CPU_is_running = 1;
                pause_emulation = 0;
            } else if(!pause_emulation) {
                printf("[*] LameNES paused!\n");
                CPU_is_running = 0;
                pause_emulation = 1;
            }
        case KEY_F1:
            if (!keycode->is_pressed) break;
            reset_emulation(); break;
        case KEY_F3:
            if (!keycode->is_pressed) break;
            load_state(); break;
        case KEY_F6:
            if (!keycode->is_pressed) break;
            save_state(); break;
        case KEY_F10:
            if (!keycode->is_pressed) break;
            if(enable_background == 1) {
                enable_background = 0;
            } else {
                enable_background = 1;
            }
        break;
        case KEY_F11:
            if (!keycode->is_pressed) break;
            if(enable_sprites == 1) {
                enable_sprites = 0;
            } else {
                enable_sprites = 1;
            }
        break;
        case KEY_F12:
            if (!keycode->is_pressed) break;
            if(startdebugger > 0) {
                disassemble = 1;
                hit_break = 1;
                debugger();
            }
        break;
    }
}


void poll_buttons(void) {
    OpenDoor_handle_io(&main_door);
}
