#include <stdio.h>
#include <stdlib.h>
#include <sys/errno.h>
#include <sys/proc.h>
#include <sys/serv.h>
#include <sys/mq.h>
#include <sys/share.h>
#include <sys/default_topics.h>

#include <collections/hashtable.h>
#include <collections/array.h>


typedef struct AreaDescriptor {
    void* shared_mem;
    char name[MAX_PATH_LENGTH];
    size_t guaranteed_size;
    Array* referents;
} AreaDescriptor;



HashTable* areas_by_name;

bool running = true;
bool debug = false;

unsigned int requestor_task_id;



AreaDescriptor* alloc_new_area_with_name(char* name, size_t size) {
    void* area;
    AreaDescriptor* desc;
    
    desc = malloc(sizeof(AreaDescriptor));
    if (desc == NULL) return NULL;
    
    area = malloc_coarse(size);
    desc->shared_mem = area;
    strncpy(desc->name, name, MAX_PATH_LENGTH);
    desc->guaranteed_size = size;
    
    if (area != NULL) {
        int status = hashtable_add(areas_by_name, desc->name, desc);
        if (status != CC_OK) {
            free(area);
            free(desc);
            return NULL;
        }
    }
    
    array_new(&desc->referents);
    array_add(desc->referents, (void*)requestor_task_id);
    
    if (debug) debug_printf("sharedrv      alloc_new_area_with_name(%s, %lu) = %p\r\n", name, size, desc);
    return desc;
}


void area_add_referent(AreaDescriptor* desc) {
    if (array_contains(desc->referents, (void*)requestor_task_id) == 0) {
        array_add(desc->referents, (void*)requestor_task_id);
    }
}


void dumb_dealloc_area(AreaDescriptor* desc) {
    free(desc->shared_mem);
    array_destroy(desc->referents);
    free(desc);
}

int dealloc_area_with_name(char* name) {
    AreaDescriptor* desc;
    hashtable_remove(areas_by_name, name, (void**)&desc);
    
    array_remove(desc->referents, (void*)requestor_task_id, NULL);
    
    if (array_size(desc->referents) == 0) {
        if (debug) debug_printf("sharedrv say goodbye to shared area %s\r\n", name);
        hashtable_remove(areas_by_name, name, NULL);
        dumb_dealloc_area(desc);
        return 0;
    }
    
    return 1;
}





bool purge_one_area_belonging_to(unsigned int referent) {
    TableEntry *current_entry;
    AreaDescriptor *current;
    if (hashtable_size(areas_by_name) == 0) return false;
    
    HASHTABLE_FOREACH(current_entry, areas_by_name, {
        current = (AreaDescriptor*) current_entry->value;
        if (array_contains(current->referents, (void*)referent)) {
            array_remove(current->referents, (void*)referent, NULL);
            if (array_size(current->referents) == 0) {
                if (debug) debug_printf("sharedrv say goodbye to shared area %s\r\n", current->name);
                hashtable_remove(areas_by_name, current->name, NULL);
                dumb_dealloc_area(current);
                return true;
            }
        }
    });
    return false;
}

void purge_areas_belonging_to(unsigned int referent) {
    bool found;
    do {
        found = purge_one_area_belonging_to(referent);
    } while (found);
}



int alloc_area_load_file(char* filename, unsigned long long* size, void** area, bool reload) {
    FILE *fp;
    fp = fopen(filename, "rb");
    if (fp == NULL) return SHARE_LOAD_FAILED;
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    void* memstore;
    if (reload && hashtable_contains_key(areas_by_name, filename)) {
        AreaDescriptor* desc;
        hashtable_get(areas_by_name, filename, (void**)&desc);
        memstore = desc->shared_mem;
        filesize = min(filesize, (long)desc->guaranteed_size);
        area_add_referent(desc);
    } else {
        memstore = alloc_new_area_with_name(filename, filesize)->shared_mem;
    }
    
    if (memstore == NULL) {
        fclose(fp);
        return SHARE_ALLOC_FAILED;
    }
    
    size_t nread = fread(memstore, 1, filesize, fp);
    if (nread != (size_t)filesize) {
        dealloc_area_with_name(filename);
        fclose(fp);
        return SHARE_LOAD_FAILED;
    }
    
    fclose(fp);
    
    (*size) = (unsigned long long)filesize;
    (*area) = memstore;
    return 0;
}


void handle_share_request(ShareServiceMessage* request) {
    if (debug) debug_printf("sharedrv handling request %d\r\n", request->verb);
    switch (request->verb) {
        case shareservice_give_area:
        {
            void* area;
            AreaDescriptor* desc;
            
            if (!hashtable_contains_key(areas_by_name, request->resource_name)) {
                if (request->flags & SHARE_FLAG_MUST_EXIST) {
                    request->status = SHARE_NOT_FOUND;
                    if (debug) debug_printf("sharedrv Failed to allocate area %s (%lu bytes): not found\r\n", request->resource_name, request->size);
                    return;
                }
                desc = alloc_new_area_with_name(request->resource_name, request->size);
            } else {
                hashtable_get(areas_by_name, request->resource_name, (void**)&desc);
            }
            if (desc == NULL) {
                request->status = SHARE_ALLOC_FAILED;
                return;
            }
            
            area = desc->shared_mem;
            if (area == NULL) {
                request->status = SHARE_ALLOC_FAILED;
                return;
            }
            if (desc->guaranteed_size != request->size) {
                request->status = SHARE_DEFINITION_CLASH;
                if (debug) debug_printf("sharedrv Allocated area %s to %p. Warning clash in size (%lu requested, %lu obtained)\r\n", request->resource_name, (unsigned long)area, request->size, desc->guaranteed_size);
                request->size = desc->guaranteed_size;
            } else {
                request->status = 0;
                if (debug) debug_printf("sharedrv Allocated area %s (%lu bytes) to %p\r\n", request->resource_name, request->size, (unsigned long)area);
            }
            area_add_referent(desc);
            request->resource_pointer = (uintptr_t) area;
        }
        break;
        case shareservice_give_file:
        {
            AreaDescriptor* desc;
            if (!hashtable_contains_key(areas_by_name, request->resource_name)) {
                if (request->flags & SHARE_FLAG_MUST_EXIST) {
                    request->status = SHARE_NOT_FOUND;
                    if (debug) debug_printf("sharedrv Failed to allocate file %s: not found\r\n", request->resource_name);
                    return;
                }
                request->status = alloc_area_load_file(request->resource_name, &(request->size), (void**)&(request->resource_pointer), false);
                if (debug) debug_printf("sharedrv Allocated file %s (%lu bytes) to %p\r\n", request->resource_name, request->size, (unsigned long)request->resource_pointer);
                return;
            } else {
                if (request->flags & SHARE_FLAG_FORCE_RELOAD) {
                    request->status = alloc_area_load_file(request->resource_name, &(request->size), (void**)&(request->resource_pointer), true);
                    if (debug) debug_printf("sharedrv Reloaded file %s (%lu bytes) in %p\r\n", request->resource_name, request->size, (unsigned long)request->resource_pointer);
                    return;
                }
                hashtable_get(areas_by_name, request->resource_name, (void**)&desc);
                if (desc == NULL) {
                    request->status = SHARE_ALLOC_FAILED;
                    return;
                }
                request->status = 0;
                request->resource_pointer = (uintptr_t) desc->shared_mem;
                request->size = desc->guaranteed_size;
                area_add_referent(desc);
                if (debug) debug_printf("sharedrv retrieved file %s (%lu bytes) in %p\r\n", request->resource_name, request->size, (unsigned long)request->resource_pointer);
            }
        }
        break;
        case shareservice_free:
        {
            if (!hashtable_contains_key(areas_by_name, request->resource_name)) {
                request->status = SHARE_NOT_FOUND;
                if (debug) debug_printf("sharedrv Failed to free area %s: not found\r\n", request->resource_name);
                return;
            }
            request->status = 0;
            if (dealloc_area_with_name(request->resource_name) == 0) {
                if (debug) debug_printf("sharedrv Freed area %s\r\n", request->resource_name);
            } else {
                if (debug) debug_printf("sharedrv area %s has open references, keeping.\r\n", request->resource_name);
            }
        }
        break;
    }
}



int main(void) {
    
    debug = (getenv("debug") != NULL);
    
    ProcIdent identity = ident();
    
    ServStatus serv_creation_status = advertise_service_advanced(DEFAULT_SHARE_SERVICE, sizeof(ShareServiceMessage), identity.phys_race_attrib, identity.data_class_attrib);
    if (serv_creation_status != SERV_SUCCESS) {
        printf("sharedrv Error %d cannot create service %s\n", serv_creation_status, DEFAULT_SHARE_SERVICE);
        return 1;
    }
    
    subscribe(DEFAULT_GRAVEYARD_TOPIC);
    
    hashtable_new(&areas_by_name);
    
    ServStatus accept_status, complete_status;
    MQStatus graveyard_status;
    ShareServiceMessage request;
    unsigned int request_id;
    
    unsigned int dead_task;
    
    while (running) {
        graveyard_status = poll(DEFAULT_GRAVEYARD_TOPIC, &dead_task);
        if (graveyard_status == MQ_SUCCESS) {
            if (debug) debug_printf("sharedrv Removing references belonging to defunct task %d\r\n", dead_task);
            purge_areas_belonging_to(dead_task);
        }
        
        accept_status = service_accept(DEFAULT_SHARE_SERVICE, (void*)&request, &requestor_task_id, &request_id);
        if (accept_status == SERV_SUCCESS) {

            handle_share_request(&request);
        
            complete_status = service_complete(DEFAULT_SHARE_SERVICE, request_id);
            
            if (complete_status != SERV_SUCCESS) {
                debug_printf("sharedrv service completion error: %d\n", complete_status);
            }
        }
        
        yield();
    }
    
    
    
    return 0;
}