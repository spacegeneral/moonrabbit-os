#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/mq.h>
#include <sys/proc.h>
#include <collections/array.h>

#define SUBCMDLEN 2048

#define TRY_ADVERTISE(chantopic) \
{ \
    status = advertise_channel_advanced(chantopic, 1024, 1, Human, Citizen, SecFullyEnforced); \
    if (status != MQ_SUCCESS) { \
        printf("Failed to advertise topic %s\n", chantopic); \
        return 2; \
    } \
}

#define TRY_SUBSCRIBE(chantopic) \
{ \
    status = subscribe(chantopic); \
    if (status != MQ_SUCCESS) { \
        printf("Failed to subscribe to topic %s\n", chantopic); \
        return 3; \
    } \
}

void forward_inbound(char *topic_com_in_raw, char *topic_com_in_filtered) {
    MQStatus status;
    uint8_t message;
    uint8_t message_filtered;
    do {
        status = poll(topic_com_in_raw, &message);
        if (status == MQ_SUCCESS) {
            
            // convert '\r' -> '\n'
            if (message == '\r') {
                message_filtered = '\n';
                publish(topic_com_in_filtered, &message_filtered);
            } 
            
            
            else {
                publish(topic_com_in_filtered, &message);
            }
        }
    } while (status == MQ_SUCCESS);
}

void forward_outbound(char *topic_com_out_raw, char *topic_com_out_filtered) {
    MQStatus status;
    uint8_t message;
    uint8_t message_filtered;
    do {
        status = poll(topic_com_out_filtered, &message);
        if (status == MQ_SUCCESS) {
            
            // convert '\n' -> '\r\n'
            if (message == '\n') {
                message_filtered = '\r';
                publish(topic_com_out_raw, &message_filtered);
                message_filtered = '\n';
                publish(topic_com_out_raw, &message_filtered);
            } 
            
            else {
                publish(topic_com_out_raw, &message);
            }
        }
    } while (status == MQ_SUCCESS);
}

int main(void) {
    char *comname;
    MQStatus status;
    char *comname_param = getenv("port");
    
    char topic_com_in_raw[SUBCMDLEN];
    char topic_com_out_raw[SUBCMDLEN];
    char topic_com_in_filtered[SUBCMDLEN];
    char topic_com_out_filtered[SUBCMDLEN];
    
    if (comname_param == NULL) {
        printf("Please provide a port=portname (i.e. port=com1) variable.\n");
        return 1;
    } else {
        comname = comname_param;
    }
    
    snprintf(topic_com_in_raw, SUBCMDLEN, "/sys/io/serial/%s/recv", comname);
    snprintf(topic_com_out_raw, SUBCMDLEN, "/sys/io/serial/%s/send", comname);
    snprintf(topic_com_in_filtered, SUBCMDLEN, "/sys/io/serial/%s/filtered/recv", comname);
    snprintf(topic_com_out_filtered, SUBCMDLEN, "/sys/io/serial/%s/filtered/send", comname);
    
    TRY_ADVERTISE(topic_com_in_filtered);
    TRY_ADVERTISE(topic_com_out_filtered);
    TRY_SUBSCRIBE(topic_com_out_filtered);
    TRY_SUBSCRIBE(topic_com_in_raw);
    
    while (true) {
        forward_inbound(topic_com_in_raw, topic_com_in_filtered);
        forward_outbound(topic_com_out_raw, topic_com_out_filtered);
        yield();
        //TODO exit condition
    }

    return 0;
}

