#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/term.h>
#include <sys/proc.h>
#include <sys/mq.h>


/* The purpose of this program is to act as part of a ring, in order to
 * introduce a dynamic delay in order to guarantee a specific number of ring
 * rounds per second (FPS).
 * As a useful side effect, this program also displays the current FPS counter
 * in the top-left side of the screen.
 */


#define TARGET_FPS 120

static int TERMWIDTH;
static int TERMHEIGHT;


int main(void) {
    bool with_term_applet = (getenv("applet") != NULL);
    
    int fps = TARGET_FPS;
    if (getenv("fps") != NULL) {
        fps = atoi(getenv("fps"));
    }
    
    TermInfo term = getterminfo();
    TERMWIDTH = term.termsize.w;
    TERMHEIGHT = term.termsize.h;
    
    clock_t prev_time = clock();
    clock_t curr_time;
    
    float delay = 1.0f / (float)fps;
    
    ProcIdent my_ident = ident();
    advertise_channel_advanced("/usr/idol/fps", 2, sizeof(int), my_ident.phys_race_attrib, my_ident.data_class_attrib, SecNoWriteUp);
    
    while (true) {
        curr_time = clock();
        
        float delta = ((float)(curr_time - prev_time)) / (float)CLOCKS_PER_SEC;
        float remaining = delay - delta;
        int missing = 0;
        
        if (remaining > 0) {
            missing = (int)(remaining * CLOCKS_PER_SEC);
            k_sleep_milli(missing);
        }
        
        int actualfps = (clock() == prev_time ? 60 : 1000 / (clock() - prev_time));
        
        prev_time = clock();
        
        if (with_term_applet) {
            Point oldcur = getterminfo().cursor;
        
            togglecursor(false);
            setcursor(0, 0);
            printf("FPS %d  ", actualfps);

            setcursor(oldcur.x, oldcur.y);
            togglecursor(true);
        }
        publish("/usr/idol/fps", &actualfps);
        
        yield();
    }
    
    return 0;
}
