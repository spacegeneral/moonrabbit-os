#ifndef GENETICS_H
#define GENETICS_H


#define GENES_PER_TASK 1
#define EXTRA_GENES 8
#define POPULATION 8
#define ELITISM 2

#define MUTATION_PROBABILITY 0.01

// 1 = no penalty
// 2 = IDLE half as likely to evolve compared to <any give task_id>
// etc...
#define IDLE_MUTATION_PENALTY 2


#define IDLE_PAYOFF 0.00001
#define MISSING_TASK_PENALTY -5.0
#define MULTIPLICITY 0.0


#define QUANTUM_OF_SLEEP_MS 2


#endif