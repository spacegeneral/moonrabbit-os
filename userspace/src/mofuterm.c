#include <stdio.h>
#include <stdlib.h>
#include <libgui/libgui.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/term.h>
#include <sys/serv.h>
#include <sys/keycodes.h>
#include <time.h>
#include <yakumo.h>

#include "modular/modular.h"


SoftTerminal maintext;
OpenDoor main_door;
bool running = true;
Bitmap displaced_by_cursor;
bool cursor_enabled = true;
bool cursor_blink_enabled = true;
bool cursor_blink_visible_half = true;

int term_ro_flags = TERM_FLAG_ZOOMABLE;

Color24 textcolor;
Color24 bgcolor;

char default_program[] = "ush";

#define TERMINFO_OVERRIDE "/usr/doors/%d/terminfo"
#define TERMOUT_OVERRIDE "/usr/doors/%d/termout"
#define TERMBITMAP_OVERRIDE "/usr/doors/%d/termbitmap"
#define CURSOR_BLINK_PERIOD 500

time_t last_blinked;
int border_zone = 2;

int size_x = 688;
int size_y = 464;

Point select_start = {.x = -1, .y = -1};
int selected_amount, old_selected_amount;

void set_overrides() {
    char topicname[4096];
    
    snprintf(topicname, 4096, DOOR_EVENT_STDIN_TOPIC_TEMPLATE, main_door.door_id);
    setenv("stdin_override", topicname, 1);
    
    snprintf(topicname, 4096, DOOR_EVENT_KEYCODE_TOPIC_TEMPLATE, main_door.door_id);
    setenv("keycode_override", topicname, 1);
    
    snprintf(topicname, 4096, TERMINFO_OVERRIDE, main_door.door_id);
    setenv("terminfo_override", topicname, 1);
    
    snprintf(topicname, 4096, TERMOUT_OVERRIDE, main_door.door_id);
    setenv("termout_override", topicname, 1);
    
    snprintf(topicname, 4096, TERMBITMAP_OVERRIDE, main_door.door_id);
    setenv("termbitmap_override", topicname, 1);
}

void unset_overrides() {
    unsetenv("stdin_override");

    unsetenv("keycode_override");

    unsetenv("terminfo_override");

    unsetenv("termout_override");
    
    unsetenv("termbitmap_override");
}


void prepare_services() {
    ProcIdent my_ident = ident();
    
    char topicname[4096];
    ServStatus serv_creation_status;
    MQStatus topic_creation_status, subscribe_status;
    
    snprintf(topicname, 4096, TERMINFO_OVERRIDE, main_door.door_id);
    serv_creation_status = advertise_service_advanced(topicname, sizeof(TermInfo), Human, Maid);
    if (serv_creation_status != SERV_SUCCESS && serv_creation_status != SERV_ALREADY_EXISTS) {
        printf("Error %d cannot create service %s\n", serv_creation_status, topicname);
        exit(3);
    }
    
    snprintf(topicname, 4096, TERMBITMAP_OVERRIDE, main_door.door_id);
    serv_creation_status = advertise_service_advanced(topicname, sizeof(TermBitmapRequest), least_safe(my_ident.phys_race_attrib, Alien), least_safe(my_ident.data_class_attrib, Priest));
    if (serv_creation_status != SERV_SUCCESS && serv_creation_status != SERV_ALREADY_EXISTS) {
        printf("Error %d cannot create service %s\n", serv_creation_status, topicname);
        exit(3);
    }
    
    snprintf(topicname, 4096, TERMOUT_OVERRIDE, main_door.door_id);
    topic_creation_status = advertise_channel_advanced(topicname, 8192, sizeof(TermOutMessage), least_safe(my_ident.phys_race_attrib, Alien), least_safe(my_ident.data_class_attrib, Priest), SecNoReadUp|SecNoDataLoss);
    if (topic_creation_status != MQ_SUCCESS && topic_creation_status != MQ_ALREADY_EXISTS) {
        printf("Error %d cannot create topic %s\n", topic_creation_status, topicname);
        exit(3);
    }
    subscribe_status = subscribe(topicname);
    if (subscribe_status != MQ_SUCCESS) {
        printf("Error %d cannot subscribe to topic %s\n", subscribe_status, topicname);
        exit(3);
    }
}

void teardown_services() {
    char topicname[4096];
    
    snprintf(topicname, 4096, TERMINFO_OVERRIDE, main_door.door_id);
    retire_service(topicname);
    
    snprintf(topicname, 4096, TERMBITMAP_OVERRIDE, main_door.door_id);
    retire_service(topicname);
    
    snprintf(topicname, 4096, TERMOUT_OVERRIDE, main_door.door_id);
    retire_service(topicname);
}


void undo_overlay_cursor_on_canvas(Bitmap *canvas) {
    if (displaced_by_cursor.colordata != NULL && displaced_by_cursor.colordata != canvas->colordata) {
        Point zero = {
            .x = 0, 
            .y = 0,
        };
        
        Bitmap screencanvas = shallow_relocated_copy(canvas, zero);
        safe_overlay_bitmap(&screencanvas, &displaced_by_cursor, NULL);
    }
}

void overlay_cursor_on_canvas(Bitmap *canvas) {
    if (displaced_by_cursor.colordata != NULL && displaced_by_cursor.colordata != canvas->colordata) {
        destroy_bitmap(&displaced_by_cursor);
    }
    
    Rect cursorpos = {
        .x = maintext.geom.x + maintext.cursor.x * (CHAR_WIDTH_UNIT << maintext.zoomlevel),
        .y = maintext.geom.y + maintext.cursor.y * (CHAR_HEIGHT_UNIT << maintext.zoomlevel),
        .w = CHAR_WIDTH_UNIT << maintext.zoomlevel,
        .h = CHAR_HEIGHT_UNIT << maintext.zoomlevel,
    };
    
    Point zero = {
        .x = 0, 
        .y = 0,
    };
    
    Bitmap screencanvas = shallow_relocated_copy(canvas, zero);

    displaced_by_cursor = safe_crop_bitmap(&screencanvas, cursorpos);
    safe_draw_rectangle_area(&screencanvas, cursorpos, textcolor);
}

void do_blink_cursor() {
    time_t now = clock();
    if (now - last_blinked > CURSOR_BLINK_PERIOD) {
        last_blinked = now;
        cursor_blink_visible_half = !cursor_blink_visible_half;
        if (cursor_blink_visible_half) {
            overlay_cursor_on_canvas(&(main_door.face));
        } else {
            undo_overlay_cursor_on_canvas(&(main_door.face));
        }
        OpenDoor_update(&main_door, true);
    }
}


bool handle_termout(TermOutMessage *msg) {
    bool should_update = true;
    switch (msg->verb) {
        case SetCursorPos:
            SoftTerminal_gotoxy(&maintext, msg->msg.position.x, msg->msg.position.y);
            break;
        case ToggleCursor:
            cursor_enabled = msg->msg.toggle.enabled;
            break;
        case SetColors:
            if (msg->msg.colors.foreground >= 0) maintext.color = msg->msg.colors.foreground;
            if (msg->msg.colors.background >= 0) maintext.background = msg->msg.colors.background;
            should_update = false;
            break;
        case SetScrollRange:
            SoftTerminal_set_scroll_range(&maintext, msg->msg.range.start, msg->msg.range.end);
            should_update = false;
            break;
        case ScrollReverse:
            SoftTerminal_scroll_down(&maintext, 1);
            break;
        case CLS:
            SoftTerminal_clear(&maintext);
            break;
        case Putchar:
            SoftTerminal_putchar(&maintext, msg->msg.character.character);
            break;
        case Puttag:
            //TODO implement
            break;
        case SetWritingDirection:
            SoftTerminal_set_writing_direction(&maintext, msg->msg.writing.direction);
            break;
        case SetTermFlags:
            maintext.flags = msg->msg.flags.flags;
            break;
        case SetZoomLevel:
            maintext.zoomlevel = (msg->msg.zoom.level >= 0 ? msg->msg.zoom.level : 0);
            break;
        default:
            should_update = false;
    }
    return should_update;
}


bool handle_requests() {
    char topicname[4096];
    bool got_char = false;
    bool cursor_moved = false;
    
    
    MQStatus poll_status;
    TermOutMessage msg;
    // stdout
    
    snprintf(topicname, 4096, TERMOUT_OVERRIDE, main_door.door_id);
    do {
        poll_status = poll(topicname, &msg);
        if (poll_status == MQ_SUCCESS) {
            if (!cursor_moved && cursor_enabled) {
                if (cursor_blink_visible_half) undo_overlay_cursor_on_canvas(&(main_door.face));
                cursor_moved = true;
            }
            
            bool cmdstatus = handle_termout(&msg);
            got_char = got_char || cmdstatus;
        }
    } while (poll_status == MQ_SUCCESS);
    
    if (cursor_enabled && cursor_moved && cursor_blink_visible_half) overlay_cursor_on_canvas(&(main_door.face));
    
    
    TermInfo request;
    unsigned int requestor_task_id;
    unsigned int request_id;
    ServStatus accept_status;
    // terminfo
    snprintf(topicname, 4096, TERMINFO_OVERRIDE, main_door.door_id);
    do {
        accept_status = service_accept(topicname, &request, &requestor_task_id, &request_id);
        if (accept_status == SERV_SUCCESS) {
            TermInfo response = {
                .termsize = {
                    .x = 0,
                    .y = 0,
                    .w = maintext.num_cols,
                    .h = maintext.num_rows,
                },
                .screensize = {
                    .x = 0,
                    .y = 0,
                    .w = main_door.face.geom.w - (border_zone*2),
                    .h = main_door.face.geom.h - (border_zone*2)
                },
                .cursor = {
                    .x = maintext.cursor.x,
                    .y = maintext.cursor.y,
                },
                .writing = maintext.writing,
                .cursor_enabled = cursor_enabled,
                .support_bitmap = 1,
                .foreground_color = maintext.color,
                .background_color = maintext.background,
                .scroll_top = maintext.scroll_top,
                .scroll_bottom = maintext.scroll_bottom,
                .flags = maintext.flags | term_ro_flags
            };
            request = response;
            ServStatus cs = service_complete(topicname, request_id);
        }
    } while (accept_status == SERV_SUCCESS);
    
    TermBitmapRequest bmprequest;
    // termbitmap
    snprintf(topicname, 4096, TERMBITMAP_OVERRIDE, main_door.door_id);
    do {
        accept_status = service_accept(topicname, &bmprequest, &requestor_task_id, &request_id);
        if (accept_status == SERV_SUCCESS) {
            
            Bitmap virtual_screen = {
                .geom = {.x = 0, 
                         .y = 0, 
                         .w = main_door.face.geom.w,
                         .h = main_door.face.geom.h},
                .colordata = main_door.face.colordata
            };
            Bitmap image = *(bmprequest.bitmap);
            int* colorkey_ptr = NULL;
            if (bmprequest.colorkey != COLORKEY_NONE) colorkey_ptr = &(bmprequest.colorkey);
            image.geom.x += border_zone;
            image.geom.y += border_zone;
            safe_overlay_bitmap(&virtual_screen, &image, colorkey_ptr);
            
            bmprequest.status = 0;
            ServStatus cs = service_complete(topicname, request_id);
        }
    } while (accept_status == SERV_SUCCESS);
    
    if (got_char) {
        OpenDoor_update(&main_door, true);
    }
    
    return got_char;
}

void quit_keycode_callback(KeyEvent* keycode) {
    if (!keycode->is_pressed) return;
    teardown_services();
    running = false;
}

void keycode_callback(KeyEvent* keycode) {
    if (!keycode->is_pressed) return;
    
    if (keycode->keycode == KEY_F8) {
        switch (maintext.writing) {
            case WD_BOOK:
                SoftTerminal_printf(&maintext, "Switching text direction to WD_RABBI\n");
                SoftTerminal_set_writing_direction(&maintext, WD_RABBI);
                break;
            case WD_RABBI:
                SoftTerminal_printf(&maintext, "Switching text direction to WD_TEGAMI\n");
                SoftTerminal_set_writing_direction(&maintext, WD_TEGAMI);
                break;
            case WD_TEGAMI:
                SoftTerminal_printf(&maintext, "Switching text direction to WD_LEMURIA\n");
                SoftTerminal_set_writing_direction(&maintext, WD_LEMURIA);
                break;
            default:
                SoftTerminal_printf(&maintext, "Switching text direction to WD_BOOK\n");
                SoftTerminal_set_writing_direction(&maintext, WD_BOOK);
                break;
        }
    }
}

static int count_selected(Point select_cur) {
    int result = 0;
    int starty = min(select_cur.y, select_start.y);
    int endy = max(select_cur.y, select_start.y);
    for (int sy=starty; sy<=endy; sy++) {
        int startx = 0;
        int endx = maintext.num_cols-1;
        
        if (select_start.y < select_cur.y) {
            if (sy == starty) startx = select_start.x;
            if (sy == endy) endx = select_cur.x;
        } else {
            if (sy == starty) startx = select_cur.x;
            if (sy == endy) endx = select_start.x;
        }
        
        if (starty == endy) {
            startx = min(select_cur.x, select_start.x);
            endx = max(select_cur.x, select_start.x);
        }
        for (int sx=startx; sx<=endx; sx++) {
            result++;
        }
    }
    return result;
}

void mouse_callback(LibguiMsgMouseEvent *event) {
    if (event->btn_left) {
        Point select_cur = {.x = (event->x - maintext.geom.x) / (CHAR_WIDTH_UNIT << maintext.zoomlevel), 
                            .y = (event->y - maintext.geom.y) / (CHAR_HEIGHT_UNIT << maintext.zoomlevel)};
        if (select_start.x >= 0) {
            bool is_select_forward = (select_cur.y > select_start.y) || ((select_cur.y == select_start.y) && (select_cur.x >= select_start.x));

            selected_amount = count_selected(select_cur) * (is_select_forward ? 1 : -1);
            if ((abs(selected_amount) < abs(old_selected_amount)) || \
                (sgn(selected_amount) != sgn(old_selected_amount))) SoftTerminal_unselect_all(&maintext);
            old_selected_amount = selected_amount;
            
            int starty = min(select_cur.y, select_start.y);
            int endy = max(select_cur.y, select_start.y);
            for (int sy=starty; sy<=endy; sy++) {
                int startx = 0;
                int endx = maintext.num_cols-1;
                
                if (select_start.y < select_cur.y) {
                    if (sy == starty) startx = select_start.x;
                    if (sy == endy) endx = select_cur.x;
                } else {
                    if (sy == starty) startx = select_cur.x;
                    if (sy == endy) endx = select_start.x;
                }
                
                if (starty == endy) {
                    startx = min(select_cur.x, select_start.x);
                    endx = max(select_cur.x, select_start.x);
                }
                for (int sx=startx; sx<=endx; sx++) {
                    SoftTerminal_select_at(&maintext, sx, sy);
                }
            }
            
        } else {
            SoftTerminal_unselect_all(&maintext);
            SoftTerminal_select_at(&maintext, select_cur.x, select_cur.y);
            select_start = select_cur;
            selected_amount = 1;
            old_selected_amount = 0;
        }
    } else {
        select_start.x = -1;
    }
}

void transform_callback(Rect* transformed, Rect* before_transformed) {
    Bitmap newface;
    
    cursor_blink_enabled = main_door.has_focus;
    
    if (transformed->w == before_transformed->w && transformed->h == before_transformed->h) return;
    
    size_x = transformed->w;
    size_y = transformed->h;
    
    newface = make_bitmap(*transformed, bgcolor);
    Rect crop_area = {
        .x = 0, .y = 0,
        .w = min(transformed->w, before_transformed->w),
        .h = min(transformed->h, before_transformed->h)
    };
    Bitmap cropped = crop_bitmap(&(main_door.face), crop_area);
    overlay_bitmap(&newface, &cropped, NULL);
    destroy_bitmap(&cropped);
    destroy_bitmap(&(main_door.face));
    
    memcpy(&(main_door.face), &newface, sizeof(Bitmap));
    
    Rect maintext_pos = {.x = border_zone, 
                         .y = border_zone, 
                         .w = transformed->w - (border_zone*2), 
                         .h = transformed->h - (border_zone*2)};
    SoftTerminal_resize(&maintext, maintext_pos);
    
    undo_overlay_cursor_on_canvas(&(main_door.face));
    displaced_by_cursor.colordata = NULL;
    
    OpenDoor_update(&main_door, true);
}

void print_help() {
    printf("Soft terminal door for the DOMUS environment.\n");
    printf("By default, the %s shell will be run at startup.\n", default_program);
    printf("Parameters:\n");
    printf("    program     alternative program to run at startup, instead of the.\n");
    printf("                default shell.\n");
    printf("    x           starting x-coordinates of the door.\n");
    printf("    y           starting y-coordinates of the door.\n");
    printf("    width       starting width of the door.\n");
    printf("    height      starting height of the door.\n");
    printf("    border      (integer) border width of the door.\n");
    printf("Keyboard controls:\n");
    printf("    F8          toggle the terminal's writing direction.\n");
}

int main(void) {
    BitmapFont font;
    srand(time(NULL));
    int return_code;
    bool returned = false;
    
    unsetenv("quiet");  //TODO kinda kludgy
    
    displaced_by_cursor.colordata = NULL;
    last_blinked = clock();
    
    textcolor = COLOR24_TEXT;
    bgcolor = COLOR24_WIN_BGROUND;
    
    if (getenv("help") != NULL) {
        print_help();
        return 0;
    }
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    char *program;
    char *program_arg = getenv("program");
    if (program_arg == NULL) {
        program = default_program;
    } else {
        program = strdup(program_arg);
        unsetenv("program");
    }
    
    if (getenv("border") != NULL) {
        border_zone = atoi(getenv("border"));
        unsetenv("border");
    }

    if (getenv("width") != NULL) {
        size_x = (atoi(getenv("width")) - border_zone*2);
        unsetenv("width");
    }
    if (getenv("height") != NULL) {
        size_y = (atoi(getenv("height")) - border_zone*2);
        unsetenv("height");
    }
    
    TermInfo ti = getterminfo();
    
    int start_x = rand() % (ti.screensize.w - (size_x + border_zone*2));
    int start_y = rand() % (ti.screensize.h - (size_y + border_zone*2));
    
    if (getenv("x") != NULL) {
        start_x = atoi(getenv("x"));
        unsetenv("x");
    }
    if (getenv("y") != NULL) {
        start_y = atoi(getenv("y"));
        unsetenv("y");
    }
    
    Rect main_area = {
        .x = start_x,
        .y = start_y,
        .w = (size_x + border_zone*2),
        .h = (size_y + border_zone*2),
    };
    bool open_success = open_door_advanced(&main_door, main_area, "Mofuterm", 1, bgcolor, COLOR24_WIN_BORDER, NULL, 0, false);
    if (!open_success) {
        printf("Mofuterm: error opening main door (%d)\n", main_door.status);
        return 2;
    }
    
    main_door.transform_callback = &transform_callback;
    
    prepare_services();
    
    Rect maintext_pos = {.x = border_zone, 
                         .y = border_zone, 
                         .w = main_area.w - (border_zone*2), 
                         .h = main_area.h - (border_zone*2)};
    maintext = SoftTerminal_init(&(main_door.face), maintext_pos, font, textcolor, bgcolor, 0, true);
    
    main_door.keycode_callback = &keycode_callback;
    main_door.mouse_callback = &mouse_callback;
    
    set_overrides();
    int task_id = epspawn_app_checkreturn(program, &return_code);
    unset_overrides();
    
    if (task_id < 0) return_code = task_id;
    
    SoftTerminal_printf(&maintext, "Executing %s (task id %d)", program, task_id);
    if (search_buildfile(program) == program) {  // not a JIT executable
        printf("\n");
    }  // otherwise, don't print a newline (looks nicer)
    
    OpenDoor_update(&main_door, true);
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        if (return_code != NOT_DONE_YET && return_code != READY_TO_RUN && !returned) {
            returned = true;
            SoftTerminal_printf(&maintext, "\nShell returned %d\n", return_code);
            main_door.keycode_callback = &quit_keycode_callback;
        }
        if (cursor_blink_enabled) do_blink_cursor();
        if (!handle_requests()) yield();
    }
    
    OpenDoor_close(&main_door);
    destroy_bitmap_font(font);
    
    return 0;
}
