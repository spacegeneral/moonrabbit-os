#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <collections/array.h>
#include <sys/proc.h>
#include <sys/serv.h>
#include <sys/mq.h>
#include <sys/fs.h>
#include <sys/errno.h>

#include <sys/happy.h>

#include "fatfs/ff.h"         /* Declarations of sector size */
#include "fatfs/diskio.h"     /* Declarations of disk functions */

#define DRV_NAME "fat"
#define MAXPATH 4096
#define SECTOR_SIZE 512
#define DIRENT_BLOCK 100

static char disk_path[MAXPATH];

FILE *disk_file = NULL;
FATFS *work_area;

static char mount_path[MAXPATH];
static char driver_topic[4096];
int driver_instance;

static bool debug;
static bool running = true;
static Array *open_files;

ProcIdent identity;

PhisicalDomainClass mount_race;
DataDomainClass mount_class;

unsigned int requestor_task_id;


typedef struct OpenFile {
    unsigned outer;
    FIL* inner;
    unsigned task_id;
    char filename[MAXPATH];
} OpenFile;



/*
    FatFs interface
 */

DSTATUS disk_status (
    BYTE pdrv     /* [IN] Physical drive number */
) {
    if (disk_file == NULL) return STA_NOINIT;
    return 0;
}

DSTATUS disk_initialize (
  BYTE pdrv           /* [IN] Physical drive number */
) {
    disk_file = fopen(disk_path, "rb+");
    return disk_status(pdrv);
}

/*
 Currnet local time shall be returned as bit-fields packed into a DWORD value. The bit fields are as follows:

bit31:25
    Year origin from the 1980 (0..127, e.g. 37 for 2017)
bit24:21
    Month (1..12)
bit20:16
    Day of the month (1..31)
bit15:11
    Hour (0..23)
bit10:5
    Minute (0..59)
bit4:0
    Second / 2 (0..29, e.g. 25 for 50) 
 */
DWORD get_fattime (void) {
    time_t currtime;
    struct tm local_time;
    currtime = time(NULL);
    localtime_r(&currtime, &local_time);
    
    return ((local_time.tm_sec / 2)    & 0b00000000000000000000000000011111 ) \
         | ((local_time.tm_min  << 5 ) & 0b00000000000000000000011111100000 ) \
         | ((local_time.tm_hour << 11) & 0b00000000000000001111100000000000 ) \
         | ((local_time.tm_mday << 16) & 0b00000000000111110000000000000000 ) \
         | ((local_time.tm_mon  << 21) & 0b00000001111000000000000000000000 ) \
         | (((local_time.tm_year - 1980 + 1900) << 25) & 0b11111110000000000000000000000000 ) \
         ;
}

time_t fattime_to_system_time(WORD time, WORD date) {
    struct tm tmptime;
    tmptime.tm_sec =  (time &            0b11111) * 2;
    tmptime.tm_min =  (time &      0b11111100000) >> 5;
    tmptime.tm_hour = (time & 0b1111100000000000) >> 11;
    tmptime.tm_mday = (date &            0b11111);
    tmptime.tm_mon =  ((date &        0b111100000) >> 5) - 1;
    tmptime.tm_year = ((date & 0b1111111000000000) >> 9) + 1980 - 1900;
    return mktime(&tmptime);
}

DRESULT disk_ioctl (
  BYTE pdrv,     /* [IN] Drive number */
  BYTE cmd,      /* [IN] Control command code */
  void* buff     /* [I/O] Parameter and data buffer */
) {
    switch (cmd) {
        case CTRL_SYNC:
            // nothing
        break;
        case GET_SECTOR_COUNT:
            {
                long save_seek = ftell(disk_file);
                fseek(disk_file, 0, SEEK_END);
                long filesize = ftell(disk_file);
                fseek(disk_file, save_seek, SEEK_SET);
                
                DWORD *result = (DWORD*) buff;
                *result = filesize / SECTOR_SIZE;
            }
        break;
        case GET_SECTOR_SIZE:
            {
                DWORD *result = (DWORD*) buff;
                *result = SECTOR_SIZE;
            }
        break;
        case GET_BLOCK_SIZE:
            {
                DWORD *result = (DWORD*) buff;
                *result = 1;
            }
        break;
        case CTRL_TRIM:
            // nothing
        break;
        default:
            return RES_PARERR;
    }
    return 0;
}

DRESULT disk_read (
  BYTE pdrv,     /* [IN] Physical drive number */
  BYTE* buff,    /* [OUT] Pointer to the read data buffer */
  DWORD sector,  /* [IN] Start sector number */
  UINT count     /* [IN] Number of sectros to read */
) {
    fseek(disk_file, sector * SECTOR_SIZE, SEEK_SET);
    size_t nread = fread(buff, SECTOR_SIZE, count, disk_file);
    if (nread != count) return RES_ERROR;
    return 0;
}

DRESULT disk_write (
  BYTE pdrv,        /* [IN] Physical drive number */
  const BYTE* buff, /* [IN] Pointer to the data to be written */
  DWORD sector,     /* [IN] Sector number to write from */
  UINT count        /* [IN] Number of sectors to write */
) {
    fseek(disk_file, sector * SECTOR_SIZE, SEEK_SET);
    size_t nwrite = fwrite(buff, SECTOR_SIZE, count, disk_file);
    if (nwrite != count) return RES_ERROR;
    return 0;
}

/*
 end of fatfs interface
 */


void translate_path(const char *original, char *translated) {
    size_t copied = 0;
    size_t orig_offset = 0;
    
    translated[0] = '\0';
    
    while (original[orig_offset] && copied < MAXPATH) {
        char currchar = original[orig_offset];
        if (currchar != mount_path[orig_offset]) {
            translated[copied] = (currchar == ',' ? '/' : currchar);
            copied++;
        }
        orig_offset++;
    }
    
    if (translated[0] == '\0') {
        translated[0] = '/';
        copied++;
    }
    
    translated[copied] = '\0';
}

#define CONTAINS(string, character) (strchr((string), (character)) != NULL)

int translate_mode(const char *unix_mode) {
    int modeint = 0;
    if (CONTAINS(unix_mode, 'r')) {
        modeint |= FA_READ;
        if (CONTAINS(unix_mode, '+')) modeint |= FA_WRITE;
        return modeint;
    } else if (CONTAINS(unix_mode, 'a')) {
        modeint |= FA_OPEN_APPEND | FA_WRITE;
        if (CONTAINS(unix_mode, '+')) modeint |= FA_READ;
        return modeint;
    } else if (CONTAINS(unix_mode, 'w')) {
        modeint |= FA_WRITE;
        if (CONTAINS(unix_mode, '+')) modeint |= FA_READ;
        if (CONTAINS(unix_mode, 'x')) {
            modeint |= FA_CREATE_NEW;
        } else {
            modeint |= FA_CREATE_ALWAYS;
        }
        return modeint;
    }
    return modeint;
}


int translate_fseek(FIL *stream, long offset, int whence) {
    long long file_size = f_size(stream);
    long curr_seek = f_tell(stream);
    if (whence == SEEK_SET) {
        if (offset < file_size) {
            f_lseek(stream, offset);
            return 0;
        }
    } else if (whence == SEEK_CUR) {
        if (curr_seek + offset < file_size) {
            f_lseek(stream, offset + curr_seek);
            return 0;
        }
    } else if (whence == SEEK_END) {
        if (file_size - offset >= 0) {
            f_lseek(stream, file_size - offset);
            return 0;
        }
    }
    return -1;
}



OpenFile* get_open_file_by_external_ref(unsigned external) {
    ARRAY_FOREACH(curr, open_files, {
        OpenFile *current = (OpenFile*) curr;
        if (current->outer == external) {
            return current;
        }
    });
    return NULL;
}

bool close_one_file_with_task_id(unsigned int task_id) {
    ARRAY_FOREACH(curr, open_files, {
        OpenFile *current = (OpenFile*) curr;
        if (current->task_id == task_id) {
            array_remove(open_files, current, NULL);
            return true;
        }
    });
    return false;
}

void close_all_files_by_task_id(unsigned int task_id) {
    bool file_found;
    do {
        file_found = close_one_file_with_task_id(task_id);
    } while (file_found);
}



void handle_fs_request(FSServiceMessage *request) {
    char translated_path[MAXPATH];
    
    if (disk_file == NULL && request->service_name != fsservice_setmount) {
        request->status = NOT_MOUNTED;
        return;
    }
    
    //if (debug) debug_printf("Fatdrv: handling request %d for app %d\r\n", request->service_name, requestor_task_id);
    
    switch (request->service_name) {
        case fsservice_create_file:
            {
                FIL fil;
                translate_path(request->msg.create_file.pathname, translated_path);
                int result = f_open(&fil, translated_path, FA_OPEN_ALWAYS);
                if (result) request->status = CANNOT_CREATE_FILE;
                else {
                    f_close(&fil);
                    request->status = 0;
                }
            }
            break;
        case fsservice_create_directory:
            {
                translate_path(request->msg.create_directory.pathname, translated_path);
                if (f_mkdir(translated_path)) request->status = CANNOT_CREATE_DIR;
                else request->status = 0;
            }
            break;
        case fsservice_create_link:
            {
                request->status = NOT_IMPLEMENTED;
            }
            break;
        case fsservice_remove:
            {
                translate_path(request->msg.remove.pathname, translated_path);
                if (debug) debug_printf("Fatdrv: removing file %s\r\n", translated_path);
                
                if (f_unlink(translated_path)) request->status = CANNOT_REMOVE;
                else request->status = 0;
            }
            break;
        case fsservice_count_directory_entries:
            {
                DIR dir;
                translate_path(request->msg.count_directory_entries.pathname, translated_path);
                if (debug) debug_printf("fatdrv listing %s (orig. %s)\r\n", translated_path, request->msg.count_directory_entries.pathname);
                
                if (f_opendir(&dir, translated_path)) {
                    request->status = NOT_A_DIRECTORY;
                } else {
                    static FILINFO fno;
                    size_t nfiles = 0;
                    
                    for (;;) {
                        int err = f_readdir(&dir, &fno);
                        if (err != FR_OK || fno.fname[0] == 0) break;
                        
                        if (strcmp(fno.fname, ".") == 0) continue;
                        if (strcmp(fno.fname, "..") == 0) continue;
                        
                        nfiles++;
                    }
 
                    f_closedir(&dir);
                    
                    request->msg.count_directory_entries.numentries = nfiles;
                    request->status = 0;
                }
            }
            break;
        case fsservice_get_directory_entry_at:
            {
                DIR dir;
                translate_path(request->msg.get_directory_entry_at.pathname, translated_path);
                if (debug) debug_printf("fatdrv listing %s (orig. %s)\r\n", translated_path, request->msg.get_directory_entry_at.pathname);
                
                if (f_opendir(&dir, translated_path)) {
                    request->status = NOT_A_DIRECTORY;
                } else {
                    static FILINFO fno;
                    size_t curr_filenum = 0;
                    bool found = false;
                    
                    for (;;) {
                        int err = f_readdir(&dir, &fno);
                        if (err != FR_OK || fno.fname[0] == 0) break;
                        
                        if (strcmp(fno.fname, ".") == 0) continue;
                        if (strcmp(fno.fname, "..") == 0) continue;
                        
                        if (curr_filenum == request->msg.get_directory_entry_at.position) {
                            DirEnt* entry = &(request->msg.get_directory_entry_at.entry);
                            
                            strncpy(entry->name, fno.fname, MAX_FILENAME_LENGTH);
                            entry->metadata.race_attrib = mount_race;
                            entry->metadata.class_attrib = mount_class;
                            entry->metadata.writable = !(fno.fattrib & AM_RDO);
                            entry->metadata.readable = true;
                            entry->metadata.executable = true;
                            time_t file_time = fattime_to_system_time(fno.ftime, fno.fdate);
                            entry->metadata.creation_time = file_time;
                            entry->metadata.modification_time = file_time;
                            if (fno.fattrib & AM_DIR) entry->filetype = Directory;
                            else entry->filetype = File;
                            entry->filesize = fno.fsize;
                            found = true;
                            break;
                        }
                        
                        curr_filenum++;
                    }
                    
                    f_closedir(&dir);
                    
                    request->status = 0;
                    if (!found) {
                        request->status = CANNOT_LIST_DIRECTORY;
                    }
                }
            }
            break;
        case fsservice_file_exists:
            {
                FILINFO fnfo;
                translate_path(request->msg.create_file.pathname, translated_path);
                
                if (debug) debug_printf("Fatdrv: file %s exists ?\r\n", translated_path);
                
                request->msg.file_exists.result = (f_stat(translated_path, &fnfo) == FR_OK);
                request->status = 0;
            }
            break;
        case fsservice_move:
            {
                char translated_path_to[MAXPATH];
                translate_path(request->msg.move.path_from, translated_path);
                translate_path(request->msg.move.path_to, translated_path_to);
                int result = f_rename(translated_path, translated_path_to);
                if (result) request->status = INTERNAL_FS_ERROR;
                else request->status = 0;
            }
            break;
        case fsservice_fopen:
            {
                //TODO: improve security
                //TODO: avoid double open
                translate_path(request->msg.fopen.pathname, translated_path);
                if (debug) debug_printf("open %s\r\n", translated_path);
                int mode = translate_mode(request->msg.fopen.mode);
                
                FIL *fil = malloc(sizeof(FIL));
                int error = f_open(fil, translated_path, mode);
                
                if (error) {
                    free(fil);
                    request->status = FILE_DOES_NOT_EXIST;
                    if (debug) debug_printf("fatdrv: fopen(%s, %s) error\r\n", translated_path, request->msg.fopen.mode);
                }
                else {
                    FILE *newfile = &(request->msg.fopen.result);
                    newfile->reserved = (void*) rand();
                    newfile->seek_pos = 0;
                    snprintf(newfile->driver, MAX_FS_DRIVER_NAME_LENGTH, "%s%d", DRV_NAME, driver_instance);
                    
                    OpenFile* file_tracking = (OpenFile*) malloc(sizeof(OpenFile));
                    file_tracking->outer = (unsigned) (newfile->reserved);
                    file_tracking->inner = fil;
                    file_tracking->task_id = requestor_task_id;
                    strncpy(file_tracking->filename, translated_path, MAXPATH);
                    
                    array_add(open_files, file_tracking);
                    
                    if (debug) debug_printf("fatdrv: %u = fopen(%s, %s) ok\r\n", (unsigned) (newfile->reserved), translated_path, request->msg.fopen.mode);
                    request->status = 0;
                }
                //TODO transfer memory
            }
            break;
        case fsservice_fclose:
            {
                FILE *fp = &(request->msg.fclose.stream);
                unsigned outer = (unsigned) (fp->reserved);
                OpenFile* file_tracking = get_open_file_by_external_ref(outer);
                if (file_tracking == NULL) {
                    if (debug) debug_printf("fatdrv: file %u is not open\r\n", outer);
                    request->status = EOF;
                } else if (file_tracking->task_id != requestor_task_id) {
                    if (debug) debug_printf("fatdrv: file %u is opened by other task\r\n", outer);
                    request->status = SECURITY_DENIED;
                } else {
                    if (debug) debug_printf("fatdrv: closing file %u ... ", outer);
                    int error = f_close(file_tracking->inner);
                    array_remove(open_files, file_tracking, NULL);
                    free(file_tracking->inner);
                    free(file_tracking);
                    if (error) {
                        if (debug) debug_printf("fatdrv: error closing file %u\r\n", outer);
                        request->status = INTERNAL_FS_ERROR;
                    }
                    else request->status = 0;
                    if (debug) debug_printf("fatdrv: closed file %u\r\n", outer);
                }
            }
            break;
        case fsservice_fflush:
            {
                FILE *fp = &(request->msg.fflush.stream);
                unsigned outer = (unsigned) (fp->reserved);
                OpenFile* file_tracking = get_open_file_by_external_ref(outer);
                if (file_tracking == NULL) {
                    request->status = EOF;
                } else if (file_tracking->task_id != requestor_task_id) {
                    if (debug) debug_printf("fatdrv: file %u is opened by other task\r\n", outer);
                    request->status = SECURITY_DENIED;
                } else {
                    int error = f_sync(file_tracking->inner);
                    if (error) request->status = INTERNAL_FS_ERROR;
                    else request->status = 0;
                }
            }
            break;
        case fsservice_fread:
            {
                FILE *fp = &(request->msg.fread.stream);
                unsigned outer = (unsigned) (fp->reserved);
                if (request->msg.fread.size == 0 || request->msg.fread.nmemb == 0) {
                    request->status = 0;
                    request->msg.fread.result = 0;
                } else {
                    OpenFile* file_tracking = get_open_file_by_external_ref(outer);
                    if (file_tracking == NULL) {
                        request->msg.fread.result = 0;
                        request->status = INTERNAL_FS_ERROR;
                        if (debug) debug_printf("fatdrv: unknown file %u\r\n", outer);
                    } else if (file_tracking->task_id != requestor_task_id) {
                        if (debug) debug_printf("fatdrv: file %u is opened by other task\r\n", outer);
                        request->status = SECURITY_DENIED;
                    } else {
                        unsigned nread;
                        int error = f_read(file_tracking->inner, request->msg.fread.payload, request->msg.fread.size * request->msg.fread.nmemb, &nread);
                        if (debug) debug_printf("fatdrv: %u = fread(buf, %lu, %lu, %u) (code: %d)\r\n", nread, request->msg.fread.size, request->msg.fread.nmemb, outer, error);
                        request->msg.fread.result = nread / request->msg.fread.size;
                        fp->seek_pos = f_tell(file_tracking->inner);
                        if (error) request->status = INTERNAL_FS_ERROR;
                        else request->status = 0;
                    }
                }
            }
            break;
        case fsservice_fwrite:
            {
                FILE *fp = &(request->msg.fwrite.stream);
                unsigned outer = (unsigned) (fp->reserved);
                if (request->msg.fwrite.size == 0 || request->msg.fwrite.nmemb == 0) {
                    request->status = 0;
                    request->msg.fwrite.result = 0;
                } else {
                    OpenFile* file_tracking = get_open_file_by_external_ref(outer);
                    if (file_tracking == NULL) {
                        request->msg.fwrite.result = 0;
                        request->status = INTERNAL_FS_ERROR;
                    } else if (file_tracking->task_id != requestor_task_id) {
                        if (debug) debug_printf("fatdrv: file %u is opened by other task\r\n", outer);
                        request->status = SECURITY_DENIED;
                    } else {
                        unsigned nwrite;
                        int error = f_write(file_tracking->inner, request->msg.fwrite.payload, request->msg.fwrite.size * request->msg.fwrite.nmemb, &nwrite);
                        request->msg.fwrite.result = nwrite / request->msg.fwrite.size;
                        fp->seek_pos = f_tell(file_tracking->inner);
                        if (error) request->status = INTERNAL_FS_ERROR;
                        else request->status = 0;
                    }
                }
            }
            break;
        case fsservice_fseek:
            {
                FILE *fp = &(request->msg.fseek.stream);
                unsigned outer = (unsigned) (fp->reserved);
                OpenFile* file_tracking = get_open_file_by_external_ref(outer);
                if (file_tracking == NULL) {
                    request->status = -1;
                } else if (file_tracking->task_id != requestor_task_id) {
                    if (debug) debug_printf("fatdrv: file %u is opened by other task\r\n", outer);
                    request->status = SECURITY_DENIED;
                } else {
                    int status = translate_fseek(file_tracking->inner, request->msg.fseek.offset, request->msg.fseek.whence);
                    fp->seek_pos = f_tell(file_tracking->inner);
                    request->status = status;
                }
            }
            break;
        case fsservice_setmount:
            {
                strncpy(disk_path, request->msg.setmount.diskfile, MAXPATH);
                strncpy(mount_path, request->msg.setmount.mountpoint, MAXPATH);
                request->status = 0;
                //printf("fatdrv mounted disk %s in location %s\n", disk_path, mount_path);
                
                if (disk_file != NULL) {
                    request->status = ALREADY_MOUNTED;
                }
                else {
                    disk_file = fopen(disk_path, "rb+");
                    if (disk_file == NULL) {
                        if (debug) debug_printf("%s: Failed to open %s\r\n", DRV_NAME, disk_path);
                        request->status = INTERNAL_FS_ERROR;
                    } else {
                        int mount_error = f_mount(work_area, "", 0);
                        if (mount_error) {
                            if (debug) debug_printf("Error %d setting fat mountpoint %s\r\n", mount_error, disk_path);
                            request->status = INTERNAL_FS_ERROR;
                        } else {
                            if (debug) debug_printf("Success setting fat mountpoint %s\r\n", disk_path);
                        }
                    }
                }
            }
            break;
        case fsservice_unsetmount:
            {
                if (disk_file == NULL) {
                    request->status = NOT_MOUNTED;
                } else {
                    if (array_size(open_files) > 0) {
                        request->status = CANNOT_UNMOUNT;
                    } else {
                        fclose(disk_file);
                        disk_file = NULL;
                        request->status = 0;
                    }
                }
            }
            break;
        case fsservice_chsec:
            request->status = NOT_IMPLEMENTED;
            break;
    }
    
    if (debug) debug_printf("Fatdrv: ---- num open files %lu\r\n", array_size(open_files));
}

int main(void) {
    debug = (getenv("debug") != NULL); // debug=true;
    
    srand(time(NULL));
    
    char *instancenum = getenv("instance");
    driver_instance = 0;
    if (instancenum != NULL) {
        driver_instance = atoi(instancenum);
    }
    snprintf(driver_topic, 4096, "/sys/fs/%s%d", DRV_NAME, driver_instance);
    
    identity = ident();
    mount_race = identity.phys_race_attrib;
    mount_class = identity.data_class_attrib;
    
    if (getenv("mount_race") != NULL) {
        mount_race = least_safe(parse_race(getenv("mount_race")), identity.phys_race_attrib);
        if (debug) {
            debug_printf("Fatdrv: mount_race=%s (%d)\r\n", getenv("mount_race"), mount_race);
        }
    }
    if (getenv("mount_class") != NULL) {
        mount_class = least_safe(parse_class(getenv("mount_class")), identity.phys_race_attrib);
        if (debug) {
            debug_printf("Fatdrv: mount_class=%s (%d)\r\n", getenv("mount_class"), mount_class);
        }
    }
    
    ServStatus serv_creation_status = advertise_service_advanced(driver_topic, sizeof(FSServiceMessage), mount_race, mount_class);
    if (serv_creation_status != SERV_SUCCESS) {
        printf("fatdrv Error %d cannot create service %s\n", serv_creation_status, driver_topic);
        return 1;
    }
    
    subscribe(DEFAULT_GRAVEYARD_TOPIC);
    
    array_new(&open_files);
    
    work_area = malloc(sizeof(FATFS));
    
    FSServiceMessage req;
    ServStatus accept_status, complete_status;
    MQStatus graveyard_status;
    
    unsigned int request_id;
    
    unsigned int dead_task;
    
    while (running) {
        graveyard_status = poll(DEFAULT_GRAVEYARD_TOPIC, &dead_task);
        if (graveyard_status == MQ_SUCCESS) {
            if (debug) debug_printf("Closing all files belonging to defunct task %d\r\n", dead_task);
            close_all_files_by_task_id(dead_task);
        }
        
        accept_status = service_accept(driver_topic, (void*)&req, &requestor_task_id, &request_id);
        if (accept_status == SERV_SUCCESS) {

            handle_fs_request(&req);
        
            time_t serv_started = clock_ms();
            complete_status = service_complete(driver_topic, request_id);
            long delta_t = (long)(clock_ms() - serv_started);
            happy(errf_lin_pos(delta_t, 300));
            
            if (complete_status != SERV_SUCCESS) {
                debug_printf("fatdrv service completion error: %d\n", complete_status);
            }
        } else {
            happy(0.0);
        }
        
        yield();
    }
    
    fclose(disk_file);
    
    return 0;
}
