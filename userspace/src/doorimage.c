#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgui/libgui.h>
#include <sys/proc.h>
#include <sys/term.h>
#include <sys/keycodes.h>


SoftTerminal info;
OpenDoor main_door;
bool running = true;


void keycode_callback(KeyEvent *keycode) {
    switch (keycode->keycode) {
        case KEY_Escape:
            running = false;
            break;
    }
}


int main(void) {
    char *imgname = getenv("file");
    if (imgname == NULL) {
        printf("Image file not specified.\n");
        exit(1);
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = 0,
        .y = 0,
        .w = 10,
        .h = 10,
    };
    bool open_success = open_main_door_advanced(&main_door, main_area, "Image viewer", 0, 0, NULL, false);
    if (!open_success) {
        printf("Error opening main door\n");
        return 2;
    }
    
    main_door.keycode_callback = &keycode_callback;
    
    destroy_bitmap(&(main_door.face));
    
    int error;
    float scale = 1.0;
    if (getenv("scale") != NULL) {
        scale = atof(getenv("scale")); //TODO fix atof
        debug_printf("%s = Image scale: %f TODO fix atof\r\n", getenv("scale"), scale);
    }
    
    error = load_any_image(&(main_door.face), imgname, scale);
    
    if (error) {
        printf("Error %d loading image\n", error);
        running = false;
    } else {
        float ratio = ((float) main_door.face.geom.w) / ((float) main_door.face.geom.h);
        
        if ((getenv("width") != NULL) && (getenv("height") != NULL)) {
            destroy_bitmap(&(main_door.face));
            error = load_any_image_with_size(&(main_door.face), imgname, atoi(getenv("width")), atoi(getenv("height")));
        }
        else if (getenv("width") != NULL) {
            destroy_bitmap(&(main_door.face));
            int auto_h = (int)(((float)atoi(getenv("width"))) / ratio);
            error = load_any_image_with_size(&(main_door.face), imgname, atoi(getenv("width")), auto_h);
        }
        else if (getenv("height") != NULL) {
            destroy_bitmap(&(main_door.face));
            int auto_w = (int)(((float)atoi(getenv("height"))) * ratio);
            error = load_any_image_with_size(&(main_door.face), imgname, auto_w, atoi(getenv("height")));
        }
        
        if (error) {
            printf("Error %d loading image\n", error);
            running = false;
        } else {
            OpenDoor_update(&main_door, true);
        }
    }
    
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        yield();
    }
    
    OpenDoor_close(&main_door);
    
    return 0;
}
