#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <yakumo.h>

#define PAGE_SIZE 4096

size_t fit_into_pages(size_t bytes) {
    if (bytes % PAGE_SIZE == 0) {
        return bytes / PAGE_SIZE;
    } else {
        return (bytes / PAGE_SIZE) + 1;
    }
}

typedef struct Memstats {
    unsigned int free;
    unsigned int total_available;
} Memstats;


int main(void) {
    long memsize = 1024*1024*128;  // 128 MB
    if (getenv("memsize") != NULL) {
        memsize = atol(getenv("memsize"));
    }
    
    long maxchunk = 32768;
    if (getenv("maxchunk") != NULL) {
        maxchunk = atol(getenv("maxchunk"));
    }
    
    FILE *fp = fopen("f,sys,dial,robot,memstats","rb");
    if (fp == NULL) {
        printf("Cannot open f,sys,dial,robot,memstats\n");
    } else {
        Memstats stats;
        if (fread(&stats, sizeof(Memstats), 1, fp) == 1) {
            printf("Available memory %u bytes (free %u bytes)\n", stats.total_available, stats.free);
        }
        fclose(fp);
    }
    
    printf("Allocation benchmark, %ld bytes total (%ld bytes max chunks).\n", memsize, maxchunk);
    
    long mem_left = memsize;
    long actual_mem = 0;
    clock_t started = clock();
    
    while (mem_left > 0) {
        long amount = (rand() % (maxchunk-1)) + 1;
        void *area = malloc(amount);
        if (area == NULL) break;
        mem_left -= amount;
        actual_mem += fit_into_pages(amount) * PAGE_SIZE;
    }
    
    if (mem_left > 0) {
        printf("Error allocating memory.\n");
    }
    
    mem_left = max(0, mem_left);
    
    clock_t finished = clock();
    
    clock_t ms_passed = ((finished - started) * 1000) / CLOCKS_PER_SEC;
    printf("Allocated %ld (%ld) bytes in %f seconds.\n", memsize - mem_left, actual_mem, ((double)ms_passed) / 1000.0);
    
    return 0;
}
 
