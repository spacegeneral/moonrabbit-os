#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <libgui/libgui.h>
#include <libgui/obj_parser.h>
#include <sys/proc.h>
#include <sys/term.h>
#include <sys/mq.h>
#include <sys/keycodes.h>
#include <math.h>
#include <yakumo.h>
#include <sys/happy.h>
#include <time.h>


bool running = true;
OpenDoor main_door;
int width = 500, height = 500;


int linecolor;
int bgroundcolor;


static bool objviewer_mouse_hold = false;
LibguiMsgMouseEvent objviewer_prev_event;
LibguiMsgMouseEvent objviewer_hold_start;

double rei = 0.0;
double objviewer_drag_coeff;

Vertex g;
Vertex scale3d;
Vertex center3d;

double scaling;  // compensate window resize
double zoom_factor = 10.0;  // user-controllable


tinyobj_attrib_t attrib;
tinyobj_shape_t* shapes = NULL;
size_t num_shapes;
tinyobj_material_t* materials = NULL;
size_t num_materials;

Vertex* vertices;
size_t num_vertices;




void objviewer_mouse_callback(LibguiMsgMouseEvent *event) {
    if (event->btn_left && !objviewer_prev_event.btn_left) {
        objviewer_hold_start = *event;
    } else if (event->btn_left && objviewer_prev_event.btn_left) {
        objviewer_mouse_hold = true;
        objviewer_drag_coeff = rei - M_PI * ((double) (objviewer_hold_start.x - event->x)) * 0.004;
    } else if (!event->btn_left && objviewer_prev_event.btn_left) {
        objviewer_mouse_hold = false;
        rei = objviewer_drag_coeff;
    }
    
    objviewer_prev_event = *event;
}


void recompute_viewing_params() {
    scale3d.x = zoom_factor * scaling;
    scale3d.y = zoom_factor * scaling;
    scale3d.z = zoom_factor * scaling;
    // center of gravity of the model
    g = vertex_invert(multi_vertex_weight_center(vertices, num_vertices));
    g = vertex_scale(g, scale3d);
}


void keycode_callback(KeyEvent *keycode) {
    if (!keycode->is_pressed) return;
    switch (keycode->keycode) {
        case KEY_Escape:
            running = false;
            break;
        case KEY_A:
            zoom_factor += 1.0;
            recompute_viewing_params();
            break;
        case KEY_Z:
            if (zoom_factor-1.0 > 0.0) zoom_factor -= 1.0;
            recompute_viewing_params();
            break;
        case KEY_Up:
            center3d.y += 10.0;
            break;
        case KEY_Down:
            center3d.y -= 10.0;
            break;
        case KEY_Right:
            center3d.x -= 10.0;
            break;
        case KEY_Left:
            center3d.x += 10.0;
            break;
    }
}


#define VERTEX_AT(rel_index) vertices[face[rel_index].v_idx]

void draw_faces(Vertex* vertices, tinyobj_vertex_index_t* face_list, size_t num_triangles) {
    for (size_t nf=0; nf<num_triangles; nf++) {
        tinyobj_vertex_index_t* face = face_list + nf*3;
        Vertex vert_a = VERTEX_AT(0);
        Vertex vert_b = VERTEX_AT(1);
        Vertex vert_c = VERTEX_AT(2);
        double u_x = vert_b.x - vert_a.x;
        double u_y = vert_b.y - vert_a.y;
        double v_x = vert_c.x - vert_a.x;
        double v_y = vert_c.y - vert_a.y;
        double normal_z = u_x * v_y - u_y * v_x;
        if (normal_z > 0) continue;
        Point point_a = vertex_project_ortho(vert_a);
        Point point_b = vertex_project_ortho(vert_b);
        Point point_c = vertex_project_ortho(vert_c);
        safe_draw_line(&(main_door.face), point_a, point_b, linecolor);
        safe_draw_line(&(main_door.face), point_b, point_c, linecolor);
        safe_draw_line(&(main_door.face), point_c, point_a, linecolor);
    }
}

#undef VERTEX_AT


int load_full_file_into_mem(char* fname, char** data, size_t* data_len) {
    FILE *fp;
    
    fp = fopen(fname, "r");

    if (fp != NULL) {
        fseek(fp, 0, SEEK_END);
        long filesize = ftell(fp);
        *data_len = (size_t) filesize;
        fseek(fp, 0, SEEK_SET);
        
        *data = (char*) malloc(sizeof(char) * filesize);
        fread(*data, 1, filesize, fp);
        fclose(fp);
        
        return 0;
    }
    
    return 1;
}


void print_help() {
    printf("Display a 3D wireframe from a given wavefront obj file.\n");
    printf("This program is a door-applet; it requires an instance of DOMUS to\n");
    printf("be running in order to work properly.\n");
    printf("The 3D model is displayed using an orthogonal projection;\n");
    printf("backface-culling is also applied.\n");
    printf("Parameters:\n");
    printf("    file        path to the obj file to display.\n");
    printf("    width       door width.\n");
    printf("    height      door height.\n");
    printf("Keyboard/Mouse controls:\n");
    printf("    Escape      quit the program.\n");
    printf("    A           increase zoom.\n");
    printf("    Z           decrease zoom.\n");
    printf("    Arrow keys  move the 3D model around.\n");
    printf("    Left click + drag\n");
    printf("                Rotate the 3D model around the Y axis.\n");
    
}


int main() {
    BitmapFont font;
    srand(time(NULL));
    
    if (getenv("help") != NULL) {
        print_help();
        return 0;
    }
    
    if (getenv("width") != NULL) width = atoi(getenv("width"));
    if (getenv("height") != NULL) height = atoi(getenv("height"));
    
    linecolor = make_color_8bpp(0, 255, 0);
    bgroundcolor = make_color_8bpp(0, 0, 0);
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    char* fname = getenv("file");
    if (fname == NULL) {
        printf("Please specify file=<obj file path>\n");
        return 1;
    }
    
    char* data;
    size_t data_len;
    
    printf("Loading %s... ", fname);
    int load_err = load_full_file_into_mem(fname, &data, &data_len);
    if (load_err) {
        printf("Could not open %s\n", fname);
        return 1;
    }
    printf("Parsing... ");
    int ret = tinyobj_parse_obj(&attrib, &shapes, &num_shapes, &materials,
                                &num_materials, data, data_len, TINYOBJ_FLAG_TRIANGULATE);
    if (ret != TINYOBJ_SUCCESS) {
        printf("Error parsing %s\n", fname);
        return 0;
    }
    
    free(data);
    
    printf("Done!\n");
    printf("Vertex count: %u\tFace count: %u\n", attrib.num_vertices, attrib.num_faces);
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = max(0, rand() % (ti.screensize.w - width)),
        .y = max(0, rand() % (ti.screensize.h - height)),
        .w = width,
        .h = height,
    };
    Rect blank_area = {
        .x = 0,
        .y = 0,
        .w = main_area.w,
        .h = main_area.h,
    };
    bool open_success = open_main_door(&main_door, main_area, "Wavefront OBJ viewer");
    if (!open_success) {
        printf("objviewer: Error opening main door\n");
        return 2;
    }
    
    main_door.keycode_callback = &keycode_callback;
    main_door.mouse_callback = &objviewer_mouse_callback;
    
    scaling = ((double) width) / 250.0;
    double omega = M_PI / (60.0 * 8.0);
    
    center3d.x = (double) width/2;
    center3d.y = (double) height/2;
    center3d.z = 0;

    vertices = attrib.vertices;
    num_vertices = (size_t)attrib.num_vertices;
    size_t num_triangles = (size_t) (attrib.num_faces / 3);
    
    for (size_t v=0; v<num_vertices; v++) {
        vertices[v].y *= -1;
    }
    
    recompute_viewing_params();
    
    Vertex* vertices_work_copy = (Vertex*) malloc(sizeof(Vertex) * num_vertices);
    
    FPSController fps = FPSController_init(35, 10);
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        
        memcpy(vertices_work_copy, vertices, sizeof(Vertex) * num_vertices);
        
        multi_vertex_scale(vertices_work_copy, num_vertices, scale3d);
        double alpha = (objviewer_mouse_hold ? objviewer_drag_coeff : rei);
        multi_vertex_rotate_euler_y(vertices_work_copy, num_vertices, alpha);
        
        Vertex center = vertex_translate(vertex_rotate_euler_y(g, alpha), 
                                         center3d);
        multi_vertex_translate(vertices_work_copy, num_vertices, center);
        
        draw_rectangle_area(&(main_door.face), blank_area, bgroundcolor);

        draw_faces(vertices_work_copy, attrib.faces, num_triangles);
        
        long late_time_ms = FPSController_step(&fps);
        if (late_time_ms-5 < 0) k_sleep_milli(-(late_time_ms - 5));
        
        OpenDoor_update(&main_door, true);  // yield in here
        
        if (!objviewer_mouse_hold) rei += omega;
        if (rei >= 2*M_PI) rei -= 2*M_PI;
    }
    
    free(vertices_work_copy);
    tinyobj_attrib_free(&attrib);
    tinyobj_shapes_free(shapes, num_shapes);
    tinyobj_materials_free(materials, num_materials);
    OpenDoor_close(&main_door);
    
    return 0;
}
