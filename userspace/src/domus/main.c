#include <stdio.h>
#include <specio.h>
#include <stdlib.h>
#include <sys/mq.h>
#include <sys/serv.h>
#include <sys/proc.h>
#include <sys/mouse.h>
#include <sys/happy.h>
#include <time.h>

#include <libgui/libgui.h>

#include "../inih/ini.h"
#include "domus.h"


Array *doors;
bool running = true;

TermInfo screeninfo;
DOMUSConfig config;

Bitmap screenarea;
BitmapFont font;

Bitmap mouse_cursor;
Bitmap mouse_cursor_default;
Bitmap mouse_cursor_wait;
Bitmap displaced_by_cursor;
Color24 cursor_colorkey;
bool do_redraw_cursor = true;


void init_main_screen() {
    Rect screengeom = {
        .x = 0,
        .y = 0,
        .w = screeninfo.screensize.w,
        .h = screeninfo.screensize.h,
    };
    
#ifndef DIRECT_FB_DRAW
    screenarea = make_bitmap(screengeom, COLOR24_TERM_BGROUND);
#else
    int *system_fb;
    int error = direct_video_command(DVToggleGrabFB, DVC_TOGGLE_GRAB, (void*)&system_fb);
    if (error) {
        printf("Error %d: could not grab framebuffer\n", error);
        exit(5);
    }
    debug_printf("Domus grabbed the framebuffer (%p)\r\n", system_fb);
    screenarea = make_bitmap_from_existing(screengeom, COLOR24_TERM_BGROUND, system_fb);
#endif
}


void init_root_door() {
    Rect screengeom = {
        .x = 0,
        .y = 0,
        .w = screeninfo.screensize.w,
        .h = screeninfo.screensize.h,
    };
    
    Bitmap background = make_bitmap(screengeom, COLOR24_TERM_BGROUND);
    load_any_image_with_size(&background, config.wallpaper_path, screeninfo.screensize.w, screeninfo.screensize.h);
    
    LibguiMsgMakedoor rootdoor_spec = {
        .face = background,
        .name = "Desktop door",
        .border = 0,
        .border_color = 0,
        .transparency_colorkey = NULL,
        .parent_door_id = 0,
    };

    unsigned int root_door_id;
    DoorSystemStatus root_door_create_status = add_new_door(&rootdoor_spec, &root_door_id, ident().taskid);
    if (root_door_create_status != DOORS_OK || root_door_id != 0) {
        printf("Error %d creating root door (id %d).\n", root_door_create_status, root_door_id);
        exit(2);
    }
}

void switch_cursor(Bitmap* new_cursor) {
    mouse_cursor.colordata = new_cursor->colordata;
    mouse_cursor.geom.w = new_cursor->geom.w;
    mouse_cursor.geom.h = new_cursor->geom.h;
    do_redraw_cursor = true;
}

void init_cursor() {
    mouse_cursor.geom.x = screeninfo.screensize.w / 2;
    mouse_cursor.geom.y = screeninfo.screensize.h / 2;
    
    int error;
    if ((error = load_any_image(&mouse_cursor_default, config.cursor_normal_path, 1.0))) {
        printf("Cannot decode cursor.\n");
        exit(4);
    }
    if ((error = load_any_image(&mouse_cursor_wait, config.cursor_busy_path, 1.0))) {
        printf("Cannot decode cursor_wait.\n");
        exit(4);
    }
    
    switch_cursor(&mouse_cursor_default);
    
    cursor_colorkey = make_color_8bpp(255, 0, 255);
    displaced_by_cursor.colordata = NULL;
    memset(&prev_mouse_event, 0, sizeof(MouseEvent));
    prev_mouse_pos.x = 0; prev_mouse_pos.y = 0;
}

void overlay_cursor_on_screen(Bitmap *screen, Color24 *cursor_colorkey) {
    if (displaced_by_cursor.colordata != NULL && displaced_by_cursor.colordata != screen->colordata) {
        destroy_bitmap(&displaced_by_cursor);
    }
    
    displaced_by_cursor = safe_crop_bitmap(screen, mouse_cursor.geom);
    safe_overlay_bitmap(screen, &mouse_cursor, cursor_colorkey);
}

void undo_overlay_cursor_on_screen(Bitmap *screen) {
    if (displaced_by_cursor.colordata != NULL && displaced_by_cursor.colordata != screen->colordata) {
        safe_overlay_bitmap(screen, &displaced_by_cursor, NULL);
    }
}



void draw_indirect_border(Bitmap *screen, DoorEntry *door) {
    for (int f=0; f<door->border; f++) {
        Rect area = {
            .x = door->face.geom.x + f,
            .y = door->face.geom.y + f,
            .w = door->face.geom.w - f*2,
            .h = door->face.geom.h - f*2,
        };
        safe_draw_rectangle(screen, area, (door == focused_door ? COLOR24_TOP_WIN_BORDER : door->border_color));
    }
}

void draw_shrunk_door(Bitmap *screen, DoorEntry* door) {
    Rect safe_overlap_area;
    
    int num_intersection = rect_intersection(screen->geom, door->face.geom, &safe_overlap_area);
    if (num_intersection == 0) return;
    
    if (safe_overlap_area.x < 0) safe_overlap_area.x = 0;
    if (safe_overlap_area.y < 0) safe_overlap_area.y = 0;
    
    safe_overlap_area.x += door->border;
    safe_overlap_area.y += door->border;
    safe_overlap_area.w -= 2*door->border;
    safe_overlap_area.h -= 2*door->border;
    
    area_overlay_bitmap(screen, &door->face, &safe_overlap_area, door->transparency_colorkey);
}

bool redraw_as_needed(Bitmap *screen) {
    bool did_redraw = false;
    ARRAY_FOREACH(curr, doors, {
        DoorEntry *door = (DoorEntry*) curr;
        if (door->redraw) {
            
            if (door->border > 0) {
                draw_shrunk_door(screen, door);
                draw_indirect_border(screen, door);
            } else {
                safe_overlay_bitmap(screen, &door->face, door->transparency_colorkey);
            }
            
            door->redraw = false;
            did_redraw = true;
        }
    });
    return did_redraw;
}


static int config_handler(void* user, const char* section, const char* name, const char* value) {
    DOMUSConfig* lconfig = (DOMUSConfig*)user;

    #define MATCH(sname, nname) (strcmp(section, sname) == 0) && (strcmp(name, nname) == 0)
    if (MATCH("wallpaper", "file")) {
        lconfig->wallpaper_path = strdup(value);
    } else if (MATCH("cursor", "normal")) {
        lconfig->cursor_normal_path = strdup(value);
    } else if (MATCH("cursor", "busy")) {
        lconfig->cursor_busy_path = strdup(value);
    } else if (MATCH("font", "file")) {
        lconfig->bitmap_font_path = strdup(value);
    } else if (MATCH("startup", "welcome")) {
        lconfig->show_welcome = (strcmp(value, "1") == 0);
    } else if (MATCH("startup", "script")) {
        lconfig->startup_script = strdup(value);
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}


void resume_handler(void) {
    expirecc(&resume_handler, 20);
    continuation();
}


int main(void) {
    array_new(&doors);
    
    screeninfo = getterminfo();
    
    if (!screeninfo.support_bitmap) {
        printf("The current terminal does not support graphics.\n");
        return 1;
    }
    
    memset(&config, 0, sizeof(DOMUSConfig));
    if (ini_parse(CONFIG_FILE_PATH, &config_handler, &config) < 0) {
        printf("Failed to load config file at %s\n", CONFIG_FILE_PATH);
        return 2;
    }
    
    init_main_screen();
    
    ServStatus md_serv_creation_status = advertise_service_advanced(MAKEDOOR_SERVICE, sizeof(LibguiMsgMakedoor), DOMUS_RACE, DOMUS_CLASS);
    if (md_serv_creation_status != SERV_SUCCESS && md_serv_creation_status != SERV_ALREADY_EXISTS) {
        printf("Error %d cannot create service %s\n", md_serv_creation_status, MAKEDOOR_SERVICE);
        return 3;
    }
    
    ServStatus kd_serv_creation_status = advertise_service_advanced(KILLDOOR_SERVICE, sizeof(LibguiMsgKilldoor), DOMUS_RACE, DOMUS_CLASS);
    if (kd_serv_creation_status != SERV_SUCCESS && kd_serv_creation_status != SERV_ALREADY_EXISTS) {
        printf("Error %d cannot create service %s\n", kd_serv_creation_status, MAKEDOOR_SERVICE);
        return 3;
    }
    
    font = load_lbf_font(config.bitmap_font_path);
    if (font.codepoints == NULL) {
        printf("Could not load bitmap font at %s\n", config.bitmap_font_path);
        return 4;
    }
    
    init_cursor();
    init_root_door();
    
    spork_thread(&patronus);

    subscribe(DEFAULT_MOUSE_INPUT_TOPIC);
    subscribe(stdin_topic);
    subscribe(keycode_topic);
    
    expirecc(&resume_handler, 20);
    
    FPSController fps = FPSController_init(60, 35);
    
    while (running) {
        do_redraw_cursor = false;
        bool must_redisplay = false;
        handle_commands();
        bool input_status = handle_input();
        must_redisplay = must_redisplay || input_status;
        if (do_redraw_cursor) undo_overlay_cursor_on_screen(&screenarea);
        bool redraw_status = redraw_as_needed(&screenarea);
        must_redisplay = must_redisplay || redraw_status;
        if (do_redraw_cursor) overlay_cursor_on_screen(&screenarea, &cursor_colorkey);
#ifndef DIRECT_FB_DRAW
        if (must_redisplay) {
            display_bitmap(&screenarea, NULL);
        }
#endif
        FPSController_step(&fps);
        yield();
    }
    
    ARRAY_FOREACH(curr, doors, {
        debug_printf("removing Door at %p ", curr);
        DoorEntry *door = (DoorEntry*) curr;
        debug_printf(" id: %d, task: %d, name: %s\r\n", door->door_id, door->owner_task, door->name);
        if (door->door_id != 0) {
            debug_printf("  ... and killing task %d\r\n", door->owner_task);
            kill(door->owner_task);
        }
        //remove_door(door->door_id);
    });
    
    direct_video_command(DVToggleGrabFB, DVC_TOGGLE_UNGRAB, NULL);
    
    unsubscribe(keycode_topic);
    unsubscribe(stdin_topic);
    unsubscribe(DEFAULT_MOUSE_INPUT_TOPIC);
    
    retire_service(MAKEDOOR_SERVICE);
    retire_service(KILLDOOR_SERVICE);
    
    return 0;
} 
