#include <stdio.h>
#include <stdlib.h>
#include <sys/mq.h>
#include <sys/serv.h>

#include <libgui/libgui.h>

#include "domus.h"


#define ZINDEX_ZERO 0



unsigned int door_id_gen = 0;
DoorEntry *focused_door;


unsigned int get_new_door_id() {
    return door_id_gen++;
}

DoorEntry *get_door_by_id(unsigned int door_id) {
    ARRAY_FOREACH(curr, doors, {
        DoorEntry *current = (DoorEntry*) curr;
        if (current->door_id == door_id) return current;
    });
    return NULL;
}

bool is_descendant(DoorEntry *parent, DoorEntry *candidate) {
    ARRAY_FOREACH(curr, parent->child_doors, {
        DoorEntry *current = (DoorEntry*) curr;
        if (current == candidate) return true;
        if (is_descendant(current, candidate)) return true;
    });
    return false;
}

DoorEntry *get_common_ancestor(DoorEntry *a, DoorEntry *b) {
    if (is_descendant(a, b)) return a;
    if (is_descendant(b, a)) return b;
    return get_common_ancestor(get_door_by_id(a->parent_door_id), b);
}

DoorEntry *get_topmost_leaf(unsigned int door_id) {
    DoorEntry *mainnode = get_door_by_id(door_id);
    if (mainnode == NULL) return NULL;

    DoorEntry *topmost = mainnode;
    
    ARRAY_FOREACH(curr, mainnode->child_doors, {
        DoorEntry *current = (DoorEntry*) curr;
        
        DoorEntry *top_z_descendant = get_topmost_leaf(current->door_id);
        if (top_z_descendant->zindex > topmost->zindex) topmost = top_z_descendant;
    });
    return topmost;
}

DoorEntry *get_topmost_at(Point position) {
    DoorEntry *topmost = get_door_by_id(0);
    
    ARRAY_FOREACH(curr, doors, {
        DoorEntry *current = (DoorEntry*) curr;
        if (current->zindex > topmost->zindex) {
            if (rect_contains(current->face.geom, position)) {
                topmost = current;
            }
        }
    });
    return topmost;
}

void recursive_change_zindex(DoorEntry *root, int delta_zindex) {
    root->zindex += delta_zindex;
    
    ARRAY_FOREACH(curr, root->child_doors, {
        DoorEntry *current = (DoorEntry*) curr;
        recursive_change_zindex(current, delta_zindex);
    });
}

void make_z_room_after(int zindex) {
    ARRAY_FOREACH(curr, doors, {
        DoorEntry *current = (DoorEntry*) curr;
        if (current->zindex > zindex) {
            current->zindex++;
        }
    });
}

void cascade_redraw(DoorEntry *place) {
    place->redraw = true;
    Point curpos = {
        .x = mouse_cursor.geom.x,
        .y = mouse_cursor.geom.y,
    };
    if (rect_contains(place->face.geom, curpos)) {
        do_redraw_cursor = true;
    }
    ARRAY_FOREACH(curr, place->child_doors, {
        DoorEntry *current = (DoorEntry*) curr;
        cascade_redraw(current);
    });
}

    
int compare_doors(const void* a, const void* b) {
    DoorEntry *door_a = *((DoorEntry**) a);  // that's why we all love C
    DoorEntry *door_b = *((DoorEntry**) b);
    
    if (door_a->zindex < door_b->zindex) {
        return -1;
    } else if (door_a->zindex > door_b->zindex) {
        return 1;
    }
    
    return 0;
}

void restore_door_list_consistency() {
    array_sort(doors, &compare_doors);
}

void defrag_door_list_zindices() {
    int curr_zindex = ZINDEX_ZERO;
    ARRAY_FOREACH(curr, doors, {
        DoorEntry *current = (DoorEntry*) curr;
        current->zindex = curr_zindex++;
    });
}

void shift_focus(DoorEntry *from, DoorEntry *to) {
    char topicname[4096];
    
    if (focused_door->door_id == to->door_id) return;
    
    snprintf(topicname, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, from->door_id);
    LibguiMsgTransformEvent transform1 = {.geom = from->face.geom, .has_focus = false};
    publish(topicname, &transform1);
    
    // find the current topmost zindex
    DoorEntry *oldking = get_topmost_leaf(0);
    int zindex_to_beat = oldking->zindex;
    // bring the focused window and its children above that value
    int delta_zindex = (zindex_to_beat + 1) - to->zindex;
    recursive_change_zindex(to, delta_zindex);
    
    // sort the door array by zindex
    restore_door_list_consistency();
    
    // remove the holes in the zindex distribution
    defrag_door_list_zindices();
    
    focused_door = to;
    
    snprintf(topicname, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, to->door_id);
    LibguiMsgTransformEvent transform2 = {.geom = to->face.geom, .has_focus = true};
    publish(topicname, &transform2);
    
    //cascade_redraw(focused_door);
    compute_global_cover_status();
    cascade_redraw(get_door_by_id(0));
}

DoorSystemStatus add_door_topics(DoorEntry *door) {
    char topicname[4096];
    
    // updating (position and bitmap)
    snprintf(topicname, 4096, UPDATEDOOR_FACE_SERVICE_TEMPLATE, door->door_id);
    ServStatus du_serv_creation_status = advertise_service_advanced(topicname, sizeof(LibguiMsgUpdatedoorFace), DOMUS_RACE, DOMUS_CLASS);
    if (du_serv_creation_status != SERV_SUCCESS && du_serv_creation_status != SERV_ALREADY_EXISTS) {
        debug_printf("Error %d cannot create service %s\r\n", du_serv_creation_status, topicname);
        return DOORS_ERR_SERV_CREATION;
    }
    
    // mouse
    snprintf(topicname, 4096, DOOR_EVENT_MOUSE_TOPIC_TEMPLATE, door->door_id);
    MQStatus me_topic_creation_status = advertise_channel_advanced(topicname, 8, sizeof(LibguiMsgMouseEvent), DOMUS_RACE, DOMUS_CLASS, SecFullyEnforced);
    if (me_topic_creation_status != MQ_SUCCESS && me_topic_creation_status != MQ_ALREADY_EXISTS) {
        debug_printf("Error %d cannot create topic %s\r\n", me_topic_creation_status, topicname);
        return DOORS_ERR_TOPIC_CREATION;
    }
    
    // stdin
    snprintf(topicname, 4096, DOOR_EVENT_STDIN_TOPIC_TEMPLATE, door->door_id);
    MQStatus si_topic_creation_status = advertise_channel_advanced(topicname, 4096, sizeof(char), DOMUS_RACE, DOMUS_CLASS, SecFullyEnforced);
    if (si_topic_creation_status != MQ_SUCCESS && si_topic_creation_status != MQ_ALREADY_EXISTS) {
        debug_printf("Error %d cannot create topic %s\r\n", si_topic_creation_status, topicname);
        return DOORS_ERR_TOPIC_CREATION;
    }
    
    // keycode
    snprintf(topicname, 4096, DOOR_EVENT_KEYCODE_TOPIC_TEMPLATE, door->door_id);
    MQStatus kc_topic_creation_status = advertise_channel_advanced(topicname, 1024, sizeof(KeyEvent), DOMUS_RACE, DOMUS_CLASS, SecFullyEnforced);
    if (kc_topic_creation_status != MQ_SUCCESS && kc_topic_creation_status != MQ_ALREADY_EXISTS) {
        debug_printf("Error %d cannot create topic %s\r\n", kc_topic_creation_status, topicname);
        return DOORS_ERR_TOPIC_CREATION;
    }
    
    // resize & translate events
    snprintf(topicname, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, door->door_id);
    MQStatus tr_topic_creation_status = advertise_channel_advanced(topicname, 1024, sizeof(LibguiMsgTransformEvent), DOMUS_RACE, DOMUS_CLASS, SecFullyEnforced);
    if (tr_topic_creation_status != MQ_SUCCESS && tr_topic_creation_status != MQ_ALREADY_EXISTS) {
        debug_printf("Error %d cannot create topic %s\r\n", tr_topic_creation_status, topicname);
        return DOORS_ERR_TOPIC_CREATION;
    }
    
    return DOORS_OK;
}

DoorSystemStatus remove_door_topics(DoorEntry *door) {
    char topicname[4096];
    
    // updating (position and bitmap)
    snprintf(topicname, 4096, UPDATEDOOR_FACE_SERVICE_TEMPLATE, door->door_id);
    ServStatus du_serv_creation_status = retire_service(topicname);
    if (du_serv_creation_status != SERV_SUCCESS) {
        debug_printf("Error %d cannot retire service %s\r\n", du_serv_creation_status, topicname);
        return DOORS_ERR_SERV_CREATION;
    }
    
    // mouse
    snprintf(topicname, 4096, DOOR_EVENT_MOUSE_TOPIC_TEMPLATE, door->door_id);
    MQStatus me_topic_creation_status = retire_channel(topicname);
    if (me_topic_creation_status != MQ_SUCCESS) {
        debug_printf("Error %d cannot retire topic %s\r\n", me_topic_creation_status, topicname);
        return DOORS_ERR_TOPIC_CREATION;
    }
    
    // stdin
    snprintf(topicname, 4096, DOOR_EVENT_STDIN_TOPIC_TEMPLATE, door->door_id);
    MQStatus si_topic_creation_status = retire_channel(topicname);
    if (si_topic_creation_status != MQ_SUCCESS) {
        debug_printf("Error %d cannot retire topic %s\r\n", si_topic_creation_status, topicname);
        return DOORS_ERR_TOPIC_CREATION;
    }
    
    // keycode
    snprintf(topicname, 4096, DOOR_EVENT_KEYCODE_TOPIC_TEMPLATE, door->door_id);
    MQStatus kc_topic_creation_status = retire_channel(topicname);
    if (kc_topic_creation_status != MQ_SUCCESS) {
        debug_printf("Error %d cannot retire topic %s\r\n", kc_topic_creation_status, topicname);
        return DOORS_ERR_TOPIC_CREATION;
    }
    
    // resize & translate events
    snprintf(topicname, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, door->door_id);
    MQStatus tr_topic_creation_status = retire_channel(topicname);
    if (tr_topic_creation_status != MQ_SUCCESS) {
        debug_printf("Error %d cannot retire topic %s\r\n", tr_topic_creation_status, topicname);
        return DOORS_ERR_TOPIC_CREATION;
    }
    
    return DOORS_OK;
}





DoorSystemStatus add_new_door(LibguiMsgMakedoor *request, unsigned int *new_door_id, int owner_task) {
    unsigned int newdoor_id = get_new_door_id();

    DoorEntry *newdoor = (DoorEntry*) malloc(sizeof(DoorEntry));
    newdoor->face = request->face;
    strncpy(newdoor->name, request->name, MAX_DOORNAME);
    newdoor->border = request->border;
    newdoor->border_color = request->border_color;
    newdoor->transparency_colorkey = request->transparency_colorkey;
    newdoor->parent_door_id = request->parent_door_id;
    newdoor->door_id = newdoor_id;
    newdoor->owner_task = owner_task;
    newdoor->redraw = true;
    newdoor->glued = request->glued;
    newdoor->covered = false;
    newdoor->remember_size = newdoor->face.geom;
    
    DoorEntry *parent = get_door_by_id(request->parent_door_id);
    
    if (newdoor_id == 0) {
        newdoor->zindex = ZINDEX_ZERO;
        parent = NULL;
        focused_door = newdoor;
    } else {
        if (parent == NULL) {
            free(newdoor);
            return DOORS_ERR_INVALID_PARENT;
        }
        
        DoorEntry *topmost_sibling = get_topmost_leaf(request->parent_door_id);
        newdoor->zindex = topmost_sibling->zindex + 1;
        make_z_room_after(newdoor->zindex);
    }
    
    DoorSystemStatus topic_status = add_door_topics(newdoor);
    if (topic_status != DOORS_OK) {
        free(newdoor);
        return topic_status;
    }
    
    if (parent != NULL) {
        array_add(parent->child_doors, newdoor);
    }
    array_new(&(newdoor->child_doors));
    array_add(doors, newdoor);
    
    *new_door_id = newdoor_id;
    
    shift_focus(focused_door, newdoor);
    
    return DOORS_OK;
}

DoorSystemStatus remove_door(unsigned int door_id) {
    DoorEntry *door = get_door_by_id(door_id);
    
    ARRAY_FOREACH(curr, doors, {
        DoorEntry *otherdoor = (DoorEntry*) curr;
        array_remove(otherdoor->child_doors, door, NULL);
        if (otherdoor->parent_door_id == door_id) {
            remove_door(otherdoor->door_id);
        }
    });
    
    if (door == focused_door) {
        shift_focus(door, get_topmost_leaf(0));
    }
    
    array_destroy(door->child_doors);
    remove_door_topics(door);
    array_remove(doors, door, NULL);
    free(door);
    
    compute_global_cover_status();
    
    return DOORS_OK;
}

void move_door(DoorEntry *door, Point new_origin) {
    char topicname[4096];
    
    if (door->door_id == 0) return;
    
    DoorEntry *parent = get_door_by_id(door->parent_door_id);
    
    door->face.geom.x = new_origin.x;
    door->face.geom.y = new_origin.y;
    
    if (door->face.geom.x < parent->face.geom.x) door->face.geom.x = parent->face.geom.x;
    if (door->face.geom.y < parent->face.geom.y) door->face.geom.y = parent->face.geom.y;
    if (door->face.geom.x + door->face.geom.w >= parent->face.geom.x + parent->face.geom.w) door->face.geom.x = parent->face.geom.x + parent->face.geom.w - door->face.geom.w - 1;
    if (door->face.geom.y + door->face.geom.h >= parent->face.geom.y + parent->face.geom.h) door->face.geom.y = parent->face.geom.y + parent->face.geom.h - door->face.geom.h - 1;
    
    snprintf(topicname, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, door->door_id);
    LibguiMsgTransformEvent transform = {.geom = door->face.geom, .has_focus = (door == focused_door)};
    publish(topicname, &transform);
    
    compute_global_cover_status();
    cascade_redraw(door);
}

void resize_door(DoorEntry *door, Point new_size) {
    char topicname[4096];
    
    if (door->door_id == 0) return;
    if (new_size.x < MINIMUM_DOOR_SIZE) new_size.x = MINIMUM_DOOR_SIZE;
    if (new_size.y < MINIMUM_DOOR_SIZE) new_size.y = MINIMUM_DOOR_SIZE;
    
    DoorEntry *parent = get_door_by_id(door->parent_door_id);
    
    Rect new_geom = door->face.geom;
    
    new_geom.w = new_size.x;
    new_geom.h = new_size.y;
    
    if (new_geom.x + new_geom.w >= parent->face.geom.x + parent->face.geom.w) new_geom.w = parent->face.geom.x + parent->face.geom.w - new_geom.x - 1;
    if (new_geom.y + new_geom.h >= parent->face.geom.y + parent->face.geom.h) new_geom.h = parent->face.geom.y + parent->face.geom.h - new_geom.y - 1;
    
    snprintf(topicname, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, door->door_id);
    LibguiMsgTransformEvent transform = {.geom = new_geom, .has_focus = (door == focused_door)};
    publish(topicname, &transform);
    
    //compute_global_cover_status();
    //cascade_redraw(door);
}

void toggle_maximize_door(DoorEntry *door) {
    char tmptopic[4096];
    snprintf(tmptopic, 4096, DOOR_EVENT_TRANSFORM_TOPIC_TEMPLATE, door->door_id);
    
    DoorEntry* parent = get_door_by_id(door->parent_door_id);
    Rect maximal_size = parent->face.geom;
    maximal_size.w--;
    maximal_size.h--;
    
    if (rect_is_zero(rect_delta(door->face.geom, maximal_size))) {
        Rect new_geom = door->remember_size;
        door->remember_size = door->face.geom;
        LibguiMsgTransformEvent transform = {.geom = new_geom, .has_focus = (door == focused_door)};
        publish(tmptopic, &transform);
    } else {
        door->remember_size = door->face.geom;
        Rect new_geom = maximal_size;
        LibguiMsgTransformEvent transform = {.geom = new_geom, .has_focus = (door == focused_door)};
        publish(tmptopic, &transform);
    }
}

static void compute_cover_status(DoorEntry *door) {
    door->covered = false;
    if (focused_door == door) return;
    ARRAY_FOREACH(curr, doors, {
        DoorEntry *otherdoor = (DoorEntry*) curr;
        if (door == otherdoor) continue;
        if (door->zindex > otherdoor->zindex) continue;
        if (otherdoor->parent_door_id == door->door_id) continue;
        
        Rect intersection;
        if (rect_intersection(door->face.geom, otherdoor->face.geom, &intersection)) {
            door->covered = true;
            return;
        }
    });
}

void compute_global_cover_status() {
    ARRAY_FOREACH(curr, doors, {
        DoorEntry *door = (DoorEntry*) curr;
        compute_cover_status(door);
    });
}
