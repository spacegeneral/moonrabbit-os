#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <yakumo.h>
#include <stdbool.h>
#include <string.h>
#include <geom.h>
#include <stdarg.h>
#include <sys/fs.h>
#include <sys/term.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/keycodes.h>
#include <libedit/readline.h>
#include <libedit/characterline.h>
#include <libgui/libgui.h>

#include "modular/modular.h"

TermInfo terminfo;
TermInfo backup_tinfo;
const char default_file[] = "f,def,home,scratchpad.txt";

#define SUBCMDLEN 4096
#define LINENUM_FMT_LEN 15
#define LINES_GROW_FACTOR 10


BitmapFont font_used;



typedef struct ReadlineState {
    CharacterLine line;
    Point basecur;
    int y_offset;
    int height;
    int reminder_width;
    bool dirty;  // must recalculate height / reminder_width
    bool decorated;
} ReadlineState;


typedef struct ExpandedText {
    ReadlineState *lines;
    size_t num_lines_avail;
    size_t num_lines_used;
    int active_line;
    int topmost_line;
} ExpandedText;

ExpandedText text;
int bottommost_drawn_line;
bool redraw_full = true;
int linenum_width = 0;
char linenum_fmt[LINENUM_FMT_LEN];

char *status_msg;
size_t status_msg_max_length;

char filename[SUBCMDLEN];
char searchstr[SUBCMDLEN];
char *copybuffer = NULL;
int default_background = SETCOLOR_UNCHANGED;

bool running = true;
bool pager_mode = false;
bool is_c_file = false;
bool is_mjs_file = false;



void ReadlineState_init(ReadlineState *line, Point pos, int y_offset) {
    CharacterLine_init(&(line->line));
    line->basecur = pos;
    line->y_offset = y_offset;
    line->dirty = true;
    line->decorated = false;
}

void ReadlineState_destroy(ReadlineState *line) {
    CharacterLine_destroy(&(line->line));
}

void ReadlineState_precompute_size_params(ReadlineState *line) {
    if (!terminfo.support_bitmap) {
        int height = (linenum_width + line->line.num_cells_used) / terminfo.termsize.w;
        if ((linenum_width + line->line.num_cells_used) % terminfo.termsize.w) height++;
        line->height = max(1, height);
        
        int width_left = (linenum_width + line->line.num_cells_used) % terminfo.termsize.w;
        line->reminder_width = terminfo.termsize.w - width_left;
    } else {
        int width = linenum_width + CharacterLine_onscreen_length(&(line->line), &font_used, 0);
        int height = width / (terminfo.termsize.w * CHAR_WIDTH_UNIT);
        if ((width / CHAR_WIDTH_UNIT) % terminfo.termsize.w) height++;
        line->height = max(1, height);
        
        int width_left = ((width / CHAR_WIDTH_UNIT) % terminfo.termsize.w);
        line->reminder_width = terminfo.termsize.w - width_left;
    }
    
    line->dirty = false;
}

int ReadlineState_get_height(ReadlineState *line) {
    if (!line->dirty) {
        return line->height;
    }
    
    ReadlineState_precompute_size_params(line);
    
    return line->height;
}

int ReadlineState_get_reminder_width(ReadlineState *line) {
    if (!line->dirty) {
        return line->reminder_width;
    }
    
    ReadlineState_precompute_size_params(line);
    
    return line->reminder_width;
}

Point ReadlineState_follow(ReadlineState *line) {
    Point pos = {
        .x = line->basecur.x,
        .y = line->basecur.y + ReadlineState_get_height(line),
    };
    return pos;
}

void ReadlineState_highlight_syntax_any(ReadlineState *line) {
    line->decorated = true;
    if (is_c_file) {
        CharacterLine_highlight_c_syntax(&(line->line));
    } else if (is_mjs_file) {
        CharacterLine_highlight_mjs_syntax(&(line->line));
    }
}

void CharacterLine_highlight_syntax_any(CharacterLine *line) {
    if (is_c_file) {
        CharacterLine_highlight_c_syntax(line);
    } else if (is_mjs_file) {
        CharacterLine_highlight_mjs_syntax(line);
    }
}

void ReadlineState_highlight_syntax_any_once(ReadlineState *line) {
    if (!line->decorated) {
        ReadlineState_highlight_syntax_any(line);
    }
}


void ExpandedText_init(ExpandedText *text) {
    text->lines = (ReadlineState*) malloc(sizeof(ReadlineState) * LINES_GROW_FACTOR);
    text->num_lines_avail = LINES_GROW_FACTOR;
    text->num_lines_used = 1;
    
    text->active_line = 0;
    text->topmost_line = 0;
    
    Point pos = {.x = 0, .y = 0};
    ReadlineState_init(text->lines + text->active_line, pos, 0);
}

void ExpandedText_grow(ExpandedText *text) {
    text->num_lines_avail += LINES_GROW_FACTOR;
    text->lines = realloc(text->lines, text->num_lines_avail * sizeof(ReadlineState));
}

void ExpandedText_fix_y_order(ExpandedText *text) {
    for (size_t nl=1; nl<text->num_lines_used; nl++) {
        text->lines[nl].basecur = ReadlineState_follow(&(text->lines[nl-1]));
        text->lines[nl].y_offset = text->lines[nl-1].y_offset;
        //debug_printf("line %d: y_offset = %d, pos: %d x %d\r\n", nl, text->lines[nl].y_offset, text->lines[nl].basecur.x, text->lines[nl].basecur.y);
    }
}

void ExpandedText_highlight_c_syntax(ExpandedText *text) {
    for (size_t nl=0; nl<text->num_lines_used; nl++) {
        CharacterLine_highlight_c_syntax(&(text->lines[nl].line));
    }
}

void ExpandedText_highlight_mjs_syntax(ExpandedText *text) {
    for (size_t nl=0; nl<text->num_lines_used; nl++) {
        CharacterLine_highlight_mjs_syntax(&(text->lines[nl].line));
    }
}

void ExpandedText_highlight_syntax_any(ExpandedText *text) {
    if (is_c_file) {
        ExpandedText_highlight_c_syntax(text);
    } else if (is_mjs_file) {
        ExpandedText_highlight_mjs_syntax(text);
    }
}

void ExpandedText_insert_empty_line_fast(ExpandedText *text, int atpos) {
    // grow array if needed
    if (text->num_lines_used+1 > text->num_lines_avail) ExpandedText_grow(text);
    
    // insert in the middle: shift forwards all the lines following the atpos
    if (atpos < text->num_lines_used) {
        for (int mwlk=text->num_lines_used; mwlk>atpos; mwlk--) {
            text->lines[mwlk] = text->lines[mwlk-1];
        }
    }
    
    Point pos = {.x = 0, .y = 0};
    ReadlineState_init(text->lines + atpos, pos, 0);
    text->num_lines_used++;
    
    //debug_printf("Inserted empty line %d (%dx%d)\r\n", atpos, text->lines[atpos].basecur.x, text->lines[atpos].basecur.y);
}

void ExpandedText_insert_empty_line(ExpandedText *text, int atpos) {
    ExpandedText_insert_empty_line_fast(text, atpos);
    ExpandedText_fix_y_order(text);
}

void ExpandedText_delete_line(ExpandedText *text, int atpos) {
    if (atpos >= text->num_lines_used) return;
    
    ReadlineState *delenda = text->lines + atpos;
    ReadlineState_destroy(delenda);
    
    for (int wlk=atpos; wlk<text->num_lines_used-1; wlk++) {
        text->lines[wlk] = text->lines[wlk+1];
    }
    
    text->num_lines_used--;
    
    ExpandedText_fix_y_order(text);
}

void ExpandedText_merge_lines(ExpandedText *text, int atpos) {
    if (atpos+1 >= text->num_lines_used) return;
    
    ReadlineState *receiver = text->lines + atpos;
    ReadlineState *donator = text->lines + atpos + 1;
    
    CharacterLine_append_other(&(receiver->line), &(donator->line), 0);
    receiver->dirty = true;
    
    ExpandedText_delete_line(text, atpos + 1);
}

void ExpandedText_split_line(ExpandedText *text, int atpos, size_t x_cut) {
    ExpandedText_insert_empty_line(text, atpos+1);
    
    ReadlineState *donator = text->lines + atpos;
    ReadlineState *receiver = text->lines + atpos + 1;
    donator->dirty = true;
    receiver->dirty = true;
    
    CharacterLine_append_other(&(receiver->line), &(donator->line), x_cut);
    donator->line.num_cells_used = x_cut;
}

void ExpandedText_eat_str(ExpandedText *text, char *str) {
    char *substr = str;
    
    while (true) {
        char *nlpos = strchr(substr, '\n');
        if (nlpos == NULL) {
            CharacterLine_digest(&(text->lines[text->active_line].line), substr);
            break;
        } else {
            char backup = nlpos[0];
            nlpos[0] = '\0';
            CharacterLine_digest(&(text->lines[text->active_line].line), substr);
            text->lines[text->active_line].dirty = true;
            text->active_line++;
            ExpandedText_insert_empty_line_fast(text, text->active_line);
            nlpos[0] = backup;
            substr = nlpos + 1;
        }
    }
    
    text->active_line = 0;
    text->lines[text->active_line].line.cursor_x = text->lines[text->active_line].line.num_cells_used;
    
    //ExpandedText_highlight_syntax_any(text);  // costly
    ExpandedText_fix_y_order(text);
}

int ExpandedText_save(ExpandedText *text, char *tofile) {
    FILE *fp;
    fp = fopen(tofile, "w");
    if (fp == NULL) return 1;
    
    for (size_t line=0; line<text->num_lines_used; line++) {
        if (!CharacterLine_save(&(text->lines[line].line), fp)) {
            fclose(fp);
            return 1;
        }
    }
    
    fclose(fp);
    return 0;
}

bool ExpandedText_search(ExpandedText *text, char *str, int line_offset, int cursor_offset, int *num_line, int *cursor_pos) {
    int hit_cur;
    for (int line=line_offset; line<text->num_lines_used; line++) {
        hit_cur = CharacterLine_search(&(text->lines[line].line), str, cursor_offset);
        cursor_offset = 0;
        if (hit_cur >= 0) {
            *num_line = line;
            *cursor_pos = hit_cur;
            return true;
        }
    }
    
    return false;
}



static void toggle_linenum(bool is_on) {
    if (is_on) {
        linenum_width = qd_log10(text.num_lines_used) + 1;
        snprintf(linenum_fmt, LINENUM_FMT_LEN, "%%0%dd ", linenum_width-1);
    } else {
        linenum_width = 0;
    }
}


static void clamp_topmost_line() {
    if (text.topmost_line < 0) text.topmost_line = 0;
    if (text.topmost_line + terminfo.termsize.h - 1 > text.num_lines_used) text.topmost_line = text.num_lines_used - terminfo.termsize.h + 1;
}


void printf_staus_temp(char *fmt, ...) {
    setcursor(0, terminfo.termsize.h-1);
    for (int b = 0; b < terminfo.termsize.w - 1; b++) printf(" ");
    printf("\r");
    
    va_list va;
    va_start(va, fmt);
    vprintf(fmt, va);
    va_end(va);
}


#define SAVEMACRO \
{ \
    if (ExpandedText_save(&text, filename) == 0) { \
        snprintf(status_msg, status_msg_max_length, "Saved to %s", filename); \
    } else { \
        snprintf(status_msg, status_msg_max_length, "Error saving %s", filename); \
    } \
}

#define SEARCHMACRO \
{ \
    bool found; \
    int found_line; \
    int found_cursor; \
     \
    found = ExpandedText_search(&text, searchstr, text.active_line, text.lines[text.active_line].line.cursor_x+1, &found_line, &found_cursor); \
    if (found) { \
        text.active_line = found_line; \
        text.topmost_line = found_line; \
        text.lines[text.active_line].line.cursor_x = found_cursor; \
        clamp_topmost_line(); \
        snprintf(status_msg, status_msg_max_length, "\"%s\" found at %d:%d", searchstr, found_line+1, found_cursor+1); \
    } else { \
        found = ExpandedText_search(&text, searchstr, 0, 0, &found_line, &found_cursor); \
        if (found) { \
            text.active_line = found_line; \
            text.topmost_line = found_line; \
            text.lines[text.active_line].line.cursor_x = found_cursor; \
            clamp_topmost_line(); \
            snprintf(status_msg, status_msg_max_length, "\"%s\" found at %d:%d (wrap around EOF)", searchstr, found_line+1, found_cursor+1); \
        } else { \
            snprintf(status_msg, status_msg_max_length, "\"%s\" not found.", searchstr); \
        } \
    } \
}

static bool readline_wip_handle_key(CharacterLine *line, KeyEvent *key) {
    terminfo = getterminfo();
    
    switch (key->keycode) {
        case KEY_Enter:
            if (!pager_mode) {
                if (line->cursor_x == line->num_cells_used) {
                    ExpandedText_insert_empty_line(&text, text.active_line+1);
                    text.active_line++;
                    redraw_full = true;
                    if (bottommost_drawn_line < text.active_line && text.active_line - text.topmost_line > terminfo.termsize.h - 2) text.topmost_line++;
                    return true;
                } else {
                    ExpandedText_split_line(&text, text.active_line, text.lines[text.active_line].line.cursor_x);
                    text.active_line++;
                    redraw_full = true;
                    if (bottommost_drawn_line < text.active_line && text.active_line - text.topmost_line > terminfo.termsize.h - 2) text.topmost_line++;
                    CharacterLine_highlight_syntax_any(line);
                    return true;
                }
            }
            break;
        case KEY_Insert:
            line->ins_mode = !line->ins_mode;
            snprintf(status_msg, status_msg_max_length, "Insert mode %s", (line->ins_mode ? "ON" : "OFF"));
            redraw_full = true;
            return true;
            break;
        case KEY_Left:
            CharacterLine_cursor_left(line);
            break;
        case KEY_Right:
            CharacterLine_cursor_right(line);
            break;
        case KEY_Up:
            if (pager_mode) {
                if (text.topmost_line-1 >= 0) {
                    text.topmost_line--;
                    text.active_line = text.topmost_line;
                    redraw_full = true;
                    return true;
                }
            } else {
                if (text.active_line-1 >= 0) {
                    text.active_line--;
                    text.lines[text.active_line].line.cursor_x = min(text.lines[text.active_line].line.num_cells_used, line->cursor_x);
                    if (text.topmost_line > text.active_line) {
                        text.topmost_line--;
                        redraw_full = true;
                    }
                    return true;
                }
            }
            break;
        case KEY_Down:
            {
                int increment = ReadlineState_get_height(&(text.lines[text.active_line]));
                if (pager_mode) {
                    if (bottommost_drawn_line+increment < text.num_lines_used) {
                        text.topmost_line += increment;
                        text.active_line = text.topmost_line;
                        redraw_full = true;
                        return true;
                    }
                } else {
                    if (text.active_line+increment < text.num_lines_used) {
                        text.active_line++;
                        text.lines[text.active_line].line.cursor_x = min(text.lines[text.active_line].line.num_cells_used, line->cursor_x);
                        if (bottommost_drawn_line < text.active_line) {
                            text.topmost_line += increment;
                            redraw_full = true;
                        }
                        return true;
                    }
                }
            }
            break;
        case KEY_PageUp:
            text.topmost_line -= terminfo.termsize.h - 2;
            clamp_topmost_line();
            text.active_line = text.topmost_line;
            text.lines[text.active_line].line.cursor_x = 0;
            redraw_full = true;
            return true;
            break;
        case KEY_PageDown:
            text.topmost_line += terminfo.termsize.h - 2;
            clamp_topmost_line();
            text.active_line = text.topmost_line;
            text.lines[text.active_line].line.cursor_x = 0;
            redraw_full = true;
            return true;
            break;
        case KEY_Home:
            line->cursor_x = 0;
            break;
        case KEY_End:
            line->cursor_x = line->num_cells_used;
            break;
        case KEY_Backspace:
            if (!pager_mode) {
                if (line->cursor_x == 0) {
                    if (text.active_line > 0) {
                        int saved_x_cur = text.lines[text.active_line-1].line.num_cells_used;
                        ExpandedText_merge_lines(&text, text.active_line-1);
                        text.active_line--;
                        text.lines[text.active_line].line.cursor_x = saved_x_cur;
                        redraw_full = true;
                        return true;
                    }
                } else {
                    CharacterLine_backspace(line);
                }
                CharacterLine_highlight_syntax_any(line);
            }
            break;
        case KEY_Delete:
            if (!pager_mode) {
                if (line->cursor_x == line->num_cells_used) {
                    if (text.active_line+1 < text.num_lines_used) {
                        ExpandedText_merge_lines(&text, text.active_line);
                        redraw_full = true;
                        return true;
                    }
                } else {
                    CharacterLine_delete(line);
                }
            }
            break;
        case KEY_F11:
            toggle_linenum(!(linenum_width > 0));
            redraw_full = true;
            return true;
            break;
        case KEY_F3:
            SEARCHMACRO;
            redraw_full = true;
            return true;
            break;
        case KEY_Escape:
            running = false;
            return true;
            break;
        default:
            if (key->mod_flags & LCTRL_DOWN_FLAG) {
                switch (key->keycode) {
                    case KEY_K:
                        if (text.num_lines_used > 1) {
                            ExpandedText_delete_line(&text, text.active_line);
                            redraw_full = true;
                            return true;
                        }
                    break;
                    case KEY_R:
                        redraw_full = true;
                        return true;
                    break;
                    case KEY_J:
                        togglecursor(true);
                        printf_staus_temp("Go to line: ");
                        char linenum[25];
                        readline_simple(linenum);
                        
                        int new_linenum;
                        if (memcmp(linenum, "0x", 2) == 0) {
                            new_linenum = atoi_hex(linenum+2);
                        } else {
                            new_linenum = atoi(linenum);
                        }
                        
                        if (new_linenum < 0) new_linenum = 0;
                        if (new_linenum > text.num_lines_used) new_linenum = text.num_lines_used-1;
                        
                        text.active_line = new_linenum;
                        text.topmost_line = text.active_line - 10;
                        if (text.topmost_line < 0) text.topmost_line = 0;
                        
                        cls();
                        
                        redraw_full = true;
                        togglecursor(false);
                        return true;
                    break;
                    case KEY_G:
                        snprintf(status_msg, status_msg_max_length, "Cursor at %d:%d", text.active_line+1, text.lines[text.active_line].line.cursor_x+1);
                        redraw_full = true;
                        return true;
                    break;
                    case KEY_W:
                        if (!pager_mode) {
                            printf_staus_temp("Save file: ");
                            readline_editline(filename);
                            SAVEMACRO;
                            redraw_full = true;
                            return true;
                        }
                    break;
                    case KEY_S:
                        if (!pager_mode) {
                            printf_staus_temp("Saving...");
                            SAVEMACRO;
                            redraw_full = true;
                            return true;
                        }
                    break;
                    case KEY_X:
                        if (!pager_mode) {
                            printf_staus_temp("Saving...");
                            SAVEMACRO;
                            running = false;
                            return true;
                        }
                    break;
                    case KEY_F:
                        if (!pager_mode) {
                            printf_staus_temp("Search: ");
                            readline_editline(searchstr);
                            SEARCHMACRO;
                            redraw_full = true;
                            return true;
                        }
                    break;
                }
            }
            else if (strlen(key->utf8) > 0 && !pager_mode) {  // printable
                status_msg[0] = '\0';
                int height_before = ReadlineState_get_height(&(text.lines[text.active_line]));
                CharacterLine_putchar(line, key);
                CharacterLine_highlight_syntax_any(line);
                text.lines[text.active_line].dirty = true;
                int height_after = ReadlineState_get_height(&(text.lines[text.active_line]));
                if (height_before != height_after) {
                    redraw_full = true;
                    return true;
                }
                break;
            }
    }
    
    return false;
}

int readline_wip(ReadlineState *state) {
    MQStatus poll_status;
    KeyEvent key;
    
    bool editing = true;
    
    subscribe(keycode_topic);
    
    int linenum = text.active_line+1;
    
    togglecursor(false);
    setcursor(state->basecur.x, state->basecur.y + state->y_offset);
    if (linenum_width > 0) printf(linenum_fmt, linenum);
    CharacterLine_print_up_to_cursor_color(&(state->line), &(terminfo.foreground_color), &default_background);
    if (!pager_mode) togglecursor(true);
    
    while (editing) {
        poll_status = poll(keycode_topic, &key);
        if (poll_status == MQ_SUCCESS && key.is_pressed) {
            editing = !readline_wip_handle_key(&(state->line), &key);
            
            togglecursor(false);
            setcursor(state->basecur.x, state->basecur.y + state->y_offset);
            setcolors(COLOR24_TEXT, SETCOLOR_UNCHANGED);
            if (linenum_width > 0) printf(linenum_fmt, linenum);
            CharacterLine_print_color(&(state->line), &(terminfo.foreground_color), &default_background);
            
            setcursor(state->basecur.x, state->basecur.y + state->y_offset);
            if (linenum_width > 0) printf(linenum_fmt, linenum);
            CharacterLine_print_up_to_cursor_color(&(state->line), &(terminfo.foreground_color), &default_background);
            if (!pager_mode) togglecursor(true);
            
        } else if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) {
            unsubscribe(keycode_topic);
            return poll_status;
        } else {
            yield();
        }
    }
    
    unsubscribe(keycode_topic);
    return 0;
}


void drawtext(ExpandedText *text) {
    terminfo = getterminfo();
    
    if (redraw_full) {
        setcursor(0, 0);
    
        text->lines[0].y_offset = -text->lines[text->topmost_line].basecur.y;
        ExpandedText_fix_y_order(text);
        
        togglecursor(false);
        int height_so_far = 0;
        int line;
        for (line = text->topmost_line; line<text->num_lines_used; line++) {
            ReadlineState *currline = text->lines + line;
            int height_added = ReadlineState_get_height(currline);
            //debug_printf("Line %d added height: %d\r\n", line, height_added);
            
            if (((height_so_far + height_added) < terminfo.termsize.h) || (line == text->topmost_line)) {
                height_so_far += height_added;
                ReadlineState_highlight_syntax_any_once(currline);
                setcursor(currline->basecur.x, currline->basecur.y + currline->y_offset);
                setcolors(COLOR24_TEXT, SETCOLOR_UNCHANGED);
                if (linenum_width > 0) printf(linenum_fmt, line+1);
                currline->line.trailing_erase = ReadlineState_get_reminder_width(currline);
                //debug_printf("   Line %d reminder width: %d\r\n", line, (int)currline->line.trailing_erase);
                CharacterLine_print_color(&(currline->line), &(terminfo.foreground_color), &default_background);
            } else {
                break;
            }
        }
        bottommost_drawn_line = line-1;
        
        if (height_so_far < terminfo.termsize.h-1) {
            if (bottommost_drawn_line+1 >= text->num_lines_used) {
                for (int vfill=0; vfill<max(0, terminfo.termsize.h-1-height_so_far); vfill++) {
                    printf(" ~");
                    for (size_t b=0; b<terminfo.termsize.w - 2 - 1; b++) printf(" ");
                    printf("\n");
                }
            } else {
                for (int vfill=0; vfill<max(0, terminfo.termsize.h-1-height_so_far); vfill++) {
                    printf(" ↓");
                    for (size_t b=0; b<terminfo.termsize.w - 2 - 1; b++) printf(" ");
                    printf("\n");
                }
            }
        }
        
        setcursor(0, terminfo.termsize.h-1);
        printf(status_msg);
        for (size_t b=0; b<terminfo.termsize.w - strlen(status_msg) - 1; b++) printf(" ");
        
        togglecursor(true);
        redraw_full = false;
    }
    
    readline_wip(text->lines + text->active_line);
}


void print_help() {
    printf("Editor for text files.\n");
    printf("Simple syntax highlighting is supported for C and JS source files.\n");
    printf("Parameters:\n");
    printf("    file        path to the file to edit.\n");
    printf("    syntax      either \"c\" or \"js\", forcibly enable a syntax\n");
    printf("                highlighting style.\n");
    printf("    pager       (no value) run in pager mode\n");
    printf("                (modification is disabled, the cursor is hidden,\n");
    printf("                the cursor keys are for scrolling).\n");
    printf("Keyboard controls:\n");
    printf("    Insert      toggle insert mode.\n");
    printf("    Arrow keys  move the cursor.\n");
    printf("    Page up     scroll one screen up.\n");
    printf("    Page down   scroll one screen down.\n");
    printf("    Ctrl+F      begin a text search.\n");
    printf("    F3          go to the next search result.\n");
    printf("    F11         toggle line numbers.\n");
    printf("    Ctrl+J      jump to a line number.\n");
    printf("    Ctrl+R      redraw the screen.\n");
    printf("    Ctrl+G      print the current cursor coordinates.\n");
    printf("    Ctrl+W      save to a chosen filename.\n");
    printf("    Ctrl+S      quick save.\n");
    printf("    Ctrl+X      save and exit.\n");
    printf("    Escape      quit without saving.\n");
}


int main(void) {
    char subcommand[SUBCMDLEN];
    
    if (getenv("help") != NULL) {
        print_help();
        return 0;
    }
    
    char *filearg = getenv("file");
    if (filearg == NULL) {
        strncpy(filename, default_file, SUBCMDLEN);
    } else {
        strncpy(filename, filearg, SUBCMDLEN);
    }
    
    bool force_c_syntax = false;
    if (getenv("syntax") != NULL) {
        force_c_syntax = (strcmp(getenv("syntax"), "c") == 0);
    }
    bool force_mjs_syntax = false;
    if (getenv("syntax") != NULL) {
        force_mjs_syntax = (strcmp(getenv("syntax"), "js") == 0);
    }
    is_c_file = endswith(filename, ".c") || endswith(filename, ".c.zz") || endswith(filename, ".h") || force_c_syntax;
    is_mjs_file = endswith(filename, ".js") || endswith(filename, ".js.zz") || force_mjs_syntax;
    
    searchstr[0] = '\0';
    pager_mode = (getenv("pager") != NULL);
    
    terminfo = getterminfo();
    memcpy(&backup_tinfo, &terminfo, sizeof(TermInfo));
    if (terminfo.support_bitmap) {
        font_used = load_lbf_font(getenv("BITMAP_FONT"));
    }
    //terminfo.termsize.w = 68;
    
    status_msg = malloc(sizeof(char) * terminfo.termsize.w);
    status_msg_max_length = terminfo.termsize.w-1;
    
    ExpandedText_init(&text);
    
    FILE *fp;
    
    fp = fopen(filename, "r");

    if (fp != NULL) {
        fseek(fp, 0, SEEK_END);
        long filesize = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        
        printf("Loading %s (%d bytes)...", filename, filesize);
        
        char *full_text = (char*) malloc(sizeof(char) * (filesize + 1));
        fread(full_text, 1, filesize, fp);
        full_text[filesize] = '\0';
        fclose(fp);
        
        printf(" done; parsing... ");

        ExpandedText_eat_str(&text, full_text);
        
        free(full_text);
        
        printf(" done; visualizing...");
        
        snprintf(status_msg, status_msg_max_length, "Loaded %d bytes from %s", filesize, filename);
    } else {
        if (pager_mode) {
            printf("Could not open file %s\n", filename);
            return 1;
        } else {
            snprintf(status_msg, status_msg_max_length, "Editing a new file");
        }
    }
    
    cls();
    
    if (pager_mode) togglecursor(false);
    
    while (running) {
        drawtext(&text);
    }
    
    terminfo = getterminfo();
    setcolors(backup_tinfo.foreground_color, backup_tinfo.background_color);
    setcursor(0, terminfo.cursor.y);
    cls();
    
    if (pager_mode) togglecursor(true);
    
    if (terminfo.support_bitmap) {
        destroy_bitmap_font(font_used);
    }
    
    return 0;
}

 
