#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/proc.h>


void resume_handler(void) {
    expirecc(&resume_handler, 200);
    continuation();
}






int main(void) {
    
    size_t n = 500000;
    
    expirecc(&resume_handler, 200);
    
    printf("Super simple single-CPU benchmark running...\n");
    
    if (getenv("limit") != NULL) {
        n = (size_t) atol(getenv("limit"));
    }
    
    
    uint64_t* primes = (uint64_t*) malloc(sizeof(uint64_t) * (n/2));
    size_t nfound = 1;
    primes[0] = 2;
    
    uint64_t upper_bound = (uint64_t)n;
    
    for (uint64_t p=3; p<upper_bound; p+=2) {
        bool is_prime = true;
        uint64_t bound = p / 2;
        
        for (size_t c=0; c<nfound; c++) {
            if (primes[c] > bound) break;
            if (p % primes[c] == 0) {
                is_prime = false;
                break;
            }
        }
        
        if (is_prime) primes[nfound++] = p;
    }
    
    uint64_t acc = 0;
    
    for (size_t c=0; c<nfound; c++) {
        acc += primes[c];
    }
    
    printf("The sum of all primes less than %lu is %llu\n", n, acc);
    
    return 0;
}
