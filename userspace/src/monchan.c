#include <stdio.h>
#include <stdlib.h>
#include <sys/errno.h>
#include <sys/proc.h>
#include <sys/serv.h>
#include <sys/mq.h>
#include <sys/share.h>
#include <sys/keycodes.h>
#include <sys/default_topics.h>
#include <time.h>


bool debug = false;
char* topicname;
size_t width = 1;


char get_ascii(unsigned char character) {
    if (character >= ' ' && character <= '~') return character;
    return '.';
} 


void format_message(void* message) {
    unsigned char* bytes = (char*) message;
    
    printf("[%010llu] ", clock_ms());
    if (debug) debug_printf("[%010llu] ", clock_ms());
    
    for (size_t i=0; i<width; i++) {
        printf("%02X", bytes[i]);
        if (debug) debug_printf("%02X", bytes[i]);
        if (i+1 < width) {
            printf(" ");
            if (debug) debug_printf(" ");
        }
    }
    
    printf(" | ");
    if (debug) debug_printf(" | ");
    
    for (size_t i=0; i<width; i++) {
        printf("%c", get_ascii(bytes[i]));
        if (debug) debug_printf("%c", get_ascii(bytes[i]));
    }
    
    printf("\n");
    if (debug) debug_printf("\n");
}


int main(void) {
    topicname = getenv("topic");
    
    if (topicname == NULL) {
        printf("Please provide a channel to monitor as \"topic=channelname\" (and optionally \"width=number\")");
        return 1;
    }
    
    if (getenv("width") != NULL) {
        width = (size_t)atol(getenv("width"));
    }
    
    debug = (getenv("debug") != NULL);
    
    MQStatus status, key_status;
    status = subscribe(topicname);
    if (status != MQ_SUCCESS) {
        printf("failed to subscribe to topic \"%s\", error %d\n", topicname, status);
        return 2;
    }
    
    KeyEvent key;
    void* message = malloc(width);
    if (message == NULL) {
        printf("failed to allocate %lu bytes of memory\n", width);
        return 3;
    }
    
    subscribe(keycode_topic);
    
    while (true) {
        status = poll(topicname, message);
        if (status != MQ_NO_MESSAGES && status != MQ_SUCCESS) {
            printf("[%010llu] error %s polling channel \"%s\".\n", clock_ms(), status, topicname);
            if (debug) debug_printf("[%010llu] error %s polling channel \"%s\".\n", clock_ms(), status, topicname);
        }
        
        if (status == MQ_SUCCESS) {
            format_message(message);
        }
        
        key_status = poll(keycode_topic, &key);
        if (key_status == MQ_SUCCESS && key.is_pressed && key.keycode == KEY_Escape) {
            printf("Escaping to shell...\n");
            break;
        }
        
        if (status != MQ_SUCCESS) yield();
    }
    
    unsubscribe(keycode_topic);
    
    unsubscribe(topicname);
    
    return 0;
}