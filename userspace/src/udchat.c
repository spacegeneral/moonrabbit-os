#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/term.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <rice.h>
#include <errno.h>
#include <sys/keycodes.h>
#include <libedit/readline.h>


bool running = false;
TermInfo tinfo;
socket_t *commsocket;
sockaddr target = {
    .port = 55555, 
    .addr = {.version = 4, .address = {255, 255, 255, 255}}
};


bool readline_handle_special_key(CharacterLine *line, KeyEvent *key) {
    setcursor(0, tinfo.termsize.h-1);
    return false;
}


void write_prompt() {
    char targetstr[SOCKADDR_STRING_LENGTH];
    format_sockaddr(targetstr, &target);
    printf("to ");
    setcolors(COLOR24_TEXT_HIGHLIGHT, SETCOLOR_UNCHANGED);
    printf("%s: ", targetstr);
    setcolors(COLOR24_TEXT, SETCOLOR_UNCHANGED);
}


void receiver(void) {
    char received[2048];
    char fromstr[SOCKADDR_STRING_LENGTH] = {0};
    size_t nrecv;
    sockaddr from;
    subscribe(commsocket->recv_topic);  // topic subscription is not inherited by child threads
    while (running) {
        nrecv = recvfrom(commsocket, received, 2048-1, &from);
        if (commsocket->errno != 0) {
            printf("socket recv error %d\n", commsocket->errno);
            running = false;
            unsubscribe(commsocket->recv_topic);
            exit(1);
        }
        received[nrecv] = '\0';
        rstrip(received);
        format_sockaddr(fromstr, &from);
        
        printf("\r");
        for (size_t i=0; i<tinfo.termsize.w-1; i++) printf(" ");
        
        setcolors(COLOR24_TEXT_HIGHLIGHT2, SETCOLOR_UNCHANGED);
        printf("\r%s> ", fromstr, received);
        setcolors(COLOR24_TEXT, SETCOLOR_UNCHANGED);
        printf("%s\n", received);
        write_prompt();
    }
    
    unsubscribe(commsocket->recv_topic);
    exit(0);
}


bool parse_command(char *command) {
    if (strcmp(command, "exit") == 0) {
        running = false;
        return true;
    } else if (memcmp(command, "target ", 7) == 0) {
        parse_ip(command+7, &(target.addr));
        return true;
    } else {
        size_t old_len = strlen(command);
        command[old_len] = '\n';
        command[old_len+1] = '\0';
        sendto(commsocket, command, strlen(command), &target);
        if (commsocket->errno != 0) printf("Socket error %d\n", commsocket->errno);
    }
    
    return false;
}


int main(void) {
    char message[2048];
    
    tinfo = getterminfo();
    running = true;
    
    commsocket = socket(SOCK_DGRAM, IPPROTO_UDP);
    if (commsocket == NULL) {
        printf("cannot open socket (error %d).\n", errno);
        return 1;
    }
    
    unsubscribe(commsocket->recv_topic);  // just to save memory
    
    spork_thread(&receiver);
    
    setcursor(0, tinfo.termsize.h-1);
    while (running) {
        write_prompt();
        readline_simple(message);
        for (size_t i=0; i<tinfo.termsize.w-1-strlen(message); i++) printf(" ");
        printf("\r");
        if (parse_command(message)) {
            setcolors(COLOR24_TEXT_HIGHLIGHT3, SETCOLOR_UNCHANGED);
            printf("[local command]\n");
            setcolors(COLOR24_TEXT, SETCOLOR_UNCHANGED);
        }
    }
    
    socket_close(commsocket);
    
    return 0;
}
