#include <stdio.h>
#include <stdlib.h>
#include <libgui/libgui.h>
#include <sys/proc.h>
#include <sys/term.h>
#include <sys/keycodes.h>


SoftTerminal info;
OpenDoor main_door;
bool running = true;


#define memfmt "mem useds/tot: %dK/%dK (%d%%)"

typedef struct Memstats {
    unsigned int free;
    unsigned int total_available;
} Memstats;


void keycode_callback(KeyEvent *keycode) {
    switch (keycode->keycode) {
        case KEY_Escape:
            running = false;
            break;
    }
}


int main(void) {
    BitmapFont font;
    
    font = load_lbf_font("f,def,init,osdata,unifont.lbf");
    if (font.codepoints == NULL) {
        printf("Could not load font!\n");
        return 1;
    }
    
    TermInfo ti = getterminfo();
    
    Rect main_area = {
        .x = ti.screensize.w - 250 - 1,
        .y = 0,
        .w = 250,
        .h = 50,
    };
    bool open_success = open_main_door(&main_door, main_area, "Mem-O-Meter");
    if (!open_success) {
        printf("doormem: Error opening main door\n");
        return 2;
    }
    
    main_door.keycode_callback = &keycode_callback;
    
    Rect label_pos = {.x = 5, .y = 5, .w = 245, .h = 40};
    SoftTerminal label = SoftTerminal_init(&(main_door.face), label_pos, font, make_color_8bpp(255, 0, 0), make_color_8bpp(90, 18, 68), 0, true);
    
    FILE *fp = fopen("f,sys,dial,robot,memstats","rb");
    if (fp == NULL) return 3;
    
    Memstats stats;
    Memstats old_stats;
    bool encounter_error = false;
    
    while (running) {
        OpenDoor_handle_io(&main_door);
        
        rewind(fp);
        if (fread(&stats, sizeof(Memstats), 1, fp) != 1) {
            if (!encounter_error) {
                encounter_error = true;
                SoftTerminal_clear(&label);
                SoftTerminal_gotoxy(&label, 0, 0);
                SoftTerminal_printf(&label, "error");
                OpenDoor_update(&main_door, true);
            }
        }
        else {
            unsigned int used = stats.total_available - stats.free;
            unsigned int used_k = used/1024;
            unsigned int total_k = stats.total_available/1024;
            encounter_error = false;
            if (stats.free != old_stats.free) {
                SoftTerminal_clear(&label);
                SoftTerminal_gotoxy(&label, 0, 0);
                SoftTerminal_printf(&label, memfmt, used_k, total_k, (used_k*100)/total_k);
                OpenDoor_update(&main_door, true);
            }
            old_stats = stats;
        }
        
        yield();
    }
    
    OpenDoor_close(&main_door);
    destroy_bitmap_font(font);
    fclose(fp);
    
    return 0;
}
