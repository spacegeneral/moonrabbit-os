#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/mq.h>
#include <sys/proc.h>
#include <collections/array.h>


void subscribe_multiple(Array *sources, Array *missing, bool do_create, long int channel_width) {
    ARRAY_FOREACH(curr, sources, {
        char* current = (char*) curr;
        MQStatus sub_status = subscribe(current);
        if (sub_status == MQ_DOES_NOT_EXIST) {
            if (do_create) {
                MQStatus advert_status = advertise_channel(current, 1024, channel_width);
                if (advert_status != MQ_SUCCESS) {
                    printf("Failed to advertise channel %s\n", current);
                    array_remove(sources, current, NULL);
                } else {
                    printf("Warning: topic %s does not exist, I'll create it.\n", current);
                    subscribe(current);
                }
            } else {
                printf("Warning: topic %s does not exist (yet), marked for retrying.\n", current);
                array_remove(sources, current, NULL);
                array_add(missing, current);
            }
        } else if (sub_status == MQ_AUTH_PROBLEM) {
            printf("Warning: lacking the security clearance to access topic %s.\n", current);
            array_remove(sources, current, NULL);
        }
    });
}

void advertise_as_needed(Array *sinks, long int channel_width) {
    ARRAY_FOREACH(curr, sinks, {
        char* current = (char*) curr;
        MQStatus advert_status = advertise_channel(current, 1024, channel_width);
        if (advert_status != MQ_ALREADY_EXISTS) {
            if (advert_status == MQ_SUCCESS) {
                printf("Warning: topic %s does not exist, I'll create it.\n", current);
            } else {
                printf("Failed to advertise channel %s\n", current);
                array_remove(sinks, current, NULL);
            }
        }
    });
}

void retry_subscribe_multiple(Array *sources, Array *missing) {
    ARRAY_FOREACH(curr, missing, {
        char* current = (char*) curr;
        MQStatus sub_status = subscribe(current);
        if (sub_status == MQ_SUCCESS) {
            array_remove(missing, current, NULL);
            array_add(sources, current);
        } else if (sub_status == MQ_AUTH_PROBLEM) {
            printf("Warning: lacking the security clearance to access topic %s.\n", current);
            array_remove(missing, current, NULL);
        }
    });
}

void publish_multiple(Array *sinks, void *message) {
    ARRAY_FOREACH(curr, sinks, {
        char* current = (char*) curr;
        publish(current, message);
    });
}

void poll_multiple(Array *sources, Array *sinks, void *buffer) {
    ARRAY_FOREACH(curr, sources, {
        char* current = (char*) curr;
        MQStatus poll_status;
        do {
            poll_status = poll(current, buffer);
            if (poll_status == MQ_SUCCESS) {
                publish_multiple(sinks, buffer);
            }
        } while (poll_status == MQ_SUCCESS);
    });
}


void print_network_status(Array *sources, Array *sinks, Array *missing) {
    size_t in_channel = 0;
    size_t boundary_len = 2;
    
#define PRINTSOURCE(ismissing) \
    { \
        for (size_t c=0; c<in_channel; c++) printf(" |"); \
        if (ismissing) { \
            printf(" +- (%s)\n", current); \
        } else { \
            printf(" +- %s\n", current); \
        } \
        in_channel++; \
    }
    
#define PRINTSINK() \
    { \
        printf("   "); \
        for (size_t c=0; c<in_channel - 1; c++) printf(" |"); \
        printf(" +--> %s\n", current); \
        in_channel--; \
    }
    
    ARRAY_FOREACH(curr, sources, {
        char* current = (char*) curr;
        PRINTSOURCE(false);
    });
    
    ARRAY_FOREACH(curr, missing, {
        char* current = (char*) curr;
        PRINTSOURCE(true);
    });
    
    for (size_t border=0; border<boundary_len-1; border++) {
        for (size_t c=0; c<in_channel; c++) printf(" |");
        printf("\n");
    }
    for (size_t c=0; c<in_channel; c++) printf(" V");
    printf("\n");
    
    size_t max_connection_side = (array_size(sources)+array_size(missing) > array_size(sinks) ? array_size(sources)+array_size(missing) : array_size(sinks));
    
    for (size_t c=0; c<max_connection_side+2; c++) printf("==");
    printf("\n");
    
    in_channel = array_size(sinks);
    
    for (size_t border=0; border<boundary_len-1; border++) {
        printf("   ");
        for (size_t c=0; c<in_channel; c++) printf(" |");
        printf("\n");
    }
    
    ARRAY_FOREACH(curr, sinks, {
        char* current = (char*) curr;
        PRINTSINK();
    });
}


int main(void) {
    Array *sources;
    Array *sinks;
    Array *missing;
    long int channel_width;
    void *buffer;
    
    array_new(&sources);
    array_new(&sinks);
    array_new(&missing);
    
    size_t num_env_vars = getenv_length();
    
    for (size_t ev=0; ev < num_env_vars; ev++) {
        char *current_var = environ[ev];
        if (memcmp(current_var, "in", 2) == 0) {
            array_add(sources, 
                      strchr(current_var, '=')+1);
        }
        else if (memcmp(current_var, "out", 3) == 0) {
            array_add(sinks, 
                      strchr(current_var, '=')+1);
        }
    }
    
    char *width_param = getenv("width");
    if (width_param == NULL) {
        channel_width = sizeof(int);
        printf("Channel width not specified, defaulting to %d\n", channel_width);
    } else {
        channel_width = atol(width_param);
    }
    
    bool do_create_src = (getenv("create_sources") != NULL);
    bool do_create_snk = (getenv("create_sinks") != NULL);
    bool quiet = (getenv("quiet") != NULL);
    
    buffer = malloc(channel_width);
    
    subscribe_multiple(sources, missing, do_create_src, channel_width);
    if (do_create_snk) {
        advertise_as_needed(sinks, channel_width);
    }
    //TODO check channel width
    
    if (array_size(sources) == 0 && array_size(missing) == 0) {
        printf("No valid sources specified!\n");
        return 1;
    }
    if (array_size(sinks) == 0) {
        printf("No valid sinks specified!\n");
        return 2;
    }
    
    if (!quiet) {
        print_network_status(sources, sinks, missing);
    }
    
    while (true) {
        retry_subscribe_multiple(sources, missing);
        poll_multiple(sources, sinks, buffer);
        yield();
        //TODO exit condition
    }
 
    return 0;
}

