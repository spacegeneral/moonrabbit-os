#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <libcrypto/libcrypto.h>


static char linenum_fmt[16];

#define SUBCMDLEN 1024

void print_help() {
    printf("Compute the sha256 checksum of a given file.\n");
    printf("Parameters:\n");
    printf("    file        path to the file to process.\n");
    printf("    chunk       size in bytes of the chunk. The file will be split\n");
    printf("                into chunk-sized pieces for processing.\n");
    printf("    progress    (no value) show a live-updated percentage of progress.\n");
    printf("    pure        (no value) Do not automatically decompress files, process\n");
    printf("                the raw, zlib-encoded data.\n");
    
}

int main(void) {
    char subcommand[SUBCMDLEN];
    char *fname;
    unsigned long int chunksize;
    bool showprogress = false;
    
    if (getenv("help") != NULL) {
        print_help();
        return 0;
    }
    
    char *file_param = getenv("file");
    if (file_param == NULL) {
        printf("File to read: ");
        getsn(subcommand, 1024);
        rstrip(subcommand);
        fname = subcommand;
    } else {
        fname = file_param;
    }
    
    char *chunksize_param = getenv("chunk");
    if (chunksize_param == NULL) {
        chunksize = 1;
    } else {
        chunksize = atoll(chunksize_param);
        if (chunksize == 0) {
            printf("Invalid chunksize");
            return 2;
        }
    }
    
    char *progress_param = getenv("progress");
    if (progress_param != NULL) {
        showprogress = true;
    } 
    
    bool open_pure = (getenv("pure") != 0);
    
    FILE *fp;
    if (open_pure) fp = fopen_no_zlib(fname, "r");
    else fp = fopen(fname, "r");
    
    if (fp == NULL) {
        printf("Error: cannot open %s\n", fname);
        return 1;
    }
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    char *chunk_mem = (char*) malloc(sizeof(char) * chunksize);
    
    SHA256_CTX context;
    unsigned char hash[32];
    sha256_init(&context);
    
    size_t steps = filesize / chunksize;
    if (filesize % chunksize != 0) steps++;
    for (size_t s=0; s<steps; s++) {
        size_t readbytes = fread(chunk_mem, 1, chunksize, fp);
        sha256_update(&context, chunk_mem, readbytes);
        if (showprogress) {
            printf("\r%03d%%", (s*100)/steps);
        }
    }
    printf("\r");
    sha256_final(&context, hash);
    for (size_t h=0; h<32; h++) {
        printf("%02X", hash[h]);
    }
    printf("\n");
    
    fclose(fp);
    free(chunk_mem);
 
    return 0;
}
 
