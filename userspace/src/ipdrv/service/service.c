#include "service.h"


void IPDRVServiceTable_dispatch(IPDRVServiceTable *table, int service, ShipyardBuffer* shipyard, void *data_start, size_t data_length) {
    if (service < 0 || service >= NUM_IPDRV_Services) return;
    if (table->table[service] != NULL) {
        table->table[service](shipyard, data_start, data_length);
    }
}

void IPDRVServiceTable_init_empty(IPDRVServiceTable *table) {
    for (int s=0; s<NUM_IPDRV_Services; s++) {
        table->table[s] = NULL;
    }
}
