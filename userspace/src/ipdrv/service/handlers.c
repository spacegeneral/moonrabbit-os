#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/network.h>

#include "service.h"
#include "../ipdrv.h"
#include "../protocol/arp.h"
#include "../protocol/icmp.h"
#include "../protocol/dhcp.h"
#include "../tables.h"
#include "../schopenhauer.h"
#include "../endianness.h"
#include "../self_description.h"
#include "../blueprints/blueprints.h"


ipdrv_service_callback pong_callback = NULL;


void arp_table_updater(ShipyardBuffer* shipyard, void *data_start, size_t data_length) {    
    ARPFrame *arp_msg = (ARPFrame*) data_start;
    (void)(shipyard);  // unused
    
    if (get_bigendian_word(arp_msg->operation) == ARP_OPER_REPLY) {
        IpAddress sender_ip = {.version = 4};
        memcpy(sender_ip.address, arp_msg->sender_protocol_address, IPv4_ADDRESS_LENGTH);
        arp_table_insert(arp_msg->sender_hw_address, &sender_ip);
#ifdef IPDRV_ADVANCED_DEBUG
        char macstr[MAC_STRING_LENGTH];
        char ipstr[MAC_STRING_LENGTH];
        format_mac(macstr, arp_msg->sender_hw_address);
        format_ip(ipstr, &sender_ip);
        debug_printf("ipdrv: updated arp table %s -> %s\r\n", ipstr, macstr);
#endif
    }
    else if (get_bigendian_word(arp_msg->operation) == ARP_OPER_REQUEST) {
        if (should_arp_respond(shipyard->src_mac)) {
            issue_arp_response(&(self_description.lan_ip), arp_msg);
        }
    }
    
}




void icmp_receiver_and_ponger(ShipyardBuffer* shipyard, void *data_start, size_t data_length) {    
    ICMPPacket *icmp_packet = (ICMPPacket*) data_start;
    
    switch (icmp_packet->header.type) {
        case ICMPv6_ECHO_REQUEST: /* fallthrough */
        case ICMP_ECHO_REQUEST:
            {
                if (!should_ping_back(&(shipyard->src_ip))) return;
                arp_as_needed(&(shipyard->src_ip));
                issue_icmp_pong(&(shipyard->src_ip), 
                                get_bigendian_word(icmp_packet->header.rest.echo.identifier), 
                                get_bigendian_word(icmp_packet->header.rest.echo.sequence_number),
                                data_start + sizeof(ICMPHeader),
                                data_length - sizeof(ICMPHeader));
            }
        break;
        case ICMPv6_ECHO_REPLY: /* fallthrough */
        case ICMP_ECHO_REPLY:
            {
                arp_as_needed(&(shipyard->src_ip));
                if (pong_callback != NULL) pong_callback(shipyard, data_start, data_length);
            }
        break;
        /*case ICMP_ROUTER_ADVERTISEMENT:
        case ICMPv6_ROUTER_ADVERTISEMENT:
            {
                arp_table_insert(shipyard->src_mac, &(self_description.gateway_ip));
#ifdef IPDRV_ADVANCED_DEBUG
                char macstr[MAC_STRING_LENGTH];
                char ipstr[MAC_STRING_LENGTH];
                format_mac(macstr, shipyard->src_mac);
                format_ip(ipstr, &(self_description.gateway_ip));
                debug_printf("ipdrv: updated gateway %s -> %s\r\n", ipstr, macstr);
#endif
            }
        break;*/
    }
}




void dhcp_receiver(ShipyardBuffer* shipyard, void *data_start, size_t data_length) {
    DHCPHeader *header = (DHCPHeader*) data_start;
    
    if (get_bigendian_dword(header->xid) != self_description.dhcp_client_state.current_transaction_id) return;  // not this transaction id
    if (memcmp(header->client_hw_address, self_description.nic_manifest.mac, MAC_ADDRESS_LENGTH) != 0) return;  // not this client mac
    
    uint8_t *typecode;
    size_t _dontcare;
    bool has_dhcp_type = find_dhcp_option(header, DHCP_OPTION_MESSAGE_TYPE, (void**)&typecode, &_dontcare);
    if (!has_dhcp_type) return; //wot
    
    if ((header->opcode == 2 && *typecode == 2) && \
        self_description.dhcp_client_state.status == DHCP_CLIENT_DISCOVERY) {  // received dhcp offer
        
        self_description.dhcp_client_state.offered_address.version = 4;
        memcpy(self_description.dhcp_client_state.offered_address.address, header->your_ip, IPv4_ADDRESS_LENGTH);
        
        self_description.dhcp_client_state.next_server.version = 4;
        memcpy(self_description.dhcp_client_state.next_server.address, header->server_ip, IPv4_ADDRESS_LENGTH);
        
        uint8_t *router_address_v4;
        if (find_dhcp_option(header, DHCP_OPTION_ROUTER, (void**)&router_address_v4, &_dontcare)) {
            self_description.dhcp_client_state.router.version = 4;
            memcpy(&(self_description.dhcp_client_state.router.address), router_address_v4, IPv4_ADDRESS_LENGTH);
        }
        
        self_description.dhcp_client_state.status = DHCP_CLIENT_OFFERED;
    }
    
    if ((header->opcode == 2 && *typecode == 5) && \
        self_description.dhcp_client_state.status == DHCP_CLIENT_REQUEST) {  // received dhcp ack
        
        memcpy(self_description.dhcp_client_state.candidate_router_mac, shipyard->src_mac, MAC_ADDRESS_LENGTH);
        self_description.dhcp_client_state.status = DHCP_CLIENT_ACKNOWLEDGED;
    }
}



void udp_dispatcher(ShipyardBuffer* shipyard, void *data_start, size_t data_length) {
    SocketDescriptor *sock = reverse_match_on_receive(shipyard);
    if (sock != NULL) {
        sockaddr from = {
            .port = shipyard->src_port,
            .addr = shipyard->src_ip,
        };
#ifdef IPDRV_ADVANCED_DEBUG
        debug_printf("Received packed on socket %d\r\n", sock->sock_id);
        ShipyardBuffer_debug(shipyard);
#endif
        receive_into_socket(sock, &from, data_start, data_length);
    }
}
