#ifndef _IPDRV_SESSION_SESSION_H
#define _IPDRV_SESSION_SESSION_H


#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <time.h>
#include <sys/network.h>


typedef struct NetSession {
    
    IpAddress lan_ip;
    IpAddress gateway_ip;
    unsigned char gateway_mac[MAC_ADDRESS_LENGTH];
    time_t created;
    
} NetSession;



bool save_session(const char *fname, NetSession *session);
bool load_session(const char *fname, NetSession *session);

bool remember_session(const char *fname);
bool restore_session(const char *fname);



#endif
