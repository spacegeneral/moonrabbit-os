#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/network.h>

#include "armor.h"
#include "pilot.h"
#include "shipyard.h"
#include "endianness.h"
#include "protocol/ethernet.h"
#include "protocol/ip.h"
#include "protocol/arp.h"
#include "protocol/icmp.h"
#include "protocol/dhcp.h"
#include "service/service.h"





bool disassemble_udp_layer(ShipyardBuffer* shipyard, IPDRVServiceTable *handlers) {
    if (try_disassemble_udp_armor(shipyard)) {
        
        if (is_message_dhcp(shipyard)) {
            IPDRVServiceTable_dispatch(handlers, DHCP_Service, shipyard, shipyard->application_data, shipyard->application_length);
            return true;
        } else {
            IPDRVServiceTable_dispatch(handlers, UDP_Service, shipyard, shipyard->application_data, shipyard->application_length);
        }
        
    } else return false;
    
    return true;
}



bool disassemble_ipv4_layer(ShipyardBuffer* shipyard, IPDRVServiceTable *handlers) {
    if (try_disassemble_ipv4_armor(shipyard)) {
        size_t ipv4_header_length = get_ip_header_length(shipyard);
        size_t ipv4_data_length = shipyard->network_total_length - ipv4_header_length;
       
        switch (shipyard->transport_protocol) {
            case PROTO_TCP:
                //TODO disassemble TCP
                return true;
                
            case PROTO_UDP:
                return disassemble_udp_layer(shipyard, handlers);
                
            case PROTO_ICMP:
                {
                    ICMPPacket *payload;
                    eject_icmp_pilot(shipyard, &payload, ipv4_data_length);
                    IPDRVServiceTable_dispatch(handlers, ICMP_Service, shipyard, payload, ipv4_data_length);
                    return true;
                }
                
            default:
                return false;
        }
        
    } else return false;
    
    return true;
}


bool disassemble_ipv6_layer(ShipyardBuffer* shipyard, IPDRVServiceTable *handlers) {
    if (try_disassemble_ipv6_armor(shipyard)) {
        size_t ipv6_header_length = get_ip_header_length(shipyard);
        size_t ipv6_data_length = shipyard->network_total_length - ipv6_header_length;
       
        switch (shipyard->transport_protocol) {
            case PROTO_TCP:
                //TODO disassemble TCP
                return true;
                
            case PROTO_UDP:
                return disassemble_udp_layer(shipyard, handlers);
                
            case PROTO_ICMP:
                {
                    ICMPPacket *payload;
                    eject_icmp_pilot(shipyard, &payload, ipv6_data_length);
                    IPDRVServiceTable_dispatch(handlers, ICMP_Service, shipyard, payload, ipv6_data_length);
                    return true;
                }
                
            default:
                return false;
        }
        
    } else return false;
    
    return true;
}


bool disassemble_received_data(ShipyardBuffer* shipyard, IPDRVServiceTable *handlers) {
    uint64_t *packet_length_tag = (uint64_t*)(shipyard->buffer + DEFAULT_SHIPYARD_MARGIN - sizeof(uint64_t));
    // try disassembling the ethernet layer
    if (try_disassemble_ethernet_armor(shipyard, (size_t)(*packet_length_tag))) {
       
        switch (shipyard->network_ethertype) {
            case EtherType_ipv4:
                return disassemble_ipv4_layer(shipyard, handlers);
            case EtherType_ipv6:
                return disassemble_ipv6_layer(shipyard, handlers);
                
            case EtherType_arp:
                {
                    ARPFrame *payload;
                    eject_arp_pilot(shipyard, &payload);
                    IPDRVServiceTable_dispatch(handlers, ARP_Service, shipyard, payload, sizeof(ARPFrame));
                    return true;
                }
                
            default:
                return false;
        }
        
    } else return false;
    
    return true;
}
