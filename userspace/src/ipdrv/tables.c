#include <stdlib.h>
#include <string.h>

#include "tables.h"
#include "self_description.h"
#include "tcp/status_transitions.h"



ARPTableEntry *arp_table;
size_t num_arp_table_entries;



static void setup_arp_table() {
    arp_table = (ARPTableEntry*) malloc(sizeof(ARPTableEntry) * DEFAULT_ARP_TABLE_SIZE);
    num_arp_table_entries = 0;
    
    arp_table_insert(bcast_mac, &bcast_ipv4);  // broadcast address
}

void arp_table_insert(unsigned char *mac, IpAddress *ip) {
    for (size_t row = 0; row < num_arp_table_entries; row++) {
        if (ip_equals(ip, &(arp_table[row].ip))) {
            memcpy(arp_table[row].mac, mac, MAC_ADDRESS_LENGTH); // update mac
            return;
        }
        if (memcmp(arp_table[row].mac, mac, MAC_ADDRESS_LENGTH) == 0) {
            memcpy(&(arp_table[row].ip), ip, sizeof(IpAddress)); // update ip
            return;
        }
    }
    
    if (num_arp_table_entries >= DEFAULT_ARP_TABLE_SIZE) num_arp_table_entries--;  // don't overflow: overwrite the last entry
    
    memcpy(arp_table[num_arp_table_entries].mac, mac, MAC_ADDRESS_LENGTH);
    memcpy(&(arp_table[num_arp_table_entries].ip), ip, sizeof(IpAddress));
    num_arp_table_entries++;
}


unsigned char *arp_table_match_ip_r(unsigned char *mac, IpAddress *ip) {
    for (size_t row = 0; row < num_arp_table_entries; row++) {
        if (ip_equals(ip, &(arp_table[row].ip))) {
            memcpy(mac, arp_table[row].mac, MAC_ADDRESS_LENGTH); // fetch mac
            return mac;
        }
    }
    return NULL;
}

unsigned char *arp_table_match_ip(IpAddress *ip) {
    static char mac[MAC_ADDRESS_LENGTH];
    return arp_table_match_ip_r(mac, ip);
}

IpAddress *arp_table_match_mac_r(unsigned char *mac, IpAddress *ip) {
    for (size_t row = 0; row < num_arp_table_entries; row++) {
        if (memcmp(arp_table[row].mac, mac, MAC_ADDRESS_LENGTH) == 0) {
            memcpy(ip, &(arp_table[row].ip), sizeof(IpAddress)); // fetch ip address
            return ip;
        }
    }
    return NULL;
}

IpAddress *arp_table_match_mac(unsigned char *mac) {
    static IpAddress ip;
    return arp_table_match_mac_r(mac, &ip);
}

unsigned char *link_level_routing(IpAddress *ip) {
    unsigned char *lan_mac = arp_table_match_ip(ip);
    if (lan_mac == NULL) {
        return arp_table_match_ip(&(self_description.gateway_ip));
    }
    return lan_mac;
}





SocketDescriptor* socket_table;



static void setup_socket_table() {
    socket_table = (SocketDescriptor*) malloc(sizeof(SocketDescriptor) * MAX_SOCKETS);
    for (size_t i=0; i<MAX_SOCKETS; i++) {
        socket_table[i].sock_id = INVALID_SOCKED_ID;
    }
}

int get_free_socket_id() {
    for (int i=0; i<MAX_SOCKETS; i++) {
        if (socket_table[i].sock_id == INVALID_SOCKED_ID) return i;
    }
    return INVALID_SOCKED_ID;
}

SocketDescriptor *get_socket_by_id(int sock_id) {
    if (sock_id < 0 || sock_id >= MAX_SOCKETS) return NULL;
    
    SocketDescriptor *ret = socket_table + sock_id;
    return ret;
}

SocketDescriptor *reverse_match_on_receive(ShipyardBuffer *shipyard) {
    for (int i=0; i<MAX_SOCKETS; i++) {
        if (socket_table[i].sock_id == INVALID_SOCKED_ID) continue;
        
        switch (socket_table[i].protocol) {
            case IPPROTO_TCP:
                //TODO implement
            break;
            case IPPROTO_UDP:
                {
                    UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) socket_table[i].connection_params;

                    if (params->source.port != shipyard->dest_port) continue;  // skipped: not targeted to this port
                    if (!ip_equals(&(params->source.addr), &(shipyard->dest_ip))) continue;  // skipped: not targeted to this IP

                    if (params->filter.port != 0) {
                        if (params->filter.port != shipyard->src_port) continue;  // skipped: filtered by source port
                    }

                    if (params->filter.addr.version == 6) {
                        if (!ip_equals(&(params->filter.addr), &(sockaddr_nofilter_v6.addr))) {
                            if (!ip_equals(&(params->filter.addr), &(shipyard->src_ip))) continue;  // skipped: filtered by source ipv6
                        }
                    } else {
                        if (!ip_equals(&(params->filter.addr), &(sockaddr_nofilter_v4.addr))) {
                            if (!ip_equals(&(params->filter.addr), &(shipyard->src_ip))) continue;  // skipped: filtered by source ipv4
                        }
                    }
                    
                    return socket_table + i;
                }
            break;
        }
        
    }
    return NULL;
}

bool test_sock_src_port_available(int port) {
    for (int i=0; i<MAX_SOCKETS; i++) {
        if (socket_table[i].sock_id == INVALID_SOCKED_ID) continue;
        
        switch (socket_table[i].protocol) {
            case IPPROTO_TCP:
                //TODO implement
            break;
            case IPPROTO_UDP:
                {
                    UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) socket_table[i].connection_params;
                    if (params->source.port == port) return false;
                }
            break;
        }
    }
    return true;
}



void setup_tables() {
    setup_arp_table();
    setup_socket_table();
    setup_tcp_machine_tables();
}
