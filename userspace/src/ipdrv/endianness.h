#ifndef _IPDRV_ENDIANNESS_H
#define _IPDRV_ENDIANNESS_H


#include <stdint.h>



void put_bigendian_word(uint16_t number, uint8_t *dest);
void put_bigendian_dword(uint32_t number, uint8_t *dest);

uint16_t get_bigendian_word(uint16_t number_be);
uint32_t get_bigendian_dword(uint32_t number_be);

#endif
 
