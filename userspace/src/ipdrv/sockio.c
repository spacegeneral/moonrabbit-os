#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>
#include <yakumo.h>
#include <sys/proc.h>
#include <sys/mq.h>
#include <sys/socket.h>
#include <sys/errno.h>


#include "ipdrv.h"
#include "tables.h"
#include "protocol/tcp.h"
#include "protocol/udp.h"
#include "blueprints/blueprints.h"
#include "self_description.h"


int get_random_ephemeral_port() {
    int candidate_port;
    int timeout = 100000;
    
    do {
        candidate_port = (rand() % (EPHEMERAL_PORT_RANGE_END - EPHEMERAL_PORT_RANGE_START)) + EPHEMERAL_PORT_RANGE_START;
        if (timeout-- <= 0) return 0;
    } while (!test_sock_src_port_available(candidate_port));

    return candidate_port;
}


size_t get_socket_mtu(SocketDescriptor *sock) {
    switch (sock->protocol) {
        case IPPROTO_TCP:
            return MAX_IP_MTU - sizeof(TCPHeader);
        case IPPROTO_UDP:
            return MAX_IP_MTU - sizeof(UDPHeader);
    }
    return 1;
}


static int fill_default_source_sockaddr(sockaddr *source) {
    source->port = get_random_ephemeral_port();
    if (source->port == 0) return SOCK_NO_PORTS_AVAIL;
    
    source->addr = self_description.lan_ip;
    return 0;
}


static int fill_default_destination_sockaddr(sockaddr *destination) {
    destination->port = DEFAULT_SOCK_PORT;
    destination->addr = bcast_ipv4;
    return 0;
}


static int finalize_new_socket(SocketDescriptor *newsock, unsigned int requestor_task_id) {
    switch (newsock->protocol) {
        case IPPROTO_UDP:
            {
                snprintf(newsock->rx_topic, 4096, DEFAULT_SOCKET_RECV_FMT_TOPIC, newsock->sock_id);
                snprintf(newsock->tx_topic, 4096, DEFAULT_SOCKET_SEND_FMT_TOPIC, newsock->sock_id);
                
                PhisicalDomainClass sock_channel_race;
                DataDomainClass sock_channel_class;
                if (negotiate_task_common_clearance(ident().taskid, requestor_task_id, &sock_channel_race, &sock_channel_class)) {
                    return NO_SOCK_AVAILABLE;
                }
                
                MQStatus setup_status;
                size_t channel_width = 1;  // stream mode, send characters
                size_t channel_length = DEFAULT_SOCK_STREAM_BUFFER;
                if (newsock->type == SOCK_DGRAM) {  // datagram mode, send datagrams
                    channel_width = sizeof(SockUDPDatagram);
                    channel_length = DEFAULT_SOCK_DGRAM_BUFFER;
                }
        
                setup_status = advertise_channel_advanced(newsock->rx_topic, channel_length, channel_width, sock_channel_race, sock_channel_class, SecFullyEnforced|SecNoDataLoss);
                if (setup_status != MQ_SUCCESS && setup_status != MQ_ALREADY_EXISTS) {
                    return MQ_ADVERT_ERRORS + setup_status;
                }
                setup_status = advertise_channel_advanced(newsock->tx_topic, channel_length, channel_width, sock_channel_race, sock_channel_class, SecFullyEnforced|SecNoDataLoss);
                if (setup_status != MQ_SUCCESS && setup_status != MQ_ALREADY_EXISTS) {
                    return MQ_ADVERT_ERRORS + setup_status;
                }
                
                UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) malloc(sizeof(UDPSocketConnectionParams));
                newsock->connection_params = params;
                int src_err = fill_default_source_sockaddr(&(params->source));
                if (src_err != 0) return src_err;
                int dst_err = fill_default_destination_sockaddr(&(params->destination));
                if (dst_err != 0) return dst_err;
                if (params->destination.addr.version == 6) {
                    params->filter = sockaddr_nofilter_v6;
                } else {
                    params->filter = sockaddr_nofilter_v4;
                }
                subscribe(newsock->tx_topic);
                return 0;
            }
        break;
        case IPPROTO_TCP:
            {
                //TODO
                return NOT_IMPLEMENTED;
            }
        break;
    }
    
    return NOT_IMPLEMENTED;
}


static int terminate_socket(SocketDescriptor *sock) {
    unsubscribe(sock->tx_topic);
    retire_channel(sock->tx_topic);
    retire_channel(sock->rx_topic);
    
    switch (sock->protocol) {
        case IPPROTO_UDP:
            {
#ifdef IPDRV_ADVANCED_DEBUG
                debug_printf("ipdrv sokio terminating sock_id=%d\r\n", sock->sock_id);
#endif
                sock->sock_id = INVALID_SOCKED_ID;
                free(sock->connection_params);
                return 0;
            }
        break;
        case IPPROTO_TCP:
            {
                //TODO
                return NOT_IMPLEMENTED;
            }
        break;
    }
    
    return NOT_IMPLEMENTED;
}


static int target_sock(SocketDescriptor *sock, sockaddr target, bool use_port, bool use_addr) {
    switch (sock->protocol) {
        case IPPROTO_UDP:
            {
                UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) sock->connection_params;
                if (use_port) params->destination.port = target.port;
                if (use_addr) {
                    if (target.addr.version != params->source.addr.version) return SOCK_IP_VERSION_MISMATCH;
                    params->destination.addr = target.addr;
                }
                return 0;
            }
        break;
        case IPPROTO_TCP:
            {
                //TODO
                return NOT_IMPLEMENTED;
            }
        break;
    }
    
    return NOT_IMPLEMENTED;
}


static int source_sock(SocketDescriptor *sock, sockaddr target, bool use_port, bool use_addr) {
    switch (sock->protocol) {
        case IPPROTO_UDP:
            {
                UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) sock->connection_params;
                if (use_port) {
                    if (!test_sock_src_port_available(target.port)) return SOCK_NO_PORTS_AVAIL;
                    params->source.port = target.port;
                }
                if (use_addr) {
                    if (target.addr.version != params->destination.addr.version) return SOCK_IP_VERSION_MISMATCH;
                    params->source.addr = target.addr;
                }
                return 0;
            }
        break;
        case IPPROTO_TCP:
            {
                //TODO
                return NOT_IMPLEMENTED;
            }
        break;
    }
    
    return NOT_IMPLEMENTED;
}


static int filter_sock(SocketDescriptor *sock, sockaddr target, bool use_port, bool use_addr) {
    switch (sock->protocol) {
        case IPPROTO_UDP:
            {
                UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) sock->connection_params;
                if (use_port) params->filter.port = target.port;
                if (use_addr) {
                    if (target.addr.version != params->source.addr.version) return SOCK_IP_VERSION_MISMATCH;
                    params->filter.addr = target.addr;
                }
                return 0;
            }
        break;
        case IPPROTO_TCP:
            {
                //TODO
                return NOT_IMPLEMENTED;
            }
        break;
    }
    
    return NOT_IMPLEMENTED;
}



void handle_sock_control_request(SocketControlServiceMessage *response, unsigned int requestor_task_id) {
    switch (response->verb) {
        case sock_get_free_number:
            {
                int sock_id = get_free_socket_id();
                if (sock_id != INVALID_SOCKED_ID) {
                    response->msg.get_free_number.free_number = sock_id;
                    response->status = 0;
                } else {
                    response->status = NO_SOCK_AVAILABLE;
                }
            }
        break;
        case sock_setup_socket_with_number:
            {
                int sock_id = response->msg.setup_socket_with_number.number;
                SocketDescriptor *newsock = get_socket_by_id(sock_id);
                if (newsock == NULL) {
                    response->status = SOCK_ID_INVALID;
#ifdef IPDRV_ADVANCED_DEBUG
                    debug_printf("ipdrv sokio sock_setup_socket_with_number: provided invalid sock_id=%d\r\n", sock_id);
#endif
                } else if (newsock->sock_id != INVALID_SOCKED_ID) {
                    response->status = NO_SOCK_AVAILABLE;
#ifdef IPDRV_ADVANCED_DEBUG
                    debug_printf("ipdrv sokio sock_setup_socket_with_number: provided occupied sock_id=%d\r\n", sock_id);
#endif
                } else {
                    newsock->sock_id = sock_id;
                    newsock->type = response->msg.setup_socket_with_number.type;
                    newsock->protocol = response->msg.setup_socket_with_number.protocol;
                    newsock->endpoint_task_id = requestor_task_id;
                    newsock->connection_params = NULL;
                    response->status = finalize_new_socket(newsock, requestor_task_id);
#ifdef IPDRV_ADVANCED_DEBUG
                    debug_printf("ipdrv sokio sock_setup_socket_with_number sock_id=%d status=%d (type=%d, proto=%d)\r\n", sock_id, response->status, newsock->type, newsock->protocol);
#endif
                }
            }
        break;
        case sock_teardown_socket_with_number:
            {
                int sock_id = response->msg.teardown_socket_with_number.number;
                SocketDescriptor *sock = get_socket_by_id(sock_id);
                if (sock == NULL) {
                    response->status = SOCK_ID_INVALID;
                } else if (sock->endpoint_task_id != requestor_task_id) {
                    response->status = SECURITY_DENIED;
                } else {
                    response->status = terminate_socket(sock);
                }
            }
        break;
        case sock_target_socket:
            {
                int sock_id = response->msg.target_socket.number;
                SocketDescriptor *sock = get_socket_by_id(sock_id);
                if (sock == NULL) {
                    response->status = SOCK_ID_INVALID;
                } else if (sock->endpoint_task_id != requestor_task_id) {
                    response->status = SECURITY_DENIED;
                } else {
                    response->status = target_sock(sock, response->msg.target_socket.sock_addr, response->msg.target_socket.use_port, response->msg.target_socket.use_addr);
                }
            }
        break;
        case sock_source_socket:
            {
                int sock_id = response->msg.source_socket.number;
                SocketDescriptor *sock = get_socket_by_id(sock_id);
                if (sock == NULL) {
                    response->status = SOCK_ID_INVALID;
                } else if (sock->endpoint_task_id != requestor_task_id) {
                    response->status = SECURITY_DENIED;
                } else {
                    response->status = source_sock(sock, response->msg.source_socket.sock_addr, response->msg.source_socket.use_port, response->msg.source_socket.use_addr);
                }
            }
        break;
        case sock_filter_socket:
            {
                int sock_id = response->msg.filter_socket.number;
                SocketDescriptor *sock = get_socket_by_id(sock_id);
                if (sock == NULL) {
                    response->status = SOCK_ID_INVALID;
                } else if (sock->endpoint_task_id != requestor_task_id) {
                    response->status = SECURITY_DENIED;
                } else {
                    response->status = filter_sock(sock, response->msg.filter_socket.sock_addr, response->msg.filter_socket.use_port, response->msg.filter_socket.use_addr);
                }
            }
        break;
        case sock_flush_socket_explicit:
            {
                int sock_id = response->msg.flush_socket_explicit.number;
                SocketDescriptor *sock = get_socket_by_id(sock_id);
                if (sock == NULL) {
                    response->status = SOCK_ID_INVALID;
                } else if (sock->endpoint_task_id != requestor_task_id) {
                    response->status = SECURITY_DENIED;
                } else {
                    flush_socket(sock);
                    response->status = 0;
                }
            }
        break;
        
        default:
            response->status = NOT_IMPLEMENTED;
    }
}


static void flush_udp_socket_stream(SocketDescriptor *sock) {
    int per_sock_bufsize = 0;
    int mtu = get_socket_mtu(sock);
    uint8_t *tmpbuf = malloc(mtu);
    bool has_chunks = true;
    MQStatus poll_status;
    UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) sock->connection_params;
    
    while (has_chunks) {
        per_sock_bufsize = 0;  // initialize empty buffer
        
        // fill the buffer until we're wither out of input or out of space
        do {
            poll_status = poll(sock->tx_topic, tmpbuf + per_sock_bufsize);
            if (poll_status == MQ_SUCCESS) {
                per_sock_bufsize++;
            }
            
            if (per_sock_bufsize >= mtu) break;
        
        } while (poll_status == MQ_SUCCESS);
        
        
        if (per_sock_bufsize > 0) {  // if we found data, transmit it
            arp_as_needed(&(params->destination.addr));
#ifdef IPDRV_ADVANCED_DEBUG
            debug_printf("ipdrv sokio flushing out UDP stream sock_id=%d, packet_size=%ld\r\n", sock->sock_id, (size_t)per_sock_bufsize);
#endif
            issue_udp_packet(&(params->source), &(params->destination), (void*)tmpbuf, per_sock_bufsize);
        }
        
        has_chunks = (poll_status == MQ_SUCCESS);  // termination condition
    }
    
    free(tmpbuf);
}


static void flush_udp_socket_datagram(SocketDescriptor *sock) {
    int mtu = get_socket_mtu(sock);
    MQStatus poll_status;
    UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) sock->connection_params;
    SockUDPDatagram *tmpbuf = (SockUDPDatagram*) malloc(sizeof(SockUDPDatagram));
    
    // submit all the datagrams in the queue
    do {
        poll_status = poll(sock->tx_topic, tmpbuf);
        if (poll_status == MQ_SUCCESS) {
            arp_as_needed(&(params->destination.addr));
            size_t packet_size = min(mtu, tmpbuf->data_length);
#ifdef IPDRV_ADVANCED_DEBUG
            debug_printf("ipdrv sokio flushing out UDP datagram sock_id=%d, packet_size=%ld\r\n", sock->sock_id, packet_size);
#endif
            issue_udp_packet(&(params->source), &(params->destination), (void*)(tmpbuf->data), packet_size);
        }
    } while (poll_status == MQ_SUCCESS);
    
    free(tmpbuf);
}


static void flush_udp_socket(SocketDescriptor *sock) {
    switch (sock->type) {
        case SOCK_STREAM:
            flush_udp_socket_stream(sock);
        break;
        case SOCK_DGRAM:
            flush_udp_socket_datagram(sock);
        break;
    }
}


void flush_socket(SocketDescriptor *sock) {
    switch (sock->protocol) {
        case IPPROTO_UDP:
            flush_udp_socket(sock);
        break;
        case IPPROTO_TCP:
            //TODO
        break;
    }
}


static void receive_into_udp_socket_stream(SocketDescriptor *sock, sockaddr *from, void *data_start, size_t data_length) {
    uint8_t *data_bytes = (uint8_t*)data_start;
    UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) sock->connection_params;
    params->last_received_from = *from;

    MQStatus pub_status;
    size_t bytes_left = data_length;
    size_t bytes_written = 0;
    do {
        pub_status = publish_multi(sock->rx_topic, (void*)(data_bytes + bytes_written), bytes_left, &bytes_written);
        bytes_left -= bytes_written;
        if (pub_status == MQ_CONTINUES) yield();
    } while (pub_status == MQ_CONTINUES);
}


static void receive_into_udp_socket_datagram(SocketDescriptor *sock, sockaddr *from, void *data_start, size_t data_length) {
    UDPSocketConnectionParams *params = (UDPSocketConnectionParams*) sock->connection_params;
    params->last_received_from = *from;
    SockUDPDatagram *tmpbuf = (SockUDPDatagram*) malloc(sizeof(SockUDPDatagram));
    int mtu = get_socket_mtu(sock);
    
    tmpbuf->from = *from;
    tmpbuf->data_length = min(data_length, mtu);
    memcpy(tmpbuf->data, data_start, tmpbuf->data_length);
    
    publish(sock->rx_topic, tmpbuf);
    
    free(tmpbuf);
}


static void receive_into_udp_socket(SocketDescriptor *sock, sockaddr *from, void *data_start, size_t data_length) {
    switch (sock->type) {
        case SOCK_STREAM:
            receive_into_udp_socket_stream(sock, from, data_start, data_length);
        break;
        case SOCK_DGRAM:
            receive_into_udp_socket_datagram(sock, from, data_start, data_length);
        break;
    }
}


void receive_into_socket(SocketDescriptor *sock, sockaddr *from, void *data_start, size_t data_length) {
    switch (sock->protocol) {
        case IPPROTO_UDP:
            receive_into_udp_socket(sock, from, data_start, data_length);
        break;
        case IPPROTO_TCP:
            //TODO
        break;
    }
}


void terminate_sockets_by_taskid(int task_id) {
    for (int i=0; i<MAX_SOCKETS; i++) {
        if (socket_table[i].sock_id == INVALID_SOCKED_ID) continue;
        if (socket_table[i].endpoint_task_id == task_id) {
#ifdef IPDRV_ADVANCED_DEBUG
            debug_printf("ipdrv sokio terminating socked %d, cause: death of task %d\r\n", i, task_id);
#endif
            terminate_socket(socket_table + i);
        }
    }
}



void transmit_from_sockets() {
    for (int i=0; i<MAX_SOCKETS; i++) {
        if (socket_table[i].sock_id == INVALID_SOCKED_ID) continue;
        
        flush_socket(socket_table + i);
    }
}
