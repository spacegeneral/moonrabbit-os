#ifndef _IPDRV_TCP_SYN_SENT_H
#define _IPDRV_TCP_SYN_SENT_H


#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/network.h>

#include "../protocol/socket.h"
#include "../protocol/tcp.h"
#include "../shipyard.h"


void tcp_sock_syn_sent_recv_segment_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);

void tcp_sock_syn_sent_error_queuing_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);

void tcp_sock_syn_sent_error_aready_exists_event(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard);

#endif
