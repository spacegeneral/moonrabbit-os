#include <stdlib.h>

#include "common.h"

 
void init_tcb(TCPSocketConnectionParams *state) {
    state->snd.wnd = TCP_INITIAL_WINDOW_SIZE;
    state->snd.una = 0;
    state->snd.nxt = 0;
}

void destroy_tcb(TCPSocketConnectionParams *state) {
    
}

bool is_segment_seq_acceptable(TCPSocketConnectionParams *state, ShipyardBuffer *shipyard) {
    TCPHeader *segm = (TCPHeader*)shipyard->transport_header;
    size_t segm_length = shipyard->transport_total_length - sizeof(TCPHeader);
    
    if (state->rcv.wnd == 0) {
        if (segm_length == 0) {
            return (state->rcv.nxt <= segm->sequence_number) && \
                   (segm->sequence_number < (state->rcv.nxt + state->rcv.wnd));
        } else {
            return ((state->rcv.nxt <= segm->sequence_number) && \
                        (segm->sequence_number < (state->rcv.nxt + state->rcv.wnd))) || \
                   ((state->rcv.nxt <= (segm->sequence_number + segm_length - 1)) && \
                        ((segm->sequence_number + segm_length - 1) < (state->rcv.nxt + state->rcv.wnd)));
        }
    } else {
        if (segm_length == 0) {
            return (segm->sequence_number == state->rcv.nxt);
        } else {
            return false;
        }
    }
}