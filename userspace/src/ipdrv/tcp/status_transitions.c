#include <stdlib.h>
#include <stdio.h>

#include "status_transitions.h"

#include "closed.h"
#include "syn_sent.h"


tcp_machine_event           tcp_machine_trigger_table       [NUM_TCPCONN_STATUSES] [NUM_TCPCONN_EVENTS];


bool tcp_machine_transition_to(TCPSocketConnectionParams *state, TCPConnectionStatus to_status) {
    state->conn_status = to_status;
    return true;
}

void setup_tcp_trigger_table() {
    for (int a=0; a<NUM_TCPCONN_STATUSES; a++) {
        for (int b=0; b<NUM_TCPCONN_EVENTS; b++) {
            tcp_machine_trigger_table[a][b] = NULL;
        }
    }
    //TODO setup trigger functions
    tcp_machine_trigger_table[TCPCONN_CLOSED][TCPEVENT_ACTIVE_OPEN   ] = &tcp_sock_closed_active_open_event;
    tcp_machine_trigger_table[TCPCONN_CLOSED][TCPEVENT_PASSIVE_OPEN  ] = &tcp_sock_closed_passive_open_event;
    tcp_machine_trigger_table[TCPCONN_CLOSED][TCPEVENT_SEND          ] = &tcp_sock_closed_error_event;
    tcp_machine_trigger_table[TCPCONN_CLOSED][TCPEVENT_RECV          ] = &tcp_sock_closed_error_event;
    tcp_machine_trigger_table[TCPCONN_CLOSED][TCPEVENT_CLOSE         ] = &tcp_sock_closed_error_event;
    tcp_machine_trigger_table[TCPCONN_CLOSED][TCPEVENT_ABORT         ] = &tcp_sock_closed_error_event;
    tcp_machine_trigger_table[TCPCONN_CLOSED][TCPEVENT_SEGMENT_ARRIVE] = &tcp_sock_closed_recv_segment_event;
    
    tcp_machine_trigger_table[TCPCONN_SYN_SENT][TCPEVENT_ACTIVE_OPEN   ] = &tcp_sock_syn_sent_error_aready_exists_event;
    tcp_machine_trigger_table[TCPCONN_SYN_SENT][TCPEVENT_PASSIVE_OPEN  ] = &tcp_sock_syn_sent_error_aready_exists_event;
    tcp_machine_trigger_table[TCPCONN_SYN_SENT][TCPEVENT_SEND          ] = &tcp_sock_syn_sent_error_queuing_event;
    tcp_machine_trigger_table[TCPCONN_SYN_SENT][TCPEVENT_RECV          ] = &tcp_sock_syn_sent_error_queuing_event;
    //tcp_machine_trigger_table[TCPCONN_SYN_SENT][TCPEVENT_CLOSE         ] = &tcp_sock_closed_error_event;
    //tcp_machine_trigger_table[TCPCONN_SYN_SENT][TCPEVENT_ABORT         ] = &tcp_sock_closed_error_event;
    tcp_machine_trigger_table[TCPCONN_SYN_SENT][TCPEVENT_SEGMENT_ARRIVE] = &tcp_sock_syn_sent_recv_segment_event;
    
    
    
}

bool tcp_machine_receive_event(TCPSocketConnectionParams *state, TCPConnectionEvents event, ShipyardBuffer *shipyard) {
    tcp_machine_event trigger = tcp_machine_trigger_table[state->conn_status][event];
    if (trigger == NULL) {
        return false;
    }
    
    trigger(state, shipyard);
    return true;
}

void tcp_machine_error(TCPSocketConnectionParams *state, int errorcode) {
    // TODO
    printf("TCP error %d\n", errorcode);
}




void setup_tcp_machine_tables() {
    setup_tcp_trigger_table();
}
