#ifndef _IPDRV_TABLES_H
#define _IPDRV_TABLES_H


#include <sys/network.h>
#include "shipyard.h"
#include "protocol/socket.h"


typedef struct ARPTableEntry {
    unsigned char mac[MAC_ADDRESS_LENGTH];
    IpAddress ip;
} ARPTableEntry;


#define DEFAULT_ARP_TABLE_SIZE 128
extern ARPTableEntry *arp_table;
extern size_t num_arp_table_entries;

void arp_table_insert(unsigned char *mac, IpAddress *ip);
unsigned char *arp_table_match_ip_r(unsigned char *mac, IpAddress *ip);
unsigned char *arp_table_match_ip(IpAddress *ip);
IpAddress *arp_table_match_mac_r(unsigned char *mac, IpAddress *ip);
IpAddress *arp_table_match_mac(unsigned char *mac);

unsigned char *link_level_routing(IpAddress *ip);




#define MAX_SOCKETS 512
extern SocketDescriptor* socket_table;

int get_free_socket_id();
SocketDescriptor *get_socket_by_id(int sock_id);
SocketDescriptor *reverse_match_on_receive(ShipyardBuffer *shipyard);
bool test_sock_src_port_available(int port);




void setup_tables();




#endif
 
