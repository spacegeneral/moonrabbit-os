#ifndef _IPDRV_SHIPYARD_H
#define _IPDRV_SHIPYARD_H


#include <stdint.h>
#include <stddef.h>
#include <sys/network.h>


#define DEFAULT_SHIPYARD_MARGIN 64
#define DEFAULT_SHIPYARD_LENGTH (MAX_LINK_MTU + 2*(DEFAULT_SHIPYARD_MARGIN))


typedef struct __attribute__((packed)) ShipyardBuffer {
    uint8_t *buffer;
    
    unsigned char src_mac[MAC_ADDRESS_LENGTH];
    unsigned char dest_mac[MAC_ADDRESS_LENGTH];
    IpAddress src_ip;
    IpAddress dest_ip;
    int src_port;
    int dest_port;
    
    uint8_t *datalink_header;
    uint8_t *datalink_data;
    size_t datalink_total_length;
    
    int network_ethertype;
    
    uint8_t *network_header;
    uint8_t *network_data;
    size_t network_total_length;
    
    int transport_protocol;
    
    uint8_t *transport_header;
    uint8_t *transport_data;
    size_t transport_total_length;
    
    uint8_t *application_data;
    size_t application_length;
} ShipyardBuffer;



/*
 
   Shipyard buffer
   
   All the ISO stack layers are logically "flattened out"
   
   //TODO diagram
 
 
 
 */



void ShipyardBuffer_initialize(ShipyardBuffer* shipyard);
void ShipyardBuffer_reset(ShipyardBuffer* shipyard);
void ShipyardBuffer_destroy(ShipyardBuffer* shipyard);
void ShipyardBuffer_debug(ShipyardBuffer* shipyard);

void ShipyardBuffer_launch(ShipyardBuffer* shipyard, NetPacket **packet_view);
void ShipyardBuffer_dock(ShipyardBuffer* shipyard, NetPacket **inbound_packet_view);



#endif
 
