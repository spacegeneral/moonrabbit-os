#ifndef _IPDRV_PROTO_DHCP_H
#define _IPDRV_PROTO_DHCP_H

#include <stdint.h>
#include <sys/network.h>

#include "../shipyard.h"

#define DHCP_CHADDR_LEN 16
#define DHCP_SNAME_LEN  64
#define DHCP_FILE_LEN  128


#define DHCP_MAGIC_COOKIE 0x63825363
#define DHCP_BROADCAST_FLAG 0b1000000000000000


#define DHCP_OPTION_ROUTER                   3
#define DHCP_OPTION_MESSAGE_TYPE            53
#define DHCP_OPTION_REQUESTED_IP_ADDRESS    50
#define DHCP_OPTION_DHCP_SERVER             54
#define DHCP_OPTION_PARAM_LIST_REQUEST      55
#define DHCP_OPTION_ENDMARK                255


typedef enum DHCParam {
    dhcp_param_subnet_mask = 1,
    dhcp_param_router = 3,
    dhcp_param_domain_name = 15,
    dhcp_param_dns = 6,
    
} DHCParam;


typedef struct DHCPHeader {
    uint8_t    opcode;
    uint8_t    htype;
    uint8_t    hlen;
    uint8_t    hops;
    uint32_t   xid;
    uint16_t   secs;
    uint16_t   flags;
    
    uint8_t    client_ip[IPv4_ADDRESS_LENGTH];
    uint8_t    your_ip[IPv4_ADDRESS_LENGTH];
    uint8_t    server_ip[IPv4_ADDRESS_LENGTH];
    uint8_t    gateway_ip[IPv4_ADDRESS_LENGTH];
    uint8_t    client_hw_address[DHCP_CHADDR_LEN];
    
    char       server_name[DHCP_SNAME_LEN];
    char       boot_filename[DHCP_FILE_LEN];
    uint32_t   magic_cookie;
    //options
} DHCPHeader;



void build_dhcp_discovery(DHCPHeader **data, size_t *length, uint32_t transaction_identifier);
void build_dhcp_request(DHCPHeader **data, size_t *length, uint32_t transaction_identifier, IpAddress *dhcp_server_ip, IpAddress *offered);

bool is_message_dhcp(ShipyardBuffer* shipyard);
bool find_dhcp_option(DHCPHeader *header, uint8_t option_code, void **data, size_t *data_length);


#endif
