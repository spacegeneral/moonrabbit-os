#ifndef _IPDRV_PROTO_ICMP_H
#define _IPDRV_PROTO_ICMP_H


#include <stdint.h>
#include <sys/network.h>


#define DEFAULT_ICMP_ECHO_PAYLOAD_LENGTH 8


typedef struct __attribute__((packed)) ICMPHeader {
    uint8_t type;
    uint8_t code;
    uint16_t checksum;
    union {
        struct {
            uint16_t identifier;
            uint16_t sequence_number;
        } echo;
        struct {
            uint32_t reserved;
        } router_solicitation_all_versions;
        struct {
            uint8_t num_addrs;
            uint8_t addr_entry_size;
            uint16_t lifetime;
        } router_advertisement_v4;
        struct {
            uint16_t cur_hop_limit_flags;
            uint16_t lifetime;
        } router_advertisement_v6;
    } rest;
} ICMPHeader;



typedef struct __attribute__((packed)) ICMPPacket {
    ICMPHeader header;
    char payload[1];
} ICMPPacket;



typedef enum ICMPTypes {
    ICMP_ECHO_REPLY = 0,
    ICMP_DESTINATION_UNREACHABLE = 3,
    ICMP_SOURCE_QUENCH = 4,
    ICMP_REDIRECT = 5,
    ICMP_ECHO_REQUEST = 8,
    ICMP_ROUTER_ADVERTISEMENT = 9,
    ICMP_ROUTER_SOLICITATION = 10,
    ICMP_TIME_EXCEEDED = 11,
    ICMP_TRACEROUTE = 30,
    
} ICMPTypes;

typedef enum ICMPv6Types {
    ICMPv6_DESTINATION_UNREACHABLE = 1,
    ICMPv6_REDIRECT = 137,
    ICMPv6_ECHO_REPLY = 129,
    ICMPv6_ECHO_REQUEST = 128,
    ICMPv6_ROUTER_ADVERTISEMENT = 134,
    ICMPv6_ROUTER_SOLICITATION = 133,
    ICMPv6_TIME_EXCEEDED = 3,
    
} ICMPv6Types;




ICMPPacket *make_icmp_echo_request(int identifier, int sequence_number, int version, size_t payload_size);
ICMPPacket *make_icmp_echo_reply(int identifier, int sequence_number, int version, void* payload, size_t payload_size);
ICMPPacket *make_icmp_router_solicitation(int identifier, int sequence_number, int version);
void update_icmp_checksum(ICMPHeader *header, size_t data_length);


#endif
