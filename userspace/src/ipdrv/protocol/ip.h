#ifndef _IPDRV_PROTO_IP_H
#define _IPDRV_PROTO_IP_H


#include <stdint.h>
#include <sys/network.h>
#include "../shipyard.h"



#define MIN_IPv4_HEADER_LENGTH 20
#define MAX_IPv4_HEADER_LENGTH 60

#define IPv6_HEADER_LENGTH 40

#define DEFAULT_IP_TTL 128


typedef struct __attribute__((packed)) IPV4Header {
    uint8_t version_ihl;
    uint8_t dscp_ecn;
    uint16_t total_length;
    uint16_t identification;
    uint16_t flags_fragment_offset;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t header_checksum;
    uint8_t src_ip[IPv4_ADDRESS_LENGTH];
    uint8_t dest_ip[IPv4_ADDRESS_LENGTH];
    // options
} IPV4Header;



typedef struct __attribute__((packed)) IPV6Header {
    uint32_t version_tclass_flowlabel;
    uint16_t total_length;
    uint8_t protocol;
    uint8_t ttl;
    uint8_t src_ip[IPv6_ADDRESS_LENGTH];
    uint8_t dest_ip[IPv6_ADDRESS_LENGTH];
} IPV6Header;




typedef enum IpProtocolNumbers {
    PROTO_HOPOPT = 0,
    PROTO_ICMP = 1,
    PROTO_IGMP = 2,
    PROTO_TCP = 6,
    PROTO_UDP = 17,
    PROTO_ENCAP = 41,
    PROTO_OSPF = 89,
    PROTO_SCTP = 132,
    
} IpProtocolNumbers;


uint16_t ip_checksum(void* vdata, size_t length);
size_t get_ip_header_length(ShipyardBuffer* shipyard);


#endif
