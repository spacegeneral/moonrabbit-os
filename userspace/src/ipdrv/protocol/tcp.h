#ifndef _IPDRV_PROTO_TCP_H
#define _IPDRV_PROTO_TCP_H


#include <stdint.h>
#include "../shipyard.h"


typedef struct __attribute__((packed)) TCPHeader {
    uint16_t src_port;
    uint16_t dest_port;
    uint32_t sequence_number;
    uint32_t ack_number;
    uint16_t data_offset_and_flags;
    uint16_t window_size;
    uint16_t checksum;
    uint16_t urgent_pointer;
} TCPHeader;

#define TCP_FLAG_FIN    0b0000000000000001
#define TCP_FLAG_SYN    0b0000000000000010
#define TCP_FLAG_RST    0b0000000000000100
#define TCP_FLAG_PSH    0b0000000000001000
#define TCP_FLAG_ACK    0b0000000000010000
#define TCP_FLAG_URG    0b0000000000100000
#define TCP_FLAG_ECE    0b0000000001000000
#define TCP_FLAG_CWR    0b0000000010000000
#define TCP_FLAG_NS     0b0000000100000000


typedef struct __attribute__((packed)) TCPPseudoHeaderV4 {
    uint8_t src_ip[IPv4_ADDRESS_LENGTH];
    uint8_t dest_ip[IPv4_ADDRESS_LENGTH];
    uint8_t zeros;
    uint8_t protocol;
    uint16_t tcp_length;
    TCPHeader tcp_header;
} TCPPseudoHeaderV4;


typedef struct __attribute__((packed)) TCPPseudoHeaderV6 {
    uint8_t src_ip[IPv6_ADDRESS_LENGTH];
    uint8_t dest_ip[IPv6_ADDRESS_LENGTH];
    uint32_t tcp_length;
    uint8_t zeros[3];
    uint8_t protocol;
    TCPHeader tcp_header;
} TCPPseudoHeaderV6;


void update_tcp_checksum(ShipyardBuffer* shipyard);
TCPHeader* make_tcp_control_segment(ShipyardBuffer* shipyard, uint16_t src_port, uint16_t dest_port, uint16_t window_size, uint16_t flags, uint32_t sequence_number, uint32_t ack_number);

unsigned int generate_iss();


// see http://www.medianet.kent.edu/techreports/TR2005-07-22-tcp-EFSM.pdf

typedef enum TCPConnectionStatus {
    TCPCONN_CLOSED,
    TCPCONN_LISTEN,
    TCPCONN_SYN_SENT,
    TCPCONN_SYN_RCVD,
    TCPCONN_ESTABLISHED,
    TCPCONN_CLOSE_WAIT,
    TCPCONN_LAST_ACK,
    TCPCONN_FIN_WAIT_1,
    TCPCONN_FIN_WAIT_2,
    TCPCONN_CLOSING,
    TCPCONN_TIME_WAIT,
    
    
    NUM_TCPCONN_STATUSES
} TCPConnectionStatus;


typedef enum TCPConnectionEvents {
    TCPEVENT_ACTIVE_OPEN,
    TCPEVENT_PASSIVE_OPEN,
    TCPEVENT_SEND,
    TCPEVENT_RECV,
    TCPEVENT_CLOSE,
    TCPEVENT_ABORT,
    
    TCPEVENT_SEGMENT_ARRIVE,
    
    TCPEVENT_REXMT_TIMEOUT,
    TCPEVENT_TIMEWAIT_TIMEOUT,
    TCPEVENT_USERTIME_TIMEOUT,
    
    
    NUM_TCPCONN_EVENTS
} TCPConnectionEvents;


typedef enum TCPError {
    DESTINATION_SOCK_NOT_SPECIFIED,
    CONNECTION_DOES_NOT_EXIST,
    QUEUING_DATA_DURING_ESTABLISHED,
    CONNECTION_ALREADY_EXISTS,
    CONNECTION_RESET,
    
} TCPError;


#endif
