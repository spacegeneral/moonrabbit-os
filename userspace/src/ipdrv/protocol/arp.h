#ifndef _IPDRV_PROTO_ARP_H
#define _IPDRV_PROTO_ARP_H


#include <stdint.h>
#include <sys/network.h>


#define ARP_HW_ADDRESS_LENGTH 6
#define ARP_PROTOCOL_ADDRESS_LENGTH 4


typedef struct __attribute__((packed)) ARPFrame {
    uint16_t hardware_type;
    uint16_t protocol_type;
    uint8_t hardware_length;
    uint8_t protocol_length;
    uint16_t operation;
    
    uint8_t sender_hw_address[ARP_HW_ADDRESS_LENGTH];
    uint8_t sender_protocol_address[ARP_PROTOCOL_ADDRESS_LENGTH];
    uint8_t target_hw_address[ARP_HW_ADDRESS_LENGTH];
    uint8_t target_protocol_address[ARP_PROTOCOL_ADDRESS_LENGTH];
    
} ARPFrame;


typedef enum ARP_HTYPE {
    ARP_HTYPE_ETHERNET = 1,
} ARP_HTYPE;


typedef enum ARP_OPER {
    ARP_OPER_REQUEST = 1,
    ARP_OPER_REPLY = 2,
} ARP_OPER;



ARPFrame make_arp_probe(unsigned char *my_mac, unsigned char *target_ip);
ARPFrame make_arp_announcement(unsigned char *my_mac, unsigned char *my_ip);
ARPFrame make_arp_response(unsigned char *my_mac, unsigned char *my_ip, unsigned char *requestor_mac, unsigned char *requestor_ip);



#endif
