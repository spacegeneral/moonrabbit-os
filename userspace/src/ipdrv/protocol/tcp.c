#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <yakumo.h>
#include <sys/network.h>

#include "../shipyard.h"
#include "../endianness.h"
#include "tcp.h"
#include "ip.h"


#define TCP_DATA_OFFSET_NORMAL 0b0101000000000000


TCPHeader* make_tcp_control_segment(ShipyardBuffer* shipyard, uint16_t src_port, uint16_t dest_port, uint16_t window_size, uint16_t flags, uint32_t sequence_number, uint32_t ack_number) {
    TCPHeader *header = malloc(sizeof(TCPHeader));
    
    put_bigendian_word(src_port, (uint8_t*)&(header->src_port));
    put_bigendian_word(dest_port, (uint8_t*)&(header->dest_port));
    put_bigendian_dword(sequence_number, (uint8_t*)&(header->sequence_number));
    header->ack_number = ack_number;
    header->urgent_pointer = 0;
    header->data_offset_and_flags = TCP_DATA_OFFSET_NORMAL | flags;
    put_bigendian_word(window_size, (uint8_t*)&(header->window_size));
    
    header->checksum = 0;
    
    return header;
}

unsigned int generate_iss() {
    return rand() % 10000;
}


void update_tcp_checksum(ShipyardBuffer* shipyard) {
    void *checksummed_data;
    size_t checksummed_data_length;
    
    TCPPseudoHeaderV4 pseudoheaderv4;
    TCPPseudoHeaderV6 pseudoheaderv6;
    
    TCPHeader *header = (TCPHeader*) shipyard->transport_header;
    header->checksum = 0;
    
    switch (shipyard->src_ip.version) {
        case 4:
            {
                memcpy(pseudoheaderv4.src_ip, shipyard->src_ip.address, IPv4_ADDRESS_LENGTH);
                memcpy(pseudoheaderv4.dest_ip, shipyard->dest_ip.address, IPv4_ADDRESS_LENGTH);
                pseudoheaderv4.zeros = 0;
                pseudoheaderv4.protocol = shipyard->transport_protocol;
                put_bigendian_word(shipyard->transport_total_length, (uint8_t*)&(pseudoheaderv4.tcp_length));
                pseudoheaderv4.tcp_header = *header;
                
                checksummed_data = &pseudoheaderv4;
                checksummed_data_length = sizeof(TCPPseudoHeaderV4);
            }
        break;
        case 6:
            {
                memcpy(pseudoheaderv6.src_ip, shipyard->src_ip.address, IPv6_ADDRESS_LENGTH);
                memcpy(pseudoheaderv6.dest_ip, shipyard->dest_ip.address, IPv6_ADDRESS_LENGTH);
                pseudoheaderv6.zeros[0] = 0; pseudoheaderv6.zeros[1] = 0; pseudoheaderv6.zeros[2] = 0;
                pseudoheaderv6.protocol = shipyard->transport_protocol;
                put_bigendian_dword(shipyard->transport_total_length, (uint8_t*)&(pseudoheaderv6.tcp_length));
                pseudoheaderv6.tcp_header = *header;
                
                checksummed_data = &pseudoheaderv6;
                checksummed_data_length = sizeof(TCPPseudoHeaderV6);
            }
        break;
    }
    
    header->checksum = ip_checksum(checksummed_data, checksummed_data_length);
}
