#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <yakumo.h>
#include <sys/network.h>

#include "../shipyard.h"
#include "../endianness.h"
#include "arp.h"
#include "ethernet.h"



ARPFrame make_arp_probe(unsigned char *my_mac, unsigned char *target_ip) {
    ARPFrame frame;
    uint8_t *frame_bytes = (uint8_t*) &frame;
    
    put_bigendian_word(ARP_HTYPE_ETHERNET, frame_bytes);
    put_bigendian_word(EtherType_ipv4, frame_bytes+2);
    
    frame.hardware_length = ARP_HW_ADDRESS_LENGTH,
    frame.protocol_length = ARP_PROTOCOL_ADDRESS_LENGTH,
    
    put_bigendian_word(ARP_OPER_REQUEST, frame_bytes+6);
    
    memcpy(frame.sender_hw_address, my_mac, ARP_HW_ADDRESS_LENGTH);
    memset(frame.sender_protocol_address, 0, ARP_PROTOCOL_ADDRESS_LENGTH);
    
    memset(frame.target_hw_address, 0x00, ARP_HW_ADDRESS_LENGTH);
    memcpy(frame.target_protocol_address, target_ip, ARP_PROTOCOL_ADDRESS_LENGTH);
    
    return frame;
}


ARPFrame make_arp_announcement(unsigned char *my_mac, unsigned char *my_ip) {
    ARPFrame frame;
    uint8_t *frame_bytes = (uint8_t*) &frame;
    
    put_bigendian_word(ARP_HTYPE_ETHERNET, frame_bytes);
    put_bigendian_word(EtherType_ipv4, frame_bytes+2);
    
    frame.hardware_length = ARP_HW_ADDRESS_LENGTH,
    frame.protocol_length = ARP_PROTOCOL_ADDRESS_LENGTH,
    
    put_bigendian_word(ARP_OPER_REPLY, frame_bytes+6);
    
    memcpy(frame.sender_hw_address, my_mac, ARP_HW_ADDRESS_LENGTH);
    memcpy(frame.sender_protocol_address, my_ip, ARP_PROTOCOL_ADDRESS_LENGTH);
    
    memset(frame.target_hw_address, 0x00, ARP_HW_ADDRESS_LENGTH);
    memcpy(frame.target_protocol_address, my_ip, ARP_PROTOCOL_ADDRESS_LENGTH);
    
    return frame;
}


ARPFrame make_arp_response(unsigned char *my_mac, unsigned char *my_ip, unsigned char *requestor_mac, unsigned char *requestor_ip) {
    ARPFrame frame;
    uint8_t *frame_bytes = (uint8_t*) &frame;
    
    put_bigendian_word(ARP_HTYPE_ETHERNET, frame_bytes);
    put_bigendian_word(EtherType_ipv4, frame_bytes+2);
    
    frame.hardware_length = ARP_HW_ADDRESS_LENGTH,
    frame.protocol_length = ARP_PROTOCOL_ADDRESS_LENGTH,
    
    put_bigendian_word(ARP_OPER_REPLY, frame_bytes+6);
    
    memcpy(frame.sender_hw_address, my_mac, ARP_HW_ADDRESS_LENGTH);
    memcpy(frame.sender_protocol_address, my_ip, ARP_PROTOCOL_ADDRESS_LENGTH);
    
    memcpy(frame.target_hw_address, requestor_mac, ARP_HW_ADDRESS_LENGTH);
    memcpy(frame.target_protocol_address, requestor_ip, ARP_PROTOCOL_ADDRESS_LENGTH);
    
    return frame;
}
