#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <yakumo.h>
#include <sys/network.h>

#include "../shipyard.h"
#include "../endianness.h"
#include "../self_description.h"
#include "dhcp.h"



bool is_message_dhcp(ShipyardBuffer* shipyard) {
    if (shipyard->application_length < sizeof(DHCPHeader)) return false;
    
    DHCPHeader *header = (DHCPHeader*) shipyard->application_data;
    if (get_bigendian_dword(header->magic_cookie) != DHCP_MAGIC_COOKIE) return false;
    
    return true;
}



int dhcp_insert_option(uint8_t *destination, uint8_t option_type, uint8_t *option_data, uint8_t option_data_length) {
    destination[0] = option_type;
    destination[1] = option_data_length;
    memcpy(destination+2, option_data, option_data_length);
    return option_data_length + 2;
}

bool find_dhcp_option(DHCPHeader *header, uint8_t option_code, void **data, size_t *data_length) {
    uint8_t *options_base = ((uint8_t*)header) + sizeof(DHCPHeader);
    
    while (options_base[0] != DHCP_OPTION_ENDMARK) {
        if (options_base[0] == option_code) {
            *data = (void*) (options_base + 2);
            *data_length = options_base[1];
            return true;
        }
        options_base += options_base[1] + 2;
    }
    
    return false;
}


void build_dhcp_discovery(DHCPHeader **data, size_t *length, uint32_t transaction_identifier) {
    size_t full_length = sizeof(DHCPHeader) + 3 + 6 + 1;
    uint8_t *memstore = malloc(full_length);
    
    DHCPHeader *header = (DHCPHeader*) memstore;
    header->opcode = 1;
    header->htype = 1;
    header->hlen = 6;
    header->hops = 0;
    put_bigendian_dword(transaction_identifier, (uint8_t*)&header->xid);
    header->secs = 0;
    put_bigendian_word(DHCP_BROADCAST_FLAG, (uint8_t*)&header->flags);
    memset(header->client_ip, 0, IPv4_ADDRESS_LENGTH);
    memset(header->your_ip, 0, IPv4_ADDRESS_LENGTH);
    memset(header->server_ip, 0, IPv4_ADDRESS_LENGTH);
    memset(header->gateway_ip, 0, IPv4_ADDRESS_LENGTH);
    memset(header->client_hw_address, 0, DHCP_CHADDR_LEN);
    memcpy(header->client_hw_address, self_description.nic_manifest.mac, MAC_ADDRESS_LENGTH);
    put_bigendian_dword(DHCP_MAGIC_COOKIE, (uint8_t*)&header->magic_cookie);
    memset(header->server_name, 0, DHCP_SNAME_LEN);
    memset(header->boot_filename, 0, DHCP_FILE_LEN);
    
    uint8_t *options_base = memstore + sizeof(DHCPHeader);
    
    uint8_t msg_dhcp_discover = 1;
    options_base += dhcp_insert_option(options_base, DHCP_OPTION_MESSAGE_TYPE, &msg_dhcp_discover, 1);
    
    uint8_t msg_params[] = {dhcp_param_subnet_mask, dhcp_param_router, dhcp_param_domain_name, dhcp_param_dns};
    options_base += dhcp_insert_option(options_base, DHCP_OPTION_PARAM_LIST_REQUEST, msg_params, 4);
    
    options_base[0] = DHCP_OPTION_ENDMARK;
    options_base++;
    
    *length = full_length;
    *data = (DHCPHeader*)memstore;
}

void build_dhcp_request(DHCPHeader **data, size_t *length, uint32_t transaction_identifier, IpAddress *dhcp_server_ip, IpAddress *offered) {
    assert(dhcp_server_ip->version == 4);
    assert(offered->version == 4);
    size_t full_length = sizeof(DHCPHeader) + 3 + 6 + 6 + 6 + 1;
    uint8_t *memstore = malloc(full_length);
    
    DHCPHeader *header = (DHCPHeader*) memstore;
    header->opcode = 1;
    header->htype = 1;
    header->hlen = 6;
    header->hops = 0;
    put_bigendian_dword(transaction_identifier, (uint8_t*)&header->xid);
    header->secs = 0;
    put_bigendian_word(DHCP_BROADCAST_FLAG, (uint8_t*)&header->flags);
    memset(header->client_ip, 0, IPv4_ADDRESS_LENGTH);
    memset(header->your_ip, 0, IPv4_ADDRESS_LENGTH);
    memset(header->server_ip, 0, IPv4_ADDRESS_LENGTH);
    memset(header->gateway_ip, 0, IPv4_ADDRESS_LENGTH);
    memset(header->client_hw_address, 0, DHCP_CHADDR_LEN);
    memcpy(header->client_hw_address, self_description.nic_manifest.mac, MAC_ADDRESS_LENGTH);
    put_bigendian_dword(DHCP_MAGIC_COOKIE, (uint8_t*)&header->magic_cookie);
    memset(header->server_name, 0, DHCP_SNAME_LEN);
    memset(header->boot_filename, 0, DHCP_FILE_LEN);
    
    uint8_t *options_base = memstore + sizeof(DHCPHeader);
    
    uint8_t msg_dhcp_request = 3;
    options_base += dhcp_insert_option(options_base, DHCP_OPTION_MESSAGE_TYPE, &msg_dhcp_request, 1);
    
    options_base += dhcp_insert_option(options_base, DHCP_OPTION_DHCP_SERVER, dhcp_server_ip->address, 4);
    
    options_base += dhcp_insert_option(options_base, DHCP_OPTION_REQUESTED_IP_ADDRESS, offered->address, 4);
    
    uint8_t msg_params[] = {dhcp_param_subnet_mask, dhcp_param_router, dhcp_param_domain_name, dhcp_param_dns};
    options_base += dhcp_insert_option(options_base, DHCP_OPTION_PARAM_LIST_REQUEST, msg_params, 4);
    
    options_base[0] = DHCP_OPTION_ENDMARK;
    options_base++;
    
    *length = full_length;
    *data = (DHCPHeader*)memstore;
}
