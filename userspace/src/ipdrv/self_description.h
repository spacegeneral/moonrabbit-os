#ifndef _IPDRV_SELF_DESCRIPTION_H
#define _IPDRV_SELF_DESCRIPTION_H


#include <sys/network.h>
#include <stdbool.h>



typedef enum DHCPClientStatusCode {
    DHCP_CLIENT_CONFUSED,
    DHCP_CLIENT_DISCOVERY,
    DHCP_CLIENT_OFFERED,
    DHCP_CLIENT_REQUEST,
    DHCP_CLIENT_ACKNOWLEDGED,
    DHCP_CLIENT_READY,
} DHCPClientStatusCode;


typedef struct DHCPClientState {
    uint32_t current_transaction_id;
    DHCPClientStatusCode status;
    IpAddress offered_address;
    IpAddress next_server;
    IpAddress router;
    unsigned char candidate_router_mac[MAC_ADDRESS_LENGTH];
} DHCPClientState;



typedef struct SelfDescription {
    NetIfaceManifest nic_manifest;
    IpAddress lan_ip;
    IpAddress gateway_ip;
    unsigned char fallback_gateway_mac[MAC_ADDRESS_LENGTH];
    char rx_topic[4096];
    char tx_topic[4096];
    bool mainloop_running;
    bool verbose;
    int preferred_ip_version;
    DHCPClientState dhcp_client_state;
    char *session_file_name;
} SelfDescription;


extern SelfDescription self_description;



#endif
 
