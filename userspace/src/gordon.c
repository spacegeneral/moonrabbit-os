#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <sys/audio.h>
#include <sys/keycodes.h>
#include <sys/mq.h>
#include <sys/proc.h>


typedef enum NoteFull {
    NOTE_C3,
    NOTE_D3,
    NOTE_E3,
    NOTE_F3,
    NOTE_G3,
    NOTE_A3,
    NOTE_B3,
    NOTE_C4,
    NOTE_D4,
    NOTE_E4,
    NOTE_F4,
    NOTE_G4,
    NOTE_A4,
    NOTE_B4,
} NoteFull;

typedef enum NoteSharp {
    NOTE_C3_SHARP,
    NOTE_D3_SHARP,
    NOTE_F3_SHARP,
    NOTE_G3_SHARP,
    NOTE_A3_SHARP,
    NOTE_C4_SHARP,
    NOTE_D4_SHARP,
    NOTE_F4_SHARP,
    NOTE_G4_SHARP,
    NOTE_A4_SHARP,
} NoteSharp;

uint16_t notefull_frequencies[] = {
    [NOTE_C3] = 131,
    [NOTE_D3] = 146,
    [NOTE_E3] = 165,
    [NOTE_F3] = 175,
    [NOTE_G3] = 196,
    [NOTE_A3] = 220,
    [NOTE_B3] = 247,
    [NOTE_C4] = 262,
    [NOTE_D4] = 294,
    [NOTE_E4] = 327,
    [NOTE_F4] = 349,
    [NOTE_G4] = 392,
    [NOTE_A4] = 440,
    [NOTE_B4] = 493,
};

uint16_t notesharp_frequencies[] = {
    [NOTE_C3_SHARP] = 139,
    [NOTE_D3_SHARP] = 156,
    [NOTE_F3_SHARP] = 185,
    [NOTE_G3_SHARP] = 208,
    [NOTE_A3_SHARP] = 233,
    [NOTE_C4_SHARP] = 277,
    [NOTE_D4_SHARP] = 311,
    [NOTE_F4_SHARP] = 367,
    [NOTE_G4_SHARP] = 415,
    [NOTE_A4_SHARP] = 466,
};


#define NUM_KEYS_MAINROW 14
int keys_mainrow[NUM_KEYS_MAINROW] = {
    KEY_Grave,
    KEY_Num1,
    KEY_Num2,
    KEY_Num3,
    KEY_Num4,
    KEY_Num5,
    KEY_Num6,
    KEY_Num7,
    KEY_Num8,
    KEY_Num9,
    KEY_Num0,
    KEY_Minus,
    KEY_Equals
};

#define NUM_KEYS_SUBROW 10
int keys_subrow[NUM_KEYS_MAINROW] = {
    KEY_Tab,
    KEY_Q,
    KEY_E,
    KEY_R,
    KEY_T,
    KEY_U,
    KEY_I,
    KEY_P,
    KEY_LeftBracket,
    KEY_RightBracket
};


int get_row_key_index(int keycode, int *row, int rowlen) {
    for (int c=0; c<rowlen; c++) {
        if (row[c] == keycode) return c;
    }
    return -1;
}

void handle_key(KeyEvent* cmd) {
    if (cmd->keycode == KEY_Escape) {
        beep_off();
        exit(0);
    }
    
    if (cmd->is_pressed) {
        int notes_offset = 0;
        unsigned frequency;
        
        int fullnote_idx = get_row_key_index(cmd->keycode, keys_mainrow, NUM_KEYS_MAINROW);
        if (fullnote_idx != -1) {
            frequency = notefull_frequencies[fullnote_idx+notes_offset];
        } else {
            int subnote_idx = get_row_key_index(cmd->keycode, keys_subrow, NUM_KEYS_SUBROW);
            if (subnote_idx != -1) {
                frequency = notesharp_frequencies[subnote_idx+notes_offset];
            }
        }
        beep_on(frequency);
    }
    else {
        beep_off();
    }
}

void keyboard_loop() {
    KeyEvent cmd;
    MQStatus poll_status = MQ_NO_MESSAGES; 
    
    subscribe(keycode_topic);
    
    while (true) {
        poll_status = poll(keycode_topic, &cmd);
        if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) {
            printf("fatal stdin error.\n");
            exit(1);
        }
        
        if (poll_status == MQ_NO_MESSAGES) yield();
        else if (poll_status == MQ_SUCCESS) {
            handle_key(&cmd);
        }
    }
    
    unsubscribe(keycode_topic);
}

void print_help() {
    printf("Play music using the PC beeper.\n");
    printf("A standard pc keyboard is required; the key<->note mapping will be shown at startup.\n");
    
}

int main(void) {
    printf("Gordon music player v0.3\n");
    printf(" _______________________________keymap________________________________\n");
    printf("|    |    |    |    |    |    |    |    |    |    |    |    |    |    |\n");
    printf("|  ` |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |  9 |  0 |  - |  = | <- |\n");
    printf("|    |    |    |    |    |    |    |    |    |    |    |    |    |    |\n");
    printf("| C3 | D3 | E3 | F3 | G3 | A3 | B3 | C4 | D4 | E4 | F4 | G4 | A4 | B4 |\n");
    printf("|  .... ....   |  .... .... ....   |  .... ....   |  .... .... ....   |\n");
    printf("|  :->: : Q:   |  : E: : R: : T:   |  : U: : I:   |  : P: : [: : ]:   |\n");
    printf("|  :::: ::::   |  :::: :::: ::::   |  :::: ::::   |  :::: :::: ::::   |\n");
    
    if (getenv("help") != NULL) {
        print_help();
        return 0;
    }
    
    keyboard_loop();

    return 0;
}
 
 
