#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/proc.h>
#include <sys/serv.h>
#include <sys/mq.h>
#include <errno.h>
#include <sys/keycodes.h>
#include <libedit/readline.h>


bool verbose = false;
bool running = true;
bool do_not_retire = false;
size_t channel_width = 1;
size_t channel_depth = 512;

ProcIdent identity;
int channel_race;
int channel_class;

socket_t *commsocket = NULL;
sockaddr target = {
    .port = 5959, 
    .addr = {.version = 4, .address = {255, 255, 255, 255}}
};



void init_socket() {
    commsocket = socket(SOCK_STREAM, IPPROTO_UDP);
    if (commsocket == NULL) {
        printf("cannot open socket (error %d).\n", errno);
        exit(4);
    }
}


int forward_request_to_net(char* request) {
    sockaddr from;
    
    sendto(commsocket, request, channel_width, &target);
    if (errno != 0) return errno;
    
    size_t nrecv = 0;
    do {
        nrecv += recvfrom(commsocket, request + nrecv, channel_width, &from);
        if (errno != 0) return errno;
    } while (nrecv < channel_width);
    
    return 0;
}

int forward_publish_to_net(char* msg) {
    sendto(commsocket, msg, channel_width, &target);
    if (errno != 0) return errno;
    
    return 0;
}



void forward_service(char* topic) {
    ServStatus status;
    
    status = advertise_service_advanced(topic, channel_width, channel_race, channel_class);
    if (status == SERV_ALREADY_EXISTS) {
        if (verbose) printf("NOTICE: service \"%s\" already exists.\n", topic);
    }
    else if (status != SERV_SUCCESS) {
        printf("Error %d: cannot create service %s\n", status, topic);
        exit(2);
    }
    
    char* request = (char*)malloc(channel_width);
    unsigned int request_id;
    unsigned int requestor_task_id;
    
    if (verbose) {
        char fromstr[SOCKADDR_STRING_LENGTH] = {0};
        format_sockaddr(fromstr, &target);
        printf("Forwarding service \"%s\" (%lu bytes wide) to %s\n", topic, channel_width, fromstr);
    }
    
    while (running) {
        status = service_accept(topic, (void*)request, &requestor_task_id, &request_id);
        if (status == SERV_SUCCESS) {

            int fwerr = forward_request_to_net(request);
            if (fwerr != 0) {
                if (verbose) printf("error forwarding service to network: %d\n", fwerr);
            }
        
            status = service_complete(topic, request_id);
            
            if (status != SERV_SUCCESS) {
                if (verbose) printf("service completion error: %d\n", status);
            }
        }
        else if (status == SERV_NO_REQUESTS) yield();
        else {
            if (verbose) printf("service acceptation error: %d\n", status);
            yield();
        }
    }
    
    free(request);
    
    if (!do_not_retire) {
        status = retire_service(topic);
        if (status != SERV_SUCCESS) {
            printf("Error %d: cannot retire service %s\n", status, topic);
            exit(3);
        }
    }
}


void forward_queue(char* topic) {
    MQStatus status;
    
    status = advertise_channel_advanced(topic, channel_depth, channel_width, channel_race, channel_class, SecFullyEnforced|SecNoDataLoss);
    if (status == MQ_ALREADY_EXISTS) {
        if (verbose) printf("NOTICE: channel \"%s\" already exists.\n", topic);
    }
    else if (status != MQ_SUCCESS) {
        printf("Error %d: cannot create channel %s\n", status, topic);
        exit(2);
    } else {
        if (verbose) printf("Creating channel \"%s\".\n", topic);
    }
    
    status = subscribe(topic);
    if (status != MQ_SUCCESS) {
        printf("Error %d: cannot subscribe to channel %s\n", status, topic);
        exit(5);
    } 
    
    char* msg = (char*)malloc(channel_width);
    
    if (verbose) {
        char fromstr[SOCKADDR_STRING_LENGTH] = {0};
        format_sockaddr(fromstr, &target);
        printf("Forwarding channel \"%s\" (%lu bytes wide) to %s\n", topic, channel_width, fromstr);
    }
    
    // TODO: publish incoming messages FROM the net
    while (running) {
        status = poll(topic, (void*)msg);
        if (status == MQ_SUCCESS) {
            
            int fwerr = forward_publish_to_net(msg);
            if (fwerr != 0) {
                if (verbose) printf("error publishing to network: %d\n", fwerr);
            }
            
        }
        else if (status == MQ_NO_MESSAGES) yield();
        else {
            if (verbose) printf("channel poll error: %d\n", status);
            yield();
        }
    }
    
    unsubscribe(topic);
    
    free(msg);

    if (!do_not_retire) {
        status = retire_channel(topic);
        if (status != MQ_SUCCESS) {
            printf("Error %d: cannot retire channel %s\n", status, topic);
            exit(3);
        }
    }
}


void die(char* message, int code) {
    printf("%s\n", message);
    exit(code);
}


int main(void) {
    
    verbose = (getenv("verbose") != NULL);
    do_not_retire = (getenv("noretire") != NULL);
    if (getenv("width") != NULL) {
        channel_width = (size_t)atoi(getenv("width"));
    }
    if (getenv("host") != NULL) {
        parse_ip(getenv("host"), &(target.addr));
    }
    if (getenv("port") != NULL) {
        target.port = (size_t)atoi(getenv("port"));
    }
    // TODO filtering
    
    identity = ident();
    channel_race = identity.phys_race_attrib;
    channel_class = identity.data_class_attrib;
    
    if (getenv("service") != NULL) {
        init_socket();
        forward_service(getenv("service"));
    }
    else if (getenv("channel") != NULL) {
        init_socket();
        forward_queue(getenv("channel"));
    }
    else {
        die("Please privice a \"service=topicname\" or \"channel=topicname\" parameter.", 1);
    }
    
    return 0;
}