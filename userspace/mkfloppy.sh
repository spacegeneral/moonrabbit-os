#!/bin/bash

rm -r homemaster/compile/lib/*
rm -r homemaster/compile/include/*
# cp -r ../sysroot/usr/* homemaster/compile/
cp -r src/modular homemaster/compile/src/
mkdir -p homemaster/compile/src/mjs
# cp src/mjs/{mjs.h,mjs.c,config.h} homemaster/compile/src/mjs/
mkdir -p homemaster/compile/crt
cp crt0.o homemaster/compile/crt/
echo "#include <stddef.h>" > homemaster/compile/include/stdint.h
find homemaster/compile/lib -type f -iname "*.a" -exec pigz -z -f {} \;

make -j16 install

find homemaster -type f -iname "*.elf" -exec pigz -z -f {} \;

sudo losetup -o 17408 /dev/loop2 initfloppy.img
sudo losetup -o 17408 /dev/loop4 homefloppy.img
sudo mount -o uid=1000,gid=100,fat=16 /dev/loop4 ./home

sudo python mkinitramfs.py /dev/loop2 initramfs
rm -rf ./home/*
cp -r homemaster/* home/

du -sh ./home

sudo umount ./home

echo "Home checksum: `sudo sha256sum /dev/loop4`"

sudo losetup -d /dev/loop4
sudo losetup -d /dev/loop2
