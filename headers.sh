#!/bin/sh
set -e
. ./config.sh

mkdir -p "$SYSROOT"
mkdir -p "$SYSROOTUSER"

for PROJECT in $SYSTEM_HEADER_PROJECTS; do
  (cd $PROJECT && DESTDIR="$SYSROOT" $MAKE install-headers)
done

for PROJECT in $USER_HEADER_PROJECTS; do
  (cd $PROJECT && DESTDIR="$SYSROOTUSER" $MAKE install-headers)
done