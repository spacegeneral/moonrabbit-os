#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unicode.h>
#include <yakumo.h>
#include <sys/keycodes.h>
#include <libedit/characterline.h>
#include <libgui/text.h>
#include <rice.h>



void CharacterLine_init(CharacterLine *line) {
    line->cells = (CharacterCell*) malloc(sizeof(CharacterCell) * CELLS_GROW_FACTOR);
    line->num_cells_avail = CELLS_GROW_FACTOR;
    line->num_cells_used = 0;
    line->cursor_x = 0;
    line->trailing_erase = 0;
    line->ins_mode = false;
    line->default_foreground_color = COLOR24_TEXT;
    line->default_background_color = SETCOLOR_UNCHANGED;
}

void CharacterLine_grow(CharacterLine *line) {
    line->num_cells_avail += CELLS_GROW_FACTOR;
    line->cells = realloc(line->cells, line->num_cells_avail * sizeof(CharacterCell));
}

void CharacterLine_destroy(CharacterLine *line) {
    free(line->cells);
}

void CharacterLine_putchar(CharacterLine *line, KeyEvent *key) {
    if (strlen(key->utf8) == 0) return;
    
    // insert mode: replace character under the cursor
    if (line->ins_mode) {
        strncpy(line->cells[line->cursor_x].utf8, key->utf8, UTF8_BYTES_ATOM_LENGTH);
        line->cells[line->cursor_x].foreground_color = line->default_foreground_color;
        line->cells[line->cursor_x].background_color = line->default_background_color;
        if (line->cursor_x == line->num_cells_used) line->num_cells_used++;
    }
    // append mode
    else {
        // grow array if needed
        if (line->num_cells_used+1 > line->num_cells_avail) CharacterLine_grow(line);
        
        // insert in the middle: shift forwards all the characters following the cursor
        if (line->cursor_x < line->num_cells_used) {
            for (int mwlk=line->num_cells_used; mwlk>line->cursor_x; mwlk--) {
                strncpy(line->cells[mwlk].utf8, line->cells[mwlk-1].utf8, UTF8_BYTES_ATOM_LENGTH);
            }
        }
        
        strncpy(line->cells[line->cursor_x].utf8, key->utf8, UTF8_BYTES_ATOM_LENGTH);
        line->cells[line->cursor_x].foreground_color = line->default_foreground_color;
        line->cells[line->cursor_x].background_color = line->default_background_color;
        line->cursor_x++;
        line->num_cells_used++;
    }
}

void CharacterLine_backspace(CharacterLine *line) {
    if (line->cursor_x <= 0) return;
    
    for (int wlk=line->cursor_x-1; wlk<line->num_cells_used-1; wlk++) {
        strncpy(line->cells[wlk].utf8, line->cells[wlk+1].utf8, UTF8_BYTES_ATOM_LENGTH);
    }
    
    line->cursor_x--;
    line->num_cells_used--;
    line->trailing_erase++;
}

void CharacterLine_delete(CharacterLine *line) {
    if (line->cursor_x == line->num_cells_used) return;
    
    for (int wlk=line->cursor_x; wlk<line->num_cells_used-1; wlk++) {
        strncpy(line->cells[wlk].utf8, line->cells[wlk+1].utf8, UTF8_BYTES_ATOM_LENGTH);
    }
    
    line->num_cells_used--;
    line->trailing_erase++;
}

void CharacterLine_cursor_left(CharacterLine *line) {
    if (line->cursor_x-1 >= 0) line->cursor_x--;
}

void CharacterLine_cursor_right(CharacterLine *line) {
    if (line->cursor_x+1 <= line->num_cells_used) line->cursor_x++;
}

static void CharacterLine_print_single_cell(CharacterCell* cell, int *current_foreground_color, int *current_background_color) {
    setcolors(cell->foreground_color, cell->background_color);
    printf("%s", cell->utf8);
}

void CharacterLine_print(CharacterLine *line) {
    for (size_t c=0; c<line->num_cells_used; c++) {
        printf("%s", line->cells[c].utf8);
    }
    for (size_t e=0; e<line->trailing_erase; e++) {
        printf(" ");
    }
    line->trailing_erase = 0;
}

void CharacterLine_print_up_to_cursor(CharacterLine *line) {
    for (size_t c=0; c<line->cursor_x; c++) {
        printf("%s", line->cells[c].utf8);
    }
}

void CharacterLine_print_color(CharacterLine *line, int *current_foreground_color, int *current_background_color) {
    for (size_t c=0; c<line->num_cells_used; c++) {
        CharacterLine_print_single_cell(line->cells + c, current_foreground_color, current_background_color);
    }
    for (size_t e=0; e<line->trailing_erase; e++) {
        printf(" ");
    }
    line->trailing_erase = 0;
}

void CharacterLine_print_up_to_cursor_color(CharacterLine *line, int *current_foreground_color, int *current_background_color) {
    for (size_t c=0; c<line->cursor_x; c++) {
        CharacterLine_print_single_cell(line->cells + c, current_foreground_color, current_background_color);
    }
}

void CharacterLine_get_str(CharacterLine *line, char *str, int start_offset) {
    size_t written = 0;
    for (int c=start_offset; c<(int)line->num_cells_used; c++) {
        written += sprintf(str+written, "%s", line->cells[c].utf8);
    }
}

bool CharacterLine_save(CharacterLine *line, FILE *tofile) {
    char *buffer = malloc(sizeof(char) * UTF8_BYTES_ATOM_LENGTH * (line->num_cells_used + 2));
    size_t bytes_to_write;
    
    if (line->num_cells_used == 0) {
        bytes_to_write = 1;
        buffer[0] = '\n';
    } else {
        CharacterLine_get_str(line, buffer, 0);
        bytes_to_write = strlen(buffer);
        buffer[bytes_to_write] = '\n';
        bytes_to_write++;
    }
    
    size_t bytes_written = fwrite(buffer, sizeof(char), bytes_to_write, tofile);
    
    free(buffer);
    
    return (bytes_written == bytes_to_write);
}

void CharacterLine_digest(CharacterLine *line, char *str) {
    char accumulator[UTF8_BYTES_ATOM_LENGTH];
    int accum_pos = 0;
    Utf8Decoder decoder;
    utf8_decoder_init(&decoder);
    
    for (size_t str_pos = 0; str_pos < strlen(str); str_pos++) {
        accumulator[accum_pos] = str[str_pos];
        accum_pos = (accum_pos+1) % UTF8_BYTES_ATOM_LENGTH;
        if (utf8_decoder_feed(&decoder, str[str_pos]) >= 0) {
            accumulator[accum_pos] = '\0';
            
            if (line->num_cells_used+1 > line->num_cells_avail) CharacterLine_grow(line);
            size_t cell_index = line->num_cells_used++;
            strncpy(line->cells[cell_index].utf8, accumulator, UTF8_BYTES_ATOM_LENGTH);
            line->cells[cell_index].foreground_color = line->default_foreground_color;
            line->cells[cell_index].background_color = line->default_background_color;
            
            accum_pos = 0;
            utf8_decoder_init(&decoder);
        }
    }
}

void CharacterLine_append_other(CharacterLine *line, CharacterLine *other, size_t offset) {
    for (size_t oc = offset; oc < other->num_cells_used; oc++) {
        CharacterLine_digest(line, other->cells[oc].utf8);
    }
}

void CharacterLine_replace_with(CharacterLine *line, char *str) {
    int initially_occupied = line->num_cells_used;
    line->num_cells_used = 0;
    CharacterLine_digest(line, str);
    line->cursor_x = line->num_cells_used;
    int finally_occupied = line->num_cells_used;
    line->trailing_erase = max(0, initially_occupied - finally_occupied);
}

int CharacterLine_search(CharacterLine *line, char *str, int start_offset) {
    char *buffer = malloc(sizeof(char) * UTF8_BYTES_ATOM_LENGTH * (line->num_cells_used + 2));
    CharacterLine_get_str(line, buffer, start_offset);
    
    if (strlen(buffer) < strlen(str)) {
        free(buffer);
        return -1;
    }
    
    for (int pos=0; pos<=strlen(buffer) - strlen(str); pos++) {
        if (memcmp(buffer + pos, str, strlen(str)) == 0) {
            char backup = buffer[pos];
            buffer[pos] = '\0';
            int found_location = strlen_utf8(buffer);
            buffer[pos] = backup;
            
            free(buffer);
            return found_location + start_offset;
        }
    }
    
    free(buffer);
    return -1;
}

size_t CharacterLine_onscreen_length(CharacterLine *line, BitmapFont *font, int start_offset) {
    long l_accumulator = 0;
    
    for (int c=start_offset; c<(int)line->num_cells_used; c++) {
        int codepoint = utf8_atom_bytes_to_codepoint(line->cells[c].utf8);
        if (codepoint < 0) continue;
        if (codepoint > font->upper_range) continue;
        int skipback_factor = combining_factor(codepoint);
        if (skipback_factor == NOT_COMBINING) {
            l_accumulator += (long) (get_codepoint_width_units(font->codepoints + codepoint) * CHAR_WIDTH_UNIT);
        } else {
            l_accumulator += (long) skipback_factor;
        }
    }
    
    if (l_accumulator < 0) return 0;
    return (size_t) l_accumulator;
}

size_t CharacterLine_onscreen_height(CharacterLine *line, BitmapFont *font, int start_offset) {
    size_t max_height_blocks = 0;
    
    for (int c=start_offset; c<(int)line->num_cells_used; c++) {
        int codepoint = utf8_atom_bytes_to_codepoint(line->cells[c].utf8);
        if (codepoint < 0) continue;
        if (codepoint > font->upper_range) continue;
        int skipback_factor = combining_factor(codepoint);
        if (skipback_factor == NOT_COMBINING) {
            size_t current_height_blocks = get_codepoint_height_units(font->codepoints + codepoint);
            if (current_height_blocks > max_height_blocks) {
                max_height_blocks = current_height_blocks;
            }
        }
    }
    return max_height_blocks * CHAR_HEIGHT_UNIT;
}
