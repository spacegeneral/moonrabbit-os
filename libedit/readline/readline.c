#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mq.h>
#include <sys/proc.h>
#include <sys/term.h>
#include <sys/happy.h>
#include <rice.h>
#include <time.h>
#include <sys/keycodes.h>
#include <libedit/characterline.h>
#include <libedit/readline.h>




// return true when finished and the line editor should return
static bool readline_simple_handle_key(CharacterLine *line, KeyEvent *key) {
    switch (key->keycode) {
        case KEY_Enter:
            return true;
        case KEY_Insert:
            line->ins_mode = !line->ins_mode;
            break;
        case KEY_Left:
            CharacterLine_cursor_left(line);
            break;
        case KEY_Right:
            CharacterLine_cursor_right(line);
            break;
        case KEY_Home:
            line->cursor_x = 0;
            break;
        case KEY_End:
            line->cursor_x = line->num_cells_used;
            break;
        case KEY_Backspace:
            CharacterLine_backspace(line);
            break;
        case KEY_Delete:
            CharacterLine_delete(line);
            break;
        default:
            if (strlen(key->utf8) > 0) {  // printable
                CharacterLine_putchar(line, key);
                break;
            }
    }
    
    return false;
}


static bool readline_handle_key_custom(CharacterLine *line, KeyEvent *key, readline_custom_key_handler user_handler) {
    switch (key->keycode) {
        case KEY_Enter:
            return true;
        case KEY_Insert:
            line->ins_mode = !line->ins_mode;
            break;
        case KEY_Left:
            CharacterLine_cursor_left(line);
            break;
        case KEY_Right:
            CharacterLine_cursor_right(line);
            break;
        case KEY_Home:
            line->cursor_x = 0;
            break;
        case KEY_End:
            line->cursor_x = line->num_cells_used;
            break;
        case KEY_Backspace:
            CharacterLine_backspace(line);
            break;
        case KEY_Delete:
            CharacterLine_delete(line);
            break;
        default:
            if (strlen(key->utf8) > 0) {  // printable
                CharacterLine_putchar(line, key);
                break;
            }
    }
    
    if (user_handler(line, key)) {
        return true;
    }
    
    return false;
}




#define ECHO_SEQUENCE \
{ \
    togglecursor(false); \
    setcursor(basecur.x, basecur.y); \
    CharacterLine_print_color(&line, &(ti.foreground_color), &(ti.background_color)); \
    setcursor(basecur.x, basecur.y); \
    CharacterLine_print_up_to_cursor_color(&line, &(ti.foreground_color), &(ti.background_color)); \
    setcolors(COLOR24_TEXT, SETCOLOR_UNCHANGED); \
    togglecursor(true); \
}


#define TERMINFO_UPDATE_SEQUENCE \
{ \
    ti = getterminfo(); \
}


#define EDIT_LOOP_BODY(parsekey_fun) \
{ \
    time_t poll_time = clock_ms(); \
    poll_status = poll(keycode_topic, &key); \
    if (poll_status == MQ_SUCCESS && key.is_pressed) { \
        TERMINFO_UPDATE_SEQUENCE; \
        editing = parsekey_fun; \
        ECHO_SEQUENCE; \
        happy(errf_lin_pos((long)(clock_ms() - poll_time), 200)); \
    } else if (poll_status != MQ_NO_MESSAGES && poll_status != MQ_SUCCESS) { \
        TERMINFO_UPDATE_SEQUENCE; \
        CharacterLine_destroy(&line); \
        unsubscribe(keycode_topic); \
        return poll_status; \
    } else { \
        happy(0); \
        yield(); \
    } \
}


int readline_simple(char *str) {
    MQStatus poll_status;
    KeyEvent key;
    CharacterLine line;
    
    TermInfo ti = getterminfo();
    Point basecur = ti.cursor;
    
    bool editing = true;
    
    CharacterLine_init(&line);
    ECHO_SEQUENCE;
    subscribe(keycode_topic);
    
    while (editing) {
        EDIT_LOOP_BODY(!readline_simple_handle_key(&line, &key));
    }
    
    CharacterLine_get_str(&line, str, 0);
    CharacterLine_destroy(&line);
    unsubscribe(keycode_topic);
    return 0;
}

int readline_custom(char *str, readline_custom_key_handler user_handler) {
    MQStatus poll_status;
    KeyEvent key;
    CharacterLine line;
    
    TermInfo ti = getterminfo();
    Point basecur = ti.cursor;
    
    bool editing = true;
    
    CharacterLine_init(&line);
    ECHO_SEQUENCE;
    subscribe(keycode_topic);
    
    while (editing) {
        EDIT_LOOP_BODY(!readline_handle_key_custom(&line, &key, user_handler));
    }
    
    CharacterLine_get_str(&line, str, 0);
    CharacterLine_destroy(&line);
    unsubscribe(keycode_topic);
    return 0;
}


int readline_editline(char *str) {
    MQStatus poll_status;
    KeyEvent key;
    CharacterLine line;
    
    TermInfo ti = getterminfo();
    Point basecur = ti.cursor;
    
    bool editing = true;
    
    CharacterLine_init(&line);
    CharacterLine_replace_with(&line, str);
    ECHO_SEQUENCE;
    
    subscribe(keycode_topic);
    
    while (editing) {
        EDIT_LOOP_BODY(!readline_simple_handle_key(&line, &key));
    }
    
    CharacterLine_get_str(&line, str, 0);
    CharacterLine_destroy(&line);
    unsubscribe(keycode_topic);
    return 0;
}


int readline_editline_custom(char *str, readline_custom_key_handler user_handler) {
    MQStatus poll_status;
    KeyEvent key;
    CharacterLine line;
    
    TermInfo ti = getterminfo();
    Point basecur = ti.cursor;
    
    bool editing = true;

    CharacterLine_init(&line);
    CharacterLine_replace_with(&line, str);
    ECHO_SEQUENCE;
    
    subscribe(keycode_topic);
    
    while (editing) {
        EDIT_LOOP_BODY(!readline_handle_key_custom(&line, &key, user_handler));
    }
    
    CharacterLine_get_str(&line, str, 0);
    CharacterLine_destroy(&line);
    unsubscribe(keycode_topic);
    return 0;
}


