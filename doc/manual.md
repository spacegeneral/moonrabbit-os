# Moonrabbit OS Operator's Manual

## Introduction

This manual is written for power users, with a skill level located in the sweet spot
between "system administrator" and "OS maintainer"; that being said, important
sections will also have a "TL;DR" section for the less skilled reader.

## Design and Philosophy

Moonrabbit OS is a single-user multitasking operating system designed for personal computers
based on the x86 architecture.
The OS is built as a combination of a ring-0 kernel (hybrid kernel, leaning heavily towards the
microkernel paradigm) and ring-3 userspace applications.
The address space is identity-mapped; however, memory protection mechanisms are put in place in
order to partially isolate userland **tasks**.

The Moonrabbit OS security model is structured following the Mandatory Access Control (MAC)
paradigm; to do so, it introduces a special biparted set of security levels, used to 
characterize both resources (i.e. **files**, **topics**, etc.) and **agents**.

In contrast with Unix-based systems, Moonrabbit OS does *not* follow the "everything is a file"
philosophy, but rather treat files simply as persistent on-disk storage (with the exception of 
**disk files**, which behave sort of like unix character devices, and **dial files**, which are
generated on the fly by the kernel when they are read).  
In contrast with DOS-based systems, Moonrabbit OS does not follow the "drive letter mount" 
philosophy; instead, it uses a tree-like virtual filesystem, where **alien** (not managed by
the kernel) filesystems can be mounted as any leaf of the vfs tree.

Inter-process communication is handled using 2 distinct systems: **the Message Queue** system
and the **Service** system.  
The message queue system allows tasks to exchange message using an OS-wide publish/subscribe
system, and is used for handling streams of data, such as stdin and stdout.
The service systems instead offers an interface using the request/reply paradigm, and is used
for managing more complex operations, such as interacting with a filesystem.

In contrast with most modern OSs, Moonrabbit OS implements multitasking in the form of
*cooperative multitasking*. In Cooperative Multitasking, it is the userland program itself which
must give up control of the CPU, using the `Yield()` primitive. In practice, a form of
round-robin scheduling is implemented, and is handled by an user space application, **ring**, 
which acts as *init* task and exposes a special interface to facilitate the creation of new tasks.

