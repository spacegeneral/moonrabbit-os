var searchData=
[
  ['security_20clearance_20levels',['Security clearance levels',['../group__LIBC__SYS__SEC.html',1,'']]],
  ['safest',['safest',['../group__LIBC__SYS__SEC.html#gaece33f82542b3fc128e2aadc2588beeb',1,'sec.h']]],
  ['securitypolicy',['SecurityPolicy',['../group__LIBC__SYS__SEC.html#ga5cec91274e34721136d4986bb09d2ab0',1,'SecurityPolicy():&#160;sec.h'],['../group__LIBC__SYS__SEC.html#ga5b0bb8b1d960a84e8a799441a3041415',1,'SecurityPolicy():&#160;sec.h']]],
  ['serviceprocess',['ServiceProcess',['../structServiceProcess.html',1,'']]],
  ['servicetopic',['ServiceTopic',['../structServiceTopic.html',1,'']]],
  ['sha256_5fctx',['SHA256_CTX',['../structSHA256__CTX.html',1,'']]],
  ['size',['size',['../structliballoc__major.html#a94315a9fd8d95755437248f20450e9ba',1,'liballoc_major::size()'],['../structliballoc__minor.html#a138f5528e084fb73761b9a23e106fb4a',1,'liballoc_minor::size()']]],
  ['slist_5fconf_5fs',['slist_conf_s',['../structslist__conf__s.html',1,'']]],
  ['slist_5fiter_5fs',['slist_iter_s',['../structslist__iter__s.html',1,'']]],
  ['slist_5fs',['slist_s',['../structslist__s.html',1,'']]],
  ['slist_5fzip_5fiter_5fs',['slist_zip_iter_s',['../structslist__zip__iter__s.html',1,'']]],
  ['snode_5fs',['snode_s',['../structsnode__s.html',1,'']]],
  ['sockaddr',['sockaddr',['../structsockaddr.html',1,'']]],
  ['sockanydatagram',['SockANYDatagram',['../structSockANYDatagram.html',1,'']]],
  ['socket_5ft',['socket_t',['../structsocket__t.html',1,'']]],
  ['socketcontrolservicemessage',['SocketControlServiceMessage',['../structSocketControlServiceMessage.html',1,'']]],
  ['socktcpdatagram',['SockTCPDatagram',['../structSockTCPDatagram.html',1,'']]],
  ['sockudpdatagram',['SockUDPDatagram',['../structSockUDPDatagram.html',1,'']]],
  ['softterminal',['SoftTerminal',['../structSoftTerminal.html',1,'']]],
  ['softterminalcell',['SoftTerminalCell',['../structSoftTerminalCell.html',1,'']]],
  ['soundcardinitializer',['SoundCardInitializer',['../structSoundCardInitializer.html',1,'']]],
  ['soundcardobject',['SoundCardObject',['../structSoundCardObject.html',1,'']]],
  ['spoon',['spoon',['../group__LIBC__SYS__PROC.html#gacde557de67fcc374cfcaf086a7a4f271',1,'spoon(const char *filename, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib):&#160;spoon.c'],['../group__LIBC__SYS__PROC.html#gacde557de67fcc374cfcaf086a7a4f271',1,'spoon(const char *filename, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib):&#160;spoon.c']]],
  ['spork',['spork',['../group__LIBC__SYS__PROC.html#gada472725df45b8f8dfbae41f4b161452',1,'spork(const char *filename, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib, int *return_code_here):&#160;spork.c'],['../group__LIBC__SYS__PROC.html#gada472725df45b8f8dfbae41f4b161452',1,'spork(const char *filename, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib, int *return_code_here):&#160;spork.c']]],
  ['spork_5fthread',['spork_thread',['../group__LIBC__SYS__PROC.html#ga426a92ac5ad02d89eb0614cfddd024eb',1,'spork_thread(void *thread_entry_point):&#160;spork_thread.c'],['../group__LIBC__SYS__PROC.html#ga426a92ac5ad02d89eb0614cfddd024eb',1,'spork_thread(void *thread_entry_point):&#160;spork_thread.c']]],
  ['stabsymbol',['StabSymbol',['../structStabSymbol.html',1,'']]],
  ['stack_5fiter_5fs',['stack_iter_s',['../structstack__iter__s.html',1,'']]],
  ['stack_5fmemarea',['stack_memarea',['../structTask.html#a3ea6d5aaaee2e7f2364b3b85c3fada0b',1,'Task']]],
  ['stack_5fs',['stack_s',['../structstack__s.html',1,'']]],
  ['stack_5fzip_5fiter_5fs',['stack_zip_iter_s',['../structstack__zip__iter__s.html',1,'']]],
  ['storagedevice',['StorageDevice',['../structStorageDevice.html',1,'']]],
  ['subscribe',['subscribe',['../group__LIBC__MQ.html#ga4490b09186913a7f0e3caf6cc8db2702',1,'subscribe(char *topic_name):&#160;pubsub.c'],['../group__LIBC__MQ.html#ga4490b09186913a7f0e3caf6cc8db2702',1,'subscribe(char *topic_name):&#160;pubsub.c']]],
  ['syscallacceptserviceparam',['SyscallAcceptServiceParam',['../structSyscallAcceptServiceParam.html',1,'']]],
  ['syscalladoptparam',['SyscallAdoptParam',['../structSyscallAdoptParam.html',1,'']]],
  ['syscalladvertisechannelparam',['SyscallAdvertiseChannelParam',['../structSyscallAdvertiseChannelParam.html',1,'']]],
  ['syscalladvertiseserviceparam',['SyscallAdvertiseServiceParam',['../structSyscallAdvertiseServiceParam.html',1,'']]],
  ['syscallbeepparam',['SyscallBeepParam',['../structSyscallBeepParam.html',1,'']]],
  ['syscallchannelstatsparam',['SyscallChannelStatsParam',['../structSyscallChannelStatsParam.html',1,'']]],
  ['syscallclockparam',['SyscallClockParam',['../structSyscallClockParam.html',1,'']]],
  ['syscallcloneparam',['SyscallCloneParam',['../structSyscallCloneParam.html',1,'']]],
  ['syscalldisownparam',['SyscallDisownParam',['../structSyscallDisownParam.html',1,'']]],
  ['syscallexpireccparam',['SyscallExpireCCParam',['../structSyscallExpireCCParam.html',1,'']]],
  ['syscallgetmountinfoparam',['SyscallGetMountInfoParam',['../structSyscallGetMountInfoParam.html',1,'']]],
  ['syscallgiftmemareaparam',['SyscallGiftMemareaParam',['../structSyscallGiftMemareaParam.html',1,'']]],
  ['syscallhenshinparam',['SyscallHenshinParam',['../structSyscallHenshinParam.html',1,'']]],
  ['syscallidentparam',['SyscallIdentParam',['../structSyscallIdentParam.html',1,'']]],
  ['syscallkillparam',['SyscallKillParam',['../structSyscallKillParam.html',1,'']]],
  ['syscallmallocparam',['SyscallMallocParam',['../structSyscallMallocParam.html',1,'']]],
  ['syscallmessageserviceidparam',['SyscallMessageServiceIDParam',['../structSyscallMessageServiceIDParam.html',1,'']]],
  ['syscallmlengthparam',['SyscallMlengthParam',['../structSyscallMlengthParam.html',1,'']]],
  ['syscallmountparam',['SyscallMountParam',['../structSyscallMountParam.html',1,'']]],
  ['syscallmsleepparam',['SyscallMsleepParam',['../structSyscallMsleepParam.html',1,'']]],
  ['syscallprelaunchkernelparam',['SyscallPrelaunchKernelParam',['../structSyscallPrelaunchKernelParam.html',1,'']]],
  ['syscallprelaunchuserparam',['SyscallPrelaunchUserParam',['../structSyscallPrelaunchUserParam.html',1,'']]],
  ['syscallpublishmultiparam',['SyscallPublishMultiParam',['../structSyscallPublishMultiParam.html',1,'']]],
  ['syscallpubpollparam',['SyscallPubPollParam',['../structSyscallPubPollParam.html',1,'']]],
  ['syscallrequestserviceparam',['SyscallRequestServiceParam',['../structSyscallRequestServiceParam.html',1,'']]],
  ['syscallretireserviceparam',['SyscallRetireServiceParam',['../structSyscallRetireServiceParam.html',1,'']]],
  ['syscalltaskswitchparam',['SyscallTaskSwitchParam',['../structSyscallTaskSwitchParam.html',1,'']]],
  ['syscalltopicparam',['SyscallTopicParam',['../structSyscallTopicParam.html',1,'']]],
  ['syscallunmountparam',['SyscallUnMountParam',['../structSyscallUnMountParam.html',1,'']]]
];
