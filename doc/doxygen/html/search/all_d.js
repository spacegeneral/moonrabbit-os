var searchData=
[
  ['name',['name',['../structTask.html#aa9c278534e34651c5c5eecae795c3abd',1,'Task']]],
  ['netifacemanifest',['NetIfaceManifest',['../structNetIfaceManifest.html',1,'']]],
  ['new_5ftask',['new_task',['../group__KERN__TASK.html#gae26c3a5ac3b7f526b5ecc9926968a137',1,'new_task(ucpuint_t entry_point, void *main_memarea, ucpuint_t stack_top, void *stack_memarea, ucpuint_t environ, ucpuint_t cr3, ucpuint_t flags, unsigned int owner_id, unsigned int parent_task, int *return_code_here, const char *name, TaskDebuginfo *debug_info):&#160;task.c'],['../group__KERN__TASK.html#gae26c3a5ac3b7f526b5ecc9926968a137',1,'new_task(ucpuint_t entry_point, void *main_memarea, ucpuint_t stack_top, void *stack_memarea, ucpuint_t environ, ucpuint_t cr3, ucpuint_t flags, unsigned int owner_id, unsigned int parent_task, int *return_code_here, const char *name, TaskDebuginfo *debug_info):&#160;task.c']]],
  ['next',['next',['../structtable__entry__s.html#a756e9c069cd75ec42e61585074922b0f',1,'table_entry_s::next()'],['../structlist__iter__s.html#a2e1de72a0f773ea50b69e4581cadd8ae',1,'list_iter_s::next()'],['../structliballoc__major.html#a97bfe7e17a18555bcf3ac0ef7a760c02',1,'liballoc_major::next()'],['../structliballoc__minor.html#aad2a683c287f891bb4e315df5734bb94',1,'liballoc_minor::next()']]],
  ['nicinitializer',['NicInitializer',['../structNicInitializer.html',1,'']]],
  ['nicobject',['NicObject',['../structNicObject.html',1,'']]],
  ['node_5fs',['node_s',['../structnode__s.html',1,'']]]
];
