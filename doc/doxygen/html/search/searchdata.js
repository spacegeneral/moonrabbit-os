var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvy",
  1: "_abcdefghiklmnopqrstuv",
  2: "acdehiklnprstuy",
  3: "abcdefhiklmnoprstuv",
  4: "cdmps",
  5: "dmps",
  6: "klmst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "variables",
  4: "typedefs",
  5: "enums",
  6: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Functions",
  3: "Variables",
  4: "Typedefs",
  5: "Enumerations",
  6: "Modules"
};

