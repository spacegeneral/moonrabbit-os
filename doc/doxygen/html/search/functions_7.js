var searchData=
[
  ['liballoc_5falloc',['liballoc_alloc',['../group__ALLOCHOOKS.html#ga53e313b25fa5a139543a46137d15d944',1,'liballoc_alloc(size_t, char *, unsigned):&#160;liballoc_iface.c'],['../group__ALLOCHOOKS.html#ga53e313b25fa5a139543a46137d15d944',1,'liballoc_alloc(size_t pages, char *filename, unsigned linenum):&#160;liballoc_iface.c']]],
  ['liballoc_5ffree',['liballoc_free',['../group__ALLOCHOOKS.html#ga8d5be3c85e157d771a44c48509b43c42',1,'liballoc_free(void *, size_t):&#160;liballoc_iface.c'],['../group__ALLOCHOOKS.html#ga8d5be3c85e157d771a44c48509b43c42',1,'liballoc_free(void *memarea, size_t pages):&#160;liballoc_iface.c']]],
  ['liballoc_5ffree_5ffine',['liballoc_free_fine',['../group__ALLOCHOOKS.html#gab490bf5533ba3b795cbd8c4f0c7736b4',1,'liballoc.h']]],
  ['liballoc_5flock',['liballoc_lock',['../group__ALLOCHOOKS.html#ga8b5670e4594b0b6f8be78fe17f0c3b53',1,'liballoc_lock():&#160;liballoc_iface.c'],['../group__ALLOCHOOKS.html#ga8b5670e4594b0b6f8be78fe17f0c3b53',1,'liballoc_lock():&#160;liballoc_iface.c']]],
  ['liballoc_5frich_5fcalloc_5ffine',['liballoc_rich_calloc_fine',['../group__ALLOCHOOKS.html#gad97dae0a360e29421f9c86be82afa1de',1,'liballoc.h']]],
  ['liballoc_5frich_5fmalloc_5ffine',['liballoc_rich_malloc_fine',['../group__ALLOCHOOKS.html#ga7c78ac6192210ff854eb09ce1bbb7386',1,'liballoc.h']]],
  ['liballoc_5frich_5frealloc_5ffine',['liballoc_rich_realloc_fine',['../group__ALLOCHOOKS.html#ga63db95bc053229c33bcbe71c92046c53',1,'liballoc.h']]],
  ['liballoc_5funlock',['liballoc_unlock',['../group__ALLOCHOOKS.html#gaedc23f198b2882d41d0caa316453967b',1,'liballoc_unlock():&#160;liballoc_iface.c'],['../group__ALLOCHOOKS.html#gaedc23f198b2882d41d0caa316453967b',1,'liballoc_unlock():&#160;liballoc_iface.c']]]
];
