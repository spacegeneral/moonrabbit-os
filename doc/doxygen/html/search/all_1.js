var searchData=
[
  ['adopt',['adopt',['../group__LIBC__SYS__PROC.html#gaaaca0010fb0e0e3cc4f4d1fa958262e1',1,'adopt(unsigned int child_task_id):&#160;adopt.c'],['../group__LIBC__SYS__PROC.html#gaaaca0010fb0e0e3cc4f4d1fa958262e1',1,'adopt(unsigned int child_task_id):&#160;adopt.c']]],
  ['advertise_5fchannel',['advertise_channel',['../group__LIBC__MQ.html#gaafe29b63827e88af6f89280ae4bb0e64',1,'advertise_channel(char *topic_name, unsigned int max_queue_length, unsigned int message_length):&#160;pubsub.c'],['../group__LIBC__MQ.html#gaafe29b63827e88af6f89280ae4bb0e64',1,'advertise_channel(char *topic_name, unsigned int max_queue_length, unsigned int message_length):&#160;pubsub.c']]],
  ['advertise_5fchannel_5fadvanced',['advertise_channel_advanced',['../group__LIBC__MQ.html#ga5caece2a1e0778374049af7ac09c8dd1',1,'advertise_channel_advanced(char *topic_name, unsigned int max_queue_length, unsigned int message_length, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib, SecurityPolicy policy):&#160;pubsub.c'],['../group__LIBC__MQ.html#ga5caece2a1e0778374049af7ac09c8dd1',1,'advertise_channel_advanced(char *topic_name, unsigned int max_queue_length, unsigned int message_length, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib, SecurityPolicy policy):&#160;pubsub.c']]],
  ['alignedioframe_5faddr_5fuint64_5ft',['AlignedIOFrame_Addr_uint64_t',['../structAlignedIOFrame__Addr__uint64__t.html',1,'']]],
  ['ar',['ar',['../structarray__iter__s.html#aae35b043f11096770f9c2d56dff9c39a',1,'array_iter_s']]],
  ['array_5fconf_5fs',['array_conf_s',['../structarray__conf__s.html',1,'']]],
  ['array_5fiter_5fs',['array_iter_s',['../structarray__iter__s.html',1,'']]],
  ['array_5fs',['array_s',['../structarray__s.html',1,'']]],
  ['array_5fzip_5fiter_5fs',['array_zip_iter_s',['../structarray__zip__iter__s.html',1,'']]],
  ['atadevice',['AtaDevice',['../structAtaDevice.html',1,'']]],
  ['audioeventmessage',['AudioEventMessage',['../structAudioEventMessage.html',1,'']]]
];
