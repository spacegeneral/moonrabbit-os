var searchData=
[
  ['keyboard_20management_20kernel_20internals',['Keyboard management kernel internals',['../group__KERN__PERIPHERALS__KEYBOARD.html',1,'']]],
  ['kernelmemmap',['KernelMemMap',['../structKernelMemMap.html',1,'']]],
  ['key',['key',['../structtable__entry__s.html#a695a974a97d273d06c9df8bed765fdb3',1,'table_entry_s::key()'],['../structrbnode__s.html#a4f422f4c8fc975671018b0184e55a329',1,'rbnode_s::key()']]],
  ['key_5fcompare',['key_compare',['../structhashtable__conf__s.html#a4a54b815f6d0da05084bbf7213c164f2',1,'hashtable_conf_s']]],
  ['key_5flength',['key_length',['../structhashtable__conf__s.html#a2fd764c528ae91ac1eed1e642b054cda',1,'hashtable_conf_s']]],
  ['keyevent',['KeyEvent',['../structKeyEvent.html',1,'']]],
  ['kill',['kill',['../group__LIBC__SYS__PROC.html#ga6cd7dc5efe677dc5cd78e3059dc80a12',1,'kill(unsigned int taskid):&#160;kill.c'],['../group__LIBC__SYS__PROC.html#ga6cd7dc5efe677dc5cd78e3059dc80a12',1,'kill(unsigned int taskid):&#160;kill.c']]]
];
