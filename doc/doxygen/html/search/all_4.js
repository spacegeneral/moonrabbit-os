var searchData=
[
  ['datadomainclass',['DataDomainClass',['../group__LIBC__SYS__SEC.html#ga934e8ed32464fa53e5520507f3a0e061',1,'DataDomainClass():&#160;sec.h'],['../group__LIBC__SYS__SEC.html#ga46d03b0ed53237745bd0ef46cabda66f',1,'DataDomainClass():&#160;sec.h']]],
  ['debug_5finfo',['debug_info',['../structTask.html#a64fa31ee0675ab086c64cf575a9a0e9f',1,'Task']]],
  ['deque',['deque',['../structdeque__iter__s.html#a007c82d726b95420c6f6911f9bc12c0d',1,'deque_iter_s']]],
  ['deque_5fconf_5fs',['deque_conf_s',['../structdeque__conf__s.html',1,'']]],
  ['deque_5fiter_5fs',['deque_iter_s',['../structdeque__iter__s.html',1,'']]],
  ['deque_5fs',['deque_s',['../structdeque__s.html',1,'']]],
  ['deque_5fzip_5fiter_5fs',['deque_zip_iter_s',['../structdeque__zip__iter__s.html',1,'']]],
  ['directvideocommandparam',['DirectVideoCommandParam',['../structDirectVideoCommandParam.html',1,'']]],
  ['disown',['disown',['../group__LIBC__SYS__PROC.html#ga3faece4d51fbd0a3d84abd4a67fbbf77',1,'disown(unsigned int child_task_id):&#160;adopt.c'],['../group__LIBC__SYS__PROC.html#ga3faece4d51fbd0a3d84abd4a67fbbf77',1,'disown(unsigned int child_task_id):&#160;adopt.c']]],
  ['doorbutton',['DoorButton',['../structDoorButton.html',1,'']]]
];
