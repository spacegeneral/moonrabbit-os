var searchData=
[
  ['rbnode_5fs',['rbnode_s',['../structrbnode__s.html',1,'']]],
  ['rect',['Rect',['../structRect.html',1,'']]],
  ['regs',['regs',['../structTask.html#a136b243ee52ff89e9ba97f4e4dba19bb',1,'Task']]],
  ['req_5fsize',['req_size',['../structliballoc__minor.html#a0e798303ee8177899564e567fea41d3f',1,'liballoc_minor']]],
  ['request_5fident',['request_ident',['../structProcIdent.html#affa5c4cc405863883a4fbc9a91d01647',1,'ProcIdent']]],
  ['reset_5ftask',['reset_task',['../group__KERN__TASK.html#gae1a7de41d0fcd60c8dfb64d8f1dc6a58',1,'reset_task(unsigned int task_id):&#160;purge.c'],['../group__KERN__TASK.html#gae1a7de41d0fcd60c8dfb64d8f1dc6a58',1,'reset_task(unsigned int task_id):&#160;purge.c']]],
  ['resource_5fcounter_5flatch',['resource_counter_latch',['../structTask.html#a048e7300511051b65452ae5fa4b23d4e',1,'Task']]],
  ['retire_5fchannel',['retire_channel',['../group__LIBC__MQ.html#gab95c070fc1bdbd4164b3fe5e027beb9f',1,'retire_channel(char *topic_name):&#160;pubsub.c'],['../group__LIBC__MQ.html#gab95c070fc1bdbd4164b3fe5e027beb9f',1,'retire_channel(char *topic_name):&#160;pubsub.c']]],
  ['return_5fcode_5fhere',['return_code_here',['../structTask.html#a5b9d0fad5846d35a5f2b97da11dcdb49',1,'Task']]],
  ['right',['right',['../structrbnode__s.html#ac0de6778ab5d37b8a90ef75c8fe81050',1,'rbnode_s']]],
  ['rtl8139nic',['Rtl8139Nic',['../structRtl8139Nic.html',1,'']]]
];
