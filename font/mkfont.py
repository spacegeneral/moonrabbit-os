import argparse
import struct
import re
import os


linere = re.compile(r"([0-9a-zA-Z]+):([0-9a-zA-Z]+)");

version = 0x10
magic = 0x700472b1
 

def parsecli():
    parser = argparse.ArgumentParser(description="Create moonrabbit os bitmap font")
    parser.add_argument('-c', '--header', help='Output in .c header style', action='store_true')
    parser.add_argument('start', metavar='start_range', help='Beginning of the code point range', type=int)
    parser.add_argument('end', metavar='end_range', help='End of the code point range', type=int)
    return parser.parse_args()


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def save_codepoint_c(tofile, codepoint, bitmap):
    dimension = (len(bitmap) // 32) - 1;
    bitmapstr = "{" + ", ".join(map(lambda s: "0x" + s, chunks(bitmap, 2))) + "}"
    tofile.write("""{
    .dimension = %d,
    .bitmap = %s,
},""" % (dimension, bitmapstr))


def save_codepoint_binary(tofile, codepoint, bitmap):
    dimension = (len(bitmap) // 32) - 1;
    tofile.write(struct.pack("B", dimension))
    bitmaplist = list(map(lambda s: int(s, 16), chunks(bitmap, 2)))
    for num in bitmaplist:
        tofile.write(struct.pack("B", num))
    if dimension == 0:
        for _ in range(16):
            tofile.write(struct.pack("B", 0))
            
            
def scan_file(fname, pointmap):
    with open(fname, "r") as fp:
        lines = fp.read().split("\n")
        for line in lines:
            if len(line) == 0: continue
            match = linere.match(line)
            if match is None: continue
            codepoint = int(match.group(1), 16)
            bitmap = str(match.group(2))
            if (codepoint >= cli.start) and (codepoint < cli.end) or (codepoint == 65533):
                pointmap[codepoint] = bitmap


if __name__ == "__main__":
    cli = parsecli()
    
    if cli.header:
        outfile = open("unifont.h", "w")
        savefun = save_codepoint_c
    else:
        outfile = open("unifont.lbf", "wb")  # "Lunar Bitmap Font"
        savefun = save_codepoint_binary
    
    pointmap = {}
    for fname in sorted(os.listdir("./hex")):
        fpath = os.path.join("./hex", fname)
        print("loading symbols from %s" % fpath)
        scan_file(fpath, pointmap)
        
    print("Got %d symbols before pad (range %d - %d)." % (len(pointmap), min(pointmap.keys()), max(pointmap.keys())))
    
    for i in range(cli.start, cli.end):
        if i not in pointmap:
            pointmap[i] = pointmap[65533]
            
    if not cli.header:
        outfile.write(struct.pack("I", magic))
        outfile.write(struct.pack("I", version))
        outfile.write(struct.pack("I", len(pointmap)))

    cp_bmp_pairs = pointmap.items()
    cp_bmp_pairs = sorted(cp_bmp_pairs, key=lambda p: p[0])
    for codepoint, bitmap in cp_bmp_pairs:
        savefun(outfile, codepoint, bitmap)
    
    
    outfile.close()
