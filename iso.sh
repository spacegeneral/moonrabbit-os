#!/bin/sh
set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/moonrabbit_os.kernel.gz isodir/boot/moonrabbit_os.kernel.gz
cat > isodir/boot/grub/grub.cfg << EOF
set timeout=0
set default=0
menuentry "Moonrabbit OS" {
	multiboot /boot/moonrabbit_os.kernel.gz
	boot
}
EOF
grub-mkrescue -o moonrabbit_os.iso isodir
