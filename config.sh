SYSTEM_HEADER_PROJECTS="libm libcrypto libc libedit libgui tinycc kernel"
USER_HEADER_PROJECTS="libm libcrypto libc libedit libgui tinycc"
PROJECTS="libm libcrypto libc libedit libgui tinycc kernel"

export PATH=$PATH:$(pwd)/crosscompiler/bin

export MAKE=${MAKE:-make}
export HOST=${HOST:-$(./default-host.sh)}

export AR=${HOST}-ar
export AS=${HOST}-as
export CC=${HOST}-gcc

export PREFIX=/usr
export EXEC_PREFIX=$PREFIX
export BOOTDIR=/boot
export LIBDIR=$EXEC_PREFIX/lib
export INCLUDEDIR=$PREFIX/include

export CFLAGS='-O2 -gstabs1'
export CPPFLAGS=''

# Configure the cross-compiler to use the desired system root.
export SYSROOT="$(pwd)/sysroot"
export SYSROOTUSER="$(pwd)/sysroot_user"
export CC="$CC --sysroot=$SYSROOT"

# Work around that the -elf gcc targets doesn't have a system include directory
# because it was configured with --without-headers rather than --with-sysroot.
if echo "$HOST" | grep -Eq -- '-elf($|-)'; then
  export CC="$CC -isystem=$INCLUDEDIR"
fi

export BUILD_TYPE="release"
