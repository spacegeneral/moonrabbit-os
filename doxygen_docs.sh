#!/bin/sh

set -e

cd ./doxygen
doxygen Doxyfile > /dev/null 2>&1
cd -
