#!/bin/sh
set -e
. ./iso.sh

qemu-system-$(./target-triplet-to-arch.sh $HOST) -s -S -boot d -cdrom moonrabbit_os.iso -drive if=ide,media=disk,format=raw,file=testdisk.raw -soundhw pcspk -m 256M -netdev user,id=u1 -device rtl8139,netdev=u1 -object filter-dump,id=f1,netdev=u1,file=netdump.dat > /dev/null 2>&1 &
gdb-multiarch -x startup.gdb
