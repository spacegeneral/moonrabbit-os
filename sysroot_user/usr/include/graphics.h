#ifndef _GRAPHICS_H
#define _GRAPHICS_H

#include <geom.h>
#include <stddef.h>
#include <stdint.h>


typedef struct Bitmap {
    Rect geom;
    int *colordata;
} Bitmap;


#define CHAR_8x16 0
#define CHAR_16x16 1

// Must be power of two baka!
#define CHAR_HEIGHT_UNIT 16
#define CHAR_WIDTH_UNIT 8

#define MAX_BITMAP_SIZE 32
#define CODEPOINT_PLACEHOLDER 65533

// Only used inside the kernel for now
#define BITMAP_DISPLAY_OK 0
#define BITMAP_DISPLAY_UNSUPPORTED -1
#define BITMAP_DISPLAY_OUT_OF_BOUNDS -2

typedef struct __attribute__((packed)) BitmapFontHeader {
    uint32_t magic;
    uint32_t version;
    uint32_t upper_range;
} BitmapFontHeader;

typedef struct __attribute__((packed)) BitmapCodePoint {
    uint8_t dimension;
    uint8_t bitmap[MAX_BITMAP_SIZE];
} BitmapCodePoint;

size_t get_codepoint_width_units(BitmapCodePoint *cp);
size_t get_codepoint_height_units(BitmapCodePoint *cp);


#endif
