#ifndef _SYS_SERV_H
#define _SYS_SERV_H

#include <sys/ipc_common.h>
#include <sys/sec.h>


typedef enum ServStatus {
    SERV_SUCCESS = 0,
    SERV_ALREADY_EXISTS,
    SERV_DOES_NOT_EXIST,
    SERV_FULL,
    SERV_NO_REQUESTS,
    SERV_PLEASE_WAIT,
    SERV_AUTH_PROBLEM,
    
} ServStatus;


ServStatus advertise_service_advanced(char *topic_name, 
                                      unsigned int frame_length,
                                      PhisicalDomainClass phys_race_attrib,
                                      DataDomainClass data_class_attrib);

ServStatus advertise_service(char *topic_name, 
                             unsigned int frame_length);

ServStatus retire_service(char *topic_name);

ServStatus service_accept(char *topic_name, 
                          void *request, 
                          unsigned int *requestor_task_id, 
                          unsigned int *request_id);

ServStatus service_complete(char *topic_name, unsigned int request_id);

ServStatus service_poll(char *topic_name, unsigned int request_id);

ServStatus service_request(char *topic_name, void *request, unsigned int *request_id);



#endif
