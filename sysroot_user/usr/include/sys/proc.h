#ifndef _SYS_PROC_H
#define _SYS_PROC_H 1

#include <sys/sec.h>
#include <stdbool.h>
#include <sys/loader.h>
#include <sys/debuginfo.h>


/** \defgroup LIBC_SYS_PROC Task management libc API
 *
 * Interface allowing userspace application to perform
 * basic task management operations, such as creating
 * a new task, killing a task, and voluntarily
 * switching to another task.
 */

/** @{ */


// Special program return codes:


// security clearance issues
#define LAUNCH_NOT_AUTHORIZED (-1)

// child thread yielded back to parent thread
// note: this is also used internally by spoon() to maintain the ring
#define NOT_DONE_YET (-2)

// tried to switch to a non-existing task
#define TASK_DOES_NOT_EXIST (-3)

// exactly what it says
#define TASK_KILLED_BECAUSE_EXCEPTION (-4)

// exactly what it says
#define TASK_KILLED (-5)

// program was loaded and prepared, but never launched
#define READY_TO_RUN (-6)

#define ABORTED (-7)

#define ADOPTION_NOT_ALLOWED (-8)

// Process initializer status codes
#define PROCINIT_STDIN_INIT_ERROR (-50)
#define PROCINIT_KEYCODE_INIT_ERROR (-60)


typedef struct TaskInfos {
    unsigned int task_id;
    unsigned int parent_task;
    unsigned int owner_id;
    unsigned long long time_counter[2];
    char name[256];
    PhisicalDomainClass race;
    DataDomainClass class;
} TaskInfos;


/** Struct collecting a series of informations regarding the identity of a running task
 */
typedef struct ProcIdent {
    unsigned int request_ident;       ///< ::IdentifiedAgent uid of the agent requesting the ProcIdent information, obtained via memory tag
    unsigned int taskid;    ///< ::Task task_id of the running task
    unsigned int owner_ident; ///< ::IdentifiedAgent uid of the agent owning the current task, same as request_ident (if no errors occurred)
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    bool is_user;           ///< true if user, false if supervisor
} ProcIdent;


/** Primitive function for starting a new task. The current task will be blocked waiting
 * for the child task to terminate.
 * The caller must specify the file path containing the executable code and the desired clearance
 * level for the new task. Note that the clearance level of the child task must be lower or equal to
 * that of the parent task.
 * A new agent will also be spawned in order to gain ownership of the child task.
 * \return the positive value returned by the program on exit, or a negative value in case of error.
 */
int spoon(const char *filename,
          PhisicalDomainClass phys_race_attrib,
          DataDomainClass data_class_attrib);


/** Easier variant of ::spoon, epspoon will start a child task with the same clearance level as the parent.
 * "epspoon" stands for "easy peasy spoon".
 * \return the positive value returned by the program on exit, or a negative value in case of error.
 */
int epspoon(const char *filename);


/** Primitive function for starting a new task. This function spawns a child task which will
 * live indipendently from the original invoker of ::spork.
 * This function arranges for the newborn task to be immediately adopted by the init process.
 * In case of failure in the adoption procedure, ::spork will fall back to the ::spoon behavior. 
 * The caller must specify the file path containing the executable code and the desired clearance
 * level for the new task. Note that the clearance level of the child task must be lower or equal to
 * that of the parent task.
 * The caller must also pass a pointer to a memory area where to receive the exit code from the child
 * task (NULL can be specified, if the caller does't care about the return code). 
 * A new agent will be spawned in order to gain ownership of the child task.
 * \return the positive task-id of the newborn task, or a negative value in case of error.
 */
int spork(const char *filename,
          PhisicalDomainClass phys_race_attrib,
          DataDomainClass data_class_attrib, 
          int *return_code_here);


/** Easier variant of ::spork, epspork will start a child task with the same clearance level as the parent.
 * "epspoon" stands for "easy peasy spork".
 * \return the positive task-id of the newborn task, or a negative value in case of error.
 */
int epspork(const char *filename, int *return_code_here);


/** This function uses ::clone internally in order to spawn a new thread quickly and easily,
 * much in the same way ::spork spawns an entirely new task.
 * The caller must pass a pointer to the entry point of the new thread (i.e. a C function pointer)
 * \return the positive task-id of the newborn task, or a negative value in case of error.
 */
int spork_thread(void *thread_entry_point);


/** This function is used internally by other task-management primitives, such
 * as ::spoon or ::spork.
 * This function creates a new task using the executable code within an 
 * user-supplied file, and assigns it to a new agent with an user-supplied
 * clearance level.
 * The new task is not started; instead, its task id is copied into the location
 * pointed by newtask_id.
 * The caller must also supply a pointer to a location where to receive the exit code from the child
 * task (NULL can be specified, if the caller does't care about the return code). 
 * \return 0 for success, an error code otherwise.
 */
int prelaunch(const char *filename,
              PhisicalDomainClass phys_race_attrib,
              DataDomainClass data_class_attrib,
              unsigned int *newtask_id, 
              int *return_code_here);


/** This function creates (but does not start) a clone of the current task,
 * which behaves roughly the same way as a "thread".
 * The clone will:
 * - have the same owner (and thus security clearance) as the original
 * - share the code, global data and dynamically allocated memory areas with the original
 * - have its own stack
 * - not inherit the open files of the original
 * - not inherit the subscribed MQ channels of the original
 * 
 * The caller must pass a pointer to the entry point of the new thread (i.e. a C function pointer)
 * and a pointer to an unsigned int which will be filled with the task id of the clone.
 * \return 0 for success, an error code otherwise.
 */
int clone(void* thread_entry_point, unsigned int* newtask_id);


/** This function is intended to be used alongside a JIT compiler.
 * The caller must provided a pointer to a memory area containing an
 * already relocated and laoded elf executable, the entry point of said executable,
 * the ::TaskDebuginfo generated at load time, and a human readable name for the new program.
 * The operating system will then hotswap the code of the current task with the one provided trough
 * the ::henshin call, and finally jump to its entrypoint.
 * The resources held by the task before calling ::henshin will be freed (except for the environment variables).
 */
void henshin(unsigned char* ready_elf, uintptr_t entrypoint, TaskDebuginfo *dbginfo, char *program_name);


/** This function returns informations about the identity of the current task
 * \return a ::ProcIdent struct
 */
ProcIdent ident();


/** This function allows the caller to voluntarily yield control of the CPU,
 * returning the control to its parent task.
 * \return 0 for success, an error code otherwise.
 */
int yield();


/** This function allows the caller to voluntarily yield control of the CPU,
 * returning the control to another task specified by its task_id.
 * \return 0 for success, an error code otherwise.
 */
int task_switch(unsigned int taskid);


/** This function kills an existing task and frees its resources.
 * The task must be specified using its task_id.
 * \return 0 for success, an error code otherwise.
 */
int kill(unsigned int taskid);


/** The current task can use this function in order to attempt to adopt
 * another existing task, specified using its task_id.
 * Note that the candidate child task must be up-for-adoption in order
 * for this operation to succeed.
 * \return 0 for success, an error code otherwise.
 */
int adopt(unsigned int child_task_id);


/** The current task can use this function in order to put
 * up-for-adoption one of its child tasks (specified by its task_id).
 * After ::disown has returned, the caller task will no
 * longer be the parent of the specified child task.
 * \return 0 for success, an error code otherwise.
 */
int disown(unsigned int child_task_id);


int gift_memarea(void* memarea, unsigned int new_owner_id);  //TODO test this



void expirecc(void* resume, unsigned long long expire_ms);
void continuation();


/** @} */

#endif
 
