#ifndef _SYS_DEFAULT_TOPICS_H
#define _SYS_DEFAULT_TOPICS_H


// Message queues

#define DEFAULT_GRAVEYARD_TOPIC "/sys/graveyard"

#define DEFAULT_STDIN_TOPIC "/sys/io/keyboard/stdin"
#define DEFAULT_KEYCODE_TOPIC "/sys/io/keyboard/keycode"
#define DEFAULT_STDOUT_TOPIC "/sys/io/stdout"
#define DEFAULT_TERMOUT_TOPIC "/sys/io/terminal/out"
#define DEFAULT_DEBUG_TOPIC "/sys/io/debug/out"

#define DEFAULT_AUDIO_BEEPER_TOPIC "/sys/io/audio/beeper/event"
#define DEFAULT_AUDIO_SINK_TEMPLATE_TOPIC "/sys/io/audio/card%d/sink/%s"

#define DEFAULT_PS2_KEYBOARD_INPUT_TOPIC "/sys/io/keyboard/ps2"
#define DEFAULT_KEYBOARD_REMAP_TOPIC "/sys/io/keyboard/remap"

#define DEFAULT_PS2_MOUSE_INPUT_TOPIC "/sys/io/mouse/ps2"
#define DEFAULT_MOUSE_INPUT_TOPIC "/sys/io/mouse/stdpoint"

#define DEFAULT_COM1_IN_TOPIC "/sys/io/serial/com1/recv"
#define DEFAULT_COM1_OUT_TOPIC "/sys/io/serial/com1/send"

#define DEFAULT_KLOG_TOPIC "/sys/klog"

#define DEFAULT_ACPI_POWEROFF_TOPIC "/sys/power/acpi/shutdown"

#define DEFAULT_TASK_ADOPTION_TOPIC "/usr/ring0/adopt"

#define DEFAULT_NET_IFACE_BASE_TOPIC "/sys/net/iface"

#define DEFAULT_SOCKET_RECV_FMT_TOPIC "/usr/net/socket/%d/recv"
#define DEFAULT_SOCKET_SEND_FMT_TOPIC "/usr/net/socket/%d/send"

#define DEFAULT_HAPPY_TOPIC "/sys/happy"


// Services

#define DEFAULT_ROOTFS_SERVICE "/sys/fs/rootfs"

#define DEFAULT_TERMINFO_SERVICE "/sys/io/terminal/info"

#define DEFAULT_TERM_DRAWBITMAP_SERVICE "/sys/io/terminal/draw/bitmap"

#define DEFAULT_SOCKET_SERVICE_BASE "/usr/net/socket"
#define DEFAULT_SOCKET_CONTROL_SERVICE "/usr/net/socket/control"

#define DEFAULT_SHARE_SERVICE "/sys/share"

#endif
