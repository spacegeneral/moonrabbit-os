#ifndef _SYS_NETWORK_H
#define _SYS_NETWORK_H

#include <stdio.h>
#include <stdint.h>
#include <enumtricks.h>


#define IPv4_ADDRESS_LENGTH 4
#define IPv4_STRING_LENGTH (IPv4_ADDRESS_LENGTH*4)
#define IPv6_ADDRESS_LENGTH 16
#define IPv6_STRING_LENGTH (IPv6_ADDRESS_LENGTH*4)

#define IP_ADDRESS_LENGTH IPv6_ADDRESS_LENGTH
#define IP_STRING_LENGTH (IP_ADDRESS_LENGTH*4)


#define MAC_ADDRESS_LENGTH 6
#define MAC_STRING_LENGTH (MAC_ADDRESS_LENGTH*3)
#define MAX_NIC_NAME_LEN 256


#define MAX_LINK_MTU 1520
#define MAX_ETHERNET_MTU 1500
#define MIN_ETHERNET_MTU 46
#define MAX_IP_MTU 512

#define MAX_UDP_MTU (MAX_IP_MTU - 8)
#define MAX_TCP_MTU (MAX_IP_MTU - 20)

#define TCP_INITIAL_WINDOW_SIZE (MAX_TCP_MTU * 4)



#define FOREACH_NETLINKTYPE(APPLIED) \
    APPLIED(Ethernet10BASE_T) \
    APPLIED(Ethernet100BASE_TX) \
    APPLIED(Ethernet1000BASE_T) \
    APPLIED(Ethernet10GBASE_T) \
    
    
typedef enum NetLinkType {
    FOREACH_NETLINKTYPE(GENERATE_ENUM)
} NetLinkType;

static unsigned long long int __attribute__((unused)) net_link_type_speed_bps[] = {
    [Ethernet10BASE_T]   = 10000000,
    [Ethernet100BASE_TX] = 100000000,
    [Ethernet1000BASE_T] = 1000000000,
    [Ethernet10GBASE_T]  = 10000000000,
};

static const char __attribute__((unused)) *net_link_type_name[] = {
    FOREACH_NETLINKTYPE(GENERATE_STRING)
};


typedef struct __attribute__((packed)) NetPacket {
    uint64_t length;
    uint8_t data[MAX_LINK_MTU];
} NetPacket;


typedef struct NetIfaceManifest {
    unsigned int nic_id;
    char name[MAX_NIC_NAME_LEN];
    unsigned char mac[MAC_ADDRESS_LENGTH];
    bool is_promisc;
    bool is_enabled;
    NetLinkType link_type;
    
} NetIfaceManifest;


typedef struct IpAddress {
    int version;
    unsigned char address[IP_ADDRESS_LENGTH];
} IpAddress;


static const unsigned char __attribute__((unused)) bcast_mac[MAC_ADDRESS_LENGTH] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
static const IpAddress __attribute__((unused)) bcast_ipv4 = {.version = 4, .address = {0xFF, 0xFF, 0xFF, 0xFF}};

static const IpAddress __attribute__((unused)) ip_any_ipv4 = {.version = 4, .address = {0, 0, 0, 0}};
static const IpAddress __attribute__((unused)) ip_any_ipv6 = {.version = 6, .address = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};


void format_mac(char *str, const unsigned char *mac);
void format_ip(char *str, const IpAddress *ip);
bool parse_ip(char *str, IpAddress *ip);
bool parse_mac(char *str, unsigned char *mac);
bool ip_equals(const IpAddress *a, const IpAddress *b);
bool mac_equals(const unsigned char *a, const unsigned char *b);

#endif
 
