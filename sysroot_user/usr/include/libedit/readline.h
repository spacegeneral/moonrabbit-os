#ifndef _LIBEDIT_READLINE_H
#define _LIBEDIT_READLINE_H


#include <geom.h>
#include <libedit/characterline.h>


typedef bool (*readline_custom_key_handler) (CharacterLine *line, KeyEvent *key);


int readline_simple(char *str);
int readline_custom(char *str, readline_custom_key_handler user_handler);

int readline_editline(char *str);
int readline_editline_custom(char *str, readline_custom_key_handler user_handler);

#endif
