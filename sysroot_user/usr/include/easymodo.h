#ifndef _EASYMODO_H
#define _EASYMODO_H


#include <stdint.h>


void trick_align_copy_u32(uint32_t *dest, uint32_t val) {
    uint8_t *dest8 = (uint8_t*) dest;
    
    dest8[0] = (val      ) & 0x000000FF;
    dest8[1] = (val >>  8) & 0x000000FF;
    dest8[2] = (val >> 16) & 0x000000FF;
    dest8[3] = (val >> 24) & 0x000000FF;
}

uint32_t trick_retrieve_unaaligned_u32(uint32_t *from) {
    uint8_t *from8 = (uint8_t*) from;
    
    return (((uint32_t) from8[0])      ) \
         | (((uint32_t) from8[1]) << 8 ) \
         | (((uint32_t) from8[2]) << 16) \
         | (((uint32_t) from8[3]) << 24) \
         ;
}



#endif
