#ifndef _LIBGUI_COLOR_H
#define _LIBGUI_COLOR_H


#define COLOR24BPP 4  /* BYTE per pixel  */
typedef int Color24;

Color24 make_color_8bpp(int red, int green, int blue);


#endif
