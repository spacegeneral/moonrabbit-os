#ifndef _GRAPHICS_UTF8DECODER_H
#define _GRAPHICS_UTF8DECODER_H


#define UTF8_END   -1
#define UTF8_ERROR -2

#define UTF8DECODER_BUFFER_LEN 8
#define MAX_UTF8_SEQUENCE_LEN 4

typedef struct Utf8Decoder {
    char buffer[UTF8DECODER_BUFFER_LEN];
    int index;
    int length;
} Utf8Decoder;

void utf8_decoder_init(Utf8Decoder *decoder);
int utf8_decoder_feed(Utf8Decoder *decoder, char c);


#endif
