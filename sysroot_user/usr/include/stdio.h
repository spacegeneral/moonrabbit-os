#ifndef _STDIO_H
#define _STDIO_H 1

#include <sys/cdefs.h>
#include <sys/term.h>
#include <sys/keycodes.h>
#include <sys/fs.h>
#include <sys/default_topics.h>
#include <sys/var_io_topics.h>
#include <stddef.h>
#include <stdbool.h>
#include <graphics.h>
#include <printf.h>


#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

#define NO_CHARACTER ((unsigned)(-1))


#ifdef __cplusplus
extern "C" {
#endif

// controls whether the getchar function echoes the read character
extern bool do_echo;


int putchar(int);
int fputc(int c, FILE *stream);
int puts(const char*);
int fputs(const char *s, FILE *stream);
void cls();
int getchar();
int getchar_nosub() ;
int fgetc(FILE *stream);
int ungetc(int c, FILE *stream);
KeyEvent getkey(bool ignore_released);
int getkcode();
int getkcode_async();
char *gets(char *s);
char *getsn(char *s, size_t limit);
char *getsn_customstop(char *s, size_t limit, char *stopchars, size_t num_stopchars);
char *fgets(char *s, int size, FILE *stream);


int cprintf(const char* topic, const char *fmt, ...);
int fprintf(FILE *fp, const char *fmt, ...);
int vfprintf(FILE *fp, const char *fmt, va_list va);


FILE *fopen(const char *pathname, const char *mode);
FILE *fopen_no_zlib(const char *pathname, const char *mode);

int fclose(FILE *stream);
size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
int fflush(FILE *stream);
int fseek(FILE *stream, long offset, int whence);
long ftell(FILE *stream);

FILE *fopen_pure(const char *pathname, const char *mode);
int fclose_pure(FILE *stream);
size_t fread_pure(void *ptr, size_t size, size_t nmemb, FILE *stream);
size_t fwrite_pure(const void *ptr, size_t size, size_t nmemb, FILE *stream);
int fflush_pure(FILE *stream);
int fseek_pure(FILE *stream, long offset, int whence);
long ftell_pure(FILE *stream);

FILE *fopen_zlib(const char *pathname, const char *mode);
int fclose_zlib(FILE *stream);
size_t fread_zlib(void *ptr, size_t size, size_t nmemb, FILE *stream);
size_t fwrite_zlib(const void *ptr, size_t size, size_t nmemb, FILE *stream);
int fflush_zlib(FILE *stream);
int fseek_zlib(FILE *stream, long offset, int whence);
long ftell_zlib(FILE *stream);


int rewind(FILE *stream);
int feof(FILE *stream);
int ferror(FILE *stream);

void setcursor(int x, int y);
void togglecursor(int enabled);
void setcolors(int foreground, int background);
void settermflags(unsigned int flags);
void setzoomlevel(int level);
void scrollreverse();
void setscrollrange(int scroll_top, int scroll_bottom);
void setwritingdirection(WritingDirection direction);
void display_bitmap(Bitmap *bitmap, int *colorkey);
void puttag(char* tagstr);
void poptag();


int scanf(const char *format, ...);
int fscanf(FILE *stream, const char *format, ...);
int sscanf(const char *str, const char *format, ...);


void setup_stdio(void);

void teardown_stdio(void);


#define debug_printf(fmt, ...) cprintf(DEFAULT_DEBUG_TOPIC, fmt, ##__VA_ARGS__)


#ifdef __cplusplus
}
#endif

#endif
