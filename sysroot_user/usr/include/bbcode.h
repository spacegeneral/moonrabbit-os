#ifndef _BBCODE_H
#define _BBCODE_H



typedef void (*parsed_content_callback) (char* tag_name, char* tag_attribute, char* tag_content);
typedef void (*parsed_tag_open_callback) (char* tag_name, char* tag_attribute);
typedef void (*parsed_tag_close_callback) (char* tag_name);


void parse_bbcode(char* code, 
                  parsed_content_callback callback_content, 
                  parsed_tag_open_callback callback_open, 
                  parsed_tag_close_callback callback_close);

void print_bbcode_open_tag_function(char* tag_name, char* tag_attribute);
void print_bbcode_content_function(char* tag_name, char* tag_attribute, char* tag_content);
void print_bbcode_close_tag_function(char* tag_name);

int color_string_to_code(char* color);


#endif
