#ifndef _UNISTD_H
#define _UNISTD_H

#include <stddef.h>
#include <sys/fs.h>

int open(const char *path, int oflag, ...);
FILE* fdopen(int fildes, const char *mode);
int close(int fildes);
size_t read(int fildes, void *buffer, size_t length);
size_t write(int fildes, const void *buffer, size_t length);
int lseek(int fildes, long offset, int whence);

#ifndef ssize_t
#define ssize_t size_t
#endif

#define unlink remove

#endif
 
