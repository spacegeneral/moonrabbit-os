#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <mm/multiboot.h>
#include <mm/mm.h>
#include <debug.h>
#include <elfload/elfarch.h>
#include <elfload/elf.h>


void extract_grub_multiboot_mmap(multiboot_info_t* mbt) {
    unsigned int mmap = mbt->mmap_addr;
    multiboot_memory_map_t* curr_mmap;
    size_t areas_found = 0;
    
    while (mmap < mbt->mmap_addr + mbt->mmap_length) {
        curr_mmap = (multiboot_memory_map_t*) mmap;
        
        kernel_mem_map.mb_mmaps[areas_found] = *curr_mmap;
        areas_found++;
        
        mmap += curr_mmap->size + sizeof(curr_mmap->size);
    }
    
    kernel_mem_map.num_mb_mmaps = areas_found;
}

unsigned long get_memory_end() {
    unsigned long highest = 0;
    for (size_t m=0; m<kernel_mem_map.num_mb_mmaps; m++) {
        multiboot_memory_map_t* curr_mmap = kernel_mem_map.mb_mmaps + m;
        unsigned long memend = ((unsigned long)curr_mmap->addr) + ((unsigned long)curr_mmap->len);
        if (memend > highest) highest = memend;
    }
    return highest;
}

void extract_grub_elf_map(multiboot_info_t* mbt) {
    kernel_mem_map.num_elf_mmaps = 0;
    
    uint8_t *headers_base = (uint8_t*) mbt->u.elf_sec.addr;
    Elf_Shdr *sh_strtab = (Elf_Shdr*)(headers_base + (mbt->u.elf_sec.shndx * mbt->u.elf_sec.size));
    
    char *string_table = (char*) (sh_strtab->sh_addr);
    
    Elf_Shdr *header;
    
    for (size_t sec=0; sec<mbt->u.elf_sec.num; sec++) {
        header = (Elf_Shdr*) (headers_base + sec*mbt->u.elf_sec.size);
        if (header->sh_size > 0) {
            strncpy(kernel_mem_map.elf_mmaps[kernel_mem_map.num_elf_mmaps].name, string_table + header->sh_name, MAX_SECTIONNAME_LENGTH);
            kernel_mem_map.elf_mmaps[kernel_mem_map.num_elf_mmaps].start = (uintptr_t)header->sh_addr;
            kernel_mem_map.elf_mmaps[kernel_mem_map.num_elf_mmaps].length = (size_t)header->sh_size;
            kernel_mem_map.num_elf_mmaps++;
        }
    }
}

unsigned long get_kernel_end() {
    unsigned long highest = 0;
    for (size_t e=0; e<kernel_mem_map.num_elf_mmaps; e++) {
        TaskSection* curr_mmap = kernel_mem_map.elf_mmaps + e;
        unsigned long memend = ((unsigned long)curr_mmap->start) + ((unsigned long)curr_mmap->length);
        if (memend > highest) highest = memend;
    }
    return highest;
}

