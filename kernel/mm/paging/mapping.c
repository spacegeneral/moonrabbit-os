#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <mm/paging.h>
#include <security/identify.h>
#include <security/hardware.h>
#include <kernel/kidou.h>





void init_memory_mapping() {
    PageManagementObject *kernel_map = new_page_management_entry(God, Emperor);
    // map the 1024 page directory entries to the address of 1024 page page tables
    for (uint32_t page_dir_entry=0; page_dir_entry<PAGE_DIR_ENTRIES; page_dir_entry++) {
        uint32_t *current_int_view = (uint32_t*) (kernel_map->page_directory + page_dir_entry);
        (*current_int_view) = 0;
        (*current_int_view) |= (((uint32_t) kernel_map->page_tables + (4 * PAGE_TAB_ENTRIES * page_dir_entry)) & 0xfffff000) | (PAGE_BIT_USER|PAGE_BIT_RW|PAGE_BIT_PRESENT);  // we set the USER bit in the page directory; the actual kernel/user distinction is enacted at page table granularity
    }
}

void indentity_map_kernel() {
    PageManagementObject *kernel_map = get_page_management_entry(God, Emperor);
    assert(kernel_map != NULL);
    ITERATE_OVER_PT_ENTRIES(current_int_view, phys_address, kernel_map, {
        (*current_int_view) |= phys_address | (PAGE_BIT_RW|PAGE_BIT_PRESENT);
    });
}

PageTableEntry* virt_addr_to_page_entry(uint32_t virt_addr, IdentifiedAgent *owner) {
    PageManagementObject *map = get_page_management_entry_owned_by(owner);
    size_t npage_table = (virt_addr >> 22) & 0x3FF;
    uint32_t *page_dir_entry = (uint32_t*) (map->page_directory + npage_table);
    size_t npage_tab_entry = (virt_addr >> 12) & 0x3FF;
    PageTableEntry *page_table_start = (PageTableEntry*)((uint32_t)(*page_dir_entry) & 0xfffff000);
    PageTableEntry *entry = page_table_start + npage_tab_entry;
    return entry;
}

void activate_mmu() {
    PageManagementObject *kernel_map = get_page_management_entry(God, Emperor);
    assert(kernel_map != NULL);
    uint32_t newcr3 = get_cr3_from_page_mgr_object(kernel_map);
    asm volatile ("mov eax, %0          \t\n\
                   mov cr3, eax         \t\n\
                                        \t\n\
                   mov eax, cr0         \t\n\
                   or eax, 0x80000000   \t\n\
                   mov cr0, eax"
                    :
                    :"r"(newcr3)
                    :"eax"
                    );
}

void refresh_tlb() {
    asm volatile (
        "mov eax, cr3     \t\n\
         mov cr3, eax"
    );
}

void setup_memory_mapping() {
    init_memory_mapping();
    indentity_map_kernel();
    activate_mmu();
#ifdef VERBOSE_STARTUP
    printf("identity mapped paging");
#endif
}
