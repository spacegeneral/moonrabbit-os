#include <stdint.h>
#include <stddef.h>
#include <mm/paging.h>
#include <mm/mm.h>
#include <security/identify.h>


/*
 * Note: the page tables are stored contiguously in memory, therefore we dont't need
 * to switch tables halfway using the page directory
 */
void or_bits_range(uint32_t *page_entry, size_t num_pages, unsigned int bits) {
    for (size_t p = 0; p<num_pages; p++) {
        uint32_t *curr = page_entry + p;
        (*curr) = (*curr) | bits;
    }
}

void set_bit_range(uint32_t *page_entry, size_t num_pages, unsigned int bit, unsigned int bit_pos) {
    uint32_t bit_select_mask = (1 << bit_pos);
    bit_select_mask = ~bit_select_mask;
    
    for (size_t p = 0; p<num_pages; p++) {
        uint32_t *curr = page_entry + p;
        uint32_t current_val_masked = (*curr) & bit_select_mask;
        (*curr) = (current_val_masked | (bit << bit_pos));
    }
}


void readonly_range(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner) {
    uint32_t *page_entry = (uint32_t*)virt_addr_to_page_entry((uint32_t)start_virt_address, owner);
    set_bit_range(page_entry, num_pages, 0, 1);
}


void set_ownership_range(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner, bool is_user) {
    uint32_t *page_entry = (uint32_t*)virt_addr_to_page_entry((uint32_t)start_virt_address, owner);
    set_bit_range(page_entry, num_pages, is_user & 1, 2);
}

void set_present_range(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner, bool present) {
    uint32_t *page_entry = (uint32_t*)virt_addr_to_page_entry((uint32_t)start_virt_address, owner);
    set_bit_range(page_entry, num_pages, present & 1, 0);
}


void set_pages_as_guard(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner) {
    set_present_range(start_virt_address, num_pages, owner, false);
    
    uint32_t *page_entry = (uint32_t*)virt_addr_to_page_entry((uint32_t)start_virt_address, owner);
    set_bit_range(page_entry, num_pages, 1, 9);
}

bool is_page_guard(void *start_virt_address, IdentifiedAgent *owner) {
    uint32_t *page_entry = (uint32_t*)virt_addr_to_page_entry((uint32_t)start_virt_address, owner);
    return (*page_entry) & PAGE_BIT_GUARD;
}

void set_pages_not_guard(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner) {
    set_present_range(start_virt_address, num_pages, owner, true);
    uint32_t *page_entry = (uint32_t*)virt_addr_to_page_entry((uint32_t)start_virt_address, owner);
    set_bit_range(page_entry, num_pages, 0, 9);
}