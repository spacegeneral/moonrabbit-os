#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <yakumo.h>
#include <stdbool.h>
#include <data/fsdial.h>
#include <data/path.h>
#include <mm/mm.h>
#include <task/task.h>
#include <security/defaults.h>


static size_t memstats_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    MemUsageStats stats;
    get_mem_statistics(&stats);
    memcpy(dest, &stats, max(maxsize, sizeof(MemUsageStats)));
    return sizeof(MemUsageStats);
}


static void setup_memstats_dial_file() {
    FileDescriptor* dialfile_r;
    FileDescriptor* dest_folder;
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_INFODIAL_RACE,
        .class_attrib = DEFAULT_SEC_INFODIAL_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    dial_fs_primitives.create_file(&dialfile_r, "memstats", meta);
    dial_bind_callback(dialfile_r, &memstats_dial_interface);
    resolve_path(OS_DIALS_R_PATH, &dest_folder);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_r);
} 


static void kernel_make_system_memmap_file() {
    FileDescriptor* mapfile;
    FileDescriptor* mapfile_small;
    FileDescriptor* dest_folder;
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_KERNELMAP_RACE,
        .class_attrib = DEFAULT_SEC_KERNELMAP_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    dial_fs_primitives.create_file(&mapfile, "kernel", meta);
    dial_bind_callback(mapfile, &kernel_memmap_dial_interface);
    dial_fs_primitives.create_file(&mapfile_small, "kernel.nodynamic", meta);
    dial_bind_callback(mapfile_small, &kernel_memmap_dial_interface);
    
    resolve_path(OS_DIALS_MEMMAPS_PATH, &dest_folder);
    
    file_method(dest_folder)->insert_into_directory(dest_folder, mapfile);
    file_method(dest_folder)->insert_into_directory(dest_folder, mapfile_small);
}


void setup_mem_special_files() {
    setup_memstats_dial_file();
    kernel_make_system_memmap_file();
}
