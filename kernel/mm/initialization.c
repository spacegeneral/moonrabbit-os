#include <stdio.h>
#include <mm/mm.h>
#include <kernel/kidou.h>
#include <arch/setup.h>


unsigned int kernel_heap_length;
KernelMemMap kernel_mem_map;
uintptr_t kernel_heap_start;


void initial_memory_setup(uint32_t mboot_magic, multiboot_info_t* mboot_info) {
    //printf("Grub magic: 0x%x; ", mboot_magic);
    if (mboot_magic != MULTIBOOT_BOOTLOADER_MAGIC) {
        printf("ERROR!\nUnsupported bootloader. Cannot retrieve memory map.");
        panic();
    }
    if (!(mboot_info->flags & MULTIBOOT_INFO_MEM_MAP)) {
        printf("ERROR!\nMemory map not present.");
        panic();
    }
    
    extract_grub_multiboot_mmap(mboot_info);
    extract_grub_elf_map(mboot_info);

    kernel_heap_start = (uintptr_t) (((get_kernel_end() + KERNEL_HEAP_ALIGN) / KERNEL_HEAP_ALIGN) * KERNEL_HEAP_ALIGN);
    
    kernel_heap_length = get_memory_end() - ((unsigned long)kernel_heap_start) - KERNEL_HEAP_MARGIN_AFTER;
#ifdef VERBOSE_STARTUP
    printf("0x%08x - 0x%08x", (unsigned long)kernel_heap_start, ((unsigned long)kernel_heap_start)+kernel_heap_length);
#endif
}
