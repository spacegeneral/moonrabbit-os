#include <mm/mm.h>
#include <mm/bios_data_areas.h>
#include <kernel/log.h>
#include <sys/debuginfo.h>


uint8_t *extended_bios_data_area;


void init_bios_data_areas() {
    extended_bios_data_area = (uint8_t*) (bios_data_area->ebda_pointer << 4);
    if (extended_bios_data_area == 0x00) {
        extended_bios_data_area = (uint8_t*) 0x000A0000;
        klog("Malformed BDA->ebda_pointer, defaulting to 0x000A0000\n");
    }
}

void kernel_internal_get_bios_areas(Array *append_sections_here) {
    TaskSection_append_new(append_sections_here, "BIOS real mode ivt (unused)", (uintptr_t)0, 1024);
    TaskSection_append_new(append_sections_here, "BIOS Data Area", (uintptr_t)bios_data_area, 256);
    TaskSection_append_new(append_sections_here, "unused free RAM", (uintptr_t)(((uint8_t*)bios_data_area)+256), 30436);
    TaskSection_append_new(append_sections_here, "OS bootsector", (uintptr_t)0x7C00, 512);
    size_t freeram_length = ((size_t) extended_bios_data_area) - 0x7E00;
    TaskSection_append_new(append_sections_here, "unused free RAM", (uintptr_t)0x7E00, freeram_length);
    size_t ebda_length = 0x100000 - ((size_t) extended_bios_data_area);
    TaskSection_append_new(append_sections_here, "Extended BIOS Data Area", (uintptr_t)extended_bios_data_area, ebda_length);
}
