#include <stdbool.h> 
#include <mm/mm.h>
#include <mm/paging.h> 
#include <security/interface.h>
#include <security/hardware.h>



bool transfer_mem_ownership(void *beginning, unsigned int owner_uid) {
    if (!SUBSYSTEM_READY(security, data_subsystem)) return true;  // don't cause trouble while we are halfway through initialization
    
    IdentifiedAgent *agent = get_agent(owner_uid);
    if (agent->uid == AGENT_NOT_PRESENT_ID) return false;
    
    void* memarea = find_address_owner(beginning);
    
    size_t mlength;
    if (!get_memarea_length(memarea, &mlength)) return false;
    
    size_t num_used_pages = mlength / PAGE_SIZE;
    if (num_used_pages == 0) return false;
    
    unsigned int previous_owner_uid = get_memarea_tag(memarea);
    bool success_tagging = tag_memarea(memarea, owner_uid);
    if (!success_tagging) return false;
    
    if (previous_owner_uid != 0) {
        cascade_kernel_appropriate_ownership(memarea, num_used_pages);
    }
    
    cascade_set_ownership(memarea, num_used_pages, agent);
    return true;
}
