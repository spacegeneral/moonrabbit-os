#include <mm/mm.h>


size_t mem_match_length(uint8_t *address, uint8_t *sequence, size_t sequence_length) {
    size_t count = 0;
    for (; count < sequence_length; count++) {
        if (address[count] != sequence[count]) break;
    }
    return count;
}


uint8_t *search_mem_region(uint32_t start, uint32_t end, uint8_t *sequence, size_t sequence_length) {
    uint8_t *curr_addr = (uint8_t*) start;
    uint8_t *end_addr = (uint8_t*) end;
    
    for (; curr_addr<end_addr; curr_addr++) {
        if (mem_match_length(curr_addr, sequence, sequence_length) == sequence_length) {
            return curr_addr;
        }
    }
    
    return NULL;
}
