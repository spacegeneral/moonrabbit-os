#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <kernel/core.h>
#include <kernel/log.h>
#include <debug.h>
#include <mm/page_allocator.h>
#include <mm/mm.h>
#include <sys/fs.h>


MemNode *memtree;
MemNode *backup_memtree;
size_t MAX_MEM_NODES;
size_t mem_node_exp_factor;

char *file_name_table = NULL;
size_t file_name_table_length = 0;
size_t num_file_name_entries = 0;
#define FILENAME_TABLE_GROW_FACTOR 4096


int make_new_leaf(size_t index, unsigned int start, unsigned int length, MemNodeType type) {
    if (index >= MAX_MEM_NODES) {
        return 0;
    }
    MemNode *leaf = memtree + index;
    leaf->tag = 0;
    leaf->start = start;
    leaf->length = length;
    leaf->type = type;
    leaf->file = 0;
    leaf->line = 0;
    return 1;
}




void* failure_no_nodes() {
#ifdef ADVANCED_DEBUG
    debug_printf("Allocator ran out of memory nodes (can't split)!\r\n");
#endif
    return NULL;
}

void* failure_no_mem() {
#ifdef ADVANCED_DEBUG
    debug_printf("Allocator ran out of memory!\r\n");
#endif
    return NULL;
}



static void *allocate_pages_recursive_retry(size_t num_pages, int desperation, void* (*failure)()) {
    size_t request_length = num_pages * PAGE_SIZE;
    size_t max_free_contiguous = size_of_largest_leaf(memtree, 0);
    
    if (request_length > max_free_contiguous) {
#ifdef ADVANCED_DEBUG
        size_t max_free = count_free_space(memtree, 0);
        debug_printf("Error: trying to allocate %lu bytes, have only %lu contiguous bytes (%lu total free bytes)\n", request_length, max_free_contiguous, max_free);
#endif
        return failure();
    }
    
    switch (desperation) {
        case 0:
            balance_memtree();  // when we go too deep in the tree, balance it and retry
            return allocate_pages(num_pages, desperation+1);
        case 1:
            if (!relocate_memtree(MAX_MEM_NODES_FROM_EXP(mem_node_exp_factor+1))) {
                return NULL;
            }
            // "commit" increase to memtree size
            mem_node_exp_factor++;
            return allocate_pages(num_pages, desperation+1);
        default:
            return failure();
    }
}


void *allocate_pages(size_t num_pages, int desperation) {
    unsigned int request_length = num_pages * PAGE_SIZE;
    size_t first_free_available = find_first_free_node(memtree, 0, request_length);
    
    if (first_free_available == MAX_MEM_NODES) {
        return allocate_pages_recursive_retry(num_pages, desperation, &failure_no_mem);
    }
    
    size_t split_node_index = first_free_available;
    MemNode *split_node = memtree + split_node_index;
    
    if (request_length == split_node->length) {  // no need to actually split, use it whole
        split_node->type = Occupied;
        
    } else if (request_length < split_node->length) {
        
        if ((LEFT_CHILD(split_node_index) >= MAX_MEM_NODES) || \
            (RIGHT_CHILD(split_node_index) >= MAX_MEM_NODES)) {
            
            if (desperation > 1) {
                #ifdef ADVANCED_DEBUG
                    debug_printf("MAX_MEM_NODES=%lu, split_node_index=%lu, LEFT_CHILD=%lu, RIGHT_CHILD=%lu\r\n", MAX_MEM_NODES, split_node_index, LEFT_CHILD(split_node_index), RIGHT_CHILD(split_node_index));
                #endif
            }
            
            return allocate_pages_recursive_retry(num_pages, desperation, &failure_no_nodes);
        }
        
        split_node->type = Split;
        split_node->tag = 0;
        
        int success = make_new_leaf(
            LEFT_CHILD(split_node_index), 
            split_node->start, 
            request_length, 
            Occupied) && \
        make_new_leaf(
            RIGHT_CHILD(split_node_index), 
            split_node->start + request_length, 
            split_node->length - request_length, 
            Free);
        
        if (!success) {
            printf("Allocator ran out of memory nodes!\n");
            panic();
        }
    } else {
        printf("If you can read this, the allocator is very very bugged.\n");
        panic();
    }
    
    return (void*) split_node->start;
}


size_t free_pages(void *address, size_t num_pages) {
    unsigned int request_length = num_pages * PAGE_SIZE;
    size_t target_node_index = find_node_starting_at(memtree, 0, (unsigned int) address);
    if (target_node_index == MAX_MEM_NODES) {
        printf("Freeing invalid memory area!\n");
        panic();
    }
    
    MemNode *target_node = memtree + target_node_index;
    if (request_length == target_node->length) {  // no split: simply convert the node to free
        target_node->type = Free;
        target_node->tag = 0;
    } else if (request_length < target_node->length) {   // target node is split if we free an area less than its length
        target_node->type = Split;
        make_new_leaf(
            LEFT_CHILD(target_node_index), 
            target_node->start, 
            request_length, 
            Free);
        make_new_leaf(
            RIGHT_CHILD(target_node_index), 
            target_node->start + request_length, 
            target_node->length - request_length, 
            Occupied);
    } else {
        printf("Trying to free too much memory at once!\n");
        panic();
    }
    
    return num_pages;
}


bool free_memarea(void *address) {
    size_t target_node_index = find_node_starting_at(memtree, 0, (unsigned int) address);
    if (target_node_index == MAX_MEM_NODES) {
        klog("Trying to free invalid memory area %p\n", address);
        return false;
    }
    
    MemNode *target_node = memtree + target_node_index;
    target_node->type = Free;
    target_node->tag = 0;
    return true;
}


void free_mem_by_tag(unsigned int tag) {
    recursive_free_tree_by_tag(memtree, 0, tag);
}

#ifdef TRACK_MEMORY

static bool find_filename_index_in_table(char *filename, size_t *index) {
    for (size_t e=0; e<num_file_name_entries; e++) {
        char *candidate = strsplit_chunk_at(file_name_table, file_name_table_length, e);
        if (strncmp(filename, candidate, MAX_FILENAME_LENGTH) == 0) {
            *index = e;
            return true;
        }
    }
    return false;
}

static void filename_table_insert_at(size_t pos, char *filename) {
    if (pos + strlen(filename) + 1 >= file_name_table_length) {
        // basically do a realloc
        size_t prev_size = file_name_table_length;
        file_name_table_length += FILENAME_TABLE_GROW_FACTOR;
        void *new_nametable = allocate_pages(fit_into_pages(file_name_table_length), 0);
        memcpy(new_nametable, file_name_table, prev_size);
        free_memarea(file_name_table);
        file_name_table = new_nametable;
        
        filename_table_insert_at(pos, filename);
        return;
    }
    
    strncpy(file_name_table + pos, filename, MAX_FILENAME_LENGTH);
}

void set_memarea_tracking_data(void *address, char *filename, unsigned linenum) {
    size_t target_node_index = find_node_starting_at(memtree, 0, (unsigned int) address);
    if (target_node_index == MAX_MEM_NODES) {
        return;
    }
    
    MemNode *target_node = memtree + target_node_index;
    target_node->line = linenum;
    
    char safe_filename[MAX_FILENAME_LENGTH];
    snprintf(safe_filename, MAX_FILENAME_LENGTH, "%s", filename);
    
    if (num_file_name_entries == 0) {
        filename_table_insert_at(0, safe_filename);
        target_node->file = 0;
        num_file_name_entries++;
    } else {
        size_t index = 0;
        bool already_exists = find_filename_index_in_table(filename, &index);
        if (already_exists) {
            target_node->file = (unsigned)index;
        } else {
            char *last_entry = strsplit_chunk_at(file_name_table, file_name_table_length, num_file_name_entries-1);
            size_t pos = ((size_t)last_entry) - ((size_t)file_name_table) + strlen(last_entry) + 1;
            filename_table_insert_at(pos, safe_filename);
            target_node->file = (unsigned)num_file_name_entries;
            num_file_name_entries++;
        }
    }
}

char *mm_get_filename_by_id(unsigned file_id) {
    if (file_id >= num_file_name_entries) {
        return "unknown";
    }
    return strsplit_chunk_at(file_name_table, file_name_table_length, file_id);
}

void apply_to_memareas_by_tag(unsigned int tag, void (*applied)(unsigned start, unsigned length, char *filename, unsigned linenum, void*), void* user_data) {
#else
void apply_to_memareas_by_tag(unsigned int tag, void (*applied)(unsigned start, unsigned length, void*), void* user_data) {
#endif
    recursive_apply_to_occupied_leaf_by_tag(memtree, 0, tag, applied, user_data);
}


bool get_memarea_length(void *address, size_t *length) {
    size_t target_node_index = find_node_starting_at(memtree, 0, (unsigned int) address);
    if (target_node_index == MAX_MEM_NODES) {
        return false;
    }
    
    MemNode *target_node = memtree + target_node_index;
    *length = target_node->length;
    return true;
}


bool tag_memarea(void *address, unsigned int tag) {
    size_t target_node_index = find_node_starting_at(memtree, 0, (unsigned int) address);
    if (target_node_index == MAX_MEM_NODES) {
        return false;
    }
    
    MemNode *target_node = memtree + target_node_index;
    target_node->tag = tag;
    return true;
}


unsigned int get_memarea_tag(void *address) {
    size_t target_node_index = find_node_starting_at(memtree, 0, (unsigned int) address);
    if (target_node_index == MAX_MEM_NODES) {
        return (unsigned)(-1);
    }
    
    MemNode *target_node = memtree + target_node_index;
    return target_node->tag;
}


void *find_address_owner(void *address) {
    size_t target_node_index = find_leaf_containing_address(memtree, 0, (unsigned int) address);
    if (target_node_index == MAX_MEM_NODES) {
        return NULL;
    }
    
    MemNode *target_node = memtree + target_node_index;
    if (target_node->type == Occupied) {
        return (void*) target_node->start;
    }
    
    return NULL;
}


void balance_memtree() {
    memcpy(backup_memtree, memtree, sizeof(MemNode) * MAX_MEM_NODES);
    balance_tree(memtree, backup_memtree);
}


void print_allocator_tree() {
    recursive_print_tree(memtree, 0, 0);
}

void print_mem_used() {  //TODO deprecate this
    recursive_print_used(memtree, 0);
}

void get_mem_statistics(MemUsageStats *stats) {
    stats->total_available = memtree[0].length;
    stats->free = count_free_space(memtree, 0);
}


bool relocate_memtree(size_t num_nodes) {
    assert(num_nodes >= MAX_MEM_NODES);
    
    void *old_memtree = memtree;
#ifdef ADVANCED_DEBUG
    size_t old_size = 2 * MAX_MEM_NODES * sizeof(MemNode);
#endif
    
    memset(backup_memtree, 0, MAX_MEM_NODES * sizeof(MemNode));
    
    MAX_MEM_NODES = ((MAX_MEM_NODES+1)*2)-1;
    
    void *newtree_base = allocate_pages(fit_into_pages(num_nodes * sizeof(MemNode) * 2), 10);
    if (newtree_base == NULL) {
#ifdef ADVANCED_DEBUG
        debug_printf("Could not relocate allocator tree (size %lu -> %lu)!\r\n", old_size, 2 * num_nodes * sizeof(MemNode));
#endif
        // rollback changes
        MAX_MEM_NODES /= 2;
        return false;
    }
    
    memcpy(newtree_base, memtree, num_nodes * sizeof(MemNode));
    
    memtree = newtree_base;
    backup_memtree = (MemNode*) (((uint8_t*) newtree_base) + (num_nodes * sizeof(MemNode)));
    
    free_memarea(old_memtree);
    MAX_MEM_NODES = num_nodes;
    
#ifdef ADVANCED_DEBUG
    debug_printf("Relocating allocator tree %p -> %p (size %lu -> %lu)\r\n", (uintptr_t)old_memtree, (uintptr_t)newtree_base, old_size, 2 * MAX_MEM_NODES * sizeof(MemNode));
#endif
    return true;
}


// The stack allocator allocates itself
void init_page_allocator(unsigned int heap_start, unsigned int heap_length) {
    // compute the allocator tree size
    mem_node_exp_factor = INITIAL_MEM_NODE_EXP;
    MAX_MEM_NODES = MAX_MEM_NODES_FROM_EXP(mem_node_exp_factor);
    // position the allocator tree start at the beginning of the heap
    memtree = (MemNode*) heap_start;
    // create the root node
    make_new_leaf(0, heap_start, heap_length, Free);
    // Take possession of the area containing the allocator tree itself.
    // We place the backup memtree conyiguously at the end of the normal memtree:
    // this way, we'll be able to reclaim the backup memtree memory for emergencies
    void *allocated_memtree = allocate_pages(fit_into_pages(2 * MAX_MEM_NODES * sizeof(MemNode)), 0);
    backup_memtree = (MemNode*) (((uint8_t*) allocated_memtree) + (MAX_MEM_NODES * sizeof(MemNode)));
    
    // paranoia check
    assert(allocated_memtree == (void*)memtree);
    size_t arealength;
    get_memarea_length(allocated_memtree, &arealength);
    assert(arealength >= (MAX_MEM_NODES * 2 * sizeof(MemNode)));
    
#ifdef TRACK_MEMORY
    file_name_table_length = FILENAME_TABLE_GROW_FACTOR;
    file_name_table = allocate_pages(fit_into_pages(file_name_table_length), 0);
#endif
}
