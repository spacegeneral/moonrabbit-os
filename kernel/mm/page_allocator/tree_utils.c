#include <stdio.h>
#include <debug.h>
#include <string.h>
#include <yakumo.h>
#include <assert.h>
#include <kernel/core.h>
#include <mm/page_allocator.h>


/*
 * Performs left-first traversal of tree, starting from root_index,
 * in search of the first Free node.
 * Only nodes with length more or equal than request_length are considered.
 * Returns the index of the first Free node found.
 * Returns MAX_MEM_NODES if none is found.
 */
size_t find_first_free_node(MemNode *tree, size_t root_index, unsigned int request_length) {
    if (root_index >= MAX_MEM_NODES) {
        return MAX_MEM_NODES;  // reject invalid node index
    }
    
    MemNode *current = tree + root_index;
    if (current->type == Free) {
        if (current->length >= request_length) return root_index;
        
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        
        size_t left_traversal_result = find_first_free_node(tree, left_index, request_length);
        if (left_traversal_result != MAX_MEM_NODES) {
            return left_traversal_result;
        }
        size_t right_traversal_result = find_first_free_node(tree, right_index, request_length);
        if (right_traversal_result != MAX_MEM_NODES) {
            return right_traversal_result;
        }
    }
    return MAX_MEM_NODES;
}


size_t find_node_starting_at(MemNode *tree, size_t root_index, unsigned int request_start) {
    if (root_index >= MAX_MEM_NODES) {
        return MAX_MEM_NODES;  // reject invalid node index
    }
    
    MemNode *current = tree + root_index;
    if (!((request_start >= current->start) && (request_start < current->start + current->length))) {
        return MAX_MEM_NODES;  // reject if node does not contain request_start address
    }
    
    if (current->type == Occupied) {
        if (current->start == request_start) return root_index;
        
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        
        size_t left_traversal_result = find_node_starting_at(tree, left_index, request_start);
        if (left_traversal_result != MAX_MEM_NODES) {
            return left_traversal_result;
        }
        size_t right_traversal_result = find_node_starting_at(tree, right_index, request_start);
        if (right_traversal_result != MAX_MEM_NODES) {
            return right_traversal_result;
        }
    }
    return MAX_MEM_NODES;
}


size_t find_leaf_containing_address(MemNode *tree, size_t root_index, unsigned int request_addr) {
    if (root_index >= MAX_MEM_NODES) {
        return MAX_MEM_NODES;  // reject invalid node index
    }
    
    MemNode *current = tree + root_index;
    if (!((request_addr >= current->start) && (request_addr < current->start + current->length))) {
        return MAX_MEM_NODES;  // reject if node does not contain request_addr
    }
    
    if (current->type == Occupied) {
        return root_index;
        
    } else if (current->type == Free) {
        return root_index;
        
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        
        size_t left_traversal_result = find_leaf_containing_address(tree, left_index, request_addr);
        if (left_traversal_result != MAX_MEM_NODES) {
            return left_traversal_result;
        }
        size_t right_traversal_result = find_leaf_containing_address(tree, right_index, request_addr);
        if (right_traversal_result != MAX_MEM_NODES) {
            return right_traversal_result;
        }
    }
    return MAX_MEM_NODES;
}


void recursive_free_tree_by_tag(MemNode *tree, size_t root_index, unsigned int request_tag) {
    if (root_index >= MAX_MEM_NODES) {
        return;
    }
    
    MemNode *current = tree + root_index;
    if (current->type == Occupied) {
        if (current->tag == request_tag) {
            current->tag = 0;
            current->type = Free;
        }
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        recursive_free_tree_by_tag(tree, left_index, request_tag);
        recursive_free_tree_by_tag(tree, right_index, request_tag);
    }
}


size_t size_of_largest_leaf(MemNode *tree, size_t root_index) {
    if (root_index >= MAX_MEM_NODES) {
        return 0;  // reject invalid node index
    }
    
    MemNode *current = tree + root_index;
    if (current->type == Free) {
        return current->length;
        
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        
        size_t largest_left_leaf = size_of_largest_leaf(tree, left_index);
        size_t largest_right_leaf = size_of_largest_leaf(tree, right_index);
        
        return max(largest_left_leaf, largest_right_leaf);
    }
    
    return 0;
}

#ifdef TRACK_MEMORY
void recursive_apply_to_occupied_leaf_by_tag(MemNode *tree, 
                                             size_t root_index, 
                                             unsigned int request_tag, 
                                             void (*applied)(unsigned, unsigned, char*, unsigned, void*), 
                                             void* user_data) {
#else
void recursive_apply_to_occupied_leaf_by_tag(MemNode *tree, 
                                             size_t root_index, 
                                             unsigned int request_tag, 
                                             void (*applied)(unsigned, unsigned, void*), 
                                             void* user_data) {
#endif
    if (root_index >= MAX_MEM_NODES) {
        return;
    }
    
    MemNode *current = tree + root_index;
    if (current->type == Occupied) {
        if (current->tag == request_tag) {
#ifdef TRACK_MEMORY
            applied(current->start, current->length, mm_get_filename_by_id(current->file), current->line, user_data);
#else
            applied(current->start, current->length, user_data);
#endif
        }
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        recursive_apply_to_occupied_leaf_by_tag(tree, left_index, request_tag, applied, user_data);
        recursive_apply_to_occupied_leaf_by_tag(tree, right_index, request_tag, applied, user_data);
    }
}


size_t enumerate_leaves_in_tree(MemNode *tree, 
                                size_t root_index, 
                                size_t *indices_store, 
                                size_t *indices_crawler_pointer) {
    if (root_index >= MAX_MEM_NODES) {
        return 0;
    }
    
    MemNode *current = tree + root_index;
    if (current->type == Occupied) {
        indices_store[*indices_crawler_pointer] = root_index;
        (*indices_crawler_pointer)++;
        return 1;
    } else if (current->type == Free) {
        indices_store[*indices_crawler_pointer] = root_index;
        (*indices_crawler_pointer)++;
        return 1;
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        size_t leaves_left = enumerate_leaves_in_tree(tree, left_index, indices_store, indices_crawler_pointer);
        size_t leaves_right = enumerate_leaves_in_tree(tree, right_index, indices_store, indices_crawler_pointer);
        return leaves_left + leaves_right;
    }
    return 0;
}


void iterator_of_faith_over_leaves(MemNode *tree, size_t root_index, size_t num_splits, void (*applied)(MemNode*, void*), void* userdata) {
    if (root_index >= MAX_MEM_NODES) {
        return;
    }
    
    if (root_index >= num_splits) {
        MemNode *current = tree + root_index;
        applied(current, userdata);
    } else {
        iterator_of_faith_over_leaves(tree, LEFT_CHILD(root_index), num_splits, applied, userdata);
        iterator_of_faith_over_leaves(tree, RIGHT_CHILD(root_index), num_splits, applied, userdata);
    }
}


void recursize_coalesce_free_siblings(MemNode *tree, size_t root_index) {
    if (root_index >= MAX_MEM_NODES) {
        return;
    }
    
    MemNode *current = tree + root_index;
    if (current->type != Split) return;
    
    recursize_coalesce_free_siblings(tree, LEFT_CHILD(root_index));
    recursize_coalesce_free_siblings(tree, RIGHT_CHILD(root_index));
    
    MemNode *left_child = tree + LEFT_CHILD(root_index);
    MemNode *right_child = tree + RIGHT_CHILD(root_index);
    
    if (left_child->type == Free && right_child->type == Free) {
        left_child->type = Null;
        right_child->type = Null;
        current->type = Free;
    }
}



struct balance_tree_iter_userdata {
    size_t iter_index;
    size_t *leaves;
    MemNode *backup_tree;
};

void balance_tree_iter_applied(MemNode *node, void *ud) {
    struct balance_tree_iter_userdata* userdata = (struct balance_tree_iter_userdata*) ud;
    size_t leafindex = userdata->leaves[userdata->iter_index];
    
    *node = userdata->backup_tree[leafindex];
    userdata->iter_index++;
}

void balance_tree(MemNode *balanced, MemNode *backup_unbalanced) {
    // coalesce adjacent free nodes
    recursize_coalesce_free_siblings(backup_unbalanced, 0);
    
    // we need a well-sized memory area to store the left-first-ordered leaf indices:
    // we can use the beginning of `balanced` (we'll fix it later)
    assert(sizeof(MemNode) > sizeof(size_t));  // or can't we...?
    size_t *leaves = (size_t*) balanced;
    
    // extract the leaves from the tree (store their indices in our special location)
    size_t leaves_crawler_pointer = 0;
    size_t num_leaves = enumerate_leaves_in_tree(backup_unbalanced, 0, leaves, &leaves_crawler_pointer);
    size_t num_splits = num_leaves - 1;
    
    // walk the new tree (balanced) left first, putting the old leaves back on
    // remember that the balanced memory area is also used to store the ordered leaf indices!
    // for this reason, we'll have to navigate it using the "iterator_of_faith", knowing:
    // 1. num_splits
    // 2. by construction, all the leaves follow all the splits
    struct balance_tree_iter_userdata userdata = {
        .iter_index = 0,
        .leaves = leaves,
        .backup_tree = backup_unbalanced,
    };
    iterator_of_faith_over_leaves(balanced, 0, num_splits, &balance_tree_iter_applied, (void*)&userdata);
    
    // finally, complete the balanced tree by inserting the split nodes
    for (long index = num_splits-1; index >= 0; index--) {
        MemNode * currnode = balanced + index;
        
        currnode->type = Split;
        currnode->tag = 0;
        currnode->file = 0;
        currnode->line = 0;
        
        MemNode * leftchild = balanced + LEFT_CHILD(index);
        MemNode * rightchild = balanced + RIGHT_CHILD(index);
        currnode->start = leftchild->start;
        currnode->length = leftchild->length + rightchild->length;
    }
}


unsigned int count_free_space(MemNode *tree, size_t root_index) {
    if (root_index >= MAX_MEM_NODES) {
        return 0;
    }
    
    MemNode *current = tree + root_index;
    if (current->type == Occupied) {
        return 0;
    } else if (current->type == Free) {
        return current->length;
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        return count_free_space(tree, left_index) + count_free_space(tree, right_index);
    }
    return 0;
}


void recursive_print_tree(MemNode *tree, size_t root_index, size_t depth_counter) {
    if (root_index >= MAX_MEM_NODES) {
        return;
    }
    
    MemNode *current = tree + root_index;
    if (current->type == Occupied) {
        indent_space(depth_counter*2);
        printf("[%d] Occupied  Start: 0x%x Length: %d B\n", root_index, current->start, current->length);
        
    } else if (current->type == Free) {
        indent_space(depth_counter*2);
        printf("[%d] Free      Start: 0x%x Length: %d B\n", root_index, current->start, current->length);
        
    } else if (current->type == Split) {
        indent_space(depth_counter*2);
        printf("[%d] Split     Start: 0x%x Length: %d B\n", root_index, current->start, current->length);
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        recursive_print_tree(tree, left_index, depth_counter+1);
        recursive_print_tree(tree, right_index, depth_counter+1);
    }
}


void recursive_print_used(MemNode *tree, size_t root_index) {
    if (root_index >= MAX_MEM_NODES) {
        return;
    }
    
    MemNode *current = tree + root_index;
    if (current->type == Occupied) {
        printf("[%d]  Start: 0x%x Length: %d B\n", root_index, current->start, current->length);
        
    } else if (current->type == Split) {
        size_t left_index = LEFT_CHILD(root_index);
        size_t right_index = RIGHT_CHILD(root_index);
        recursive_print_used(tree, left_index);
        recursive_print_used(tree, right_index);
    }
}




