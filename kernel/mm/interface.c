#include <mm/interface.h>
#include <mm/mm.h>
#include <kernel/kidou.h>
#include <stdio.h>


int memorymanagement_readylevel_internal = 0;

static int memorymanagement_ready_level() {
    return memorymanagement_readylevel_internal;
}


static void memorymanagement_initialize(uint32_t mboot_magic, multiboot_info_t* mboot_info) {
#ifdef VERBOSE_STARTUP
    printf("Initializing memory management subsystem.\n    Detecting usable memory: ");
#endif
    initial_memory_setup(mboot_magic, mboot_info);
#ifdef VERBOSE_STARTUP
    printf(" done!\n    Scanning bios data areas... ");
#endif
    init_bios_data_areas();
#ifdef VERBOSE_STARTUP
    printf("done!\n    Initializing page allocator... ");
#endif
    init_page_allocator((unsigned int) kernel_heap_start, (unsigned int) kernel_heap_length);
#ifdef VERBOSE_STARTUP
    printf(" done!\nMemory subsystem 50%% ready.\n");
#endif
    memorymanagement_readylevel_internal = 50;
}

static void memorymanagement_finalize() {
#ifdef VERBOSE_STARTUP
    printf("Finalizing memory management subsystem.\n    Setting up memory map with ");
#endif
    setup_memory_mapping();
#ifdef VERBOSE_STARTUP
    printf(" done!\nMemory subsystem 100%% ready.\n");
#endif
    memorymanagement_readylevel_internal = 100;
}


static void memorymanagement_teardown() {
    memorymanagement_readylevel_internal = 0;
}


UNIQUE_IMPLEMENTATION_OF(mm, memory_subsystem, { \
    .ready_level = &memorymanagement_ready_level, \
    .initialize = &memorymanagement_initialize, \
    .finalize = &memorymanagement_finalize, \
    .teardown = &memorymanagement_teardown \
})
