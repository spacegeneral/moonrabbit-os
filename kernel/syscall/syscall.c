#include <stdio.h>
#include <syscall.h>
#include <os.h>
#include <arch/setup.h>
#include <arch/interrupt.h>
#include <kernel/log.h>
#include <mm/mm.h>
#include <security/identify.h>
#include <task/task.h>
#include <task/sched.h>


int current_time_selector = TIME_COUNTER_USER;


void __attribute__((optimize("O0"))) handle_syscall(struct interrupt_frame* frame) {
    current_time_selector = TIME_COUNTER_KERNEL;
    addr_t code = saved_registers_location->eax;
    addr_t param_ptr = saved_registers_location->ebx;
    addr_t source_addr = frame->eip;
    change_saved_ip(saved_registers_location, source_addr);
    change_saved_sp(saved_registers_location, frame->esp);
    
    if (code >= NUM_AVAILABLE_SYSCALLS) {
        klog("Warning: requesting unknown syscall code: 0x%x param: 0x%x\n", code, param_ptr);
    } else {
        syscall_impl handler = syscall_table[code];
        if (handler == 0) {
            klog("Warning: syscall not implemented code: 0x%x param: 0x%x\n", code, param_ptr);
        } else {
            void *mem_start = find_address_owner((void*) source_addr);
            unsigned int mem_tag = get_memarea_tag(mem_start);
            Task *current_task = get_current_task();
            unsigned int owner_id = current_task->owner_id;
            
            // memory protection
            if (owner_id != mem_tag) {
                IdentifiedAgent *troubled_agent = get_agent(owner_id);
                printf("Memory protection failure!!!\n");
                printf("Syscall %d was invoked by task %d,\nOwned by %s the %s %s (agent %d)\nBUT address 0x%x was tagged with agent %d!\n",
                    code,
                    current_task->task_id,
                    troubled_agent->human_name,
                    phisical_domain_class_name[troubled_agent->phys_race_attrib],
                    data_domain_class_name[troubled_agent->data_class_attrib],
                    owner_id,
                    source_addr,
                    mem_tag
                );
                
                unsigned int next_task = current_task->parent_task;
                printf("The task will be terminated, and control transferred to its parent %d\n", next_task);
                
                if (current_task->return_code_here != NULL)
                    *(current_task->return_code_here) = TASK_KILLED_BECAUSE_EXCEPTION;
                
                purge_task(current_task->task_id);
                
                iswitch_task(next_task);
            }
            
            //debug_printf("Handling syscall 0x%x (%s), param 0x%x, source 0x%x, owner=%d.\r\n", code, syscall_names[code], param_ptr, source_addr, mem_tag);
            handler((void*)param_ptr, mem_tag);
        }
    }
    bool continue_to_next = !try_schedule_next();
    if (continue_to_next) {
        //debug_printf("iret from syscall handler\r\n");
        ireturn_to_current_task();
    }
}
