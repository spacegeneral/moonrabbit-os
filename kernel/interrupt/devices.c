#include <interrupt/devices.h>
#include <interrupt/interface.h>
#include <interrupt/idt.h>
#include <interrupt/pic.h>
#include <interrupt/timing.h>

#include <task/task.h>
#include <task/sched.h>

#include <syscall.h>
#include <stdio.h>

#include <peripherals/keyboard.h>
#include <peripherals/floppy.h>
#include <peripherals/serial.h>
#include <peripherals/mouse.h>

InterruptSourceDevice *interr_pit;
InterruptSourceDevice *interr_ps2keyboard;
InterruptSourceDevice *interr_floppy;
InterruptSourceDevice *interr_com1;
InterruptSourceDevice *interr_ps2mouse;
InterruptSourceDevice *interr_isa_free_1;
InterruptSourceDevice *interr_isa_free_2;
InterruptSourceDevice *interr_isa_free_3;

InterruptSourceDevice interr_pit_desc;
InterruptSourceDevice interr_ps2keyboard_desc;
InterruptSourceDevice interr_floppy_desc;
InterruptSourceDevice interr_com1_desc;
InterruptSourceDevice interr_ps2mouse_desc;
InterruptSourceDevice interr_isa_free_1_desc;
InterruptSourceDevice interr_isa_free_2_desc;
InterruptSourceDevice interr_isa_free_3_desc;


isa_free_handler isa_free_callback[3];


interrupt_timer_callback cback_timer[MAX_INTERRUPT_TIMER_CALLBACKS];
int timer_callbacks_assigned = 0;


/* level 2 irq handlers */

void handler_pit(struct interrupt_frame* frame) \
    IRQHANDLER_DECORATOR(IRQ_PIT, {
    ++system_clock;
    ++resource_clock;
    
    if (resource_clock > RESOURCE_CLOCK_EPOCH) {
        reset_tasks_resources();
        resource_clock = 1;
    }
                         
    for (size_t i=0; i<MAX_INTERRUPT_TIMER_CALLBACKS; i++) {
        interrupt_timer_callback cback = cback_timer[i];
        if (cback == NULL) break;
        cback(system_clock);
    }
    
    Task *current_task = get_current_task();
    if (current_task != NULL) {
        current_task->time_counter[current_time_selector]++;
        if (frame->cs != KERNEL_CODE_SELECTOR) {
            if (current_task->preempt.enabled && (current_task->preempt.stage == PREEMPTION_OFF)) {
                (*saved_registers_location) = (*saved_registers_timer_location);
                saved_registers_location->eip = frame->eip;
                saved_registers_location->esp = frame->esp;
                saved_registers_location->eflags = frame->eflags;
                current_task->preempt.time_accumulator++;
                if (try_acquire_continuation(current_task)) {
                    //debug_printf("(%u preempted %llu > %llu!)\r\n", current_task->task_id, current_task->preempt.time_accumulator, current_task->preempt.timeout);
                    pic_send_eoi(IRQ_PIT);
                    try_schedule_next();
                }
            }
        }
    }
})

void handler_ps2keyboard(struct interrupt_frame* frame) \
    IRQHANDLER_DECORATOR(IRQ_KBD, {
    (void)frame; // unused
    publish_new_ps2_scancode();
})
    
void handler_floppy(struct interrupt_frame* frame) \
    IRQHANDLER_DECORATOR(IRQ_FLOPPY, {
    (void)frame; // unused
    catch_floppy_irq();
})

void handler_com1(struct interrupt_frame* frame) \
    IRQHANDLER_DECORATOR(IRQ_COM1, {
    (void)frame; // unused
    read_serial_com1();
})

void handler_ps2mouse(struct interrupt_frame* frame) \
    IRQHANDLER_DECORATOR(IRQ_MOUSE, {
    (void)frame; // unused
    catch_ps2_mouse_streaming_byte();  // this does sound a bit weird in retrospect...
})
    
void handler_isa_free_1(struct interrupt_frame* frame) \
    IRQHANDLER_DECORATOR(IRQ_ISA_FREE_1, {
    if (isa_free_callback[0] != NULL) isa_free_callback[0](frame, IRQ_ISA_FREE_1);
})
    
void handler_isa_free_2(struct interrupt_frame* frame) \
    IRQHANDLER_DECORATOR(IRQ_ISA_FREE_2, {
    if (isa_free_callback[1] != NULL) isa_free_callback[1](frame, IRQ_ISA_FREE_2);
})
    
void handler_isa_free_3(struct interrupt_frame* frame) \
    IRQHANDLER_DECORATOR(IRQ_ISA_FREE_3, {
    if (isa_free_callback[2] != NULL) isa_free_callback[2](frame, IRQ_ISA_FREE_3);
})



bool register_interrupt_timer_callback(interrupt_timer_callback fun) {
    if (timer_callbacks_assigned >= MAX_INTERRUPT_TIMER_CALLBACKS) return false;
    
    cback_timer[timer_callbacks_assigned++] = fun;
    return true;
}

int locate_interrupt_timer_callback_idnex(interrupt_timer_callback fun) {
    for (int i=0; i<MAX_INTERRUPT_TIMER_CALLBACKS; i++) {
        if (cback_timer[i] == fun) return i;
    }
    return -1;
}

bool unregister_interrupt_timer_callback(interrupt_timer_callback fun) {
    int index = locate_interrupt_timer_callback_idnex(fun);
    if (index == -1) return false;
    if ((index + 1) == timer_callbacks_assigned) {
        cback_timer[index] = NULL;
        timer_callbacks_assigned--;
    } else {
        for (int s=index; s<timer_callbacks_assigned-1; s++) {
            cback_timer[s] = cback_timer[s+1];
        }
        cback_timer[timer_callbacks_assigned-1] = NULL;
        timer_callbacks_assigned--;
    }
    return true;
}



#define SETUP_IRQ_DEVICE(devname, picirqline, pichandler) \
{ \
    interr_##devname##_desc.pic_irq_number = picirqline; \
    interr_##devname##_desc.pic_handler = pichandler; \
    interr_##devname##_desc.is_enabled = false; \
    interr_##devname = &interr_##devname##_desc; \
}
    
    

void setup_irq_device_definitions() {
    for (int i=0; i<MAX_INTERRUPT_TIMER_CALLBACKS; i++) {
        cback_timer[i] = NULL;
    }
    
    isa_free_callback[0] = NULL;
    isa_free_callback[1] = NULL;
    isa_free_callback[2] = NULL;
    
    SETUP_IRQ_DEVICE(pit,         IRQ_PIT,    &lvl1_handler_pit);
    SETUP_IRQ_DEVICE(ps2keyboard, IRQ_KBD,    &lvl1_handler_ps2keyboard);
    SETUP_IRQ_DEVICE(floppy,      IRQ_FLOPPY, &lvl1_handler_floppy);
    SETUP_IRQ_DEVICE(com1,        IRQ_COM1,   &lvl1_handler_com1);
    SETUP_IRQ_DEVICE(ps2mouse,    IRQ_MOUSE,  &lvl1_handler_ps2mouse);
    
    SETUP_IRQ_DEVICE(isa_free_1,  IRQ_ISA_FREE_1,  &lvl1_handler_isa_free_1);
    SETUP_IRQ_DEVICE(isa_free_2,  IRQ_ISA_FREE_2,  &lvl1_handler_isa_free_2);
    SETUP_IRQ_DEVICE(isa_free_3,  IRQ_ISA_FREE_3,  &lvl1_handler_isa_free_3);
}


void device_enable_interrupt(InterruptSourceDevice *device) {
#ifdef USING_LEGACY_PIC
    if (!device->is_enabled) {
        set_interrupt_handler(IRQ(device->pic_irq_number), device->pic_handler);
        IRQ_clear_mask(device->pic_irq_number);
    }
#endif
}

void device_disable_interrupt(InterruptSourceDevice *device) {
#ifdef USING_LEGACY_PIC
    if (device->is_enabled) {
        IRQ_set_mask(device->pic_irq_number);
        disable_interrupt_handler(IRQ(device->pic_irq_number));
    }
#endif
}


InterruptSourceDevice *get_isa_free_device(int irq_num) {
    switch (irq_num) {
        case IRQ_ISA_FREE_1: return interr_isa_free_1;
        case IRQ_ISA_FREE_2: return interr_isa_free_2;
        case IRQ_ISA_FREE_3: return interr_isa_free_3;
    }
    return NULL;
}

void attach_isa_free_device_callback(int irq_num, isa_free_handler callback) {
    switch (irq_num) {
        case IRQ_ISA_FREE_1: 
            isa_free_callback[0] = callback;
            break;
        case IRQ_ISA_FREE_2: 
            isa_free_callback[1] = callback;
            break;
        case IRQ_ISA_FREE_3: 
            isa_free_callback[2] = callback;
            break;
    }
}
void clear_isa_free_device_callback(int irq_num) {
    switch (irq_num) {
        case IRQ_ISA_FREE_1: 
            isa_free_callback[0] = NULL;
            break;
        case IRQ_ISA_FREE_2: 
            isa_free_callback[1] = NULL;
            break;
        case IRQ_ISA_FREE_3: 
            isa_free_callback[2] = NULL;
            break;
    }
}
isa_free_handler get_isa_free_device_callback(int irq_num) {
    switch (irq_num) {
        case IRQ_ISA_FREE_1: 
            return isa_free_callback[0];
            break;
        case IRQ_ISA_FREE_2: 
            return isa_free_callback[1];
            break;
        case IRQ_ISA_FREE_3: 
            return isa_free_callback[2];
            break;
    }
    return NULL;
}
