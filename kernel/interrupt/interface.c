#include <interrupt/interface.h>
#include <interrupt/timing.h>
#include <interrupt/idt.h>
#include <interrupt/pic.h>
#include <interrupt/devices.h>
#include <stdio.h>
#include <kernel/kidou.h>


int interrupt_readylevel_internal = 0;

static int interrupt_ready_level() {
    return interrupt_readylevel_internal;
}

static void interrupt_setup() {
#ifdef VERBOSE_STARTUP
    printf("Initializing interrupt subsystem.\n    Setting up device definitions... ");
#endif
    setup_irq_device_definitions();
#ifdef VERBOSE_STARTUP
    printf(" done!\n    Initializing basic IDT... ");
#endif
    setup_basic_interrupt_and_exception_handlers();
#ifdef USING_LEGACY_PIC
    #ifdef VERBOSE_STARTUP
    printf(" done!\n    Initializing PIC... ");
    #endif
    setup_pic();
#endif
#ifdef VERBOSE_STARTUP
    printf(" done!\n    Setting up timer using ");
#endif
    setup_timer();
#ifdef VERBOSE_STARTUP
    printf(" done!\nInterrupt subsystem ready (but interrupts are still disabled).\n");
#endif
    interrupt_readylevel_internal = 100;
}

static void interrupt_teardown() {
    interrupt_readylevel_internal = 0;
}


UNIQUE_IMPLEMENTATION_OF(interrupt, subsystem, { \
    .ready_level = &interrupt_ready_level, \
    .setup = &interrupt_setup, \
    .teardown = &interrupt_teardown \
})



static bool interrupt_enable_device(InterruptSourceDevice *device) {
    device_enable_interrupt(device);
    return true;
}

static bool interrupt_disable_device(InterruptSourceDevice *device) {
    device_disable_interrupt(device);
    return true;
}



UNIQUE_IMPLEMENTATION_OF(interrupt, interr_onoff, { \
    .enable_device = &interrupt_enable_device, \
    .disable_device = &interrupt_disable_device \
})
