#include <stdbool.h>
#include <interrupt/pit.h>
#include <interrupt/interface.h>
#include <interrupt/timing.h>
#include <asmutils.h>
#include <stdio.h>
#include <kernel/kidou.h>


void setup_timer() {
#ifdef USING_LEGACY_PIT
    set_pit_frequency(INTERRUPT_FREQUENCY);
    CALL_IMPL(interrupt, interr_onoff, enable_device, interr_pit);
    #ifdef VERBOSE_STARTUP
        printf("PIT");
    #endif
#endif
    system_clock = 0;
}


void wait_milliseconds(unsigned long long int millisec) {
    unsigned long long int millisec_per_tick = 1000 / INTERRUPT_FREQUENCY;
    
    unsigned long long int started = system_clock;
    while (true) {
        halt();
        unsigned long long int millisec_passed = (system_clock - started) * millisec_per_tick;
        if (millisec_passed >= millisec) {
            break;
        }
    }
}
