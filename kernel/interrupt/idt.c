#include <stdio.h>
#include <stdint.h>
#include <interrupt/idt.h>
#include <interrupt/exception.h>
#include <kernel/core.h>
#include <syscall.h>


    
/* exception handlers */

#define SETUP_EXCEPTION_HANDLER(id) \
set_interrupt_handler(EXC(id), &handler_exception_##id);



void setup_basic_interrupt_and_exception_handlers() {
    
    FORALL_EXCEPTIONS(SETUP_EXCEPTION_HANDLER)
    
    set_syscall_handler(EXC(SYSCALL_INT), &handler_syscall);
    
}
