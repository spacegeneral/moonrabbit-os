#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/sec.h>
#include <sys/proc.h>
#include <kernel/core.h>
#include <builtins.h>
#include <arch/setup.h>
#include <interrupt/exception.h>
#include <security/identify.h>
#include <mm/mm.h>
#include <mm/paging.h>
#include <task/task.h>
#include <debug.h>
#include <rice.h>


#define HAS_ERRORCODE(code) \
    (code == 8 || (code >= 10 && code <= 14) || code == 17 || code == 30)
    
    
    
char *get_exception_name(int code) {
    switch (code) {
        case 0: return "divide by zero";
        case 1: return "debug";
        case 2: return "non-maskable interrupt";
        case 3: return "breakpoint";
        case 4: return "overflow";
        case 5: return "bound range exceeded";
        case 6: return "invalid opcode";
        case 7: return "device not available";
        case 8: return "double fault";
        case 9: return "coprocessor segment overrun (?)";
        case 10: return "invalid TSS";
        case 11: return "segment not present";
        case 12: return "stack-segment fault";
        case 13: return "general protection fault";
        case 14: return "page fault";
        case 16: return "x87 floating-point exception";
        case 17: return "alignment check";
        case 18: return "machine check";
        case 19: return "SIMD floating-point exception";
        case 20: return "virtualization exception";
        case 30: return "security exception";
    }
    return "unknown\n";
}


void debug_page_fault_exception(IdentifiedAgent* troubled_agent) {
    ucpuint_t fault_address;
    asm volatile ( "mov %0, cr2"
                    : "=a" (fault_address)
                    );
    ucpuint_t fault_page = (((ucpuint_t)fault_address) / PAGE_SIZE);
    if (is_page_guard((void*)(fault_page * PAGE_SIZE), troubled_agent)) {
        printf("Guard page hit at virtual address %p in ", fault_address);
        debug_printf("Guard page hit at virtual address %p in ", fault_address);
    } else {
        printf("Exception occurred at virtual address %p in ", fault_address);
        debug_printf("Exception occurred at virtual address %p in ", fault_address);
    }
    if (print_debug_memarea_info((uintptr_t)fault_address)) {
        printf(".\n");
        debug_printf(".\r\n");
    } else {
        printf("unknwon region.\n");
        debug_printf("unknwon region.\r\n");
    }
}


void userland_exception_handler(int code, unsigned error_code, addr_t ip, addr_t sp, ucpuint_t flags) {
    (void)error_code;
    Task *troubled_task = get_current_task();
    change_saved_ip(saved_registers_location, ip);
    change_saved_sp(saved_registers_location, sp);
    change_saved_flags(saved_registers_location, flags);
    
    store_saved_registers_into_task(troubled_task);
    IdentifiedAgent *troubled_agent = get_agent(troubled_task->owner_id);
    
    if (code == 14) debug_page_fault_exception(troubled_agent);
    
    printf("The error occurred while running task ");
    setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED); printf("%s", troubled_task->name); setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
    printf(" (%d), owned by %s the %s %s\n",
        troubled_task->task_id,
        troubled_agent->human_name,
        phisical_domain_class_name[troubled_agent->phys_race_attrib],
        data_domain_class_name[troubled_agent->data_class_attrib]
    );
    debug_printf("The error occurred while running task %s (%d), owned by %s the %s %s\r\n",
        troubled_task->name,
        troubled_task->task_id,
        troubled_agent->human_name,
        phisical_domain_class_name[troubled_agent->phys_race_attrib],
        data_domain_class_name[troubled_agent->data_class_attrib]
    );
    
    print_exception_debug_user(troubled_task, ip);
    
    printf("Register content:\n");
    debug_printf("Register content:\r\n");
    print_regs(&(troubled_task->regs));
    
    addr_t memarea = (addr_t)find_address_owner((void*)ip);
    unsigned int memtag = get_memarea_tag((void*)memarea);
    
    if (memtag == 0) {
        printf("The error occurred inside a kernel-owned memory area.\n");
        debug_printf("The error occurred inside a kernel-owned memory area.\r\n");
    } else if (memarea < kernel_heap_start) {
        printf("The error occurred inside non allocable kernel area %p in [%p - %p).\n", memarea, 0, kernel_heap_start);
        debug_printf("The error occurred inside non allocable kernel area %p in [%p - %p).\r\n", memarea, 0, kernel_heap_start);
    } else if (memarea >= (kernel_heap_start + kernel_heap_length)) {
        printf("The error occurred int the area outside allocable address space [%p - %p].\n", kernel_heap_start + kernel_heap_length, (unsigned)(-1));
        debug_printf("The error occurred in the area outside allocable address space [%p - %p].\r\n", kernel_heap_start + kernel_heap_length, (unsigned)(-1));
    } else {
        if (troubled_task->owner_id != memtag) {
            setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED); printf("Memory protection failure!!!\n"); setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
            printf("The active task is owned by agent %d, but address %p was tagged with agent %d!\n",
                troubled_task->owner_id,
                ip,
                memtag
            );
            debug_printf("Memory protection failure!!!\r\n");
            debug_printf("The active task is owned by agent %d, but address %p was tagged with agent %d!\r\n",
                troubled_task->owner_id,
                ip,
                memtag
            );
        } 
    }
    
    unsigned int next_task = troubled_task->parent_task;
    char next_task_name[MAX_TASKNAME_LENGTH];
    snprintf(next_task_name, MAX_TASKNAME_LENGTH, "unknown");
    if (task_exists(next_task)) {
        snprintf(next_task_name, MAX_TASKNAME_LENGTH, "%s", get_task(next_task)->name);
    }
    printf("The task will be terminated, and control transferred to its parent %s (%d)\n", next_task_name, next_task);
    debug_printf("The task will be terminated, and control transferred to its parent %s (%d)\r\n", next_task_name, next_task);
    
    if (troubled_task->return_code_here != NULL)
        *(troubled_task->return_code_here) = TASK_KILLED_BECAUSE_EXCEPTION;
    
    purge_task(troubled_task->task_id);
    
    if (!task_exists(next_task)) {
        printf("  .... which is an invalid task.\n");
        debug_printf("  .... which is an invalid task.\r\n");
        panic();
    } else {
        iswitch_task(next_task);
    }
}


void kerneland_exception_handler(int code, addr_t ip) {
    if (code == 14) debug_page_fault_exception(get_agent(0));
    
    printf("The error occurred while running ");
    setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED); printf("kernel"); setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
    printf(" code.\n");
    debug_printf("The error occurred while running kernel code.\r\n");
    
    addr_t memarea = (addr_t)find_address_owner((void*)ip);
    unsigned int memtag = get_memarea_tag((void*)memarea);
    
    if (memtag == 0) {
        printf("The error occurred inside a kernel-owned memory area.\n");
        debug_printf("The error occurred inside a kernel-owned memory area.\r\n");
    } else if (memarea < kernel_heap_start) {
        printf("The error occurred inside non allocable kernel area [%p - %p).\n", 0, kernel_heap_start);
        debug_printf("The error occurred inside non allocable kernel area [%p - %p).\r\n", 0, kernel_heap_start);
    } else if (memarea >= (kernel_heap_start + kernel_heap_length)) {
        printf("The error occurred int the area outside allocable address space [%p - %p].\n", kernel_heap_start + kernel_heap_length, (unsigned)(-1));
        debug_printf("The error occurred in the area outside allocable address space [%p - %p].\r\n", kernel_heap_start + kernel_heap_length, (unsigned)(-1));
    } else {
        if (0 != memtag) {
            printf("Memory protection failure!!!\n");
            printf("The kernel is owned by agent %d, but address %p was tagged with agent %d!\n",
                0,
                ip,
                memtag
            );
            debug_printf("Memory protection failure!!!\r\n");
            debug_printf("The kernel is owned by agent %d, but address %p was tagged with agent %d!\r\n",
                0,
                ip,
                memtag
            );
        } 
    }
    
    printf("Kernel errors are not recoverable.\n");
    debug_printf("Kernel errors are not recoverable.\r\n");
    panic();
}


void contextual_exception_handler(int code, void* frame) {
    addr_t ip;
    addr_t sp;
    ucpuint_t flags;
    ucpuint_t cs;
    unsigned error_code = 0;
    printf("Caught error: ");
    setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED); printf("%s", get_exception_name(code)); setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
    printf(" (code: %d)!\n", code);
    debug_printf("Caught error: %s (code: %d)!\r\n", get_exception_name(code), code);
    if (HAS_ERRORCODE(code)) {
        struct exception_frame* eframe = (struct exception_frame*) frame;
        ip = eframe->eip;
        cs = eframe->cs;
        sp = eframe->esp;
        flags = eframe->eflags;
        error_code = eframe->error_code;
        printf("Eflags: 0x%x  CS: 0x%x  EIP: %p  Error code: 0x%x\n", eframe->eflags, eframe->cs, eframe->eip, eframe->error_code);
        debug_printf("Eflags: 0x%x  CS: 0x%x  EIP: %p  Error code: 0x%x\r\n", eframe->eflags, eframe->cs, eframe->eip, eframe->error_code);
    } else {
        struct interrupt_frame* iframe = (struct interrupt_frame*) frame;
        ip = iframe->eip;
        cs = iframe->cs;
        sp = iframe->esp;
        flags = iframe->eflags;
        printf("Eflags: 0x%x  CS: 0x%x  EIP: %p\n", iframe->eflags, iframe->cs, iframe->eip);
        debug_printf("Eflags: 0x%x  CS: 0x%x  EIP: %p\r\n", iframe->eflags, iframe->cs, iframe->eip);
    }
    
    if (cs == USER_CODE_SEGMENT) userland_exception_handler(code, error_code, ip, sp, flags);
    else kerneland_exception_handler(code, ip);
}
