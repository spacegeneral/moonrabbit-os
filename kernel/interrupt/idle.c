#include <interrupt/timing.h>


void idle() {
    while (1) {
        asm volatile (
            "hlt"
        );
    }
}

void halt() {
    asm volatile ("hlt");
}

