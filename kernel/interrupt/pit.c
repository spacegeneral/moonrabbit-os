#include <interrupt/pit.h>
#include <interrupt/timing.h>
#include <asmutils.h>


void set_pit_frequency(uint16_t hertz) {
    uint16_t divisor = PIT_MAGIC_DIVISOR / hertz;
    outb(PIT_COMMAND, 0x36);
    outb(PIT_CHANNEL_0, (divisor & 0xFF));
    outb(PIT_CHANNEL_0, (divisor >> 8));
}


void set_pit_beep_frequency(uint16_t hertz) {
    uint16_t divisor = PIT_MAGIC_DIVISOR / hertz;
    outb(PIT_COMMAND, 0xb6);
    outb(PIT_CHANNEL_2, (divisor & 0xFF));
    outb(PIT_CHANNEL_2, (divisor >> 8));
}

void start_pit_beep() {
    uint8_t tmp = inb(PIT_SPEAKER);
    if (tmp != (tmp | 3)) {
        outb(PIT_SPEAKER, tmp | 3);
    }
}

void stop_pit_beep() {
    uint8_t tmp = inb(PIT_SPEAKER) & 0xFC;
    outb(PIT_SPEAKER, tmp);
}
