#include <acpi/acpi.h>
#include <acpi/power.h>
#include <kernel/log.h>
#include <stdlib.h>
#include <asmutils.h>

#include <ipc/interface.h>
#include <sys/default_topics.h>
#include <security/defaults.h>


uint16_t SLP_TYPa;
uint16_t SLP_TYPb;

bool acpi_power_supported = false;


// The quick and dity way (no AML interpreter)
// from: https://forum.osdev.org/viewtopic.php?t=16990
//
// bytecode of the \_S5 object
// -----------------------------------------
//        | (optional) |    |    |    |   
// NameOP | \          | _  | S  | 5  | _
// 08     | 5A         | 5F | 53 | 35 | 5F
//
// -----------------------------------------------------------------------------------------------------------
//           |           |              | ( SLP_TYPa   ) | ( SLP_TYPb   ) | ( Reserved   ) | (Reserved    )
// PackageOP | PkgLength | NumElements  | byteprefix Num | byteprefix Num | byteprefix Num | byteprefix Num
// 12        | 0A        | 04           | 0A         05  | 0A          05 | 0A         05  | 0A         05
//
//----this-structure-was-also-seen----------------------
// PackageOP | PkgLength | NumElements |
// 12        | 06        | 04          | 00 00 00 00
//
// (Pkglength bit 6-7 encode additional PkgLength bytes [shouldn't be the case here])
//


static void handle_acpi_poweroff_message(const void *message) {
    (void)message; // unused
    acpi_power_off_system();
}


void setup_acpi_power_topics() {
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_ACPI_POWEROFF_TOPIC, 1, sizeof(int), DEFAULT_SEC_SHUTDOWN_TOPICS_RACE, DEFAULT_SEC_SHUTDOWN_TOPICS_CLASS, DEFAULT_SEC_SHUTDOWN_TOPICS_POLICY);
    
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_ACPI_POWEROFF_TOPIC, &handle_acpi_poweroff_message);
}


void init_acpi_power() {
    acpi_power_supported = false;
    FADT *fadt = get_sdt("FACP");
    if (fadt == NULL) {
        klog("Unable to locate FADT!\n");
        return;
    }
    
    char *S5Addr = (char *) fadt->dsdt +36; // skip header
    int dsdtLength = *(fadt->dsdt+1) - 36;
    
    while (0 < dsdtLength--) {
        if (memcmp(S5Addr, "_S5_", 4) == 0) {
            break;
        }
        S5Addr++;
    }
    // check if \_S5 was found
    if (dsdtLength > 0) {
        // check for valid AML structure
        if ((*(S5Addr-1) == 0x08 || (*(S5Addr-2) == 0x08 && *(S5Addr-1) == '\\')) && *(S5Addr+4) == 0x12) {
            S5Addr += 5;
            S5Addr += ((*S5Addr &0xC0)>>6) +2;   // calculate PkgLength size

            if (*S5Addr == 0x0A)
                S5Addr++;   // skip byteprefix
            SLP_TYPa = *(S5Addr)<<10;
            S5Addr++;

            if (*S5Addr == 0x0A)
                S5Addr++;   // skip byteprefix
            SLP_TYPb = *(S5Addr)<<10;
            
            acpi_power_supported = true;
            setup_acpi_power_topics();
            klog("ACPI supports power management\n");
        } else {
            klog("\\_S5 parse error.\n");
        }
    } else {
        klog("\\_S5 not present.\n");
    }
}


bool is_acpi_power_supported() {
    return acpi_power_supported;
}


void acpi_power_off_system() {
    if (!is_acpi_power_supported()) {
        klog("Power management via ACPI is not supported!\n");
        return;
    }
    bool enable_success = enable_acpi();
    if (!enable_success) return;
    
    FADT *fadt = get_sdt("FACP");
    if (fadt == NULL) {
        klog("Unable to locate FADT!\n");
        return;
    }
    
    uint16_t SLP_EN = 1<<13;
    
    // send the shutdown command
    outw((uint16_t) fadt->PM1aControlBlock, SLP_TYPa | SLP_EN);
    if (fadt->PM1bControlBlock != 0) {
        outw((uint16_t) fadt->PM1bControlBlock, SLP_TYPb | SLP_EN);
    }
}





