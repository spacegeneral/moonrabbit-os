#include <acpi/acpi.h>
#include <kernel/log.h>
#include <assert.h>


// RSDT

ACPISDTHeader *get_rsdt_header(RSDT table) {
    return (ACPISDTHeader*) table;
}

size_t get_rsdt_num_entries(RSDT table) {
    ACPISDTHeader *header = get_rsdt_header(table);
    return (header->length - sizeof(ACPISDTHeader)) / 4;
}

void *get_rsdt_entry(RSDT table, size_t entry_id) {
    size_t numentries = get_rsdt_num_entries(table);
    assert(entry_id < numentries);
    
    uint8_t *table_base = (uint8_t *) table;
    table_base += sizeof(ACPISDTHeader);
    uint32_t *entries = (uint32_t*)table_base;
    return (void*) entries[entry_id];
}


// other tables

void *get_sdt_acpi_10(char *signature) {
    RSDT root = (RSDT) rsdp->first_part.rsdt_address;
    size_t numentries = get_rsdt_num_entries(root);
    for (size_t e=0; e<numentries; e++) {
        ACPISDTHeader *entry = get_rsdt_entry(root, e);
        if (entry == NULL) continue;
        if (memcmp(entry->signature, signature, ACPISDT_SIGNATURE_LENGTH) == 0) {
            return (void*) entry;
        }
    }
    return NULL;
}

void *get_sdt(char *signature) {
    int revision = rsdp->first_part.revision;
    if (revision == 0) {
        return get_sdt_acpi_10(signature);
    } else {
        assert(false);  // TODO implement acpi 2.0 support
    }
    return NULL;
}
