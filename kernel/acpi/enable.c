#include <acpi/acpi.h>
#include <time.h>
#include <kernel/log.h>
#include <asmutils.h>
#include <stdlib.h>


bool is_acpi_enabled() {
    if (!is_acpi_supported()) return false;
    
    FADT *fadt = get_sdt("FACP");
    if (fadt == NULL) {
        klog("Unable to locate FADT!\n");
        return false;
    }
    
    return (inw((uint16_t)fadt->PM1aControlBlock) & 1) != 0;
}


bool enable_acpi() {
    if (is_acpi_enabled()) return true;
    
    FADT *fadt = get_sdt("FACP");
    if (fadt == NULL) {
        klog("Unable to locate FADT!\n");
        return false;
    }
    
    if (fadt->SMI_CommandPort == 0 || fadt->AcpiEnable == 0) {
        klog("ACPI cannot be enabled.\n");
        return false;
    }
    
    outb((uint16_t) fadt->SMI_CommandPort, fadt->AcpiEnable); // send acpi enable command
    // give 3 seconds time to enable acpi
    int i=0;
    for (; i<300; i++) {
        if ((inw((uint16_t) fadt->PM1aControlBlock) & 1) == 1) {
            break;
        }
        k_sleep_milli(10);
    }
    if (fadt->PM1bControlBlock != 0) {
        for (; i<300; i++ ) {
            if ( (inw((unsigned int) fadt->PM1bControlBlock) & 1) == 1 ) {
                break;
            }
            k_sleep_milli(10);
        }
    }
        
    if (i<300) {
        klog("Enabled acpi.\n");
        return true;
    }
    
    klog("Unable to enable acpi.\n");
    return false;
}
