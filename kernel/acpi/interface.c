#include <acpi/interface.h>
#include <builtins.h>
#include <acpi/acpi.h>


int acpi_readylevel_internal = 0;

static int acpi_ready_level() {
    return acpi_readylevel_internal;
}

static void acpi_setup() {
    initialize_acpi();
    detect_vm();  // now is a good time to attempt this
}

static void acpi_teardown() {
    acpi_readylevel_internal = 0;
}


UNIQUE_IMPLEMENTATION_OF(acpi, subsystem, { \
    .ready_level = &acpi_ready_level, \
    .setup = &acpi_setup, \
    .teardown = &acpi_teardown \
})
