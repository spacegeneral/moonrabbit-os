#include <acpi/interface.h>
#include <acpi/power.h>
#include <mm/bios_data_areas.h>
#include <mm/mm.h>
#include <kernel/log.h>
#include <peripherals/rtc.h>
#include <peripherals/ps2.h>
#include <interfaces.h>
#include <acpi/acpi.h>
#include <stdio.h>
#include <kernel/kidou.h>
#include <sys/debuginfo.h>


RSDPDescriptor *rsdp;



void klog_rsdp_info() {
    char oemid_terminated[7];
    memcpy(oemid_terminated, rsdp->first_part.oemid, 6);
    oemid_terminated[6] = '\0';
    klog("ACPI version %s by %s\n", 
         (rsdp->first_part.revision == 0 ? "1.0" : ">=2.0"), 
         oemid_terminated);
    if (rsdp->first_part.revision == 0) {
        klog("RSDT located at address 0x%08x\n", (uint32_t)rsdp->first_part.rsdt_address);
        int numentries = get_rsdt_num_entries((RSDT)rsdp->first_part.rsdt_address);
        klog("RSDT has %d entries.\n", numentries);
    }
}

void locate_rsdp() {
    static char signature[] = "RSD PTR ";
    
    // search ebda
    klog("ACPI: searching for RSDP in EBDA, range 0x%08x - 0x%08x\n", (uint32_t) extended_bios_data_area, ((uint32_t) extended_bios_data_area) + EBDA_LENGTH - 1);
    uint8_t *ebda_rsdp = search_mem_region((uint32_t) extended_bios_data_area, 
                                           ((uint32_t) extended_bios_data_area) + EBDA_LENGTH, // search the first KB
                                           (uint8_t*)signature, 8);
    if (ebda_rsdp != NULL) {
        rsdp = (RSDPDescriptor*) ebda_rsdp;
        klog("RSDP located in EBDA at address 0x%08x\n", (uint32_t)ebda_rsdp);
        return;
    }
}



bool is_acpi_supported() {
    return rsdp != NULL;
}


void acpi_get_century_register() {
    FADT *fadt = get_sdt("FACP");
    if (fadt == NULL) {
        klog("Unable to locate FADT!\n");
        return;
    }
    uint8_t century_reg = fadt->Century;
    set_century_register(century_reg);
    klog("Detected century register: 0x%02x\n", century_reg);
    #ifdef VERBOSE_STARTUP
        printf("(0x%02x) ", century_reg);
    #endif
}


void acpi_detect_ps2_controller_present() {
    FADT *fadt = get_sdt("FACP");
    if (fadt == NULL) {
        klog("Unable to locate FADT!\n");
        return;
    }
    uint16_t flags = fadt->BootArchitectureFlags;
    bool present = (flags & 0b10) != 0;
    ps2_controller_present = present;
    #ifdef VERBOSE_STARTUP
        printf("(%s) ", (present ? "present" : "missing"));
    #endif
}


void acpi_internal_getsections(Array *append_sections_here) {
    if (is_acpi_supported()) {
        ACPISDTHeader* rsdt_header = get_rsdt_header((RSDT)(rsdp->first_part.rsdt_address));
        TaskSection_append_new(append_sections_here, "ACPI RSDT", (uintptr_t)rsdt_header, (size_t)(rsdt_header->length));
        FADT *fadt = get_sdt("FACP");
        if (fadt != NULL) TaskSection_append_new(append_sections_here, "ACPI FADT", (uintptr_t)fadt, (size_t)(fadt->header.length));
    }
}


void initialize_acpi() {
    rsdp = NULL;
    
#ifdef VERBOSE_STARTUP
    printf("Initializing ACPI subsystem.\n    Locating RDSP... ");
#endif
    locate_rsdp();
    if (is_acpi_supported()) {
        acpi_readylevel_internal = ACPI_READYLEVEL_BASIC;
        klog_rsdp_info();
        
        #ifdef VERBOSE_STARTUP
            printf("done!\n    Fetching century register... ");
        #endif
        acpi_get_century_register();
        
        if (rsdp->first_part.revision > 0) {
            #ifdef VERBOSE_STARTUP
                printf("done!\n    Detecting PS2 controller... ");
            #endif
            acpi_detect_ps2_controller_present();
        }
        
        #ifdef VERBOSE_STARTUP
            printf("done!\n    Initializing ACPI power interface... ");
        #endif
        init_acpi_power();
        if (is_acpi_power_supported()) {
            acpi_readylevel_internal = ACPI_READYLEVEL_POWER;
            #ifdef VERBOSE_STARTUP
                printf("done!\n    Enabling ACPI... ");
            #endif
        } else {
            #ifdef VERBOSE_STARTUP
                printf("FAIL!\n    Enabling ACPI... ");
            #endif
        }
        
        if (enable_acpi()) {
            acpi_readylevel_internal = ACPI_READYLEVEL_FULL;
            #ifdef VERBOSE_STARTUP
                printf("done!\nACPI subsystem ready.\n");
            #endif
        } else {
            #ifdef VERBOSE_STARTUP
                printf("FAIL!\nACPI subsystem NOT ready!\n");
            #endif
        }
    } else {
        #ifdef VERBOSE_STARTUP
            printf("FAIL!\nACPI subsystem NOT ready!\n");
        #endif
    }
}
