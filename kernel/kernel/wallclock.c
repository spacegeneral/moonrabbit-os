#include <kernel/wallclock.h>
#include <peripherals/rtc.h>
#include <stdio.h>


bool get_system_time(time_t *time) {
    struct tm current_time;
    read_rtc(&current_time);
    if (time != NULL)
        (*time) = mktime(&current_time);
    return true;
}

bool set_system_time(time_t *time) {
    (void)time; // TODO use this
    return false; // TODO implement this
}
