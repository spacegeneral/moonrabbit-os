#include <stdio.h>
#include <kernel/core.h>
#include <interrupt/timing.h>


void panic() {
    printf("Panic!\n");
    idle();
}


void kernel_ubsan_panic(const char* violation, const char* filename, uint32_t line, uint32_t column) {
    printf("Kernel misbehavior: %s\nIn file %s, line:col %u:%u\n", violation, filename, line, column);
    panic();
} 
