#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include <sys/proc.h>

#include <kernel/tty.h>
#include <kernel/log.h>
#include <kernel/rice.h>
#include <kernel/core.h>
#include <kernel/init.h>
#include <kernel/bitmap_font.h>
#include <builtins.h>
#include <cpu/cpuid.h>

#include <arch/interface.h>
#include <arch/setup.h>
#include <mm/interface.h>
#include <interrupt/interface.h>
#include <interrupt/timing.h>
#include <peripherals/interface.h>
#include <data/interface.h>
#include <data/path.h>
#include <security/interface.h>
#include <task/interface.h>
#include <ipc/interface.h>
#include <acpi/interface.h>
#include <audio/interface.h>

#include <localize/messages.h>


volatile unsigned long long int system_clock = 0;

char **environ;  // not actually used inside the kernel
int TTY_CURSOR;
EMPTY_IMPLEMENTATION(video, terminal)


void motd() {
    char cpuname[13];
    cpuid_vendor_string(cpuname);
    printf("\n        .++++++.\n\
     .++++ +++++++.\n\
    ++++ ++ ++++++++     %s %s, %s build\n\
   ++++++ ++ ++++++++    Version: %s\n\
  ++++++++  0 *+++++++   Built: %s\n\
  ++++++++.  .W+++++++   CPU: %s\n\
  ++++++++    ++++++++   \n\
   ++++++    +. +++++\n\
    +++.    ++++++++\n\
     '+++++..  +++'\n\
        '++++++'\n", "Moonrabbit OS", HOSTARCH, BUILD_TYPE, BUILD_VERSION, BUILD_DATE, cpuname);
}


#ifdef BEEP_WELCOME
static void do_beep_welcome() {
    beep(293, 300);  // plays the tune from close encounters of the 3rd kind
    beep(329, 300);
    beep(261, 300);
    wait_milliseconds(100);
    beep(130, 300);
    beep(196, 400);
}
#endif


void initialization(ucpuint_t mboot_magic, ucpuint_t mboot_header) {
    multiboot_info_t* mboot_info = (multiboot_info_t*)mboot_header;
    TTY_CURSOR = TTY_CURSOR_ASCII;
    uint64_t framebuffer_addr_long = mboot_info->framebuffer_addr;
    if (framebuffer_addr_long > arch_uword_max) {
        panic();
    }
    if (mboot_info->framebuffer_type == 2) {  // EGA-standard text mode
        CLONE_IMPLEMENTATION(video, vga, terminal);
    } else {
        CLONE_IMPLEMENTATION(video, vbe, terminal);
    }
    addr_t framebuffer_addr = arch_u64_truncate_to_addr(framebuffer_addr_long);
    CALL_IMPL(video,        terminal,               initialize,  // setup basic terminal printing
                                                        (void*)framebuffer_addr, 
                                                        mboot_info->framebuffer_width, 
                                                        mboot_info->framebuffer_height, 
                                                        mboot_info->framebuffer_bpp,
                                                        mboot_info->framebuffer_pitch);
    motd();
    CALL_IMPL(arch,         subsystem,              setup); // setup arch-dependant things (SSE, ...)
    CALL_IMPL(mm,           memory_subsystem,       initialize, // setup page frame allocation
                                                        mboot_magic,
                                                        mboot_info);
    CALL_IMPL(security,     data_subsystem,         initialize); // setup identification system
    CALL_IMPL(mm,           memory_subsystem,       finalize); // setup page mapping
    CALL_IMPL(ipc,          data_subsystem,         initialize);
    CALL_IMPL(interrupt,    subsystem,              setup);
    CALL_IMPL(vfs,          data_subsystem,         initialize);  // klog becomes available here
    CALL_IMPL(acpi,         subsystem,              setup);
    CALL_IMPL(peripherals,  interr_ctrl_subsystem,  setup);
    CALL_IMPL(audio,        subsystem,              setup);
    CALL_IMPL(vfs,          data_subsystem,         finalize);
    CALL_IMPL(security,     data_subsystem,         finalize);
    CALL_IMPL(ipc,          data_subsystem,         finalize);
    CALL_IMPL(task,         tasking_subsystem,      initialize);
    klog(MSG_DONE_BASIC_INIT_EN);
}

void initramfs_supported_startup() {
    bool mounted;
    printf(MSGI18N(MSG_MOUNTING_INIT));
    mounted = mount_initial_disk();
    while (!mounted) {
        printf(MSGI18N(MSG_INSERT_INIT_MEDIA));
        char key; do {key = getchar();} while (key != '\n');
        mounted = mount_initial_disk();
    }
    
    if (!load_lbf_file(BITMAP_FONT_PATH)) {
        printf(MSGI18N(MSG_ERR_LOAD_CUSTOM_FONT), BITMAP_FONT_PATH);
    } else {
        TTY_CURSOR = TTY_CURSOR_UNICODE;
    }
    omni_terminal_finalize();
    klog(MSG_DONE_ADVANCED_INIT_EN);
    CALL_IMPL(task, tasking_subsystem, finalize);
}



void kernel_main(ucpuint_t mboot_magic, ucpuint_t mboot_header) {
    initialization(mboot_magic, mboot_header);
    srand(time(NULL));
    
#ifdef BEEP_WELCOME
    do_beep_welcome();
#endif
    
    initramfs_supported_startup();
    
    printf(MSGI18N(MSG_MEDITATING));
    idle();
}
