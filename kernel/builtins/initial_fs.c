#include <stdio.h>
#include <string.h>
#include <data/path.h>
#include <kernel/init.h>
#include <kernel/core.h>
#include <data/fs/internalfs.h>
#include <data/fs/initramfs.h>
#include <data/gpt.h>
#include <security/identify.h>
#include <peripherals/floppy.h>

#include <sys/fs.h>

#include <localize/messages.h>





bool scan_partitions(bool autoadd) {
    DirEnt *entries = NULL;
    size_t num_entries;
    
    int failure;
    internalfs_composites.list_directory(get_agent(0), OS_DISK_PATH, &entries, &num_entries, &failure);
    if (failure != 0) {
        return false;
    }
    
    char diskpath[2048];
    long disk_num_part;
    for (size_t d=0; d<num_entries; d++) {
        snprintf(diskpath, 2048, "%s,%s", OS_DISK_PATH, entries[d].name);
        scan_disk_for_gpt(diskpath, &disk_num_part, autoadd);
    }
    
    free(entries);
    return true;
}


bool find_first_initrd_part(char *diskpath) {
    DirEnt *part_entries = NULL;
    size_t num_entries;
    
    int failure = 0;
    internalfs_composites.list_directory(get_agent(0), OS_PART_PATH, &part_entries, &num_entries, &failure);
    if (failure != 0) {
        return false;
    }
    
    for (size_t p=0; p<num_entries; p++) {
        if (endswith(part_entries[p].name, INIT_PARTNAME)) {
            snprintf(diskpath, 2048, "%s,%s", OS_PART_PATH, part_entries[p].name);
            free(part_entries);
            return true;
        }
    }
    
    free(part_entries);
    return false;
}

bool mount_initramfs(char *diskpath, 
                     FileMetaData metadata,
                     char *mount_path) {
    FileDescriptor *mount;
    
    FileDescriptor *diskfile;
    if (!resolve_path(diskpath, &diskfile)) return false;
    
    if (!initramfs_get_root_mountpoint(&mount, diskpath, metadata)) return false;
    
    snprintf(mount->name, MAX_FILENAME_LENGTH, "initramfs%d", 0);
    
    snprintf(mount_path, 2048, "%s,%s", OS_MOUNT_PATH, mount->name);
    if (!install_file(mount_path, mount)) {
        free(mount);
        return false;
    }
    
    return true;
}


#define PRE_NEWLINE (is_any_floppy_inserted_lazy() ? "\n" : "")

bool mount_initial_disk() {
    FileMetaData meta = {
        .race_attrib = Dragon,
        .class_attrib = Knight,
        .executable = false,
        .writable = true,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    
    toggle_floppy_visual_transfer(true);
    
    scan_partitions(true);
    
    char diskpath[2048];
 
    if (!find_first_initrd_part(diskpath)) {
        printf(MSGI18N(MSG_NO_INITRD_FOUND), PRE_NEWLINE);
        return false;
    }
    
    char mountpath[2048];
    if (!mount_initramfs(diskpath, 
                         meta,
                         mountpath)) {
        printf(MSGI18N(MSG_FAILED_MOUNT_INITIAL_FS), PRE_NEWLINE);
        return false;
    }
    
    char linkpath[2048];
    snprintf(linkpath, 2048, "%s,init", OS_DEFAULT_FS_PATH);
    int failure;
    internalfs_composites.create_link(get_agent(1), linkpath, mountpath, &failure);
    if (failure) {
        printf(MSGI18N(MSG_INITRD_LINKAGE_FAILED), PRE_NEWLINE, linkpath, mountpath, failure);
        return false;
    }
    
    toggle_floppy_visual_transfer(false);
    printf(MSGI18N(MSG_INITRD_MOUNT_SUCCESS), PRE_NEWLINE, diskpath, mountpath);
    return true;
}
