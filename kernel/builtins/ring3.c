#include <syscall.h>
#include <os.h>
#include <stdio.h>
#include <sys/proc.h>
#include <stdlib.h>
#include <builtins.h>
#include <arch/setup.h>
#include <kernel/core.h>
#include <kernel/init.h>
#include <kernel/bitmap_font.h>
#include <data/path.h>
#include <mm/mm.h>
#include <mm/paging.h>
#include <security/identify.h>
#include <security/authorize.h>
#include <security/hardware.h>
#include <task/task.h>
#include <sys/loader.h>

#include <localize/messages.h>


static int init_return;


void jump_to_ring3() {
    printf(MSGI18N(MSG_ENTER_USER_MODE));
    // load the elf code in memory
    unsigned char* initdata = userland_loader(INITFILE);
    if (initdata == NULL) {
        printf(MSGI18N(MSG_ERROR_LAUNCH_INIT), LAUNCHELF_ERR_FOPEN);
        panic();
    }
    
    #if defined(__is_libk)
    ksdafsjdf();
    #endif
    
    
    HalfElfLoaderReturn loaded_elf = half_elf_loader(initdata, "init");
    if (loaded_elf.status != LAUNCHELF_OK) {
        printf(MSGI18N(MSG_ERROR_LAUNCH_INIT), loaded_elf.status);
        free(initdata);
        panic();
    }
    free(initdata);
    
    // spawn a new agent
    unsigned int new_process_ident = add_new_agent(Angel, King, "init", true);
    // create (or reuse) a page management object for the new agent
    PageManagementObject *process_page_mgr = new_page_management_entry(Angel, King);
    
    // assigne the elf memory to the new agent
    //size_t num_elf_used_pages = get_memarea_length(loaded_elf.memarea) / PAGE_SIZE;
    transfer_mem_ownership(loaded_elf.memarea, new_process_ident);
    
    // create a stack for the new process
    void *new_stack = malloc(PROC_DEFAULT_STACKSIZE);
    uint32_t new_stack_top = ((uint32_t)new_stack) + PROC_DEFAULT_STACKSIZE - 16;
    transfer_mem_ownership(new_stack, new_process_ident);
    
    // create an environment block for the new process
    char **new_env = (char**) malloc(sizeof(char*) * 4);
    new_env[0] = NULL;
    
    setenv_r("KERNEL_VER", BUILD_VERSION, 1, &new_env);
    transfer_mem_ownership(new_env[0], new_process_ident);
    
    setenv_r("PWD", SHELL_INITIAL_WORK_DIR, 1, &new_env);
    transfer_mem_ownership(new_env[1], new_process_ident);
    
    if (bitmap_font_table != NULL) {
        setenv_r("BITMAP_FONT", BITMAP_FONT_PATH, 1, &new_env);
        transfer_mem_ownership(new_env[2], new_process_ident);
    }
    
    transfer_mem_ownership(new_env, new_process_ident);
    
    //printf("Init: %dB mem; entrypoint 0x%x; stack top 0x%x;\n", loaded_elf.mem_required, loaded_elf.entrypoint, new_stack_top);
    
    Task *newtask = new_task(
        loaded_elf.entrypoint, 
        loaded_elf.memarea,
        new_stack_top, 
        new_stack,
        (uint32_t)new_env,
        get_cr3_from_page_mgr_object(process_page_mgr), 
        0, 
        new_process_ident,
        INVALID_TASK_ID,
        &init_return,
        "init",
        loaded_elf.debug_info
    );
    
    char stackname[256];
    snprintf(stackname, 256, "STACK-main");
    TaskDebuginfo_append_mem_section(newtask->debug_info, stackname, (uintptr_t)new_stack, PROC_DEFAULT_STACKSIZE);
    
    tss_flush();
    
    uint32_t stack_top;
    asm volatile(
        "mov %[stacktop], esp"
        : [stacktop] "=r"(stack_top)
        : 
        : "esp");
    set_kernel_stack(stack_top);
    
    iswitch_task(0);
    
    __builtin_unreachable();
}
