#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <debug.h>
#include <sys/proc.h>
#include <kernel/core.h>
#include <elfload/elfload.h>
#include <builtins.h>


// elfload interface

static bool fpread(el_ctx *ctx, void *dest, size_t nb, size_t offset) {
    memcpy(dest, ctx->raw_elf + offset, nb);

    return true;
}

static void *alloccb(
    el_ctx *ctx,
    Elf_Addr phys,
    Elf_Addr virt,
    Elf_Addr size)
{
    (void) ctx;
    (void) phys;
    (void) size;
    return (void*) virt;
}


// debug info extraction

static char *find_section_names(el_ctx *ctx) {
    int err;
    Elf_Shdr sh_strtab;
    
    if ((err = el_load_sh_strtab(ctx, &sh_strtab))) return NULL;
    
    size_t strtab_length = sh_strtab.sh_size;
    size_t strtab_offset = sh_strtab.sh_offset;
    if (strtab_length == 0) return NULL;
    
    return (char*)(ctx->raw_elf + strtab_offset);
}


static bool get_section(el_ctx *ctx, char *name, Elf_Shdr *shdr) {
    char *section_names = find_section_names(ctx);
    if (section_names == NULL) return false;
    
    int err;
    unsigned i = 0;
    err = el_findshdr(ctx, shdr, name, &i, section_names);
    
    return err == 0;
}

#define EL_SHOFF(ctx, num) (((ctx)->ehdr.e_shoff + (num) * (ctx)->ehdr.e_shentsize))

static int fill_debug_info(el_ctx *ctx, uintptr_t global_offset, TaskDebuginfo *debug_info) {
    char *section_names = find_section_names(ctx);
    Elf_Shdr shdr;
    int rv;
    
    size_t actual_sections = 0;
    debug_info->mem_map_length = ctx->ehdr.e_shnum;
    debug_info->mem_map = (TaskSection*) malloc(sizeof(TaskSection) * debug_info->mem_map_length);
    debug_info->valid = true;
    debug_info->num_stab_symbols = 0;
    
    for (int section_id = 0; section_id < ctx->ehdr.e_shnum; section_id++) {
        if ((rv = el_pread(ctx, &shdr, sizeof shdr, EL_SHOFF(ctx, section_id)))) {
            debug_info->valid = false;
            free(debug_info->mem_map);
            return rv;
        }
        
        const char *s_name = section_names + shdr.sh_name;
        int program_id = program_id_from_section(ctx, &shdr);
        if (!(strlen(s_name) == 0 && shdr.sh_size == 0) && \
            (program_id >= 0)) {
            
            strncpy(debug_info->mem_map[actual_sections].name, s_name, MAX_SECTIONNAME_LENGTH);
            debug_info->mem_map[actual_sections].start = global_offset + shdr.sh_addr;
            debug_info->mem_map[actual_sections].length = (size_t) shdr.sh_size;

            if (strcmp(s_name, ".stab") == 0) {
                debug_info->stabs = (StabSymbol*) debug_info->mem_map[actual_sections].start;
                
                if (debug_info->stabs[0].n_type == STAB_N_UNDF && debug_info->stabs[0].n_desc > 0) {
                    debug_info->num_stab_symbols = debug_info->stabs[0].n_desc + 1;
                } else {
                    debug_info->num_stab_symbols = debug_info->mem_map[actual_sections].length / sizeof(StabSymbol);
                    if (debug_info->mem_map[actual_sections].length % sizeof(StabSymbol) != 0) {
                        debug_printf("kernel loader warning: .stab section size (%lu) not multiple of %lu\r\n", debug_info->mem_map[actual_sections].length, sizeof(StabSymbol));
                    }
                }
                    
            } else if (strcmp(s_name, ".stabstr") == 0) {
                debug_info->stabstr = (char*) debug_info->mem_map[actual_sections].start;
            }
        
            actual_sections++;
        }
        
    }
    
    debug_info->mem_map = realloc(debug_info->mem_map, actual_sections * sizeof(TaskSection));
    debug_info->mem_map_length = actual_sections;
    
    return 0;
}




HalfElfLoaderReturn half_elf_loader(unsigned char* rawelf, const char *program_name) {
    HalfElfLoaderReturn state;
    
    el_ctx ctx;
    ctx.pread = fpread;
    ctx.raw_elf = rawelf;
    
    int initerr;
    if ((initerr = el_init(&ctx))) {
        state.status = LAUNCHELF_ERR_INIT - initerr;
        return state;
    }
    size_t needed_size = ctx.memsz;
    void *memarea = malloc(needed_size);
    
    ctx.base_load_vaddr = ctx.base_load_paddr = (uintptr_t) memarea;
    
    int loaderr;
    if ((loaderr = el_load(&ctx, &alloccb))) {
        free(memarea);
        state.status = LAUNCHELF_ERR_LOAD - loaderr;
        return state;
    }
    int relocerr;
    if ((relocerr = el_relocate(&ctx))) {
        free(memarea);
        state.status = LAUNCHELF_ERR_RELOC - relocerr;
        return state;
    }
    
    uintptr_t epaddr = ctx.ehdr.e_entry + (uintptr_t) memarea;
    
    TaskDebuginfo *dbginfo = malloc(sizeof(TaskDebuginfo));
    fill_debug_info(&ctx, (uintptr_t) memarea, dbginfo);
    state.debug_info = dbginfo;
    
    #ifdef ADVANCED_DEBUG
        Elf_Shdr text_header;
        if (!get_section(&ctx, ".text", &text_header)) {
            debug_printf("Loaded %s @%p [%lu B]; entrypoint @ %p; .text header not found\r\n", program_name, memarea, needed_size, epaddr);
        } else {
            uintptr_t textaddr = text_header.sh_offset + (uintptr_t) memarea;
            debug_printf("Loaded %s @%p [%lu B]; entrypoint @ %p; .text @%p (%lu symbols)\r\n", program_name, memarea, needed_size, epaddr, textaddr, dbginfo->num_stab_symbols);
        }
    #endif
    
    state.entrypoint = epaddr;
    state.memarea = memarea;
    state.mem_required = needed_size;
    state.status = LAUNCHELF_OK;
    return state;
} 
