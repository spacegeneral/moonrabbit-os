#include <builtins.h>
#include <task/task.h>
#include <sys/debuginfo.h>
#include <arch/setup.h>
#include <mm/mm.h>
#include <stdio.h>
#include <rice.h>


unsigned int compute_addr(StabSymbol* function, StabSymbol* line) {
    if (function == NULL || line == NULL) return 0;
    return function->n_value + line->n_value;
}


/*static char* apretty_print_function_with_scope(char* stabstr_entry) {
    size_t maxlen = strlen(stabstr_entry) + strlen("xxxxxx function ");
    char* pretty = malloc(maxlen);
    char functype = strchr(stabstr_entry, ':')[1];
    snprintf(pretty, maxlen, "%s function %s", (functype == 'F' ? "global" : "local"), stabstr_entry);
    strchr(pretty, ':')[0] = '\0';
    return pretty;
}*/

static char* apretty_print_function(char* stabstr_entry) {
    size_t maxlen = strlen(stabstr_entry) + strlen("xxxxxx function ");
    char* pretty = malloc(maxlen);
    snprintf(pretty, maxlen, "%s", stabstr_entry);
    strchr(pretty, ':')[0] = '\0';
    return pretty;
}



bool get_debug_line_info(Task* task, addr_t ip, char** filename, char** functionname, unsigned* linenum) {
    unsigned int addr = (unsigned int) ip;
    
    StabSymbol* last_file = NULL;
    StabSymbol* last_function = NULL;
    StabSymbol* last_line = NULL;
    
    StabSymbol* curr_symbol;
    
    for (size_t i=0; i<task->debug_info->num_stab_symbols; i++) {
        curr_symbol = task->debug_info->stabs + i;
        char* symbol_name = task->debug_info->stabstr + curr_symbol->n_strx;
        //debug_printf("curr_symbol type 0x%02x: %s, desc: %d, value: 0x%08x\r\n", curr_symbol->n_type, symbol_name, curr_symbol->n_desc, curr_symbol->n_value);
        switch (curr_symbol->n_type) {
            case STAB_N_SO:
                last_file = curr_symbol;
            break;
            case STAB_N_FUN:
                if (symbol_name[0] != '\0') {
                    last_function = curr_symbol;
                }
            break;
            case STAB_N_SLINE:
            {
                unsigned int last_addr = compute_addr(last_function, last_line);
                unsigned int curr_addr = compute_addr(last_function, curr_symbol);
                
                if (addr >= last_addr && addr <= curr_addr) {
                    (*filename) = task->debug_info->stabstr + last_file->n_strx;
                    (*functionname) = task->debug_info->stabstr + last_function->n_strx;
                    (*linenum) = (unsigned)last_line->n_desc;
                    return true;
                }
                
                last_line = curr_symbol;
            }
                
            break;
            default:
                continue;
        }
    }
    return false;
}


void print_stack_trace(Task* task) {  // minus the last entry, where the exception occurred
    char* filename;
    char* functionname;
    unsigned linenum;
    
    bool continue_walking = true;
    
    addr_t current_ebp = task->regs.ebp;  //TODO multiplatform
    addr_t ip;
    
    while (continue_walking) {
        ip = *( (addr_t*)(current_ebp + 4) );
        //debug_printf("IP: 0x%08X, EBP: 0x%08X, *EBP: 0x%08X\r\n", ip, current_ebp, *( (addr_t*)(current_ebp) ));
        continue_walking = get_debug_line_info(task, ip, &filename, &functionname, &linenum);
        if (continue_walking) {
            char* function_name = apretty_print_function(functionname);
            printf("                       %s, %s():%u\n", filename, function_name, linenum);
            debug_printf("                       %s, %s():%u\r\n", filename, function_name, linenum);
            free(function_name);
            
            current_ebp = *( (addr_t*)(current_ebp) );
        }
    }
}


void print_exception_debug_user(Task* task, addr_t ip) {
    char* filename;
    char* functionname;
    unsigned linenum;
    
    if (get_debug_line_info(task, ip, &filename, &functionname, &linenum)) {
        char* function_name = apretty_print_function(functionname);
        printf("Exception stack trace: ");
        setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED); printf("%s", filename); setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
        printf(", ");
        setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED); printf("%s", function_name); setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
        printf("():");
        setcolors(COLOR24_TEXT_HIGHLIGHT, COLOR24_UNDEFINED); printf("%u\n", linenum); setcolors(COLOR24_TEXT, COLOR24_UNDEFINED);
        debug_printf("Exception stack trace: %s, %s:%u\r\n", filename, function_name, linenum);
        free(function_name);
        
        print_stack_trace(task);
    }
}




bool print_debug_memarea_memmap_at(uintptr_t addr, Task *task, TaskSection* mem_map, size_t mem_map_length) {
    for (size_t i=0; i<mem_map_length; i++) {
        TaskSection* section = mem_map + i;
        if (addr >= section->start && addr < (section->start + section->length)) {
            printf("region %s [0x%08X-0x%08X] (task %s (%d))", section->name, section->start, section->start+section->length-1, task->name, task->task_id);
            debug_printf("region %s [0x%08X-0x%08X] (task %s (%d))", section->name, section->start, section->start+section->length-1, task->name, task->task_id);
            return true;
        }
    }
    return false;
}


typedef struct print_debug_memarea_state {
    bool found;
    unsigned addr;
    Task *task;
} print_debug_memarea_state;


static void applied_to_heap_memareas(unsigned start, unsigned length, char *filename, unsigned linenum, void *user_data) {
    print_debug_memarea_state* state = (print_debug_memarea_state*)user_data;
    if (state->addr >= start && state->addr < (start + length)) {
        if (state->task != NULL) {
            printf("HEAP region %s:%u [0x%08X-0x%08X] (task %s (%d))", filename, linenum, start, start + length - 1, state->task->name, state->task->task_id);
            debug_printf("HEAP region %s:%u [0x%08X-0x%08X] (task %s (%d))", filename, linenum, start, start + length - 1, state->task->name, state->task->task_id);
        } else {
            printf("kernel HEAP region %s:%u [0x%08X-0x%08X]", filename, linenum, start, start + length - 1);
            debug_printf("kernel HEAP region %s:%u [0x%08X-0x%08X]", filename, linenum, start, start + length - 1);
        }
        state->found = true;
    }
}


bool print_debug_memarea_info(uintptr_t addr) {
    print_debug_memarea_state kstate = {
        .found = false,
        .addr = (unsigned)addr,
        .task = NULL
    };
    
    apply_to_memareas_by_tag(0, &applied_to_heap_memareas, (void*)&kstate);
    if (kstate.found) return true;
    
    for (size_t t=0; t<MAX_TASKS; t++) {
        Task *currtask = tasklist + t;
        if (currtask->task_id != INVALID_TASK_ID) {
            if (currtask->debug_info->valid) {
                if (print_debug_memarea_memmap_at(addr, currtask, currtask->debug_info->mem_map, currtask->debug_info->mem_map_length)) return true;
            }
            print_debug_memarea_state state = {
                .found = false,
                .addr = (unsigned)addr,
                .task = currtask
            };
            apply_to_memareas_by_tag(currtask->owner_id, &applied_to_heap_memareas, (void*)&state);
            if (state.found) return true;
        }
    }
    return false;
}



