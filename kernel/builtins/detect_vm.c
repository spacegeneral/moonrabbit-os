#include <string.h>
#include <acpi/interface.h>
#include <acpi/rsdp.h>
#include <kernel/log.h>


bool is_running_inside_vm = false;


bool is_inside_vm() {
    return is_running_inside_vm;
}

void detect_vm() {
    if (is_running_inside_vm) return;  // we already know
    
    if (CALL_IMPL(acpi, subsystem, ready_level) >= ACPI_READYLEVEL_BASIC) {
        char oemid_terminated[7];
        memcpy(oemid_terminated, rsdp->first_part.oemid, 6);
        
        if (strcmp(oemid_terminated, "BOCHS ") == 0) {
            is_running_inside_vm = true;
            klog("VM detected! (reason: \"BOCHS \" RSDP OEMID)\n");
            return;
        }
    }
    
    is_running_inside_vm = false;
}
