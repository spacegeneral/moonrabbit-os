#include "softfp.h"
#include <stdio.h>
#include <kernel/core.h>



double __muldf3(double a, double b) {
    double *a_ptr = &a;
    double *b_ptr = &b;
    
    //debug_printf("Multiplying %f (%llu) * %f (%llu) \r\n", a, *((uint64_t*)a_ptr), b, *((uint64_t*)b_ptr));
    
    uint32_t fflags;
    uint64_t result = mul_sf64(*((uint64_t*)a_ptr), 
                               *((uint64_t*)b_ptr), 
                                 RM_RNE, 
                                 &fflags);
    uint64_t *result_ptr = &result;
    return *((double*)result_ptr);
}


double __divdf3(double a, double b) {
    double *a_ptr = &a;
    double *b_ptr = &b;
    
    //debug_printf("Dividing %f (%llu) / %f (%llu) \r\n", a, *((uint64_t*)a_ptr), b, *((uint64_t*)b_ptr));
    
    uint32_t fflags;
    uint64_t result = div_sf64(*((uint64_t*)a_ptr), 
                               *((uint64_t*)b_ptr), 
                                 RM_RNE, 
                                 &fflags);
    
    uint64_t *result_ptr = &result;
    return *((double*)result_ptr);
}




double ll_to_double(long long i) {
    uint32_t fflags;
    uint64_t result = cvt_i64_sf64(i, RM_RNE, &fflags);
    uint64_t *result_ptr = &result;
    return *((double*)result_ptr);
}

double __floattidf(long long i) {
    return ll_to_double(i);
}

double ull_to_double(unsigned long long i) {
    uint32_t fflags;
    uint64_t result = cvt_u64_sf64(i, RM_RNE, &fflags);
    uint64_t *result_ptr = &result;
    return *((double*)result_ptr);
}

double __floatuntidf(unsigned long long i) {
    return ull_to_double(i);
}

double i_to_double(int i) {
    uint32_t fflags;
    uint64_t result = cvt_i32_sf64(i, RM_RNE, &fflags);
    uint64_t *result_ptr = &result;
    return *((double*)result_ptr);
}

double __floatsidf (int i) {
    return i_to_double(i);
}

double f_decode_to_double(uint32_t f) {
    uint32_t fflags;
    uint64_t result = cvt_sf32_sf64(f, &fflags);
    uint64_t *result_ptr = &result;
    return *((double*)result_ptr);
}


