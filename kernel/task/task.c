#include <task/task.h>
#include <builtins.h>
#include <stdio.h>
#include <data/fsdial.h>
#include <data/path.h>
#include <ipc/interface.h>
#include <interrupt/timing.h>
#include <mm/mm.h>
#include <security/defaults.h>
#include <data/fs/internalfs.h>
#include <syscall.h>
#include <sys/happy.h>


Task tasklist[MAX_TASKS];

static unsigned int running_task_id;


unsigned volatile long long int resource_clock = 1;



unsigned int get_free_task_id() {
    for (unsigned int uid=0; uid<MAX_TASKS; uid++) {
        if (tasklist[uid].task_id == INVALID_TASK_ID) return uid;
    }
    return INVALID_TASK_ID;
}


bool task_exists(unsigned int task_id) {
    if (task_id == INVALID_TASK_ID) return false;
    if (task_id >= MAX_TASKS) return false;
    return tasklist[task_id].task_id != INVALID_TASK_ID;
}


void mark_task_invalid(unsigned int task_id) {
    tasklist[task_id].task_id = INVALID_TASK_ID;
}


Task* get_current_task() {
    if (running_task_id == INVALID_TASK_ID) return NULL;
    return tasklist + running_task_id;
}

Task* get_task(unsigned int task_id) {
    return tasklist + task_id;
}


int count_tasks_by_owner(unsigned int owner_id) {
    int count = 0;
    for (size_t i=0; i<MAX_TASKS; i++) {
        if (tasklist[i].task_id == INVALID_TASK_ID) continue;
        if (tasklist[i].owner_id == owner_id) count++;
    }
    return count;
}


void reset_tasks_resources() {
    for (size_t i=0; i<MAX_TASKS; i++) {
        tasklist[i].time_counter_latch[TIME_COUNTER_USER] = tasklist[i].time_counter[TIME_COUNTER_USER];
        tasklist[i].time_counter_latch[TIME_COUNTER_KERNEL] = tasklist[i].time_counter[TIME_COUNTER_KERNEL];
        tasklist[i].resource_counter_latch = resource_clock;
        tasklist[i].time_counter[TIME_COUNTER_USER] = 0;
        tasklist[i].time_counter[TIME_COUNTER_KERNEL] = 0;
    }
}


void ireturn_to_current_task() {
    current_time_selector = TIME_COUNTER_USER;
    irestore_registers(saved_registers_location);
}


void store_saved_registers_into_task(Task* task) {
    task->regs = *saved_registers_location;
}


void superswitch_task(unsigned int next_task_id) {
    if (running_task_id != INVALID_TASK_ID) {
        tasklist[running_task_id].regs = *saved_registers_location;
    }
    Registers *to = &(tasklist[next_task_id].regs);
    running_task_id = next_task_id;
    restore_registers(to);
}


static void normal_iswitch_task(unsigned int next_task_id) {
    Registers *to = &(tasklist[next_task_id].regs);
    running_task_id = next_task_id;
    current_time_selector = TIME_COUNTER_USER;
    irestore_registers(to);
}

static void resume_handler_iswitch_task(unsigned int next_task_id) {
    Registers *to = &(tasklist[next_task_id].regs);
    change_saved_ip(to, tasklist[next_task_id].preempt.resume);
    running_task_id = next_task_id;
    current_time_selector = TIME_COUNTER_USER;
    tasklist[next_task_id].preempt.stage = PREEMPTION_RESUMED;
    irestore_registers(to);
}

static void continuation_handler_iswitch_task(unsigned int next_task_id) {
    Registers *to = &(tasklist[next_task_id].preempt.continuation);
    running_task_id = next_task_id;
    current_time_selector = TIME_COUNTER_USER;
    tasklist[next_task_id].preempt.stage = PREEMPTION_OFF;
    irestore_registers(to);
}



void iswitch_task(unsigned int next_task_id) {
    if (running_task_id != INVALID_TASK_ID) {
        tasklist[running_task_id].regs = *saved_registers_location;
    }
    iswitch_task_forget_current(next_task_id);
}

void iswitch_task_continuation(unsigned int next_task_id) {
    if (tasklist[next_task_id].preempt.stage == PREEMPTION_RESUMED) {
        tasklist[next_task_id].regs = tasklist[next_task_id].preempt.continuation;
        continuation_handler_iswitch_task(next_task_id);
    } else {
        if (running_task_id != INVALID_TASK_ID) {
            tasklist[running_task_id].regs = *saved_registers_location;
        }
        iswitch_task_forget_current(next_task_id);
    }
}

void iswitch_task_forget_current(unsigned int next_task_id) {
    if (tasklist[next_task_id].preempt.stage == PREEMPTION_ACQUIRED) {
        resume_handler_iswitch_task(next_task_id);
    } else {
        normal_iswitch_task(next_task_id);
    }
}



void activate_multitasking_system() {
    jump_to_ring3();
}



static size_t tasks_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)(filename);  //unused
    size_t written = 0;
    
    for (size_t t=0; t<MAX_TASKS; t++) {
        Task *currtask = tasklist + t;
        if (currtask->task_id == INVALID_TASK_ID) continue;
        double user_percent = ((ull_to_double(currtask->time_counter_latch[TIME_COUNTER_USER]) * 100.0) / ull_to_double(currtask->resource_counter_latch));
        double kernel_percent = ((ull_to_double(currtask->time_counter_latch[TIME_COUNTER_KERNEL]) * 100.0) / ull_to_double(currtask->resource_counter_latch));
        written += snprintf(dest+written, maxsize-written, "[%02d] (User:%.2f%% Kern:%.2f%% Happy:%.2f) owner:%d parent:%d %s\n", 
                            currtask->task_id, user_percent, kernel_percent, currtask->happyness, currtask->owner_id, currtask->parent_task, currtask->name);
    }
    
    return written;
}


static size_t tasks_robot_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)(filename);  //unused
    
    TaskInfoStats info;
    
    size_t written = 0;
    
    for (size_t t=0; t<MAX_TASKS; t++) {
        Task *currtask = tasklist + t;
        if (currtask->task_id == INVALID_TASK_ID) continue;
        if (written + sizeof(TaskInfoStats) > maxsize) break;
        
        memset(&info, 0, sizeof(TaskInfoStats));
        info.task_id = currtask->task_id;
        info.parent_task = currtask->parent_task;
        info.owner_id = currtask->owner_id;
        info.time_counter[TIME_COUNTER_USER] = currtask->time_counter_latch[TIME_COUNTER_USER];
        info.time_counter[TIME_COUNTER_KERNEL] = currtask->time_counter_latch[TIME_COUNTER_KERNEL];
        strncpy(info.name, currtask->name, MAX_TASKNAME_LENGTH);
        info.phys_race_attrib = get_agent(currtask->owner_id)->phys_race_attrib;
        info.data_class_attrib = get_agent(currtask->owner_id)->data_class_attrib;
        
        memcpy(dest + written, (char*)&info, sizeof(TaskInfoStats));
        written += sizeof(TaskInfoStats);
    }
    
    return written;
}


size_t tasks_subtree_dial_interface(Task *task, char *dest, size_t maxsize, int margin) {
    size_t written = 0;
    
    for (int m=0; m<margin; m++) {
        written += snprintf(dest+written, maxsize-written, " ");
    }
    
    double user_percent = ((ull_to_double(task->time_counter_latch[TIME_COUNTER_USER]) * 100.0) / ull_to_double(task->resource_counter_latch));
    double kernel_percent = ((ull_to_double(task->time_counter_latch[TIME_COUNTER_KERNEL]) * 100.0) / ull_to_double(task->resource_counter_latch));
    
    written += snprintf(dest+written, maxsize-written, "[%02d] (User:%.2f%% Kern:%.2f%% Happy:%.2f) owner:%d %s\n", task->task_id, user_percent, kernel_percent, task->happyness, task->owner_id, task->name);
    for (size_t t=0; t<MAX_TASKS; t++) {
        Task *currtask = tasklist + t;
        if (currtask->task_id == INVALID_TASK_ID) continue;
        if (currtask->parent_task == task->task_id) {
            written += tasks_subtree_dial_interface(currtask, dest+written, maxsize-written, margin+4);
        }
    }
    
    return written;
}

static size_t tasks_tree_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)(filename);  //unused
    size_t written = 0;
    
    Task *root = NULL;
    
    for (size_t t=0; t<MAX_TASKS; t++) {  // find the root
        Task *currtask = tasklist + t;
        if (currtask->task_id == INVALID_TASK_ID) continue;
        if (currtask->parent_task == INVALID_TASK_ID) {
            root = currtask;
            break;
        }
    }
    if (root == NULL) return written;
    
    written += tasks_subtree_dial_interface(root, dest, maxsize, 0);
    
    return written;
}


void tasks_make_system_dial_files() {
    FileDescriptor* dialfile_h;
    FileDescriptor* dialfile2_h;
    FileDescriptor* dialfile_r;
    
    FileDescriptor* dest_folder_h;
    FileDescriptor* dest_folder_r;
    
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_INFODIAL_RACE,
        .class_attrib = DEFAULT_SEC_INFODIAL_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    dial_fs_primitives.create_file(&dialfile_h, "tasks", meta);
    dial_bind_callback(dialfile_h, &tasks_dial_interface);
    dial_fs_primitives.create_file(&dialfile_r, "tasks", meta);
    dial_bind_callback(dialfile_r, &tasks_robot_dial_interface);
    dial_fs_primitives.create_file(&dialfile2_h, "tasktree", meta);
    dial_bind_callback(dialfile2_h, &tasks_tree_dial_interface);
    
    resolve_path(OS_DIALS_H_PATH, &dest_folder_h);
    resolve_path(OS_DIALS_R_PATH, &dest_folder_r);
    
    file_method(dest_folder_h)->insert_into_directory(dest_folder_h, dialfile_h);
    file_method(dest_folder_h)->insert_into_directory(dest_folder_h, dialfile2_h);
    file_method(dest_folder_r)->insert_into_directory(dest_folder_r, dialfile_r);
}


void tasks_make_system_memmap_file(int task_id) {
    FileDescriptor* mapfile;
    FileDescriptor* dest_folder;
    char name[MAX_FILENAME_LENGTH];
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_TASKMAP_RACE,
        .class_attrib = DEFAULT_SEC_TASKMAP_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    snprintf(name, MAX_FILENAME_LENGTH, "%d", task_id);
    dial_fs_primitives.create_file(&mapfile, name, meta);
    dial_bind_callback(mapfile, &tasks_memmap_dial_interface);
    
    resolve_path(OS_DIALS_MEMMAPS_PATH, &dest_folder);
    
    file_method(dest_folder)->insert_into_directory(dest_folder, mapfile);
}

void tasks_remove_system_memmap_file(int task_id) {
    char path[4096];
    snprintf(path, 4096, "%s,%d", OS_DIALS_MEMMAPS_PATH, task_id);
    
    int remove_status;
    internalfs_composites.remove(get_agent(0), path, &remove_status);
}


static Task* finishing_touches(Task* newtask) {
    newtask->preempt.enabled = false;
    newtask->preempt.stage = PREEMPTION_OFF;
    return newtask;
}


Task* new_task(ucpuint_t entry_point, 
               void *main_memarea,
               ucpuint_t stack_top,
               void *stack_memarea,
               ucpuint_t environ,
               ucpuint_t cr3, 
               ucpuint_t flags, 
               unsigned int owner_id,
               unsigned int parent_task,
               int *return_code_here, 
               const char* name, 
               TaskDebuginfo *debug_info) {
    
    unsigned int task_id = get_free_task_id();
    if (task_id == INVALID_TASK_ID) return NULL;
    
    return finishing_touches(make_task(task_id, entry_point, main_memarea, stack_top, stack_memarea, environ, cr3, flags, owner_id, parent_task, return_code_here, name, debug_info));
}

Task* new_task_with_id(unsigned int task_id, 
                       ucpuint_t entry_point, 
                       void *main_memarea,
                       ucpuint_t stack_top,
                       void *stack_memarea,
                       ucpuint_t environ,
                       ucpuint_t cr3, 
                       ucpuint_t flags, 
                       unsigned int owner_id,
                       unsigned int parent_task,
                       int *return_code_here, 
                       const char* name, 
                       TaskDebuginfo *debug_info) {
    
    return finishing_touches(make_task(task_id, entry_point, main_memarea, stack_top, stack_memarea, environ, cr3, flags, owner_id, parent_task, return_code_here, name, debug_info));
}




static void kernel_happiness_tally(const void *message) {
    HappyServiceMessage *msg = (HappyServiceMessage*) message;
    if (task_exists(msg->task_id)) {
        void* hptr = (void*)&(msg->happyness);
        get_task(msg->task_id)->happyness = f_decode_to_double(*((uint32_t*)hptr));
    }
}


void setup_multitasking_system() {
    running_task_id = INVALID_TASK_ID;
    for (size_t t=0; t<MAX_TASKS; t++) {
        Task *currtask = tasklist + t;
        currtask->task_id = INVALID_TASK_ID;
    }
    tasks_make_system_dial_files();
    
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_GRAVEYARD_TOPIC, 1024, sizeof(int), DEFAULT_SEC_GRAVEYARD_RACE, DEFAULT_SEC_GRAVEYARD_CLASS, DEFAULT_SEC_GRAVEYARD_POLICY);
    
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_HAPPY_TOPIC, 1024, sizeof(HappyServiceMessage), DEFAULT_HAPPY_TOPIC_RACE, DEFAULT_HAPPY_TOPIC_CLASS, DEFAULT_HAPPY_TOPIC_POLICY);
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_HAPPY_TOPIC, &kernel_happiness_tally);
}

