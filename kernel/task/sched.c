#include <task/sched.h>
#include <task/task.h>

#include <sys/proc.h>


static bool iret_next_scheduled = false;

void sched_ireturn_next_task() {
    iret_next_scheduled = true;
}

bool consume_sched_ireturn_next_task() {
    bool ret = iret_next_scheduled;
    iret_next_scheduled = false;
    return ret;
}



bool try_schedule_next() {
    if (consume_sched_ireturn_next_task()) {
        unsigned int next_task = get_current_task()->parent_task;
        if (next_task != INVALID_TASK_ID) {
            if (get_current_task()->return_code_here != NULL)
                *(get_current_task()->return_code_here) = NOT_DONE_YET;
            iswitch_task(next_task);
            return true;  // never, actually
        }
    }
    return false;
}

bool try_acquire_continuation(Task* task) {
    if (task->preempt.time_accumulator > task->preempt.timeout) {
        task->regs = *saved_registers_location;
        task->preempt.continuation = task->regs;
        task->preempt.enabled = false;
        task->preempt.stage = PREEMPTION_ACQUIRED;
        sched_ireturn_next_task();
        return true;
    }
    return false;
}

void enable_task_self_preempt(Task* task, uintptr_t resume, unsigned long long expire) {
    //task->preempt.stage = PREEMPTION_OFF;
    task->preempt.enabled = true;
    task->preempt.resume = resume;
    task->preempt.timeout = expire;
    task->preempt.time_accumulator = 0;
}
