#include <mm/mm.h>
#include <task/task.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <security/identify.h>
#include <sys/proc.h>
#include <ipc/interface.h>
#include <data/vfs.h>
#include <security/hardware.h>
#include <kernel/core.h>


bool purge_task(unsigned int task_id) {
    if (!task_exists(task_id)) return false;
    if (!reset_task(task_id)) return false;
    
    Task *troubled_task = get_task(task_id);

    if (count_tasks_by_owner(troubled_task->owner_id) == 1) {
        delete_agent(troubled_task->owner_id);
    }

    mark_task_invalid(task_id);  // mark task invalid
    
    CALL_IMPL(ipc, pubsub, publish, DEFAULT_GRAVEYARD_TOPIC, &task_id, 0);  // publish eulogy
    
    return true;
}

#ifdef MALLOC_GUARD
static void applied_to_heap_memareas(unsigned start, unsigned length, char *filename, unsigned linenum, void *user_data) {
    (void)filename;
    (void)linenum;
    (void)user_data;
    if (length / PAGE_SIZE >= MALLOC_GUARD_THRESHOLD) cascade_clear_guard_pages((void*)start, length / PAGE_SIZE);
}
#endif

bool reset_task(unsigned int task_id) {
    if (!task_exists(task_id)) return false;
    
    Task *troubled_task = get_task(task_id);
    //IdentifiedAgent *troubled_agent = get_agent(troubled_task->owner_id);

    if (count_tasks_by_owner(troubled_task->owner_id) == 1) {
#ifdef MALLOC_GUARD
        apply_to_memareas_by_tag(troubled_task->owner_id, &applied_to_heap_memareas, NULL);
#endif
        free_mem_by_tag(troubled_task->owner_id);
        // do not remove the agent
        TaskDebuginfo_destroy(troubled_task->debug_info);
    } else {
        free(troubled_task->stack_memarea);
        TaskDebuginfo_remove_section_by_start(troubled_task->debug_info, (uintptr_t)troubled_task->stack_memarea);
    }
    
    mq_global_unsubscribe(task_id);
    close_all_files_by_agent(troubled_task->owner_id);
    // do not invalidate the task
    tasks_remove_system_memmap_file(task_id);
    
    // do not pub to graveyard topic
    
    return true;
}
