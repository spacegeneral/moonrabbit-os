#include <stdio.h>
#include <string.h>
#include <task/task.h>
#include <builtins.h>
#include <mm/mm.h>
#include <collections/array.h>
#include <kernel/tty.h>
#include <kernel/core.h>
#include <acpi/acpi.h>

#ifdef TRACK_MEMORY
#include <sys/fs.h>
#endif


#define PRINTSECTION_MAXLINE 2048
typedef struct memmap_printsection_entry {
    unsigned start;
    char line[PRINTSECTION_MAXLINE];
} memmap_printsection_entry;


typedef struct memmap_sprintf_state {
    Task *task;
    Array *sections;
    
    unsigned prev_end;
    
    size_t streak_count;
    unsigned streak_length;
    unsigned streak_start;
#ifdef TRACK_MEMORY
    char filename[MAX_FILENAME_LENGTH];
    unsigned int linenum;
#endif
} memmap_sprintf_state;


static int compare_memmap_printsection_entry(const void* a, const void* b) {
    memmap_printsection_entry* entry_a = *((memmap_printsection_entry**) a);
    memmap_printsection_entry* entry_b = *((memmap_printsection_entry**) b);
    if (entry_a->start < entry_b->start) return -1;
    else if (entry_a->start > entry_b->start) return 1;
    return 0;
}


#ifdef TRACK_MEMORY
static void applied_to_heap_memareas(unsigned start, unsigned length, char *filename, unsigned linenum, void *user_data) {
    memmap_sprintf_state* state = (memmap_sprintf_state*) user_data;
    
    if ((start == (unsigned)state->task->main_memarea) || \
        (start == (unsigned)state->task->stack_memarea)) return;  // these areas *are* on the kernel heap, but already displayed in detail
    
    if (state->streak_count == 0) {
        state->streak_length = length;
        state->streak_start = start;
        state->streak_count = 1;
        strncpy(state->filename, filename, MAX_FILENAME_LENGTH);
        state->linenum = linenum;
    } else if (state->prev_end == start && length == state->streak_length && (strcmp(state->filename, filename) == 0) && state->linenum == linenum) {
        state->streak_count++;
    } else {
        if (state->streak_count == 1) {
            memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
            snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                       "    0x%08x - 0x%08x  [ %*u B ]  HEAP chunk from %s:%u\n", 
                                       state->streak_start, 
                                       state->streak_start + state->streak_length - 1, 
                                       8, state->streak_length, 
                                       state->filename, state->linenum);
            newentry->start = state->streak_start;
            array_add(state->sections, newentry);
        } else if (state->streak_count > 1) {
            memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
            snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                       "    0x%08x - 0x%08x  [ %*u B x %u ]  HEAP chunks from %s:%u\n", 
                                       state->streak_start, 
                                       state->streak_start + (state->streak_length * state->streak_count) - 1, 
                                       8, state->streak_length, 
                                       state->streak_count, 
                                       state->filename, state->linenum);
            newentry->start = state->streak_start;
            array_add(state->sections, newentry);
        }
        
        state->streak_length = length;
        state->streak_start = start;
        state->streak_count = 1;
        strncpy(state->filename, filename, MAX_FILENAME_LENGTH);
        state->linenum = linenum;
    }
    state->prev_end = start + length;
}
#else
static void applied_to_heap_memareas(unsigned start, unsigned length, void *user_data) {
    memmap_sprintf_state* state = (memmap_sprintf_state*) user_data;
    
    if ((start == (unsigned)state->task->main_memarea) || \
        (start == (unsigned)state->task->stack_memarea)) return;  // these areas *are* on the kernel heap, but already displayed in detail
    
    if (state->streak_count == 0) {
        state->streak_length = length;
        state->streak_start = start;
        state->streak_count = 1;
    } else if (state->prev_end == start && length == state->streak_length) {
        state->streak_count++;
    } else {
        if (state->streak_count == 1) {
            memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
            snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                       "    0x%08x - 0x%08x  [ %*u B ]  HEAP dynamic chunk\n", 
                                       state->streak_start, state->streak_start + state->streak_length - 1, 8, state->streak_length);
            newentry->start = state->streak_start;
            array_add(state->sections, newentry);
        } else if (state->streak_count > 1) {
            memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
            snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                       "    0x%08x - 0x%08x  [ %*u B x %u ]  HEAP dynamic chunks\n", 
                                       state->streak_start, 
                                       state->streak_start + (state->streak_length * state->streak_count) - 1, 
                                       8, state->streak_length, 
                                       state->streak_count);
            newentry->start = state->streak_start;
            array_add(state->sections, newentry);
        }
        
        state->streak_length = length;
        state->streak_start = start;
        state->streak_count = 1;
    }
    state->prev_end = start + length;
}
#endif

size_t tasks_memmap_dial_interface(char *filename, char *dest, size_t maxsize) {
    int taskid = atoi(filename);
    if (!task_exists(taskid)) return snprintf(dest, maxsize, "Task %d does not exist!\n", taskid);
    
    Task *task = get_task(taskid);
    if (!task->debug_info->valid) return snprintf(dest, maxsize, "Task %d has invalid debug informations!\n", taskid);
    
    memmap_sprintf_state state = {
        .task = task,
        .prev_end = 0,
        .streak_count = 0,
        .streak_length = 0,
        .streak_start = 0,
    };
    array_new(&(state.sections));
    
    for (size_t e=0; e<task->debug_info->mem_map_length; e++) {
        TaskSection *section = task->debug_info->mem_map + e;
        memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, "    0x%08x - 0x%08x  [ %*lu B ]  %s\n", 
                                                       (unsigned)section->start, 
                                                       (unsigned)section->start + (unsigned)section->length - 1,
                                                       8, section->length,
                                                       section->name);
        newentry->start = (unsigned)section->start;
        array_add(state.sections, newentry);
    }
    apply_to_memareas_by_tag(task->owner_id, &applied_to_heap_memareas, (void*)&state);
    
#ifdef TRACK_MEMORY
    if (state.streak_count == 1) {
        memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                  "    0x%08x - 0x%08x  [ %*u B ]  HEAP chunk from %s:%u\n", 
                                  state.streak_start, state.streak_start + state.streak_length - 1, 8, state.streak_length, state.filename, state.linenum);
        newentry->start = state.streak_start;
        array_add(state.sections, newentry);
    } else if (state.streak_count > 1) {
        memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                  "    0x%08x - 0x%08x  [ %*u B x %lu ]  HEAP chunks from %s:%u\n", 
                                  state.streak_start, state.streak_start + state.streak_length * state.streak_count - 1, 8, state.streak_count, state.streak_length, state.filename, state.linenum);
        newentry->start = state.streak_start;
        array_add(state.sections, newentry);
    }
#else
    if (state.streak_count == 1) {
        memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                  "    0x%08x - 0x%08x  [ %*u B ]  HEAP dynamic chunk\n", 
                                  state.streak_start, state.streak_start + state.streak_length - 1, 8, state.streak_length);
        newentry->start = state.streak_start;
        array_add(state.sections, newentry);
    } else if (state.streak_count > 1) {
        memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                  "    0x%08x - 0x%08x  [ %*u B x %lu ]  HEAP dynamic chunks\n", 
                                  state.streak_start, state.streak_start + state.streak_length * state.streak_count - 1, 8, state.streak_count, state.streak_length);
        newentry->start = state.streak_start;
        array_add(state.sections, newentry);
    }
#endif
    
    array_sort(state.sections, &compare_memmap_printsection_entry);
    
    size_t written = 0;
    written += snprintf(dest + written, maxsize - written, "Memory map for task %d (program %s):\n", taskid, task->name);
    
    ARRAY_FOREACH(curr, state.sections, {
        memmap_printsection_entry *entry = (memmap_printsection_entry*) curr;
        written += snprintf(dest + written, maxsize - written, "%s", entry->line);
        free(entry);
    });
    
    array_destroy(state.sections);
    
    return written;
}


static void extend_printsection_array(Array *printsection_array, void (*tasksection_populator)(Array*)) {
    Array *new_sections;
    array_new(&(new_sections));
    tasksection_populator(new_sections);
    ARRAY_FOREACH(curr, new_sections, {
        TaskSection *section = (TaskSection*) curr;
        memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, "    0x%08x - 0x%08x  [ %*lu B ]  %s\n", 
                                                       (unsigned)section->start, 
                                                       (unsigned)section->start + (unsigned)section->length - 1,
                                                       8, section->length,
                                                       section->name);
        newentry->start = (unsigned)section->start;
        array_add(printsection_array, newentry);
        free(section);
    });
    array_destroy(new_sections);
}


size_t kernel_memmap_dial_interface(char *filename, char *dest, size_t maxsize) {
    bool nodynamic = endswith(filename, ".nodynamic");
    memmap_sprintf_state state = {
        .task = NULL,
        .prev_end = 0,
        .streak_count = 0,
        .streak_length = 0,
        .streak_start = 0,
    };
    array_new(&(state.sections));
    
    // add kernel elf sections
    for (size_t e=0; e<kernel_mem_map.num_elf_mmaps; e++) {
        TaskSection *section = kernel_mem_map.elf_mmaps + e;
        memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, "    0x%08x - 0x%08x  [ %*lu B ]  %s\n", 
                                                       (unsigned)section->start, 
                                                       (unsigned)section->start + (unsigned)section->length - 1,
                                                       8, section->length,
                                                       section->name);
        newentry->start = (unsigned)section->start;
        array_add(state.sections, newentry);
    }
    
    // add sections belonging to the bios and such
    extend_printsection_array(state.sections, &kernel_internal_get_bios_areas);
    
    // add sections belonging to the video subsystem (framebuffer and such)
    extend_printsection_array(state.sections, REFERENCE_IMPL(video, terminal, getbuffers));
    
    // add hard memory limits
    {
        memmap_printsection_entry* newentry;
        
        unsigned long kheap_start = ((unsigned long)kernel_heap_start);
        newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, "    0x%08lx               Begin of allocable memory\n", kheap_start);
        newentry->start = (unsigned)kheap_start;
        array_add(state.sections, newentry);
        
        unsigned long kheap_end = ((unsigned long)kernel_heap_start) + kernel_heap_length;
        newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, "                 0x%08lx  End of allocable memory\n", kheap_end-1);
        newentry->start = (unsigned)kheap_end;
        array_add(state.sections, newentry);
        
        unsigned long mem_end = get_memory_end();
        newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
        snprintf(newentry->line, PRINTSECTION_MAXLINE, "                 0x%08lx  End of physical memory\n", mem_end-1);
        newentry->start = (unsigned)mem_end;
        array_add(state.sections, newentry);
        
    }
    
    // add special ACPI sections
    extend_printsection_array(state.sections, &acpi_internal_getsections);
    
    if (!nodynamic) {
        apply_to_memareas_by_tag(0, &applied_to_heap_memareas, (void*)&state);
        
#ifdef TRACK_MEMORY
        if (state.streak_count == 1) {
            memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
            snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                    "    0x%08x - 0x%08x  [ %*u B ]  HEAP chunk from %s:%u\n", 
                                    state.streak_start, state.streak_start + state.streak_length - 1, 8, state.streak_length, state.filename, state.linenum);
            newentry->start = state.streak_start;
            array_add(state.sections, newentry);
        } else if (state.streak_count > 1) {
            memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
            snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                    "    0x%08x - 0x%08x  [ %*u B x %lu ]  HEAP chunks from %s:%u\n", 
                                    state.streak_start, state.streak_start + state.streak_length * state.streak_count - 1, 8, state.streak_count, state.streak_length, state.filename, state.linenum);
            newentry->start = state.streak_start;
            array_add(state.sections, newentry);
        }
#else
        if (state.streak_count == 1) {
            memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
            snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                    "    0x%08x - 0x%08x  [ %*u B ]  HEAP dynamic chunk\n", 
                                    state.streak_start, state.streak_start + state.streak_length - 1, 8, state.streak_length);
            newentry->start = state.streak_start;
            array_add(state.sections, newentry);
        } else if (state.streak_count > 1) {
            memmap_printsection_entry* newentry = (memmap_printsection_entry*) malloc(sizeof(memmap_printsection_entry));
            snprintf(newentry->line, PRINTSECTION_MAXLINE, 
                                    "    0x%08x - 0x%08x  [ %*u B x %lu ]  HEAP dynamic chunks\n", 
                                    state.streak_start, state.streak_start + state.streak_length * state.streak_count - 1, 8, state.streak_count, state.streak_length);
            newentry->start = state.streak_start;
            array_add(state.sections, newentry);
        }
#endif
    }
    
    array_sort(state.sections, &compare_memmap_printsection_entry);
    
    size_t written = 0;
    written += snprintf(dest + written, maxsize - written, "Memory map for the Moonrabbit OS kernel%s:\n", (nodynamic ? " (excluding dynamic-allocated chunks)" : ""));
    
    ARRAY_FOREACH(curr, state.sections, {
        memmap_printsection_entry *entry = (memmap_printsection_entry*) curr;
        written += snprintf(dest + written, maxsize - written, "%s", entry->line);
        free(entry);
    });
    
    array_destroy(state.sections);
    
    return written;
}
