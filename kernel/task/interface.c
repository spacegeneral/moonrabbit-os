#include <task/interface.h>
#include <task/task.h>

int task_readylevel_internal = 0;

static int task_ready_level() {
    return task_readylevel_internal;
}

static void task_initialize() {
    setup_multitasking_system();
    task_readylevel_internal = 50;
}

static void task_finalize() {
    activate_multitasking_system();
    task_readylevel_internal = 100;
}

static void task_teardown() {
    task_readylevel_internal = 0;
}


UNIQUE_IMPLEMENTATION_OF(task, tasking_subsystem, { \
    .ready_level = &task_ready_level, \
    .initialize = &task_initialize, \
    .finalize = &task_finalize, \
    .teardown = &task_teardown \
})

// Run finalize() to jump to userspace (ring 3)
