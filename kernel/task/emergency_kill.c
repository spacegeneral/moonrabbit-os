#include <task/task.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/proc.h>


void emergency_current_task_kill() {
    Task *troubled_task = get_current_task();
    unsigned int next_task = troubled_task->parent_task;
    printf("Emergency kill sequence!!\n");
    printf("The task %d will be terminated, and control transferred to its parent %d\n", troubled_task->task_id, next_task);
    
    if (troubled_task->return_code_here != NULL)
        *(troubled_task->return_code_here) = TASK_KILLED_BECAUSE_EXCEPTION;
    
    purge_task(troubled_task->task_id);
    
    iswitch_task(next_task);
}

