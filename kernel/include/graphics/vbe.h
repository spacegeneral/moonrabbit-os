#ifndef _GRAPHICS_VBE_H
#define _GRAPHICS_VBE_H

#include <stdint.h>
#include <stddef.h>
#include <graphics.h>
#include <kernel/rice.h>

extern unsigned char* screen;
extern size_t vbe_width;
extern size_t vbe_height;
extern size_t vbe_pitch;
extern size_t vbe_bpp;
extern int vbe_scroll_top;
extern int vbe_scroll_bottom;
extern int vbe_writing;
extern unsigned int vbe_term_flags;


#define FRAMEBUFFER_SIZE (vbe_height * vbe_pitch)


inline void vbe_putpixel(int x, int y, int color) {
    unsigned char *pixel = screen + y*vbe_pitch + x*vbe_bpp;
    pixel[0] = color & 255;          // BLUE
    pixel[1] = (color >> 8) & 255;   // GREEN
    pixel[2] = (color >> 16) & 255;  // RED
}

inline void vbe_putpixel_r(int x, int y, int color, unsigned char *dest) {
    unsigned char *pixel = dest + y*vbe_pitch + x*vbe_bpp;
    pixel[0] = color & 255;          // BLUE
    pixel[1] = (color >> 8) & 255;   // GREEN
    pixel[2] = (color >> 16) & 255;  // RED
}

inline void vbe_putpixel_quick(unsigned char *pixel, int color) {
    pixel[0] = color & 255;          // BLUE
    pixel[1] = (color >> 8) & 255;   // GREEN
    pixel[2] = (color >> 16) & 255;  // RED
}

inline int vbe_getpixel(int x, int y) {
    unsigned char *pixel = screen + y*vbe_pitch + x*vbe_bpp;
    return ((int)pixel[0]) | (((int)pixel[1]) << 8) | (((int)pixel[2]) << 16);
}

inline int vbe_getpixel_quick(unsigned char *pixel) {
    return ((int)pixel[0]) | (((int)pixel[1]) << 8) | (((int)pixel[2]) << 16);
}

void vbe_init_textmode();
void vbe_swap_buffer();
void vbe_scroll_up(size_t numpixels, int fillcolor);
void vbe_scroll_down(size_t numpixels, int fillcolor);
void vbe_scroll_left(size_t numpixels, int fillcolor);
void vbe_scroll_right(size_t numpixels, int fillcolor);
void vbe_cls(int color);

int vbe_draw_codepoint(size_t col, size_t row, int codepoint, int color);
void vbe_fill_rect(Rect *rect, int fillcolor);

void vbe_clean_cursor();
void vbe_take_cursor_patch();
void vbe_update_cursor(int x, int y);


// Interface

void vbe_terminal_putchar(char c);
void vbe_terminal_cls();
void vbe_terminal_scroll_down();
void vbe_terminal_gotoxy(size_t x, size_t y);
void vbe_terminal_set_colors(int foreground, int background);
void vbe_terminal_set_scroll_range(int scroll_top, int scroll_bottom);
int vbe_terminal_direct_video_command(int verb, int argument, void *result, unsigned int agent);
TermInfo vbe_terminal_getinfo();
void vbe_terminal_internal_getbuffers(Array *append_sections_here);
void vbe_terminal_enable_cursor();
void vbe_terminal_disable_cursor();
void vbe_terminal_update_cursor(int x, int y);
void vbe_terminal_initialize(void *video_memory, size_t width, size_t height, size_t bpp, size_t pitch);
void vbe_terminal_finalize();
void vbe_terminal_implementation_specific_finalize();
void vbe_terminal_teardown();
int vbe_terminal_ready_level();
void vbe_set_writing_direction(WritingDirection writing);
void vbe_set_term_flags(unsigned int flags);
void vbe_display_bitmap(Bitmap *bitmap, int colorkey);

extern int vbe_terminal_readylevel_internal;

extern EMPTY_IMPLEMENTATION(vbe, terminal)


#endif
