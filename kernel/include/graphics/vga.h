#ifndef _GRAPHICS_VGA_H
#define _GRAPHICS_VGA_H

#include <stdint.h>
#include <graphics.h>

extern int vga_width;
extern int vga_height;
extern size_t vga_cursor_x;
extern size_t vga_cursor_y;
extern int vga_scroll_top;
extern int vga_scroll_bottom;
extern bool vga_cursor_enabled;
extern int vga_writing;
extern unsigned int vga_term_flags;

enum vga_color {
    VGA_COLOR_BLACK = 0,
    VGA_COLOR_BLUE = 1,
    VGA_COLOR_GREEN = 2,
    VGA_COLOR_CYAN = 3,
    VGA_COLOR_RED = 4,
    VGA_COLOR_MAGENTA = 5,
    VGA_COLOR_BROWN = 6,
    VGA_COLOR_LIGHT_GREY = 7,
    VGA_COLOR_DARK_GREY = 8,
    VGA_COLOR_LIGHT_BLUE = 9,
    VGA_COLOR_LIGHT_GREEN = 10,
    VGA_COLOR_LIGHT_CYAN = 11,
    VGA_COLOR_LIGHT_RED = 12,
    VGA_COLOR_LIGHT_MAGENTA = 13,
    VGA_COLOR_LIGHT_BROWN = 14,
    VGA_COLOR_WHITE = 15,
    NUM_VGA_COLORS
};

static __attribute__((unused)) const int vga_to_24bpp_map[NUM_VGA_COLORS] = {
    0x00000000,
    0x000000AA,
    0x0000AA00,
    0x0000AAAA,
    0x00AA0000,
    0x00AA00AA,
    0x00AA5500,
    0x00FFFFFF,
    0x00555555,
    0x005555FF,
    0x0055FF55,
    0x0055FFFF,
    0x00FF5555,
    0x00FF55FF,
    0x00FFFF55,
    0x00FFFFFF,
};

static inline uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg) {
	return fg | bg << 4;
}

static inline uint16_t vga_entry(unsigned char uc, uint8_t color) {
	return (uint16_t) uc | (uint16_t) color << 8;
}

void vga_cls(uint8_t color);


// Interface

void vga_terminal_putchar(char c);
void vga_terminal_cls();
void vga_terminal_scrolldown();
void vga_terminal_gotoxy(size_t x, size_t y);
void vga_terminal_set_colors(int foreground, int background);
void vga_terminal_set_scroll_range(int scroll_top, int scroll_bottom);
int vga_terminal_direct_video_command(int verb, int argument, void *result, unsigned int agent);
TermInfo vga_terminal_getinfo();
void vga_terminal_internal_getbuffers(Array *append_sections_here);
void vga_terminal_enable_cursor();
void vga_terminal_disable_cursor();
void vga_terminal_update_cursor(int x, int y);
void vga_terminal_initialize(void *video_memory, size_t width, size_t height, size_t bpp, size_t pitch);
void vga_terminal_finalize();
void vga_terminal_implementation_specific_finalize();
void vga_terminal_teardown();
int vga_terminal_ready_level();
void vga_set_writing_direction(WritingDirection writing);
void vga_set_term_flags(unsigned int flags);
void vga_display_bitmap(Bitmap *bitmap, int colorkey);

extern int vga_terminal_readylevel_internal;


extern EMPTY_IMPLEMENTATION(vga, terminal)

#endif
