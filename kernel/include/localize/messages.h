#ifndef _LOCALIZE_MESSAGES_H
#define _LOCALIZE_MESSAGES_H

#include <localize/localize.h>


#define MSG_MEDITATING_IT "Meditazione...\n"
#define MSG_MEDITATING_EN "Meditating...\n"
#define MSG_MOUNTING_INIT_IT "Montaggio media di init\n"
#define MSG_MOUNTING_INIT_EN "Mounting init media\n"
#define MSG_INSERT_INIT_MEDIA_IT "Inserisci il media di init, poi premi invio..."
#define MSG_INSERT_INIT_MEDIA_EN "Insert the init media, then press enter..."
#define MSG_ERR_LOAD_CUSTOM_FONT_IT "Impossibile caricare il font speciale %s\nSaranno disponibili solo i caratteri ASCII di base.\n"
#define MSG_ERR_LOAD_CUSTOM_FONT_EN "Could not load custom font %s\nOnly basic ASCII available.\n"
#define MSG_DONE_BASIC_INIT_IT "Inizializzazione di base completata\n"
#define MSG_DONE_BASIC_INIT_EN "Done basic initialization\n"
#define MSG_DONE_ADVANCED_INIT_IT "inizializzazione avanzata completata\n"
#define MSG_DONE_ADVANCED_INIT_EN "Done advanced initialization\n"
#define MSG_ENTER_USER_MODE_IT "Ingresso in modalità utente\n"
#define MSG_ENTER_USER_MODE_EN "Entering user mode\n"
#define MSG_ERROR_LAUNCH_INIT_IT "Impossibile lanciare init: errore %d\n"
#define MSG_ERROR_LAUNCH_INIT_EN "Cannot launch init: error %d\n"
#define MSG_INITRD_MOUNT_SUCCESS_IT "%sMontato %s -> %s\n"
#define MSG_INITRD_MOUNT_SUCCESS_EN "%sMounted %s -> %s\n"
#define MSG_NO_INITRD_FOUND_IT "%sinitrd non trovato\n"
#define MSG_NO_INITRD_FOUND_EN "%sNo initrd found\n"
#define MSG_FAILED_MOUNT_INITIAL_FS_IT "%sMontaggio del filesystem iniziale fallito\n"
#define MSG_FAILED_MOUNT_INITIAL_FS_EN "%sFailed to mount initial filesystem\n"
#define MSG_INITRD_LINKAGE_FAILED_IT "%sLinkaggio fallito: %s -> %s (errore %d)\n"
#define MSG_INITRD_LINKAGE_FAILED_EN "%sFailed to link %s -> %s (error %d)\n"

#endif
