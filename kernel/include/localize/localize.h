#ifndef _LOCALIZE_LOCALIZE_H
#define _LOCALIZE_LOCALIZE_H

#include <i18n.h>


#define KBD_LOCALE_DEFAULT KEYMAP_US


#define MSGI18N(message) message##_##EN
//                                   ^^ change this (i.e. to IT)

#endif
