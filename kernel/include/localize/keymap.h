#ifndef _LOCALIZE_KEYMAP_H
#define _LOCALIZE_KEYMAP_H

#include <localize/localize.h>

#define UNPRINTABLE_BYTES ""
#define MAX_PRINTABLE_BYTE_SEQ 6
#define KEYCODE2PRINTABLE_MAP_SIZE 91


void switch_keyboard_mapping(KeymapLocale locale);


typedef struct FullKeycodeToPrintableMap {
    const char mod_none[KEYCODE2PRINTABLE_MAP_SIZE][MAX_PRINTABLE_BYTE_SEQ];
    const char mod_uppercase[KEYCODE2PRINTABLE_MAP_SIZE][MAX_PRINTABLE_BYTE_SEQ];
} FullKeycodeToPrintableMap;


extern const FullKeycodeToPrintableMap keycode2printable_maps[NUM_KEYMAP_LOCALES];


extern const FullKeycodeToPrintableMap *default_keycode2printable_map;


#endif
