#ifndef INTERFACES_H
#define INTERFACES_H

#include <oop/impl.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <mm/multiboot.h>
#include <interrupt/devices.h>
#include <ipc/serv.h>
#include <ipc/mq.h>
#include <sys/term.h>



/*
 * Interface design style guide:
 * 1. simple subsystem: methods "setup" and "teardown", with no parameters
 * 2. advanced subsystem: methods "activate" (takes parameters) and "teardown" (no parameters)
 * 3. complex subsystem: two-step activation split in "initialize" (takes parameters) and "finalize" (takes no parameters). Deactivation is done through "teardown" as usual.
 * 
 * 
 */


// Subsystem interfaces

/* The subsystem ready_level return value ranges between 0-100 inclusive
 * 
 * 
 */


DECL_INTERFACE(subsystem, \
    int (*ready_level) (void); \
    void (*setup) (void); \
    void (*teardown) (void); \
)

DECL_INTERFACE(interr_ctrl_subsystem, \
    int (*ready_level) (void); \
    void (*setup) (void); \
    void (*teardown) (void); \
)

DECL_INTERFACE(data_subsystem, \
    int (*ready_level) (void); \
    void (*initialize) (void); \
    void (*finalize) (void); \
    void (*teardown) (void); \
)

DECL_INTERFACE(tasking_subsystem, \
    int (*ready_level) (void); \
    void (*initialize) (void); \
    void (*finalize) (void); \
    void (*teardown) (void); \
)

DECL_INTERFACE(memory_subsystem, \
    int (*ready_level) (void); \
    void (*initialize) (uint32_t mboot_magic, multiboot_info_t* mboot_info); \
    void (*finalize) (void); \
    void (*teardown) (void); \
)

// Video terminal control & generic subsystem

DECL_INTERFACE(terminal, \
    int (*ready_level) (void); \
    void (*initialize) (void *video_memory, size_t width, size_t height, size_t bpp, size_t pitch); \
    void (*finalize) (void); \
    void (*teardown) (void); \
    void (*putchar)(char c); \
    void (*cls)(); \
    void (*scrolldown)(); \
    void (*gotoxy)(size_t x, size_t y); \
    void (*set_colors)(int foreground, int background); \
    void (*set_scroll_range)(int scroll_top, int scroll_bottom); \
    int (*direct_video_command)(int verb, int argument, void *result, unsigned int agent); \
    TermInfo (*getinfo)(); \
    void (*getbuffers)(Array *append_sections_here); \
    void (*enable_cursor)(); \
    void (*disable_cursor)(); \
    void (*update_cursor)(int x, int y); \
    void (*set_writing_direction)(WritingDirection writing); \
    void (*set_term_flags) (unsigned int flags); \
    void (*display_bitmap) (Bitmap *bitmap, int colorkey); \
)

#define SUBSYSTEM_READY(objname, subname) (CALL_IMPL(objname, subname, ready_level) == 100)

// Communication interfaces


DECL_INTERFACE(pubsub, \
    MQStatus (*advertise_channel) (char *topic_name, unsigned int max_queue_length, unsigned int message_length, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib, SecurityPolicy policy); \
    MQStatus (*retire_channel) (char *topic_name, unsigned int agent_id); \
    MQStatus (*subscribe) (char *topic_name, unsigned int task_id); \
    MQStatus (*unsubscribe) (char *topic_name, unsigned int task_id); \
    MQStatus (*kernel_subscribe) (char *topic_name, kernel_mq_subscriber callback); \
    MQStatus (*kernel_unsubscribe) (char *topic_name, kernel_mq_subscriber callback); \
    MQStatus (*publish) (char *topic_name, const void *message, unsigned int agent_id); \
    MQStatus (*publish_multi) (char *topic_name, const void *message, unsigned int agent_id, size_t num_messages, size_t *num_written); \
    MQStatus (*poll) (char *topic_name, unsigned int task_id, void *message); \
    MQStatus (*stats) (char *topic_name, ChannelStats *stats); \
)


DECL_INTERFACE(service, \
    ServStatus (*advertise_service) (char *topic_name, unsigned int frame_length, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib); \
    ServStatus (*retire_service) (char *topic_name, unsigned int agent_id); \
    ServStatus (*accept) (char *topic_name, void *request, unsigned int *requestor_task_id, unsigned int *request_uid, unsigned int acceptor_task_id); \
    ServStatus (*kernel_accept) (char *topic_name, kernel_serv_acceptor callback); \
    ServStatus (*server_complete) (char *topic_name, unsigned int request_uid, unsigned int agent_id); \
    ServStatus (*request) (char *topic_name, void *request, unsigned int *request_id, unsigned int requestor_task_id); \
    ServStatus (*poll_response) (char *topic_name, unsigned int request_id, unsigned int task_id); \
)


// Interrupt control

DECL_INTERFACE(interr_onoff, \
    bool (*enable_device)  (InterruptSourceDevice *device); \
    bool (*disable_device) (InterruptSourceDevice *device); \
)


#endif
