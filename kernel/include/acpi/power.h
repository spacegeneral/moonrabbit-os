#ifndef _ACPI_POWER_H
#define _ACPI_POWER_H


#include <stdint.h>


extern uint16_t SLP_TYPa;
extern uint16_t SLP_TYPb;


void init_acpi_power();
bool is_acpi_power_supported();
void acpi_power_off_system();


#endif  
