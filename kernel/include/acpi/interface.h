#ifndef _ACPI_INTERFACE_H
#define _ACPI_INTERFACE_H

#include <interfaces.h>

#define ACPI_READYLEVEL_BASIC 30
#define ACPI_READYLEVEL_POWER 60
#define ACPI_READYLEVEL_FULL 100


extern int acpi_readylevel_internal;


extern EMPTY_IMPLEMENTATION(acpi, subsystem)  // see acpi/interface.c


#endif  
