#ifndef _ACPI_TABLES_H
#define _ACPI_TABLES_H


#include <stdint.h>
#include <stddef.h>


#define ACPISDT_SIGNATURE_LENGTH 4


// table header
// all acpi tables start with this header
typedef struct __attribute__((packed)) ACPISDTHeader {
    char signature[ACPISDT_SIGNATURE_LENGTH];
    uint32_t length;
    uint8_t revision;
    uint8_t checksum;
    char oemid[6];
    char oem_table_id[8];
    uint32_t oem_revision;
    uint32_t creator_id;
    uint32_t creator_revision;
} ACPISDTHeader;


// GenericAddressStructure
typedef struct __attribute__((packed)) GenericAddressStructure {
  uint8_t AddressSpace;
  uint8_t BitWidth;
  uint8_t BitOffset;
  uint8_t AccessSize;
  uint64_t Address;
} GenericAddressStructure;


// Root System Description Table (RSDT)

typedef void* RSDT;  // since rsdt is a struct with a variable number of members, we'll just treat is as an opaque pointer

ACPISDTHeader *get_rsdt_header(RSDT table);
size_t get_rsdt_num_entries(RSDT table);
void *get_rsdt_entry(RSDT table, size_t entry_id);


void *get_sdt(char *signature);



// Fixed ACPI Description Table (FADT)

typedef struct FADT {
    ACPISDTHeader header;
    uint32_t FirmwareCtrl;
    uint32_t *dsdt;
 
    // field used in ACPI 1.0; no longer in use, for compatibility only
    uint8_t  Reserved;
 
    uint8_t  PreferredPowerManagementProfile;
    uint16_t SCI_Interrupt;
    uint32_t SMI_CommandPort;
    uint8_t  AcpiEnable;
    uint8_t  AcpiDisable;
    uint8_t  S4BIOS_REQ;
    uint8_t  PSTATE_Control;
    uint32_t PM1aEventBlock;
    uint32_t PM1bEventBlock;
    uint32_t PM1aControlBlock;
    uint32_t PM1bControlBlock;
    uint32_t PM2ControlBlock;
    uint32_t PMTimerBlock;
    uint32_t GPE0Block;
    uint32_t GPE1Block;
    uint8_t  PM1EventLength;
    uint8_t  PM1ControlLength;
    uint8_t  PM2ControlLength;
    uint8_t  PMTimerLength;
    uint8_t  GPE0Length;
    uint8_t  GPE1Length;
    uint8_t  GPE1Base;
    uint8_t  CStateControl;
    uint16_t WorstC2Latency;
    uint16_t WorstC3Latency;
    uint16_t FlushSize;
    uint16_t FlushStride;
    uint8_t  DutyOffset;
    uint8_t  DutyWidth;
    uint8_t  DayAlarm;
    uint8_t  MonthAlarm;
    uint8_t  Century;
 
    // reserved in ACPI 1.0; used since ACPI 2.0+
    uint16_t BootArchitectureFlags;
 
    uint8_t  Reserved2;
    uint32_t Flags;
 
    // 12 byte structure; see below for details
    GenericAddressStructure ResetReg;
 
    uint8_t  ResetValue;
    uint8_t  Reserved3[3];
 
    //TODO: acpi 2.0 support
} FADT;



#endif  
