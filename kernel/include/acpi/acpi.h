#ifndef _ACPI_ACPI_H
#define _ACPI_ACPI_H


#include <stdbool.h>
#include <acpi/rsdp.h>
#include <acpi/tables.h>
#include <collections/array.h>


void initialize_acpi();
bool enable_acpi();
bool is_acpi_supported();
void acpi_internal_getsections(Array *append_sections_here);


#endif  
