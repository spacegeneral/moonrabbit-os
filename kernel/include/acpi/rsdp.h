#ifndef _ACPI_RSDP_H
#define _ACPI_RSDP_H


#include <stdint.h>


typedef struct __attribute__ ((packed)) RSDPDescriptor10 {
    char signature[8];
    uint8_t checksum;
    char oemid[6];
    uint8_t revision;
    uint32_t rsdt_address;
} RSDPDescriptor10;


typedef struct __attribute__ ((packed)) RSDPDescriptor {
    RSDPDescriptor10 first_part;

    uint32_t length;
    uint64_t xsdt_address;
    uint8_t extended_checksum;
    uint8_t reserved[3];
} RSDPDescriptor;


extern RSDPDescriptor *rsdp;


#endif  
