#ifndef _DATA_PUREVIRTUAL_H
#define _DATA_PUREVIRTUAL_H

#include <enumtricks.h>
#include <stdbool.h>
#include <stdint.h>
#include <collections/array.h>
#include <data/vfs.h>


/*
 *  RAM-only file
 */


/*
 *  Useful notes:
 *  The file_write primitive must "bump up" the file size if needed,
 *  i.e. size_after_write = seek_pos + bytes_written
 *  if (seek_pos + bytes_written) < size_before_write, the file
 *  size will remain unchanged.
 *  If the bumping process leaves an initial "empty space" (between the offset
 *  of the old end-of-file and the seek_pos), then the content of
 *  that area is unspecified. TODO maybe 0 it out for security
 *  
 */


typedef struct PureVirtualFileAccessData {
    uint8_t *content;
    uint64_t size;
} PureVirtualFileAccessData;


typedef struct PureVirtualDirectoryAccessData {
    Array *children;
} PureVirtualDirectoryAccessData;


typedef struct PureVirtualLinkAccessData {
    char *dest_name;
} PureVirtualLinkAccessData;


extern bool virtualfs_create_directory(FileDescriptor **fd, char *name, FileMetaData meta);
extern bool virtualfs_create_file(FileDescriptor **fd, char *name, FileMetaData meta);
extern bool virtualfs_create_link(FileDescriptor **fd, char *name, FileMetaData meta);

extern bool virtualfs_insert_into_directory(FileDescriptor *dest_dir, FileDescriptor *src_file);
extern bool virtualfs_remove_file(FileDescriptor **dead_file);
extern bool virtualfs_unlist_file(FileDescriptor *dest_dir, FileDescriptor *dead_file);
extern bool virtualfs_remove_empty_directory(FileDescriptor **dead_file);
extern bool virtualfs_retarget_link(FileDescriptor *link, const char *destination_path);

extern size_t virtualfs_impl_read_file(void *dest, size_t num_bytes, size_t seek, FileDescriptor *file);
extern size_t virtualfs_impl_write_file(const void *src, size_t num_bytes, size_t seek, FileDescriptor *file);
extern size_t virtualfs_impl_get_filesize(FileDescriptor *file);

extern bool virtualfs_alist_directory(FileDescriptor *dir, FileDescriptor ***content, size_t *num_files);
extern bool virtualfs_get_child(FileDescriptor *dir, char *name, FileDescriptor **child);
extern bool virtualfs_aget_link_dest(FileDescriptor *link, char **dest_str);

extern bool virtualfs_change_metadata(FileDescriptor* fd, FileMetaData newmeta);


static const FSprimitives virtual_fs_primitives = {
    .create_directory = &virtualfs_create_directory,
    .create_file = &virtualfs_create_file,
    .create_link = &virtualfs_create_link,
    
    .insert_into_directory = &virtualfs_insert_into_directory,
    .remove_file = &virtualfs_remove_file,
    .unlist_file = &virtualfs_unlist_file,
    .remove_empty_directory = &virtualfs_remove_empty_directory,
    .retarget_link = &virtualfs_retarget_link,
    
    .impl_read_file = &virtualfs_impl_read_file,
    .impl_write_file = &virtualfs_impl_write_file,
    .impl_get_filesize = &virtualfs_impl_get_filesize,
    
    .alist_directory = &virtualfs_alist_directory,
    .get_child = &virtualfs_get_child,
    .aget_link_dest = &virtualfs_aget_link_dest,
    
    .change_metadata = &virtualfs_change_metadata
};


#endif 
