#ifndef _DATA_NOPFS_H
#define _DATA_NOPFS_H

#include <stdbool.h>
#include <stdint.h>
#include <data/vfs.h>


/*
 * FSprimitives stub wich does absolutly nothing
 */


extern bool nopfs_create_directory(FileDescriptor **fd, char *name, FileMetaData meta);
extern bool nopfs_create_file(FileDescriptor **fd, char *name, FileMetaData meta);
extern bool nopfs_create_link(FileDescriptor **fd, char *name, FileMetaData meta);

extern bool nopfs_insert_into_directory(FileDescriptor *dest_dir, FileDescriptor *src_file);
extern bool nopfs_remove_file(FileDescriptor **dead_file);
extern bool nopfs_unlist_file(FileDescriptor *dest_dir, FileDescriptor *dead_file);
extern bool nopfs_remove_empty_directory(FileDescriptor **dead_file);
extern bool nopfs_retarget_link(FileDescriptor *link, const char *destination_path);

extern size_t nopfs_impl_read_file(void *dest, size_t num_bytes, size_t seek, FileDescriptor *file);
extern size_t nopfs_impl_write_file(const void *src, size_t num_bytes, size_t seek, FileDescriptor *file);
extern size_t nopfs_impl_get_filesize(FileDescriptor *file);

extern bool nopfs_alist_directory(FileDescriptor *dir, FileDescriptor ***content, size_t *num_files);
extern bool nopfs_get_child(FileDescriptor *dir, char *name, FileDescriptor **child);
extern bool nopfs_aget_link_dest(FileDescriptor *link, char **dest_str);

extern bool nopfs_change_metadata(FileDescriptor* fd, FileMetaData newmeta);


static const FSprimitives nop_fs_primitives = {
    .create_directory = &nopfs_create_directory,
    .create_file = &nopfs_create_file,
    .create_link = &nopfs_create_link,
    
    .insert_into_directory = &nopfs_insert_into_directory,
    .remove_file = &nopfs_remove_file,
    .unlist_file = &nopfs_unlist_file,
    .remove_empty_directory = &nopfs_remove_empty_directory,
    .retarget_link = &nopfs_retarget_link,
    
    .impl_read_file = &nopfs_impl_read_file,
    .impl_write_file = &nopfs_impl_write_file,
    .impl_get_filesize = &nopfs_impl_get_filesize,
    
    .alist_directory = &nopfs_alist_directory,
    .get_child = &nopfs_get_child,
    .aget_link_dest = &nopfs_aget_link_dest,
    
    .change_metadata = &nopfs_change_metadata
};


#endif 
