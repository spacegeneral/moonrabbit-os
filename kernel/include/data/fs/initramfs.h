#ifndef _DATA_FS_INITRAMFS_H
#define _DATA_FS_INITRAMFS_H 

#include <stdint.h>

#define INITRAMFS_MAGIC 0xCABAAD10


/*
 * Initramfs structure:
 * 
 * [InitramfsHeader]
 * 
 * [Directory
 *   (array of FileDescriptor) 
 * ]
 * 
 * [Data
 *   for a file:
 *      uint32_t compressed size
 *      uint32_t uncompressed size
 *      [array of bytes] zlib compressed payload
 *   for a link:
 *      uint32_t strlen of link string
 *      [array of bytes] link string
 *   for a directory:
 *      uint32_t number of children
 *      [array of uint32_t] for each children, its offset in the Directory
 * ]
 */


typedef struct InitramfsHeader {
    uint32_t magic;
    uint32_t version;
    uint32_t num_entries;
    uint32_t future_use[125];
} InitramfsHeader;


typedef struct InitramfsFileAccessData {
    uint32_t size;
    uint8_t *content;
} InitramfsAccessData;


typedef struct InitramfsDirectoryAccessData {
    uint32_t num_entries;
    uint32_t *child_id;
} InitramfsDirectoryAccessData;


typedef struct InitramfsLinkAccessData {
    uint32_t name_length;
    char *dest_name;
} InitramfsLinkAccessData;


FileDescriptor *build_vfs_from_file(FILE *fp);
bool initramfs_get_root_mountpoint(FileDescriptor **root, 
                                   char *fs_file_path, 
                                   FileMetaData metadata);


#endif
