#ifndef _DATA_FS_INTERNALFS_H
#define _DATA_FS_INTERNALFS_H 

#include <stdint.h> 
#include <data/vfs.h>
#include <data/path.h>


extern bool internalfs_mount(FileDescriptor **root, char *fs_file_path, FileMetaData metadata);

extern void internalfs_create_file(IdentifiedAgent *agent, const char *path, int *result);
extern void internalfs_create_directory(IdentifiedAgent *agent, const char *path, int *result);
extern void internalfs_create_link(IdentifiedAgent *agent, const char *path_from, const char *path_to, int *result);

extern void internalfs_remove(IdentifiedAgent *agent, const char *path, int *result);
extern void internalfs_list_directory(IdentifiedAgent *agent, const char *path, DirEnt **entries, size_t *num_files, int *result);
extern void internalfs_count_directory_entries(IdentifiedAgent* agent, const char* path, size_t* num_files, int* result);
extern void internalfs_get_directory_entry_at(IdentifiedAgent* agent, const char* path, size_t position, DirEnt* entry, int* result);
extern void internalfs_file_exists(IdentifiedAgent *agent, const char *path, int *result);

extern void internalfs_move(IdentifiedAgent* agent, const char* path_from, const char* path_to, int* result);

extern void internalfs_fopen(IdentifiedAgent *agent, const char *path, const char *mode, FILE *file, int *status);
extern void internalfs_fclose(IdentifiedAgent *agent, FILE *file, int *result);
extern void internalfs_fflush(IdentifiedAgent *agent, FILE *file, int *result);
extern void internalfs_fread(IdentifiedAgent *agent, void *dest, size_t size, size_t nmemb, FILE *file, size_t *result);
extern void internalfs_fwrite(IdentifiedAgent *agent, const void *src, size_t size, size_t nmemb, FILE *file, size_t *result);
extern void internalfs_fseek(IdentifiedAgent *agent, FILE *stream, long offset, int whence, int *result);

extern void internalfs_chsec(IdentifiedAgent* agent, const char* path, int newrace, int newclass, int* result);


static const FScomposites internalfs_composites = {
    .mount = &internalfs_mount,
    
    .create_file = &internalfs_create_file,
    .create_directory = &internalfs_create_directory,
    .create_link = &internalfs_create_link,
    
    .remove = &internalfs_remove,
    .list_directory = &internalfs_list_directory,
    .count_directory_entries = &internalfs_count_directory_entries,
    .get_directory_entry_at = &internalfs_get_directory_entry_at,
    
    .file_exists = &internalfs_file_exists,
    
    .move = &internalfs_move,
    
    .fopen = &internalfs_fopen,
    .fclose = &internalfs_fclose,
    .fflush = &internalfs_fflush,
    .fread = &internalfs_fread,
    .fwrite = &internalfs_fwrite,
    .fseek = &internalfs_fseek,
    
    .chsec = &internalfs_chsec,

};


#endif
