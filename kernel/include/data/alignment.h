#ifndef _DATA_ALIGNMENT_H
#define _DATA_ALIGNMENT_H


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>



/*
 Any data transfer to/from a chunk-based medium can be split into logical regions:
 (note: sector, chunk and block are used interchangeably here to mean basically LBA)
 
   chnk 0  chnk 1  chnk 2  chnk 3  chnk 4
  
 0       4       8      12      16      20
 |       |       |       |       |       |
 |   |   |   |   |   |   |   |   |   |   |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                         |
     |<----------------------->|
          transferred bytes
 
                ][
                \/
                
                
  first                    last
  chunk                    chunk
 
 0       4       8      12      16      20
 |       |       |       |       |       |
 |   |   |   |   |   |   |   |   |   |   |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |   |               |     |
     |<->|<------------->|<--->|       
        ^        ^           ^
 |<->|  :        :           :
   ^    :   core aligned     :
   :    :                    :
   :    head unaligned       tail unaligned
   :
   (head unaligned offset)
   
   
 Conventions in describing the IO frame:
 
 having head_unaligned_bytes_length > 0 indicates the presence
        of a head region, which must handled carefully
 having core_aligned_bytes_length > 0 indicates the presence
        of a core region, which can be transferred in bulk
 having tail_unaligned_bytes_length > 0 indicates the presence
        of a tail region, which must handled carefully
 
 if present, the core starts at chunk first_sector+1
 if present, the head resides in chunk first_sector
 if present, the tail resides in chunk last_sector
 
 
 
 Special notes: when writing head and tail, the respective
 chunks must be read first, partially overwritten, and then
 written.
 
 Note: when a transfer operation involves a data region which
 fits entirely within a chunk (but has length less than the chunk
 length), by convention we assign 
 core_aligned_bytes_length = 0, 
 tail_unaligned_bytes_length = 0,
 and we put offset and length of our tiny region in 
 head_unaligned_offset_in_sector and head_unaligned_bytes_length
 
 */




#define DECL_ALIGN_IO_FRAME(datatype) \
typedef struct AlignedIOFrame_Addr_##datatype { \
    datatype start_address; \
    datatype data_length; \
    datatype sector_length; \
     \
    datatype first_sector; \
    datatype last_sector; \
    datatype sector_count; \
     \
    datatype _temp; \
     \
    datatype head_unaligned_offset_in_sector; \
    datatype head_unaligned_bytes_length; \
    datatype core_aligned_sector_start; \
    datatype core_aligned_bytes_length; \
    datatype tail_unaligned_bytes_length; \
     \
} AlignedIOFrame_Addr_##datatype \

#define TYPEOF_IO_FRAME(datatype) AlignedIOFrame_Addr_##datatype

#define DECL_ALIGN_IO_FRAME_INITIALIZER(datatype) void init_ioframe_##datatype(AlignedIOFrame_Addr_##datatype *frame, datatype start_address, datatype data_length, datatype sector_length)

#define INITIALIZE_IO_FRAME(datatype, frame, start_address, data_length, sector_length) init_ioframe_##datatype(frame, start_address, data_length, sector_length)



DECL_ALIGN_IO_FRAME(uint64_t);
DECL_ALIGN_IO_FRAME_INITIALIZER(uint64_t);



#endif
