#ifndef _DATA_FSDIAL_H
#define _DATA_FSDIAL_H

#include <enumtricks.h>
#include <stdbool.h>
#include <stdint.h>
#include <data/vfs.h>
#include <data/nopfs.h>

#define MAX_DIAL_READBLOCK (1024*1024*2)  // 2MB

typedef size_t (*dial_reading_callback)(char *filename, char *dest, size_t maxsize);


extern bool dialfs_create_file(FileDescriptor **fd, char *name, FileMetaData meta);

extern size_t dialfs_impl_read_file(void *dest, size_t num_bytes, size_t seek, FileDescriptor *file);
extern size_t dialfs_impl_write_file(const void *src, size_t num_bytes, size_t seek, FileDescriptor *file);
extern size_t dialfs_impl_get_filesize(FileDescriptor *file);


static const FSprimitives dial_fs_primitives = {
    .create_directory = &nopfs_create_directory,
    .create_file = &dialfs_create_file,
    .create_link = &nopfs_create_link,
    
    .insert_into_directory = &nopfs_insert_into_directory,
    .remove_file = &nopfs_remove_file,
    .unlist_file = &nopfs_unlist_file,
    .remove_empty_directory = &nopfs_remove_empty_directory,
    .retarget_link = &nopfs_retarget_link,
    
    .impl_read_file = &dialfs_impl_read_file,
    .impl_write_file = &dialfs_impl_write_file,
    .impl_get_filesize = &dialfs_impl_get_filesize,
    
    .alist_directory = &nopfs_alist_directory,
    .get_child = &nopfs_get_child,
    .aget_link_dest = &nopfs_aget_link_dest
};

extern void dial_bind_callback(FileDescriptor *fd, dial_reading_callback callback);

#endif 
