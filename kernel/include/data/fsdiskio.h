#ifndef _DATA_FSDISKIO_H
#define _DATA_FSDISKIO_H

#include <enumtricks.h>
#include <stdbool.h>
#include <stdint.h>
#include <data/vfs.h>
#include <data/nopfs.h>
#include <data/virtual_file.h>


typedef struct FSDiskIOFileAccessData {
    unsigned int diskid;
    uint64_t offset;
    uint64_t disksize;
} FSDiskIOFileAccessData;


extern bool diskiofs_create_file(FileDescriptor **fd, char *name, FileMetaData meta);
extern bool diskiofs_remove_file(FileDescriptor **dead_file);

extern size_t diskiofs_impl_read_file(void *dest, size_t num_bytes, size_t seek, FileDescriptor *file);
extern size_t diskiofs_impl_write_file(const void *src, size_t num_bytes, size_t seek, FileDescriptor *file);
extern size_t diskiofs_impl_get_filesize(FileDescriptor *file);


static const FSprimitives diskio_fs_primitives = {
    .create_directory = &nopfs_create_directory,
    .create_file = &diskiofs_create_file,
    .create_link = &nopfs_create_link,
    
    .insert_into_directory = &nopfs_insert_into_directory,
    .remove_file = &diskiofs_remove_file,
    .unlist_file = &nopfs_unlist_file,
    .remove_empty_directory = &nopfs_remove_empty_directory,
    .retarget_link = &nopfs_retarget_link,
    
    .impl_read_file = &diskiofs_impl_read_file,
    .impl_write_file = &diskiofs_impl_write_file,
    .impl_get_filesize = &diskiofs_impl_get_filesize,
    
    .alist_directory = &nopfs_alist_directory,
    .get_child = &nopfs_get_child,
    .aget_link_dest = &nopfs_aget_link_dest
};

extern void diskio_bind_file(FileDescriptor *fd, unsigned int diskid, uint64_t offset, uint64_t size);

#endif 
