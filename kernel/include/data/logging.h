#ifndef _DATA_LOGGING_H
#define _DATA_LOGGING_H

#define KLOGFILE_NAME "kern.log"
#define KLOGFILE_FOLDER "f,sys"
#define KLOGFILE "f,sys,kern.log"
#define MAXLOGLINE 4096

#include <stdbool.h>

extern bool logging_system_ready;

void klog(char *fmt, ...);

void setup_klog_system();


#endif
