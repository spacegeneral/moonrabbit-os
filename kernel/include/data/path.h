#ifndef _DATA_PATH_H
#define _DATA_PATH_H

#include <enumtricks.h>
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <data/vfs.h>
#include <data/security.h>
#include <sys/fs.h>


/*
 * Set of helper functions and constants used to deal with
 * path manipulation within the kernel
 */


#define PARENT_DIR_NAME ".."
#define OS_DISK_PATH  "f,metal,disk"
#define OS_PART_PATH  "f,metal,part"
#define OS_MOUNT_PATH "f,fs"
#define OS_DEFAULT_FS_PATH "f,def"
#define OS_DIALS_H_PATH "f,sys,dial,human"
#define OS_DIALS_MEMMAPS_PATH "f,sys,dial,human,maps"
#define OS_DIALS_R_PATH "f,sys,dial,robot"
#define BITMAP_FONT_PATH "f,def,init,osdata,unifont.lbf"
#define SHELL_INITIAL_WORK_DIR "f"

// generate the full path from the root to a specified FileDescriptor
// the full path will be stored on the heap. It is responsibility of
// the called to deallocate it.
bool afull_file_path(FileDescriptor *file, char **dest_str);

// finds the FileDescriptor pointer matching a given path
bool resolve_path(const char *path, FileDescriptor **to_file);
// the deep variant also checks if to_file is a link, and resolves it.
// This is the default behaviour for resolve_mount and resolve_mount_explicit_path
// NOTE: at most 1 level of "tail link" is resolved
bool resolve_path_deep(const char *path, FileDescriptor **to_file);
bool resolve_mount(const char *orig_path, FileDescriptor **to_mount);
bool resolve_mount_explicit_path(const char *orig_path, FileDescriptor **to_mount, char *explicit_path);

// same as resolve_path, but returns the parent directory of the file
bool resolve_parent_path(const char *child_path, FileDescriptor **to_file);

// f,def,home,somefolder,myfile.ext -> myfile.ext
bool extract_file_name(const char *full_path, char *just_name);
// f,def,home,somefolder,myfile.ext -> f,def,home,somefolder
bool extract_file_directory_path(const char *full_path, char *directory_path);

// insert the supplied file into a directory, then adjust the name of the file
// so that the full file path will equal to the given path.
// Note 1: if file was under a different directory, the file is not unlisted
// Note 2: the directory structure containing the file must already exist
bool install_file(const char *path, FileDescriptor *file);



#endif 
