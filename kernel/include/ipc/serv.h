#ifndef _IPC_SERV_H
#define _IPC_SERV_H


#include <collections/array.h>
#include <data/security.h>
#include <stdbool.h>
#include <ipc/common.h>
#include <sys/serv.h>



#define MAX_PROCESS_LIST_LENGTH 65536


typedef enum ServProcStatus {
    OFFERED,
    ACCEPTED,
    COMPLETED,
    
} ServProcStatus;


typedef struct ServiceProcess {
    unsigned int requestor_task;
    unsigned int acceptor_task;
    unsigned int request_id;
    void *request_ptr;
    void *response_ptr;
    ServProcStatus status;
} ServiceProcess;


typedef struct ServiceTopic {
    char name[MAX_TOPIC_LENGTH];
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    
    unsigned int frame_length;
    Array *processes;
    
    Array *kernel_acceptors;
} ServiceTopic;



typedef bool (*kernel_serv_acceptor)(void *frame, unsigned int request_uid, unsigned int requestor_task_id);



ServiceTopic *register_new_service(char *topic_name, unsigned int frame_length, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib);
bool serv_retire_service(ServiceTopic *service);

ServiceTopic *get_service_by_name(char *name);
ServiceProcess *process_by_request_id(ServiceTopic *service, unsigned int request_uid);

bool accept_service_request(ServiceTopic *service, void *request, unsigned int *requestor_task_id, unsigned int *request_uid, unsigned int acceptor_task_id);
void kernel_accept(ServiceTopic *service, kernel_serv_acceptor callback);

void complete_service_request(ServiceTopic *service, unsigned int request_uid);
bool require_service(ServiceTopic *service, void *request, unsigned int *request_id, unsigned int requestor_task_id);
bool check_completed_and_close(ServiceTopic *service, unsigned int request_id);

void setup_serv_system();
void serv_make_system_dial_files();

#endif
