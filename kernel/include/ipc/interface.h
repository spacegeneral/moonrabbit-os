#ifndef _IPC_INTERFACE_H
#define _IPC_INTERFACE_H

#include <interfaces.h>


// basic initialization
extern EMPTY_IMPLEMENTATION(ipc, data_subsystem)  // see ipc/interface.c

// publish-subscribe message queues
extern EMPTY_IMPLEMENTATION(ipc, pubsub)  // see ipc/interface.c

// request-reply services
extern EMPTY_IMPLEMENTATION(ipc, service)  // see ipc/interface.c



#endif   
