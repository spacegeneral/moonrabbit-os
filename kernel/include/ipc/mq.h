#ifndef _IPC_MQ_H
#define _IPC_MQ_H


#include <collections/array.h>
#include <data/security.h>
#include <stdbool.h>
#include <ipc/common.h>
#include <sys/mq.h>


/* The MQ (Message Queue) system defines a global kernel-mediated
 * publisher-subscriber communication system to exchange data between
 * process (and between process and kernel).
 * It behaves as expected, with primitives for advertising/retiring a topic,
 * subscribing/unsubscribing to an existing topic, and broadcasting messages
 * to a topic.
 * The dispatching part is however quite different from the usual:
 * - when the kernel is subscribed to a topic, a new message causes the
 *   immediate execution of a kernel callback (kernel_mq_subscriber), without
 *   any buffering whatsoever.
 * - when a task is subscribed to a topic, new messages are put into a queue of
 *   fixed length, which acts as a "mailbox". The userspace process has to
 *   explicitly empty its own queue by using the poll primitive (nonblocking).
 *   In case of overflow, the queue discards the oldest message(s).
 * 
 */



typedef struct MessageTopic {
    char name[MAX_TOPIC_LENGTH];
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    SecurityPolicy policy;
    unsigned int max_queue_length;
    unsigned int message_length;
    Array* subscribers;
    Array* kernel_subscribers;
} MessageTopic;

typedef void (*kernel_mq_subscriber)(const void *message);


typedef struct MessageSubscriber {
    void* queue;
    size_t ring_read_head, ring_write_head;
    unsigned int task_id;
    MessageTopic* topic;
} MessageSubscriber;


MessageTopic *register_new_channel(char* name, unsigned int max_queue_length, unsigned int message_length, PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib, SecurityPolicy policy);
bool mq_retire_channel(MessageTopic* channel);

void subscribe_to_channel(MessageTopic* channel, unsigned int task_id);
void kernel_subscribe_to_channel(MessageTopic* channel, kernel_mq_subscriber callback);
bool unsubscribe_from_channel(MessageTopic* channel, unsigned int task_id);
bool kernel_unsubscribe_from_channel(MessageTopic* channel, kernel_mq_subscriber callback);

MessageTopic *get_channel_by_name(char* name);

bool publish_message(MessageTopic* channel, const void* message);
bool poll_channel(MessageTopic* channel, unsigned int task_id, void *dest);
bool next_write_causes_overrun(MessageTopic* channel);
bool fill_in_channel_stats(MessageTopic* channel, ChannelStats* stats);

void mq_global_unsubscribe(unsigned int task_id);
void mq_make_system_dial_files();

void setup_mq_system();

#endif
