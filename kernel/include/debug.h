#ifndef _DEBUG_H
#define _DEBUG_H

#include <stddef.h>
#include <stdio.h>
#include <arch/setup.h>


inline void indent_space(size_t num) {
    for (size_t i=0; i<num; i++) printf(" ");
}


void print_memory(unsigned int addr, unsigned int length);
void print_allocator_tree();
void print_mem_used();

void print_regs(Registers *regs);

#endif 
