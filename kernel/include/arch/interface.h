#ifndef _ARCH_INTERFACE_H
#define _ARCH_INTERFACE_H

#include <interfaces.h>


extern void arch_setup();
extern void arch_teardown();



extern EMPTY_IMPLEMENTATION(arch, subsystem)  // see arch/interface.c


#endif
