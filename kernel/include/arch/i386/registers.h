#ifndef ARCH_I386_REGISTERS_H
#define ARCH_I386_REGISTERS_H


typedef struct __attribute__((packed)) Registers {
    uint32_t eax, ebx, ecx, edx, esi, edi, esp, ebp, eip, eflags, cr3;
} Registers;



#endif
