#ifndef ARCH_I386_SETUP_H
#define ARCH_I386_SETUP_H

#include <stdint.h>
#include <arch/registers.h>
#include <task/multiplatform.h>


#define NUM_GDT_ENTRIES 3
#define NUM_IDT_ENTRIES 256


#define KERNEL_CODE_SELECTOR 0x08
#define KERNEL_DATA_SELECTOR 0x10
#define TSS_SELECTOR         0x28


typedef uint32_t addr_t;
typedef uint32_t ucpuint_t;
static __attribute__((unused)) unsigned long long int arch_uword_max = 0xFFFFFFFF;

inline addr_t arch_u64_truncate_to_addr(uint64_t long_addr) {
    return (uint32_t)(long_addr & 0xFFFFFFFF);
}

extern ucpuint_t kernel_code_selector;


typedef struct GDTEntrySource {
    uint32_t base;
    uint32_t limit;
    uint8_t type;
} GDTEntrySource;


typedef struct __attribute__((packed)) GDTEntry {
    unsigned int limit_low:16;
    unsigned int base_low : 24;
    //attribute byte split into bitfields
    unsigned int accessed :1;
    unsigned int read_write :1; //readable for code, writable for data
    unsigned int conforming_expand_down :1; //conforming for code, expand down for data
    unsigned int code :1; //1 for code, 0 for data
    unsigned int always_1 :1; //should be 1 for everything but TSS and LDT
    unsigned int DPL :2; //priviledge level
    unsigned int present :1;
    //and now into granularity
    unsigned int limit_high :4;
    unsigned int available :1;
    unsigned int always_0 :1; //should always be 0
    unsigned int big :1; //32bit opcodes for code, uint32_t stack for data
    unsigned int gran :1; //1 to use 4k page addressing, 0 for byte addressing
    unsigned int base_high :8;
} GDTEntry;

typedef struct __attribute__((packed)) GDT {
    uint16_t size;
    void *base;
} GDT;


typedef struct __attribute__((packed)) IDTEntry {
    uint16_t offset_1; // offset bits 0..15
    uint16_t selector; // a code segment selector in GDT or LDT
    uint8_t zero;      // unused, set to 0
    uint8_t type_attr; // type and attributes, see below
    uint16_t offset_2; // offset bits 16..31
} IDTEntry;

typedef struct __attribute__((packed)) IDT {
    uint16_t size;
    void *base;
} IDT;


typedef struct __attribute__((packed)) TSSEntry {
    uint32_t prev_tss;   // The previous TSS - if we used hardware task switching this would form a linked list.
    uint32_t esp0;       // The stack pointer to load when we change to kernel mode.
    uint32_t ss0;        // The stack segment to load when we change to kernel mode.
    uint32_t esp1;       // everything below here is unusued now.. 
    uint32_t ss1;
    uint32_t esp2;
    uint32_t ss2;
    uint32_t cr3;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax;
    uint32_t ecx;
    uint32_t edx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint32_t es;         
    uint32_t cs;        
    uint32_t ss;        
    uint32_t ds;        
    uint32_t fs;       
    uint32_t gs;         
    uint32_t ldt;      
    uint16_t trap;
    uint16_t iomap_base;
} TSSEntry;


extern GDT *gdt;
extern IDT *idt;

extern uint8_t *gdt_entries_base;
extern IDTEntry *idt_entries;
extern TSSEntry *tss_entry;

extern void* kernel_stack_top;

extern void tss_flush();
void set_kernel_stack(ucpuint_t stack);


Task* make_task(unsigned int task_id,
                ucpuint_t entry_point, 
                void *main_memarea,
                ucpuint_t stack_top,
                void *stack_memarea,
                ucpuint_t environ,
                ucpuint_t cr3, 
                ucpuint_t flags, 
                unsigned int owner_id,
                unsigned int parent_task,
                int *return_code_here, 
                const char* name, 
                TaskDebuginfo *debug_info);

void change_saved_ip(Registers* regs, addr_t new_ip);
void change_saved_sp(Registers* regs, addr_t new_sp);
void change_saved_flags(Registers* regs, ucpuint_t new_flags);



#endif
