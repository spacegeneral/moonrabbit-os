#ifndef ARCH_I386_INTERRUPT_H
#define ARCH_I386_INTERRUPT_H

#include <stdint.h>

#define IRQ_1_BASE 0x20
#define IRQ_2_BASE 0x28

#define EXC(code) (code)
#define IRQ(code) ((code) + IRQ_1_BASE)


#define IRQHANDLER_DECORATOR(code, body)    \
{                                           \
    asm volatile("cli");                    \
    body                                    \
    pic_send_eoi(code);                     \
    asm volatile("sti");                    \
}

#define IRQHANDLER_DECORATOR_NOCLEAR(code, body)    \
{                                           \
    body                                    \
    pic_send_eoi(code);                     \
}


#define FORALL_EXCEPTIONS(APPLIEDMACRO) \
    APPLIEDMACRO(0) \
    APPLIEDMACRO(1) \
    APPLIEDMACRO(2) \
    APPLIEDMACRO(3) \
    APPLIEDMACRO(4) \
    APPLIEDMACRO(5) \
    APPLIEDMACRO(6) \
    APPLIEDMACRO(7) \
    APPLIEDMACRO(8) \
    APPLIEDMACRO(9) \
    APPLIEDMACRO(10) \
    APPLIEDMACRO(11) \
    APPLIEDMACRO(12) \
    APPLIEDMACRO(13) \
    APPLIEDMACRO(14) \
    APPLIEDMACRO(15) \
    APPLIEDMACRO(16) \
    APPLIEDMACRO(17) \
    APPLIEDMACRO(18) \
    APPLIEDMACRO(19) \
    APPLIEDMACRO(20) \
    APPLIEDMACRO(30) \
    
    
typedef struct interrupt_frame {
    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
    uint32_t esp;
} interrupt_frame;

typedef struct exception_frame {
    uint32_t error_code;
    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
    uint32_t esp;
} exception_frame;


#endif 
