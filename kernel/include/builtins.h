#ifndef _BUILTINS_H
#define _BUILTINS_H

#include <stddef.h>
#include <stdint.h>
#include <sys/loader.h>
#include <sys/debuginfo.h>
#include <task/task.h>



typedef struct HalfElfLoaderReturn {
    uintptr_t entrypoint;
    void *memarea;
    size_t mem_required;
    int status;
    TaskDebuginfo *debug_info;
} HalfElfLoaderReturn;



int launch_elf_file(const char* filename);
void jump_to_ring3();

bool scan_partitions(bool autoadd);


void detect_vm();
bool is_inside_vm();


HalfElfLoaderReturn half_elf_loader(unsigned char* rawelf, const char *program_name);



void print_exception_debug_user(Task* task, addr_t ip);
bool print_debug_memarea_info(uintptr_t addr);



double ull_to_double(unsigned long long i);
double ll_to_double(long long i);
double i_to_double(int i);
double f_decode_to_double(uint32_t f);



#endif
