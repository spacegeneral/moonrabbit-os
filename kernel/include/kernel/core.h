#ifndef KERNEL_CORE_H
#define KERNEL_CORE_H


#define ADVANCED_DEBUG 1
#define SERIAL_DEBUG 1
#define TRACK_MEMORY 1
#define MALLOC_GUARD 1
#define MALLOC_GUARD_THRESHOLD 20


#include <stdint.h>


void panic();

void kernel_ubsan_panic(const char* violation, const char* filename, uint32_t line, uint32_t column);


#endif
