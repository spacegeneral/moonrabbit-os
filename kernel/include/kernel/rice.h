#ifndef _KERNEL_RICE_H
#define _KERNEL_RICE_H

#include <stdint.h>
#include <rice.h>

#define VGA
#define VBE

#define FONT_SCALE_FACTOR 0  // This is an exponent of 2: i.e. 2^1 = double size, s^2 = quadruple...

//#define BEEP_WELCOME


// solid-block style cursor
#define VGA_CURSOR_SCANLINE_START   0
#define VGA_CURSOR_SCANLINE_END     15

#define TTY_BACKGROUND COLOR24_TERM_BGROUND
#define TTY_FOREGROUND COLOR24_TEXT
#define TTY_CURSOR_ASCII 0xB6
#define TTY_CURSOR_UNICODE 0x2588
extern int TTY_CURSOR;



#endif
