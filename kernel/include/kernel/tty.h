#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H

#include <sys/term.h>
#include <sys/debuginfo.h>
#include <collections/array.h>
#include <interfaces.h>

#include <graphics/vga.h>
#include <graphics/vbe.h>


extern EMPTY_IMPLEMENTATION(video, terminal)

void omni_terminal_finalize();


#endif
