#ifndef _KERNEL_BITMAP_FONT_H
#define _KERNEL_BITMAP_FONT_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <graphics.h>


static __attribute__((unused)) BitmapCodePoint default_bitmap_font[] = {
    #include "unifont.h"
};

static __attribute__((unused)) BitmapCodePoint undefined_character = {
    .dimension = 0,
    .bitmap = {0x00, 0x00, 0x00, 0x7E, 0x66, 0x5A, 0x5A, 0x7A, 0x76, 0x76, 0x7E, 0x76, 0x76, 0x7E, 0x00, 0x00},
};


extern BitmapCodePoint* bitmap_font_table;
extern size_t bitmap_font_upper_range;


bool load_lbf_file(char *path);

#endif
