#ifndef KERNEL_BUGS_H
#define KERNEL_BUGS_H

#include <stdbool.h>

/* A qemu bug in which setting ps/2 keyboard scancode set 1 will result in 
   making the keyboard unworkable (wrong scancode set applied?).
   The bug is not present on hardware, though.
*/
bool have_qemu_ps2_scancode_bug();


#endif
 
