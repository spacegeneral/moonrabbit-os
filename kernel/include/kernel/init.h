#ifndef _KERNEL_INIT_H
#define _KERNEL_INIT_H

#include <data/vfs.h>

#define INITFILE "f,def,init,app,ring.elf"
#define INIT_PARTNAME "Lunar-initrd"
#define INITFS_DUMMY_DRIVER "DUMMY_MOUNT_INITRAMFS"


bool mount_initial_disk();


#endif
