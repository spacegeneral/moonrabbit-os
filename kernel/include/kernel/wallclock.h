#ifndef _KERNEL_WALLCLOCK_H
#define _KERNEL_WALLCLOCK_H

#include <time.h>
#include <stdbool.h>


bool get_system_time(time_t *time);
bool set_system_time(time_t *time);


#endif
