#ifndef _AUDIO_SOUNDCARD_H
#define _AUDIO_SOUNDCARD_H


#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>



#define MAX_SOUNDCARD_NAME 256


typedef struct SoundCardObject {
    unsigned int soundcard_id;
    char name[MAX_SOUNDCARD_NAME];
    int irq_num;
    void *driver;
    
    int (*negotiate_irq_assignment) ();
    void (*interrupt_callback) (struct SoundCardObject *self);
    bool (*default_sink) (struct SoundCardObject *self, uint8_t* samples, size_t byte_count);
} SoundCardObject;


typedef struct SoundCardInitializer {
    SoundCardObject* (*soundcard_autodetect) ();
} SoundCardInitializer;


void setup_soundcard();


#endif
