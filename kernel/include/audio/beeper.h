#ifndef _AUDIO_BEEPER_H
#define _AUDIO_BEEPER_H

#include <stdint.h>


void system_beep(uint16_t frequency, unsigned long long int duration);
void setup_beeper();


#endif
