#ifndef _AUDIO_SB16_H
#define _AUDIO_SB16_H

#include <stdint.h>
#include <stdbool.h>
#include <audio/soundcard.h>

#define SB16_CANDIDATE_IO_BASE_COUNT 4

#define SB16_DESIRED_IRQ 10
#define SB16_DESIRED_ISA_DMA_CHANNEL 1

#define SB16_MIXER_ADDRESS_PORT (sb16_io_base+0x6)
#define SB16_MIXER_DATA_PORT (sb16_io_base+0x6)
#define SB16_RESET_PORT (sb16_io_base+0x6)
#define SB16_READ_DATA_PORT (sb16_io_base+0xA)
#define SB16_WRITE_CMD_PORT (sb16_io_base+0xC)
#define SB16_READ_BUFFER_STATUS_PORT (sb16_io_base+0xE)

#define SB16_ACK_8BIT_DMA_INTERRUPT_PORT (sb16_io_base+0xE)
#define SB16_ACK_16BIT_DMA_INTERRUPT_PORT (sb16_io_base+0xF)
#define SB16_ACK_MPU401_INTERRUPT_PORT (sb16_io_base+0x100)


#define SB16_RESET_OK 0xAA
#define SB16_DATA_AVAIL_MASK 0b10000000


#define SB16_CMD_GET_VERSION 0xE1
#define SB16_CMD_OUTPUT 0x41
#define SB16_CMD_INPUT 0x42


#define SB16_MIXER_REGISTER_IRQ 0x80
#define SB16_MIXER_REGISTER_DMA 0x81
#define SB16_MIXER_REGISTER_INTERRUPT_STATUS 0x82


typedef enum SB16IOMode {
    PCM_8_MONO,
    PCM_8_STEREO,
    PCM_16_MONO,
    PCM_16_STEREO
} SB16IOMode;


static __attribute__((unused)) uint16_t candidate_io_base[SB16_CANDIDATE_IO_BASE_COUNT] = {0x220, 0x240, 0x260, 0x280};
extern uint16_t sb16_io_base;
extern uint16_t sb16_dps_version;
extern size_t sb16_doublebuf_offset;


inline uint8_t sb16_irq_set_bit(int irq_num) {
    switch (irq_num) {
        case 2:  return 0b0001;
        case 5:  return 0b0010;
        case 7:  return 0b0100;
        case 10: return 0b1000;
        default: return 0;
    }
}


SoundCardObject* sb16_autodetect();


#endif
