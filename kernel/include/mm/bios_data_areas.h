#ifndef _MM_BIOS_DATA_AREAS_H
#define _MM_BIOS_DATA_AREAS_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <collections/array.h>


// see http://stanislavs.org/helppc/bios_data_area.html


typedef struct __attribute__((packed)) BiosDataArea {
    uint16_t com1_port;
    uint16_t com2_port;
    uint16_t com3_port;
    uint16_t com4_port;
    
    uint16_t lpt1_port;
    uint16_t lpt2_port;
    uint16_t lpt3_port;
    
    uint16_t ebda_pointer;
    
    uint16_t equipment_flags;
    
    uint8_t pc_jr_ir_keyboard_error_count;
    uint16_t mem_kbytes;
    uint8_t reserved1;
    uint8_t ps2_bios_control_flags;
    
    uint16_t keyboard_state_flags;
    
    uint8_t alternate_keypad_entry;
    
    uint16_t keybaord_buffer_head_offset;
    uint16_t keybaord_buffer_tail_offset;
    uint8_t keyboard_buffer[32];
    
    uint8_t drive_recalibration_status;
    uint8_t diskette_motor_status;
    uint8_t motor_shutoff_counter;
    uint8_t last_diskette_operation_status;
} BiosDataArea;


static __attribute__((unused)) BiosDataArea *bios_data_area = (BiosDataArea*)0x0400;


extern uint8_t *extended_bios_data_area;
#define EBDA_LENGTH 0x60000



#endif 
