#ifndef _MM_H
#define _MM_H

#include <stddef.h>
#include <stdbool.h>
#include <kernel/core.h>
#include <mm/multiboot.h>
#include <mm/paging.h>
#include <sync/spinlock.h>
#include <sys/debuginfo.h>

#define PAGE_SIZE 4096

extern uintptr_t kernel_heap_start;
extern unsigned int kernel_heap_length;
#define KERNEL_HEAP_ALIGN 4096
#define KERNEL_HEAP_MARGIN_AFTER 0xC0000


#define KERNEL_CODE_SEGMENT 0x08
#define USER_CODE_SEGMENT   0x1B


#define MAX_LISTED_MULTIBOOT_MMAPS 32
#define MAX_LISTED_ELF_MMAPS 32
typedef struct KernelMemMap {
    TaskSection elf_mmaps[MAX_LISTED_ELF_MMAPS];
    size_t num_elf_mmaps;
    multiboot_memory_map_t mb_mmaps[MAX_LISTED_MULTIBOOT_MMAPS];
    size_t num_mb_mmaps;
} KernelMemMap;
extern KernelMemMap kernel_mem_map;


DECLARE_LOCK(mem_allocator_lock);


typedef struct MemUsageStats {
    unsigned int free;
    unsigned int total_available;
} MemUsageStats;


inline size_t fit_into_pages(size_t bytes) {
    if (bytes % PAGE_SIZE == 0) {
        return bytes / PAGE_SIZE;
    } else {
        return (bytes / PAGE_SIZE) + 1;
    }
}


void initial_memory_setup(uint32_t mboot_magic, multiboot_info_t* mboot_info);
void extract_grub_multiboot_mmap(multiboot_info_t* mbt);
void extract_grub_elf_map(multiboot_info_t* mbt);
unsigned long get_memory_end();
unsigned long get_kernel_end();
void kernel_internal_get_bios_areas(Array *append_sections_here);

void init_page_allocator(unsigned int heap_start, unsigned int heap_length);
void setup_mem_special_files();

void *allocate_pages(size_t num_pages, int desperation);
void set_memarea_tracking_data(void *address, char *filename, unsigned linenum);
size_t free_pages(void *address, size_t num_pages);
bool free_memarea(void *address);
bool get_memarea_length(void *address, size_t *length);
bool tag_memarea(void *address, unsigned int tag);
unsigned int get_memarea_tag(void *address);
void free_mem_by_tag(unsigned int tag);
#ifdef TRACK_MEMORY
    void apply_to_memareas_by_tag(unsigned int tag, void (*applied)(unsigned start, unsigned length, char *filename, unsigned linenum, void*), void* user_data);
#else
    void apply_to_memareas_by_tag(unsigned int tag, void (*applied)(unsigned start, unsigned length, void*), void* user_data);
#endif
void *find_address_owner(void *address);

void get_mem_statistics(MemUsageStats *stats);
uint8_t *search_mem_region(uint32_t start, uint32_t end, uint8_t *sequence, size_t sequence_length);
void init_bios_data_areas();

bool transfer_mem_ownership(void *beginning, unsigned int owner_uid);






#endif 
