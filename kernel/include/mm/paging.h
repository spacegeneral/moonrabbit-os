#ifndef _MM_PAGING_H
#define _MM_PAGING_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <security/identify.h>


#define PAGE_DIR_ENTRIES 1024
#define PAGE_TAB_ENTRIES 1024


#define PAGE_BIT_PRESENT 0b0001
#define PAGE_BIT_RW      0b0010
#define PAGE_BIT_USER    0b0100

#define PAGE_BIT_GUARD   (1 << 9)

#define HAS_INVLPG

#ifdef HAS_INVLPG
static inline void __native_flush_tlb_single(unsigned long addr)
{
   asm volatile(
       "mov eax, %0 \t\n\
        invlpg [eax]" 
        ::"r" (addr) : "eax");
}
#else
static inline void __native_flush_tlb_single(unsigned long addr) {}
#endif


typedef struct PageDirecEntry {
    unsigned present:1;
    unsigned readwrite:1;
    unsigned user:1;
    unsigned write_through:1;
    unsigned disable_cache:1;
    unsigned accessed:1;
    unsigned _zero:1;
    unsigned big_pse_page:1;
    unsigned _ignored:1;
    unsigned available:3;
    unsigned page_table_address:20;
} PageDirecEntry;


typedef struct PageTableEntry {
    unsigned present:1;
    unsigned readwrite:1;
    unsigned user:1;
    unsigned write_through:1;
    unsigned disable_cache:1;
    unsigned accessed:1;
    unsigned dirty:1;
    unsigned _zero:1;
    unsigned global:1;
    unsigned available:3;
    unsigned physical_address:20;
} PageTableEntry;


typedef struct PageManagementObject {
    PageDirecEntry *page_directory;
    PageTableEntry *page_tables;
    bool valid;
} PageManagementObject;


#define ITERATE_OVER_PT_ENTRIES(content_ptr_name, phys_addr_name, map_object, body) \
{ \
    for (uint32_t npage_table=0; npage_table<PAGE_DIR_ENTRIES; npage_table++) { \
        for (uint32_t page_tab_entry=0; page_tab_entry<PAGE_TAB_ENTRIES; page_tab_entry++) { \
            uint32_t *content_ptr_name = (uint32_t*) (map_object->page_tables + (npage_table * PAGE_TAB_ENTRIES) + page_tab_entry); \
            uint32_t phys_addr_name = ((npage_table & 0x3FF) << 22) | ((page_tab_entry & 0x3FF) << 12); \
            body \
        } \
    } \
}


PageTableEntry* virt_addr_to_page_entry(uint32_t virt_addr, IdentifiedAgent *owner);
void setup_memory_mapping();
void refresh_tlb();

void readonly_range(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner);
void set_ownership_range(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner, bool is_user);
void set_present_range(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner, bool present);

void set_pages_as_guard(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner);
bool is_page_guard(void *start_virt_address, IdentifiedAgent *owner);
void set_pages_not_guard(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner);

#endif
