#ifndef _MM_PAGE_ALLOCATOR_H
#define _MM_PAGE_ALLOCATOR_H

#include <stddef.h>
#include <kernel/core.h>

typedef enum {
    Null = 0,
    Placeholder,
    Free,
    Occupied,
    Split
} MemNodeType;


typedef struct MemNode {
    MemNodeType type;
    
    unsigned int tag;
    unsigned int start;
    unsigned int length;
    
    unsigned int file;
    unsigned int line;
} MemNode;


extern size_t MAX_MEM_NODES;
extern size_t mem_node_exp_factor;


size_t find_first_free_node(MemNode *tree, size_t root_index, unsigned int request_length);
size_t find_node_starting_at(MemNode *tree, size_t root_index, unsigned int request_start);
size_t find_leaf_containing_address(MemNode *tree, size_t root_index, unsigned int request_addr);
void recursive_free_tree_by_tag(MemNode *tree, size_t root_index, unsigned int request_tag);
#ifdef TRACK_MEMORY
void recursive_apply_to_occupied_leaf_by_tag(MemNode *tree, size_t root_index, unsigned int request_tag, void (*applied)(unsigned, unsigned, char*, unsigned, void*), void* user_data);
char *mm_get_filename_by_id(unsigned file_id);
#else
void recursive_apply_to_occupied_leaf_by_tag(MemNode *tree, size_t root_index, unsigned int request_tag, void (*applied)(unsigned, unsigned, void*), void* user_data);
#endif
void recursive_print_tree(MemNode *tree, size_t root_index, size_t depth_counter);
void balance_tree(MemNode *balanced, MemNode *backup_unbalanced);
void recursive_print_used(MemNode *tree, size_t root_index);
unsigned int count_free_space(MemNode *tree, size_t root_index);
size_t size_of_largest_leaf(MemNode *tree, size_t root_index);

void balance_memtree();
bool relocate_memtree(size_t tree_size);


#define INITIAL_MEM_NODE_EXP 13
#define MAX_MEM_NODES_FROM_EXP(exp) ((1<<(exp))-1)

#define LEFT_CHILD(index) ((((index)+1)*2)-1)
#define RIGHT_CHILD(index) (((index)+1)*2)
#define PARENT(index) ((index)/2)

#endif 
  
