#ifndef OOP_IMPL_H
#define OOP_IMPL_H


#include <string.h>


#define DECL_INTERFACE(name, methods) \
typedef struct impl_##name { \
    methods \
} impl_##name;


#define IMPLEMENTATION_OF(objname, interface, ...) \
static __attribute__((unused)) impl_##interface objname##_##interface = \
    __VA_ARGS__ \
;


#define UNIQUE_IMPLEMENTATION_OF(objname, interface, ...) \
impl_##interface objname##_##interface = \
    __VA_ARGS__ \
;


#define EMPTY_IMPLEMENTATION(objname, interface) \
impl_##interface objname##_##interface;


#define CLONE_IMPLEMENTATION(destobj, srcobj, interface) \
simple_memcpy(&(destobj##_##interface), &(srcobj##_##interface), sizeof(impl_##interface));


#define CALL_IMPL(objname, interface, method, ...) \
objname##_##interface.method(__VA_ARGS__)


#define REFERENCE_IMPL(objname, interface, method) \
objname##_##interface.method


#endif
