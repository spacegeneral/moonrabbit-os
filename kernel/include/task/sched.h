#ifndef _TASK_SCHED_H
#define _TASK_SCHED_H

#include <stdbool.h>
#include <stdint.h>
#include <task/task.h>

void sched_ireturn_next_task();

bool try_schedule_next();
bool try_acquire_continuation(Task* task);

void enable_task_self_preempt(Task* task, uintptr_t resume, unsigned long long expire);


#endif
