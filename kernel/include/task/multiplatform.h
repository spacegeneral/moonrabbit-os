#ifndef _TASK_MULTIPLATFORM_H
#define _TASK_MULTIPLATFORM_H


#include <stddef.h>
#include <arch/registers.h>
#include <sys/debuginfo.h>
#include <sys/sec.h>


#define MAX_TASKNAME_LENGTH 256


typedef enum PreemptionStage {
    PREEMPTION_OFF,
    PREEMPTION_ACQUIRED,
    PREEMPTION_RESUMED
} PreemptionStage;


typedef struct TaskPreemption {
    Registers continuation;
    uintptr_t resume;
    unsigned long long timeout;
    unsigned long long time_accumulator;
    bool enabled;
    PreemptionStage stage;
} TaskPreemption;


/** Task descriptor (platform-agnostic)
 */
typedef struct Task {
    Registers regs;             ///< saved register state
    TaskPreemption preempt;
    char name[MAX_TASKNAME_LENGTH];     ///< human-readable name
    unsigned int owner_id;      ///< id of an existing agent, can *NOT* be INVALID_TASK_ID for a running task
    unsigned int task_id;       ///< id of the task itself
    unsigned int parent_task;   ///< id of the parent task, can be INVALID_TASK_ID only for the init task
    int *return_code_here;      ///< pointer to the location where the return code is to be stored, can be NULL
    void *main_memarea;         ///< pointer to the memory area where the elf segments are loaded
    void *stack_memarea;        ///< pointer to the memory area used for the task's stack
    TaskDebuginfo *debug_info;  ///< debug informations, such as the memory map. The pointed TaskDebuginfo object is shared between sibling threads
    unsigned long long time_counter[2];  ///< active counter of quanta of time spent on this task (user and kernel mode), used only for internal calculations
    unsigned long long time_counter_latch[2];   ///< counter of quanta of time spent on this task (user and kernel mode)
    unsigned long long resource_counter_latch;  ///< counter of total kernel time quanta spent on the task, used only for internal calculations
    double happyness;
} Task; 


typedef struct TaskInfoStats {
    unsigned int task_id;
    unsigned int parent_task;
    unsigned int owner_id;
    unsigned long long time_counter[2];
    char name[MAX_TASKNAME_LENGTH];
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
} TaskInfoStats;



#endif
