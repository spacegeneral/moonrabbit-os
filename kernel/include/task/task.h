#ifndef _TASK_TASK_H
#define _TASK_TASK_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <arch/setup.h>
#include <sys/debuginfo.h>
#include <task/multiplatform.h>



/** \defgroup KERN_TASK Task management kernel internals
 *
 * The tasking system is minimal on the kernel side, it just takes care of
 * directly switching between ::Task when asked.
 * 
 * In the current version of Moonrabbit OS, only a single CPU is used, 
 * meaning that only a single task is actually under execution at any given
 * moment.
 * The scheduling of tasks is carried out entirely in userspace, via primitives
 * such as ::yield and ::task_switch (see sys/proc.h for details)
 */

/** @{ */


// maximum allowed number of simultaneous tasks
#define MAX_TASKS 256

#define INVALID_TASK_ID (MAX_TASKS+1)

#define PROC_DEFAULT_STACKSIZE 1024*256  // 256KB


typedef enum TimeCounterSelector {
    TIME_COUNTER_USER = 0,
    TIME_COUNTER_KERNEL
} TimeCounterSelector;


extern Task tasklist[MAX_TASKS]; ///< table containing all the ::Task objects, including unused ones 

extern void restore_registers(Registers *regs);
extern void irestore_registers(Registers *regs);
extern void save_registers(Registers *regs);

void setup_multitasking_system();
void activate_multitasking_system();

void superswitch_task(unsigned int next_task_id);
void iswitch_task(unsigned int next_task_id);
void iswitch_task_forget_current(unsigned int next_task_id);
void iswitch_task_continuation(unsigned int next_task_id);


Task* new_task(ucpuint_t entry_point,        ///< pointer to the _start procedure
               void* main_memarea,           ///< pointer to the main memory area (.text, .data, .bss, .rodata & other elf sections)
               ucpuint_t stack_top,          ///< value of the initial ESP value
               void* stack_memarea,          ///< pointer to the stack memory area
               ucpuint_t environ,            ///< pointer to the environ structure
               ucpuint_t cr3,                ///< initial cr3, pointer to a page directory (array of PageDirecEntry, see mm/paging.h)
               ucpuint_t flags, 
               unsigned int owner_id,
               unsigned int parent_task,
               int* return_code_here,
               const char* name,
               TaskDebuginfo* debug_info);

Task* new_task_with_id(unsigned int task_id, 
                       ucpuint_t entry_point, 
                       void* main_memarea,
                       ucpuint_t stack_top,
                       void* stack_memarea,
                       ucpuint_t environ,
                       ucpuint_t cr3, 
                       ucpuint_t flags, 
                       unsigned int owner_id,
                       unsigned int parent_task,
                       int* return_code_here, 
                       const char* name, 
                       TaskDebuginfo* debug_info);


unsigned int get_free_task_id();
Task* get_current_task();
Task* get_task(unsigned int task_id);
int count_tasks_by_owner(unsigned int owner_id);
bool task_exists(unsigned int task_id);

void reset_tasks_resources();
void mark_task_invalid(unsigned int task_id);


/** Release the resources (memory, open files, subscriptions)
 * of a running task and then kills it.
 * If the agent owning the dead task does not own any other task,
 * he is also killed.
 * \return true for success, false in case of errors
 */
bool purge_task(unsigned int task_id);

/** Release the resources (memory, open files, subscriptions)
 * of a running task without actually killing it.
 * \return true for success, false in case of errors
 */
bool reset_task(unsigned int task_id);

void tasks_remove_system_memmap_file(int task_id);
void tasks_make_system_memmap_file(int task_id);
void emergency_current_task_kill();

void store_saved_registers_into_task(Task* task);

size_t tasks_memmap_dial_interface(char *filename, char *dest, size_t maxsize);
size_t kernel_memmap_dial_interface(char *filename, char *dest, size_t maxsize);

void ireturn_to_current_task();


extern Registers* saved_registers_location;
extern Registers* saved_registers_timer_location;


/** @} */


#endif
