#ifndef _SECURITY_IDENTIFY_H
#define _SECURITY_IDENTIFY_H

#include <data/security.h>
#include <stdbool.h>


/* The purpose of the identification subsystem is to maintain a list of agents.
 * IdentifiedAgent(s) are objects with a clearance level associated to them, and are
 * aggregated to Task(s) in order to enrich them with security-specific attributes.
 * The same agent may have multiple tasks associated with them (a task may only
 * have a single associated agent instead).
 * That being said, agents only serve the purpose of helping managing the 
 * authentication aspects of running tasks, and are not equivalent to the
 * concept of users in unix-like OSs, despite of this fact, agents do have a
 * human-readable screen name.
 */


#define MAX_AGENT_NAME 256
#define MAX_AGENTS 256
#define AGENT_NOT_PRESENT_ID MAX_AGENTS+1

typedef struct IdentifiedAgent {
    unsigned int uid;
    PhisicalDomainClass phys_race_attrib;
    DataDomainClass data_class_attrib;
    bool is_user;
    char human_name[MAX_AGENT_NAME];
} IdentifiedAgent;


unsigned int add_new_agent(PhisicalDomainClass phys_race_attrib, 
                           DataDomainClass data_class_attrib, 
                           char *human_name,
                           bool is_user);
void delete_agent(unsigned int uid);
IdentifiedAgent* get_agent(unsigned int uid);
unsigned int get_free_agent_id();
IdentifiedAgent throwaway_dummy_agent(PhisicalDomainClass phys_race_attrib, 
                                      DataDomainClass data_class_attrib, 
                                      bool is_user);

void setup_identification_system();
void setup_identification_special_files();


#endif
 
