#ifndef _SECURITY_DEFAULTS_H
#define _SECURITY_DEFAULTS_H

#include <data/security.h>

/*
 * 
 * hardware/metal access clearance | data/confidentality access clearance
 *                                 |
 *                              God|Emperor            <- kernel 
 *                           Angel | King              <- whole disk
 *                         Dragon  |  Princess         <- partition
 *                           Elf   |   Knight
 *                        Alien    |    Priest         <- Administration
 *                       Human     |     Citizen       <- Normal operation
 *                      Dwarf      |      Maid
 *                       Cat       |       Prisoner
 *                    Ghost        |        Slave
 * 
 * 
 * 
 * 
 */


#define DEFAULT_SEC_ROOTFS_RACE  Dragon
#define DEFAULT_SEC_ROOTFS_CLASS Knight

#define DEFAULT_SEC_GRAVEYARD_RACE  God
#define DEFAULT_SEC_GRAVEYARD_CLASS Emperor
#define DEFAULT_SEC_GRAVEYARD_POLICY SecNoWriteUp

#define DEFAULT_SEC_DISKIO_RACE  Angel
#define DEFAULT_SEC_DISKIO_CLASS King

#define DEFAULT_SEC_PARTITION_RACE  Dragon
#define DEFAULT_SEC_PARTITION_CLASS Princess

#define DEFAULT_SEC_INFODIAL_RACE  Human
#define DEFAULT_SEC_INFODIAL_CLASS Maid

#define DEFAULT_SEC_TASKMAP_RACE  Elf
#define DEFAULT_SEC_TASKMAP_CLASS Knight

#define DEFAULT_SEC_KERNELMAP_RACE  Angel
#define DEFAULT_SEC_KERNELMAP_CLASS King

#define DEFAULT_SEC_KLOG_TOPIC_RACE  Alien
#define DEFAULT_SEC_KLOG_TOPIC_CLASS Priest
#define DEFAULT_SEC_KLOG_TOPIC_POLICY SecNoReadUp

#define DEFAULT_SEC_KEYBOARD_TOPICS_RACE  Human
#define DEFAULT_SEC_KEYBOARD_TOPICS_CLASS Citizen
#define DEFAULT_SEC_KEYBOARD_TOPICS_POLICY SecFullyEnforced

#define DEFAULT_SEC_MOUSE_TOPICS_RACE  Human
#define DEFAULT_SEC_MOUSE_TOPICS_CLASS Citizen
#define DEFAULT_SEC_MOUSE_TOPICS_POLICY SecFullyEnforced

#define DEFAULT_TERMINAL_OUT_TOPICS_RACE  Alien
#define DEFAULT_TERMINAL_OUT_TOPICS_CLASS Priest
#define DEFAULT_TERMINAL_OUT_TOPICS_POLICY SecNoReadUp|SecNoDataLoss

#define DEFAULT_DEBUG_OUT_TOPICS_RACE  Elf
#define DEFAULT_DEBUG_OUT_TOPICS_CLASS Citizen
#define DEFAULT_DEBUG_OUT_TOPICS_POLICY SecNoReadUp

#define DEFAULT_TERMINAL_CMD_TOPICS_RACE  Dwarf
#define DEFAULT_TERMINAL_CMD_TOPICS_CLASS Maid
#define DEFAULT_TERMINAL_CMD_TOPICS_POLICY SecNoReadUp

#define DEFAULT_TERMINAL_INFO_TOPICS_RACE  Human
#define DEFAULT_TERMINAL_INFO_TOPICS_CLASS Maid
#define DEFAULT_TERMINAL_INFO_TOPICS_POLICY SecFullyEnforced

#define DEFAULT_SEC_SERIAL_IO_TOPICS_RACE  Human
#define DEFAULT_SEC_SERIAL_IO_TOPICS_CLASS Citizen
#define DEFAULT_SEC_SERIAL_IO_TOPICS_POLICY SecFullyEnforced

#define DEFAULT_SEC_SHUTDOWN_TOPICS_RACE  Elf
#define DEFAULT_SEC_SHUTDOWN_TOPICS_CLASS Citizen
#define DEFAULT_SEC_SHUTDOWN_TOPICS_POLICY SecFullyEnforced

#define DEFAULT_ROOTFS_TOPICS_RACE  Dwarf
#define DEFAULT_ROOTFS_TOPICS_CLASS Citizen
#define DEFAULT_ROOTFS_TOPICS_POLICY SecNoWriteUp

#define DEFAULT_DIRECTVIDEO_RACE  Elf
#define DEFAULT_DIRECTVIDEO_CLASS Knight

#define DEFAULT_NETIFACE_RACE  Elf
#define DEFAULT_NETIFACE_CLASS King
#define DEFAULT_NETIFACE_POLICY SecFullyEnforced

#define DEFAULT_BEEPER_TOPIC_RACE  Ghost
#define DEFAULT_BEEPER_TOPIC_CLASS Slave
#define DEFAULT_BEEPER_TOPIC_POLICY SecFullyEnforced

#define DEFAULT_AUDIO_SINK_TOPIC_RACE  Human
#define DEFAULT_AUDIO_SINK_TOPIC_CLASS Citizen
#define DEFAULT_AUDIO_SINK_TOPIC_POLICY SecFullyEnforced

#define DEFAULT_HAPPY_TOPIC_RACE  Cat
#define DEFAULT_HAPPY_TOPIC_CLASS Prisoner
#define DEFAULT_HAPPY_TOPIC_POLICY SecFullyEnforced

#endif
 
