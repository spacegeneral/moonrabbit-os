#ifndef _SECURITY_HARDWARE_H
#define _SECURITY_HARDWARE_H


#include <mm/paging.h>


extern PageManagementObject page_management_entries[NUM_SECURITY_CLEARANCE_LEVELS][NUM_SECURITY_CLEARANCE_LEVELS];


void setup_hardware_security_bookeeping();
PageManagementObject* new_page_management_entry(PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib);
PageManagementObject* get_page_management_entry(PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib);
PageManagementObject* get_page_management_entry_owned_by(IdentifiedAgent *owner);
uint32_t get_cr3_from_page_mgr_object(PageManagementObject *entry);

void cascade_set_ownership(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner);
void cascade_kernel_appropriate_ownership(void *start_virt_address, size_t num_pages);

void cascade_set_guard_pages(void *start_virt_address, size_t num_pages);
void cascade_clear_guard_pages(void *start_virt_address, size_t num_pages);



#endif
