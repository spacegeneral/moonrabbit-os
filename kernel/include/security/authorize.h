#ifndef _SECURITY_AUTHORIZE_H
#define _SECURITY_AUTHORIZE_H

#include <data/security.h>
#include <data/vfs.h>
#include <stdbool.h>


/* This is just a collection of primitives for comparing clearance levels of
 * resources and agents, in order to check whether said agent is allowed to
 * access that resource (return true).
 */


bool authorize_simple(PhisicalDomainClass resource_race, 
                      DataDomainClass resource_class,
                      PhisicalDomainClass agent_race, 
                      DataDomainClass agent_class);

bool authorize_agent_id(PhisicalDomainClass resource_race, 
                        DataDomainClass resource_class,
                        unsigned int agent_id);

bool authorize_agent_id_write_up(PhisicalDomainClass channel_race, 
                                 DataDomainClass channel_class,
                                 SecurityPolicy policy,
                                 unsigned int agent_id);

bool authorize_agent_id_read_up(PhisicalDomainClass channel_race, 
                                DataDomainClass channel_class,
                                SecurityPolicy policy,
                                unsigned int agent_id);

bool authorize_task_id(PhisicalDomainClass resource_race, 
                       DataDomainClass resource_class,
                       unsigned int task_id);

bool authorize_task_id_write_up(PhisicalDomainClass channel_race, 
                                DataDomainClass channel_class,
                                SecurityPolicy policy,
                                unsigned int task_id);

bool authorize_task_id_read_up(PhisicalDomainClass channel_race, 
                               DataDomainClass channel_class,
                               SecurityPolicy policy,
                               unsigned int task_id);

bool authorize_file(FileDescriptor *resource,
                    PhisicalDomainClass agent_race, 
                    DataDomainClass agent_class);

#endif
 
