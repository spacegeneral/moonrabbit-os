#ifndef _SYSCALL_H
#define _SYSCALL_H

#include <stdint.h>
#include <os.h>
#include <stddef.h>
#include <stdbool.h>
#include <arch/interrupt.h>


extern int current_time_selector;


#define SYSCALL_INT 0x93


typedef void (*syscall_impl)(void *param_ptr, unsigned int ident);


extern void malloc_implementation(void *param_ptr, unsigned int ident);
extern void free_implementation(void *param_ptr, unsigned int ident);
extern void mlength_implementation(void *param_ptr, unsigned int ident);
extern void gift_memarea_implementation(void *param_ptr, unsigned int ident);
extern void direct_video_command_implementation(void *param_ptr, unsigned int ident);

extern void get_mount_info_implementation(void *param_ptr, unsigned int ident);
extern void mount_implementation(void *param_ptr, unsigned int ident);
extern void unmount_implementation(void *param_ptr, unsigned int ident);
extern void scan_media_implementation(void *param_ptr, unsigned int ident);

extern void gettime_implementation(void *param_ptr, unsigned int ident);

extern void ident_implementation(void *param_ptr, unsigned int ident);
extern void exit_implementation(void *param_ptr, unsigned int ident);
extern void abort_implementation(void *param_ptr, unsigned int ident);
extern void prelaunch_kernel_implementation(void *param_ptr, unsigned int ident);
extern void prelaunch_user_implementation(void *param_ptr, unsigned int ident);
extern void henshin_implementation(void *param_ptr, unsigned int ident);
extern void taskswitch_implementation(void *param_ptr, unsigned int ident);

extern void expirecc_implementation(void *param_ptr, unsigned int ident);
extern void continuation_implementation(void *param_ptr, unsigned int ident);

extern void yield_implementation(void *param_ptr, unsigned int ident);
extern void clone_implementation(void *param_ptr, unsigned int ident);
extern void disown_implementation(void *param_ptr, unsigned int ident);
extern void adopt_implementation(void *param_ptr, unsigned int ident);
extern void kill_implementation(void *param_ptr, unsigned int ident);

extern void advertise_channel_implementation(void *param_ptr, unsigned int ident);
extern void retire_channel_implementation(void *param_ptr, unsigned int ident);
extern void subscribe_implementation(void *param_ptr, unsigned int ident);
extern void unsubscribe_implementation(void *param_ptr, unsigned int ident);
extern void publish_implementation(void *param_ptr, unsigned int ident);
extern void publish_multi_implementation(void *param_ptr, unsigned int ident);
extern void poll_implementation(void *param_ptr, unsigned int ident);
extern void channel_stats_implementation(void *param_ptr, unsigned int ident);

extern void advertise_service_implementation(void *param_ptr, unsigned int ident);
extern void retire_service_implementation(void *param_ptr, unsigned int ident);
extern void accept_service_implementation(void *param_ptr, unsigned int ident);
extern void complete_service_implementation(void *param_ptr, unsigned int ident);
extern void poll_service_implementation(void *param_ptr, unsigned int ident);
extern void request_service_implementation(void *param_ptr, unsigned int ident);

extern void msleep_implementation(void *param_ptr, unsigned int ident);
extern void clock_implementation(void *param_ptr, unsigned int ident);


static const syscall_impl syscall_table[NUM_AVAILABLE_SYSCALLS] = {
    [SyscallMalloc] = &malloc_implementation,
    [SyscallFree] = &free_implementation,
    [SyscallMlength] = &mlength_implementation,
    [SyscallGiftMemarea] = &gift_memarea_implementation,
    [DirectVideoCommand] = &direct_video_command_implementation,
    [SyscallGetMountInfo] = &get_mount_info_implementation,
    [SyscallMount] = &mount_implementation,
    [SyscallUnMount] = &unmount_implementation,
    [SyscallScanMedia] = &scan_media_implementation,
    [SyscallIdent] = &ident_implementation,
    [SyscallExit] = &exit_implementation,
    [SyscallPrelaunch] = &prelaunch_user_implementation,
    [SyscallHenshin] = &henshin_implementation,
    [SyscallTaskSwitch] = &taskswitch_implementation,
    [SyscallExpireCC] = &expirecc_implementation,
    [SyscallContinuation] = &continuation_implementation,
    [SyscallYield] = &yield_implementation,
    [SyscallClone] = &clone_implementation,
    [SyscallAbort] = &abort_implementation,
    [SyscallDisown] = &disown_implementation,
    [SyscallAdopt] = &adopt_implementation,
    [SyscallKill] = &kill_implementation,
    [SyscallGettime] = &gettime_implementation,
    [SyscallMsleep] = &msleep_implementation,
    [SyscallClock] = &clock_implementation,
    [SyscallAdvertiseChannel] = &advertise_channel_implementation,
    [SyscallRetireChannel] = &retire_channel_implementation,
    [SyscallSubscribe] = &subscribe_implementation,
    [SyscallUnsubscribe] = &unsubscribe_implementation,
    [SyscallPublish] = &publish_implementation,
    [SyscallPublishMulti] = &publish_multi_implementation,
    [SyscallPoll] = &poll_implementation,
    [SyscallChannelStats] = &channel_stats_implementation,
    [SyscallAdvertiseService] = &advertise_service_implementation,
    [SyscallRetireService] = &retire_service_implementation,
    [SyscallAcceptService] = &accept_service_implementation,
    [SyscallRequestService] = &request_service_implementation,
    [SyscallCompleteService] = &complete_service_implementation,
    [SyscallPollService] = &poll_service_implementation,
};


void handle_syscall(struct interrupt_frame* frame);


#endif
