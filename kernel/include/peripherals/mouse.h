#ifndef _PERIPHERALS_MOUSE_H
#define _PERIPHERALS_MOUSE_H

#include <sys/mouse.h>


#define MOUSE_MSG_FRAME_LENGTH 3
#define PS2_MOUSE_RATE 40
#define PS2_MOUSE_PERIOD_MS (1000/PS2_MOUSE_RATE)

#define PS2_MOUSE_STREAMING


static __attribute__((unused)) unsigned int ps2_mouse_resolutions[] = {1, 2, 4, 8};

void catch_ps2_mouse_streaming_byte();
void setup_ps2_mouse_system();
void publish_ps2_mouse_event();


void setup_mouse_system();

#endif
