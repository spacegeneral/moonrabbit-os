#ifndef _PERIPHERALS_RTL8139_H
#define _PERIPHERALS_RTL8139_H 


#include <peripherals/network.h>


#define REALTEK_VEND     0x10EC
#define RTL8139_DEV      0x8139


#define RTL8139_PORT_MAC_BASE        0x00
#define RTL8139_PORT_MAR_BASE        0x08
#define RTL8139_PORT_RBSTART_BASE    0x30
#define RTL8139_PORT_COMMAND         0x37
#define RTL8139_PORT_CAPR            0x38
#define RTL8139_PORT_IMR             0x3C
#define RTL8139_PORT_ISR             0x3E
#define RTL8139_PORT_TCR             0x40
#define RTL8139_PORT_RCR             0x44
#define RTL8139_PORT_MPC             0x4C
#define RTL8139_PORT_CONFIG_9346     0x50
#define RTL8139_PORT_CONFIG_BASE     0x51
#define RTL8139_PORT_MSR             0x58
#define RTL8139_PORT_BMCR            0x62

#define RTL8139_PORT_TX_START_BASE   0x20
#define RTL8139_PORT_TX_STATUS_BASE  0x10

#define RTL8139_PORT_TX_STATUS_OWN_FLAG     0b0010000000000000


#define RTL8139_CMD_RESET_BIT               0b00010000
#define RTL8139_CMD_RX_ENABLE_BIT           0b00001000
#define RTL8139_CMD_TX_ENABLE_BIT           0b00000100
#define RTL8139_CMD_RX_BUF_EMPTY            0b00000001


#define RTL8139_MSR_SPEED_BIT               0b00001000


#define RTL8139_BMCR_DUPLEX_BIT             0b0000000100000000


#define RTL8139_INTERR_FLAG_ROK             0b0000000000000001
#define RTL8139_INTERR_FLAG_RER             0b0000000000000010
#define RTL8139_INTERR_FLAG_TOK             0b0000000000000100
#define RTL8139_INTERR_FLAG_TER             0b0000000000001000
#define RTL8139_INTERR_FLAG_RXOVW           0b0000000000010000
#define RTL8139_INTERR_FLAG_PUN_LINKCHG     0b0000000000100000
#define RTL8139_INTERR_FLAG_FOVW            0b0000000001000000
#define RTL8139_INTERR_FLAG_LENCHG          0b0010000000000000
#define RTL8139_INTERR_FLAG_TIMEOUT         0b0100000000000000
#define RTL8139_INTERR_FLAG_SERR            0b1000000000000000

#define RTL8139_INTERR_DEFAULT_FLAGS        (RTL8139_INTERR_FLAG_ROK | RTL8139_INTERR_FLAG_TOK)


#define RTL8139_PKT_HEADER_FAE              0b0000000000000010
#define RTL8139_PKT_HEADER_CRC              0b0000000000000100
#define RTL8139_PKT_HEADER_LONG             0b0000000000001000
#define RTL8139_PKT_HEADER_RUNT             0b0000000000010000
#define RTL8139_PKT_HEADER_ISE              0b0000000000100000

#define RTL8139_PKT_HEADER_ANY_ERROR        (RTL8139_PKT_HEADER_FAE | RTL8139_PKT_HEADER_CRC | RTL8139_PKT_HEADER_LONG | RTL8139_PKT_HEADER_RUNT | RTL8139_PKT_HEADER_ISE)


#define RTL8139_RCR_FLAG_AAP                0b00000001       // accept all packets
#define RTL8139_RCR_FLAG_APM                0b00000010       // accept phisical match packets
#define RTL8139_RCR_FLAG_AM                 0b00000100       // accept multicast packets
#define RTL8139_RCR_FLAG_AB                 0b00001000       // accept broadcast packets
#define RTL8139_RCR_FLAG_AER                0b00010000       // accpet error packets
#define RTL8139_RCR_FLAG_WRAP               0b01000000       // wrap mode


#define RTL8139_TX_DMA_BURST   4
#define RTL8139_RX_DMA_BURST   4

#define RTL8139_TX_FIFO_THRESH   256 
#define RTL8139_RX_FIFO_THRESH   4

#define RX_BUF_LEN_IDX                    0
#define RTL8139_RX_BUFFER_BASE_SIZE       (8192 << RX_BUF_LEN_IDX) 
#define RTL8139_RX_BUFFER_SIZE            (RTL8139_RX_BUFFER_BASE_SIZE + 16 + 1500)

#define RTL8139_TX_RING_SLOTS 4


#define RTL8139_BASE_RCR_CONFIG (RTL8139_RCR_FLAG_WRAP | (RTL8139_RX_DMA_BURST<<8) | (RX_BUF_LEN_IDX<<11) | (RTL8139_RX_FIFO_THRESH << 13))
#define RTL8139_DEFAULT_RCR_CONFIG (RTL8139_BASE_RCR_CONFIG | RTL8139_RCR_FLAG_AB | RTL8139_RCR_FLAG_AM | RTL8139_RCR_FLAG_APM)
#define RTL8139_PROMISC_RCR_CONFIG (RTL8139_BASE_RCR_CONFIG | RTL8139_RCR_FLAG_AB | RTL8139_RCR_FLAG_AM | RTL8139_RCR_FLAG_APM | RTL8139_RCR_FLAG_AAP)
#define RTL8139_DISABLED_RCR_CONFIG (RTL8139_BASE_RCR_CONFIG)

#define RTL8139_DEFAULT_TCR_CONFIG ((RTL8139_TX_DMA_BURST<<8) | 0x03000000)



typedef struct Rtl8139Nic {
    uint16_t io_base;
    uint8_t *rx_buffer;
    int rx_ring_current_byte_offset;
    
    int tx_ring_current_offset;
    int tx_ring_occupied_slots;
    uint8_t *tx_safe_buffer;
    
    unsigned long tx_errors;
    unsigned long rx_errors;
    
    bool is_fast;  // true: 100MB/s false: 10MB/s
    bool is_full_duplex;
} Rtl8139Nic;


void rtl8139_handle_rx(NicObject *self);
NicObject* match_pci_dev_rtl8139(PciDevice *device);

#endif
