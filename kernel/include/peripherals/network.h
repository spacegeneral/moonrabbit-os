#ifndef _PERIPHERALS_NETWORK_H
#define _PERIPHERALS_NETWORK_H


#include <stdbool.h>
#include <stdint.h>
#include <sys/network.h>
#include <peripherals/pci.h>
#include <yakumo.h>


typedef struct NicObject {
    unsigned int nic_id;
    char name[MAX_NIC_NAME_LEN];
    unsigned char mac[MAC_ADDRESS_LENGTH];
    int irq_num;
    void *driver;
    bool is_promisc;
    bool is_enabled;
    
    void (*interrupt_callback) (struct NicObject *self);
    bool (*transmit) (struct NicObject *self, NetPacket *packet);
    void (*toggle_promisc) (struct NicObject *self, bool enabled);
    void (*toggle_enable) (struct NicObject *self, bool enabled);
    void (*fill_manifest_info) (struct NicObject *self, NetIfaceManifest *manifest);
} NicObject;


typedef struct NicInitializer {
    NicObject* (*match_pci_dev) (PciDevice *device);
} NicInitializer;




void forward_packet(NicObject *nic, NetPacket *packet);
void setup_network_system();



#endif
