#ifndef _PERIPHERALS_SERIAL_H
#define _PERIPHERALS_SERIAL_H 

#include <stdbool.h>
#include <stdint.h>

/*
 * 
 *                   A
 * 
 * ###   #### ###  #  # ###  #    #  ###
 * #  #  #    #  # #  # #  # #    # #
 * ###   #### ###  #  # ###  #    # #
 * #  #  #    #    #  # #  # #    # #
 * #   # #### #     ##  ###  #### #  ###
 * 
 *              PRODUCTION
 * 
 */


#define COM1_PORT 0x3F8
#define COM2_PORT 0x2F8
#define COM3_PORT 0x3E8
#define COM4_PORT 0x2E8


void setup_serial_ports();
void read_serial_com1();


#endif
