#ifndef _PERIPHERALS_PCI_H
#define _PERIPHERALS_PCI_H

#include <stdint.h>
#include <stddef.h>
#include <enumtricks.h>
#include <stdbool.h>

#define CONFIG_ADDRESS_PORT 0xCF8
#define CONFIG_DATA_PORT 0xCFC

#define BAR_TYPE_MEMORY_MAPPED 0
#define BAR_TYPE_IO_MAPPED 1

#define FOREACH_PCICLASS(PCICLASS) \
    PCICLASS(AncientDevice) \
    PCICLASS(MassStorageController) \
    PCICLASS(NetworkController) \
    PCICLASS(DisplayController) \
    PCICLASS(MultimediaController) \
    PCICLASS(MemoryController) \
    PCICLASS(BridgeDevice) \
    PCICLASS(SimpleCommunicationControllers) \
    PCICLASS(BaseSystemPeripherals) \
    PCICLASS(InputDevices) \
    PCICLASS(DockingStations) \
    PCICLASS(Processors) \
    PCICLASS(SerialBusControllers) \
    PCICLASS(WirelessControllers) \
    PCICLASS(IntelligentIOControllers) \
    PCICLASS(SatelliteCommunicationControllers) \
    PCICLASS(EncryptionDecryptionControllers) \
    PCICLASS(DataAcquisitionAndSignalProcessingControllers) \
    PCICLASS(Reserved) \
    
    
typedef enum PciClassCode {
    FOREACH_PCICLASS(GENERATE_ENUM)
} PciClassCode;

static const char __attribute__((unused)) *pci_class_name[] = {
    FOREACH_PCICLASS(GENERATE_STRING)
};



typedef struct __attribute__((packed)) PciDevice {
    uint8_t bus, slot, function;
    uint16_t vendor_id, device_id;
    uint16_t command, status;
    uint8_t revision_id, prog_if, subclass, class_code;
    uint8_t cache_line_size, latency_timer, header_type, bist;
    uint32_t bar[6];
    uint32_t cardbus_cis_pointer;
    uint16_t subsystem_vendor_id, subsystem_id;
    uint32_t expansion_rom_base_address;
    uint8_t capabilities_pointer, reserved_b1, reserved_b2, reserved_b3;
    uint32_t reserved_l1;
    uint8_t interrupt_line, interrupt_pin, min_grant, max_latency;
} PciDevice;


typedef void (*pci_dev_apply_function)(PciDevice *device);


void setup_pci_system();

void pci_write_command(PciDevice * device, uint16_t command);
void pci_enable_bus_mastering(PciDevice * device);

size_t snprintf_lspci(char *dest, size_t maxstr, bool verbose);
void apply_on_pci_devices(pci_dev_apply_function function);
size_t get_num_devices();


inline bool is_pci_bridge(PciDevice * device) {
    return device->class_code == BridgeDevice && device->subclass == 0x04;
}

inline int pci_bar_type(PciDevice * device, int bar_num) {
    return device->bar[bar_num] & 0b11;
}


#endif
