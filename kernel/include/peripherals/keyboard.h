#ifndef _PERIPHERALS_KEYBOARD_H
#define _PERIPHERALS_KEYBOARD_H

#include <stdint.h>
#include <stdbool.h>
#include <localize/keymap.h>
#include <sys/keycodes.h>


/** \defgroup KERN_PERIPHERALS_KEYBOARD Keyboard management kernel internals
 
 @verbatim
  +-------------------+
  |  keyboard system  |
  |     i.e. PS/2     |
  |                   |    byte
  | (scancode source) -------------> [PS2 scancode topic]
  |     |      |      |
  |     |byte  |      |
  |     V      |byte  |
  |(internal)  |      |
  | (state)    |      |                           KeyEvent
  |   |        |      |                         +----------> [Global keycode topic]
  |   |        V      |                         |
  |   +->(convert)    |   KeyEvent              |
  |     (to keycode) --------------> (translation and )
  +-------------------+               (postprocessing)         
                                         ^      |
                                         |      |    byte
                                         |      +----------> [Global stdin topic]
                                         |
                                   global keymap
 @endverbatim
 */

/** @{ */


#define PS2_KEYBOARD_SCANCODE_SET 1


void publish_new_ps2_scancode();
void keyevent_translate_and_publish_keycode(KeyEvent *event);

void setup_keyboard_systems();

void setup_ps2_keyboard();


/** @} */


#endif
