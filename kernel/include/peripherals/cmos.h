#ifndef _PERIPHERALS_CMOS_H
#define _PERIPHERALS_CMOS_H 


#include <asmutils.h>


#define CMOS_ADDRESS_PORT 0x70
#define CMOS_DATA_PORT    0x71


inline uint8_t get_cmos_register(uint8_t reg) {
    outb(CMOS_ADDRESS_PORT, reg);
    return inb(CMOS_DATA_PORT);
}


inline int get_cmos_update_in_progress_flag() {
    outb(CMOS_ADDRESS_PORT, 0x0A);
    return (inb(CMOS_DATA_PORT) & 0x80);
}


#endif
