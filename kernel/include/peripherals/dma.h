#ifndef _PERIPHERALS_DMA_H
#define _PERIPHERALS_DMA_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>


typedef enum DMADirection {
    isa_dma_dir_read = 1,
    isa_dma_dir_write = 2
} DMADirection;


typedef enum DMAReg {
    DMA_FLIPFLOP_REG,
    DMA_SINGLE_CHANNEL_MASK_REG,
    DMA_PAGE_ADDRESS_REG,
    DMA_MODE_REG,
    DMA_START_ADDRESS_2_6_REG,
    DMA_COUNT_2_6_REG,
} DMAReg;


typedef struct IsaDmaRequest {
    DMADirection direction;
    int channel;
    bool auto_init;
    uintptr_t buffer;
    size_t buffer_size;
} IsaDmaRequest;


inline uint16_t get_dma_reg(int channel, DMAReg name) {
    switch (name) {
        case DMA_FLIPFLOP_REG:
            if (channel<=3) return 0x0C;
            return 0xD8;
        case DMA_SINGLE_CHANNEL_MASK_REG:
            if (channel<=3) return 0x0A;
            return 0xD4;
        case DMA_MODE_REG:
            if (channel<=3) return 0x0B;
            return 0xD6;
        case DMA_START_ADDRESS_2_6_REG:
            if (channel<=3) return 0x04;
            return 0xC8;
        case DMA_COUNT_2_6_REG:
            if (channel<=3) return 0x05;
            return 0xCA;
            
        case DMA_PAGE_ADDRESS_REG:
            switch (channel) {
                case 0: return 0x87;
                case 1: return 0x83;
                case 2: return 0x81;
                case 3: return 0x82;
                case 4: return 0x8F;
                case 5: return 0x8B;
                case 6: return 0x89;
                case 7: return 0x8A;
            }
    }
    return 0x80;
}


void isa_dma_init(IsaDmaRequest* request);


extern uint8_t *floppy_dma_buffer;
static const size_t floppy_dma_buffer_size = 0x4800;

extern uint8_t *isa_soundcard_dma_buffer;
static const size_t isa_soundcard_dma_buffer_size = 65536;


#endif
