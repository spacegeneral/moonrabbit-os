#ifndef _PERIPHERALS_FLOPPY_H
#define _PERIPHERALS_FLOPPY_H


#include <stdint.h>
#include <enumtricks.h>


#define MAX_FLOPPYS 4
#define COLD_MOTOR_THRESHOLD 150
#define DEFAULT_DOR_VALUE 0b1100


#define FLOPPY_STATUS_REGISTER_A                0x3F0 // read-only
#define FLOPPY_STATUS_REGISTER_B                0x3F1 // read-only
#define FLOPPY_DIGITAL_OUTPUT_REGISTER          0x3F2
#define FLOPPY_TAPE_DRIVE_REGISTER              0x3F3
#define FLOPPY_MAIN_STATUS_REGISTER             0x3F4 // read-only
#define FLOPPY_DATARATE_SELECT_REGISTER         0x3F4 // write-only
#define FLOPPY_DATA_FIFO                        0x3F5
#define FLOPPY_DIGITAL_INPUT_REGISTER           0x3F7 // read-only
#define FLOPPY_CONFIGURATION_CONTROL_REGISTER   0x3F7  // write-only


#define FLOPPY_CMD_SPECIFY              3
#define FLOPPY_CMD_RECALIBRATE          7
#define FLOPPY_CMD_SENSE_INTERRUPT      8
#define FLOPPY_CMD_SEEK                 15
#define FLOPPY_CMD_VERSION              16
#define FLOPPY_CMD_CONFIGURE            19
#define FLOPPY_CMD_UNLOCK               20
#define FLOPPY_CMD_LOCK                 148



#define FOREACH_FLOPPYROLE(FLOPPYROLE) \
    FLOPPYROLE(FloppyMaster) \
    FLOPPYROLE(FloppySlave) \

typedef enum FloppyRole {
    FOREACH_FLOPPYROLE(GENERATE_ENUM)
} FloppyRole;

static __attribute__((unused)) const char *floppy_role_name[] = {
    "Master", "Slave"
};


#define FOREACH_FLOPPYTYPE(FLOPPYTYPE) \
    FLOPPYTYPE(FloppyNone) \
    FLOPPYTYPE(FD_360_KB_5_25_Drive) \
    FLOPPYTYPE(FD_1_2_MB_5_25_Drive) \
    FLOPPYTYPE(FD_720_KB_3_5_Drive) \
    FLOPPYTYPE(FD_1_44_MB_3_5_Drive) \
    FLOPPYTYPE(FD_2_88_MB_3_5_drive) \
    
typedef enum FloppyType {
    FOREACH_FLOPPYTYPE(GENERATE_ENUM)
    NUM_FLOPPY_TYPES
} FloppyType;

static __attribute__((unused)) const char *floppy_type_name[] = {
    "No floppy", 
    "360 KB 5.25\" Drive", 
    "1.2 MB 5.25\" Drive",
    "720 KB 3.5\" Drive",
    "1.44 MB 3.5\" Drive",
    "2.88 MB 3.5\" drive"
};


typedef enum FloppyRate {
    FLOPPY_RATE_500K = 0x00,  /* 500 Kbps */
    FLOPPY_RATE_300K = 0x01,  /* 300 Kbps */
    FLOPPY_RATE_250K = 0x02,  /* 250 Kbps */
    FLOPPY_RATE_1M   = 0x03,  /*   1 Mbps */
} FloppyRate;

typedef struct FloppyFormat {
    uint16_t cylinders;
    uint16_t sides;
    uint16_t sectors;
    uint16_t rate;
} FloppyFormat;

static __attribute__((unused)) FloppyFormat floppy_formats[NUM_FLOPPY_TYPES] = {
    {0, 0, 0, 0},  // None
    {40, 2, 9, FLOPPY_RATE_250K},  // 360_KB_5_25_Drive
    {80, 2, 15, FLOPPY_RATE_500K},  // 1_2_MB_5_25_Drive
    {80, 2, 9, FLOPPY_RATE_250K},  // 720_KB_3_5_Drive
    {80, 2, 18, FLOPPY_RATE_500K},  // 1_44_MB_3_5_Drive
    {80, 2, 36, FLOPPY_RATE_1M},  // 2_88_MB_3_5_drive
    
};
static __attribute__((unused)) unsigned int floppy_size[NUM_FLOPPY_TYPES] = {
    0, 
    368640,
    1228800,
    737280,
    1474560,
    2949120,
    
};


typedef struct FloppyDevice {
    bool used;
    int id;
    bool inserted;
    FloppyRole role;
    FloppyType type;
    unsigned long long int engine_last_warm;
} FloppyDevice;


typedef struct FloppyCache {
    uint8_t *cache;
    uint8_t *cyl_bitmap;
    int floppy_type;
} FloppyCache;

#define IS_CYL_CACHED(cache, cylnum) (cache->cyl_bitmap[cylnum/8] & (1 << (cylnum % 8)))
#define MARK_CYL_CACHED(cache, cylnum) cache->cyl_bitmap[cylnum/8] |= (1 << (cylnum % 8))



extern volatile int floppy_irq_caught;


uint64_t floppy_device_read(FloppyDevice *device, 
                            uint64_t start_address, 
                            uint64_t data_length, 
                            uint8_t *destbuf);
uint64_t floppy_device_write(FloppyDevice *device, 
                             uint64_t start_address, 
                             uint64_t data_length, 
                             const uint8_t *srcbuf);
size_t snprint_floppy_device_info(char *dest, size_t maxstr, FloppyDevice *floppy, bool verbose);
void catch_floppy_irq();
void setup_floppy_system();
void toggle_floppy_visual_transfer(bool activated);
uint64_t get_floppy_capacity(FloppyDevice *device);
bool is_any_floppy_inserted_lazy();


#endif
