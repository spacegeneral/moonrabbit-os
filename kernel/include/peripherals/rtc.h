#ifndef _PERIPHERALS_RTC_H
#define _PERIPHERALS_RTC_H 

#include <time.h>
#include <stdbool.h>
#include <stdint.h>
#include <peripherals/cmos.h>


void read_rtc(struct tm *current_time);
void set_century_register(uint8_t century_register_value);


#endif
