#ifndef _PERIPHERALS_IDE_H
#define _PERIPHERALS_IDE_H

#include <enumtricks.h>

#define FOREACH_IDECHANNEL(IDECHANNEL) \
    IDECHANNEL(IDEPrimaryChannel) \
    IDECHANNEL(IDESecondaryChannel) \
    
typedef enum IdeChannel {
    FOREACH_IDECHANNEL(GENERATE_ENUM)
} IdeChannel;

static const char __attribute__((unused)) *ide_channel_name[] = {
    "Primary", "Secondary"
};

void setup_ide_system();

#endif
