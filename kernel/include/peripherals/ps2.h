#ifndef _PERIPHERALS_PS2_H
#define _PERIPHERALS_PS2_H

#include <stdint.h>
#include <stdbool.h>

#define PS2_DATA_PORT 0x60
#define PS2_COMMAND_PORT 0x64

#define NUM_PS2_SPECIAL_BYTES 7



#define PS2_CMD_ENABLE_FIRST_DEVICE      0xAE
#define PS2_CMD_DISABLE_FIRST_DEVICE     0xAD
#define PS2_CMD_ENABLE_SECOND_DEVICE     0xA8
#define PS2_CMD_DISABLE_SECOND_DEVICE    0xA7
#define PS2_CMD_TEST_FIRST_DEVICE        0xAB
#define PS2_CMD_TEST_SECOND_DEVICE       0xA9
#define PS2_CMD_SELF_TEST                0xAA
#define PS2_CMD_READ_INTERNAL_RAM(byte)  (0x20 + (byte))
#define PS2_CMD_WRITE_INTERNAL_RAM(byte) (0x60 + (byte))

#define PS2_CONFIG_BYTE                  0x00


static __attribute__((unused)) uint8_t ps2_special_bytes[NUM_PS2_SPECIAL_BYTES] = {0x00, 0xEE, 0xFA, 0xFC, 0xFD, 0xFE, 0xFF};


extern bool ps2_controller_present;
extern bool ps2_controller_dual_channel;


int get_ps2_scancode(uint8_t *code);

void ps2_wait(uint8_t a_type);
void ps2_controller_command_write(uint8_t a_write);
void ps2_first_port_command_write(uint8_t a_write);
void ps2_second_port_command_write(uint8_t a_write);
uint8_t ps2_read_data();

void initialize_ps2_controller();


#endif
