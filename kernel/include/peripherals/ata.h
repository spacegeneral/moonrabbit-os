#ifndef _PERIPHERALS_ATA_H
#define _PERIPHERALS_ATA_H

#include <stdint.h>
#include <enumtricks.h>


#define SECTOR_SIZE_BYTES 512

#define ATA_MAXTRANSFER_SECTORS 65534


#define FOREACH_ATAROLE(ATAROLE) \
    ATAROLE(ATAMaster) \
    ATAROLE(ATASlave) \

typedef enum AtaRole {
    FOREACH_ATAROLE(GENERATE_ENUM)
} AtaRole;

static const char __attribute__((unused)) *ata_role_name[] = {
    "Master", "Slave"
};


#define FOREACH_ATA_ADDRESSMODE(ATA_ADDRESSMODE) \
    ATA_ADDRESSMODE(CHS) \
    ATA_ADDRESSMODE(LBA28) \
    ATA_ADDRESSMODE(LBA48) \
    
typedef enum AtaAddressMode {
    FOREACH_ATA_ADDRESSMODE(GENERATE_ENUM)
} AtaAddressMode;

static const char __attribute__((unused)) *ata_address_mode_name[] = {
    FOREACH_ATA_ADDRESSMODE(GENERATE_STRING)
};


#define FOREACH_ATA_TYPE(ATA_TYPE) \
    ATA_TYPE(IDE_SATA) \
    ATA_TYPE(IDE_SATAPI) \
    ATA_TYPE(IDE_PATA) \
    ATA_TYPE(IDE_PATAPI) \
    
    
typedef enum AtaType {
    FOREACH_ATA_TYPE(GENERATE_ENUM)
} AtaType;

static const char __attribute__((unused)) *ata_type_name[] = {
    "SATA", "SATAPI", "PATA", "PATAPI"
};


typedef enum AtaIdentifyReturnCode {
    ATAIDENT_SUCCESS = 0,
    ATAIDENT_DEVICE_DOES_NOT_EXIST,
    ATAIDENT_UNKNOWN_DEVICE_TYPE,
    
} AtaIdentifyReturnCode;


typedef struct AtaDevice {
    int ide_channel, ata_role;
    AtaType type;
    uint32_t capabilities;
    uint16_t commandsets[6];
    AtaAddressMode address_mode;
    uint64_t size;  // size in sectors
    char model[41];
    uint16_t io_base;
    uint16_t control_base;
    
} AtaDevice;



#define ATA_DATA_PORT(device) ((device)->io_base + 0)  // the only 16 bit port; all other ports are 8 bit
#define ATA_FEATURES_ERROR_PORT(device) ((device)->io_base + 1)
#define ATA_SECTOR_COUNT_PORT(device) ((device)->io_base + 2)
#define ATA_LBA_0_PORT(device) ((device)->io_base + 3)
#define ATA_LBA_1_PORT(device) ((device)->io_base + 4)
#define ATA_LBA_2_PORT(device) ((device)->io_base + 5)
#define ATA_DEVICE_SELECT_PORT(device) ((device)->io_base + 6)
#define ATA_COMMAND_STATUS_PORT(device) ((device)->io_base + 7)
#define ATA_CONTROL_ALTERNATE_STATUS_PORT(device) ((device)->control_base + 2)


#define ATA_CMD_IDENTIFY 0xEC
#define ATA_CMD_IDENTIFY_PACKET 0xA1
#define ATA_CMD_READ_SECTORS_EXT 0x24
#define ATA_CMD_WRITE_SECTORS_EXT 0x34
#define ATA_CMD_CACHE_FLUSH 0xE7


#define ATA_SR_BSY 0x80
#define ATA_SR_RDY 0x40
#define ATA_SR_DRQ 0x08
#define ATA_SR_ERR 0x01
#define ATA_SR_DF 0x20


size_t snprint_ata_device_info(char *dest, size_t maxstring, AtaDevice *device, bool verbose);
void ata_test_and_add_device(uint16_t io_base, uint16_t control_base, int ide_channel, int ata_role);
uint64_t ata_device_read(AtaDevice *device, uint64_t start_address, uint64_t data_length, uint8_t *destbuf);
uint64_t ata_device_write(AtaDevice *device, uint64_t start_address, uint64_t data_length, const uint8_t* destbuf);


#endif
