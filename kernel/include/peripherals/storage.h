#ifndef _PERIPHERALS_STORAGE_H
#define _PERIPHERALS_STORAGE_H

#include <enumtricks.h>
#include <stdint.h>

#define FOREACH_STORAGECLASS(STORAGECLASS) \
    STORAGECLASS(DISK_UNKNOWN) \
    STORAGECLASS(DISK_IDE_SATA) \
    STORAGECLASS(DISK_IDE_SATAPI) \
    STORAGECLASS(DISK_IDE_PATA) \
    STORAGECLASS(DISK_IDE_PATAPI) \
    STORAGECLASS(DISK_FLOPPY) \
    
    
typedef enum StorageClass {
    FOREACH_STORAGECLASS(GENERATE_ENUM)
} StorageClass;

static const char __attribute__((unused)) *storage_class_name[] = {
    FOREACH_STORAGECLASS(GENERATE_STRING)
};


typedef struct StorageDevice {
    StorageClass type;
    unsigned int identifier;
    void *driver_data;
} StorageDevice;



void new_disk_identified(StorageClass type, void *driver_data);
void setup_storage_system();
size_t snprint_lsdisk(char *dest, size_t maxstr, bool verbose);

uint64_t get_disk_id_capacity(unsigned int diskid);
uint64_t read_from_disk_id(unsigned int diskid, uint64_t start_address, uint64_t data_length, uint8_t *destbuf);
uint64_t write_to_disk_id(unsigned int diskid, uint64_t start_address, uint64_t data_length, const uint8_t *srcbuf);


#endif
