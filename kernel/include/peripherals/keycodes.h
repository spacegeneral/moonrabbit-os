#ifndef _PERIPHERALS_KEYCODES_H
#define _PERIPHERALS_KEYCODES_H

#include <sys/keycodes.h>
#include <localize/keymap.h>

#define NUM_SCANCODES_SETS 1
#define NUM_SCANCODES_DEFINED 95


static const KeyCode ps2_scancode2keycode_table[NUM_SCANCODES_SETS][NUM_SCANCODES_DEFINED] = {
    [0] = {
        KEY_None, KEY_Escape, KEY_Num1, KEY_Num2, KEY_Num3, KEY_Num4, KEY_Num5,
        KEY_Num6, KEY_Num7, KEY_Num8, KEY_Num9, KEY_Num0, KEY_Minus,
        KEY_Equals, KEY_Backspace, KEY_Tab, KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T,
        KEY_Y, KEY_U, KEY_I, KEY_O, KEY_P, KEY_LeftBracket, KEY_RightBracket,
        KEY_Enter, KEY_LeftControl, KEY_A, KEY_S, KEY_D, KEY_F, KEY_G, KEY_H,
        KEY_J, KEY_K, KEY_L, KEY_Semicolon, KEY_Apostrophe, KEY_Grave,
        KEY_LeftShift, KEY_Backslash, KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N,
        KEY_M, KEY_Comma, KEY_Period, KEY_Slash, KEY_RightShift,
        KEY_PadMultiply, // Also PrintScreen
        KEY_LeftAlt, KEY_Space, KEY_CapsLock, KEY_F1, KEY_F2, KEY_F3,
        KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, 
        KEY_NumLock, KEY_ScrollLock, 
        KEY_Home, // Also Pad7
        KEY_Up, // Also Pad8
        KEY_PageUp, // Also Pad9
        KEY_PadMinus,
        KEY_Left, // Also Pad4
        KEY_Pad5, 
        KEY_Right, // Also Pad6
        KEY_PadPlus, 
        KEY_End, // Also Pad1
        KEY_Down, // Also Pad2
        KEY_PageDown, // Also Pad3
        KEY_Insert, // Also Pad0
        KEY_Delete, KEY_None, KEY_None, KEY_NonUsBackslash, KEY_F11, KEY_F12, KEY_Pause,
        KEY_None, KEY_LeftGui, KEY_RightGui, KEY_Menu, KEY_None, 
        },
    /*[1] = {
        None, KEY_F9, KEY_None, KEY_F5, KEY_F3, KEY_F1, KEY_F2, KEY_F12, KEY_None, KEY_F10, KEY_F8, KEY_F6, KEY_F4,
        Tab, KEY_Grave, KEY_None, KEY_None, KEY_LeftAlt, KEY_LeftShift, KEY_None, KEY_LeftControl, KEY_Q, KEY_Num1,
        None, KEY_None, KEY_None, KEY_Z, KEY_S, KEY_A, KEY_W, KEY_Num2, KEY_None, KEY_None, KEY_C, KEY_X, KEY_D, KEY_E, KEY_Num4, KEY_Num3,
        None, KEY_None, KEY_Space, KEY_V, KEY_F, KEY_T, KEY_R, KEY_Num5, KEY_None, KEY_None, KEY_N, KEY_B, KEY_H, KEY_G, KEY_Y, KEY_Num6,
        None, KEY_None, KEY_None, KEY_M, KEY_J, KEY_U, KEY_Num7, KEY_Num8, KEY_None, KEY_None, KEY_Comma, KEY_K, KEY_I, KEY_O, 
        Num0, KEY_Num9, KEY_None, KEY_None, KEY_Period, KEY_Slash, KEY_L, KEY_Semicolon, KEY_P, KEY_Minus, 
        None, KEY_None, KEY_None, KEY_Apostrophe, KEY_None, KEY_LeftBracket, KEY_Equals, KEY_None, KEY_None,
        CapsLock, KEY_RightShift, KEY_Enter, KEY_RightBracket, KEY_None, KEY_Backslash,
        None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, 
        Backspace, KEY_None, KEY_None, KEY_Pad1, KEY_None, KEY_Pad4, KEY_Pad7, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, 
        None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, KEY_None, 
    }*/
};



#endif
