#ifndef _INTERRUPT_INTERFACE_H
#define _INTERRUPT_INTERFACE_H

#include <interfaces.h>

#define USING_LEGACY_PIC


extern EMPTY_IMPLEMENTATION(interrupt, subsystem)  // see interrupt/interface.c

extern EMPTY_IMPLEMENTATION(interrupt, interr_onoff)  // see interrupt/interface.c


#endif  
