#ifndef _INTERRUPT_EXCEPTION_H
#define _INTERRUPT_EXCEPTION_H

#include <stddef.h>
#include <arch/interrupt.h>


void contextual_exception_handler(int code, void* frame);

#endif
 
