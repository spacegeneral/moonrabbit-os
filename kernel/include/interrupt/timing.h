#ifndef _INTERRUPT_TIMING_H
#define _INTERRUPT_TIMING_H

#include <stddef.h>
#include <stdbool.h>

#define INTERRUPT_FREQUENCY 1000
#define MAX_INTERRUPT_TIMER_CALLBACKS 32
#define USING_LEGACY_PIT

extern volatile unsigned long long int system_clock;
extern volatile unsigned long long int resource_clock;
#define RESOURCE_CLOCK_EPOCH 1000LLU

#define system_clock_millisec() ((system_clock*1000) / INTERRUPT_FREQUENCY)

void idle();
void halt();

void setup_timer();
void wait_milliseconds(unsigned long long int millisec);

typedef void (*interrupt_timer_callback) (unsigned long long int system_clock);

bool register_interrupt_timer_callback(interrupt_timer_callback fun);
bool unregister_interrupt_timer_callback(interrupt_timer_callback fun);

#endif 
 
