#ifndef _INTERRUPT_PIT_H
#define _INTERRUPT_PIT_H


#include <stdint.h>

#define PIT_CHANNEL_0 0x40
#define PIT_CHANNEL_1 0x41
#define PIT_CHANNEL_2 0x42
#define PIT_COMMAND 0x43
#define PIT_SPEAKER 0x61

#define PIT_MAGIC_DIVISOR 1193180 


void set_pit_frequency(uint16_t hertz);
void set_pit_beep_frequency(uint16_t hertz);
void start_pit_beep();
void stop_pit_beep();


#endif
