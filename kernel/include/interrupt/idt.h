#ifndef _INTERRUPT_IDT_H
#define _INTERRUPT_IDT_H

#include <stddef.h>
#include <interrupt/pic.h>
#include <arch/interrupt.h>


#define DECLARE_EXCEPTION_HANDLER(id) \
    extern void handler_exception_##id(struct interrupt_frame* frame);
    

FORALL_EXCEPTIONS(DECLARE_EXCEPTION_HANDLER)


void set_interrupt_handler(unsigned interrupt_code, void *handler);
void set_syscall_handler(unsigned interrupt_code, void *handler);
void disable_interrupt_handler(unsigned interrupt_code);

extern void handler_syscall(struct interrupt_frame* frame);

void setup_basic_interrupt_and_exception_handlers();

#endif
