#ifndef _INTERRUPT_DEVICES_H
#define _INTERRUPT_DEVICES_H

#include <stdint.h>
#include <stdbool.h>
#include <interrupt/idt.h>


#define IRQ_PIT 0
#define IRQ_KBD 1
#define IRQ_CASCADE 2
#define IRQ_FLOPPY 6
#define IRQ_COM1 4
#define IRQ_ISA_FREE_1 9
#define IRQ_ISA_FREE_2 10
#define IRQ_ISA_FREE_3 11
#define IRQ_MOUSE 12


typedef void (*interrupt_handler) (struct interrupt_frame* frame);
typedef void (*isa_free_handler) (struct interrupt_frame* frame, int irq_num);


typedef struct InterruptSourceDevice {
    uint8_t pic_irq_number;
    interrupt_handler pic_handler;
    bool is_enabled;
} InterruptSourceDevice;


extern void lvl1_handler_pit(struct interrupt_frame* frame);
extern void lvl1_handler_ps2keyboard(struct interrupt_frame* frame);
extern void lvl1_handler_floppy(struct interrupt_frame* frame);
extern void lvl1_handler_com1(struct interrupt_frame* frame);
extern void lvl1_handler_ps2mouse(struct interrupt_frame* frame);
extern void lvl1_handler_isa_free_1(struct interrupt_frame* frame);
extern void lvl1_handler_isa_free_2(struct interrupt_frame* frame);
extern void lvl1_handler_isa_free_3(struct interrupt_frame* frame);

extern InterruptSourceDevice *interr_pit;
extern InterruptSourceDevice *interr_ps2keyboard;
extern InterruptSourceDevice *interr_floppy;
extern InterruptSourceDevice *interr_com1;
extern InterruptSourceDevice *interr_ps2mouse;
extern InterruptSourceDevice *interr_isa_free_1;
extern InterruptSourceDevice *interr_isa_free_2;
extern InterruptSourceDevice *interr_isa_free_3;

void device_enable_interrupt(InterruptSourceDevice *device);
void device_disable_interrupt(InterruptSourceDevice *device);

InterruptSourceDevice *get_isa_free_device(int irq_num);
void attach_isa_free_device_callback(int irq_num, isa_free_handler callback);
void clear_isa_free_device_callback(int irq_num);
isa_free_handler get_isa_free_device_callback(int irq_num);


void setup_irq_device_definitions();




#endif
