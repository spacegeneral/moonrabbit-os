#ifndef _INTERRUPT_NMI_H
#define _INTERRUPT_NMI_H

#include <stddef.h>
#include <asmutils.h>


static inline void NMI_enable(void) {
    outb(0x70, inb(0x70) & 0x7F);
}

static inline void NMI_disable(void) {
    outb(0x70, inb(0x70) | 0x80);
} 

#endif
