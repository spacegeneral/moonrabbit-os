#ifndef _ASMUTILS_H
#define _ASMUTILS_H

#include <stddef.h>
#include <stdint.h>


inline void outb(uint16_t port, uint8_t val) {
    asm volatile ( "out %1, %0" : : "a"(val), "Nd"(port) );
}

inline void outw(uint16_t port, uint16_t val) {
    asm volatile ( "out %1, %0" : : "a"(val), "Nd"(port) );
}

inline void outl(uint16_t port, uint32_t val) {
    asm volatile ( "out %1, %0" : : "a"(val), "Nd"(port) );
}

inline uint8_t inb(uint16_t port) {
    uint8_t ret;
    asm volatile ( "in %0, %1"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

inline uint16_t inw(uint16_t port) {
    uint16_t ret;
    asm volatile ( "in %0, %1"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

inline uint32_t inl(uint16_t port) {
    uint32_t ret;
    asm volatile ( "in %0, %1"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

inline void io_wait_1us(void) {
    /* Port 0x80 is used for 'checkpoints' during POST. */
    /* The Linux kernel seems to think it is free for use :-/ */
    asm volatile ( "outb 0x80, al" : : "a"(0) );
    /* %%al instead of %0 makes no difference.  TODO: does the register need to be zeroed? */
}

inline void io_wait(unsigned time_us) {
    for (unsigned i=0; i<time_us; i++) io_wait_1us();
}

inline char are_interrupts_enabled() {
    unsigned long flags;
    asm volatile ( "pushf\n\t"
                   "pop %0"
                   : "=g"(flags) );
    return flags & (1 << 9);
}


#endif
