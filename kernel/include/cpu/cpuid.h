#ifndef _CPU_CPUID_H
#define _CPU_CPUID_H
 

void cpuid(unsigned int index, int regs[4]);
void cpuid_vendor_string(char *str);

 
#endif
