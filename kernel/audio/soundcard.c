#include <audio/soundcard.h>
#include <audio/sb16.h>
#include <kernel/log.h>
#include <collections/array.h>
#include <interrupt/devices.h>
#include <sys/audio.h>
#include <ipc/interface.h>
#include <security/defaults.h>
#include <stdio.h>


Array *soundcard_devices;
unsigned int soundcard_ident_gen;


#define NUM_SOUNDCARD_SUPPORTED 1
static SoundCardInitializer soundcard_initializers[NUM_SOUNDCARD_SUPPORTED] = {
    { .soundcard_autodetect = &sb16_autodetect, },
    
};


void sound_system_irq_catcher(struct interrupt_frame* frame, int irq_num) {
    (void)(frame); // unused
    ARRAY_FOREACH(curr, soundcard_devices, {
        SoundCardObject *card = (SoundCardObject*) curr;
        if (card->irq_num == irq_num) {
            card->interrupt_callback(card);
            return;
        }
    });
}


bool setup_new_card_irq_handler(SoundCardObject *card) {
    card->irq_num = card->negotiate_irq_assignment();
    if (card->irq_num < 0) {
        klog("Cannot assign interrupt handler for soundcard %d!\n", card->soundcard_id);
        return false;
    }
    
    attach_isa_free_device_callback(card->irq_num, &sound_system_irq_catcher);
    InterruptSourceDevice *irqdev = get_isa_free_device(card->irq_num);
    if (irqdev == NULL) {
        clear_isa_free_device_callback(card->irq_num);
        klog("Cannot enable interrupts for soundcard %d!\n", card->soundcard_id);
        return false;
    }
    device_enable_interrupt(irqdev);
    
    return true;
}


//TODO
// the current infrastructure only allow a maximum amount of soundcards (fixed at compile-time)

#define FOREACH_SOUNDCARD_SLOT(appliedmacro) \
    appliedmacro(0) \
    appliedmacro(1) \
    appliedmacro(2) \


#define DECLARE_SINK_MSG_HANDLER(idnum) \
static void default_sink_handler_##idnum(const void *message) { \
    SoundCardObject *card; \
    if (array_get_at(soundcard_devices, idnum, (void**)&card) != CC_OK) return; \
    if (!card->default_sink(card, (uint8_t*) message, AUDIO_PACKET_SIZE)) { \
        klog("Soft sink error on card%d: cannot play stream.\n", idnum); \
    } \
}

#define CASE_SELECT_SINK_MSG_HANDLER(idnum) \
case idnum: return &default_sink_handler_##idnum;



FOREACH_SOUNDCARD_SLOT(DECLARE_SINK_MSG_HANDLER)

static kernel_mq_subscriber get_assigned_default_sink_handler(SoundCardObject *card) {
    switch (card->soundcard_id) {
        FOREACH_SOUNDCARD_SLOT(CASE_SELECT_SINK_MSG_HANDLER)
    }
    
    return NULL;
}


static void setup_new_card_topics(SoundCardObject *card) {
    char topicname[2048];
    // default sink
    snprintf(topicname, 2048, DEFAULT_AUDIO_SINK_TEMPLATE_TOPIC, card->soundcard_id, "default");
    CALL_IMPL(ipc, pubsub, advertise_channel, topicname, 16, AUDIO_PACKET_SIZE, DEFAULT_AUDIO_SINK_TOPIC_RACE, DEFAULT_AUDIO_SINK_TOPIC_CLASS, DEFAULT_AUDIO_SINK_TOPIC_POLICY);
    CALL_IMPL(ipc, pubsub, kernel_subscribe, topicname, get_assigned_default_sink_handler(card));
}


void setup_soundcard() {
    soundcard_ident_gen = 0;
    array_new(&soundcard_devices);
    klog("Detecting sound cards...\n");
    for (size_t i=0; i<NUM_SOUNDCARD_SUPPORTED; i++) {
        SoundCardObject* newcard = soundcard_initializers[i].soundcard_autodetect();
        if (newcard != NULL) {
            newcard->soundcard_id = soundcard_ident_gen++;
            array_add(soundcard_devices, newcard);
            
            if (!setup_new_card_irq_handler(newcard)) continue;
            setup_new_card_topics(newcard);
            
            klog("Installed sound card #%d, name: %s, IRQ: %d\n", newcard->soundcard_id, newcard->name, newcard->irq_num);
        }
    }
}
