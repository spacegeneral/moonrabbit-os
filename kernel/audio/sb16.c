#include <audio/sb16.h>
#include <asmutils.h>
#include <kernel/log.h>
#include <audio/soundcard.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <interrupt/devices.h>
#include <peripherals/dma.h>


uint16_t sb16_io_base = 0x220;
uint16_t sb16_dps_version;
size_t sb16_doublebuf_offset;



static bool read_dsp(uint8_t* data) {
    long timeout = 10000;
    uint8_t buffer_status;
    
    do {
        buffer_status = inb(SB16_READ_BUFFER_STATUS_PORT);
    } while (!(buffer_status & SB16_DATA_AVAIL_MASK) && (timeout-- > 0));
    
    if (timeout <= 0) return false;
    
    *data = inb(SB16_READ_DATA_PORT);
    return true;
}

static bool write_dsp(uint8_t data) {
    long timeout = 10000;
    uint8_t buffer_status;
    
    do {
        buffer_status = inb(SB16_WRITE_CMD_PORT);
    } while ((buffer_status & SB16_DATA_AVAIL_MASK) && (timeout-- > 0));
    
    if (timeout <= 0) return false;
    
    outb(SB16_WRITE_CMD_PORT, data);
    return true;
}

static bool read_mixer_register(uint8_t register_addr, uint8_t* data) {
    outb(SB16_MIXER_ADDRESS_PORT, register_addr);
    *data = inb(SB16_MIXER_DATA_PORT);
    return true;
}

static bool write_mixer_register(uint8_t register_addr, uint8_t data) {
    outb(SB16_MIXER_ADDRESS_PORT, register_addr);
    outb(SB16_MIXER_DATA_PORT, data);
    return true;
}

static bool reset_dsp() {
    outb(SB16_RESET_PORT, 1);
    io_wait(3);
    outb(SB16_RESET_PORT, 0);
    io_wait(150);  // 100us would be enough
    uint8_t ack;
    if (!read_dsp(&ack)) return false;
    return ack == 0xAA;
}

static void read_dsp_version() {
    uint8_t data = 0;
    write_dsp(SB16_CMD_GET_VERSION);
    read_dsp(&data);
    sb16_dps_version = ((uint16_t)data) << 8;
    read_dsp(&data);
    sb16_dps_version = sb16_dps_version | ((uint16_t)data);
}


static bool detect_initialize_sb16() {
    for (int c=0; c<SB16_CANDIDATE_IO_BASE_COUNT; c++) {
        sb16_io_base = candidate_io_base[c];
        if (reset_dsp()) {
            read_dsp_version();
            klog("Soundblaster 16 (version %04x) detected at address 0x%03x\n", sb16_dps_version, sb16_io_base);
            return true;
        }
    }
    return false;
}

static uint8_t ack_interrupt() {
    uint8_t interr_register;
    read_mixer_register(SB16_MIXER_REGISTER_INTERRUPT_STATUS, &interr_register);
    interr_register = interr_register & 0b111;
    switch (interr_register) {
        case 0b001:
            inb(SB16_ACK_8BIT_DMA_INTERRUPT_PORT);
            break;
        case 0b010:
            inb(SB16_ACK_16BIT_DMA_INTERRUPT_PORT);
            break;
        case 0b100:
            inb(SB16_ACK_MPU401_INTERRUPT_PORT);
            break;
    }
    return interr_register;
}

static void sb16_set_dma_channel(int channel) {
    uint8_t dma_register;
    read_mixer_register(SB16_MIXER_REGISTER_DMA, &dma_register);
    dma_register = dma_register & 0b00010100;  // save the "undefined" bits
    dma_register = (1 << channel) & 0b11101011;
    write_mixer_register(SB16_MIXER_REGISTER_DMA, dma_register);
}


// consumes 64K from samples
bool begin_output_sequence(uint8_t* samples, SB16IOMode mode, uint16_t rate) {
    uint8_t bcommand = ((mode == PCM_8_MONO) || (mode == PCM_8_STEREO) ? 0xC6 : 0xB6);
    uint8_t bmode;
    size_t blocksize_divider = 1;
    switch (mode) {
        case PCM_8_MONO:    bmode = 0; break;
        case PCM_8_STEREO:  bmode = 0x20; break;
        case PCM_16_MONO:   bmode = 0x10; blocksize_divider = 2; break;
        case PCM_16_STEREO: bmode = 0x30; blocksize_divider = 2; break;
        default: return false;
    }
    size_t dsp_block_size = (isa_soundcard_dma_buffer_size / blocksize_divider) / 2;  // double-buffering
    sb16_doublebuf_offset = 0;
    
    memcpy(isa_soundcard_dma_buffer, samples, isa_soundcard_dma_buffer_size);
    
    IsaDmaRequest req = {
        .direction = isa_dma_dir_write,
        .channel = SB16_DESIRED_ISA_DMA_CHANNEL,
        .auto_init = true,
        .buffer = (uintptr_t)isa_soundcard_dma_buffer,
        .buffer_size = isa_soundcard_dma_buffer_size,
    };
    isa_dma_init(&req);
    
    // set transfer sampling rate
    outb(SB16_WRITE_CMD_PORT, SB16_CMD_OUTPUT);
    outb(SB16_WRITE_CMD_PORT, (uint8_t)(rate >> 8));
    outb(SB16_WRITE_CMD_PORT, (uint8_t)(rate & 0x00FF));
    
    // send io command
    outb(SB16_WRITE_CMD_PORT, bcommand);
    outb(SB16_WRITE_CMD_PORT, bmode);
    outb(SB16_WRITE_CMD_PORT, (uint8_t)(((uint16_t)dsp_block_size) & 0x00FF));
    outb(SB16_WRITE_CMD_PORT, (uint8_t)(((uint16_t)dsp_block_size) >> 8));
    
    return true;
}




void sb16_interrupt_handler(SoundCardObject *self) {
    (void)self;  //TODO
    uint8_t interr_source = ack_interrupt();
    (void)interr_source;//TODO
}

int sb16_negotiate_irq_assignment() {
    isa_free_handler free_irq_10 = get_isa_free_device_callback(SB16_DESIRED_IRQ); 
    if (free_irq_10 != NULL) return -1;  // the required irq number is already used by some other device
    
    uint8_t irq_register;
    read_mixer_register(SB16_MIXER_REGISTER_IRQ, &irq_register);
    irq_register = irq_register & 0b11110000;  // save the upper "undefined" bits
    irq_register = irq_register | sb16_irq_set_bit(SB16_DESIRED_IRQ);
    write_mixer_register(SB16_MIXER_REGISTER_IRQ, irq_register);
    
    return SB16_DESIRED_IRQ;
}

bool sb16_default_sink(SoundCardObject *self, uint8_t* samples, size_t byte_count) {
    (void)self;  // TODO
    (void)samples;  // TODO
    (void)byte_count;  // TODO
    
    return true;
}



//TODO: currently, only a single soundblaster 16 is allowed

SoundCardObject* sb16_autodetect() {
    if (!detect_initialize_sb16()) return false;
    
    SoundCardObject *card = (SoundCardObject*) malloc(sizeof(SoundCardObject));
    strncpy(card->name, "SoundBlaster 16", MAX_SOUNDCARD_NAME);
    
    card->interrupt_callback = &sb16_interrupt_handler;
    card->negotiate_irq_assignment = &sb16_negotiate_irq_assignment;
    card->default_sink = &sb16_default_sink;
    
    sb16_set_dma_channel(SB16_DESIRED_ISA_DMA_CHANNEL);
    
    return card;
}
