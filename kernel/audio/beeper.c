#include <audio/beeper.h>
#include <interrupt/pit.h>
#include <interrupt/timing.h>

#include <ipc/interface.h>
#include <sys/audio.h>
#include <sys/default_topics.h>
#include <security/defaults.h>


bool beeper_is_playing;


void system_beep(uint16_t frequency, unsigned long long int duration) {
    if (frequency != 0) {
        set_pit_beep_frequency(frequency);
        start_pit_beep();
        wait_milliseconds(duration);
        stop_pit_beep();
    }
}


static void handle_beeper_event_message(const void *message) {
    AudioEventMessage *event = (AudioEventMessage*) message;
    
    if (beeper_is_playing && (event->flags == 0 || event->frequency == 0)) {
        stop_pit_beep();
        beeper_is_playing = false;
    }
    else if (!beeper_is_playing && (event->flags & AUDIO_EVENT_MSG_FLAG_NOTE_ON) && event->frequency != 0) {
        set_pit_beep_frequency((uint16_t)event->frequency);
        start_pit_beep();
        beeper_is_playing = true;
    }
    else if (beeper_is_playing && event->frequency != 0) {
        stop_pit_beep();
        set_pit_beep_frequency((uint16_t)event->frequency);
        start_pit_beep();
    }
}


void setup_beeper() {
    beeper_is_playing = false;
    
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_AUDIO_BEEPER_TOPIC, 1024, sizeof(AudioEventMessage), DEFAULT_BEEPER_TOPIC_RACE, DEFAULT_BEEPER_TOPIC_CLASS, DEFAULT_BEEPER_TOPIC_POLICY);
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_AUDIO_BEEPER_TOPIC, &handle_beeper_event_message);
}
