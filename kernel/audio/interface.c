#include <audio/interface.h>
#include <audio/beeper.h>
#include <audio/soundcard.h>


int audio_readylevel_internal = 0;
 

static int audio_ready_level() {
    return audio_readylevel_internal;
}

static void audio_setup() {
    setup_beeper();
    setup_soundcard();
    audio_readylevel_internal = 100;
}

static void audio_teardown() {
    audio_readylevel_internal = 0;
}


UNIQUE_IMPLEMENTATION_OF(audio, subsystem, { \
    .ready_level = &audio_ready_level, \
    .setup = &audio_setup, \
    .teardown = &audio_teardown, \
})
