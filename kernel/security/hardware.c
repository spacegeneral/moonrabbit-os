#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <mm/mm.h>

#include <security/hardware.h>
#include <security/identify.h>


// All the agents with the same security clearance share the same page management entry
PageManagementObject page_management_entries[NUM_SECURITY_CLEARANCE_LEVELS][NUM_SECURITY_CLEARANCE_LEVELS];


#define ITERATE_OVER_PAGE_MGR_ENTRIES(currname, body) \
{ \
    for (int race_level_iter=0; race_level_iter<NUM_SECURITY_CLEARANCE_LEVELS; race_level_iter++) { \
        for (int class_level_iter=0; class_level_iter<NUM_SECURITY_CLEARANCE_LEVELS; class_level_iter++) { \
            PageManagementObject *currname = &(page_management_entries[race_level_iter][class_level_iter]); \
            body \
        } \
    } \
}



PageManagementObject* new_page_management_entry(PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib) {
    PageManagementObject *selected = &(page_management_entries[phys_race_attrib][data_class_attrib]);
    
    if (!selected->valid) {
        // setup a space in heap for 1024 page directory entries + 
        // 1024*1204 page table entries
        size_t needed_space = (PAGE_DIR_ENTRIES * sizeof(PageDirecEntry)) + \
                              (PAGE_DIR_ENTRIES * PAGE_TAB_ENTRIES * sizeof(PageTableEntry));
        uint8_t *mem = (uint8_t*) allocate_pages(fit_into_pages(needed_space), 0);
        //printf("new page mgr @ %p, owner %d %d\n", mem, phys_race_attrib, data_class_attrib);
        selected->page_directory = (PageDirecEntry*) mem;
        selected->page_tables = (PageTableEntry*) (mem + (PAGE_DIR_ENTRIES * sizeof(PageDirecEntry)));
        
        // all the user maps are initialized to a copy of the master kernel memory map
        if ((phys_race_attrib != God) && (data_class_attrib != Emperor)) {
            PageManagementObject *master_kernel_map = get_page_management_entry(God, Emperor);
            memcpy(mem, master_kernel_map->page_directory, needed_space);
        }
    }
    
    selected->valid = true;
    return selected;
}

PageManagementObject* get_page_management_entry(PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib) {
    PageManagementObject *selected = &(page_management_entries[phys_race_attrib][data_class_attrib]);
    if (!selected->valid) return NULL;
    return selected;
}

PageManagementObject* get_page_management_entry_owned_by(IdentifiedAgent *owner) {
    if (owner->uid == AGENT_NOT_PRESENT_ID) return NULL;
    PageManagementObject *selected = &(page_management_entries[owner->phys_race_attrib][owner->data_class_attrib]);
    if (!selected->valid) return NULL;
    return selected;
}

uint32_t get_cr3_from_page_mgr_object(PageManagementObject *entry) {
    assert(entry->valid);
    return (uint32_t)entry->page_directory;
}


// When an agent takes ownership of a memory range, all the more privileged agents also automatically
// gain (hardware) ownership of it, meaning that a highly-privileged agent can access
// the memory of lesser agents at will.
// To do so, simply update the page management entries of the existing agents with higher privilege
// than the current owner, with ONE EXCEPTION: the master kernel map is left untouched.
void cascade_set_ownership(void *start_virt_address, size_t num_pages, IdentifiedAgent *owner) {
    if ((owner->phys_race_attrib == God) && (owner->data_class_attrib == Emperor)) {
        cascade_kernel_appropriate_ownership(start_virt_address, num_pages);
        return;
    }
    
    for (unsigned race_level_iter=1; race_level_iter<=owner->phys_race_attrib; race_level_iter++) {
        for (unsigned class_level_iter=1; class_level_iter<=owner->data_class_attrib; class_level_iter++) {
            PageManagementObject *current = &(page_management_entries[race_level_iter][class_level_iter]);
            if (current->valid) {
                // don't bother finding the actual agent (or agents!!) bound to a particular page manager object,
                // instead instance a throwaway one who's only useful for containing the security clearance attributes.
                // such a short, sad life.
                IdentifiedAgent other_owner = throwaway_dummy_agent(race_level_iter, class_level_iter, true);
                //printf("Transfer ownership of %p [%lu] to %d %d (mgr: %p)\n", (uintptr_t)start_virt_address, num_pages, race_level_iter, class_level_iter, (uintptr_t)(get_page_management_entry_owned_by(&other_owner)->page_directory));
                set_ownership_range(start_virt_address, num_pages, &other_owner, true);
            }
        }
    }
    
    refresh_tlb();
}

void cascade_kernel_appropriate_ownership(void *start_virt_address, size_t num_pages) {
    //IdentifiedAgent *owner = get_agent(0);
    for (unsigned race_level_iter=1; race_level_iter<NUM_SECURITY_CLEARANCE_LEVELS; race_level_iter++) {
        for (unsigned class_level_iter=1; class_level_iter<NUM_SECURITY_CLEARANCE_LEVELS; class_level_iter++) {
            PageManagementObject *current = &(page_management_entries[race_level_iter][class_level_iter]);
            if (current->valid) {
                IdentifiedAgent other_owner = throwaway_dummy_agent(race_level_iter, class_level_iter, false);
                set_ownership_range(start_virt_address, num_pages, &other_owner, false);
            }
        }
    }
    refresh_tlb();
}

void cascade_set_guard_pages(void *start_virt_address, size_t num_pages) {
    //IdentifiedAgent *owner = get_agent(0);
    for (unsigned race_level_iter=0; race_level_iter<NUM_SECURITY_CLEARANCE_LEVELS; race_level_iter++) {
        for (unsigned class_level_iter=0; class_level_iter<NUM_SECURITY_CLEARANCE_LEVELS; class_level_iter++) {
            PageManagementObject *current = &(page_management_entries[race_level_iter][class_level_iter]);
            if (current->valid) {
                IdentifiedAgent other_owner = throwaway_dummy_agent(race_level_iter, class_level_iter, false);
                set_pages_as_guard(start_virt_address, num_pages, &other_owner);
            }
        }
    }
    refresh_tlb();
}

void cascade_clear_guard_pages(void *start_virt_address, size_t num_pages) {
    //IdentifiedAgent *owner = get_agent(0);
    for (unsigned race_level_iter=0; race_level_iter<NUM_SECURITY_CLEARANCE_LEVELS; race_level_iter++) {
        for (unsigned class_level_iter=0; class_level_iter<NUM_SECURITY_CLEARANCE_LEVELS; class_level_iter++) {
            PageManagementObject *current = &(page_management_entries[race_level_iter][class_level_iter]);
            if (current->valid) {
                IdentifiedAgent other_owner = throwaway_dummy_agent(race_level_iter, class_level_iter, false);
                set_pages_not_guard(start_virt_address, num_pages, &other_owner);
            }
        }
    }
    refresh_tlb();
}


void setup_hardware_security_bookeeping() {
    ITERATE_OVER_PAGE_MGR_ENTRIES(current, {
        current->page_directory = NULL;
        current->page_tables = NULL;
        current->valid = false;
    });
}


