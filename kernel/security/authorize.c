#include <security/authorize.h>
#include <security/identify.h>
#include <data/vfs.h>
#include <task/task.h>


bool authorize_simple(PhisicalDomainClass resource_race, 
                      DataDomainClass resource_class,
                      PhisicalDomainClass agent_race, 
                      DataDomainClass agent_class) {
    return (resource_race >= agent_race) && (resource_class >= agent_class);
}

bool authorize_agent_id(PhisicalDomainClass resource_race, 
                        DataDomainClass resource_class,
                        unsigned int agent_id) {
    IdentifiedAgent *agent = get_agent(agent_id);
    if (agent == NULL) return false;
    
    return authorize_simple(resource_race, 
                            resource_class, 
                            agent->phys_race_attrib, 
                            agent->data_class_attrib);
}

bool authorize_agent_id_write_up(PhisicalDomainClass channel_race, 
                                 DataDomainClass channel_class,
                                 SecurityPolicy policy,
                                 unsigned int agent_id) {
    if (policy & SecNoWriteUp) {
        return authorize_agent_id(channel_race, channel_class, agent_id);
    } else {
        return true;
    }
}

bool authorize_agent_id_read_up(PhisicalDomainClass channel_race, 
                                DataDomainClass channel_class,
                                SecurityPolicy policy,
                                unsigned int agent_id) {
    if (policy & SecNoReadUp) {
        return authorize_agent_id(channel_race, channel_class, agent_id);
    } else {
        return true;
    }
}

bool authorize_task_id(PhisicalDomainClass resource_race, 
                       DataDomainClass resource_class,
                       unsigned int task_id) {
    Task *task = get_task(task_id);
    if (task == NULL) return false;
    
    return authorize_agent_id(resource_race, 
                              resource_class, 
                              task->owner_id);
}

bool authorize_task_id_write_up(PhisicalDomainClass channel_race, 
                                DataDomainClass channel_class,
                                SecurityPolicy policy,
                                unsigned int task_id) {
    if (policy & SecNoWriteUp) {
        return authorize_task_id(channel_race, channel_class, task_id);
    } else {
        return true;
    }
}

bool authorize_task_id_read_up(PhisicalDomainClass channel_race, 
                               DataDomainClass channel_class,
                               SecurityPolicy policy,
                               unsigned int task_id) {
    if (policy & SecNoReadUp) {
        return authorize_task_id(channel_race, channel_class, task_id);
    } else {
        return true;
    }
}

bool authorize_file(FileDescriptor *resource,
                    PhisicalDomainClass agent_race, 
                    DataDomainClass agent_class) {
    return authorize_simple(resource->metadata.race_attrib, 
                            resource->metadata.class_attrib, 
                            agent_race, 
                            agent_class);
}
