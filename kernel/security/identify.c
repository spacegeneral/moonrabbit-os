#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <security/identify.h>

#include <data/fsdial.h>
#include <data/path.h>
#include <security/defaults.h>


IdentifiedAgent *identified_agents;



unsigned int add_new_agent(PhisicalDomainClass phys_race_attrib, 
                           DataDomainClass data_class_attrib, 
                           char *human_name,
                           bool is_user) {
    unsigned int new_uid = get_free_agent_id();
    if (new_uid == AGENT_NOT_PRESENT_ID) return AGENT_NOT_PRESENT_ID;
    IdentifiedAgent *newagent = identified_agents + new_uid;
    newagent->uid = new_uid;
    newagent->phys_race_attrib = phys_race_attrib;
    newagent->data_class_attrib = data_class_attrib;
    newagent->is_user = is_user;
    strncpy(newagent->human_name, human_name, MAX_AGENT_NAME);
    newagent->human_name[MAX_AGENT_NAME-1] = '\0';
    return new_uid;
}

unsigned int get_free_agent_id() {
    for (unsigned int uid=0; uid<MAX_AGENTS; uid++) {
        if (identified_agents[uid].uid == AGENT_NOT_PRESENT_ID) return uid;
    }
    return AGENT_NOT_PRESENT_ID;
}


void delete_agent(unsigned int uid) {
    identified_agents[uid].uid = AGENT_NOT_PRESENT_ID;
}


IdentifiedAgent* get_agent(unsigned int uid) {
    if (identified_agents[uid].uid != AGENT_NOT_PRESENT_ID) {
        return identified_agents + uid;
    }
    return NULL;
}

IdentifiedAgent throwaway_dummy_agent(PhisicalDomainClass phys_race_attrib, DataDomainClass data_class_attrib, bool is_user) {
    IdentifiedAgent agent;
    agent.phys_race_attrib = phys_race_attrib;
    agent.data_class_attrib = data_class_attrib;
    agent.uid = 0;
    agent.is_user = is_user;
    snprintf(agent.human_name, MAX_AGENT_NAME, "throwaway");
    return agent;
}


static size_t agents_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    size_t written = 0;
    
    for (size_t t=0; t<MAX_AGENTS; t++) {
        IdentifiedAgent *curragent = identified_agents + t;
        if (curragent->uid == AGENT_NOT_PRESENT_ID) continue;
        written += snprintf(dest+written, maxsize-written, "%d: %s the %s %s (%s)\n",
                            curragent->uid, curragent->human_name, 
                            phisical_domain_class_name[curragent->phys_race_attrib],
                            data_domain_class_name[curragent->data_class_attrib], 
                            (curragent->is_user ? "user" : "kernel"));
    }
    
    return written;
}


void setup_identification_special_files() {
    FileDescriptor* dialfile_h;
    FileDescriptor* dest_folder;
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_INFODIAL_RACE,
        .class_attrib = DEFAULT_SEC_INFODIAL_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    dial_fs_primitives.create_file(&dialfile_h, "agents", meta);
    dial_bind_callback(dialfile_h, &agents_dial_interface);
    
    resolve_path(OS_DIALS_H_PATH, &dest_folder);
    
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_h);
}


void setup_identification_system() {
    identified_agents = (IdentifiedAgent*) malloc(sizeof(IdentifiedAgent) * MAX_AGENTS);
    for (size_t a=0; a<MAX_AGENTS; a++) {
        identified_agents[a].uid = AGENT_NOT_PRESENT_ID;
    }
    add_new_agent(God, Emperor, "kernel", false);
    add_new_agent(Angel, King, "mountain", false);
}

