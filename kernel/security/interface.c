#include <security/interface.h>
#include <security/identify.h>
#include <security/hardware.h>
#include <kernel/kidou.h>
#include <stdio.h>


int security_readylevel_internal = 0;

static int security_ready_level() {
    return security_readylevel_internal;
}

static void security_initialize() {
#ifdef VERBOSE_STARTUP
    printf("Initializing security subsystem.\n    Setting up ident system... ");
#endif
    setup_identification_system();
#ifdef VERBOSE_STARTUP
    printf("done!\n    Setting up HW security bookeeping... \n");
#endif    
    setup_hardware_security_bookeeping();
#ifdef VERBOSE_STARTUP
    printf("done!\nSecurity subsystem 50%% ready.\n");
#endif
    security_readylevel_internal = 50;
}

static void security_finalize() {
#ifdef VERBOSE_STARTUP
    printf("Finalizing security subsystem.\n    Setting up special files... ");
#endif
    setup_identification_special_files();
#ifdef VERBOSE_STARTUP
    printf("done!\nSecurity subsystem 100%% ready.\n");
#endif
    security_readylevel_internal = 100;
}

static void security_teardown() {
    security_readylevel_internal = 0;
}


UNIQUE_IMPLEMENTATION_OF(security, data_subsystem, { \
    .ready_level = &security_ready_level, \
    .initialize = &security_initialize, \
    .finalize = &security_finalize, \
    .teardown = &security_teardown \
})
