#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <specio.h>
#include <string.h>
#include <sys/term.h>
#include <sys/errno.h>

#include <kernel/tty.h>
#include <kernel/rice.h>
#include <kernel/log.h>
#include <ipc/interface.h>
#include <security/defaults.h>
#include <security/identify.h>
#include <security/hardware.h>
#include <mm/mm.h>

#include <libgui/bitmap.h>


#ifdef VBE
#include <graphics/vbe.h>

unsigned char* screen;

size_t vbe_width;
size_t vbe_height;
size_t vbe_pitch;
size_t vbe_bpp;
int vbe_writing;

int vbe_terminal_readylevel_internal;


UNIQUE_IMPLEMENTATION_OF(vbe, terminal, { \
    .initialize = &vbe_terminal_initialize, \
    .finalize = &vbe_terminal_implementation_specific_finalize, \
    .teardown = &vbe_terminal_teardown, \
    .ready_level = &vbe_terminal_ready_level, \
    .putchar = &vbe_terminal_putchar, \
    .cls = &vbe_terminal_cls, \
    .scrolldown = &vbe_terminal_scroll_down, \
    .gotoxy = &vbe_terminal_gotoxy, \
    .set_colors = &vbe_terminal_set_colors, \
    .set_scroll_range = &vbe_terminal_set_scroll_range, \
    .direct_video_command = &vbe_terminal_direct_video_command, \
    .getinfo = &vbe_terminal_getinfo, \
    .getbuffers = &vbe_terminal_internal_getbuffers, \
    .enable_cursor = &vbe_terminal_enable_cursor, \
    .disable_cursor = &vbe_terminal_disable_cursor, \
    .update_cursor = &vbe_terminal_update_cursor, \
    .set_writing_direction = &vbe_set_writing_direction, \
    .set_term_flags = &vbe_set_term_flags, \
    .display_bitmap = &vbe_display_bitmap \
})


void vbe_terminal_initialize(void *video_memory, size_t width, size_t height, size_t bpp, size_t pitch) {
    screen = (unsigned char*) video_memory;
    vbe_width = width;
    vbe_height = height;
    vbe_pitch = pitch;
    vbe_bpp = bpp/8;
    vbe_writing = WD_BOOK;
    vbe_init_textmode();
    vbe_terminal_enable_cursor();
    vbe_terminal_readylevel_internal = 50;
}

int vbe_terminal_ready_level() {
    return vbe_terminal_readylevel_internal;
}

void vbe_terminal_implementation_specific_finalize() {
    klog("VBE screen framebuffer address: 0x%x (%d bytes per pixel), length: %d bytes (%d pages)\n", screen, vbe_bpp, FRAMEBUFFER_SIZE, fit_into_pages(FRAMEBUFFER_SIZE));

    vbe_terminal_readylevel_internal = 100;
}

int vbe_terminal_direct_video_command(int verb, int argument, void *result, unsigned int agent) {
    switch (verb) {
        case DVToggleGrabFB:
            // TODO handle grab conflicts
            if (argument == DVC_TOGGLE_GRAB) {
                unsigned char** memptr = (unsigned char**) result;
                
                cascade_set_ownership((void*)screen, fit_into_pages(FRAMEBUFFER_SIZE), get_agent(agent));
                *memptr = screen;
            } else {
                cascade_set_ownership((void*)screen, fit_into_pages(FRAMEBUFFER_SIZE), get_agent(0));
            }
            break;
        default:
            return NOT_IMPLEMENTED;
    }
    
    return 0;
}


void vbe_terminal_teardown() {}



void vbe_terminal_internal_getbuffers(Array *append_sections_here) {
    TaskSection_append_new(append_sections_here, "VBE linear framebuffer", (uintptr_t)screen, (size_t)FRAMEBUFFER_SIZE);
}





void vbe_scroll_up(size_t numpixels, int fillcolor) {
    size_t scrollspace = numpixels * vbe_pitch;
    unsigned char *screenpos = screen + (vbe_scroll_top * vbe_pitch);
    
    for (size_t yb = (vbe_scroll_top / numpixels); yb < (vbe_scroll_bottom / numpixels) - 1; yb++) {
        memcpy(screenpos, screenpos + scrollspace, scrollspace);
        screenpos += scrollspace;
    }
    
    for (size_t y = 0; y < numpixels; y++) {
        for (size_t x = 0; x < vbe_width; x++) {
            vbe_putpixel_quick(screenpos, fillcolor);
            screenpos += vbe_bpp;
        }
    }
}

void vbe_scroll_down(size_t numpixels, int fillcolor) {
    size_t scrollspace = numpixels * vbe_pitch;
    unsigned char *screenpos = screen + ((vbe_scroll_bottom - numpixels*2) * vbe_pitch);
    
    for (size_t yb = (vbe_scroll_top / numpixels); yb < (vbe_scroll_bottom / numpixels) - 1; yb++) {
        memcpy(screenpos, screenpos - scrollspace, scrollspace);
        screenpos -= scrollspace;
    }
    
    screenpos = screen + (vbe_scroll_top * vbe_pitch);
    for (size_t y = 0; y <= numpixels; y++) {
        for (size_t x = 0; x < vbe_width; x++) {
            vbe_putpixel_quick(screenpos, fillcolor);
            screenpos += vbe_bpp;
        }
    }
}

void vbe_scroll_left(size_t numpixels, int fillcolor) {
    (void)numpixels;
    (void)fillcolor;
    //TODO stub
}

void vbe_scroll_right(size_t numpixels, int fillcolor) {
    (void)numpixels;
    (void)fillcolor;
    //TODO stub
}


void vbe_fill_rect(Rect *rect, int fillcolor) {
    for (int y = rect->y; y < rect->y+rect->h; y++) {
        for (int x = rect->x; x < rect->x+rect->w; x++) {
            vbe_putpixel(x, y, fillcolor);
        }
    }
}


void vbe_display_bitmap(Bitmap *bitmap, int colorkey) {
    int* colorkey_ptr = NULL;
    if (colorkey != COLORKEY_NONE) colorkey_ptr = &colorkey;
    Bitmap screenbmp = {
        .geom = {.x = 0, .y = 0, .w = (int)vbe_width, .h = (int)vbe_height},
        .colordata = (int*)screen
    };
    safe_overlay_bitmap(&screenbmp, bitmap, colorkey_ptr);
    vbe_take_cursor_patch();
}


void vbe_set_writing_direction(WritingDirection writing) {
    vbe_writing = (int)writing;
}

void vbe_set_term_flags(unsigned int flags) {
    vbe_term_flags = flags;
}



#endif
