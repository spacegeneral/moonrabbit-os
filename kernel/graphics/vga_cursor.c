#include <asmutils.h>
#include <stdint.h>
#include <kernel/tty.h>
#include <kernel/rice.h>

#ifdef VGA
#include <graphics/vga.h>

size_t vga_cursor_x;
size_t vga_cursor_y;
bool vga_cursor_enabled;

void vga_terminal_enable_cursor() {
    outb(0x3D4, 0x0A);
    outb(0x3D5, (inb(0x3D5) & 0xC0) | VGA_CURSOR_SCANLINE_START);

    outb(0x3D4, 0x0B);
    outb(0x3D5, (inb(0x3E0) & 0xE0) | VGA_CURSOR_SCANLINE_END);
    
    vga_cursor_enabled = true;
}

void vga_terminal_disable_cursor() {
    outb(0x3D4, 0x0A);
    outb(0x3D5, 0x20);
    
    vga_cursor_enabled = false;
}

void vga_terminal_update_cursor(int x, int y) {
    if (x>=0 && x<(int)vga_width && y>=0 && y<(int)vga_height) {
        vga_cursor_x = x;
        vga_cursor_y = y;
        uint16_t pos = y * vga_width + x;

        outb(0x3D4, 0x0F);
        outb(0x3D5, (uint8_t) (pos & 0xFF));
        outb(0x3D4, 0x0E);
        outb(0x3D5, (uint8_t) ((pos >> 8) & 0xFF));
    }
}

#endif
