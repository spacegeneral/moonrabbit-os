#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unicode.h>

#include <kernel/tty.h>
#include <kernel/bitmap_font.h>
#include <kernel/rice.h>

#define TAB_SPAN 8

#ifdef VBE
#include <graphics/vbe.h>
#include <utf8decoder.h>

static int terminal_row;
static int terminal_column;
static int num_rows;
static int num_columns;
int vbe_scroll_top;
int vbe_scroll_bottom;

static int terminal_color;
static int terminal_background;
unsigned int vbe_term_flags;

static bool cursor_enabled = false;
static int cursor_row = 0;
static int cursor_column = 0;

int cursor_patch[(CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR) * (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR) * 4];
bool has_cursor_patch = false;

static Utf8Decoder decoder;


void vbe_init_textmode() {
    terminal_row = 0;
    terminal_column = 0;
    vbe_term_flags = 0;
    terminal_color = TTY_FOREGROUND;
    terminal_background = TTY_BACKGROUND;  // black
    num_columns = (int)vbe_width / (CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR);
    num_rows = (int)vbe_height / (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR);
    vbe_scroll_top = 0;
    vbe_scroll_bottom = vbe_height;
    
    bitmap_font_table = default_bitmap_font;
    bitmap_font_upper_range = sizeof(default_bitmap_font) / sizeof(BitmapCodePoint);
    utf8_decoder_init(&decoder);
    vbe_cls(TTY_BACKGROUND);
}

TermInfo vbe_terminal_getinfo() {
    TermInfo info = {
        .termsize = {
            .x = 0,
            .y = 0,
            .w = num_columns,
            .h = num_rows,
        },
        .screensize = {
            .x = 0,
            .y = 0,
            .w = vbe_width,
            .h = vbe_height
        },
        .cursor = {
            .x = cursor_column,
            .y = cursor_row,
        },
        .writing = vbe_writing,
        .cursor_enabled = cursor_enabled,
        .support_bitmap = 1,
        .foreground_color = terminal_color,
        .background_color = terminal_background,
        .scroll_top = vbe_scroll_top / (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR),
        .scroll_bottom = vbe_scroll_bottom / (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR),
        .flags = vbe_term_flags
    };
    return info;
}

void vbe_terminal_set_colors(int foreground, int background) {
    if (foreground >= 0) terminal_color = foreground;
    if (background >= 0) terminal_background = background;
}


void vbe_cls(int color) {
    Rect screenrect = {
        .x = 0,
        .y = 0,
        .w = vbe_width,
        .h = vbe_height
    };
    vbe_fill_rect(&screenrect, color);
}


int vbe_draw_codepoint_anywhere(size_t start_x, size_t start_y, int codepoint, 
                                int color, bool with_background) {
    BitmapCodePoint chardata = ((size_t)codepoint >= bitmap_font_upper_range ? undefined_character : bitmap_font_table[codepoint]);
    size_t char_width, char_width_power, char_height;
    
    switch (chardata.dimension) {
        case CHAR_8x16:
            char_width = 8;
            char_width_power = 3;
            char_height = 16;
            break;
        case CHAR_16x16:
            char_width = 16;
            char_width_power = 4;
            char_height = 16;
            break;
        default:
            return chardata.dimension;
    }
    
    char_width <<= FONT_SCALE_FACTOR;
    char_width_power += FONT_SCALE_FACTOR;
    char_height <<= FONT_SCALE_FACTOR;
    
    size_t ypace_power = char_width_power - 3 - FONT_SCALE_FACTOR;
    size_t carriage_return = vbe_bpp << char_width_power;
    
    unsigned char *screenpos = screen + (start_y * vbe_pitch) + (start_x * vbe_bpp);
    
    if (!with_background) {
        for (size_t y = 0; y < char_height; y++) {
            for (size_t x = 0; x < char_width; x++) {
                size_t ysel = y >> FONT_SCALE_FACTOR;
                size_t xsel = x >> FONT_SCALE_FACTOR;
                char row = chardata.bitmap[(ysel << ypace_power) + (xsel >> 3)];
                if (row & (0b10000000 >> (xsel % 8))) vbe_putpixel_quick(screenpos, color);
                screenpos += vbe_bpp;
            }
            screenpos = screenpos + vbe_pitch - carriage_return;
        }
    } else {
        for (size_t y = 0; y < char_height; y++) {
            for (size_t x = 0; x < char_width; x++) {
                size_t ysel = y >> FONT_SCALE_FACTOR;
                size_t xsel = x >> FONT_SCALE_FACTOR;
                char row = chardata.bitmap[(ysel << ypace_power) + (xsel >> 3)];
                if (row & (0b10000000 >> (xsel % 8))) vbe_putpixel_quick(screenpos, color);
                else vbe_putpixel_quick(screenpos, terminal_background);
                screenpos += vbe_bpp;
            }
            screenpos = screenpos + vbe_pitch - carriage_return;
        }
    }
    
    if (vbe_term_flags & TERM_FLAG_UNDERLINE) {
        screenpos = screen + ((start_y + char_height - 1) * vbe_pitch) + (start_x * vbe_bpp);
        for (size_t x = 0; x < char_width; x++) {
            vbe_putpixel_quick(screenpos, color);
            screenpos += vbe_bpp;
        }
    }
    
    
    return chardata.dimension;
}


int vbe_draw_codepoint(size_t col, size_t row, int codepoint, int color) {
    size_t start_x = (col) * (CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR);
    size_t start_y = (row) * (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR);
    
    return vbe_draw_codepoint_anywhere(start_x, start_y, codepoint, color, true);
}

int vbe_fill_codepoint_block(size_t col, size_t row, int dimension, int color) {
    size_t char_width, char_width_power, char_height, area;
    size_t start_x = (col) * (CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR);
    size_t start_y = (row) * (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR);
    
    switch (dimension) {
        case CHAR_8x16:
            char_width = 8;
            char_width_power = 3;
            char_height = 16;
            area = 128;
            break;
        case CHAR_16x16:
            char_width = 16;
            char_width_power = 4;
            char_height = 16;
            area = 256;
            break;
        default:
            return dimension;
    }
    
    char_width <<= FONT_SCALE_FACTOR;
    char_width_power += FONT_SCALE_FACTOR;
    char_height <<= FONT_SCALE_FACTOR;
    
    for (size_t index=0; index<area; index++) {
        size_t x = index % char_width;
        size_t y = index >> char_width_power;
        vbe_putpixel(x + start_x, y + start_y, color);
    }
    
    return dimension;
}

void vbe_terminal_scroll_up() {
    vbe_scroll_up(CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR, terminal_background);
}

void vbe_terminal_scroll_down() {
    vbe_scroll_down(CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR, terminal_background);
}

void vbe_terminal_scroll_left() {
    vbe_scroll_left(CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR, terminal_background);
}

void vbe_terminal_scroll_right() {
    vbe_scroll_right(CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR, terminal_background);
}

void vbe_terminal_set_scroll_range(int scroll_top, int scroll_bottom) {
    if (scroll_top < 0 || scroll_bottom < 0) return;
    
    if (scroll_bottom <= scroll_top) return;
    if (scroll_bottom > num_rows) return;
    if (scroll_top > num_rows) return;
    
    vbe_scroll_top = scroll_top * (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR);
    vbe_scroll_bottom = scroll_bottom * (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR);
}


#define NEWLINE_BOOK() \
    terminal_column = 0; \
    terminal_row++; \
    if (terminal_row >= num_rows) { \
        vbe_terminal_scroll_up(); \
        terminal_row--; \
    }
    
#define NEWLINE_RABBI() \
    terminal_column = num_columns-1; \
    terminal_row++; \
    if (terminal_row >= num_rows) { \
        vbe_terminal_scroll_up(); \
        terminal_row--; \
    }
    
#define NEWLINE_TEGAMI() \
    terminal_row = 0; \
    terminal_column--; \
    if (terminal_column < 0) { \
        vbe_terminal_scroll_left(); \
        terminal_column++; \
        terminal_column=num_columns-1; /* TODO temp fix only, because SoftTerminal_scroll_left is a stub */ \
    }

#define NEWLINE_LEMURIA() \
    terminal_row = num_rows-1; \
    terminal_column++; \
    if (terminal_column >= num_columns) { \
        vbe_terminal_scroll_right(); \
        terminal_column--; \
        terminal_column=0; /* TODO temp fix only, because SoftTerminal_scroll_right is a stub */ \
    }
    
#define NEWLINE() \
    switch (vbe_writing) { \
        case WD_BOOK: \
            NEWLINE_BOOK() \
        break; \
        case WD_RABBI: \
            NEWLINE_RABBI() \
        break; \
        case WD_TEGAMI: \
            NEWLINE_TEGAMI() \
        break; \
        case WD_LEMURIA: \
            NEWLINE_LEMURIA() \
        break; \
        default: break; \
    }


#define BACKSPACE() \
    switch (vbe_writing) { \
        case WD_BOOK: \
            if (terminal_column-1 >= 0) terminal_column--; \
        break; \
        case WD_RABBI: \
            if (terminal_column+1 < num_columns) terminal_column++; \
        break; \
        case WD_TEGAMI: \
            if (terminal_row-1 >= 0) terminal_row--; \
        break; \
        case WD_LEMURIA: \
            if (terminal_row+1 < num_rows) terminal_row++; \
        break; \
        default: break; \
    }

#define CARRIAGE_RETURN() \
    switch (vbe_writing) { \
        case WD_BOOK: \
            terminal_column = 0; \
        break; \
        case WD_RABBI: \
            terminal_column = num_columns-1; \
        break; \
        case WD_TEGAMI: \
            terminal_row = 0; \
        break; \
        case WD_LEMURIA: \
            terminal_row = num_columns-1; \
        break; \
        default: break; \
    }
    
#define PRE_ADVANCE() \
    switch (vbe_writing) { \
        case WD_RABBI: \
            for (int a=0; a<advance; a++) { \
                terminal_column--; \
                if (terminal_column < 0) { \
                    NEWLINE_RABBI() \
                } \
            } \
        break; \
        case WD_LEMURIA: \
            terminal_row--; \
            if (terminal_row < 0) { \
                NEWLINE_LEMURIA() \
            } \
        break; \
        default: break; \
    }
    
#define POST_ADVANCE() \
    switch (vbe_writing) { \
        case WD_BOOK: \
            for (int a=0; a<advance; a++) { \
                terminal_column++; \
                if (terminal_column >= num_columns) { \
                    NEWLINE_BOOK() \
                } \
            } \
        break; \
        case WD_RABBI: \
            terminal_column--; \
            if (terminal_column < 0) { \
                NEWLINE_RABBI() \
            } \
        break; \
        case WD_TEGAMI: \
            terminal_row++; \
            if (terminal_row >= num_rows) { \
                NEWLINE_TEGAMI() \
            } \
        break; \
        case WD_LEMURIA: \
            terminal_row--; \
            if (terminal_row < 0) { \
                NEWLINE_LEMURIA() \
            } \
        break; \
        default: break; \
    }
    
#define ROTATE_SKIPBACK() \
{ \
    int tmp; \
    switch (vbe_writing) { \
        case WD_BOOK: \
        break; \
        case WD_RABBI: \
            delta_x = -delta_x; \
        break; \
        case WD_TEGAMI: \
            tmp = delta_y; \
            delta_y = delta_x; \
            delta_x = -tmp; \
        break; \
        case WD_LEMURIA: \
            tmp = delta_y; \
            delta_y = -delta_x; \
            delta_x = tmp; \
        break; \
        default: break; \
    } \
}

    
static int vbe_calc_tabdelta() {
    int stoppos;
    int deltapos;
    
    switch (vbe_writing) {
        case WD_BOOK:
            if (terminal_column % TAB_SPAN == 0) {
                stoppos = terminal_column + TAB_SPAN;
            } else {
                stoppos = ((terminal_column / TAB_SPAN) + 1) * TAB_SPAN;
            }
            deltapos = stoppos - terminal_column;
        break;
        case WD_RABBI:
            if (terminal_column % TAB_SPAN == 0) {
                stoppos = terminal_column - TAB_SPAN;
            } else {
                stoppos = ((terminal_column / TAB_SPAN) - 1) * TAB_SPAN;
            }
            deltapos = stoppos - terminal_column;
        break;
        case WD_TEGAMI:
            if (terminal_row % TAB_SPAN == 0) {
                stoppos = terminal_row + TAB_SPAN;
            } else {
                stoppos = ((terminal_row / TAB_SPAN) + 1) * TAB_SPAN;
            }
            deltapos = stoppos - terminal_row;
        break;
        case WD_LEMURIA:
            if (terminal_row % TAB_SPAN == 0) {
                stoppos = terminal_row - TAB_SPAN;
            } else {
                stoppos = ((terminal_row / TAB_SPAN) - 1) * TAB_SPAN;
            }
            deltapos = stoppos - terminal_row;
        break;
        
        default:
            deltapos = 0;
    }
    
    deltapos = clamp_pos(deltapos);
    return deltapos;
}



void terminal_putcodepoint(int c) {
    // erase the current cursor cell
    if (cursor_enabled) {
        vbe_clean_cursor();
    }
    
    if (c == '\n') {
        
        NEWLINE();
        
    } else if (c == '\t') {
        
        int deltax = vbe_calc_tabdelta();
        for (int dx=0; dx<deltax; dx++) {
            terminal_putcodepoint(' ');
        }
        
    } else if (c == '\b') {
        
        BACKSPACE()
        
        vbe_fill_codepoint_block(terminal_column, terminal_row, CHAR_8x16, terminal_background);
        
    } else if (c == '\r') {
        
        CARRIAGE_RETURN()
        
    } else {
        
        int skipback_factor = combining_factor(c);
        
        if (skipback_factor == NOT_COMBINING) {
            int advance = ((size_t)c >= bitmap_font_upper_range ? 0 : bitmap_font_table[c].dimension);
            
            PRE_ADVANCE()
            
            advance = vbe_draw_codepoint(terminal_column, terminal_row, c, terminal_color)+1;
            
            POST_ADVANCE()
            
        } else {
            int delta_x = ((((int)terminal_column) * CHAR_WIDTH_UNIT ) + skipback_factor) << FONT_SCALE_FACTOR;
            int delta_y = (terminal_row * (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR));
            ROTATE_SKIPBACK();
            int start_x = delta_x;
            int start_y = delta_y;
            
            if (start_x < 0) {
                start_x = (num_columns * (CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR)) + start_x;
                start_y -= CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR;
                if (start_y < 0) start_y = 0;
            }
            
            vbe_draw_codepoint_anywhere((size_t)start_x, (size_t)start_y, c, terminal_color, false);
        }
        
    }
    
    vbe_update_cursor(terminal_column, terminal_row);
}

void vbe_terminal_putchar(char c) {
    int codepoint = utf8_decoder_feed(&decoder, c);
    if (codepoint > 0) {
        terminal_putcodepoint(process_codepoint(vbe_term_flags, codepoint, bitmap_font_upper_range));
    }
}

void vbe_terminal_gotoxy(size_t x, size_t y) {
    if ((int)x<num_columns && (int)y<num_rows) {
        terminal_row = (int)y;
        terminal_column = (int)x;
    }
}

void vbe_terminal_cls() {
    vbe_cls(TTY_BACKGROUND);
}

void vbe_clean_cursor() {
    size_t patch_width = (CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR);
    size_t patch_height = (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR);
    
    int *pixel_from = cursor_patch;
    unsigned char *screenpos = screen + (cursor_row * patch_height * vbe_pitch) + (cursor_column * patch_width * vbe_bpp);
    size_t carriage_return = vbe_bpp * (CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR);
    
    if (has_cursor_patch) {
        has_cursor_patch = false;
        for (size_t y=0; y<patch_height; y++) {
            for (size_t x = 0; x < patch_width; x++) {
                vbe_putpixel_quick(screenpos, *pixel_from);
                pixel_from += 1;
                screenpos += vbe_bpp;
            }
            screenpos = screenpos + vbe_pitch - carriage_return;
        }
    }
}

void vbe_take_cursor_patch() {
    size_t patch_width = (CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR);
    size_t patch_height = (CHAR_HEIGHT_UNIT << FONT_SCALE_FACTOR);
    
    int *pixel_to = cursor_patch;
    unsigned char *screenpos = screen + (cursor_row * patch_height * vbe_pitch) + (cursor_column * patch_width * vbe_bpp);
    size_t carriage_return = vbe_bpp * (CHAR_WIDTH_UNIT << FONT_SCALE_FACTOR);
    
    has_cursor_patch = true;
    
    for (size_t y=0; y<patch_height; y++) {
        for (size_t x = 0; x < patch_width; x++) {
            *pixel_to = vbe_getpixel_quick(screenpos);
            pixel_to += 1;
            screenpos += vbe_bpp;
        }
        screenpos = screenpos + vbe_pitch - carriage_return;
    }
}

void vbe_update_cursor(int x, int y) {
    
    if (x<num_columns && y<num_rows) {
        cursor_row = y;
        cursor_column = x;
        
        if (cursor_enabled) {
            vbe_take_cursor_patch();
            vbe_draw_codepoint(cursor_column, cursor_row, TTY_CURSOR, terminal_color);
        }
    }
}

void vbe_terminal_enable_cursor() {
    cursor_enabled = true;
    vbe_take_cursor_patch();
    vbe_draw_codepoint(cursor_column, cursor_row, TTY_CURSOR, terminal_color);
}
void vbe_terminal_disable_cursor() {
    cursor_enabled = false;
    vbe_clean_cursor();
}
void vbe_terminal_update_cursor(int x, int y) {
    if (x < 0 || y < 0) return;
    if (cursor_enabled) vbe_clean_cursor();
    vbe_update_cursor(x, y);
}



#endif
