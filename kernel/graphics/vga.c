#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <yakumo.h>
#include <sys/errno.h>
#include <sys/term.h>

#include <kernel/tty.h>
#include <kernel/rice.h>
#include <kernel/log.h>
#include <mm/mm.h>


#define TAB_SPAN 8

#ifdef VGA
#include <graphics/vga.h>

static uint16_t* const VGA_MEMORY = (uint16_t*) 0xB8000;

static int terminal_row;
static int terminal_column;
static uint8_t terminal_color;
static uint16_t* terminal_buffer;
int vga_scroll_top;
int vga_scroll_bottom;
unsigned int vga_term_flags;

int vga_width;
int vga_height;

int vga_writing;


int vga_terminal_readylevel_internal;


UNIQUE_IMPLEMENTATION_OF(vga, terminal, { \
    .initialize = &vga_terminal_initialize, \
    .finalize = &vga_terminal_implementation_specific_finalize, \
    .teardown = &vga_terminal_teardown, \
    .ready_level = &vga_terminal_ready_level, \
    .putchar = &vga_terminal_putchar, \
    .cls = &vga_terminal_cls, \
    .scrolldown = &vga_terminal_scrolldown, \
    .gotoxy = &vga_terminal_gotoxy, \
    .set_colors = &vga_terminal_set_colors, \
    .set_scroll_range = &vga_terminal_set_scroll_range, \
    .direct_video_command = &vga_terminal_direct_video_command, \
    .getinfo = &vga_terminal_getinfo, \
    .getbuffers = &vga_terminal_internal_getbuffers, \
    .enable_cursor = &vga_terminal_enable_cursor, \
    .disable_cursor = &vga_terminal_disable_cursor, \
    .update_cursor = &vga_terminal_update_cursor, \
    .set_writing_direction = &vga_set_writing_direction, \
    .set_term_flags = &vga_set_term_flags, \
    .display_bitmap = &vga_display_bitmap, \
})


void vga_terminal_initialize(void *video_memory, size_t width, size_t height, size_t bpp, size_t pitch) {
    (void) video_memory;
    (void) bpp;
    (void) pitch;
    vga_width = (int)width;
    vga_height = (int)height;
    vga_scroll_top = 0;
    vga_scroll_bottom = height;
    terminal_row = 0;
    terminal_column = 0;
    vga_writing = WD_BOOK;
    terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
    terminal_buffer = VGA_MEMORY;
    vga_cls(terminal_color);
    vga_terminal_enable_cursor();
    vga_terminal_readylevel_internal = 50;
}

int vga_terminal_ready_level() {
    return vga_terminal_readylevel_internal;
}

void vga_terminal_implementation_specific_finalize() {
    klog("VGA screen framebuffer address: 0x%x (%d bytes per pixel), length: %d bytes (%d pages)\n", terminal_buffer, 2, 32768, fit_into_pages(32768));
    vga_terminal_readylevel_internal = 100;
}

int vga_terminal_direct_video_command(int verb, int argument, void *result, unsigned int agent) {
    (void)verb;
    (void)argument;
    (void)result;
    (void)agent;
    
    return NOT_IMPLEMENTED;
}

void vga_terminal_teardown() {}

void vga_terminal_internal_getbuffers(Array *append_sections_here) {
    TaskSection_append_new(append_sections_here, "VGA linear framebuffer", (uintptr_t)(0x000A0000), 131072);
}

TermInfo vga_terminal_getinfo() {
    TermInfo info = {
        .termsize = {
            .x = 0,
            .y = 0,
            .w = vga_width,
            .h = vga_height,
        },
        .screensize = {
            .x = 0,
            .y = 0,
            .w = vga_width * CHAR_WIDTH_UNIT,
            .h = vga_height * CHAR_HEIGHT_UNIT,
        },
        .cursor = {
            .x = vga_cursor_x,
            .y = vga_cursor_y,
        },
        .writing = vga_writing,
        .cursor_enabled = vga_cursor_enabled,
        .support_bitmap = 0,
        .foreground_color = vga_to_24bpp_map[(terminal_color & 0x0F)],
        .background_color = vga_to_24bpp_map[(terminal_color & 0xF0) >> 4],
        .scroll_top = vga_scroll_top,
        .scroll_bottom = vga_scroll_bottom,
        .flags = vga_term_flags
    };
    return info;
}

static uint8_t color24_to_color8(int color24) {
    uint8_t best_match = 0;
    long best_match_value = 99999999;
    
    long color_r = (color24 & 0x00FF0000) >> 16;
    long color_g = (color24 & 0x0000FF00) >> 8;
    long color_b = (color24 & 0x000000FF);
    
    for (uint8_t c=0; c<NUM_VGA_COLORS; c++) {
        long candidate_r = (vga_to_24bpp_map[c] & 0x00FF0000) >> 16;
        long candidate_g = (vga_to_24bpp_map[c] & 0x0000FF00) >> 8;
        long candidate_b = (vga_to_24bpp_map[c] & 0x000000FF);
        
        long diff_r = (color_r - candidate_r) * (color_r - candidate_r);
        long diff_g = (color_g - candidate_g) * (color_g - candidate_g);
        long diff_b = (color_b - candidate_b) * (color_b - candidate_b);
        
        long diff = diff_r + diff_g + diff_b;
        if (diff < best_match_value) {
            best_match_value = diff;
            best_match = c;
        }
    }
    return best_match;
}

void vga_terminal_set_colors(int foreground, int background) {
    uint8_t curr_fg = terminal_color & 0x0F;
    uint8_t curr_bg = (terminal_color & 0xF0) >> 4;
    if (foreground >= 0) curr_fg = color24_to_color8(foreground) & 0x0F;
    if (background >= 0) curr_bg = color24_to_color8(background) & 0x0F;
    terminal_color = vga_entry_color(curr_fg, curr_bg);
}

void vga_cls(uint8_t color) {
    for (int y = 0; y < vga_height; y++) {
        for (int x = 0; x < vga_width; x++) {
            const int index = y * vga_width + x;
            terminal_buffer[index] = vga_entry(' ', color);
        }
    }
}

void terminal_putentryat(unsigned char c, uint8_t color, int x, int y) {
    const int index = y * vga_width + x;
    terminal_buffer[index] = vga_entry(c, color);
}

void vga_terminal_gotoxy(size_t x, size_t y) {
    if ((int)x >= vga_width) x = vga_width-1;
    if ((int)y >= vga_height) y = vga_height-1;
    terminal_column = (int)x;
    terminal_row = (int)y;
}

void vga_terminal_scroll_up() {
    for (int y = 0; y < vga_height-1; y++) {
        for (int x = 0; x < vga_width; x++) {
            const int current = y * vga_width + x;
            const int underneath = (y + 1) * vga_width + x;
            terminal_buffer[current] = terminal_buffer[underneath];
        }
    }
    for (int x = 0; x < vga_width; x++) {
        const int lowest = (vga_height - 1) * vga_width + x;
        terminal_buffer[lowest] = vga_entry(' ', terminal_color);
    }
}

void vga_terminal_scrolldown() {
    for (int y = 1; y < vga_height; y++) {
        for (int x = 0; x < vga_width; x++) {
            const int current = y * vga_width + x;
            const int overhead = (y - 1) * vga_width + x;
            terminal_buffer[current] = terminal_buffer[overhead];
        }
    }
    for (int x = 0; x < vga_width; x++) {
        const int highest = x;
        terminal_buffer[highest] = vga_entry(' ', terminal_color);
    }
}

void vga_terminal_scroll_left() {
    //TODO stub
}

void vga_terminal_scroll_right() {
    //TODO stub
}


#define NEWLINE_BOOK() \
    terminal_column = 0; \
    terminal_row++; \
    if (terminal_row >= vga_height) { \
        vga_terminal_scroll_up(); \
        terminal_row--; \
    }
    
#define NEWLINE_RABBI() \
    terminal_column = vga_width-1; \
    terminal_row++; \
    if (terminal_row >= vga_height) { \
        vga_terminal_scroll_up(); \
        terminal_row--; \
    }
    
#define NEWLINE_TEGAMI() \
    terminal_row = 0; \
    terminal_column--; \
    if (terminal_column < 0) { \
        vga_terminal_scroll_left(); \
        terminal_column++; \
        terminal_column=vga_width-1; /* TODO temp fix only, because SoftTerminal_scroll_left is a stub */ \
    }

#define NEWLINE_LEMURIA() \
    terminal_row = vga_height-1; \
    terminal_column++; \
    if (terminal_column >= vga_width) { \
        vga_terminal_scroll_right(); \
        terminal_column--; \
        terminal_column=0; /* TODO temp fix only, because SoftTerminal_scroll_right is a stub */ \
    }
    
#define NEWLINE() \
    switch (vga_writing) { \
        case WD_BOOK: \
            NEWLINE_BOOK() \
        break; \
        case WD_RABBI: \
            NEWLINE_RABBI() \
        break; \
        case WD_TEGAMI: \
            NEWLINE_TEGAMI() \
        break; \
        case WD_LEMURIA: \
            NEWLINE_LEMURIA() \
        break; \
        default: break; \
    }


#define BACKSPACE() \
    switch (vga_writing) { \
        case WD_BOOK: \
            if (terminal_column-1 >= 0) terminal_column--; \
        break; \
        case WD_RABBI: \
            if (terminal_column+1 < vga_width) terminal_column++; \
        break; \
        case WD_TEGAMI: \
            if (terminal_row-1 >= 0) terminal_row--; \
        break; \
        case WD_LEMURIA: \
            if (terminal_row+1 < vga_height) terminal_row++; \
        break; \
        default: break; \
    }

#define CARRIAGE_RETURN() \
    switch (vga_writing) { \
        case WD_BOOK: \
            terminal_column = 0; \
        break; \
        case WD_RABBI: \
            terminal_column = vga_width-1; \
        break; \
        case WD_TEGAMI: \
            terminal_row = 0; \
        break; \
        case WD_LEMURIA: \
            terminal_row = vga_width-1; \
        break; \
        default: break; \
    }
    
#define POST_ADVANCE() \
    switch (vga_writing) { \
        case WD_BOOK: \
            terminal_column++; \
            if (terminal_column >= vga_width) { \
                NEWLINE_BOOK() \
            } \
        break; \
        case WD_RABBI: \
            terminal_column--; \
            if (terminal_column < 0) { \
                NEWLINE_RABBI() \
            } \
        break; \
        case WD_TEGAMI: \
            terminal_row++; \
            if (terminal_row >= vga_height) { \
                NEWLINE_TEGAMI() \
            } \
        break; \
        case WD_LEMURIA: \
            terminal_row--; \
            if (terminal_row < 0) { \
                NEWLINE_LEMURIA() \
            } \
        break; \
        default: break; \
    }
    
static int vga_calc_tabdelta() {
    int stoppos;
    int deltapos;
    
    switch (vga_writing) {
        case WD_BOOK:
            if (terminal_column % TAB_SPAN == 0) {
                stoppos = terminal_column + TAB_SPAN;
            } else {
                stoppos = ((terminal_column / TAB_SPAN) + 1) * TAB_SPAN;
            }
            deltapos = stoppos - terminal_column;
        break;
        case WD_RABBI:
            if (terminal_column % TAB_SPAN == 0) {
                stoppos = terminal_column - TAB_SPAN;
            } else {
                stoppos = ((terminal_column / TAB_SPAN) - 1) * TAB_SPAN;
            }
            deltapos = stoppos - terminal_column;
        break;
        case WD_TEGAMI:
            if (terminal_row % TAB_SPAN == 0) {
                stoppos = terminal_row + TAB_SPAN;
            } else {
                stoppos = ((terminal_row / TAB_SPAN) + 1) * TAB_SPAN;
            }
            deltapos = stoppos - terminal_row;
        break;
        case WD_LEMURIA:
            if (terminal_row % TAB_SPAN == 0) {
                stoppos = terminal_row - TAB_SPAN;
            } else {
                stoppos = ((terminal_row / TAB_SPAN) - 1) * TAB_SPAN;
            }
            deltapos = stoppos - terminal_row;
        break;
        
        default:
            deltapos = 0;
    }
    
    deltapos = clamp_pos(deltapos);
    return deltapos;
}
    
void vga_terminal_putchar(char c) {
    if (c == 0) return;
    
    if (c == '\n') {
        
        NEWLINE();
        
    } else if (c == '\t') {
        
        int deltax = vga_calc_tabdelta();
        for (int dx=0; dx<deltax; dx++) {
            vga_terminal_putchar(' ');
        }
        
    } else if (c == '\b') {
        
        BACKSPACE()
        
        terminal_putentryat(' ', terminal_color, terminal_column, terminal_row);
    } else if (c == '\r') {
        
        CARRIAGE_RETURN()
        
    } else {
        unsigned char uc = c;
        terminal_putentryat(uc, terminal_color, terminal_column, terminal_row);
        
        POST_ADVANCE()
    }
    vga_terminal_update_cursor(terminal_column, terminal_row);
}

void vga_terminal_set_scroll_range(int scroll_top, int scroll_bottom) {
    if (scroll_top < 0 || scroll_bottom < 0) return;
    
    if (scroll_bottom <= scroll_top) return;
    if (scroll_bottom > (int)vga_height) return;
    if (scroll_top > (int)vga_height) return;
    
    vga_scroll_top = scroll_top;
    vga_scroll_bottom = scroll_bottom;
}

void vga_terminal_cls() {
    vga_cls(terminal_color);
}

void vga_set_writing_direction(WritingDirection writing) {
    vga_writing = (int)writing;
}

void vga_set_term_flags(unsigned int flags) {
    vga_term_flags = flags;
}

void vga_display_bitmap(Bitmap *bitmap, int colorkey) {
    (void)colorkey;
    int stopy = min((vga_height-1), max((bitmap->geom.h / CHAR_HEIGHT_UNIT), 1));
    int stopx = min((vga_width-1), max((bitmap->geom.w / CHAR_WIDTH_UNIT), 1));
    for (int yimg = terminal_row; yimg < stopy + terminal_row; yimg++) {
        for (int ximg = terminal_column; ximg < stopx + terminal_column; ximg++) {
            bool x_edge = (ximg == terminal_column) || (ximg == (stopx+terminal_column-1));
            bool y_edge = (yimg == terminal_row) || (yimg == (stopy+terminal_row-1));
            if (x_edge && y_edge) {
                terminal_putentryat('+', terminal_color, ximg, yimg);
            } else if (x_edge) {
                terminal_putentryat('|', terminal_color, ximg, yimg);
            } else if (y_edge) {
                terminal_putentryat('-', terminal_color, ximg, yimg);
            } else {
                terminal_putentryat('.', terminal_color, ximg, yimg);
            }
        }
    }
}



#endif
