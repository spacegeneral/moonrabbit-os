#include <stdio.h>
#include <stdlib.h>
#include <kernel/bitmap_font.h>


BitmapCodePoint *bitmap_font_table;
size_t bitmap_font_upper_range;


bool load_lbf_file(char *path) {
    bitmap_font_table = NULL;
    
    FILE *fp = fopen(path, "rb");
    if (fp == NULL) return false;
    
    BitmapFontHeader header;
    if (fread(&header, sizeof(BitmapFontHeader), 1, fp) != 1) {
        fclose(fp);
        return false;
    }
    
    BitmapCodePoint *custom_bitmap_font = (BitmapCodePoint*) malloc(sizeof(BitmapCodePoint) * header.upper_range);
    
    if (fread(custom_bitmap_font, sizeof(BitmapCodePoint), header.upper_range, fp) < header.upper_range) {
        free(custom_bitmap_font);
        fclose(fp);
        return false;
    }

    bitmap_font_table = custom_bitmap_font;
    bitmap_font_upper_range = header.upper_range;
    fclose(fp);
    return true;
}

