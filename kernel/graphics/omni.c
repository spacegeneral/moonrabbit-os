#include <sys/term.h>
#include <kernel/tty.h>
#include <kernel/log.h>
#include <ipc/interface.h>
#include <security/defaults.h>
#include <sys/default_topics.h>

int terminal_readylevel_internal = 0;


static void terminal_out_wrapper(const void *message) {
    TermOutMessage *msg = (TermOutMessage*) message;
    
    switch (msg->verb) {
        case SetCursorPos:
            CALL_IMPL(video, terminal, update_cursor, msg->msg.position.x, msg->msg.position.y);
            CALL_IMPL(video, terminal, gotoxy, msg->msg.position.x, msg->msg.position.y);
            break;
        case ToggleCursor:
            if (msg->msg.toggle.enabled) {CALL_IMPL(video, terminal, enable_cursor);}
            else {CALL_IMPL(video, terminal, disable_cursor);}
            break;
        case SetColors:
            CALL_IMPL(video, terminal, set_colors, msg->msg.colors.foreground, msg->msg.colors.background);
            break;
        case SetScrollRange:
            CALL_IMPL(video, terminal, set_scroll_range, msg->msg.range.start, msg->msg.range.end);
            break;
        case ScrollReverse:
            CALL_IMPL(video, terminal, scrolldown);
            break;
        case CLS:
            CALL_IMPL(video, terminal, cls);
            break;
        case Putchar:
            CALL_IMPL(video, terminal, putchar, msg->msg.character.character);
            break;
        case Puttag:
            // TODO implement me maybe
            break;
        case SetWritingDirection:
            CALL_IMPL(video, terminal, set_writing_direction, msg->msg.writing.direction);
            break;
        case SetTermFlags:
            CALL_IMPL(video, terminal, set_term_flags, msg->msg.flags.flags & TERM_FLAG_RW_MASK);
            break;
        case SetZoomLevel:
            // We don't implement this in the kernel
            break;
        default:
            klog("Received unknown terminal command: %d\n", msg->verb);
    }
}


static bool terminal_info_wrapper(void *request, unsigned int request_uid, unsigned int requestor_task_id) {
    (void)request_uid; // unused
    (void)requestor_task_id; // unused
    TermInfo info = CALL_IMPL(video, terminal, getinfo);
    memcpy(request, &info, sizeof(TermInfo));
    return true;
}


static bool terminal_drawbitmap_wrapper(void *request, unsigned int request_uid, unsigned int requestor_task_id) {
    (void)request_uid; // unused
    (void)requestor_task_id; // unused
    TermBitmapRequest *req = (TermBitmapRequest *) request;
    
    CALL_IMPL(video, terminal, display_bitmap, req->bitmap, req->colorkey);
    
    req->status = 0;

    return true;
}


void omni_terminal_finalize() {
    CALL_IMPL(video, terminal, finalize);
    
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_STDOUT_TOPIC, 4096, sizeof(char), DEFAULT_TERMINAL_OUT_TOPICS_RACE, DEFAULT_TERMINAL_OUT_TOPICS_CLASS, DEFAULT_TERMINAL_OUT_TOPICS_POLICY);
    
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_TERMOUT_TOPIC, 4096, sizeof(TermOutMessage), DEFAULT_TERMINAL_CMD_TOPICS_RACE, DEFAULT_TERMINAL_CMD_TOPICS_CLASS, DEFAULT_TERMINAL_OUT_TOPICS_POLICY);
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_TERMOUT_TOPIC, &terminal_out_wrapper);
    
    CALL_IMPL(ipc, service, advertise_service, DEFAULT_TERMINFO_SERVICE, sizeof(TermInfo), DEFAULT_TERMINAL_INFO_TOPICS_RACE, DEFAULT_TERMINAL_INFO_TOPICS_CLASS);
    CALL_IMPL(ipc, service, kernel_accept, DEFAULT_TERMINFO_SERVICE, &terminal_info_wrapper);
    
    CALL_IMPL(ipc, service, advertise_service, DEFAULT_TERM_DRAWBITMAP_SERVICE, sizeof(TermBitmapRequest), DEFAULT_TERMINAL_OUT_TOPICS_RACE, DEFAULT_TERMINAL_OUT_TOPICS_CLASS);
    CALL_IMPL(ipc, service, kernel_accept, DEFAULT_TERM_DRAWBITMAP_SERVICE, &terminal_drawbitmap_wrapper);
}
