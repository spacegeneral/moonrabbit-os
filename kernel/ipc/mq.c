#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <ipc/mq.h>
#include <collections/hashtable.h>
#include <collections/array.h>
#include <kernel/log.h>
#include <kernel/core.h>
#include <mm/mm.h>
#include <data/fsdial.h>
#include <data/path.h>
#include <security/defaults.h>


/*
 * 
 * The message queue is handled as a ringbuffer
 * 
 * 
 * [ ] empty cell
 * [#] full cell
 * [_] don't care cell
 * 
 * 
 * Empty condition: read and write heads on the same cell
 * 
 * r,w
 *  |
 *  V
 * [ |_|_|_|_|_|_|_|_|_|_|_|_]
 * 
 * 
 * Write 3 entries: the write head advances 3 cells
 * (NOTE: the spot under the write head is empty by definition)
 * 
 *  r     w
 *  |     |
 *  V     V
 * [#|#|#| |_|_|_|_|_|_|_|_|_]
 * 
 * 
 * Read 1 entry: the read head advances 1 cell
 * 
 *    r   w
 *    |   |
 *    V   V
 * [_|#|#| |_|_|_|_|_|_|_|_|_]
 * 
 * 
 * Fill up the whole ring:
 * (NOTE: the r,w heads wrap around the end of the buffer)
 * 
 *  w r   
 *  | |   
 *  V V   
 * [ |#|#|#|#|#|#|#|#|#|#|#|#]
 * 
 * Add one entry to the ring:
 * (NOTE: the write head cannot catch up to the read head,
 *  but the reverse is fine)
 * 
 * 1. the write head checks if advancing would cause a
 *    catch up with the read head
 *    1.1. if so, advance the read head by 1 cell, effectively
 *         discarding the content of the oldest cell
 * 2. write as normal
 * 
 *    w r   
 *    | |   
 *    V V   
 * [#| |#|#|#|#|#|#|#|#|#|#|#]
 *    ^
 *    the content of this cell is lost
 * 
 */


HashTable *channels_by_topic;


MessageTopic *register_new_channel(char *name, 
                                   unsigned int max_queue_length, 
                                   unsigned int message_length, 
                                   PhisicalDomainClass phys_race_attrib, 
                                   DataDomainClass data_class_attrib, 
                                   SecurityPolicy policy) {
    if (hashtable_contains_key(channels_by_topic, name)) {
        return NULL;
    }
    
    MessageTopic *newtopic = (MessageTopic*) malloc(sizeof(MessageTopic));
    
    snprintf(newtopic->name, MAX_TOPIC_LENGTH, "%s", name);
    newtopic->max_queue_length = max_queue_length;
    newtopic->message_length = message_length;
    newtopic->phys_race_attrib = phys_race_attrib;
    newtopic->data_class_attrib = data_class_attrib;
    newtopic->policy = policy;
    array_new(&newtopic->subscribers);
    array_new(&newtopic->kernel_subscribers);
    
    int status = hashtable_add(channels_by_topic, newtopic->name, newtopic);
    if (status != CC_OK) {
        printf("Cannot add new topic to table!\n");
        panic();
    }
    
    if (memcmp(newtopic->name, "/sys/", 5) == 0)
        klog("Registered new topic \"%s\" with clearance %s %s (0x%X), msg_len=%d, queue_len=%d\n", newtopic->name, phisical_domain_class_name[newtopic->phys_race_attrib], data_domain_class_name[newtopic->data_class_attrib], newtopic->policy, newtopic->message_length, newtopic->max_queue_length);
    
    return newtopic;
}

bool mq_retire_channel(MessageTopic *channel) {
    if (hashtable_contains_key(channels_by_topic, channel->name)) {
       
        ARRAY_FOREACH(curr, channel->subscribers, {
            MessageSubscriber *current = (MessageSubscriber*) curr;
            free(current->queue);
            free(current);
        });
        
        array_destroy(channel->subscribers);
        array_destroy(channel->kernel_subscribers);
        hashtable_remove(channels_by_topic, channel->name, NULL);
        if (memcmp(channel->name, "/sys/", 5) == 0) klog("Retired topic \"%s\"\n", channel->name);
        free(channel);
        
        return true;
    }
    
    return false;
}


bool has_subscriber(MessageTopic *channel, unsigned int task_id) {
    ARRAY_FOREACH(curr, channel->subscribers, {
        MessageSubscriber *current = (MessageSubscriber*) curr;
        if (current->task_id == task_id) {
            return true;
        }
    });
    return false;
}

void subscribe_to_channel(MessageTopic *channel, unsigned int task_id) {
    if (has_subscriber(channel, task_id)) {
        return;
    }
    
    MessageSubscriber *newsub = (MessageSubscriber*) malloc(sizeof(MessageSubscriber));
    
    newsub->task_id = task_id;
    newsub->queue = malloc(channel->message_length * channel->max_queue_length);
    newsub->ring_read_head = 0;
    newsub->ring_write_head = 0;
    newsub->topic = channel;
    
    array_add(channel->subscribers, newsub);
    
    //klog("Task %d subscribed to topic \"%s\"\n", task_id, channel->name);
}

void kernel_subscribe_to_channel(MessageTopic *channel, kernel_mq_subscriber callback) {
    array_add(channel->kernel_subscribers, callback);
}

bool kernel_unsubscribe_from_channel(MessageTopic *channel, kernel_mq_subscriber callback) {
    ARRAY_FOREACH(curr, channel->kernel_subscribers, {
        kernel_mq_subscriber current = (kernel_mq_subscriber) curr;
        if (current == callback) {
            array_remove(channel->kernel_subscribers, current, NULL);
            return true;
        }
    });
    return false;
}

bool unsubscribe_from_channel(MessageTopic *channel, unsigned int task_id) {
    if (!has_subscriber(channel, task_id)) {
        return false;
    }
    
    ARRAY_FOREACH(curr, channel->subscribers, {
        MessageSubscriber *current = (MessageSubscriber*) curr;
        if (current->task_id == task_id) {
            array_remove(channel->subscribers, current, NULL);
            free(current->queue);
            free(current);
            //klog("Task %d unsubscribed from topic \"%s\"\n", task_id, channel->name);
            return true;
        }
    });
    
    return false;
}


MessageTopic *get_channel_by_name(char *name) {
    if (!hashtable_contains_key(channels_by_topic, name)) {
        return NULL;
    }
    MessageTopic *channel;
    hashtable_get(channels_by_topic, name, (void**)&channel);
    return channel;
}


#define ADVANCED_WRITE_HEAD ((subscriber->ring_write_head + 1) % max_queue)
#define ADVANCED_READ_HEAD ((subscriber->ring_read_head + 1) % max_queue)

bool channel_pop(MessageSubscriber *subscriber, void *dest, size_t msglen, size_t max_queue) {
    uint8_t *ring_bytes = (uint8_t*) subscriber->queue;
    
    if (subscriber->ring_read_head == subscriber->ring_write_head) {
        return false;
    }
    
    memcpy(dest,
           ring_bytes + msglen*subscriber->ring_read_head, 
           msglen);
    
    subscriber->ring_read_head = ADVANCED_READ_HEAD;
    return true;
}

// return true if a ringbuffer overrun is imminent
static bool enqueue_new_message(MessageSubscriber *subscriber, const void *message, size_t msglen, size_t max_queue) {
    uint8_t *ring_bytes = (uint8_t*) subscriber->queue;
    
    if (ADVANCED_WRITE_HEAD == subscriber->ring_read_head) {  // ring overrun: discard oldest entry
        subscriber->ring_read_head = ADVANCED_READ_HEAD;  // the write head cannot catch up to the read head
    }
    
    memcpy(ring_bytes + msglen*subscriber->ring_write_head, // copy the message into the free spot
           message, 
           msglen);

    subscriber->ring_write_head = ADVANCED_WRITE_HEAD;  // advance the write head
    
    return ADVANCED_WRITE_HEAD == subscriber->ring_read_head; // overrun imminent!
}

bool next_write_causes_overrun(MessageTopic *channel) {
    size_t max_queue = channel->max_queue_length;
    ARRAY_FOREACH(curr, channel->subscribers, {
        MessageSubscriber *subscriber = (MessageSubscriber*) curr;
        if (ADVANCED_WRITE_HEAD == subscriber->ring_read_head) return true;
    });
    return false;
}

#undef ADVANCED_WRITE_HEAD
#undef ADVANCED_READ_HEAD

bool publish_message(MessageTopic *channel, const void *message) {
    ARRAY_FOREACH(curr, channel->kernel_subscribers, {
        kernel_mq_subscriber current = (kernel_mq_subscriber) curr;
        current(message);
    });
    
    bool overrun_imminent = false;
    ARRAY_FOREACH(curr, channel->subscribers, {
        MessageSubscriber *current = (MessageSubscriber*) curr;
        bool op_overrun_imminent = enqueue_new_message(current, message, channel->message_length, channel->max_queue_length);
        overrun_imminent = overrun_imminent || op_overrun_imminent;
    });
    
    return overrun_imminent;
}

bool poll_channel(MessageTopic *channel, unsigned int task_id, void *dest) {
    ARRAY_FOREACH(curr, channel->subscribers, {
        MessageSubscriber *current = (MessageSubscriber*) curr;
        if (current->task_id == task_id) {
            return channel_pop(current, dest, channel->message_length, channel->max_queue_length);
        }
    });
    return false;
}

bool fill_in_channel_stats(MessageTopic* channel, ChannelStats* stats) {
    strncpy(stats->name, channel->name, MAX_TOPIC_LENGTH);
    stats->phys_race_attrib = channel->phys_race_attrib;
    stats->data_class_attrib = channel->data_class_attrib;
    stats->policy = channel->policy;
    stats->max_queue_length = channel->max_queue_length;
    stats->message_length = channel->message_length;
    return true;
}

void mq_global_unsubscribe(unsigned int task_id) {
    TableEntry *current_entry;
    MessageTopic *current;
    HASHTABLE_FOREACH(current_entry, channels_by_topic, {
        current = (MessageTopic*) current_entry->value;
        unsubscribe_from_channel(current, task_id);
    });
}


static size_t mq_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    size_t written = 0;
    
    TableEntry *current_entry;
    MessageTopic *current;
    HASHTABLE_FOREACH(current_entry, channels_by_topic, {
        current = (MessageTopic*) current_entry->value;
        written += snprintf(dest+written, maxsize-written, "%s\n", current->name);
    });
    
    return written;
}

void mq_make_system_dial_files() {
    FileDescriptor* dialfile_h;
    FileDescriptor* dest_folder;
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_INFODIAL_RACE,
        .class_attrib = DEFAULT_SEC_INFODIAL_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    dial_fs_primitives.create_file(&dialfile_h, "topics", meta);
    dial_bind_callback(dialfile_h, &mq_dial_interface);
    resolve_path(OS_DIALS_H_PATH, &dest_folder);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_h);
}

void setup_mq_system() {
    hashtable_new(&channels_by_topic);
}
