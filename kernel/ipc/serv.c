#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <ipc/serv.h>
#include <collections/hashtable.h>
#include <collections/array.h>
#include <kernel/log.h>
#include <kernel/core.h>
#include <task/task.h>

#include <data/fsdial.h>
#include <data/path.h>
#include <security/defaults.h>


HashTable *services_by_topic;


ServiceTopic *register_new_service(char *topic_name, 
                                   unsigned int frame_length, 
                                   PhisicalDomainClass phys_race_attrib, 
                                   DataDomainClass data_class_attrib) {
    if (hashtable_contains_key(services_by_topic, topic_name)) {
        return NULL;
    }
    
    ServiceTopic *newtopic = (ServiceTopic*) malloc(sizeof(ServiceTopic));
    
    snprintf(newtopic->name, MAX_TOPIC_LENGTH, "%s", topic_name);
    newtopic->frame_length = frame_length;
    newtopic->phys_race_attrib = phys_race_attrib;
    newtopic->data_class_attrib = data_class_attrib;
    
    array_new(&newtopic->processes);
    array_new(&newtopic->kernel_acceptors);
    
    int status = hashtable_add(services_by_topic, newtopic->name, newtopic);
    if (status != CC_OK) {
        printf("Cannot add new service to table!\n");
        panic();
    }
    
    if (memcmp(newtopic->name, "/sys/", 5) == 0)
        klog("Registered new service \"%s\" with clearance %s %s, frame_len=%d\n", newtopic->name, phisical_domain_class_name[newtopic->phys_race_attrib], data_domain_class_name[newtopic->data_class_attrib], newtopic->frame_length);
    
    return newtopic;
}


bool serv_retire_service(ServiceTopic *service) {
    if (hashtable_contains_key(services_by_topic, service->name)) {
        
        ARRAY_FOREACH(curr, service->processes, {
            ServiceProcess *current = (ServiceProcess*) curr;
            free(current);
        });
        
        array_destroy(service->processes);
        array_destroy(service->kernel_acceptors);
        
        if (memcmp(service->name, "/sys/", 5) == 0) klog("Retired service \"%s\"\n", service->name);
        hashtable_remove(services_by_topic, service->name, NULL);
        free(service);
        
        return true;
    }
    
    return false;
}

ServiceTopic *get_service_by_name(char *name) {
    if (!hashtable_contains_key(services_by_topic, name)) {
        return NULL;
    }
    ServiceTopic *service;
    hashtable_get(services_by_topic, name, (void**)&service);
    return service;
}


bool is_process_dead(ServiceProcess *process) {
    switch (process->status) {
        case OFFERED:
            return !task_exists(process->requestor_task);
        case ACCEPTED:  /* fallthrough */
        case COMPLETED:
            return !(task_exists(process->requestor_task) && task_exists(process->acceptor_task));
    }
    return false;
}

bool remove_single_dead_process(ServiceTopic *service) {
    ARRAY_FOREACH(curr, service->processes, {
        ServiceProcess *current = (ServiceProcess*) curr;
        if (is_process_dead(current)) {
            free(current);
            array_remove(service->processes, current, NULL);
            return true;
        }
    });
    return false;
}

void prune_all_dead_processes(ServiceTopic *service) {
    bool removal_done = false;
    do {
        removal_done = remove_single_dead_process(service);
    } while (removal_done);
}


ServiceProcess *process_by_request_id(ServiceTopic *service, unsigned int request_uid) {
    prune_all_dead_processes(service);
    ARRAY_FOREACH(curr, service->processes, {
        ServiceProcess *current = (ServiceProcess*) curr;
        if (current->request_id == request_uid) {
            return current;
        }
    });
    return NULL;
}

bool accept_service_request(ServiceTopic *service, 
                            void *request, 
                            unsigned int *requestor_task_id, 
                            unsigned int *request_uid, 
                            unsigned int acceptor_task_id) {
    prune_all_dead_processes(service);

    ARRAY_FOREACH(curr, service->processes, {
        ServiceProcess *current = (ServiceProcess*) curr;
        if (current->status == OFFERED) {
            current->response_ptr = request;
            if (current->response_ptr != current->request_ptr) {
                memcpy(current->response_ptr, current->request_ptr, service->frame_length);
            }
            *request_uid = current->request_id;
            *requestor_task_id = current->requestor_task;
            current->acceptor_task = acceptor_task_id;
            current->status = ACCEPTED;
            return true;
        }
    });
    
    return false;
}


void kernel_accept(ServiceTopic *service, kernel_serv_acceptor callback) {
    array_add(service->kernel_acceptors, callback);
}




void complete_service_request(ServiceTopic *service, unsigned int request_uid) {
    ServiceProcess *request_process = process_by_request_id(service, request_uid);
    if (request_process->response_ptr != request_process->request_ptr) {
        memcpy(request_process->request_ptr, request_process->response_ptr, service->frame_length);
    }
    request_process->status = COMPLETED;
}


int compare_processes(const void* a, const void* b) {
    ServiceProcess *proc_a = (ServiceProcess*) a;
    ServiceProcess *proc_b = (ServiceProcess*) b;
    
    if (proc_a->request_id < proc_b->request_id) {
        return -1;
    } else if (proc_a->request_id == proc_b->request_id) {
        return 0;
    } else {
        return 1;
    }
}


unsigned int find_free_request_id(ServiceTopic *service) {
    unsigned int previous_occupied_id = 0;
    unsigned int max_occupied_id = 0;
    
    if (array_size(service->processes) > 0) {
        array_sort(service->processes, &compare_processes);
        
        ARRAY_FOREACH(curr, service->processes, {
            ServiceProcess *current = (ServiceProcess*) curr;
            if (current->request_id > previous_occupied_id+1) {
                return current->request_id - 1;
            }
            previous_occupied_id = current->request_id;
            if (current->request_id > max_occupied_id) max_occupied_id = current->request_id;
        });
    }
    
    return max_occupied_id+1;
}


bool require_service(ServiceTopic *service, void *request, unsigned int *request_id, unsigned int requestor_task_id) {
    if (array_size(service->processes) >= MAX_PROCESS_LIST_LENGTH) {
        //debug_printf("total %lu requests enqueued to service %s: FULL!!!!\r\n", array_size(service->processes), service->name);
        return false;  //server is full
    }
    
    // fetch a new request id (we know for sure that the server has free spots)
    unsigned int new_request_id = find_free_request_id(service);
    *request_id = new_request_id;
    
    // allocate the new process
    ServiceProcess *new_process = (ServiceProcess*) malloc(sizeof(ServiceProcess));
    new_process->request_id = new_request_id;
    new_process->requestor_task = requestor_task_id;
    new_process->acceptor_task = 0;
    new_process->status = OFFERED;
    new_process->request_ptr = request;
    new_process->response_ptr = NULL;
    
    int status = array_add(service->processes, new_process);
    if (status != CC_OK) {
        printf("Cannot add new process to table!\n");
        panic();
    }
    
    // quick kernel response
    // you may specify how many kernel acceptors as you want, but only one may complete the job
    if (array_size(service->kernel_acceptors) > 0) {
        ArrayIter iterator;
        kernel_serv_acceptor current;
        array_iter_init(&iterator, service->kernel_acceptors);
        for (; array_iter_next(&iterator, (void**)&current) == CC_OK ;) {
            bool kernel_accepted = current(request, 
                                           new_request_id, 
                                           requestor_task_id);
            if (kernel_accepted) {
                new_process->status = COMPLETED; // that was fast
                break;
            }
        }
    }
    
    //debug_printf("total %lu requests enqueued to service %s\r\n", array_size(service->processes), service->name);
    
    return true;
}

bool check_completed_and_close(ServiceTopic *service, unsigned int request_id) {
    ServiceProcess *target_process = process_by_request_id(service, request_id);
    if (target_process->status == COMPLETED) {
        free(target_process);
        array_remove(service->processes, target_process, NULL);
        return true;
    }
    return false;
}


static size_t serv_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    size_t written = 0;
    
    TableEntry *current_entry;
    ServiceTopic *current;
    HASHTABLE_FOREACH(current_entry, services_by_topic, {
        current = (ServiceTopic*) current_entry->value;
        written += snprintf(dest+written, maxsize-written, "%s\n", current->name);
    });
    
    
    return written;
}

void serv_make_system_dial_files() {
    FileDescriptor* dialfile_h;
    FileDescriptor* dest_folder;
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_INFODIAL_RACE,
        .class_attrib = DEFAULT_SEC_INFODIAL_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    dial_fs_primitives.create_file(&dialfile_h, "services", meta);
    dial_bind_callback(dialfile_h, &serv_dial_interface);
    resolve_path(OS_DIALS_H_PATH, &dest_folder);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_h);
}

void setup_serv_system() {
    hashtable_new(&services_by_topic);
} 
