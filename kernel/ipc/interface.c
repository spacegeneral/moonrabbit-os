#include <ipc/interface.h>
#include <kernel/log.h>
#include <kernel/core.h>
#include <ipc/mq.h>
#include <sys/mq.h>
#include <ipc/serv.h>
#include <sys/serv.h>
#include <security/authorize.h>
#include <security/identify.h>
#include <task/task.h>
#include <stdio.h>
#include <kernel/kidou.h>
#include <task/sched.h>

int ipc_readylevel_internal = 0;

static int ipc_ready_level() {
    return ipc_readylevel_internal;
}

static void ipc_setup() {
#ifdef VERBOSE_STARTUP
    printf("Initializing inter-process communication subsystem.\n    Setting up message queue system... ");
#endif
    setup_mq_system();
    klog("Message queue system enabled\n");
#ifdef VERBOSE_STARTUP
    printf(" done!\n    Setting up services system... ");
#endif
    setup_serv_system();
    klog("Service system enabled\n");
#ifdef VERBOSE_STARTUP
    printf("done!\nInter-process communication subsystem 50%% ready.\n");
#endif
    ipc_readylevel_internal = 50;
}

static void ipc_finalize() {
#ifdef VERBOSE_STARTUP
    printf("Finalizing inter-process communication subsystem.\n    Setting up special files... ");
#endif
    mq_make_system_dial_files();
    serv_make_system_dial_files();
#ifdef VERBOSE_STARTUP
    printf("done!\nInter-process communication 100%% ready.\n");
#endif
    ipc_readylevel_internal = 100;
}

static void ipc_teardown() {
    ipc_readylevel_internal = 0;
}

UNIQUE_IMPLEMENTATION_OF(ipc, data_subsystem, { \
    .ready_level = &ipc_ready_level, \
    .initialize = &ipc_setup, \
    .finalize = &ipc_finalize, \
    .teardown = &ipc_teardown \
})





static MQStatus ipc_advertise_channel(char *topic_name,
                                      unsigned int max_queue_length,
                                      unsigned int message_length,
                                      PhisicalDomainClass phys_race_attrib,
                                      DataDomainClass data_class_attrib, 
                                      SecurityPolicy policy) {
    MessageTopic *newchannel = register_new_channel(topic_name, max_queue_length, message_length, phys_race_attrib, data_class_attrib, policy);
    if (newchannel == NULL) return MQ_ALREADY_EXISTS;
    return MQ_SUCCESS;
}

static MQStatus ipc_retire_channel(char *topic_name, unsigned int agent_id) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    if (!authorize_agent_id(channel->phys_race_attrib, channel->data_class_attrib, agent_id)) return MQ_AUTH_PROBLEM;
    
    if (!mq_retire_channel(channel)) return MQ_DOES_NOT_EXIST;
    return MQ_SUCCESS;
}

static MQStatus ipc_subscribe(char *topic_name, unsigned int task_id) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    if (task_id != INVALID_TASK_ID) {
        if (!authorize_task_id_read_up(channel->phys_race_attrib, channel->data_class_attrib, channel->policy, task_id)) return MQ_AUTH_PROBLEM;
    }
    
    subscribe_to_channel(channel, task_id);
    return MQ_SUCCESS;
}

static MQStatus ipc_unsubscribe(char *topic_name, unsigned int task_id) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    if (task_id != INVALID_TASK_ID) {
        if (!authorize_task_id_read_up(channel->phys_race_attrib, channel->data_class_attrib, channel->policy, task_id)) return MQ_AUTH_PROBLEM;
    }
    
    if (!unsubscribe_from_channel(channel, task_id)) return MQ_NOT_SUBSCRIBED;
    return MQ_SUCCESS;
}

static MQStatus ipc_kernel_subscribe(char *topic_name, kernel_mq_subscriber callback) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    kernel_subscribe_to_channel(channel, callback);
    return MQ_SUCCESS;
}

static MQStatus ipc_kernel_unsubscribe(char *topic_name, kernel_mq_subscriber callback) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    if (!kernel_unsubscribe_from_channel(channel, callback)) return MQ_NOT_SUBSCRIBED;
    return MQ_SUCCESS;
}

static MQStatus ipc_publish(char *topic_name, const void *message, unsigned int agent_id) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    if (!authorize_agent_id_write_up(channel->phys_race_attrib, channel->data_class_attrib, channel->policy, agent_id)) return MQ_AUTH_PROBLEM;
    
    if (channel->policy & SecNoDataLoss) {
        if (next_write_causes_overrun(channel)) return MQ_QUEUE_FULL;
    }
    
    bool overrun_imminent = publish_message(channel, message);
    
    if (overrun_imminent) {
        if (channel->policy & SecNoDataLoss) {
            if (agent_id == 0) {
                klog("Ringbuffer overrun imminent on topic %s, cannot reschedule kernel! Data loss imminent!\n", topic_name);
            } else {
                // instead of ireturning to the current task, try to switch to the next one; this way, we try
                // to prevent a future MQ_QUEUE_FULL
                sched_ireturn_next_task();
                if (memcmp(topic_name, "/sys/", 5) == 0) {
                    klog("Ringbuffer overrun imminent on topic %s, rescheduling caller task.\n", topic_name);
                } else {
                    debug_printf("Ringbuffer overrun imminent on topic %s, rescheduling caller task.\r\n", topic_name);
                }
            }
        }
    }
    return MQ_SUCCESS;
}

static MQStatus ipc_publish_multi(char *topic_name, const void *message, unsigned int agent_id, size_t num_messages, size_t *num_written) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    (*num_written) = 0;
    
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    if (!authorize_agent_id_write_up(channel->phys_race_attrib, channel->data_class_attrib, channel->policy, agent_id)) return MQ_AUTH_PROBLEM;
    
    if (channel->policy & SecNoDataLoss) {
        if (next_write_causes_overrun(channel)) return MQ_QUEUE_FULL;
    }
    
    const uint8_t* curr_msg = (const uint8_t*) message;
    
    for (size_t m=0; m<num_messages; m++) {
        bool overrun_imminent = publish_message(channel, (const void*)curr_msg);
        (*num_written) += 1;
        
        if (overrun_imminent && ((*num_written) < num_messages)) {
            if (channel->policy & SecNoDataLoss) {
                if (agent_id == 0) {
                    klog("Ringbuffer overrun imminent on topic %s, cannot reschedule kernel! Data loss imminent!\n", topic_name);
                } else {
                    // instead of ireturning to the current task, try to switch to the next one; this way, we try
                    // to prevent a future MQ_QUEUE_FULL
                    sched_ireturn_next_task();
                    if (memcmp(topic_name, "/sys/", 5) == 0) {
                        klog("Ringbuffer overrun imminent on topic %s, rescheduling caller task.\n", topic_name);
                    } else {
                        debug_printf("Ringbuffer overrun imminent on topic %s, rescheduling caller task.\r\n", topic_name);
                    }
                    return MQ_CONTINUES;
                }
            }
        }
        curr_msg += channel->message_length;
    }
    return MQ_SUCCESS;
}

static MQStatus ipc_poll(char *topic_name, unsigned int task_id, void *message) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    if (task_id != INVALID_TASK_ID) {
        if (!authorize_task_id_read_up(channel->phys_race_attrib, channel->data_class_attrib, channel->policy, task_id)) return MQ_AUTH_PROBLEM;
    }
    
    if (!poll_channel(channel, task_id, message)) return MQ_NO_MESSAGES;
    return MQ_SUCCESS;
}

static MQStatus ipc_channel_stats(char *topic_name, ChannelStats *stats) {
    MessageTopic *channel = get_channel_by_name(topic_name);
    if (channel == NULL) return MQ_DOES_NOT_EXIST;
    
    if (!fill_in_channel_stats(channel, stats)) return MQ_DOES_NOT_EXIST;

    return MQ_SUCCESS;
}

UNIQUE_IMPLEMENTATION_OF(ipc, pubsub, { \
    .advertise_channel = &ipc_advertise_channel, \
    .retire_channel = &ipc_retire_channel, \
    .subscribe = &ipc_subscribe, \
    .unsubscribe = &ipc_unsubscribe, \
    .kernel_subscribe = &ipc_kernel_subscribe, \
    .kernel_unsubscribe = &ipc_kernel_unsubscribe, \
    .publish = &ipc_publish, \
    .publish_multi = &ipc_publish_multi, \
    .poll = &ipc_poll, \
    .stats = &ipc_channel_stats, \
})







static ServStatus ipc_advertise_service(char *topic_name, 
                                       unsigned int frame_length, 
                                       PhisicalDomainClass phys_race_attrib, 
                                       DataDomainClass data_class_attrib) {
    ServiceTopic *newservice = register_new_service(topic_name, frame_length, phys_race_attrib, data_class_attrib);
    if (newservice == NULL) return SERV_ALREADY_EXISTS;
    return SERV_SUCCESS;
}


static ServStatus ipc_retire_service(char *topic_name, unsigned int agent_id) {
    ServiceTopic *service = get_service_by_name(topic_name);
    if (service == NULL) return SERV_DOES_NOT_EXIST;
    
    if (!authorize_agent_id(service->phys_race_attrib, service->data_class_attrib, agent_id)) return SERV_AUTH_PROBLEM;
    
    if (!serv_retire_service(service)) return SERV_DOES_NOT_EXIST;
    return SERV_SUCCESS;
}

static ServStatus ipc_accept(char *topic_name, 
                             void *request, 
                             unsigned int *requestor_task_id, 
                             unsigned int *request_uid, 
                             unsigned int acceptor_task_id) {
    ServiceTopic *service = get_service_by_name(topic_name);
    if (service == NULL) return SERV_DOES_NOT_EXIST;
    
    if (!authorize_task_id(service->phys_race_attrib, service->data_class_attrib, acceptor_task_id)) return SERV_AUTH_PROBLEM;
    
    if (!accept_service_request(service, request, requestor_task_id, request_uid, acceptor_task_id)) return SERV_NO_REQUESTS;
    return SERV_SUCCESS;
}

static ServStatus ipc_kernel_accept(char *topic_name, kernel_serv_acceptor callback) {
    ServiceTopic *service = get_service_by_name(topic_name);
    if (service == NULL) return SERV_DOES_NOT_EXIST;
    
    kernel_accept(service, callback);
    return SERV_SUCCESS;
}

static ServStatus ipc_server_complete(char *topic_name, unsigned int request_uid, unsigned int agent_id) {
    ServiceTopic *service = get_service_by_name(topic_name);
    if (service == NULL) return SERV_DOES_NOT_EXIST;
    
    if (!authorize_agent_id(service->phys_race_attrib, service->data_class_attrib, agent_id)) return SERV_AUTH_PROBLEM;
    
    ServiceProcess *target_process = process_by_request_id(service, request_uid);
    if (target_process == NULL) return SERV_AUTH_PROBLEM;
    Task *target_task = get_task(target_process->acceptor_task);
    if (target_task == NULL) return SERV_AUTH_PROBLEM;
    if (target_task->owner_id != agent_id) return SERV_AUTH_PROBLEM;   // prevent rogue agents from completing the task of others
    
    complete_service_request(service, request_uid);
    return SERV_SUCCESS;
}

static ServStatus ipc_request(char *topic_name, void *request, unsigned int *request_id, unsigned int requestor_task_id) {
    ServiceTopic *service = get_service_by_name(topic_name);
    if (service == NULL) return SERV_DOES_NOT_EXIST;
    
    if (!authorize_task_id(service->phys_race_attrib, service->data_class_attrib, requestor_task_id)) return SERV_AUTH_PROBLEM;

    if (!require_service(service, request, request_id, requestor_task_id)) return SERV_FULL;
    return SERV_SUCCESS;
}

static ServStatus ipc_poll_response(char *topic_name, unsigned int request_id, unsigned int task_id) {
    ServiceTopic *service = get_service_by_name(topic_name);
    if (service == NULL) return SERV_DOES_NOT_EXIST;
    
    if (!authorize_task_id(service->phys_race_attrib, service->data_class_attrib, task_id)) return SERV_AUTH_PROBLEM;
    
    ServiceProcess *target_process = process_by_request_id(service, request_id);
    if (target_process == NULL) return SERV_AUTH_PROBLEM;
    if (target_process->requestor_task != task_id) return SERV_AUTH_PROBLEM;  // nice try
    
    if (!check_completed_and_close(service, request_id)) return SERV_PLEASE_WAIT;
    return SERV_SUCCESS;
}

UNIQUE_IMPLEMENTATION_OF(ipc, service, { \
    .advertise_service = &ipc_advertise_service, \
    .retire_service = &ipc_retire_service, \
    .accept = &ipc_accept, \
    .kernel_accept = &ipc_kernel_accept, \
    .server_complete = &ipc_server_complete, \
    .request = &ipc_request, \
    .poll_response = &ipc_poll_response, \
})
