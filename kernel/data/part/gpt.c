#include <stdio.h>
#include <stdlib.h>
#include <data/vfs.h>
#include <data/path.h>
#include <data/fsdiskio.h>
#include <data/gpt.h>
#include <security/defaults.h>
#include <kernel/log.h>


#define BLOCK_SIZE 512


void guid_to_str(uint8_t *uuid, char *str) {
    sprintf(str, 
        "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X", 
        uuid[3], uuid[2], uuid[1], uuid[0], 
        uuid[5], uuid[4], 
        uuid[7], uuid[6],
        uuid[8], uuid[9], uuid[10], uuid[11], uuid[12], uuid[13], uuid[14], uuid[15]
    );
}


bool search_older_part_file(unsigned int diskid, unsigned long partid, char *old_filename) {
    DirEnt *entries = NULL;
    size_t numentries;
    char candidate[MAX_FILENAME_LENGTH];
    snprintf(candidate, MAX_FILENAME_LENGTH, "%d.%d", diskid, partid);
    if (listdir(OS_PART_PATH, &entries, &numentries)) return false;
    for (size_t e=0; e<numentries; e++) {
        if (strncmp(entries[e].name, candidate, strlen(candidate)) == 0) {
            sprintf(old_filename, "%s", entries[e].name);
            return true;
        }
    }
    free(entries);
    return false;
}


void make_partition_diskio_file(unsigned int diskid, 
                                unsigned long partid, 
                                char *typename,
                                uint64_t offset,
                                uint64_t length) {
    FileDescriptor* diskfile;
    FileDescriptor* dest_folder;
    char name[MAX_FILENAME_LENGTH];
    char oldname[MAX_FILENAME_LENGTH];
    
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_PARTITION_RACE,
        .class_attrib = DEFAULT_SEC_PARTITION_CLASS,
        .executable = false,
        .writable = true,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    snprintf(name, MAX_FILENAME_LENGTH, "%d.%d.%s", diskid, partid, typename);
    
    bool replaceable_older_file = search_older_part_file(diskid, partid, oldname);
    
    if (replaceable_older_file) {
        // Don't check if the name/offset+length of the new and old entry match:
        // just delete the old entry, and re-create it from scratch
        if (remove(oldname) != 0) {
            klog("Failed to create %s: can't remove old entry %s\n", name, oldname);
            return;
        }
        klog("Partition file %s will replace older entry %s\n", name, oldname);
    }
    
    diskio_fs_primitives.create_file(&diskfile, name, meta);
    diskio_bind_file(diskfile, diskid, offset, length);
    if (!resolve_path(OS_PART_PATH, &dest_folder)) {
        printf("Error: cannot create %s,%s\n", OS_PART_PATH, diskfile->name);
        return;
    }
    file_method(dest_folder)->insert_into_directory(dest_folder, diskfile);
    if (!replaceable_older_file) klog("Found new partition %s\n", name);
}


bool process_entry(unsigned int diskid, unsigned long partid, GPTPartEntry *entry, bool auto_add) {
    char guid[37];
    char human_name[30] = "unknown-partition";
    guid_to_str(entry->type_guid, guid);
    
    if (strcmp(guid, NULL_UUID) == 0) return true;  // empty entry
    
    for (size_t test_type=0; test_type < (sizeof(gpt_parttypes) / sizeof(fdisk_parttype)); test_type++) {
        fdisk_parttype *current = gpt_parttypes + test_type;
        if (strcmp(guid, current->typestr) == 0) {
            strncpy(human_name, current->name, 30);
            break;
        }
    }
    if (auto_add) {
        uint64_t size = (entry->last_lba - entry->first_lba + 1) * BLOCK_SIZE;
        make_partition_diskio_file(diskid, 
                                   partid, 
                                   human_name, 
                                   entry->first_lba * BLOCK_SIZE, 
                                   size);
    }
    
    return true;
}


bool scan_disk_for_gpt(char *diskname, long *num_partitions, bool auto_add) {
    FileDescriptor *diskfile;
    if (!resolve_path(diskname, &diskfile)) return false;
    
    if (diskfile->access_strategy != RAW_DISKIO) return false;
    
    FSDiskIOFileAccessData *diskaccess = (FSDiskIOFileAccessData*)diskfile->access_data;
    unsigned int diskid = diskaccess->diskid;
    
    FILE *fp = fopen(diskname, "rb");
    if (fp == NULL) {
        return false;
    }
    
    fseek(fp, BLOCK_SIZE, SEEK_SET);
    
    GPTHeader header;
    size_t readpayload = fread(&header, 1, sizeof(GPTHeader), fp);
    if (readpayload != sizeof(GPTHeader)) {
        fclose(fp);
        return false;
    }
    
    if (header.signature != GPT_MAGIC) {
        fclose(fp);
        return false;
    }
    
    klog("Found gpt signature on disk %s\n", diskname);
    
    (*num_partitions) = header.num_partitions;
    
    GPTPartEntry entry;
    fseek(fp, BLOCK_SIZE * header.starting_partition_entries_lba, SEEK_SET);
    for (size_t p=0; p<header.num_partitions; p++) {
        if (fread(&entry, sizeof(GPTPartEntry), 1, fp) != 1) {
            fclose(fp);
            return false;
        }
        if (!process_entry(diskid, p, &entry, auto_add)) {
            fclose(fp);
            return false;
        }
        if (entry_is_empty(&entry)) break; //Lazy mode
    }
    fclose(fp);
    return true;
}
