#include <stdlib.h>
#include <data/vfs.h>
#include <stdio.h>
#include <collections/array.h>


Array *open_files_table;
static unsigned int fopen_ident_gen;


/*
 * the open file system is handled using a pair of OpenFile descriptors:
 * - the ledger descriptor, used in the open file table, contains a pointer to
 *   the internal file object
 * - the user descriptor, which lacks the internal file pointer
 * 
 * the 2 descriptors are matched using a uid and a random token
 * 
 */



bool generic_open_file(unsigned int agent_id, FileDescriptor *src_file, OpenFile *user_descriptor) {
    LOCK(open_file_table_lock);
    
    OpenFile *ledger_copy = (OpenFile*) malloc(sizeof(OpenFile));
    OpenFile *user_copy = user_descriptor;
    
    ledger_copy->file = src_file;
    user_copy->file = NULL;
    
    ledger_copy->seek_pos = 0;
    user_copy->seek_pos = 0;
    
    for (size_t t=0; t<OPENFILE_TOKEN_LEN; t++) {
        ledger_copy->unique_token[t] = (unsigned char) rand();
        user_copy->unique_token[t] = ledger_copy->unique_token[t];
    }
    
    ledger_copy->id = fopen_ident_gen++;
    user_copy->id = ledger_copy->id;
    
    ledger_copy->owner_agent_id = agent_id;
    user_copy->owner_agent_id = ledger_copy->owner_agent_id;
    
    array_add(open_files_table, ledger_copy);
    
    UNLOCK(open_file_table_lock);
    return true;
}

bool generic_close_file(OpenFile *user_descriptor) {
    LOCK(open_file_table_lock);
    
    OpenFile *user_target = user_descriptor;
    
    ARRAY_FOREACH(curr, open_files_table, {
        OpenFile *ledger_descriptor = (OpenFile*) curr;
        if (user_target->id == ledger_descriptor->id) {
            if (memcmp(ledger_descriptor->unique_token, user_target->unique_token, OPENFILE_TOKEN_LEN) == 0) {
                array_remove(open_files_table, ledger_descriptor, NULL);
                free(ledger_descriptor);
                UNLOCK(open_file_table_lock);
                return true;
            }
        }
    });
    
    UNLOCK(open_file_table_lock);
    return false;
}

static bool close_single_file_by_agent(unsigned int agent_id) {
    ARRAY_FOREACH(curr, open_files_table, {
        OpenFile *ledger_descriptor = (OpenFile*) curr;
        if (ledger_descriptor->owner_agent_id == agent_id) {
            array_remove(open_files_table, ledger_descriptor, NULL);
            free(ledger_descriptor);
            return true;
        }
    });
    return false;
}

bool close_all_files_by_agent(unsigned int agent_id) {
    LOCK(open_file_table_lock);
    
    bool open_file_found;
    do {
        open_file_found = close_single_file_by_agent(agent_id);
    } while(open_file_found);
    
    UNLOCK(open_file_table_lock);
    return true;
}

bool resolve_open_file(OpenFile *user_descriptor, FileDescriptor **file_descriptor) {
    LOCK(open_file_table_lock);
    
    
    ARRAY_FOREACH(curr, open_files_table, {
        OpenFile *ledger_descriptor = (OpenFile*) curr;
        if (user_descriptor->id == ledger_descriptor->id) {
            if (memcmp(ledger_descriptor->unique_token, user_descriptor->unique_token, OPENFILE_TOKEN_LEN) == 0) {
                (*file_descriptor) = ledger_descriptor->file;
                UNLOCK(open_file_table_lock);
                return true;
            }
        }
    });
    
    UNLOCK(open_file_table_lock);
    return false;
}

size_t generic_read_file(void* dest, size_t num_bytes, OpenFile* open_descriptor) {
    FileDescriptor *filedesc;
    size_t seek = open_descriptor->seek_pos;
    bool success = resolve_open_file(open_descriptor, &filedesc);
    if (!success) {
        debug_printf("Could not resolve file 0x%x\r\n", open_descriptor);
        return 0;
    }
    size_t read = file_method(filedesc)->impl_read_file(dest, num_bytes, seek, filedesc);
    open_descriptor->seek_pos += read;
    return read;
}

size_t generic_write_file(const void* src, size_t num_bytes, OpenFile* open_descriptor) {
    FileDescriptor *filedesc;
    size_t seek = open_descriptor->seek_pos;
    bool success = resolve_open_file(open_descriptor, &filedesc);
    if (!success) {
        debug_printf("Could not resolve file 0x%x\r\n", open_descriptor);
        return 0;
    }
    size_t write = file_method(filedesc)->impl_write_file(src, num_bytes, seek, filedesc);
    open_descriptor->seek_pos += write;
    return write;
}

size_t generic_get_filesize(OpenFile* open_descriptor) {
    FileDescriptor *filedesc;
    bool success = resolve_open_file(open_descriptor, &filedesc);
    if (!success) {
        debug_printf("Could not resolve file 0x%x\r\n", open_descriptor);
        return 0;
    }
    return file_method(filedesc)->impl_get_filesize(filedesc);
}

OpenFile* generic_convert_FILE_to_OpenFile(FILE* fp) {
    return (OpenFile*) fp;
}

FILE* generic_convert_OpenFile_to_FILE(OpenFile* fp) {
    return (FILE*) fp;
}



void setup_open_files_table() {
    fopen_ident_gen = 0;
    array_new(&open_files_table);
}
