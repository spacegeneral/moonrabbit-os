#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/errno.h>
#include <time.h>
#include <data/vfs.h>
#include <data/virtual_file.h>
#include <data/path.h>
#include <data/nopfs.h>
#include <data/fsdiskio.h>
#include <data/fsdial.h>
#include <data/fs/internalfs.h>
#include <security/defaults.h>
#include <security/identify.h>
#include <task/task.h>
#include <sys/default_topics.h>
#include <ipc/interface.h>


FileDescriptor* root;


const FSprimitives* file_method(FileDescriptor *fd) {
    switch (fd->access_strategy) {
        case MOUNT_ALIENFS: /* fallthrough */
        case PURE_VIRTUAL: return &virtual_fs_primitives;
        case RAW_DISKIO: return &diskio_fs_primitives;
        case DIAL_READ: return &dial_fs_primitives;
        
        default: return &nop_fs_primitives;
    }
    return NULL;
}

#define CHECK_ALIEN(pathstring) \
{ \
    resolve_mount(pathstring, &mount); \
    if (mount != NULL) { \
        printf("operation %d encountered an alien\n", request->service_name); \
        request->status = ALIEN_DRIVER; \
        break; \
    } \
}

#define CHECK_ALIEN_OPENFILE(streamstring) \
{ \
    if (strcmp(streamstring, ROOTFS_DRIVER_NAME) != 0) { \
        printf("operation %d encountered an alien\n", request->service_name); \
        request->status = ALIEN_DRIVER; \
        break; \
    } \
}


bool internalfs_service_handler(void *req, unsigned int request_uid, unsigned int agent_id) {
    (void)request_uid; // unused
    FSServiceMessage *request = (FSServiceMessage*) req;
    
    IdentifiedAgent *requester = get_agent(agent_id);
    FileDescriptor *mount = NULL;
    
    switch (request->service_name) {
        case fsservice_create_file:
            CHECK_ALIEN(request->msg.create_file.pathname);
            debug_printf("touch %s\r\n", request->msg.remove.pathname);
            internalfs_composites.create_file(requester, request->msg.create_file.pathname, &(request->status));
            break;
        case fsservice_create_directory:
            CHECK_ALIEN(request->msg.create_directory.pathname);
            internalfs_composites.create_directory(requester, request->msg.create_directory.pathname, &(request->status));
            break;
        case fsservice_create_link:
            CHECK_ALIEN(request->msg.create_link.path_from);
            internalfs_composites.create_link(requester, request->msg.create_link.path_from, request->msg.create_link.path_to, &(request->status));
            break;
        case fsservice_remove:
            CHECK_ALIEN(request->msg.remove.pathname);
            debug_printf("remove %s\r\n", request->msg.remove.pathname);
            internalfs_composites.remove(requester, request->msg.remove.pathname, &(request->status));
            break;
        case fsservice_count_directory_entries:
            CHECK_ALIEN(request->msg.count_directory_entries.pathname);
            internalfs_composites.count_directory_entries(requester,
                                                          request->msg.count_directory_entries.pathname, 
                                                          &(request->msg.count_directory_entries.numentries), 
                                                          &(request->status));
            break;
        case fsservice_get_directory_entry_at:
            CHECK_ALIEN(request->msg.get_directory_entry_at.pathname);
            internalfs_composites.get_directory_entry_at(requester,
                                                         request->msg.get_directory_entry_at.pathname, 
                                                         request->msg.get_directory_entry_at.position, 
                                                         &(request->msg.get_directory_entry_at.entry), 
                                                         &(request->status));
            break;
        case fsservice_file_exists:
            CHECK_ALIEN(request->msg.file_exists.pathname);
            internalfs_composites.file_exists(requester, request->msg.file_exists.pathname, (int*)&(request->msg.file_exists.result));
            request->status = 0;
            break;
        case fsservice_move:
            CHECK_ALIEN(request->msg.move.path_from);
            internalfs_composites.move(requester, request->msg.move.path_from, request->msg.move.path_to, &(request->status));
            break;
        case fsservice_fopen:
            CHECK_ALIEN(request->msg.fopen.pathname);
            internalfs_composites.fopen(requester, 
                                        request->msg.fopen.pathname, 
                                        request->msg.fopen.mode, 
                                        &(request->msg.fopen.result), 
                                        &(request->status));
            //debug_printf("fopen('%s', '%s') = %p (%d)\r\n", request->msg.fopen.pathname, request->msg.fopen.mode, request->msg.fopen.result, request->status);
            break;
        case fsservice_fclose:
            CHECK_ALIEN_OPENFILE(request->msg.fclose.stream.driver);
            internalfs_composites.fclose(requester, &(request->msg.fclose.stream), &(request->status));
            break;
        case fsservice_fflush:
            CHECK_ALIEN_OPENFILE(request->msg.fflush.stream.driver);
            internalfs_composites.fflush(requester, &(request->msg.fflush.stream), &(request->status));
            break;
        case fsservice_fread:
            CHECK_ALIEN_OPENFILE(request->msg.fread.stream.driver);
            internalfs_composites.fread(requester, 
                                        (void*)request->msg.fread.payload, 
                                        request->msg.fread.size, 
                                        request->msg.fread.nmemb, 
                                        &(request->msg.fread.stream), 
                                        &(request->msg.fread.result));
            request->status = 0;
            break;
        case fsservice_fwrite:
            CHECK_ALIEN_OPENFILE(request->msg.fwrite.stream.driver);
            internalfs_composites.fwrite(requester, 
                                         (const void*)request->msg.fwrite.payload, 
                                         request->msg.fwrite.size, 
                                         request->msg.fwrite.nmemb, 
                                         &(request->msg.fwrite.stream), 
                                         &(request->msg.fwrite.result));
            request->status = 0;
            break;
        case fsservice_fseek:
            CHECK_ALIEN_OPENFILE(request->msg.fseek.stream.driver);
            internalfs_composites.fseek(requester, 
                                         &(request->msg.fseek.stream), 
                                         request->msg.fseek.offset, 
                                         request->msg.fseek.whence, 
                                         &(request->status));
            break;
        case fsservice_setmount:
            request->status = NOT_IMPLEMENTED;
            break;
        case fsservice_unsetmount:
            request->status = NOT_IMPLEMENTED;
            break;
        case fsservice_chsec:
            CHECK_ALIEN(request->msg.fopen.pathname);
            internalfs_composites.chsec(requester, 
                                        request->msg.chsec.pathname, 
                                        request->msg.chsec.newrace, 
                                        request->msg.chsec.newclass, 
                                        &(request->status));
            break;
    }
    
    
    return true;
}


static bool internalfs_service_adapter(void *req, unsigned int request_uid, unsigned int requestor_task_id) {
    return internalfs_service_handler(req, request_uid, get_agent(get_task(requestor_task_id)->owner_id)->uid);
}


void setup_root_fs() {
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_ROOTFS_RACE,
        .class_attrib = DEFAULT_SEC_ROOTFS_CLASS,
        .executable = false,
        .writable = true,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    
    setup_open_files_table();
    
    internalfs_composites.mount(&root, "f", meta);
    
    CALL_IMPL(ipc, service, advertise_service, DEFAULT_ROOTFS_SERVICE, sizeof(FSServiceMessage), DEFAULT_ROOTFS_TOPICS_RACE, DEFAULT_ROOTFS_TOPICS_CLASS);
    CALL_IMPL(ipc, service, kernel_accept, DEFAULT_ROOTFS_SERVICE, &internalfs_service_adapter);
}
    



