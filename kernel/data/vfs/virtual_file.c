#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <yakumo.h>
#include <data/vfs.h>
#include <data/virtual_file.h>
#include <collections/array.h>


bool virtualfs_create_directory(FileDescriptor **fd, char *name, FileMetaData meta) {
    FileDescriptor *newfile = (FileDescriptor*) malloc(sizeof(FileDescriptor));
    strncpy(newfile->name, name, MAX_FILENAME_LENGTH);
    strncpy(newfile->driver, ROOTFS_DRIVER_NAME, MAX_FS_DRIVER_NAME_LENGTH);
    newfile->parent = NULL;
    newfile->metadata = meta;
    newfile->filetype = Directory;
    newfile->access_strategy = PURE_VIRTUAL;
    
    PureVirtualDirectoryAccessData *diraccess = (PureVirtualDirectoryAccessData*) malloc(sizeof(PureVirtualDirectoryAccessData));
    array_new(&(diraccess->children));
    newfile->access_data = diraccess;
    
    (*fd) = newfile;
    return true;
}
bool virtualfs_create_file(FileDescriptor **fd, char *name, FileMetaData meta) {
    FileDescriptor *newfile = (FileDescriptor*) malloc(sizeof(FileDescriptor));
    strncpy(newfile->name, name, MAX_FILENAME_LENGTH);
    strncpy(newfile->driver, ROOTFS_DRIVER_NAME, MAX_FS_DRIVER_NAME_LENGTH);
    newfile->parent = NULL;
    newfile->metadata = meta;
    newfile->filetype = File;
    newfile->access_strategy = PURE_VIRTUAL;
    
    PureVirtualFileAccessData *file = (PureVirtualFileAccessData*) malloc(sizeof(PureVirtualFileAccessData));
    file->content = NULL;
    file->size = 0;
    newfile->access_data = file;
    
    (*fd) = newfile;
    return true;
}
bool virtualfs_create_link(FileDescriptor **fd, char *name, FileMetaData meta) {
    FileDescriptor *newfile = (FileDescriptor*) malloc(sizeof(FileDescriptor));
    strncpy(newfile->name, name, MAX_FILENAME_LENGTH);
    strncpy(newfile->driver, ROOTFS_DRIVER_NAME, MAX_FS_DRIVER_NAME_LENGTH);
    newfile->parent = NULL;
    newfile->metadata = meta;
    newfile->filetype = Link;
    newfile->access_strategy = PURE_VIRTUAL;
    
    PureVirtualLinkAccessData *link = (PureVirtualLinkAccessData*) malloc(sizeof(PureVirtualLinkAccessData));
    link->dest_name = NULL;
    newfile->access_data = link;
    
    (*fd) = newfile;
    return true;
}


bool virtualfs_insert_into_directory(FileDescriptor *dest_dir, FileDescriptor *src_file) {
    PureVirtualDirectoryAccessData *diraccess = (PureVirtualDirectoryAccessData*) dest_dir->access_data;
    size_t prev_occurrences = array_contains(diraccess->children, src_file);
    if (prev_occurrences == 0) {
        array_add(diraccess->children, src_file);
        src_file->parent = dest_dir;
        return true;
    }
    return false;
}
bool virtualfs_remove_file(FileDescriptor **dead_file) {
    FileDescriptor *target = *dead_file;
    FileDescriptor *parent = target->parent;
    if (target->filetype == File) {
        PureVirtualFileAccessData *access = target->access_data;
        if (access->content != NULL) free(access->content);
        free(access);
    } else if (target->filetype == Link) {
        PureVirtualLinkAccessData *access = target->access_data;
        if (access->dest_name != NULL) free(access->dest_name);
        free(access);
    } else {
        return false;  // not a file or link
    }
    if (parent != NULL) {
        file_method(parent)->unlist_file(parent, target);
    }
    free(target);
    return true;
}
bool virtualfs_unlist_file(FileDescriptor *dest_dir, FileDescriptor *dead_file) {
    PureVirtualDirectoryAccessData *diraccess = (PureVirtualDirectoryAccessData*) dest_dir->access_data;
    ARRAY_FOREACH(curr, diraccess->children, {
        FileDescriptor *current = (FileDescriptor*) curr;
        if (current == dead_file) {
            array_remove(diraccess->children, current, NULL);
            return true;
        }
    });
    return false;
}
bool virtualfs_remove_empty_directory(FileDescriptor **dead_file) {
    FileDescriptor *target = *dead_file;
    FileDescriptor *parent = target->parent;
    if (target->filetype == Directory) {
        PureVirtualDirectoryAccessData *access = target->access_data;
        
        size_t numchildren = array_size(access->children);
        if (numchildren > 0) return false;
        
        array_destroy(access->children);
        free(access);
    } else {
        return false;  // not a directory
    }
    if (parent != NULL) {
        file_method(parent)->unlist_file(parent, target);
    }
    free(target);
    return true;
}
bool virtualfs_retarget_link(FileDescriptor *link, const char *destination_path) {
    PureVirtualLinkAccessData *linkccess = (PureVirtualLinkAccessData*) link->access_data;
    if (linkccess->dest_name != NULL) {
        free(linkccess->dest_name);
    }
    size_t namelen = strlen(destination_path);
    linkccess->dest_name = (char*) malloc(sizeof(char) * (namelen + 1));
    strncpy(linkccess->dest_name, destination_path, namelen);
    linkccess->dest_name[namelen] = '\0';
    return true;
}


size_t virtualfs_impl_read_file(void *dest, size_t num_bytes, size_t seek, FileDescriptor *file) {
    PureVirtualFileAccessData *access = file->access_data;
    uint64_t filesize = access->size;
    uint8_t *source = access->content;
    if (source == NULL) return 0;
    uint8_t *destination = (uint8_t*) dest;
    if (seek >= filesize) return 0;
    size_t amount = min(filesize, seek + num_bytes) - seek;
    memcpy(destination, source + seek, amount);
    return amount;
}

size_t virtualfs_impl_write_file(const void *src, size_t num_bytes, size_t seek, FileDescriptor *file) {
    PureVirtualFileAccessData *access = file->access_data;
    uint64_t datasize = seek + num_bytes;
    if (access->content == NULL) {
        access->content = (uint8_t*) malloc(sizeof(uint8_t) * datasize);
        access->size = datasize;
    } else if (access->size < datasize) {
        access->content = (uint8_t*) realloc(access->content, sizeof(uint8_t) * datasize);
        access->size = datasize;
    }
    uint64_t filesize = access->size;
    uint8_t *destination = access->content;
    uint8_t *source = (uint8_t*) src;
    size_t amount = min(filesize, seek + num_bytes) - seek;
    memcpy(destination + seek, source, amount);
    return amount;
}

size_t virtualfs_impl_get_filesize(FileDescriptor *file) {
    if (file->filetype == File) {
        PureVirtualFileAccessData *access = file->access_data;
        return access->size;
    } else if (file->filetype == Link) {
        PureVirtualLinkAccessData *access = file->access_data;
        return strlen(access->dest_name);
    } else if (file->filetype == Directory) {
        PureVirtualDirectoryAccessData *access = file->access_data;
        return array_size(access->children) * sizeof(FileDescriptor*);
    }
    
    return 0;
}


bool virtualfs_alist_directory(FileDescriptor *dir, FileDescriptor ***content, size_t *num_files) {
    PureVirtualDirectoryAccessData *diraccess = (PureVirtualDirectoryAccessData*) dir->access_data;
    size_t numchildren = array_size(diraccess->children);
    if (numchildren > 0) {
        FileDescriptor **files = (FileDescriptor**) malloc(sizeof(FileDescriptor*) * numchildren);
        ArrayIter iterator;
        FileDescriptor *current;
        array_iter_init(&iterator, diraccess->children);
        for (size_t curr_id = 0; array_iter_next(&iterator, (void**)&current) == CC_OK ;curr_id++) {
            files[curr_id] = current;
        }
        (*num_files) = numchildren;
        (*content) = files;
        return true;
    }
    return false;
}
bool virtualfs_get_child(FileDescriptor *dir, char *name, FileDescriptor **child) {
    PureVirtualDirectoryAccessData *diraccess = (PureVirtualDirectoryAccessData*) dir->access_data;

    ARRAY_FOREACH(curr, diraccess->children, {
        FileDescriptor *current = (FileDescriptor*) curr;
        if (strcmp(current->name, name) == 0) {
            (*child) = current;
            return true;
        }
    });
    
    return false;
}
bool virtualfs_aget_link_dest(FileDescriptor *link, char **dest_str) {
    PureVirtualLinkAccessData *linkaccess = (PureVirtualLinkAccessData*) link->access_data;
    if (linkaccess->dest_name == NULL) return false;
    char *namecopy = (char*) malloc(sizeof(char) * (strlen(linkaccess->dest_name)+1));
    memcpy(namecopy, linkaccess->dest_name, strlen(linkaccess->dest_name)+1);
    
    (*dest_str) = namecopy;
    
    return true;
}

bool virtualfs_change_metadata(FileDescriptor* fd, FileMetaData newmeta) {
    fd->metadata = newmeta;
    return true;
}

