#include <data/nopfs.h>


bool nopfs_create_directory(FileDescriptor** fd, char* name, FileMetaData meta) {
    (void)fd; // unused
    (void)name; // unused
    (void)meta; // unused
    return false;
}
bool nopfs_create_file(FileDescriptor** fd, char* name, FileMetaData meta) {
    (void)fd; // unused
    (void)name; // unused
    (void)meta; // unused
    return false;
}
bool nopfs_create_link(FileDescriptor** fd, char* name, FileMetaData meta) {
    (void)fd; // unused
    (void)name; // unused
    (void)meta; // unused
    return false;
}

bool nopfs_insert_into_directory(FileDescriptor* dest_dir, FileDescriptor* src_file) {
    (void)dest_dir; // unused
    (void)src_file; // unused
    return false;
}
bool nopfs_remove_file(FileDescriptor** dead_file) {
    (void)dead_file; // unused
    return false;
}
bool nopfs_unlist_file(FileDescriptor* dest_dir, FileDescriptor* dead_file) {
    (void)dest_dir; // unused
    (void)dead_file; // unused
    return false;
}
bool nopfs_remove_empty_directory(FileDescriptor** dead_file) {
    (void)dead_file; // unused
    return false;
}
bool nopfs_retarget_link(FileDescriptor* link, const char* destination_path) {
    (void)link; // unused
    (void)destination_path; // unused
    return false;
}

size_t nopfs_impl_read_file(void* dest, size_t num_bytes, size_t seek, FileDescriptor* file) {
    (void)dest; // unused
    (void)num_bytes; // unused
    (void)seek; // unused
    (void)file; // unused
    return 0;
}
size_t nopfs_impl_write_file(const void* src, size_t num_bytes, size_t seek, FileDescriptor* file) {
    (void)src; // unused
    (void)num_bytes; // unused
    (void)seek; // unused
    (void)file; // unused
    return 0;
}
size_t nopfs_impl_get_filesize(FileDescriptor* file) {
    (void)file; // unused
    return 0;
}

bool nopfs_alist_directory(FileDescriptor* dir, FileDescriptor*** content, size_t* num_files) {
    (void)dir; // unused
    (void)content; // unused
    (void)num_files; // unused
    return false;
}
bool nopfs_get_child(FileDescriptor* dir, char* name, FileDescriptor** child) {
    (void)dir; // unused
    (void)name; // unused
    (void)child; // unused
    return false;
}
bool nopfs_aget_link_dest(FileDescriptor* link, char** dest_str) {
    (void)link; // unused
    (void)dest_str; // unused
    return false;
}

bool nopfs_change_metadata(FileDescriptor* fd, FileMetaData newmeta) {
    (void)fd; // unused
    (void)newmeta; // unused
    return false;
}
