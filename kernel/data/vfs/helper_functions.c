#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <data/vfs.h>
#include <data/path.h>


bool afull_file_path(FileDescriptor *file, char **dest_str) {
    size_t partial_path_len = 0;
    int full_path_depth = -1;
    char *full_path;
    
    // a first reverse-genealogy traversal to get the exact length of the full path string
    FileDescriptor *current = file;
    while (current != NULL) {
        partial_path_len += strlen(current->name);
        full_path_depth++;
        current = current->parent;
    }
    
    //               filenames contribution   separators contribution
    //                           VVV                VVV
    size_t full_path_len = partial_path_len + full_path_depth;
    full_path = (char*) malloc(sizeof(char) * (full_path_len + 1));
    
    // a second reverse-genealogy traversal to copy the individual filenames in their exact place
    current = file;
    size_t wrote_so_far = 0;
    while (current != NULL) {
        size_t strpos = full_path_len - strlen(current->name) - wrote_so_far;
        memcpy(
            full_path + strpos,
            current->name,
            strlen(current->name)
        );
        
        if (current->parent != NULL) {
            full_path[strpos-1] = DIR_SEPARATOR;
        }
        
        wrote_so_far += strlen(current->name) + 1;
        current = current->parent;
    }
    full_path[full_path_len] = '\0';
    (*dest_str) = full_path;
    return true;
}

#define RESOLVE_PATH_HANDLE_LINK() \
{ \
    char *linkdest;   \
    \
    if (!file_method(current_file)->aget_link_dest(current_file, &linkdest)) return ((*to_file) != NULL); \
    \
    if (!resolve_path_any(linkdest, &current_file, false /* TODO: max 1 level of link traversal allowed */)) { \
        free(linkdest); \
        free(path); \
        return ((*to_file) != NULL); \
    } \
    free(linkdest); \
}

bool resolve_path_any(const char *orig_path, FileDescriptor **to_file, bool go_deep) {
    //debug_printf("Resolving path \"%s\"\r\n", orig_path);
    size_t path_strlen = strlen(orig_path);
    char *path = (char*) malloc(sizeof(char) * (strlen(orig_path) + 1));
    memcpy(path, orig_path, strlen(orig_path) + 1);
    // split the path name in tokens
    size_t num_tokens = strsplit(path, DIR_SEPARATOR, path_strlen);
    if (num_tokens == 0) {
        free(path);
        return false;
    }
    
    size_t current_token = 0;
    FileDescriptor *current_file = root;
    
    bool skip_namecmp_because_link = false;
    // walk the "token list"
    while (current_token < num_tokens) {
        char *current_filename = strsplit_chunk_at(path, path_strlen, current_token);
        
        if ((strcmp(current_filename, current_file->name) == 0) || \
                    skip_namecmp_because_link || \
                    (strcmp(current_filename, PARENT_DIR_NAME) == 0)) { // match token <-> current file name
                        
            //debug_printf("%s <-> %s\r\n", current_filename, current_file->name);
            skip_namecmp_because_link = false;
            
            if (current_token == num_tokens-1) {  // last token: return the file
                if (current_file->filetype == Link && go_deep) {
                    RESOLVE_PATH_HANDLE_LINK();
                }
                
                (*to_file) = current_file;
                free(path);
                return true;
            }
            
            // not last token
            if (current_file->filetype == Directory) {  // extract the next file using the next token
                char *next_filename = strsplit_chunk_at(path, path_strlen, current_token+1);
                if (!file_method(current_file)->get_child(current_file, next_filename, &current_file)) {
                    if (strcmp(next_filename, PARENT_DIR_NAME) == 0) {
                        if (current_file->parent == NULL) {
                            free(path);
                            return false;
                        }
                        current_file = current_file->parent;
                    } else {
                        //debug_printf("%s has no child %s\r\n", current_file, next_filename);
                        free(path);
                        return false;
                    }
                }
            } else if (current_file->filetype == Link) {
                char *linkdest;  // this one is allocated; remember to free it
                // get the path of the destination
                if (!file_method(current_file)->aget_link_dest(current_file, &linkdest)) {
                    free(path);
                    return false;
                }
                // get the destination file
                if (!resolve_path(linkdest, &current_file)) {
                    //debug_printf("broken link (%s = %s)\r\n", current_filename, linkdest);
                    free(linkdest);
                    free(path);
                    return false;
                }
                
                free(linkdest);
                // current file is now the link destination file. Parse it again
                skip_namecmp_because_link = true;
                current_token--;
            }
            
            current_token++;
            
        } else {  // mismatched token <-> current file name
            //debug_printf("mismatched (%s)\r\n", current_filename);
            free(path);
            return false;
        }
    }
    
    free(path);
    return false;
}

bool resolve_path(const char *orig_path, FileDescriptor **to_file) {
    return resolve_path_any(orig_path, to_file, false);
}

bool resolve_path_deep(const char *orig_path, FileDescriptor **to_file) {
    return resolve_path_any(orig_path, to_file, true);
}

#define RESOLVE_MOUNT_HANDLE_LINK() \
{ \
    char *linkdest;   \
    \
    if (!file_method(current_file)->aget_link_dest(current_file, &linkdest)) return ((*to_mount) != NULL); \
    \
    if (!resolve_path(linkdest, &current_file)) { \
        free(linkdest); \
        free(path); \
        return ((*to_mount) != NULL); \
    } \
    free(linkdest); \
}

#define RESOLVE_MOUNT_HANDLE_LINK_EXPLICIT() \
{ \
    char *linkdest;   \
    \
    if (!file_method(current_file)->aget_link_dest(current_file, &linkdest)) { \
        explicit_path[explicit_path_pointer-1] = '\0';  /* remove trailing separator */ \
        free(path); \
        return ((*to_mount) != NULL); \
    } \
    /*explicit_path_pointer = sprintf(explicit_path, "%s,", linkdest);*/ \
    \
    if (!resolve_path(linkdest, &current_file)) { \
        free(linkdest); \
        free(path); \
        explicit_path[explicit_path_pointer-1] = '\0';  /* remove trailing separator */ \
        return ((*to_mount) != NULL); \
    } \
    free(linkdest); \
}

#define RESOLVE_MOUNT_DETECT_MOUNT() \
{ \
    if (current_file->access_strategy == MOUNT_ALIENFS) { \
        (*to_mount) = current_file; \
    } \
}

bool resolve_mount(const char *orig_path, FileDescriptor **to_mount) {
    size_t path_strlen = strlen(orig_path);
    char *path = (char*) malloc(sizeof(char) * (strlen(orig_path) + 1));
    memcpy(path, orig_path, strlen(orig_path) + 1);
    // split the path name in tokens
    size_t num_tokens = strsplit(path, DIR_SEPARATOR, path_strlen);
    if (num_tokens == 0) {
        free(path);
        return false;
    }
    
    size_t current_token = 0;
    FileDescriptor *current_file = root;
    
    bool skip_namecmp_because_link = false;
    
    (*to_mount) = NULL;
    
    // walk the "token list"
    while (current_token < num_tokens) {
        char *current_filename = strsplit_chunk_at(path, path_strlen, current_token);
        //printf("%s <-> %s (%p)\n", current_filename, current_file->name, current_file);
        if ((strcmp(current_filename, current_file->name) == 0) || skip_namecmp_because_link) { // match token <-> current file name
            skip_namecmp_because_link = false;
            
            RESOLVE_MOUNT_DETECT_MOUNT();
            
            if (current_token == num_tokens-1) {  // last file
                if (current_file->filetype == Link) {
                    RESOLVE_MOUNT_HANDLE_LINK();
                    RESOLVE_MOUNT_DETECT_MOUNT();
                }
                
                free(path);
                return ((*to_mount) != NULL);
            }
            
            // not last token
            if (current_file->filetype == Directory) {  // extract the next file using the next token
                char *next_filename = strsplit_chunk_at(path, path_strlen, current_token+1);
                if (!file_method(current_file)->get_child(current_file, next_filename, &current_file)) {
                    free(path);
                    return ((*to_mount) != NULL);
                }
            } else if (current_file->filetype == Link) {
                RESOLVE_MOUNT_HANDLE_LINK();
                // current file is now the link destination file. Parse it again
                skip_namecmp_because_link = true;
                current_token--;
            }
            
            current_token++;
            
        } else {
            free(path);
            return ((*to_mount) != NULL);
        }
    }
    
    free(path);
    return ((*to_mount) != NULL);
}
bool resolve_mount_explicit_path(const char *orig_path, FileDescriptor **to_mount, char *explicit_path) {
    size_t path_strlen = strlen(orig_path);
    char *path = (char*) malloc(sizeof(char) * (strlen(orig_path) + 1));
    memcpy(path, orig_path, strlen(orig_path) + 1);
    // split the path name in tokens
    size_t num_tokens = strsplit(path, DIR_SEPARATOR, path_strlen);
    if (num_tokens == 0) {
        free(path);
        return false;
    }
    
    size_t current_token = 0;
    size_t explicit_path_pointer = 0;
    FileDescriptor *current_file = root;
    
    bool skip_namecmp_because_link = false;
    
    (*to_mount) = NULL;
    
    // walk the "token list"
    while (current_token < num_tokens) {
        char *current_filename = strsplit_chunk_at(path, path_strlen, current_token);
        //printf("%s <-> %s (%p)\n", current_filename, current_file->name, current_file);
        if ((strcmp(current_filename, current_file->name) == 0) || skip_namecmp_because_link) { // match token <-> current file name
            if (!skip_namecmp_because_link) {
                explicit_path_pointer += sprintf(explicit_path + explicit_path_pointer, "%s,", current_filename);
            }
            skip_namecmp_because_link = false;
            
            RESOLVE_MOUNT_DETECT_MOUNT();
            
            if (current_token == num_tokens-1) {  // last file
                if (current_file->filetype == Link) {
                    RESOLVE_MOUNT_HANDLE_LINK_EXPLICIT();
                    RESOLVE_MOUNT_DETECT_MOUNT();
                }
                
                free(path);
                explicit_path[explicit_path_pointer-1] = '\0';  /* remove trailing separator */
                return ((*to_mount) != NULL);
            }
            
            // not last token
            if (current_file->filetype == Directory) {  // extract the next file using the next token
                char *next_filename = strsplit_chunk_at(path, path_strlen, current_token+1);
                if (!file_method(current_file)->get_child(current_file, next_filename, &current_file)) {
                    free(path);
                    explicit_path[explicit_path_pointer-1] = '\0';  /* remove trailing separator */
                    return ((*to_mount) != NULL);
                }
            } else if (current_file->filetype == Link) {
                RESOLVE_MOUNT_HANDLE_LINK_EXPLICIT();
                // current file is now the link destination file. Parse it again
                skip_namecmp_because_link = true;
                current_token--;
            }
            
            current_token++;
            
        } else {
            free(path);
            explicit_path[explicit_path_pointer-1] = '\0';  /* remove trailing separator */
            return ((*to_mount) != NULL);
        }
    }
    
    free(path);
    explicit_path[explicit_path_pointer-1] = '\0';  /* remove trailing separator */
    return ((*to_mount) != NULL);
}
bool resolve_parent_path(const char *child_path, FileDescriptor **to_file) {
    // make a copy of the input path
    char *path = (char*) malloc(sizeof(char) * (strlen(child_path) + 1));
    memcpy(path, child_path, strlen(child_path) + 1);
    
    // replace the last path separator with a null character
    char *last_sep = strrchr(path, DIR_SEPARATOR);
    if (last_sep == NULL) {
        free(path);
        return false;
    }
    last_sep[0] = '\0';
    
    FileDescriptor *parent_directory;
    if (!resolve_path_deep(path, &parent_directory)) {
        free(path);
        return false;
    }
    free(path);
    (*to_file) = parent_directory;
    return true;
}
bool extract_file_name(const char *full_path, char *just_name) {
    char *last_sep = strrchr(full_path, DIR_SEPARATOR);
    if (last_sep == NULL) {
        strncpy(just_name, full_path, MAX_FILENAME_LENGTH);
        return true;
    }
    
    strncpy(just_name, last_sep+1, MAX_FILENAME_LENGTH);
    
    return true;
}
bool extract_file_directory_path(const char *full_path, char *directory_path) {
    char *last_sep = strrchr(full_path, DIR_SEPARATOR);
    if (last_sep == NULL) {
        strcpy(directory_path, full_path);
        return true;
    }
    char bak = last_sep[0];
    last_sep[0] = '\0';
    strcpy(directory_path, full_path);
    last_sep[0] = bak;
    
    return true;
}
bool install_file(const char *orig_path, FileDescriptor *file) {
    FileDescriptor *install_directory;
    
    if (!resolve_parent_path(orig_path, &install_directory)) {
        return false;
    }
    
    if (!file_method(install_directory)->insert_into_directory(install_directory, file)) {
        return false;
    }
    
    if (!extract_file_name(orig_path, file->name)) {
        return false;  //TODO: clean up the state if something fails here
    }
    
    return true;
}

