#include <data/fsdiskio.h>
#include <peripherals/storage.h>


bool diskiofs_create_file(FileDescriptor **fd, char *name, FileMetaData meta) {
    FileDescriptor *newfile = (FileDescriptor*) malloc(sizeof(FileDescriptor));
    strncpy(newfile->name, name, MAX_FILENAME_LENGTH);
    strncpy(newfile->driver, ROOTFS_DRIVER_NAME, MAX_FS_DRIVER_NAME_LENGTH);
    newfile->parent = NULL;
    newfile->metadata = meta;
    newfile->filetype = File;
    newfile->access_strategy = RAW_DISKIO;
    
    FSDiskIOFileAccessData *file = (FSDiskIOFileAccessData*) malloc(sizeof(FSDiskIOFileAccessData));
    file->diskid = 0;
    file->disksize = 0;
    file->offset = 0;
    newfile->access_data = file;
    
    (*fd) = newfile;
    return true;
}


void diskio_bind_file(FileDescriptor *fd, unsigned int diskid, uint64_t offset, uint64_t size) {
    FSDiskIOFileAccessData *file = (FSDiskIOFileAccessData*) fd->access_data;
    file->diskid = diskid;
    file->offset = offset;
    if (size == 0) {
        file->disksize = get_disk_id_capacity(diskid);
    } else {
        file->disksize = size;
    }
}


bool diskiofs_remove_file(FileDescriptor **dead_file) {
    FileDescriptor *target = *dead_file;
    FileDescriptor *parent = target->parent;
    
    FSDiskIOFileAccessData *diskdata = (FSDiskIOFileAccessData*) target->access_data;
    free(diskdata);
    
    if (parent != NULL) {
        file_method(parent)->unlist_file(parent, target);
    }
    free(target);
    return true;
}


size_t diskiofs_impl_read_file(void *dest, size_t num_bytes, size_t seek, FileDescriptor *file) {
    FSDiskIOFileAccessData *access = file->access_data;
    return read_from_disk_id(access->diskid, seek + access->offset, num_bytes, (uint8_t*) dest);
}

size_t diskiofs_impl_write_file(const void *src, size_t num_bytes, size_t seek, FileDescriptor *file) {
    FSDiskIOFileAccessData *access = file->access_data;
    return write_to_disk_id(access->diskid, seek + access->offset, num_bytes, (const uint8_t*) src);
}

size_t diskiofs_impl_get_filesize(FileDescriptor *file) {
    FSDiskIOFileAccessData *disk = (FSDiskIOFileAccessData*) file->access_data;
    return disk->disksize;
}
