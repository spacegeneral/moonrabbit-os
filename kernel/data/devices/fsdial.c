#include <stdio.h>
#include <string.h>

#include <data/fsdial.h>


bool dialfs_create_file(FileDescriptor** fd, char* name, FileMetaData meta) {
    FileDescriptor* newfile = (FileDescriptor*) malloc(sizeof(FileDescriptor));
    strncpy(newfile->name, name, MAX_FILENAME_LENGTH);
    strncpy(newfile->driver, ROOTFS_DRIVER_NAME, MAX_FS_DRIVER_NAME_LENGTH);
    newfile->parent = NULL;
    newfile->metadata = meta;
    newfile->filetype = File;
    newfile->access_strategy = DIAL_READ;
    
    newfile->access_data = NULL;
    
    (*fd) = newfile;
    return true;
}


void dial_bind_callback(FileDescriptor* fd, dial_reading_callback callback) {
    fd->access_data = callback;
}


size_t dialfs_impl_read_file(void* dest, size_t num_bytes, size_t seek, FileDescriptor* file) {
    char* readblock = (char*) malloc(sizeof(char)*  MAX_DIAL_READBLOCK);
    memset(readblock, 0, MAX_DIAL_READBLOCK);
    char* destptr = (char*) dest;
    
    dial_reading_callback callback = (dial_reading_callback) file->access_data;
    size_t data_size = callback(file->name, readblock, MAX_DIAL_READBLOCK);
    size_t read_bytes = 0;
    for (size_t pos = seek; (pos < data_size) && (read_bytes < num_bytes); pos++) {
        destptr[read_bytes] = readblock[pos];
        read_bytes++;
    }
    free(readblock);
    
    return read_bytes;
}

size_t dialfs_impl_write_file(const void* src, size_t num_bytes, size_t seek, FileDescriptor* file) {
    (void)src; // unused
    (void)num_bytes; // unused
    (void)seek; // unused
    (void)file; // unused
    return 0;
}

size_t dialfs_impl_get_filesize(FileDescriptor* file) {
    char* readblock = (char*) malloc(sizeof(char)*  MAX_DIAL_READBLOCK);
    dial_reading_callback callback = (dial_reading_callback) file->access_data;
    size_t totalsize = callback(file->name, readblock, MAX_DIAL_READBLOCK);
    free(readblock);
    
    return totalsize;
}
