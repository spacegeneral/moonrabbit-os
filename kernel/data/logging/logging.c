#include <stdio.h>
#include <stdlib.h>
#include <data/logging.h>
#include <interrupt/timing.h>
#include <data/virtual_file.h>
#include <data/path.h>

#include <sys/default_topics.h>
#include <security/defaults.h>
#include <ipc/interface.h>


static FILE *klogfile;
bool logging_system_ready = false;


void klog(char *fmt, ...) {
    if (logging_system_ready) {
        fprintf(klogfile, "[%08llu] ", system_clock_millisec());
        fseek(klogfile, -1, SEEK_CUR);
        va_list va;
        va_start(va, fmt);
        vfprintf(klogfile, fmt, va);
        va_end(va);
        fseek(klogfile, -1, SEEK_CUR);
    }
}


static void klog_message_handler(const void *message) {
    char *msg = (char*) message;
    klog(msg);
}


void setup_klog_system() {
    FileDescriptor* logfile;
    FileDescriptor* dest_folder;
    FileMetaData meta = {
        .race_attrib = Elf,
        .class_attrib = Priest,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    virtual_fs_primitives.create_file(&logfile, KLOGFILE_NAME, meta);
    resolve_path(KLOGFILE_FOLDER, &dest_folder);
    file_method(dest_folder)->insert_into_directory(dest_folder, logfile);
    
    klogfile = fopen(KLOGFILE, "w");
    if (klogfile == NULL) printf("Warning: could not create kernel log file!");
    
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_KLOG_TOPIC, 1024, sizeof(char*), DEFAULT_SEC_KLOG_TOPIC_RACE, DEFAULT_SEC_KLOG_TOPIC_CLASS, DEFAULT_SEC_KLOG_TOPIC_POLICY);
    
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_KLOG_TOPIC, &klog_message_handler);
    
    logging_system_ready = true;
    klog("Created kernel log file\n");
}

