#include <data/interface.h>
#include <data/vfs.h>
#include <data/logging.h>
#include <data/path.h>
#include <stdio.h>
#include <kernel/kidou.h>
#include <kernel/core.h>
#include <mm/mm.h>
#include <peripherals/interface.h>


int vfs_readylevel_internal = 0;

static int vfs_ready_level() {
    return vfs_readylevel_internal;
}

static void vfs_initialize() {
#ifdef VERBOSE_STARTUP
    printf("Initializing virtual filesystem.\n    Setting up root... ");
#endif
    setup_root_fs();
#ifdef VERBOSE_STARTUP
    printf(" done!\n    Setting up kernel logger... ");
#endif
    setup_klog_system();
#ifdef VERBOSE_STARTUP
    printf("done!\nVirtual filesystem 50%% ready.\n");
#endif
    vfs_readylevel_internal = 50;
}

static void vfs_finalize() {
    if (CALL_IMPL(peripherals, interr_ctrl_subsystem, ready_level) < 50) {
        printf("Failed to start vfs subsystem: peripherals subsystem not yet initialized!\n");
        panic();
    }
#ifdef VERBOSE_STARTUP
    printf("Finalizing virtual filesystem.\n    Setting up special files... ");
#endif
    setup_mem_special_files();
#ifdef VERBOSE_STARTUP
    printf("done!\nVirtual filesystem 100%% ready.\n");
#endif
    vfs_readylevel_internal = 100;
}

static void vfs_teardown() {
    vfs_readylevel_internal = 0;
}


UNIQUE_IMPLEMENTATION_OF(vfs, data_subsystem, { \
    .ready_level = &vfs_ready_level, \
    .initialize = &vfs_initialize, \
    .finalize = &vfs_finalize, \
    .teardown = &vfs_teardown \
})

// run finalize only after the storage subsystem is ready!
// (the storage subsystem is part of the peripherals subsystem)
