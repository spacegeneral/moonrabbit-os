#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/errno.h>
#include <data/vfs.h>
#include <data/path.h>
#include <data/fs/internalfs.h>
#include <data/virtual_file.h>
#include <security/authorize.h>
#include <mm/mm.h>


FileDescriptor* make_system_core_dir(char* name, FileMetaData metadata) {
    FileDescriptor* coredir;
    virtual_fs_primitives.create_directory(&coredir, name, metadata);
    return coredir;
}

FileDescriptor* make_system_core_file(char* name, FileMetaData metadata) {
    FileDescriptor* corefile;
    virtual_fs_primitives.create_file(&corefile, name, metadata);
    return corefile;
}


bool internalfs_mount(FileDescriptor** root, 
                      char* fs_file_path, 
                      FileMetaData metadata) {
    (void)fs_file_path; // unused
    FileDescriptor* new_root;
    
    new_root = make_system_core_dir("f", metadata);
    new_root->access_strategy = PURE_VIRTUAL;
    FileDescriptor* metal = make_system_core_dir("metal", metadata);
    FileDescriptor* part = make_system_core_dir("part", metadata);
    FileDescriptor* fs = make_system_core_dir("fs", metadata);
    FileDescriptor* sys = make_system_core_dir("sys", metadata);
    FileDescriptor* def = make_system_core_dir("def", metadata);
    FileDescriptor* dials = make_system_core_dir("dial", metadata);
    FileDescriptor* dials_h = make_system_core_dir("human", metadata);
    FileDescriptor* dials_maps = make_system_core_dir("maps", metadata);
    FileDescriptor* dials_r = make_system_core_dir("robot", metadata);
    FileDescriptor* disk = make_system_core_dir("disk", metadata);
    
    file_method(new_root)->insert_into_directory(new_root, metal);
    file_method(metal)->insert_into_directory(metal, disk);
    file_method(metal)->insert_into_directory(metal, part);
    file_method(new_root)->insert_into_directory(new_root, fs);
    file_method(new_root)->insert_into_directory(new_root, def);
    file_method(new_root)->insert_into_directory(new_root, sys);
    file_method(sys)->insert_into_directory(sys, dials);
    file_method(dials)->insert_into_directory(dials, dials_h);
    file_method(dials)->insert_into_directory(dials, dials_r);
    file_method(dials_h)->insert_into_directory(dials_h, dials_maps);
    
    (*root) = new_root;
    
    return true;
}


#define TOUCH_FILE_BOILERPLATE(requester, pathname, opt_pathname, creation_fun) \
{ \
    FileDescriptor* existing_file; \
    \
    /* check if the file already exists */ \
    bool can_resolve_path = resolve_path(pathname, &existing_file); \
    \
    if (!can_resolve_path) {  /* file does not exist: create one */ \
        FileDescriptor* parent_directory; \
        /* check if there is a directory to contain it */ \
        bool can_resolve_parent = resolve_parent_path(pathname, &parent_directory); \
        if (!can_resolve_parent) { \
            *result = FILE_DOES_NOT_EXIST; \
            return;  /* file not found */ \
        } \
        /* check if we are authorized to modify it */ \
        if (!(authorize_file(parent_directory,  \
                             requester->phys_race_attrib, requester->data_class_attrib))) { \
            *result = SECURITY_DENIED; \
            return;  /* not enough permission */ \
        } \
        /* check if it's writable*/ \
        if (!parent_directory->metadata.writable) { \
            *result = CANNOT_CREATE_FILE; \
            return;  /* directory read only */ \
        } \
        FileMetaData newmeta = { \
            .race_attrib = requester->phys_race_attrib, \
            .class_attrib = requester->data_class_attrib, \
            .executable = true, \
            .writable = true, \
            .readable = true, \
            .creation_time = time(NULL), \
            .modification_time = time(NULL) \
        }; \
        /* do create a new file*/ \
        if(!creation_fun(requester, pathname, opt_pathname, newmeta)) { \
            *result = INTERNAL_FS_ERROR; \
            return;  /* cannot list for reasons */ \
        } \
        \
    } else {  /* update modification time */ \
        if (!(authorize_file(existing_file,  \
                             requester->phys_race_attrib, requester->data_class_attrib))) { \
            *result = SECURITY_DENIED; \
            return;  /* not enough permission */ \
        } \
        FileMetaData* existing_meta = &(existing_file->metadata); \
        existing_meta->modification_time = time(NULL); \
    } \
    \
    *result = 0; \
}



#define MAKE_FILE_BOILERPLATE(agent, creation_fun) \
{ \
    (void)_; /* don't-care, always unused (parameter just used for interface consistency to reduce code replication) */ \
    (void)agent; /* unused */ \
    FileDescriptor* parent_directory; \
    if (!resolve_parent_path(path, &parent_directory)) { \
        return false; \
    } \
    FileDescriptor* new_file; \
    if (!file_method(parent_directory)->creation_fun(&new_file, "a", meta)) { \
        return false; \
    } \
    if (!install_file(path, new_file)) { \
        return false; \
    } \
     \
    return true; \
}

bool _create_file(IdentifiedAgent* agent, const char* path, const char* _, FileMetaData meta) \
    MAKE_FILE_BOILERPLATE(agent, create_file)
    
bool _create_directory(IdentifiedAgent* agent, const char* path, const char* _, FileMetaData meta) \
    MAKE_FILE_BOILERPLATE(agent, create_directory)

bool make_link(IdentifiedAgent* agent, const char* path, const char* _, FileMetaData meta) \
    MAKE_FILE_BOILERPLATE(agent, create_link)
bool _create_link(IdentifiedAgent* agent, const char* path_from, const char* path_to, FileMetaData meta) {
    if (!make_link(agent, path_from, NULL, meta)) return false;
    FileDescriptor* link;
    if (!resolve_path(path_from, &link)) return false;
    if (!file_method(link)->retarget_link(link, path_to)) return false;
    
    return true;
}


void internalfs_create_file(IdentifiedAgent* agent, const char* path, int* result) \
    TOUCH_FILE_BOILERPLATE(agent, path, NULL, _create_file)
    
void internalfs_create_directory(IdentifiedAgent* agent, const char* path, int* result) \
    TOUCH_FILE_BOILERPLATE(agent, path, NULL, _create_directory)

void internalfs_create_link(IdentifiedAgent* agent, const char* path_from, const char* path_to, int* result) \
    TOUCH_FILE_BOILERPLATE(agent, path_from, path_to, _create_link)


void internalfs_remove(IdentifiedAgent* agent, const char* path, int* result) {
    FileDescriptor* targetfile;
    bool fileexists = resolve_path(path, &targetfile);
    
    if (!fileexists) {
        *result = -1;
        return;
    }
    if (!(authorize_file(targetfile, 
                         agent->phys_race_attrib, agent->data_class_attrib))) {
        *result = -1;  // not enough permission
        return;
    }
    FileDescriptor* parent = targetfile->parent;
    if (parent == NULL || parent->filetype != Directory) {
        *result = -1;  // wtf
        return;
    }
    
    if (!file_method(parent)->unlist_file(parent, targetfile)) {
        *result = -1;  // cant unlist
        return;
    }
    
    bool removal_success;
    if (targetfile->filetype == Directory) {
        removal_success = file_method(targetfile)->remove_empty_directory(&targetfile);
    } else {
        removal_success = file_method(targetfile)->remove_file(&targetfile);
    }
    
    if (!removal_success) {
        *result = -1;  // cant remove
        return;
    }

    *result = 0;
}

bool alist_directory(FileDescriptor* directory, DirEnt** entries, size_t* num_files) {
    size_t nfiles = 0;
    FileDescriptor** content;
    DirEnt* filelist = NULL;
    if (!file_method(directory)->alist_directory(directory, &content, &nfiles)) {
        (*entries) = NULL;
        (*num_files) = 0;
        return false;
    }
    if (nfiles > 0) filelist = (DirEnt*) malloc(sizeof(DirEnt)*  nfiles);
    memset(filelist, 0, sizeof(DirEnt)*  nfiles);
    
    for (size_t n=0; n<nfiles; n++) {
        strncpy(filelist[n].name, content[n]->name, MAX_FILENAME_LENGTH);
        memcpy(&(filelist[n].metadata), &(content[n]->metadata), sizeof(FileMetaData));
        filelist[n].filetype = content[n]->filetype;
        filelist[n].filesize = file_method(content[n])->impl_get_filesize(content[n]);
    }
    free(content);
    
    (*entries) = filelist;
    (*num_files) = nfiles;
    return true;
}


void internalfs_list_directory(IdentifiedAgent* agent, 
                               const char* path, 
                               DirEnt** entries, 
                               size_t* num_files, 
                               int* result) {
    FileDescriptor* directory;
    bool can_resolve_path = resolve_path(path, &directory);
    if (!can_resolve_path) {
        *result = FILE_DOES_NOT_EXIST;  // file not found
        return;
    }
    
    if (!(authorize_file(directory, 
                         agent->phys_race_attrib, agent->data_class_attrib))) {
        *result = SECURITY_DENIED;  // not enough permission
        return;
    }
    
    if (directory->filetype == Link) {
        char* linkdest;
        bool can_resolve_link = file_method(directory)->aget_link_dest(directory, &linkdest);
        if (!can_resolve_link) {
            free(linkdest);
            *result = FILE_DOES_NOT_EXIST;  // file not found
            return;
        }
        bool can_resolve_path = resolve_path(linkdest, &directory);
        free(linkdest);
        if (!can_resolve_path) {
            *result = FILE_DOES_NOT_EXIST;  // file not found
            return;
        }
    }
    
    if (directory->filetype != Directory) {
        *result = NOT_A_DIRECTORY;  // not a directory
        return;
    }
    if (!directory->metadata.readable) {
        *result = CANNOT_READ_FILE;  // not readable
        return;
    }
    
    bool can_list_dir = alist_directory(directory, entries, num_files);
    if (!can_list_dir) {
        *num_files = 0;
        *result = 0;
        return;
    }
    
    // assign the allocated memory to the agent
    void* used_mem =* entries;
    bool success_own = transfer_mem_ownership(used_mem, agent->uid);
    if (!success_own) {
        free(used_mem);
        *result = CANNOT_TRANSFER_MEM;  // cannot change memory ownership
        return;
    }
    
    *result = 0;
}

void internalfs_count_directory_entries(IdentifiedAgent* agent, 
                                        const char* path, 
                                        size_t* num_files, 
                                        int* result) {
    FileDescriptor* directory;
    *num_files = 0;
    
    bool can_resolve_path = resolve_path(path, &directory);
    if (!can_resolve_path) {
        *result = FILE_DOES_NOT_EXIST;  // file not found
        return;
    }
    
    if (!(authorize_file(directory, 
                         agent->phys_race_attrib, agent->data_class_attrib))) {
        *result = SECURITY_DENIED;  // not enough permission
        return;
    }
    
    if (directory->filetype == Link) {
        char* linkdest;
        bool can_resolve_link = file_method(directory)->aget_link_dest(directory, &linkdest);
        if (!can_resolve_link) {
            free(linkdest);
            *result = FILE_DOES_NOT_EXIST;  // file not found
            return;
        }
        bool can_resolve_path = resolve_path(linkdest, &directory);
        free(linkdest);
        if (!can_resolve_path) {
            *result = FILE_DOES_NOT_EXIST;  // file not found
            return;
        }
    }
    
    if (directory->filetype != Directory) {
        *result = NOT_A_DIRECTORY;  // not a directory
        return;
    }
    if (!directory->metadata.readable) {
        *result = CANNOT_READ_FILE;  // not readable
        return;
    }
    
    //TODO optimize this
    DirEnt* entries;
    bool can_list_dir = alist_directory(directory, &entries, num_files);
    if (!can_list_dir) {
        *num_files = 0;
        *result = CANNOT_LIST_DIRECTORY;
        return;
    }
    
    *result = 0;
    free(entries);
}

void internalfs_get_directory_entry_at(IdentifiedAgent* agent, 
                                       const char* path, 
                                       size_t position, 
                                       DirEnt* entry,
                                       int* result) {
    FileDescriptor* directory;
    bool can_resolve_path = resolve_path(path, &directory);
    if (!can_resolve_path) {
        *result = FILE_DOES_NOT_EXIST;  // file not found
        return;
    }
    
    if (!(authorize_file(directory, 
                         agent->phys_race_attrib, agent->data_class_attrib))) {
        *result = SECURITY_DENIED;  // not enough permission
        return;
    }
    
    if (directory->filetype == Link) {
        char* linkdest;
        bool can_resolve_link = file_method(directory)->aget_link_dest(directory, &linkdest);
        if (!can_resolve_link) {
            free(linkdest);
            *result = FILE_DOES_NOT_EXIST;  // file not found
            return;
        }
        bool can_resolve_path = resolve_path(linkdest, &directory);
        free(linkdest);
        if (!can_resolve_path) {
            *result = FILE_DOES_NOT_EXIST;  // file not found
            return;
        }
    }
    
    if (directory->filetype != Directory) {
        *result = NOT_A_DIRECTORY;  // not a directory
        return;
    }
    if (!directory->metadata.readable) {
        *result = CANNOT_READ_FILE;  // not readable
        return;
    }
    
    //TODO optimize this
    DirEnt* entries;
    size_t num_files;
    bool can_list_dir = alist_directory(directory, &entries, &num_files);
    if (!can_list_dir) {
        *result = CANNOT_LIST_DIRECTORY;
        return;
    }
    
    if (position < num_files) {
        *entry = entries[position];
        *result = 0;
    } else {
        *result = CANNOT_LIST_DIRECTORY;
    }
    
    free(entries);
}

void internalfs_file_exists(IdentifiedAgent* agent, const char* path, int* result) {
    (void)agent; // unused
    FileDescriptor* file;
    bool can_resolve_path = resolve_path(path, &file);
    *result = can_resolve_path;
}

void internalfs_move(IdentifiedAgent* agent, const char* path_from, const char* path_to, int* result) {
    FileDescriptor* from_file;
    FileDescriptor* to_file;
    FileDescriptor* to_folder;
    
    bool from_file_exists = resolve_path(path_from, &from_file);
    if (!from_file_exists) {
        *result = FILE_DOES_NOT_EXIST;
        return;
    }
    
    if (!(authorize_file(from_file,
                         agent->phys_race_attrib, agent->data_class_attrib)) || 
        !(authorize_file(from_file->parent,
                         agent->phys_race_attrib, agent->data_class_attrib))) {
        *result = SECURITY_DENIED;
        return;
    }

    bool to_file_exists = resolve_path(path_to, &to_file);
    if (to_file_exists) {
        *result = CANNOT_CREATE_FILE;
        return;
    }
    
    char* path_to_folder = (char*) malloc(sizeof(char) * (strlen(path_to) + 1));
    if (!extract_file_directory_path(path_to, path_to_folder)) {
        *result = INTERNAL_FS_ERROR;
        free(path_to_folder);
        return;
    }
    
    bool to_folder_exists = resolve_path(path_to_folder, &to_folder);
    free(path_to_folder);
    if (!to_folder_exists) {
        *result = CANNOT_CREATE_DIR;
        return;
    }
    
    if (!(authorize_file(to_folder,
                         agent->phys_race_attrib, agent->data_class_attrib))) {
        *result = SECURITY_DENIED;
        return;
    }
    
    file_method(from_file->parent)->unlist_file(from_file->parent, from_file);
    if (!install_file(path_to, from_file)) {
        *result = INTERNAL_FS_ERROR;
        return;
    }
    *result = 0;
}

void internalfs_fopen(IdentifiedAgent* agent, const char* path, const char* mode, FILE* file, int* status) {
    FileDescriptor* testfile;
    bool fileexists = resolve_path(path, &testfile);
    if (!fileexists) {
        bool can_create = false;
        if ((strchr(mode, 'w') != NULL) || (strchr(mode, 'a') != NULL)) {
            int creation_result;
            internalfs_create_file(agent, path, &creation_result);
            can_create = (creation_result == 0);
        } else {
            *status = FILE_DOES_NOT_EXIST;
            return;
        }
        if (!can_create) {
            *status = CANNOT_CREATE_FILE;
            return;
        }
    } 
    
    fileexists = resolve_path(path, &testfile);
    if (!fileexists) {
        *status = CANNOT_CREATE_FILE;
        return;
    }
    
    if (!(authorize_file(testfile,
                            agent->phys_race_attrib, agent->data_class_attrib))) {
        *status = SECURITY_DENIED;
        return;  /* not enough permission */
    }
    if (testfile->filetype != File) {
        *status = NOT_A_FILE;
        return;
    }
    // TODO implement read/write modes
    OpenFile ofp;
    bool open_ok = generic_open_file(agent->uid, testfile, &ofp);
    if (!open_ok) {
        *status = INTERNAL_FS_ERROR;
        return;
    }
    strncpy(ofp.driver, ROOTFS_DRIVER_NAME, MAX_FS_DRIVER_NAME_LENGTH);
    
    (*status) = 0;
    (*file) = *(generic_convert_OpenFile_to_FILE(&ofp));
}

void internalfs_fclose(IdentifiedAgent* agent, FILE* file, int* result) {
    (void)agent; // unused
    OpenFile *ofile = generic_convert_FILE_to_OpenFile(file);
    bool close_ok = generic_close_file(ofile);
    if (!close_ok) {
        *result = -1;
        return;
    }
    *result = 0;
}

void internalfs_fflush(IdentifiedAgent* agent, FILE* file, int* result) {
    (void)agent; // unused
    (void)file; // unused
    *result = 0;
}

void internalfs_fread(IdentifiedAgent* agent, void* dest, size_t size, size_t nmemb, FILE* file, size_t* result) {
    (void)agent; // unused
    size_t nbytes = size*  nmemb;
    size_t read_bytes = generic_read_file(dest, nbytes, generic_convert_FILE_to_OpenFile(file));
    *result = (read_bytes / size);
}

void internalfs_fwrite(IdentifiedAgent* agent, const void* src, size_t size, size_t nmemb, FILE* file, size_t* result) {
    (void)agent; // unused
    size_t nbytes = size*  nmemb;
    size_t write_bytes = generic_write_file(src, nbytes, generic_convert_FILE_to_OpenFile(file));
    *result = (write_bytes / size);
}

void internalfs_fseek(IdentifiedAgent* agent, FILE* stream, long offset, int whence, int* result) {
    (void)agent; // unused
    long long file_size = generic_get_filesize(generic_convert_FILE_to_OpenFile(stream));
    if (whence == SEEK_SET) {
        if (offset < file_size) {
            stream->seek_pos = offset;
            *result = 0;
            return;
        }
    } else if (whence == SEEK_CUR) {
        if (stream->seek_pos + offset < file_size) {
            stream->seek_pos += offset;
            *result = 0;
            return;
        }
    } else if (whence == SEEK_END) {
        if (file_size - offset >= 0) {
            stream->seek_pos = file_size - offset;
            *result = 0;
            return;
        }
    }
    *result = -1;
}

void internalfs_chsec(IdentifiedAgent* agent, const char* path, int newrace, int newclass, int* result) {
    FileDescriptor* testfile;
    
    bool fileexists = resolve_path(path, &testfile);
    if (!fileexists) {
        *result = FILE_DOES_NOT_EXIST;
        return;
    }
    
    if (!(authorize_file(testfile,
                         agent->phys_race_attrib, agent->data_class_attrib))) {
        *result = SECURITY_DENIED;
        return;  /* not enough permission */
    }
    
    if (!authorize_simple(newrace, newclass, 
                          agent->phys_race_attrib, agent->data_class_attrib)) {
        *result = SECURITY_DENIED;
        return;  /* not enough permission */
    }
    
    FileMetaData newmeta = testfile->metadata;
    newmeta.race_attrib = newrace;
    newmeta.class_attrib = newclass;
    
    if (!file_method(testfile)->change_metadata(testfile, newmeta)) {
        *result = INTERNAL_FS_ERROR;
        return;  /* wot */
    }
    
    *result = 0;
}

