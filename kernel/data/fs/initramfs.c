#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <data/vfs.h>
#include <data/path.h>
#include <data/virtual_file.h>
#include <data/fs/initramfs.h>
#include <miniz/miniz.h>


bool initramfs_get_root_mountpoint(FileDescriptor **root, 
                                   char *fs_file_path, 
                                   FileMetaData metadata) {
    FILE *fp = fopen(fs_file_path, "rb");
    
    if (fp == NULL) return false;
    
    (*root) = build_vfs_from_file(fp);
    
    if ((*root) == NULL) return false;
    
    (*root)->metadata = metadata;
    snprintf((*root)->name, MAX_FILENAME_LENGTH, "initramfs");
    fclose(fp);
    return true;
}


bool load_directory(FILE *fp, FileDescriptor **entrymap, size_t numentries) {
    for (size_t en=0; en<numentries; en++) {
        FileDescriptor *current = (FileDescriptor*) malloc(sizeof(FileDescriptor));
        if (fread(current, sizeof(FileDescriptor), 1, fp) != 1) return false;
        entrymap[en] = current;
    }
    return true;
}

bool finalize_entry(FILE *fp, FileDescriptor **entrymap, size_t entry_index) {
    FileDescriptor *entry = entrymap[entry_index];
    uint32_t data_start = (uint32_t) entry->access_data;
    fseek(fp, data_start, SEEK_SET);

    if (entry->filetype == File) {
        uint32_t compressed_content_size;
        uint32_t uncompressed_content_size;
        if (fread(&uncompressed_content_size, sizeof(uint32_t), 1, fp) != 1) return false;
        if (fread(&compressed_content_size, sizeof(uint32_t), 1, fp) != 1) return false;
        
        PureVirtualFileAccessData *file = (PureVirtualFileAccessData*) malloc(sizeof(PureVirtualFileAccessData));
        if ((compressed_content_size == 0) || (uncompressed_content_size == 0)) {
            file->content = NULL;
            file->size = 0;
        } else {
            uint8_t *compressed_data = (uint8_t*) malloc(sizeof(uint8_t) * compressed_content_size);
            if (fread(compressed_data, sizeof(uint8_t), compressed_content_size, fp) != compressed_content_size) return false;
            
            //file->content = (uint8_t*) malloc(sizeof(uint8_t) * uncompressed_content_size);
            unsigned long content_size_precise = uncompressed_content_size;
            file->content = tinfl_decompress_mem_to_heap(compressed_data, compressed_content_size, &content_size_precise, TINFL_FLAG_PARSE_ZLIB_HEADER);
            free(compressed_data);
            if (file->content == NULL) {
                printf("Un-compression failure!\n");
                return false;
            }
            file->size = content_size_precise;
            //file->content = realloc(file->content, content_size_precise);
            
        }
        entry->access_data = file;
    }
    
    else if (entry->filetype == Directory) {
        uint32_t num_entries;
        if (fread(&num_entries, sizeof(uint32_t), 1, fp) != 1) return false;

        PureVirtualDirectoryAccessData *diraccess = (PureVirtualDirectoryAccessData*) malloc(sizeof(PureVirtualDirectoryAccessData));
        array_new(&(diraccess->children));
        
        for (size_t ch=0; ch<num_entries; ch++) {
            uint32_t child_index;
            if (fread(&child_index, sizeof(uint32_t), 1, fp) != 1) return false;
            
            FileDescriptor *child = entrymap[child_index];
            child->parent = entry;
            array_add(diraccess->children, child);
        }
        
        entry->access_data = diraccess;
    }
    
    else if (entry->filetype == Link) {
        uint32_t name_length;
        if (fread(&name_length, sizeof(uint32_t), 1, fp) != 1) return false;
        
        PureVirtualLinkAccessData *link = (PureVirtualLinkAccessData*) malloc(sizeof(PureVirtualLinkAccessData));
        
        char *tmp_name = (char*) malloc(sizeof(char) * (name_length+1));
        tmp_name[name_length] = '\0';
        if (fread(tmp_name, sizeof(char), name_length, fp) != name_length) return false;
        link->dest_name = tmp_name;
        
        entry->access_data = link;
    }
    
    else {
        return false;
    }
    
    return true;
}

FileDescriptor *build_vfs_from_file(FILE *fp) {
    InitramfsHeader header;
    if (fread(&header, sizeof(InitramfsHeader), 1, fp) != 1) return NULL;
    if (header.magic != INITRAMFS_MAGIC) {
        printf("Bad initrd magic: 0x%x\n", header.magic);
        return NULL;
    }
    
    FileDescriptor **entrymap = (FileDescriptor**) malloc(sizeof(FileDescriptor*) * header.num_entries);
    if (entrymap == NULL) return NULL;
    
    if (!load_directory(fp, entrymap, header.num_entries)) return NULL;

    for (size_t en=0; en<header.num_entries; en++) {
        if (!finalize_entry(fp, entrymap, en)) {
            printf("failure to finalize initrd entry %s\n", entrymap[en]->name);
            free(entrymap);
            return NULL;
        }
    }
    
    FileDescriptor *root = entrymap[0];
    free(entrymap);
    return root;
}
