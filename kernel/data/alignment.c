#include <data/alignment.h>



/*

  Simplified example
  
  +- unit 
  sector size = 4
  start address = 3
  data length = 12

  sec 0   sec 1   sec 2   sec 3   sec 4
  
 0       4       8      12      16      20
 |       |       |       |       |       |
 |   |   |   |   |   |   |   |   |   |   |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       |
       |<--------------------->|
          transferred bytes
          
 start sector = 3 // 4 == 0
 start sector address = 0 * 4 == 0
 start address unaligned = 3 % 4 != 0 ? TRUE
 unaligned initial bytes chunk = 4 - (3 % 4) == 1
 corrected data length = 12 - 1 + 4 == 15
 (NOTE: in a parallel universe where "data length = 11" and "start address = 4":
        corrected data length = 11 - 0 + 4 == 15  WRONG
        corrected data length = 11  RIGHT)
 
 end sector = (0 + 15) // 4 == 3
 end address unaligned = (0 + 15) % 4 != 0 ? TRUE
 sector count = 3 - 0 == 3
 BUT end address unaligned is TRUE
 sector count = sector count + 1 == 4
 
 */


DECL_ALIGN_IO_FRAME_INITIALIZER(uint64_t) {
    frame->start_address = start_address;
    frame->data_length = data_length;
    frame->sector_length = sector_length;
    
    frame->first_sector = frame->start_address / frame->sector_length;
    
    if (frame->first_sector == ((frame->start_address + frame->data_length) / frame->sector_length)) {  // all IO operation happens inside a single sector
        frame->last_sector = frame->first_sector;
        frame->sector_count = 1;
        
        frame->head_unaligned_offset_in_sector = frame->start_address - (frame->first_sector * frame->sector_length);
        frame->head_unaligned_bytes_length = frame->data_length;
        frame->core_aligned_sector_start = 0;
        frame->core_aligned_bytes_length = 0;
        frame->tail_unaligned_bytes_length = 0;
        
    } else {
        frame->head_unaligned_offset_in_sector = frame->start_address % frame->sector_length;
        bool start_address_unaligned = (frame->head_unaligned_offset_in_sector != 0);
        frame->head_unaligned_bytes_length = frame->sector_length - frame->head_unaligned_offset_in_sector;
        
        if (frame->head_unaligned_bytes_length == frame->sector_length) frame->head_unaligned_bytes_length = 0;
        
        // put the "corrected data length" in _temp
        if (start_address_unaligned) {
            frame->_temp = frame->data_length - frame->head_unaligned_bytes_length  + frame->sector_length;
            frame->core_aligned_sector_start = frame->first_sector + 1;
        } else {
            frame->_temp = frame->data_length;
            frame->core_aligned_sector_start = frame->first_sector;
        }
        
        frame->last_sector = ((frame->first_sector * frame->sector_length) + frame->_temp) / frame->sector_length;
        bool end_address_unaligned = ((((frame->first_sector * frame->sector_length) + frame->_temp) % frame->sector_length) != 0);
        
        frame->sector_count = frame->last_sector - frame->first_sector;
        frame->tail_unaligned_bytes_length = 0;
        
        if (end_address_unaligned) {
            frame->sector_count++;
            frame->tail_unaligned_bytes_length = frame->start_address + frame->data_length - (frame->last_sector * frame->sector_length);
        }
        
        frame->core_aligned_bytes_length = data_length - frame->head_unaligned_bytes_length - frame->tail_unaligned_bytes_length;
    }
} 
