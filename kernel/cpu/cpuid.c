#include <cpu/cpuid.h>
#include <string.h>
#include <asmutils.h>


void cpuid(unsigned int index, int regs[4]) {
    asm volatile(
        "cpuid            \n\t"
        : "=a"(regs[0]), [ebx] "=b"(regs[1]), "=c"(regs[2]), "=d"(regs[3])
        : "a"(index));
}


void cpuid_vendor_string(char *str) {
    int intview[4];
    
    cpuid(0, intview);
    
    int i = 0;
    int o = 0;
    for (i=4; i<8; i++) {
        str[o++] = ((char *)intview)[i];
    }
    for (i=12; i<16; i++) {
        str[o++] = ((char *)intview)[i];
    }
    for (i=8; i<12; i++) {
        str[o++] = ((char *)intview)[i];
    }
    
    str[12] = '\0';
}
