#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <asmutils.h>
#include <data/vfs.h>
#include <data/path.h>
#include <data/fsdial.h>
#include <peripherals/pci.h>
#include <collections/array.h>
#include <security/defaults.h>
#include <kernel/log.h>
#include <kernel/kidou.h>


void scan_bus(uint8_t busnum);

Array *pci_devices;


static size_t pci_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    return snprintf_lspci(dest, maxsize, false);
}

static size_t pci_f_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    return snprintf_lspci(dest, maxsize, true);
}

void pci_make_system_dial_files() {
    FileDescriptor* dialfile_h;
    FileDescriptor* dialfile_hv;
    FileDescriptor* dest_folder;
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_INFODIAL_RACE,
        .class_attrib = DEFAULT_SEC_INFODIAL_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    dial_fs_primitives.create_file(&dialfile_h, "pci", meta);
    dial_fs_primitives.create_file(&dialfile_hv, "pci.full", meta);
    dial_bind_callback(dialfile_h, &pci_dial_interface);
    dial_bind_callback(dialfile_hv, &pci_f_dial_interface);
    resolve_path(OS_DIALS_H_PATH, &dest_folder);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_h);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_hv);
}


uint32_t pci_read_config_long(uint8_t bus, 
                              uint8_t slot, 
                              uint8_t func,
                              uint8_t offset) {
    uint32_t address;
    uint32_t lbus  = (uint32_t)bus;
    uint32_t lslot = (uint32_t)slot;
    uint32_t lfunc = (uint32_t)func;
    
    address = (uint32_t)((lbus << 16) | (lslot << 11) | (lfunc << 8) | (offset & 0xfc) | ((uint32_t)0x80000000));
    
    outl(CONFIG_ADDRESS_PORT, address);
    return inl(CONFIG_DATA_PORT);
}

void pci_write_config_long(uint8_t bus, 
                           uint8_t slot, 
                           uint8_t func,
                           uint8_t offset,
                           uint32_t data_out) {
    uint32_t address;
    uint32_t lbus  = (uint32_t)bus;
    uint32_t lslot = (uint32_t)slot;
    uint32_t lfunc = (uint32_t)func;
    
    address = (uint32_t)((lbus << 16) | (lslot << 11) | (lfunc << 8) | (offset & 0xfc) | ((uint32_t)0x80000000));
    
    outl(CONFIG_ADDRESS_PORT, address);
    return outl(CONFIG_DATA_PORT, data_out);
}

uint16_t pci_read_config_word(uint8_t bus, 
                              uint8_t slot, 
                              uint8_t func,
                              uint8_t offset,
                              uint8_t which_word) {
    uint32_t conf_dword = pci_read_config_long(bus, slot, func, offset);
    return (uint16_t) ((conf_dword >> (which_word * 16)) & 0xFFFF);
}

uint8_t pci_read_config_byte(uint8_t bus, 
                             uint8_t slot, 
                             uint8_t func,
                             uint8_t offset,
                             uint8_t which_byte) {
    uint32_t conf_dword = pci_read_config_long(bus, slot, func, offset);
    return (uint8_t) ((conf_dword >> (which_byte * 8)) & 0xFF);
}

uint8_t pci_read_header_type(uint8_t bus, uint8_t slot, uint8_t func) {
    return pci_read_config_byte(bus, slot, func, 0x0C, 2);
}

uint16_t pci_read_vendor_id(uint8_t bus, uint8_t slot, uint8_t func) {
    return pci_read_config_word(bus, slot, func, 0x00, 0);
}

bool pci_device_exists(uint8_t bus, uint8_t slot, uint8_t func) {
    return (pci_read_vendor_id(bus, slot, func) != 0xFFFF);
}

uint8_t pci_bridge_read_secondary_bus(uint8_t bus, uint8_t slot, uint8_t func) {
    return pci_read_config_byte(bus, slot, func, 0x18, 1);
}

void pci_write_command(PciDevice * device, uint16_t command) {
    uint32_t write_value = (((uint32_t)device->status) << 16) | ((uint32_t)command);
    pci_write_config_long(device->bus, device->slot, device->function, 0x04, write_value);
    
    uint32_t current_register; // rescan content
    current_register = pci_read_config_long(device->bus, device->slot, device->function, 0x04);
    device->command = current_register & 0x0000FFFF;
    device->status = current_register >> 16;
}

void pci_enable_bus_mastering(PciDevice * device) {
    uint16_t cmd_enable_busmaster = device->command | 0b100;
    pci_write_command(device, cmd_enable_busmaster);
}


bool check_already_registered(PciDevice *candidate) {
    ARRAY_FOREACH(curr, pci_devices, {
        PciDevice *current = (PciDevice*) curr;
        if (current == candidate) return true;
        if (current->bus == candidate->bus && current->slot == candidate->slot && current->function == candidate->function) return true;
    });
    return false;
}

    
void scan_function(uint8_t bus, uint8_t device, uint8_t function) {
    PciDevice *new_device = (PciDevice*) malloc(sizeof(PciDevice));
    new_device->bus = bus;
    new_device->slot = device;
    new_device->function = function;
    
    if (check_already_registered(new_device)) {
        free(new_device);
        return;
    }
    
    array_add(pci_devices, new_device);
    
    uint32_t current_register;
    
    current_register = pci_read_config_long(bus, device, function, 0x00);
    new_device->vendor_id = current_register & 0x0000FFFF;
    new_device->device_id = current_register >> 16;
    
    current_register = pci_read_config_long(bus, device, function, 0x04);
    new_device->command = current_register & 0x0000FFFF;
    new_device->status = current_register >> 16;
    
    current_register = pci_read_config_long(bus, device, function, 0x08);
    new_device->revision_id = current_register & 0x000000FF;
    new_device->prog_if = (current_register >> 8) & 0x000000FF;
    new_device->subclass = (current_register >> 16) & 0x000000FF;
    new_device->class_code = (current_register >> 24) & 0x000000FF;

    current_register = pci_read_config_long(bus, device, function, 0x0C);
    new_device->cache_line_size = current_register & 0x000000FF;
    new_device->latency_timer = (current_register >> 8) & 0x000000FF;
    new_device->header_type = (current_register >> 16) & 0x000000FF;
    new_device->bist = (current_register >> 24) & 0x000000FF;
    
    if (is_pci_bridge(new_device)) {
        // PCI to PCI bridge: scan this other bus
        uint8_t secondary_bus = pci_bridge_read_secondary_bus(bus, device, function);
        scan_bus(secondary_bus);
    } else {  // Normal PCI device: scan all the remaining fields
        for (size_t b=0; b<6; b++) {
            new_device->bar[b] = pci_read_config_long(bus, device, function, 0x10 + 4*b);
        }
        
        new_device->cardbus_cis_pointer = pci_read_config_long(bus, device, function, 0x28);
        
        current_register = pci_read_config_long(bus, device, function, 0x2C);
        new_device->subsystem_vendor_id = current_register & 0x0000FFFF;
        new_device->subsystem_id = current_register >> 16;
        
        new_device->expansion_rom_base_address = pci_read_config_long(bus, device, function, 0x30);
        
        current_register = pci_read_config_long(bus, device, function, 0x34);
        new_device->capabilities_pointer = current_register & 0x000000FF;
        
        current_register = pci_read_config_long(bus, device, function, 0x3C);
        new_device->interrupt_line = current_register & 0x000000FF;
        new_device->interrupt_pin = (current_register >> 8) & 0x000000FF;
        new_device->min_grant = (current_register >> 16) & 0x000000FF;
        new_device->max_latency = (current_register >> 24) & 0x000000FF;
    }
    
}

void scan_device(uint8_t busnum, uint8_t devnum) {
    if (!pci_device_exists(busnum, devnum, 0)) return;
    
    scan_function(busnum, devnum, 0);
    uint8_t header_type = pci_read_header_type(busnum, devnum, 0);
    if ((header_type & 0x80) != 0) {  // multifunction device
        for (uint8_t pci_function=0; pci_function<8; pci_function++) {
            if (pci_device_exists(busnum, devnum, pci_function))
                scan_function(busnum, devnum, pci_function);
        }
    }
}

void scan_bus(uint8_t busnum) {
    for (uint8_t device=0; device<32; device++) {
        scan_device(busnum, device);
    }
}

void scan_all_buses() {
    uint8_t root_header = pci_read_header_type(0, 0, 0);
    
    if ((root_header & 0x80) == 0) {  // single PCI host controller
        scan_bus(0);
    } else {
        for (uint8_t pci_function=0; pci_function<8; pci_function++) {
            if (pci_device_exists(0, 0, pci_function)) break;
            scan_bus(pci_function);
        }
    }
}


void apply_on_pci_devices(pci_dev_apply_function function) {
    ARRAY_FOREACH(curr, pci_devices, {
        PciDevice *current = (PciDevice*) curr;
        function(current);
    });
}


size_t get_num_devices() {
    return array_size(pci_devices);
}



size_t snprintf_device_info(char *dest, size_t maxstr, PciDevice *device, bool verbose) {
    size_t written = 0;
    written += snprintf(dest, maxstr, "+- %02x:%02x.%02x %s  [%x:%x]\n", 
                        device->bus, device->slot, device->function,
                        pci_class_name[device->class_code],
                        device->vendor_id, device->device_id
    );
    if (verbose) {
        written += snprintf(dest+written, maxstr-written, "|           status:       0x%04x  command:     0x%04x\n", device->status, device->command);
        written += snprintf(dest+written, maxstr-written, "|           class:          0x%02x  subclass:      0x%02x  prog if:        0x%02x  revision:         0x%02x\n", device->class_code, device->subclass, device->prog_if, device->revision_id);
        written += snprintf(dest+written, maxstr-written, "|           BIST:           0x%02x  header type:   0x%02x  latency timer:  0x%02x  cache line size:  0x%02x\n", device->bist, device->header_type, device->latency_timer, device->cache_line_size);
        if (!is_pci_bridge(device)) {
            written += snprintf(dest+written, maxstr-written, "|           BAR 0:    0x%08lx  BAR 1:   0x%08lx  BAR 2:    0x%08lx\n", device->bar[0], device->bar[1], device->bar[2]);
            written += snprintf(dest+written, maxstr-written, "|           BAR 3:    0x%08lx  BAR 4:   0x%08lx  BAR 5:    0x%08lx\n", device->bar[3], device->bar[4], device->bar[5]);
        written += snprintf(dest+written, maxstr-written, "|           interrupt line: 0x%02x  interrupt pin: 0x%02x  min grant:      0x%02x  max latency:      0x%02x\n", device->interrupt_line, device->interrupt_pin, device->min_grant, device->max_latency);
        }
    }
    written += snprintf(dest+written, maxstr-written, "|\n");
    return written;
}


size_t snprintf_lspci(char *dest, size_t maxstr, bool verbose) {
    if (array_size(pci_devices) > 0) {
        size_t written = 0;
        ArrayIter iterator;
        PciDevice *current;
        array_iter_init(&iterator, pci_devices);
        for (; array_iter_next(&iterator, (void**)&current) == CC_OK ;) {
            written += snprintf_device_info(dest+written, maxstr-written, current, verbose);
        }
        return written;
    }
    return 0;
}


void setup_pci_system() {
    array_new(&pci_devices);
    scan_all_buses();
    pci_make_system_dial_files();
    klog("Detected %d PCI devices\n", array_size(pci_devices));
#ifdef VERBOSE_STARTUP
    printf("Detected %d PCI devices\n", array_size(pci_devices));
#endif
}
