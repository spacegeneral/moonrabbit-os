#include <stdio.h>
#include <kernel/core.h>
#include <kernel/log.h>
#include <peripherals/serial.h>
#include <security/defaults.h>
#include <sys/default_topics.h>
#include <ipc/interface.h>
#include <interrupt/interface.h>
#include <asmutils.h>



void initialize_com_port(uint16_t portnum) {
    outb(portnum + 1, 0x00);    // Disable all interrupts
    outb(portnum + 3, 0x80);    // Enable DLAB (set baud rate divisor)
    outb(portnum + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
    outb(portnum + 1, 0x00);    //                  (hi byte)
    outb(portnum + 3, 0x03);    // 8 bits, no parity, one stop bit
    outb(portnum + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
    outb(portnum + 4, 0x0B);    // IRQs enabled, RTS/DSR set
    outb(portnum + 1, 0x01);    // Interrupt when data received
}



int is_transmit_empty(uint16_t portnum) {
    return inb(portnum + 5) & 0x20;
}
 
void send_serial(uint16_t portnum, uint8_t character) {
    while (is_transmit_empty(portnum) == 0);
    
    outb(portnum, character);
}


 
void read_serial(uint16_t portnum, char *topic_pub) {
    int control;
    
    do {
        control = inb(portnum + 5) & 1;
        if (control) {
            uint8_t recv = inb(portnum);
            CALL_IMPL(ipc, pubsub, publish, topic_pub, &recv, 0);
        }
        
    } while (control);
}



static void sender_wrapper_com1(const void *message) {
    uint8_t *character_from = (uint8_t*) message;
    send_serial(COM1_PORT, *character_from);
}

void read_serial_com1() {
    read_serial(COM1_PORT, DEFAULT_COM1_IN_TOPIC);
}



void setup_serial_ports() {
    // initialize com1
    initialize_com_port(COM1_PORT);
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_COM1_IN_TOPIC, \
                               1024, sizeof(uint8_t), \
                               DEFAULT_SEC_SERIAL_IO_TOPICS_RACE, \
                               DEFAULT_SEC_SERIAL_IO_TOPICS_CLASS, \
                               DEFAULT_SEC_SERIAL_IO_TOPICS_POLICY);
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_COM1_OUT_TOPIC, \
                               1024, sizeof(uint8_t), \
                               DEFAULT_SEC_SERIAL_IO_TOPICS_RACE, \
                               DEFAULT_SEC_SERIAL_IO_TOPICS_CLASS, \
                               DEFAULT_SEC_SERIAL_IO_TOPICS_POLICY);
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_COM1_OUT_TOPIC, &sender_wrapper_com1);
    
#ifdef SERIAL_DEBUG
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_DEBUG_TOPIC, \
                               1024, sizeof(uint8_t), \
                               DEFAULT_DEBUG_OUT_TOPICS_RACE, \
                               DEFAULT_DEBUG_OUT_TOPICS_CLASS, \
                               DEFAULT_DEBUG_OUT_TOPICS_POLICY);
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_DEBUG_TOPIC, &sender_wrapper_com1);
        
#ifdef ADVANCED_DEBUG
    debug_printf("Using the serial port for debug output\r\n");
#endif
#endif
    
    CALL_IMPL(interrupt, interr_onoff, enable_device, interr_com1);
    
    klog("COM1 serial port initialized.\n");
}
