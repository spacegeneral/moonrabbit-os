#include <stdint.h>
#include <stdio.h>
#include <sys/default_topics.h>
#include <peripherals/keyboard.h>
#include <peripherals/keycodes.h>
#include <ipc/interface.h>



const char* postprocess_char(KeyEvent *event) {
    bool uppercase = ((event->mod_flags & (LSHIFT_DOWN_FLAG | RSHIFT_DOWN_FLAG | CAPSLOCK_DOWN_FLAG)) != 0);
    if (uppercase) {
        return default_keycode2printable_map->mod_uppercase[event->keycode];
    } else {
        return default_keycode2printable_map->mod_none[event->keycode];
    }
}


void keyevent_translate_and_publish_keycode(KeyEvent *event) {
    const char* printable = postprocess_char(event);
    snprintf(event->utf8, MAX_PRINTABLE_BYTE_SEQ, "%s", printable);
    CALL_IMPL(ipc, pubsub, publish, DEFAULT_KEYCODE_TOPIC, event, 0);
    
    if (!event->is_pressed) return;
    
    size_t numbytes = strlen(printable);
    if (numbytes > 0) {
        for (size_t c=0; c<numbytes; c++) {
            CALL_IMPL(ipc, pubsub, publish, DEFAULT_STDIN_TOPIC, &(printable[c]), 0);
        }
    }
}

