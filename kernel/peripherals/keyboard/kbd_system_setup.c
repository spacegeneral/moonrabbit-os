#include <kernel/log.h>
#include <sys/default_topics.h>
#include <peripherals/keyboard.h>
#include <peripherals/keycodes.h>
#include <peripherals/ps2.h>
#include <localize/localize.h>
#include <security/defaults.h>
#include <ipc/mq.h>
#include <ipc/interface.h>
#include <interrupt/interface.h>
#include <task/task.h>





static void ctrl_alt_escape(const void *message) {
    KeyEvent *event = (KeyEvent*) message;
    
    if ((event->keycode == KEY_Escape) && ((event->mod_flags & (LCTRL_DOWN_FLAG | LALT_DOWN_FLAG)) != 0)) {
        emergency_current_task_kill();
    }
}

static void handle_remap_message(const void *message) {
    KeymapLocale *from_locale = (KeymapLocale*) message;
    switch_keyboard_mapping(*from_locale);
}


void setup_keyboard_systems() {
    switch_keyboard_mapping(KBD_LOCALE_DEFAULT);
    
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_KEYBOARD_REMAP_TOPIC, 1024, sizeof(KeymapLocale), DEFAULT_SEC_KEYBOARD_TOPICS_RACE, DEFAULT_SEC_KEYBOARD_TOPICS_CLASS, DEFAULT_SEC_KEYBOARD_TOPICS_POLICY);
    
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_KEYCODE_TOPIC, 1024, sizeof(KeyEvent), DEFAULT_SEC_KEYBOARD_TOPICS_RACE, DEFAULT_SEC_KEYBOARD_TOPICS_CLASS, DEFAULT_SEC_KEYBOARD_TOPICS_POLICY);
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_STDIN_TOPIC, 1024, sizeof(uint8_t), DEFAULT_SEC_KEYBOARD_TOPICS_RACE, DEFAULT_SEC_KEYBOARD_TOPICS_CLASS, DEFAULT_SEC_KEYBOARD_TOPICS_POLICY);
    
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_KEYBOARD_REMAP_TOPIC, &handle_remap_message);
    CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_KEYCODE_TOPIC, &ctrl_alt_escape);
    
    if (ps2_controller_present) {
        setup_ps2_keyboard();
        
        CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_PS2_KEYBOARD_INPUT_TOPIC, 1024, sizeof(uint8_t), DEFAULT_SEC_KEYBOARD_TOPICS_RACE, DEFAULT_SEC_KEYBOARD_TOPICS_CLASS, DEFAULT_SEC_KEYBOARD_TOPICS_POLICY);
        
        CALL_IMPL(interrupt, interr_onoff, enable_device, interr_ps2keyboard);
    }
    
    klog("Keyboard system ready\n");
}
