#include <localize/keymap.h>

const FullKeycodeToPrintableMap *default_keycode2printable_map;

void switch_keyboard_mapping(KeymapLocale locale) {
    if (locale < NUM_KEYMAP_LOCALES) {
        default_keycode2printable_map = keycode2printable_maps + locale;
    }
}


const FullKeycodeToPrintableMap keycode2printable_maps[NUM_KEYMAP_LOCALES] = {
    [KEYMAP_US] = {
        .mod_none = {
                    UNPRINTABLE_BYTES, "1", "2", "3", "4", "5", 
                    "6", "7", "8", "9", "0", "-", 
                    "=", "\b", "\t", "q", "w", "e", "r", "t",
                    "y", "u", "i", "o", "p", "[", "]", 
                    "\n", UNPRINTABLE_BYTES, "a", "s", "d", "f", "g", "h", 
                    "j", "k", "l", ";", "'", "`", 
                    UNPRINTABLE_BYTES, "\\", "z", "x", "c", "v", "b", "n", 
                    "m", ",", ".", "/", UNPRINTABLE_BYTES, "*", 
                    UNPRINTABLE_BYTES, " ", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, "-", UNPRINTABLE_BYTES, 
                    "5", UNPRINTABLE_BYTES, "+", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES
                },
        .mod_uppercase = {
                    UNPRINTABLE_BYTES, "!", "@", "#", "$", "%", 
                    "^", "&", "*", "(", ")", "_", 
                    "+", "\b", "\r", "Q", "W", "E", "R", "T",
                    "Y", "U", "I", "O", "P", "{", "}", 
                    "\n", UNPRINTABLE_BYTES, "A", "S", "D", "F", "G", "H", 
                    "J", "K", "L", ":", "\"", "~", 
                    UNPRINTABLE_BYTES, "|", "Z", "X", "C", "V", "B", "N", 
                    "M", "<", ">", "?", UNPRINTABLE_BYTES, "*", 
                    UNPRINTABLE_BYTES, " ", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, "-", UNPRINTABLE_BYTES, 
                    "5", UNPRINTABLE_BYTES, "+", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES
                },
    },
    [KEYMAP_IT] = {
        .mod_none = {
                    UNPRINTABLE_BYTES, "1", "2", "3", "4", "5", 
                    "6", "7", "8", "9", "0", "\'", 
                    "ì", "\b", "\t", "q", "w", "e", "r", "t",
                    "y", "u", "i", "o", "p", "è", "+", 
                    "\n", UNPRINTABLE_BYTES, "a", "s", "d", "f", "g", "h", 
                    "j", "k", "l", "ò", "à", "ù", 
                    UNPRINTABLE_BYTES, "\\", "z", "x", "c", "v", "b", "n", 
                    "m", ",", ".", "-", UNPRINTABLE_BYTES, "*", 
                    UNPRINTABLE_BYTES, " ", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, "-", UNPRINTABLE_BYTES, 
                    "5", UNPRINTABLE_BYTES, "+", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES
                },
        .mod_uppercase = {
                    UNPRINTABLE_BYTES, "!", "\"", "£", "$", "%", 
                    "&", "/", "(", ")", "=", "?", 
                    "^", "\b", "\t", "Q", "W", "E", "R", "T",
                    "Y", "U", "I", "O", "P", "é", "*", 
                    "\n", UNPRINTABLE_BYTES, "A", "S", "D", "F", "G", "H", 
                    "J", "K", "L", "ç", "°", "§", 
                    UNPRINTABLE_BYTES, "\\", "Z", "X", "C", "V", "B", "N", 
                    "M", ";", ":", "_", UNPRINTABLE_BYTES, "*", 
                    UNPRINTABLE_BYTES, " ", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, "-", UNPRINTABLE_BYTES, 
                    "5", UNPRINTABLE_BYTES, "+", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES
                },
    },
    [KEYMAP_JA] = { //TODO
        .mod_none = {
                    UNPRINTABLE_BYTES, "ぬ", "ふ", "あ", "う", "え", 
                    "お", "や", "ゆ", "よ", "わ", "ほ", 
                    "へ", "\b", "\t", "た", "て", "い", "す", "か",
                    "ん", "な", "に", "ら", "せ", "\u3099", "\u309A", 
                    "\n", UNPRINTABLE_BYTES, "ち", "と", "し", "は", "き", "く", 
                    "ま", "の", "り", "れ", "け", "む", 
                    UNPRINTABLE_BYTES, "ー", "つ", "さ", "そ", "ひ", "こ", "み", 
                    "も", "ね", "る", "め", UNPRINTABLE_BYTES, "*", 
                    UNPRINTABLE_BYTES, " ", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, "-", UNPRINTABLE_BYTES, 
                    "5", UNPRINTABLE_BYTES, "+", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES
                },
        .mod_uppercase = {
                    UNPRINTABLE_BYTES, "!", "@", "#", "$", "%", 
                    "^", "&", "*", "(", ")", "_", 
                    "+", "\b", "\r", "Q", "W", "E", "R", "T",
                    "Y", "U", "I", "O", "P", "{", "}", 
                    "\n", UNPRINTABLE_BYTES, "A", "S", "D", "F", "G", "H", 
                    "J", "K", "L", ":", "\"", "~", 
                    UNPRINTABLE_BYTES, "|", "Z", "X", "C", "V", "B", "N", 
                    "M", "<", ">", "?", UNPRINTABLE_BYTES, "*", 
                    UNPRINTABLE_BYTES, " ", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, "-", UNPRINTABLE_BYTES, 
                    "5", UNPRINTABLE_BYTES, "+", UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, 
                    UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES, UNPRINTABLE_BYTES
                },
    },
};

