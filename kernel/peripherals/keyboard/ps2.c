#include <stdint.h>
#include <sys/default_topics.h>
#include <sys/keycodes.h>
#include <peripherals/keyboard.h>
#include <peripherals/keycodes.h>
#include <peripherals/ps2.h>
#include <interrupt/timing.h>
#include <ipc/interface.h>
#include <kernel/log.h>
#include <kernel/bugs.h>
#include <asmutils.h>
#include <stdio.h>


unsigned scancode_set = 0;  // 0 == set 1

bool l_shift_pressed = false;
bool r_shift_pressed = false;
bool capslock_on = false;
bool l_alt_on = false;
bool l_ctrl_on = false;


// Only for scancode set 1 (pc / at)

#define LSHIFT_DOWN 0x2A
#define RSHIFT_DOWN 0x36
#define LSHIFT_UP 0xAA
#define RSHIFT_UP 0xB6
#define LALT_DOWN 0x38
#define LALT_UP 0xB8
#define LCTRL_DOWN 0x1D
#define LCTRL_UP 0x9D
#define CAPSLOCK_DOWN 0x3A



void stateful_keys(uint8_t scancode) {
    switch (scancode) {
        case LSHIFT_DOWN:
            l_shift_pressed = true;
            break;
        case RSHIFT_DOWN:
            r_shift_pressed = true;
            break;
        case LSHIFT_UP:
            l_shift_pressed = false;
            break;
        case RSHIFT_UP:
            r_shift_pressed = false;
            break;
        case LALT_DOWN:
            l_alt_on = true;
            break;
        case LALT_UP:
            l_alt_on = false;
            break;
        case LCTRL_DOWN:
            l_ctrl_on = true;
            break;
        case LCTRL_UP:
            l_ctrl_on = false;
            break;
        case CAPSLOCK_DOWN:
            capslock_on = !capslock_on;
            break;
    }
}



// called from the interrupt handler
void publish_new_ps2_scancode() {
    uint8_t scancode;
    int error = get_ps2_scancode(&scancode);
    if (error) {
        klog("Timeout getting ps2 scancode.\n");
        return;
    }
    
    CALL_IMPL(ipc, pubsub, publish, DEFAULT_PS2_KEYBOARD_INPUT_TOPIC, &scancode, 0);
    
    stateful_keys(scancode);
    
    unsigned flags = 0;
    if (l_shift_pressed) flags |= LSHIFT_DOWN_FLAG;
    if (r_shift_pressed) flags |= RSHIFT_DOWN_FLAG;
    if (l_alt_on) flags |= LALT_DOWN_FLAG;
    if (l_ctrl_on) flags |= LCTRL_DOWN_FLAG;
    if (capslock_on) flags |= CAPSLOCK_DOWN_FLAG;
    
    uint8_t scancode_without_released_bit = scancode & 0b01111111;
    if (scancode_without_released_bit < NUM_SCANCODES_DEFINED) {
        KeyCode translated = ps2_scancode2keycode_table[scancode_set][scancode_without_released_bit];
        KeyEvent event = {
            .keycode = translated,
            .is_pressed = !(scancode & 0b10000000),
            .utf8 = "",
            .mod_flags = flags,
            .timestamp = system_clock_millisec(),
        };
        
        keyevent_translate_and_publish_keycode(&event);
    }
    
}


void setup_ps2_keyboard() {
    ps2_controller_command_write(PS2_CMD_ENABLE_FIRST_DEVICE);
    
    //Enable the interrupts
    ps2_controller_command_write(PS2_CMD_READ_INTERNAL_RAM(PS2_CONFIG_BYTE));
    ps2_wait(0);
    uint8_t status = (inb(PS2_DATA_PORT) | 1);
    ps2_controller_command_write(PS2_CMD_WRITE_INTERNAL_RAM(PS2_CONFIG_BYTE));
    ps2_wait(1);
    outb(PS2_DATA_PORT, status);
    
    if (!have_qemu_ps2_scancode_bug()) {
        // set scancode set
        uint8_t ack_code;
        
        ps2_first_port_command_write(0xF0);
        ack_code = ps2_read_data();  // ack
        if (ack_code == 0xFE) {  // resend
            ps2_first_port_command_write(0xF0);
            ack_code = ps2_read_data();  // ack
        }
        
        ps2_first_port_command_write(PS2_KEYBOARD_SCANCODE_SET);
        ack_code = ps2_read_data();  // ack
        if (ack_code == 0xFE) {  // resend
            ps2_first_port_command_write(PS2_KEYBOARD_SCANCODE_SET);
            ack_code = ps2_read_data();  // ack
        }
        
        // get the current scancode set
        ps2_first_port_command_write(0);
        ack_code = ps2_read_data();  // ack
        if (ack_code == 0xFE) {  // resend
            ps2_first_port_command_write(0);
            ack_code = ps2_read_data();  // ack
        }
        uint8_t current_scancode_set = ps2_read_data();
        
        klog("ps2 keyboard ready, using scancode set %d\n", current_scancode_set);
    } else {
        klog("ps2 keyboard ready, using scancode set 1 (because QEMU)\n");
    }
    
}
