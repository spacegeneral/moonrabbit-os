#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <peripherals/mouse.h>
#include <peripherals/ps2.h>
#include <interrupt/timing.h>
#include <interrupt/interface.h>
#include <ipc/interface.h>
#include <sys/default_topics.h>
#include <asmutils.h>
#include <kernel/log.h>
#include <sync/spinlock.h>


#define MOUSE_F_BIT 0x20
#define MOUSE_V_BIT 0x08


/*
 * Part of the following code is taken from
 * Mouse.inc by SANiK
 * with permission
 */

static uint8_t mouse_msg_frame[MOUSE_MSG_FRAME_LENGTH];
static uint8_t mouse_msg_frame_old[MOUSE_MSG_FRAME_LENGTH];
static int mouse_frame_pos = 0;
static bool ps2mouse_enabled = false;


DECLARE_LOCK(ps2mouse_lock);



void ps2_mouse_reset() {
    uint8_t response1;
    uint8_t response2;
    ps2_second_port_command_write(0xFF);
    response2 = ps2_read_data();
    do {
        response1 = response2;
        response2 = ps2_read_data();
    } while (!(response1 == 0xAA && response2 == 0x00));
}


void catch_ps2_mouse_streaming_byte() {
    uint8_t status = inb(PS2_COMMAND_PORT);
    
    while (status & 1) {
        uint8_t mouse_in = inb(PS2_DATA_PORT);
        if (status & MOUSE_F_BIT) {
            switch (mouse_frame_pos) {
                case 0:
                    mouse_msg_frame[0] = mouse_in;
                    if (!(mouse_in & MOUSE_V_BIT)) return;
                    mouse_frame_pos++;
                    break;
                case 1:
                    mouse_msg_frame[1] = mouse_in;
                    mouse_frame_pos++;
                    break;
                case 2:
                    mouse_msg_frame[2] = mouse_in;
                    /* We now have a full mouse packet ready to use */
                    if (mouse_msg_frame[0] & 0x80 || mouse_msg_frame[0] & 0x40) {
                        /* x/y overflow? bad packet! */
                        break;
                    }
                    mouse_frame_pos = 0;
                    
                    publish_ps2_mouse_event();
                    break;
            }
        }
        status = inb(PS2_COMMAND_PORT);
    }
}

void mouse_periodic_nonstreaming_request(unsigned long long int system_clock) {
    if (!ps2mouse_enabled) return;
    if (system_clock_millisec() % PS2_MOUSE_PERIOD_MS != 0) return;
    
    int mouse_ring_ptr = 0;
    uint8_t mouse_msg_ring[MOUSE_MSG_FRAME_LENGTH + 1];
    
    ps2_second_port_command_write(0xEB);  // request single packet
    
    int timeout = 100;
    int ack_ring_index = 0;
    int ack_tail_countdown = -1;
    while (timeout-- > 0) {
        if (ack_tail_countdown == 0) break;
            
        mouse_msg_ring[mouse_ring_ptr] = ps2_read_data();
        
        if (ack_tail_countdown > 0) ack_tail_countdown--;
        
        if (mouse_msg_ring[mouse_ring_ptr] == 0xFA) {
            ack_ring_index = mouse_ring_ptr;
            ack_tail_countdown = MOUSE_MSG_FRAME_LENGTH;
        }
        
        mouse_ring_ptr = (mouse_ring_ptr + 1) % (MOUSE_MSG_FRAME_LENGTH + 1);
        
        //debug_printf("mouse ring 0x%02x 0x%02x 0x%02x 0x%02x timeout %d\r\n", mouse_msg_ring[0], mouse_msg_ring[1], mouse_msg_ring[2], mouse_msg_ring[3], timeout);
    }
    
    if (timeout <= 0) return;
    
    mouse_msg_frame[0] = mouse_msg_ring[(ack_ring_index + 1) % (MOUSE_MSG_FRAME_LENGTH + 1)];
    mouse_msg_frame[1] = mouse_msg_ring[(ack_ring_index + 2) % (MOUSE_MSG_FRAME_LENGTH + 1)];
    mouse_msg_frame[2] = mouse_msg_ring[(ack_ring_index + 3) % (MOUSE_MSG_FRAME_LENGTH + 1)];
    
    if (mouse_msg_frame[0] == mouse_msg_frame_old[0] && mouse_msg_frame[1] == mouse_msg_frame_old[1] && mouse_msg_frame[2] == mouse_msg_frame_old[2]) return;
    
    debug_printf("mouse report 0x%02x 0x%02x 0x%02x 0x%02x\r\n", mouse_msg_ring[(ack_ring_index + 0) % (MOUSE_MSG_FRAME_LENGTH + 1)], mouse_msg_frame[0], mouse_msg_frame[1], mouse_msg_frame[2]);
    
    mouse_msg_frame_old[0] = mouse_msg_frame[0];
    mouse_msg_frame_old[1] = mouse_msg_frame[1];
    mouse_msg_frame_old[2] = mouse_msg_frame[2];

    publish_ps2_mouse_event();
}

void ps2_mouse_set_rate(uint8_t rate) {
    ps2_second_port_command_write(0xF3);
    ps2_read_data();  //Acknowledge
    ps2_second_port_command_write(rate);
    ps2_read_data();  //Acknowledge
}

void ps2_mouse_report_to_klog() {
    ps2_second_port_command_write(0xE9);
    uint8_t ack = ps2_read_data();
    uint8_t first = ps2_read_data();
    uint8_t res = ps2_read_data();
    uint8_t rate = ps2_read_data();
    if (ack != 0xFA) {
        klog("WARNING: ps2 mouse report command NOT acknowledged'\n");
    } else {
        klog("ps2 mouse report: status=0x%02x; resolution=%d ppm; sample rate=%d Hz\n", first, ps2_mouse_resolutions[res], rate);
    }
}



void publish_ps2_mouse_event() {
    LOCK(ps2mouse_lock);
    
    MouseEvent new_event;
    int8_t x_coord = (int8_t)mouse_msg_frame[1];
    int8_t y_coord = (int8_t)mouse_msg_frame[2];
    /*if (mouse_msg_frame[0] & 0b00100000) {
        y_coord = -y_coord;
    }
    if (mouse_msg_frame[0] & 0b00010000) {
        x_coord = -x_coord;
    }*/
    
    new_event.delta_x = (int)(x_coord);
    new_event.delta_y = (int)(y_coord);
    new_event.pointer_id = PS2_MOUSE_ID;
    new_event.left_btn_down =   ((mouse_msg_frame[0] & 0b00000001) != 0);
    new_event.right_btn_down =  ((mouse_msg_frame[0] & 0b00000010) != 0);
    new_event.middle_btn_down = ((mouse_msg_frame[0] & 0b00000100) != 0);
    new_event.timestamp = system_clock_millisec();
    
    CALL_IMPL(ipc, pubsub, publish, DEFAULT_PS2_MOUSE_INPUT_TOPIC, &new_event, 0);
    UNLOCK(ps2mouse_lock);
}


void setup_ps2_mouse_system() {
    //Enable the auxiliary ps2 device
    ps2_controller_command_write(PS2_CMD_ENABLE_SECOND_DEVICE);

#ifdef PS2_MOUSE_STREAMING
    //Enable the interrupts
    ps2_controller_command_write(PS2_CMD_READ_INTERNAL_RAM(PS2_CONFIG_BYTE));
    ps2_wait(0);
    uint8_t status_orig = inb(PS2_DATA_PORT);
    uint8_t status_modif = (status_orig | 2);
    ps2_controller_command_write(PS2_CMD_WRITE_INTERNAL_RAM(PS2_CONFIG_BYTE));
    ps2_wait(1);
    outb(PS2_DATA_PORT, status_modif);
    //printf(" [0x%02x] ", status_orig);
#endif
    
    ps2_mouse_reset();

    //Tell the mouse to use default settings
    ps2_second_port_command_write(0xF6);
    ps2_read_data();  //Acknowledge
    
#ifdef PS2_MOUSE_STREAMING
    //set rate
    ps2_mouse_set_rate(PS2_MOUSE_RATE);
    ps2_mouse_report_to_klog();
    //enable streaming
    ps2_second_port_command_write(0xf4);
    ps2_read_data();  //Acknowledge
    CALL_IMPL(interrupt, interr_onoff, enable_device, interr_ps2mouse);
    klog("3-button PS/2 mouse enabled in streaming mode.\n");
#else
    if (!register_interrupt_timer_callback(mouse_periodic_nonstreaming_request)) {
        klog("Failed to register mouse timer callback.\n");
        return;
    }
    klog("3-button PS/2 mouse enabled in report mode.\n");
#endif
    
    ps2mouse_enabled = true;
}
