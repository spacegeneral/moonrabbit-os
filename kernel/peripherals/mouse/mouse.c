#include <stddef.h>
#include <stdint.h>
#include <peripherals/mouse.h>
#include <peripherals/ps2.h>
#include <ipc/interface.h>
#include <sys/default_topics.h>
#include <security/defaults.h>

#define MOUSE_QUEUE_LEN 8


void forward_ps2(const void *message) {
    CALL_IMPL(ipc, pubsub, publish, DEFAULT_MOUSE_INPUT_TOPIC, message, 0);
}


void setup_mouse_system() {
    CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_MOUSE_INPUT_TOPIC, MOUSE_QUEUE_LEN, sizeof(MouseEvent), DEFAULT_SEC_MOUSE_TOPICS_RACE, DEFAULT_SEC_MOUSE_TOPICS_CLASS, DEFAULT_SEC_MOUSE_TOPICS_POLICY);
    
    if (ps2_controller_present && ps2_controller_dual_channel) {
        setup_ps2_mouse_system();
        CALL_IMPL(ipc, pubsub, advertise_channel, DEFAULT_PS2_MOUSE_INPUT_TOPIC, MOUSE_QUEUE_LEN, sizeof(MouseEvent), DEFAULT_SEC_MOUSE_TOPICS_RACE, DEFAULT_SEC_MOUSE_TOPICS_CLASS, DEFAULT_SEC_MOUSE_TOPICS_POLICY);
        CALL_IMPL(ipc, pubsub, kernel_subscribe, DEFAULT_PS2_MOUSE_INPUT_TOPIC, &forward_ps2);
    }
    
}
