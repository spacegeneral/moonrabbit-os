#include <stdlib.h>
#include <stdio.h>
#include <asmutils.h>
#include <kernel/core.h>
#include <peripherals/dma.h>


void isa_dma_init(IsaDmaRequest* request) {
    union {
        unsigned char b[4]; // 4 bytes
        unsigned long l;    // 1 long = 32-bit
    } a, c; // address and count

    a.l = (unsigned) request->buffer;
    c.l = (unsigned) request->buffer_size - 1; // -1 because of DMA counting
    
    uint8_t channel = 0;
    if (request->channel >= 4) channel = (((uint8_t)request->channel) >> 2) & 0b11;
    else channel = ((uint8_t)request->channel) & 0b11;

    // check that address is at most 24-bits (under 16MB)
    // check that count is at most 16-bits (DMA limit)
    // check that if we add count and address we don't get a carry
    // (DMA can't deal with such a carry, this is the 64k boundary limit)
    if ((a.l >> 24) || (c.l >> 16) || (((a.l&0xffff)+c.l)>>16)) {
        printf("floppy_dma_init: static buffer problem\n");
        panic();
    }
    
    unsigned char mode;
    uint8_t auto_init = (request->auto_init ? 0b00010000 : 0);
    switch (request->direction) {
        // 01:0:0:01:10 = single/inc/no-auto/to-mem/chan2
        case isa_dma_dir_read:  mode = 0b01000100 | auto_init | channel; break;
        // 01:0:0:10:10 = single/inc/no-auto/from-mem/chan2
        case isa_dma_dir_write: mode = 0b01001000 | auto_init | channel; break;
        
        default: return;
    }
    
    outb(get_dma_reg(request->channel, DMA_SINGLE_CHANNEL_MASK_REG),     0b100 | channel );  // mask chan
    
    outb(get_dma_reg(request->channel, DMA_FLIPFLOP_REG),                0xff   );  // reset flip-flop
    outb(get_dma_reg(request->channel, DMA_START_ADDRESS_2_6_REG),       a.b[0] );  // address low byte
    outb(get_dma_reg(request->channel, DMA_START_ADDRESS_2_6_REG),       a.b[1] );  // address high byte
    
    outb(get_dma_reg(request->channel, DMA_PAGE_ADDRESS_REG),            a.b[2] );  // external page register
    
    outb(get_dma_reg(request->channel, DMA_FLIPFLOP_REG),                0xff   );  // reset flip-flop
    outb(get_dma_reg(request->channel, DMA_COUNT_2_6_REG),               c.b[0] );  // count low byte
    outb(get_dma_reg(request->channel, DMA_COUNT_2_6_REG),               c.b[1] );  // count high byte
    
    outb(get_dma_reg(request->channel, DMA_MODE_REG),                    mode   );  // set mode (see above)

    outb(get_dma_reg(request->channel, DMA_SINGLE_CHANNEL_MASK_REG),     channel         );  // unmask chan
}
