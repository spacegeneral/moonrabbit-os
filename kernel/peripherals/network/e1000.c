#include <stdlib.h>
#include <peripherals/pci.h>
#include <peripherals/e1000.h>
#include <kernel/log.h>





NicObject* e1000_initialize(PciDevice *device) {
    E1000Nic *driver = (E1000Nic*) malloc(sizeof(E1000Nic));
    
    driver->bar_type = pci_bar_type(device, 0);
    driver->io_base = device->bar[0] & ~1;
    driver->mem_base = device->bar[0] & ~3;
    
    driver->has_eeprom = false;
    
    return NULL; //TODO finish driver
}



NicObject* match_pci_dev_e1000(PciDevice *device) {
    if (device->vendor_id != INTEL_VEND) return NULL;
    if (device->device_id == E1000_DEV || device->device_id == E1000_I217 || device->device_id == E1000_82577LM) return e1000_initialize(device);
    return NULL;
}
