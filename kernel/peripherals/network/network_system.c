#include <stdlib.h>
#include <sys/default_topics.h>
#include <peripherals/pci.h>
#include <peripherals/network.h>
#include <peripherals/e1000.h>
#include <peripherals/rtl8139.h>
#include <collections/array.h>
#include <security/defaults.h>
#include <interrupt/devices.h>
#include <data/fsdial.h>
#include <data/path.h>
#include <ipc/interface.h>
#include <kernel/log.h>
#include <kernel/kidou.h>


Array *nic_devices;
unsigned int nic_ident_gen;


#define NUM_NIC_SUPPORTED 2
static NicInitializer nic_initializers[NUM_NIC_SUPPORTED] = {
    { .match_pci_dev = &match_pci_dev_e1000, },
    { .match_pci_dev = &match_pci_dev_rtl8139, },
    
};



void network_system_irq_catcher(struct interrupt_frame* frame, int irq_num) {
    (void)(frame); // unused
    ARRAY_FOREACH(curr, nic_devices, {
        NicObject *nic = (NicObject*) curr;
        if (nic->irq_num == irq_num) {
            nic->interrupt_callback(nic);
            return;
        }
    });
}



bool setup_new_nic_irq_handler(NicObject *nic) {
    attach_isa_free_device_callback(nic->irq_num, &network_system_irq_catcher);
    InterruptSourceDevice *irqdev = get_isa_free_device(nic->irq_num);
    if (irqdev == NULL) {
        clear_isa_free_device_callback(nic->irq_num);
        klog("Cannot enable interrupts for nic %d!\n", nic->nic_id);
        return false;
    }
    device_enable_interrupt(irqdev);
    return true;
}


//TODO
// the current infrastructure only allow a maximum amount of NICs (fixed at compile-time)

#define FOREACH_NETIF_SLOT(appliedmacro) \
    appliedmacro(0) \
    appliedmacro(1) \
    appliedmacro(2) \


#define DECLARE_TX_MSG_HANDLER(idnum) \
static void tx_handler_##idnum(const void *message) { \
    NicObject *nic; \
    if (array_get_at(nic_devices, idnum, (void**)&nic) != CC_OK) return; \
    if (!nic->transmit(nic, (NetPacket*) message)) { \
        klog("Soft TX error on eth%d: cannot transmit packet.\n", idnum); \
    } \
}

#define CASE_SELECT_TX_MSG_HANDLER(idnum) \
case idnum: return &tx_handler_##idnum;



FOREACH_NETIF_SLOT(DECLARE_TX_MSG_HANDLER)

static kernel_mq_subscriber get_assigned_tx_msg_handler(NicObject *nic) {
    switch (nic->nic_id) {
        FOREACH_NETIF_SLOT(CASE_SELECT_TX_MSG_HANDLER)
    }
    
    return NULL;
}



void forward_packet(NicObject *nic, NetPacket *packet) {
    char topicname[2048];
    snprintf(topicname, 2048, "%s/eth%d/rx", DEFAULT_NET_IFACE_BASE_TOPIC, nic->nic_id);
    CALL_IMPL(ipc, pubsub, publish, topicname, packet, 0);
}



static void setup_new_nic_topics(NicObject *nic) {
    char topicname[2048];
    snprintf(topicname, 2048, "%s/eth%d/tx", DEFAULT_NET_IFACE_BASE_TOPIC, nic->nic_id);
    CALL_IMPL(ipc, pubsub, advertise_channel, topicname, 8, sizeof(NetPacket), DEFAULT_NETIFACE_RACE, DEFAULT_NETIFACE_CLASS, DEFAULT_NETIFACE_POLICY);
    CALL_IMPL(ipc, pubsub, kernel_subscribe, topicname, get_assigned_tx_msg_handler(nic));
    
    snprintf(topicname, 2048, "%s/eth%d/rx", DEFAULT_NET_IFACE_BASE_TOPIC, nic->nic_id);
    CALL_IMPL(ipc, pubsub, advertise_channel, topicname, 64, sizeof(NetPacket), DEFAULT_NETIFACE_RACE, DEFAULT_NETIFACE_CLASS, DEFAULT_NETIFACE_POLICY);
}



void scan_pci_nic(PciDevice *device) {
    for (size_t nnic = 0; nnic<NUM_NIC_SUPPORTED; nnic++) {
        NicObject *newnic = nic_initializers[nnic].match_pci_dev(device);
        if (newnic != NULL) {
            newnic->nic_id = nic_ident_gen++;
            array_add(nic_devices, newnic);
            
            setup_new_nic_irq_handler(newnic);
            setup_new_nic_topics(newnic);
            
            char macstr[MAC_STRING_LENGTH];
            format_mac(macstr, newnic->mac);
            klog("Installed NIC #%d, name: %s, MAC: %s, IRQ: %d\n", newnic->nic_id, newnic->name, macstr, newnic->irq_num);
        }
    }
}


static size_t nic_manifest_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    size_t written = 0;
    NetIfaceManifest tmp_manifest;
    
    ARRAY_FOREACH(curr, nic_devices, {
        NicObject *nic = (NicObject*) curr;
        nic->fill_manifest_info(nic, &tmp_manifest);
        
        if (written + sizeof(NetIfaceManifest) < maxsize) {
            memcpy(dest + written, &tmp_manifest, sizeof(NetIfaceManifest));
            written += sizeof(NetIfaceManifest);
        }
    });
    
    return written;
}

static size_t nic_info_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    size_t written = 0;
    NetIfaceManifest tmp_manifest;
    
    ARRAY_FOREACH(curr, nic_devices, {
        NicObject *nic = (NicObject*) curr;
        nic->fill_manifest_info(nic, &tmp_manifest);
        char macstr[MAC_STRING_LENGTH];
        format_mac(macstr, tmp_manifest.mac);
        written += snprintf(dest + written, maxsize - written, 
                            "eth%d: %s (%s)\n      haddr: %s  %s %s\n", 
                            tmp_manifest.nic_id, tmp_manifest.name, net_link_type_name[tmp_manifest.link_type], macstr, (tmp_manifest.is_promisc ? "PROMISCUOUS" : "NORMAL"), (tmp_manifest.is_enabled ? "UP" : "DOWN"));
    });
    
    return written;
}

static void setup_mem_special_files() {
    FileDescriptor* dialfile_r;
    FileDescriptor* dialfile_h;
    FileDescriptor* dest_folder;
    
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_INFODIAL_RACE,
        .class_attrib = DEFAULT_SEC_INFODIAL_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    
    dial_fs_primitives.create_file(&dialfile_r, "nics", meta);
    dial_bind_callback(dialfile_r, &nic_manifest_dial_interface);
    resolve_path(OS_DIALS_R_PATH, &dest_folder);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_r);
    
    dial_fs_primitives.create_file(&dialfile_h, "nics", meta);
    dial_bind_callback(dialfile_h, &nic_info_dial_interface);
    resolve_path(OS_DIALS_H_PATH, &dest_folder);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_h);
} 

void setup_network_system() {
    nic_ident_gen = 0;
    array_new(&nic_devices);
    apply_on_pci_devices(&scan_pci_nic);
    setup_mem_special_files();
}
