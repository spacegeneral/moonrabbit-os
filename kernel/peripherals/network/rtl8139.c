#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <asmutils.h>
#include <peripherals/pci.h>
#include <peripherals/network.h>
#include <peripherals/rtl8139.h>
#include <kernel/log.h>
#include <kernel/core.h>


static uint8_t rtl8139_read_register_u8(Rtl8139Nic *driver, uint16_t reg) {
    return inb(driver->io_base + reg);
}

static uint16_t rtl8139_read_register_u16(Rtl8139Nic *driver, uint16_t reg) {
    return inw(driver->io_base + reg);
}

static uint32_t rtl8139_read_register_u32(Rtl8139Nic *driver, uint16_t reg) {
    return inl(driver->io_base + reg);
}

static void rtl8139_write_register_u8(Rtl8139Nic *driver, uint16_t reg, uint8_t value) {
    outb(driver->io_base + reg, value);
}

static void rtl8139_write_register_u16(Rtl8139Nic *driver, uint16_t reg, uint16_t value) {
    outw(driver->io_base + reg, value);
}

static void rtl8139_write_register_u32(Rtl8139Nic *driver, uint16_t reg, uint32_t value) {
    outl(driver->io_base + reg, value);
}




static void rtl8139_power_on(Rtl8139Nic *driver) {
    rtl8139_write_register_u8(driver, RTL8139_PORT_CONFIG_BASE + 1, 0x0);
}

static bool rtl8139_soft_reset(Rtl8139Nic *driver) {
    rtl8139_write_register_u8(driver, RTL8139_PORT_COMMAND, RTL8139_CMD_RESET_BIT);
    
    int timeout = 10000;
    while (timeout-- > 0) {
        bool rst = ((rtl8139_read_register_u8(driver, RTL8139_PORT_COMMAND) & RTL8139_CMD_RESET_BIT) != 0);
        if (!rst) return true;
    }
    
    return false;
}

static void rtl8139_configure(Rtl8139Nic *driver) {
    // enable tx and rx
    rtl8139_write_register_u8(driver, RTL8139_PORT_COMMAND, RTL8139_CMD_RX_ENABLE_BIT | RTL8139_CMD_TX_ENABLE_BIT);
    // set receive configuration bits (don't accept frames just yet)
    rtl8139_write_register_u32(driver, RTL8139_PORT_RCR, RTL8139_DISABLED_RCR_CONFIG);
    // set transmit configuration bits
    rtl8139_write_register_u32(driver, RTL8139_PORT_TCR, RTL8139_DEFAULT_TCR_CONFIG);
    
    // set normal mode
    //rtl8139_write_register_u8(driver, RTL8139_PORT_CONFIG_9346, 0x0);
    
    // setup rx buffer
    driver->rx_buffer = malloc(RTL8139_RX_BUFFER_SIZE);
    driver->rx_ring_current_byte_offset = 0;
    driver->rx_errors = 0;
    rtl8139_write_register_u32(driver, RTL8139_PORT_RBSTART_BASE, (uint32_t)driver->rx_buffer);
    
    // setup tx buffer
    driver->tx_ring_current_offset = 0;
    driver->tx_ring_occupied_slots = 0;
    driver->tx_safe_buffer = malloc(MAX_LINK_MTU * RTL8139_TX_RING_SLOTS);
    driver->tx_errors = 0;
    for (int nb=0; nb<RTL8139_TX_RING_SLOTS; nb++) {
        uint16_t tx_start_reg = RTL8139_PORT_TX_START_BASE + 4*nb;
        rtl8139_write_register_u32(driver, 
                                   tx_start_reg, 
                                   (uint32_t)(driver->tx_safe_buffer + MAX_LINK_MTU*nb));
    }
    
    // reset missing packet counter
    rtl8139_write_register_u32(driver, RTL8139_PORT_MPC, 0);
    
    // set receive configuration bits
    rtl8139_write_register_u32(driver, RTL8139_PORT_RCR, RTL8139_DEFAULT_RCR_CONFIG);
    
    // set MAR registers for multicast
    rtl8139_write_register_u32(driver, RTL8139_PORT_MAR_BASE, 0xFFFFFFFF);
    rtl8139_write_register_u32(driver, RTL8139_PORT_MAR_BASE+4, 0xFFFFFFFF);
    
    // enable tx and rx
    rtl8139_write_register_u8(driver, RTL8139_PORT_COMMAND, RTL8139_CMD_RX_ENABLE_BIT | RTL8139_CMD_TX_ENABLE_BIT);
    
    // enable interrupt for tx OK and rx OK
    rtl8139_write_register_u16(driver, RTL8139_PORT_IMR, RTL8139_INTERR_DEFAULT_FLAGS);
}





static void rtl8139_initiate_tx(Rtl8139Nic *driver, unsigned length) {
    uint16_t tx_start_reg = RTL8139_PORT_TX_START_BASE + 4*driver->tx_ring_current_offset;
    uint16_t tx_status_reg = RTL8139_PORT_TX_STATUS_BASE + 4*driver->tx_ring_current_offset;
    
    rtl8139_write_register_u32(driver, 
                               tx_start_reg, 
                               (uint32_t)(driver->tx_safe_buffer + MAX_LINK_MTU*driver->tx_ring_current_offset));
    rtl8139_write_register_u32(driver, 
                               tx_status_reg, 
                               ((RTL8139_TX_FIFO_THRESH<<11) & 0x003F0000) | (length & 0x1FFF));
    
    driver->tx_ring_occupied_slots++;
    driver->tx_ring_current_offset = (driver->tx_ring_current_offset + 1) % RTL8139_TX_RING_SLOTS;
}


static bool rtl8139_tx_buffer_available(Rtl8139Nic *driver, int ring_offset) {
    uint16_t tx_status_reg = RTL8139_PORT_TX_STATUS_BASE + 4*ring_offset;
    return (rtl8139_read_register_u32(driver, tx_status_reg) & RTL8139_PORT_TX_STATUS_OWN_FLAG) != 0;
}


static void rtl8139_handle_tx_interr(Rtl8139Nic *driver) {
    // Apparently, we must read the status of every descriptor when
    // a tx interrupt occurs.
    for (int slot=0; slot<RTL8139_TX_RING_SLOTS; slot++) {
        uint16_t tx_status_reg = RTL8139_PORT_TX_STATUS_BASE + 4*slot;
        rtl8139_read_register_u32(driver, tx_status_reg);
    }
}



void rtl8139_interrupt_handler(NicObject *self) {
    Rtl8139Nic *driver = (Rtl8139Nic*) self->driver;
    
    rtl8139_write_register_u16(driver, RTL8139_PORT_IMR, 0);  // clear interrupts
    
    uint16_t interrupt_status = rtl8139_read_register_u16(driver, RTL8139_PORT_ISR);
    
    rtl8139_write_register_u16(driver, 
                               RTL8139_PORT_ISR, 
                               interrupt_status & ~(RTL8139_INTERR_FLAG_FOVW | RTL8139_INTERR_FLAG_RXOVW | RTL8139_INTERR_FLAG_ROK)); // acknowledge rx/tx
    
/*#ifdef ADVANCED_DEBUG
    debug_printf("NIC %d interrupt status 0x%04x\r\n", self->nic_id, interrupt_status);
#endif*/
    
    if (interrupt_status & RTL8139_INTERR_FLAG_ROK) {
        rtl8139_handle_rx(self);
    }
    else if (interrupt_status & RTL8139_INTERR_FLAG_RER) {
        // handle rx error
        driver->rx_errors++;
#ifdef ADVANCED_DEBUG
        debug_printf("NIC %d rx error\r\n", self->nic_id);
#endif
    }
    
    if (interrupt_status & RTL8139_INTERR_FLAG_TOK) {
        // handle tx ok
        rtl8139_handle_tx_interr(driver);
        driver->tx_ring_occupied_slots--;
    }
    else if (interrupt_status & RTL8139_INTERR_FLAG_TER) {
        // handle tx oerror
        rtl8139_handle_tx_interr(driver);
        driver->tx_errors++;
        driver->tx_ring_occupied_slots--;
#ifdef ADVANCED_DEBUG
        debug_printf("NIC %d tx error\r\n", self->nic_id);
#endif
    }
    
    rtl8139_write_register_u16(driver, 
                               RTL8139_PORT_ISR, 
                               interrupt_status & (RTL8139_INTERR_FLAG_FOVW | RTL8139_INTERR_FLAG_RXOVW | RTL8139_INTERR_FLAG_ROK));
    rtl8139_write_register_u16(driver, RTL8139_PORT_IMR, RTL8139_INTERR_DEFAULT_FLAGS);  // restore interrupt
}


bool rtl8139_transmit(NicObject *self, NetPacket *packet) {
    Rtl8139Nic *driver = (Rtl8139Nic*) self->driver;
    
    if (!self->is_enabled) return false;
    if (driver->tx_ring_occupied_slots >= RTL8139_TX_RING_SLOTS) return false;
    if (packet->length == 0) return false;
    if (!rtl8139_tx_buffer_available(driver, driver->tx_ring_current_offset)) return false;
    
/*#ifdef ADVANCED_DEBUG
        debug_printf("NIC %d trasmitting %llu B packet...\r\n", self->nic_id, packet->length);
#endif*/
    
    unsigned length = min(packet->length, MAX_LINK_MTU);
    memcpy(driver->tx_safe_buffer + MAX_LINK_MTU*driver->tx_ring_current_offset, 
           packet->data, 
           length);
    
    rtl8139_initiate_tx(driver, length);
    
    return true;
}

static void rtl8139_handle_rx_single_packet(NicObject *self) {
    Rtl8139Nic *driver = (Rtl8139Nic*) self->driver;
    
    int ring_offset = driver->rx_ring_current_byte_offset % RTL8139_RX_BUFFER_BASE_SIZE;
    uint8_t *packet_start = driver->rx_buffer + ring_offset;
    
    uint16_t rx_status = *((uint16_t*)packet_start);
    uint16_t rx_length = *((uint16_t*)(packet_start+2));
    uint8_t *data_start = packet_start+4;
    
/*#ifdef ADVANCED_DEBUG
    debug_printf("NIC %d receiving %d B packet (%p [+%d])...\r\n", self->nic_id, rx_length, data_start, (unsigned)packet_start - (unsigned)driver->rx_buffer);
#endif*/
    
    if ((rx_status & RTL8139_PKT_HEADER_ANY_ERROR) != 0) {
        klog("Warning: RTL8139 rx error (status=0x%04x, length=0x%04x); resetting NIC.\n", rx_status, rx_length);
        if (!rtl8139_soft_reset(driver)) {
            klog("Warning: RTL8139 soft reset timed out.\n");
        }
        rtl8139_configure(driver);
        return;
    }
    
    NetPacket *packet;
    
    if (self->is_enabled) {
        packet = (NetPacket*) malloc(sizeof(NetPacket));
        packet->length = rx_length;
        memcpy(packet->data, data_start, rx_length);  // remember to set WRAP!!
    }
    
    driver->rx_ring_current_byte_offset = (driver->rx_ring_current_byte_offset + rx_length + 4 + 3) & ~3;
    rtl8139_write_register_u16(driver, RTL8139_PORT_CAPR, driver->rx_ring_current_byte_offset - 16);
    
    if (self->is_enabled) {
        forward_packet(self, packet);
        free(packet);
    }
}


void rtl8139_handle_rx(NicObject *self) {
    Rtl8139Nic *driver = (Rtl8139Nic*) self->driver;
    
    while ((rtl8139_read_register_u8(driver, RTL8139_PORT_COMMAND) & RTL8139_CMD_RX_BUF_EMPTY) == 0) {
        rtl8139_handle_rx_single_packet(self);
    }
}


void rtl8139_toggle_promisc(NicObject *self, bool enabled) {
    Rtl8139Nic *driver = (Rtl8139Nic*) self->driver;
    
    if (!self->is_promisc && enabled) {
        rtl8139_write_register_u32(driver, RTL8139_PORT_RCR, RTL8139_PROMISC_RCR_CONFIG);
    } else if (self->is_promisc && !enabled) {
        rtl8139_write_register_u32(driver, RTL8139_PORT_RCR, RTL8139_DEFAULT_RCR_CONFIG);
    }
    
    self->is_promisc = enabled;
}

void rtl8139_toggle_enable(NicObject *self, bool enabled) {
    Rtl8139Nic *driver = (Rtl8139Nic*) self->driver;
    self->is_promisc = false;
    
    if (!self->is_enabled && enabled) {
        rtl8139_write_register_u32(driver, RTL8139_PORT_RCR, RTL8139_DEFAULT_RCR_CONFIG);
    } else if (self->is_enabled && !enabled) {
        rtl8139_write_register_u32(driver, RTL8139_PORT_RCR, RTL8139_DISABLED_RCR_CONFIG);
    }
    
    self->is_enabled = enabled;
}


void rtl8139_fill_manifest_info(NicObject *self, NetIfaceManifest *manifest) {
    Rtl8139Nic *driver = (Rtl8139Nic*) self->driver;
    
    manifest->nic_id = self->nic_id;
    strncpy(manifest->name, self->name, MAX_NIC_NAME_LEN);
    memcpy(manifest->mac, self->mac, MAC_ADDRESS_LENGTH);
    manifest->is_promisc = self->is_promisc;
    manifest->is_enabled = self->is_enabled;
    manifest->link_type = (driver->is_fast ? Ethernet100BASE_TX : Ethernet10BASE_T);
}



NicObject* rtl8139_initialize(PciDevice *device) {
    int bar_type = pci_bar_type(device, 0);
    if (bar_type != BAR_TYPE_IO_MAPPED) {
        klog("Aborting RTL8139 initialization: PCI BAR0 is *NOT* IO mapped.\n");
        return NULL;
    }
    
    Rtl8139Nic *driver = (Rtl8139Nic*) malloc(sizeof(Rtl8139Nic));
    driver->io_base = device->bar[0] & ~1;
    
    NicObject *nic = (NicObject*) malloc(sizeof(NicObject));
    nic->driver = (void*) driver;
    nic->irq_num = device->interrupt_line;
    nic->interrupt_callback = &rtl8139_interrupt_handler;
    nic->transmit = &rtl8139_transmit;
    nic->is_promisc = false;
    nic->toggle_promisc = &rtl8139_toggle_promisc;
    nic->is_enabled = true;
    nic->toggle_enable = &rtl8139_toggle_enable;
    nic->fill_manifest_info = &rtl8139_fill_manifest_info;
    
    snprintf(nic->name, MAX_NIC_NAME_LEN, "Realtek RTL8139");
    
    for (int m=0; m<MAC_ADDRESS_LENGTH; m++) {
        nic->mac[m] = rtl8139_read_register_u8(driver, RTL8139_PORT_MAC_BASE + m);
    }
    
    driver->is_fast = (rtl8139_read_register_u8(driver, RTL8139_PORT_MSR) & RTL8139_MSR_SPEED_BIT) == 0;
    driver->is_full_duplex = (rtl8139_read_register_u16(driver, RTL8139_PORT_BMCR) & RTL8139_BMCR_DUPLEX_BIT) != 0;
    
    pci_enable_bus_mastering(device);
    
    rtl8139_power_on(driver);
    
    if (!rtl8139_soft_reset(driver)) {
        klog("Warning: RTL8139 soft reset timed out.\n");
    }
    
    rtl8139_configure(driver);
    
    return nic;
}
 
 
NicObject* match_pci_dev_rtl8139(PciDevice *device) {
    if (device->vendor_id == REALTEK_VEND && device->device_id == RTL8139_DEV) return rtl8139_initialize(device);
    return NULL;
}
