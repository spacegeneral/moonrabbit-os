#include <peripherals/interface.h>
#include <kernel/log.h>
#include <peripherals/pci.h>
#include <peripherals/storage.h>
#include <peripherals/keyboard.h>
#include <peripherals/mouse.h>
#include <peripherals/ps2.h>
#include <peripherals/network.h>
#include <peripherals/serial.h>
#include <interrupt/devices.h>
#include <stdio.h>
#include <kernel/kidou.h>


int peripherals_readylevel_internal = 0;

static int peripherals_ready_level() {
    return peripherals_readylevel_internal;
}

static void peripherals_setup() {
#ifdef VERBOSE_STARTUP
    printf("Initializing peripherals subsystem.\n    Initializing PS2 controller... ");
#endif
    initialize_ps2_controller();
#ifdef VERBOSE_STARTUP
    printf("%s!\n    Setting up keyboards... ", (ps2_controller_present ? "done" : "not present, FAIL!"));
#endif
    setup_keyboard_systems();
#ifdef VERBOSE_STARTUP
    printf("done!\n    Initializing mice... ");
#endif
    setup_mouse_system();
#ifdef VERBOSE_STARTUP
    printf("done!\n    Initializing PCI interface\n        ");
#endif
    setup_pci_system();
#ifdef VERBOSE_STARTUP
    printf("    ...Done!\n    Initializing serial ports... ");
#endif
    setup_serial_ports();
#ifdef VERBOSE_STARTUP
    printf(" done!\n    Enabling interrupts... ");
#endif
    asm volatile ("sti");
    klog("Interrupts enabled\n");
#ifdef VERBOSE_STARTUP
    printf(" done!\n    Setting up storage devices\n        ");
#endif
    setup_storage_system();
#ifdef VERBOSE_STARTUP
    printf(" done!\n    Setting up networking system\n        ");
#endif    
    setup_network_system();
#ifdef VERBOSE_STARTUP
    printf("    ...done!\nPeripherals subsystem ready.\n");
#endif
    peripherals_readylevel_internal = 100;
}

static void peripherals_teardown() {
    peripherals_readylevel_internal = 0;
}


UNIQUE_IMPLEMENTATION_OF(peripherals, interr_ctrl_subsystem, { \
    .ready_level = &peripherals_ready_level, \
    .setup = &peripherals_setup, \
    .teardown = &peripherals_teardown \
})
