#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <asmutils.h>
#include <peripherals/ata.h>
#include <peripherals/ide.h>
#include <peripherals/storage.h>
#include <interrupt/timing.h>
#include <kernel/log.h>
#include <data/alignment.h>
#include <yakumo.h>


#define IDENT_BYTES(i) ((ident_words[(i)/2] >> (8 * ((i+1) % 2))) & 0xFF)


void new_ata_device(AtaDevice **device, 
                    uint16_t io_base, 
                    uint16_t control_base, 
                    int ide_channel, 
                    int ata_role) {
    AtaDevice *newdevice = (AtaDevice*) malloc(sizeof(AtaDevice));
    
    newdevice->ide_channel = ide_channel;
    newdevice->ata_role = ata_role;
    newdevice->io_base = io_base;
    newdevice->control_base = control_base;
    
    (*device) = newdevice;
}

void ata_device_wait(AtaDevice *device) {
    (void)inb(ATA_CONTROL_ALTERNATE_STATUS_PORT(device));
    (void)inb(ATA_CONTROL_ALTERNATE_STATUS_PORT(device));
    (void)inb(ATA_CONTROL_ALTERNATE_STATUS_PORT(device));
    (void)inb(ATA_CONTROL_ALTERNATE_STATUS_PORT(device));
}

void ata_select_device(AtaDevice *device, uint8_t selectbyte) {
    outb(ATA_DEVICE_SELECT_PORT(device), selectbyte | ((uint8_t)device->ata_role << 4));
    ata_device_wait(device);
}

//WARNING: does not work on qemu (untested on metal)
void ata_device_soft_reset(AtaDevice *device) {
    uint8_t status;
    outb(ATA_CONTROL_ALTERNATE_STATUS_PORT(device), 0x04);
    outb(ATA_CONTROL_ALTERNATE_STATUS_PORT(device), 0x00);
    ata_device_wait(device);
    while (true) {
        status = inb(ATA_CONTROL_ALTERNATE_STATUS_PORT(device));
        if (((status & ATA_SR_BSY) == 0) && ((status & ATA_SR_RDY) != 0)) {
            break;
        }
    }
}

AtaIdentifyReturnCode ata_identify_device(AtaDevice *device) {
    uint16_t ident_words[256];
    uint8_t err = 0;
    uint8_t status;
    
    ata_select_device(device, 0xA0);  // select this device
    
    outb(ATA_LBA_0_PORT(device), 0);
    outb(ATA_LBA_1_PORT(device), 0);
    outb(ATA_LBA_2_PORT(device), 0);
    outb(ATA_SECTOR_COUNT_PORT(device), 0);
    ata_device_wait(device);
    
    outb(ATA_COMMAND_STATUS_PORT(device), ATA_CMD_IDENTIFY);  // send identify command
    ata_device_wait(device);
    
    if ((status = inb(ATA_COMMAND_STATUS_PORT(device))) == 0) {  // check if the drive exists
        return ATAIDENT_DEVICE_DOES_NOT_EXIST;
    }
    while (true) {  // wait for specific bits to set
        status = inb(ATA_COMMAND_STATUS_PORT(device));
        if ((status & ATA_SR_ERR) != 0) {
            err = 1;
            break;
        }
        if (((status & ATA_SR_BSY) == 0) && ((status & ATA_SR_DRQ) != 0)) {
            break;
        }
    }
    
    uint8_t cl = inb(ATA_LBA_1_PORT(device));  // find out the device type
    uint8_t ch = inb(ATA_LBA_2_PORT(device));
    if (cl == 0x14 && ch == 0xEB) {
        device->type = IDE_PATAPI;
    } else if (cl == 0x69 && ch == 96) {
        device->type = IDE_SATAPI;
    } else if (cl == 0 && ch == 0) {
        device->type = IDE_PATA;
    } else if (cl == 0x3C && ch == 0xC3) {
        device->type = IDE_SATA;
    } else {
        return ATAIDENT_UNKNOWN_DEVICE_TYPE;
    }
    
    if (err != 0) {
        outb(ATA_COMMAND_STATUS_PORT(device), ATA_CMD_IDENTIFY_PACKET);  // send different identify command
        ata_device_wait(device);
    }
    
    for (size_t index=0; index<256; index++) {  // get the juicy data
        ident_words[index] = inw(ATA_DATA_PORT(device));
    }
    
    // extract the juicy info
    device->capabilities = ((uint32_t) (ident_words[49])) + (((uint32_t) (ident_words[50])) << 16);
    for (size_t c=0; c<6; c++) {
        device->commandsets[c] = ident_words[82+c];
    }

    if ((device->commandsets[1] & 0x0400) != 0) {
        device->address_mode = LBA48;
        device->size = ((uint64_t) ident_words[100]) + (((uint64_t) ident_words[101]) << 16) + (((uint64_t) ident_words[102]) << 32) + (((uint64_t) ident_words[103]) << 48);
    } else if ((device->capabilities & 0x0200) != 0) {
        device->address_mode = LBA28;
        device->size = ((uint64_t) ident_words[60]) + (((uint64_t) ident_words[61]) << 16);
    } else {
        device->address_mode = CHS;
        device->size = ((uint64_t) ident_words[60]) + (((uint64_t) ident_words[61]) << 16);
    }
    
    for (size_t i=0; i<41; i++) {
        device->model[i] = IDENT_BYTES(i+54);
    }
    device->model[40] = '\0';
    rstrip(device->model);
    
    return ATAIDENT_SUCCESS;
}

bool ata_device_poll(AtaDevice *device) {
    uint8_t status;
    while (true) {
        status = inb(ATA_COMMAND_STATUS_PORT(device));
        if ((status & ATA_SR_ERR) != 0) {
            klog("ATA Poll failure (ATA_SR_ERR bit set) (this is possibly due from reaching end-of-disk, ignore the error in that case)\n");
            return false;
        }
        if ((status & ATA_SR_DF) != 0) {
            klog("ATA Poll failure (ATA_SR_DF bit set)\n");
            return false;
        }
        if (((status & ATA_SR_BSY) == 0) && ((status & ATA_SR_DRQ) != 0)) {
            return true;
        }
    }
    return false;
}


bool ata_device_cache_flush(AtaDevice *device) {
    uint8_t status;
    
    outb(ATA_COMMAND_STATUS_PORT(device), ATA_CMD_CACHE_FLUSH);
    
    int timeout=1000000;
    while (timeout-- > 0) {
        status = inb(ATA_COMMAND_STATUS_PORT(device));
        if (((status & ATA_SR_BSY) == 0)) {
            return true;
        }
    }
    return false;
}


void ata_test_and_add_device(uint16_t io_base, 
                             uint16_t control_base, 
                             int ide_channel, 
                             int ata_role) {
    AtaDevice *newdevice;
    new_ata_device(&newdevice, io_base, control_base, ide_channel, ata_role);
    
    AtaIdentifyReturnCode ident = ata_identify_device(newdevice);
    if (ident == ATAIDENT_SUCCESS) {
        //printf("ata identified device 0x%x 0x%x %d %d\n", io_base, control_base, ide_channel, ata_role);
        StorageClass storageclass;
        switch (newdevice->type) {
            case IDE_PATAPI: storageclass = DISK_IDE_PATAPI; break;
            case IDE_SATAPI: storageclass = DISK_IDE_SATAPI; break;
            case IDE_PATA: storageclass = DISK_IDE_PATA; break;
            case IDE_SATA: storageclass = DISK_IDE_SATA; break;
            default: storageclass = DISK_UNKNOWN; break;
        }
        new_disk_identified(storageclass, (void*) newdevice);
    } else {
        free(newdevice);
    }
}

size_t snprint_ata_device_info(char *dest, size_t maxstring, AtaDevice *device, bool verbose) {
    size_t written = snprintf(dest, maxstring, "%s %s %s %s\n", 
        ide_channel_name[device->ide_channel],
        ata_role_name[device->ata_role],
        ata_type_name[device->type],
        device->model
    );
    if (verbose) {
        written += snprintf(dest+written, maxstring-written, "  Addr mode: %s  Size: %llu B\n", 
               ata_address_mode_name[device->address_mode], 
               device->size * SECTOR_SIZE_BYTES
              );
        written += snprintf(dest+written, maxstring-written, "  Capabilities: 0x%lx\n", device->capabilities);
    }
    return written;
}

static uint64_t ata_pio_lba48_read_sectors(AtaDevice *device, 
                                    uint64_t start_sector, 
                                    uint64_t sector_count, 
                                    uint8_t *destbuf) {
    uint64_t bytes_read = 0;
    uint64_t sector_address = start_sector;
    uint16_t sector_count16 = (uint16_t)(sector_count & 0xFFFF);
    
    uint8_t sector_count_high = (uint8_t)(sector_count16 >> 8);
    uint8_t sector_count_low = (uint8_t)(sector_count16 & 0x00FF);
    uint8_t lba0 = (uint8_t)(sector_address & 0xFF);
    uint8_t lba1 = (uint8_t)((sector_address >> 8) & 0xFF);
    uint8_t lba2 = (uint8_t)((sector_address >> 16) & 0xFF);
    uint8_t lba3 = (uint8_t)((sector_address >> 24) & 0xFF);
    uint8_t lba4 = (uint8_t)((sector_address >> 32) & 0xFF);
    uint8_t lba5 = (uint8_t)((sector_address >> 40) & 0xFF);
    
    ata_select_device(device, 0x40);
    
    outb(ATA_SECTOR_COUNT_PORT(device), sector_count_high);
    ata_device_wait(device);
    outb(ATA_LBA_0_PORT(device), lba3);
    ata_device_wait(device);
    outb(ATA_LBA_1_PORT(device), lba4);
    ata_device_wait(device);
    outb(ATA_LBA_2_PORT(device), lba5);
    ata_device_wait(device);
    
    outb(ATA_SECTOR_COUNT_PORT(device), sector_count_low);
    ata_device_wait(device);
    outb(ATA_LBA_0_PORT(device), lba0);
    ata_device_wait(device);
    outb(ATA_LBA_1_PORT(device), lba1);
    ata_device_wait(device);
    outb(ATA_LBA_2_PORT(device), lba2);
    ata_device_wait(device);
    
    outb(ATA_COMMAND_STATUS_PORT(device), ATA_CMD_READ_SECTORS_EXT);
    ata_device_wait(device);
    
    for (size_t sector=0; sector<sector_count; sector++) {
        bool pollstatus = ata_device_poll(device);
        if (!pollstatus) {
            return bytes_read;
        }
        for (size_t dc=0; dc<SECTOR_SIZE_BYTES/2; dc++) {
            uint16_t word_read = inw(ATA_DATA_PORT(device));
            destbuf[bytes_read++] = (uint8_t)(word_read & 0xFF);
            destbuf[bytes_read++] = (uint8_t)(word_read >> 8);
        }
    }
    
    ata_device_wait(device);
    
    return bytes_read;
}

static uint64_t ata_pio_lba48_write_sectors(AtaDevice *device, 
                                     uint64_t start_sector, 
                                     uint64_t sector_count, 
                                     const uint8_t *srcbuf) {
    uint64_t bytes_write = 0;
    uint64_t sector_address = start_sector;
    uint16_t sector_count16 = (uint16_t)(sector_count & 0xFFFF);
    
    uint8_t sector_count_high = (uint8_t)(sector_count16 >> 8);
    uint8_t sector_count_low = (uint8_t)(sector_count16 & 0x00FF);
    uint8_t lba0 = (uint8_t)(sector_address & 0xFF);
    uint8_t lba1 = (uint8_t)((sector_address >> 8) & 0xFF);
    uint8_t lba2 = (uint8_t)((sector_address >> 16) & 0xFF);
    uint8_t lba3 = (uint8_t)((sector_address >> 24) & 0xFF);
    uint8_t lba4 = (uint8_t)((sector_address >> 32) & 0xFF);
    uint8_t lba5 = (uint8_t)((sector_address >> 40) & 0xFF);
    
    ata_select_device(device, 0x40);
    
    outb(ATA_SECTOR_COUNT_PORT(device), sector_count_high);
    ata_device_wait(device);
    outb(ATA_LBA_0_PORT(device), lba3);
    ata_device_wait(device);
    outb(ATA_LBA_1_PORT(device), lba4);
    ata_device_wait(device);
    outb(ATA_LBA_2_PORT(device), lba5);
    ata_device_wait(device);
    
    outb(ATA_SECTOR_COUNT_PORT(device), sector_count_low);
    ata_device_wait(device);
    outb(ATA_LBA_0_PORT(device), lba0);
    ata_device_wait(device);
    outb(ATA_LBA_1_PORT(device), lba1);
    ata_device_wait(device);
    outb(ATA_LBA_2_PORT(device), lba2);
    ata_device_wait(device);
    
    outb(ATA_COMMAND_STATUS_PORT(device), ATA_CMD_WRITE_SECTORS_EXT);
    ata_device_wait(device);
    
    for (size_t sector=0; sector<sector_count; sector++) {
        bool pollstatus = ata_device_poll(device);
        if (!pollstatus) {
            return bytes_write;
        }
        for (size_t dc=0; dc<SECTOR_SIZE_BYTES/2; dc++) {
            uint16_t partlow = srcbuf[bytes_write++] & 0xFF;
            uint16_t parthi = srcbuf[bytes_write++] << 8;
            uint16_t word_write = (partlow | parthi);
            outw(ATA_DATA_PORT(device), word_write);
        }
    }
    
    ata_device_wait(device);
    
    ata_device_cache_flush(device);
    
    return bytes_write;
}


#define DECL_ATA_PIO_LBA48_RW_SECTORS_UNLIMITED(direction, buf_type)                                                                \
static uint64_t ata_pio_lba48_##direction##_sectors_unlimited(AtaDevice *device,                                                    \
                                                              uint64_t start_sector,                                                \
                                                              uint64_t sector_count,                                                \
                                                              buf_type buf) {                                                       \
    size_t num_full_transfers = sector_count / ATA_MAXTRANSFER_SECTORS;                                                             \
    bool do_partial_transfer = (sector_count % ATA_MAXTRANSFER_SECTORS != 0);                                                       \
    uint64_t partial_transfer_size = sector_count - (num_full_transfers * ATA_MAXTRANSFER_SECTORS);                                 \
                                                                                                                                    \
    uint64_t byte_count = 0;                                                                                                        \
    uint64_t op_byte_count = 0;                                                                                                     \
                                                                                                                                    \
    for (size_t f=0; f<num_full_transfers; f++) {                                                                                   \
        op_byte_count = ata_pio_lba48_##direction##_sectors(device,                                                                 \
                                                            start_sector + f*ATA_MAXTRANSFER_SECTORS,                               \
                                                            ATA_MAXTRANSFER_SECTORS,                                                \
                                                            buf + f*ATA_MAXTRANSFER_SECTORS*SECTOR_SIZE_BYTES);                     \
        byte_count += op_byte_count;                                                                                                \
        if (op_byte_count < ATA_MAXTRANSFER_SECTORS*SECTOR_SIZE_BYTES) return byte_count;                                           \
    }                                                                                                                               \
    if (do_partial_transfer) {                                                                                                      \
        op_byte_count = ata_pio_lba48_##direction##_sectors(device,                                                                 \
                                                            start_sector + num_full_transfers*ATA_MAXTRANSFER_SECTORS,              \
                                                            partial_transfer_size,                                                  \
                                                            buf + num_full_transfers*ATA_MAXTRANSFER_SECTORS*SECTOR_SIZE_BYTES);    \
        byte_count += op_byte_count;                                                                                                \
        if (op_byte_count < partial_transfer_size*SECTOR_SIZE_BYTES) return byte_count;                                             \
    }                                                                                                                               \
                                                                                                                                    \
    return byte_count;                                                                                                              \
}

DECL_ATA_PIO_LBA48_RW_SECTORS_UNLIMITED(read, uint8_t*)
DECL_ATA_PIO_LBA48_RW_SECTORS_UNLIMITED(write, const uint8_t*)


static uint64_t ata_pio_lba48_read(AtaDevice *device, 
                                   uint64_t start_address, 
                                   uint64_t data_length, 
                                   uint8_t *destbuf) {
    TYPEOF_IO_FRAME(uint64_t) ioframe;
    INITIALIZE_IO_FRAME(uint64_t, &ioframe, start_address, data_length, SECTOR_SIZE_BYTES);
    
    uint64_t op_read_bytes;
    uint64_t read_bytes = 0;
    
    uint8_t headbuffer[SECTOR_SIZE_BYTES];
    uint8_t tailbuffer[SECTOR_SIZE_BYTES];
    
    //debug_printf("ata lba48 read @ %llu, len: %llu\r\n    sectors %llu-%llu (%llu)\r\n    first unaligned offs: %llu, len: %llu\r\n    core aligned: start_sec: %llu, len: %llu\r\n    last partial len: %llu\r\n", start_address, data_length, ioframe.first_sector, ioframe.last_sector, ioframe.sector_count, ioframe.head_unaligned_offset_in_sector, ioframe.head_unaligned_bytes_length, ioframe.core_aligned_sector_start, ioframe.core_aligned_bytes_length, ioframe.tail_unaligned_bytes_length);
    
    // first unaligned chunk
    if (ioframe.head_unaligned_bytes_length > 0) {
        op_read_bytes = ata_pio_lba48_read_sectors(device, ioframe.first_sector, 1, headbuffer);
        memcpy(destbuf, 
               headbuffer + ioframe.head_unaligned_offset_in_sector, 
               ioframe.head_unaligned_bytes_length);
        if (op_read_bytes < SECTOR_SIZE_BYTES) return read_bytes + op_read_bytes;
        
        read_bytes += ioframe.head_unaligned_bytes_length;
        destbuf += ioframe.head_unaligned_bytes_length;
    }
    
    // central aligned chunk
    if (ioframe.core_aligned_bytes_length > 0) {
        op_read_bytes = ata_pio_lba48_read_sectors_unlimited(device, 
                                                           ioframe.core_aligned_sector_start, 
                                                           ioframe.core_aligned_bytes_length / SECTOR_SIZE_BYTES, 
                                                           destbuf);
        if (op_read_bytes < ioframe.core_aligned_bytes_length) return read_bytes + op_read_bytes;
        
        read_bytes += ioframe.core_aligned_bytes_length;
        destbuf += ioframe.core_aligned_bytes_length;
    }
    
    // last partial chunk
    if (ioframe.tail_unaligned_bytes_length > 0) {
        op_read_bytes = ata_pio_lba48_read_sectors(device, ioframe.last_sector, 1, tailbuffer);
        memcpy(destbuf, 
               tailbuffer, 
               ioframe.tail_unaligned_bytes_length);
        if (op_read_bytes < SECTOR_SIZE_BYTES) return read_bytes + op_read_bytes;
        
        read_bytes += ioframe.tail_unaligned_bytes_length;
        //destbuf += ioframe.tail_unaligned_bytes_length;
    }
    
    return read_bytes;
}


static uint64_t ata_pio_lba48_write(AtaDevice* device, 
                                    uint64_t start_address, 
                                    uint64_t data_length, 
                                    const uint8_t* srcbuf) {
    TYPEOF_IO_FRAME(uint64_t) ioframe;
    INITIALIZE_IO_FRAME(uint64_t, &ioframe, start_address, data_length, SECTOR_SIZE_BYTES);
    
    uint64_t op_write_bytes;
    uint64_t op_read_bytes;
    uint64_t write_bytes = 0;
    
    uint8_t headbuffer[SECTOR_SIZE_BYTES];
    uint8_t tailbuffer[SECTOR_SIZE_BYTES];
    
    // first unaligned chunk
    if (ioframe.head_unaligned_bytes_length > 0) {
        op_read_bytes = ata_pio_lba48_read_sectors(device, ioframe.first_sector, 1, headbuffer);
        if (op_read_bytes < SECTOR_SIZE_BYTES) return write_bytes;
        
        memcpy(headbuffer + ioframe.head_unaligned_offset_in_sector, 
               srcbuf, 
               ioframe.head_unaligned_bytes_length);
        
        op_write_bytes = ata_pio_lba48_write_sectors(device, ioframe.first_sector, 1, headbuffer);
        if (op_write_bytes < SECTOR_SIZE_BYTES) return write_bytes + op_write_bytes;
        
        write_bytes += ioframe.head_unaligned_bytes_length;
        srcbuf += ioframe.head_unaligned_bytes_length;
    }
    
    // central aligned chunk
    if (ioframe.core_aligned_bytes_length > 0) {
        op_write_bytes = ata_pio_lba48_write_sectors_unlimited(device, 
                                                            ioframe.core_aligned_sector_start, 
                                                            ioframe.core_aligned_bytes_length / SECTOR_SIZE_BYTES, 
                                                            srcbuf);
        if (op_write_bytes < ioframe.core_aligned_bytes_length) return write_bytes + op_write_bytes;
        
        write_bytes += ioframe.core_aligned_bytes_length;
        srcbuf += ioframe.core_aligned_bytes_length;
    }
    
    // last partial chunk
    if (ioframe.tail_unaligned_bytes_length > 0) {
        op_read_bytes = ata_pio_lba48_read_sectors(device, ioframe.last_sector, 1, tailbuffer);
        if (op_read_bytes < SECTOR_SIZE_BYTES) return write_bytes;

        memcpy(tailbuffer, 
               srcbuf, 
               ioframe.tail_unaligned_bytes_length);
        
        op_write_bytes = ata_pio_lba48_write_sectors(device, ioframe.last_sector, 1, tailbuffer);
        if (op_write_bytes < SECTOR_SIZE_BYTES) return write_bytes + op_write_bytes;
        
        write_bytes += ioframe.tail_unaligned_bytes_length;
        //srcbuf += ioframe.tail_unaligned_bytes_length;
    }
    
    return write_bytes;
}



uint64_t ata_device_read(AtaDevice *device, 
                         uint64_t start_address, 
                         uint64_t data_length, 
                         uint8_t *destbuf) {
    if (device->type == IDE_PATA || device->type == IDE_SATA) {
        if (device->address_mode == LBA48) {
            return ata_pio_lba48_read(device, start_address, data_length, destbuf);
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}


uint64_t ata_device_write(AtaDevice *device, 
                          uint64_t start_address, 
                          uint64_t data_length, 
                          const uint8_t *destbuf) {
    if (device->type == IDE_PATA || device->type == IDE_SATA) {
        if (device->address_mode == LBA48) {
            return ata_pio_lba48_write(device, start_address, data_length, destbuf);
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}
