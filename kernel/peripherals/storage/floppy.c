#include <stdio.h>
#include <stdlib.h>

#include <asmutils.h>
#include <kernel/log.h>
#include <kernel/core.h>
#include <kernel/kidou.h>
#include <peripherals/floppy.h>
#include <peripherals/cmos.h>
#include <peripherals/storage.h>
#include <peripherals/dma.h>
#include <interrupt/timing.h>
#include <interrupt/interface.h>


#define SECTOR_SIZE_BYTES 512
#define MAX_RETRIES 10

FloppyDevice floppy_devices[MAX_FLOPPYS];
FloppyCache floppy_cache[MAX_FLOPPYS];

volatile int floppy_irq_caught;
bool visualize_transfer;



static __attribute__((unused)) void lba_to_chs(uint64_t lba, FloppyType type, uint16_t* cyl, uint16_t* head, uint16_t* sector) {
    unsigned sectors_per_track = (unsigned) floppy_formats[type].sectors;
    unsigned heads_per_cylinder = (unsigned) floppy_formats[type].sides;
    unsigned lba_u = (unsigned) lba;
    
    unsigned cyl_u = lba_u / (heads_per_cylinder * sectors_per_track);
    unsigned temp = lba_u % (heads_per_cylinder * sectors_per_track);
    unsigned head_u = temp / sectors_per_track;
    unsigned sector_u = temp % sectors_per_track + 1;
    
    *cyl    = (uint16_t) cyl_u;
    *head   = (uint16_t) head_u;
    *sector = (uint16_t) sector_u;
}

static __attribute__((unused)) uint64_t chs_to_lba(FloppyType type, uint16_t cyl, uint16_t head, uint16_t sector) {
    unsigned sectors_per_track = (unsigned) floppy_formats[type].sectors;
    unsigned heads_per_cylinder = (unsigned) floppy_formats[type].sides;
    
    unsigned cyl_u = (unsigned) cyl;
    unsigned head_u = (unsigned) head;
    unsigned sector_u = (unsigned) sector;
    
    return (cyl_u * heads_per_cylinder + head_u) * sectors_per_track + (sector_u - 1);
}



void __attribute__((optimize("O0"))) catch_floppy_irq() {
    floppy_irq_caught = 1;
    //printf("Caught floppy irq\n");
}

bool __attribute__((optimize("O0"))) wait_floppy_irq(unsigned long long int timeout_millisec) {
    //("wait floppy irq\n");
    wait_milliseconds(15);
    unsigned long long int started = system_clock_millisec();
    while (!floppy_irq_caught) {
        asm volatile("hlt");
        unsigned long long int delta = system_clock_millisec() - started;
        if (delta > timeout_millisec) {
            //printf("Timeout: delta=%llu\n", delta);
            return false;
        }
    }
    floppy_irq_caught = 0;
    return true;
}

/* sendbyte() routine from intel manual */
void floppy_sendbyte(uint8_t byte) {
    volatile int msr;
    int tmo;
   
    for (tmo = 0; tmo < 128; tmo++) {
        msr = inb(FLOPPY_MAIN_STATUS_REGISTER);
        if ((msr & 0xc0) == 0x80) {
            outb(FLOPPY_DATA_FIFO, byte);
            return;
        }
        inb(0x80);   /* delay */
    }
}

/* getbyte() routine from intel manual */
int floppy_getbyte() {
    volatile int msr;
    int tmo;

    for (tmo = 0; tmo < 128; tmo++) {
        msr = inb(FLOPPY_MAIN_STATUS_REGISTER);
        if ((msr & 0xd0) == 0xd0) {
            return inb(FLOPPY_DATA_FIFO);
        }
        inb(0x80);   /* delay */
    }

    return -1;   /* read timeout */
}



void init_floppy_cache(FloppyDevice *device) {
    unsigned int size_bytes = floppy_size[device->type];
    int num_cyl = floppy_formats[device->type].cylinders;
    FloppyCache *cache = floppy_cache + device->id;
    
    cache->cache = (uint8_t*) malloc(size_bytes);
    memset(cache->cache, 0, size_bytes);
    cache->cyl_bitmap = (uint8_t*) malloc(num_cyl/8);
    memset(cache->cyl_bitmap, 0, num_cyl/8);
    cache->floppy_type = device->type;
}

void clear_floppy_cache(FloppyDevice *device) {
    FloppyCache *cache = floppy_cache + device->id;
    int num_cyl = floppy_formats[device->type].cylinders;
    memset(cache->cyl_bitmap, 0, num_cyl/8);
    memset(cache->cache, 0, floppy_size[device->type]);
}

void destroy_floppy_cache(FloppyDevice *device) {
    FloppyCache *cache = floppy_cache + device->id;
    
    free(cache->cache);
    free(cache->cyl_bitmap);
    cache->cache = NULL;
    cache->cyl_bitmap = NULL;
}

void toggle_floppy_visual_transfer(bool activated) {
    visualize_transfer = activated;
}

void visualize_cache(FloppyDevice *device, size_t select_cyl) {
    FloppyCache *cache = floppy_cache + device->id;
    size_t num_cyl = floppy_formats[device->type].cylinders;
    printf(" [");
    for (size_t c=0; c<num_cyl; c++) {
        char curr = '-';
        if (IS_CYL_CACHED(cache, c) && (c == select_cyl)) curr = 'o';
        else if (IS_CYL_CACHED(cache, c)) curr = '#';
        else if (c == select_cyl) curr = '+';
        printf("%c", curr);
    }
    printf("]\r");
}


void add_floppy(int id, int role, int type) {
    FloppyDevice *current = floppy_devices+id;
    current->id = id;
    current->inserted = true;
    current->used = true;
    current->role = role;
    current->type = type;
    current->engine_last_warm = 0;
    
    init_floppy_cache(current);
    klog("Found floppy %d: %s\n", id, floppy_type_name[type]);
#ifdef VERBOSE_STARTUP
    printf("Floppy %d: %s... ", id, floppy_type_name[type]);
#endif
    new_disk_identified(DISK_FLOPPY, current);
}


int scan_cmos_floppy() {
    int found = 0;
    uint8_t reg10 = get_cmos_register(0x10);
    if (reg10 & 0x0F) {
        add_floppy(1, FloppySlave, reg10 & 0x0F);
        found++;
    }
    if (reg10 >> 4) {
        add_floppy(0, FloppyMaster, reg10 >> 4);
        found++;
    }
    return found;
}




bool controller_correct_version() {
    floppy_sendbyte(FLOPPY_CMD_VERSION);
    uint8_t version = floppy_getbyte();
    if (version == 0x90) return true;
    
    printf("Unknown floppy version 0x%02X\n", version);
    
    return true;  //TODO very unsafe
}

/*
 * Floppy controller atomic commands
 */

void controller_configure_cmd(bool implied_seek_enable, 
                              bool fifo_disable, 
                              bool drive_polling_mode_disable, 
                              uint8_t threshold, 
                              uint8_t precompensation) {
    floppy_sendbyte(FLOPPY_CMD_CONFIGURE);
    floppy_sendbyte(0);
    floppy_sendbyte((implied_seek_enable << 6)
                    | (fifo_disable << 5)
                    | (drive_polling_mode_disable << 4)
                    | (threshold - 1));
    floppy_sendbyte(precompensation);
}

void floppy_controller_lock_cmd() { 
    floppy_sendbyte(FLOPPY_CMD_LOCK); 
    (void)floppy_getbyte();
    
}
void floppy_controller_unlock_cmd() { 
    floppy_sendbyte(FLOPPY_CMD_UNLOCK); 
    (void)floppy_getbyte();
}

void floppy_controller_sense_interrupt_cmd(uint8_t *st0, uint8_t *new_cyl) {
    floppy_sendbyte(FLOPPY_CMD_SENSE_INTERRUPT);
    *st0 = floppy_getbyte();
    *new_cyl = floppy_getbyte();
}

void floppy_controller_reset_cmd() {
    outb(FLOPPY_DIGITAL_OUTPUT_REGISTER, 0x0);
    wait_milliseconds(10);
    outb(FLOPPY_DIGITAL_OUTPUT_REGISTER, DEFAULT_DOR_VALUE);
}



void floppy_specify_cmd() {
    uint8_t srt = 8;
    uint8_t hut = 15;
    uint8_t hlt = 5;
    uint8_t ndma = 0;
    floppy_sendbyte(FLOPPY_CMD_SPECIFY);
    floppy_sendbyte((srt << 4) | hut);
    floppy_sendbyte((hlt << 1) | ndma);
}

void floppy_recalibrate_cmd(FloppyDevice *device) {
    floppy_sendbyte(FLOPPY_CMD_RECALIBRATE);
    floppy_sendbyte(device->id & 0b11);
}

void floppy_seek_cmd(FloppyDevice *device, uint16_t head, uint16_t cyl) {
    floppy_sendbyte(FLOPPY_CMD_SEEK);
    floppy_sendbyte((((uint8_t)head) << 2) | (device->id & 0b11));
    floppy_sendbyte((uint8_t)cyl);
}

void floppy_select_cmd(FloppyDevice *device, bool motor_on, bool smart_motor) {
    //printf("Select floppy %d, motor=%d, smart_motor=%d\n", device->id, motor_on, smart_motor);
    // prepare parameters
    uint8_t select = device->id & 0b11;
    uint8_t motor = 0;
    if (motor_on) {
        motor = (1 << (4 + select));
    }
    // set register
    outb(FLOPPY_DIGITAL_OUTPUT_REGISTER, select | motor | DEFAULT_DOR_VALUE);
    // if needed, wait for the motor to spin up
    if (motor_on) {
        unsigned long long int cold_time = system_clock_millisec() - device->engine_last_warm;
        if (!smart_motor) {
            wait_milliseconds(500);
        } else if (smart_motor && (cold_time > COLD_MOTOR_THRESHOLD)) {
            wait_milliseconds(500);
        } else {
            wait_milliseconds(15);
        }
    }
}

void motor_off_cmd(FloppyDevice *device) {
    //printf("Motor off floppy %d\n", device->id);
    outb(FLOPPY_DIGITAL_OUTPUT_REGISTER, DEFAULT_DOR_VALUE);
    device->engine_last_warm = system_clock_millisec();
}

void floppy_readwrite_cyl_cmd(FloppyDevice *device, uint16_t cyl, bool is_write) {
    uint16_t head = 0;
    // mt=1, mfm=1
    uint8_t command = 0xC0 | (is_write ? 0x5 : 0x6);
    
    //printf("transfer cyl: %d\n", cyl);
    /* 0: command              */ floppy_sendbyte(command);
    /* 1: device id            */ floppy_sendbyte((((uint8_t)head) << 2) | (device->id & 0b11));
    /* 2: track number         */ floppy_sendbyte((uint8_t)cyl);
    /* 3: head number          */ floppy_sendbyte((uint8_t)head);
    /* 4: starting sector      */ floppy_sendbyte(1);
    /* 5: 2=512B per sector    */ floppy_sendbyte(2);
    /* 6: sector count to xfer */ floppy_sendbyte(18);
    /* 7: GAP1 size            */ floppy_sendbyte(0x1B);
    /* 8: 0xFF=512B per sector */ floppy_sendbyte(0xFF);
}


/*
 * Floppy controller composite sequences
 */


bool floppy_recalibrate_sequence(FloppyDevice *device) {
    //printf("Recalibrate floppy %d\n", device->id);
    
    floppy_select_cmd(device, true, false);
    floppy_recalibrate_cmd(device);
    
    if (!wait_floppy_irq(5000)) return false;
    
    uint8_t st0, new_cyl;
    floppy_controller_sense_interrupt_cmd(&st0, &new_cyl);
    
    bool success = (st0 == (0x20 | (device->id & 0b11)));
    
    return success;
}

bool floppy_seek_sequence(FloppyDevice *device, uint16_t head, uint16_t cyl) {
    //printf("Seek floppy %d head %d to track %d\n", device->id, head, cyl);
    
    bool success = false;
    
    for (size_t bailout=0; bailout<20 && !success; bailout++) {
        floppy_select_cmd(device, true, false);
        floppy_seek_cmd(device, head, cyl);
        
        if (!wait_floppy_irq(5000)) return false;
        
        uint8_t st0, new_cyl;
        floppy_controller_sense_interrupt_cmd(&st0, &new_cyl);
        
        success = (st0 == (0x20 | (device->id & 0b11))) && (new_cyl == cyl);
        if (!success) {
            static const char * status[] =
            { "normal", "error", "invalid", "drive" };
            klog("Floppy seek unsuccessful: status = %s (0x%x), cyl = %d (wanted: %d)\n", 
                   status[st0 >> 6], st0, new_cyl, cyl);
        }
    }
    
    return success;
}

bool floppy_controller_init_sequence(FloppyDevice *flagship) {
    //printf("Resetting floppy controller, flagship=%d\n", flagship->id);
    floppy_controller_reset_cmd();
    outb(FLOPPY_CONFIGURATION_CONTROL_REGISTER, floppy_formats[flagship->type].rate);
    
    if (!wait_floppy_irq(5000)) return false;
    
    uint8_t st0, new_cyl;
    for (size_t c=0; c<4; c++) {
        floppy_controller_sense_interrupt_cmd(&st0, &new_cyl);
    }
    
    controller_configure_cmd(true,  // enable implied seek
                             false, // enable fifo
                             true,  // disable polling
                             15,    // threshold
                             0);    // precompensation
    floppy_controller_lock_cmd();
    floppy_specify_cmd();
    return true;
}

bool floppy_media_detect_sequence(FloppyDevice *device, bool *changed) {
    floppy_select_cmd(device, true, true);
    
    bool chg = ((inb(FLOPPY_DIGITAL_INPUT_REGISTER) & 0x80) != 0);
    (*changed) = chg;
    if (chg) {
        bool seek_succ = floppy_seek_sequence(device, 0, 10);
        wait_milliseconds(100);
        bool chg_again = ((inb(FLOPPY_DIGITAL_INPUT_REGISTER) & 0x80) != 0);
        device->inserted = seek_succ && !chg_again;
    }
    
    motor_off_cmd(device);
    return true;
}

bool floppy_readwrite_cyl_sequence(FloppyDevice *device, 
                                   uint16_t cyl, 
                                   bool is_write) {
/*#ifdef ADVANCED_DEBUG
    debug_printf("Floppy driver is %s cylinder %d\r\n", (is_write ? "writing" : "reading"), cyl);
#endif*/
    floppy_select_cmd(device, true, true);
    outb(FLOPPY_CONFIGURATION_CONTROL_REGISTER, floppy_formats[device->type].rate);
    
    /*bool calibrated = true;
    while (!calibrated) {
        calibrated = floppy_recalibrate_sequence(device);
        if (!calibrated) printf("Re-running recalibration sequence\n");
    }*/
    
    floppy_seek_sequence(device, 0, cyl);
    floppy_seek_sequence(device, 1, cyl);
    wait_milliseconds(100);
    
    IsaDmaRequest req = {
        .direction = is_write ? isa_dma_dir_write : isa_dma_dir_read,
        .channel = 2,
        .auto_init = false,
        .buffer = (uintptr_t)floppy_dma_buffer,
        .buffer_size = floppy_dma_buffer_size,
    };
    
    isa_dma_init(&req);
    floppy_readwrite_cyl_cmd(device, cyl, is_write);
    
    if (!wait_floppy_irq(4000)) {
        klog("Timeout accessing floppy\n");
        motor_off_cmd(device);
        return false;
    }
    
    // first read status information
    __attribute__((unused)) uint8_t st0, st1, st2, rcy, rhe, rse, bps;
    st0 = floppy_getbyte();
    st1 = floppy_getbyte();
    st2 = floppy_getbyte();

    rcy = floppy_getbyte();
    rhe = floppy_getbyte();
    rse = floppy_getbyte();
    // bytes per sector, should be what we programmed in
    bps = floppy_getbyte();
    
    bool success = true;
    if(st0 & 0xC0) {
        static const char * status[] =
        { 0, "error", "invalid command", "drive not ready" };
        klog("floppy_readwrite_cyl_sequence: status = %s\n", status[st0 >> 6]);
        success = false;
    }
    if(st1 & 0x80) {
        klog("floppy_readwrite_cyl_sequence: end of cylinder\n");
        success = false;
    }
    if(st0 & 0x08) {
        klog("floppy_readwrite_cyl_sequence: drive not ready\n");
        success = false;
    }
    if(st1 & 0x20) {
        klog("floppy_readwrite_cyl_sequence: CRC error\n");
        success = false;
    }
    if(st1 & 0x10) {
        klog("floppy_readwrite_cyl_sequence: controller timeout\n");
        success = false;
    }
    if(st1 & 0x04) {
        klog("floppy_readwrite_cyl_sequence: no data found\n");
        success = false;
    }
    if((st1|st2) & 0x01) {
        klog("floppy_readwrite_cyl_sequence: no address mark found\n");
        success = false;
    }
    if(st2 & 0x40) {
        klog("floppy_readwrite_cyl_sequence: deleted address mark\n");
        success = false;
    }
    if(st2 & 0x20) {
        klog("floppy_readwrite_cyl_sequence: CRC error in data\n");
        success = false;
    }
    if(st2 & 0x10) {
        klog("floppy_readwrite_cyl_sequence: wrong cylinder\n");
        success = false;
    }
    if(st2 & 0x04) {
        klog("floppy_readwrite_cyl_sequence: uPD765 sector not found\n");
        success = false;
    }
    if(st2 & 0x02) {
        klog("floppy_readwrite_cyl_sequence: bad cylinder\n");
        success = false;
    }
    if(bps != 0x2) {
        klog("floppy_readwrite_cyl_sequence: wanted 512B/sector, got %d", (1<<(bps+7)));
        success = false;
    }
    if(st1 & 0x02) {
        klog("floppy_readwrite_cyl_sequence: not writable\n");
        success = false;
    }
    
    motor_off_cmd(device);
    
    return success;
}


bool floppy_try_readwrite_sequence(FloppyDevice *device, 
                                   uint16_t cyl, 
                                   bool is_write) {
    for (int tries=0; tries<MAX_RETRIES; tries++) {
        bool status = floppy_readwrite_cyl_sequence(device, cyl, is_write);
        if (status) return true;
        klog("Error accessing floppy %d, retrying...\n", device->id);
    }
    klog("Exceeded maximum retries on floppy %d!\n", device->id);
    return false;
}



/*
    Interfaces with the disk system
 */

uint64_t floppy_device_read(FloppyDevice *device, 
                            uint64_t start_address, 
                            uint64_t data_length, 
                            uint8_t *destbuf) {
    //printf("read bytes start: %llu  length: %llu\n", start_address, data_length);
    //printf("reading floppy type %d\n", device->type);
    
    uint16_t head_;
    uint16_t sector_;
    uint16_t start_cyl, end_cyl, start_sector, start_head;
    
    uint64_t blok_address = start_address / SECTOR_SIZE_BYTES;
    uint64_t end_blok_address = ((start_address + data_length) / SECTOR_SIZE_BYTES) + 1;
    lba_to_chs(blok_address, device->type, &start_cyl, &start_head, &start_sector);
    lba_to_chs(end_blok_address, device->type, &end_cyl, &head_, &sector_);
    
    uint16_t num_cyl = end_cyl - start_cyl + 2;
    if (num_cyl > floppy_formats[device->type].cylinders) 
        num_cyl = floppy_formats[device->type].cylinders;
    
    /*printf("FDD read length %llu (0x%llX), range 0x%016llX-0x%016llX, start lba: %llu, end lba: %llu, start cyl: %d, end cyl: %d, cyl count: %d\n", 
        data_length, data_length,
        start_address, start_address + data_length, 
        blok_address, end_blok_address,
        start_cyl, end_cyl, 
        num_cyl);*/
    
    bool changed;
    floppy_media_detect_sequence(device, &changed);
    if (changed) {
/*#ifdef ADVANCED_DEBUG
        debug_printf("Floppy swap detected\r\n");
#endif*/
        klog("Floppy swap detected\n");
        clear_floppy_cache(device);
    }
    if (!device->inserted) {
/*#ifdef ADVANCED_DEBUG
        debug_printf("Warning: no floppy inserted!\r\n");
#endif*/
        klog("Warning: no floppy inserted!\n");
        return 0;
    }
    
    FloppyCache *cacheobj = floppy_cache + device->id;
    
    for (size_t c=0; c<num_cyl; c++) {
        size_t curr_cyl = start_cyl + c;
        if (visualize_transfer) visualize_cache(device, curr_cyl);
        if (!IS_CYL_CACHED(cacheobj, curr_cyl)) {
            // read a whole cylinder (18 sectors * 2 heads)
            if (!floppy_try_readwrite_sequence(device, curr_cyl, false)) return 0;
            // copy from dma region to cache buffer
            memcpy(cacheobj->cache + (floppy_dma_buffer_size * curr_cyl), 
                   floppy_dma_buffer, 
                   floppy_dma_buffer_size);
            MARK_CYL_CACHED(cacheobj, curr_cyl);
        }
        if (visualize_transfer) visualize_cache(device, -1);
    }
    
    uint64_t max_size = floppy_size[device->type];
    uint64_t actual_data_length = data_length;
    if (start_address + data_length >= max_size) {
        actual_data_length = max_size - start_address;
    }
    
/*#ifdef ADVANCED_DEBUG
    debug_printf("Floppy system read addr 0x%llx, requested length %llu, actual length %llu, num cyl: %d\r\n", start_address, data_length, actual_data_length, num_cyl);
#endif*/
    
    memcpy(destbuf, cacheobj->cache + start_address, actual_data_length);
    
    return actual_data_length;
}

uint64_t floppy_device_write(FloppyDevice *device, 
                             uint64_t start_address, 
                             uint64_t data_length, 
                             const uint8_t *srcbuf) {
    //printf("read bytes start: %llu  length: %llu\n", start_address, data_length);
    uint16_t head_;
    uint16_t sector_;
    uint16_t start_cyl, end_cyl, start_sector, start_head;
    
    uint64_t blok_address = start_address / SECTOR_SIZE_BYTES;
    uint64_t end_blok_address = (start_address + data_length) / SECTOR_SIZE_BYTES;
    lba_to_chs(blok_address, device->type, &start_cyl, &start_head, &start_sector);
    lba_to_chs(end_blok_address, device->type, &end_cyl, &head_, &sector_);
    
    uint16_t num_cyl = end_cyl - start_cyl + 1;
    if (num_cyl > floppy_formats[device->type].cylinders) 
        num_cyl = floppy_formats[device->type].cylinders;
    
    bool changed;
    floppy_media_detect_sequence(device, &changed);
    if (changed) {
        klog("Floppy swap detected\n");
        clear_floppy_cache(device);
    }
    if (!device->inserted) {
        klog("Warning: no floppy inserted!\n");
        return 0;
    }
    
    FloppyCache *cache = floppy_cache + device->id;
    
    memcpy(cache->cache + start_address, srcbuf, data_length);
    
    for (size_t c=0; c<num_cyl; c++) {
        size_t curr_cyl = start_cyl + c;
        // copy from cache buffer to dma region
        memcpy(floppy_dma_buffer, 
               cache->cache + (floppy_dma_buffer_size * curr_cyl),
               floppy_dma_buffer_size);
        // write a whole cylinder (18 sectors * 2 heads)
        if(!floppy_try_readwrite_sequence(device, curr_cyl, true)) return 0;
    }
    
    return data_length;
}

size_t snprint_floppy_device_info(char *dest, size_t maxstr, FloppyDevice *floppy, bool verbose) {
    (void)verbose; // unused
    size_t written = snprintf(dest, maxstr, "%s floppy: %s\n", 
        floppy_role_name[floppy->role],
        floppy_type_name[floppy->type]
    );
    
    
    return written;
}

uint64_t get_floppy_capacity(FloppyDevice *device) {
    return floppy_size[device->type];
}


bool is_any_floppy_inserted_lazy() {
    for (size_t f=0; f<MAX_FLOPPYS; f++) {
        if (floppy_devices[f].used && floppy_devices[f].inserted) {
            return true;
        }
    }
    return false;
}

void setup_floppy_system() {
    floppy_irq_caught = 0;
    visualize_transfer = false;
    for (size_t f=0; f<MAX_FLOPPYS; f++) floppy_devices[f].used = false;
    
    CALL_IMPL(interrupt, interr_onoff, enable_device, interr_floppy);
    
    int num_floppys = scan_cmos_floppy();
    if (num_floppys == 0) return;
    
    floppy_controller_reset_cmd();
    if (!controller_correct_version()) return;
    
    if (!floppy_controller_init_sequence(floppy_devices)) {
        #ifdef VERBOSE_STARTUP
            printf("CTR FAIL\n");
        #endif
        return;
    }
    
    for (size_t f=0; f<MAX_FLOPPYS; f++) {
        if (floppy_devices[f].used) {
            bool success = floppy_recalibrate_sequence(floppy_devices + f);
            motor_off_cmd(floppy_devices + f);
            if (!success) {
                #ifdef VERBOSE_STARTUP
                    printf("CAL FAIL\n");
                #endif
                klog("Failed to recalibrate floppy %d\n", f);
                return;
            }
            bool changed;
            floppy_media_detect_sequence(floppy_devices + f, &changed);
        }
    }
#ifdef VERBOSE_STARTUP
    printf("OK\n");
#endif
}
