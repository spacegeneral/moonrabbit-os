#include <stdint.h>
#include <peripherals/ide.h>
#include <peripherals/pci.h>
#include <peripherals/ata.h>



void scan_ide_bus(PciDevice *device) {
    uint16_t primary_io_base = (uint16_t)device->bar[0];
    uint16_t primary_control_base = (uint16_t)device->bar[1];
    uint16_t secondary_io_base = (uint16_t)device->bar[2];
    uint16_t secondary_control_base = (uint16_t)device->bar[3];
    
    if (primary_io_base == 0 || primary_io_base == 1) primary_io_base = 0x1F0;
    if (primary_control_base == 0 || primary_control_base == 1) primary_control_base = 0x3F4;
    if (secondary_io_base == 0 || secondary_io_base == 1) secondary_io_base = 0x170;
    if (secondary_control_base == 0 || secondary_control_base == 1) secondary_control_base = 0x374;
    
    ata_test_and_add_device(primary_io_base, primary_control_base, IDEPrimaryChannel, ATAMaster);
    ata_test_and_add_device(primary_io_base, primary_control_base, IDEPrimaryChannel, ATASlave);
    ata_test_and_add_device(secondary_io_base, secondary_control_base, IDESecondaryChannel, ATAMaster);
    ata_test_and_add_device(secondary_io_base, secondary_control_base, IDESecondaryChannel, ATASlave);
}


void scan_pci_device(PciDevice *device) {
    if (device->class_code == MassStorageController && device->subclass == 0x01) {
        scan_ide_bus(device);
    }
}

void ide_scan_cpi_bus() {
    apply_on_pci_devices(&scan_pci_device);
}

void setup_ide_system() {
    ide_scan_cpi_bus();
}

