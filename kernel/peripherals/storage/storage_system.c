#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <peripherals/storage.h>
#include <peripherals/ide.h>
#include <collections/array.h>
#include <peripherals/ata.h>
#include <peripherals/floppy.h>
#include <data/fsdiskio.h>
#include <data/fsdial.h>
#include <data/path.h>
#include <security/defaults.h>
#include <kernel/log.h>


Array *storage_devices;
unsigned int disk_ident_gen;



void make_system_diskio_file(unsigned int diskid) {
    FileDescriptor* diskfile;
    FileDescriptor* dest_folder;
    char name[MAX_FILENAME_LENGTH];
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_DISKIO_RACE,
        .class_attrib = DEFAULT_SEC_DISKIO_CLASS,
        .executable = false,
        .writable = true,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    ltoa(diskid, name, 10);
    diskio_fs_primitives.create_file(&diskfile, name, meta);
    diskio_bind_file(diskfile, diskid, 0, 0);
    if (!resolve_path(OS_DISK_PATH, &dest_folder)) {
        printf("Error: cannot create %s,%s\n", OS_DISK_PATH, diskfile->name);
        return;
    }
    file_method(dest_folder)->insert_into_directory(dest_folder, diskfile);
}

static size_t disks_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    return snprint_lsdisk(dest, maxsize, false);
}

static size_t disks_f_dial_interface(char *filename, char *dest, size_t maxsize) {
    (void)filename; // unused
    return snprint_lsdisk(dest, maxsize, true);
}

void storage_make_system_dial_files() {
    FileDescriptor* dialfile_h;
    FileDescriptor* dialfile_hv;
    FileDescriptor* dest_folder;
    FileMetaData meta = {
        .race_attrib = DEFAULT_SEC_INFODIAL_RACE,
        .class_attrib = DEFAULT_SEC_INFODIAL_CLASS,
        .executable = false,
        .writable = false,
        .readable = true,
        .creation_time = time(NULL),
        .modification_time = time(NULL)
    };
    dial_fs_primitives.create_file(&dialfile_h, "disks", meta);
    dial_fs_primitives.create_file(&dialfile_hv, "disks.full", meta);
    dial_bind_callback(dialfile_h, &disks_dial_interface);
    dial_bind_callback(dialfile_hv, &disks_f_dial_interface);
    resolve_path(OS_DIALS_H_PATH, &dest_folder);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_h);
    file_method(dest_folder)->insert_into_directory(dest_folder, dialfile_hv);
}


void new_disk_identified(StorageClass type, void *driver_data) {
    StorageDevice *newdevice = (StorageDevice*) malloc(sizeof(newdevice));
    newdevice->type = type;
    newdevice->driver_data = driver_data;
    newdevice->identifier = disk_ident_gen++;
    
    size_t prev_occurrences = array_contains(storage_devices, newdevice);
    if (prev_occurrences == 0) {
        array_add(storage_devices, newdevice);
        make_system_diskio_file(newdevice->identifier);
    }
}


size_t snprint_disk_info(char *dest, size_t maxstr, StorageDevice *disk, bool verbose) {
    size_t written = snprintf(dest, maxstr, "Disk #%d ", disk->identifier);
    switch (disk->type) {
        case DISK_UNKNOWN: written += snprintf(dest+written, maxstr-written, "Unknown\n"); 
        break;
        case DISK_IDE_SATA: written += snprint_ata_device_info(dest+written, maxstr-written, (AtaDevice*) disk->driver_data, verbose); 
        break;
        case DISK_IDE_SATAPI: written += snprint_ata_device_info(dest+written, maxstr-written, (AtaDevice*) disk->driver_data, verbose); 
        break;
        case DISK_IDE_PATA: written += snprint_ata_device_info(dest+written, maxstr-written, (AtaDevice*) disk->driver_data, verbose); 
        break;
        case DISK_IDE_PATAPI: written += snprint_ata_device_info(dest+written, maxstr-written, (AtaDevice*) disk->driver_data, verbose); 
        break;
        case DISK_FLOPPY: written += snprint_floppy_device_info(dest+written, maxstr-written, (FloppyDevice*) disk->driver_data, verbose); 
        break;
        default: written += snprintf(dest+written, maxstr-written, "Unimplemented\n"); break;
    }
    return written;
}


uint64_t get_disk_device_capacity(StorageDevice *disk) {
    switch (disk->type) {
        case DISK_UNKNOWN: return 0;
        case DISK_IDE_SATA:
        case DISK_IDE_SATAPI:
        case DISK_IDE_PATA:
        case DISK_IDE_PATAPI: return ((AtaDevice*) disk->driver_data)->size * SECTOR_SIZE_BYTES;
        case DISK_FLOPPY: return get_floppy_capacity((FloppyDevice*)disk->driver_data);
    }
    return 0;
}
uint64_t read_from_disk_device(StorageDevice *disk, 
                               uint64_t start_address, 
                               uint64_t data_length, 
                               uint8_t *destbuf) {
    switch (disk->type) {
        case DISK_UNKNOWN: return 0;
        case DISK_IDE_SATA: return ata_device_read((AtaDevice*) disk->driver_data, start_address, data_length, destbuf);
        case DISK_IDE_SATAPI: return ata_device_read((AtaDevice*) disk->driver_data, start_address, data_length, destbuf);
        case DISK_IDE_PATA: return ata_device_read((AtaDevice*) disk->driver_data, start_address, data_length, destbuf);
        case DISK_IDE_PATAPI: return ata_device_read((AtaDevice*) disk->driver_data, start_address, data_length, destbuf);
        case DISK_FLOPPY: return floppy_device_read((FloppyDevice*) disk->driver_data, start_address, data_length, destbuf);
    }
    return 0;
}
uint64_t write_to_disk_device(StorageDevice *disk, 
                              uint64_t start_address, 
                              uint64_t data_length, 
                              const uint8_t *srcbuf) {
    switch (disk->type) {
        case DISK_UNKNOWN: return 0;
        case DISK_IDE_SATA: return ata_device_write((AtaDevice*) disk->driver_data, start_address, data_length, srcbuf);
        case DISK_IDE_SATAPI: return ata_device_write((AtaDevice*) disk->driver_data, start_address, data_length, srcbuf);
        case DISK_IDE_PATA: return ata_device_write((AtaDevice*) disk->driver_data, start_address, data_length, srcbuf);
        case DISK_IDE_PATAPI: return ata_device_write((AtaDevice*) disk->driver_data, start_address, data_length, srcbuf);
        case DISK_FLOPPY: return floppy_device_write((FloppyDevice*) disk->driver_data, start_address, data_length, srcbuf);
    }
    return 0;
}


uint64_t get_disk_id_capacity(unsigned int diskid) {
    ARRAY_FOREACH(curr, storage_devices, {
        StorageDevice *current = (StorageDevice*) curr;
        if (current->identifier == diskid) {
            return get_disk_device_capacity(current);
        }
    });
    return 0;
}
uint64_t read_from_disk_id(unsigned int diskid, 
                           uint64_t start_address, 
                           uint64_t data_length, 
                           uint8_t *destbuf) {
    ARRAY_FOREACH(curr, storage_devices, {
        StorageDevice *current = (StorageDevice*) curr;
        if (current->identifier == diskid) {
            return read_from_disk_device(current, start_address, data_length, destbuf);
        }
    });
    return 0;
}
uint64_t write_to_disk_id(unsigned int diskid, 
                          uint64_t start_address, 
                          uint64_t data_length, 
                          const uint8_t *srcbuf) {
    ARRAY_FOREACH(curr, storage_devices, {
        StorageDevice *current = (StorageDevice*) curr;
        if (current->identifier == diskid) {
            return write_to_disk_device(current, start_address, data_length, srcbuf);
        }
    });
    return 0;
}


size_t snprint_lsdisk(char *dest, size_t maxstr, bool verbose) {
    size_t written = 0;
    ARRAY_FOREACH(curr, storage_devices, {
        StorageDevice *current = (StorageDevice*) curr;
        written += snprint_disk_info(dest+written, maxstr-written, current, verbose);
    });
    return written;
}


void setup_storage_system() {
    disk_ident_gen = 0;
    array_new(&storage_devices);
    setup_ide_system();
    setup_floppy_system();
    storage_make_system_dial_files();
    klog("Storage system ready with %d available disks\n", array_size(storage_devices));
}
