#include <stdio.h>
#include <peripherals/rtc.h>
#include <peripherals/cmos.h>


static uint8_t century_register = 0x00; 


void set_century_register(uint8_t century_register_value) {
    century_register = century_register_value;
}


void read_rtc(struct tm *current_time) {
    struct tm time_tmp;
    struct tm *timep = &time_tmp;
    unsigned char century = 0;
    unsigned char last_second;
    unsigned char last_minute;
    unsigned char last_hour;
    unsigned char last_day;
    unsigned char last_month;
    unsigned char last_year;
    unsigned char last_century;
    unsigned char registerB;

    // Note: This uses the "read registers until you get the same values twice in a row" technique
    //       to avoid getting dodgy/inconsistent values due to RTC updates

    while (get_cmos_update_in_progress_flag());                // Make sure an update isn't in progress
    timep->tm_sec = get_cmos_register(0x00);
    timep->tm_min = get_cmos_register(0x02);
    timep->tm_hour = get_cmos_register(0x04);
    timep->tm_mday = get_cmos_register(0x07);
    timep->tm_mon = get_cmos_register(0x08);
    timep->tm_year = get_cmos_register(0x09);
    if (century_register != 0) {
        century = get_cmos_register(century_register);
    }

    do {
        last_second = timep->tm_sec;
        last_minute = timep->tm_min;
        last_hour = timep->tm_hour;
        last_day = timep->tm_mday;
        last_month = timep->tm_mon;
        last_year = timep->tm_year;
        last_century = century;

        while (get_cmos_update_in_progress_flag());           // Make sure an update isn't in progress
        timep->tm_sec = get_cmos_register(0x00);
        timep->tm_min = get_cmos_register(0x02);
        timep->tm_hour = get_cmos_register(0x04);
        timep->tm_mday = get_cmos_register(0x07);
        timep->tm_mon = get_cmos_register(0x08);
        timep->tm_year = get_cmos_register(0x09);
        if(century_register != 0) {
                century = get_cmos_register(century_register);
        }
    } while( (last_second != timep->tm_sec) || (last_minute != timep->tm_min) || (last_hour != timep->tm_hour) ||
            (last_day != timep->tm_mday) || (last_month != timep->tm_mon) || (last_year != timep->tm_year) ||
            (last_century != century) );

    registerB = get_cmos_register(0x0B);

    // Convert BCD to binary values if necessary

    if (!(registerB & 0x04)) {
        timep->tm_sec = (timep->tm_sec & 0x0F) + ((timep->tm_sec / 16) * 10);
        timep->tm_min = (timep->tm_min & 0x0F) + ((timep->tm_min / 16) * 10);
        timep->tm_hour = ( (timep->tm_hour & 0x0F) + (((timep->tm_hour & 0x70) / 16) * 10) ) | (timep->tm_hour & 0x80);
        timep->tm_mday = (timep->tm_mday & 0x0F) + ((timep->tm_mday / 16) * 10);
        timep->tm_mon = (timep->tm_mon & 0x0F) + ((timep->tm_mon / 16) * 10);
        timep->tm_year = (timep->tm_year & 0x0F) + ((timep->tm_year / 16) * 10);
        if(century_register != 0) {
                century = (century & 0x0F) + ((century / 16) * 10);
        }
    }

    // Convert 12 hour clock to 24 hour clock if necessary

    if (!(registerB & 0x02) && (timep->tm_hour & 0x80)) {
        timep->tm_hour = ((timep->tm_hour & 0x7F) + 12) % 24;
    }

    // Calculate the full (4-digit) year

    if (century_register != 0) {
        timep->tm_year += century * 100;
    } else {
        timep->tm_year += (CURRENT_YEAR / 100) * 100;
        if(timep->tm_year < CURRENT_YEAR) timep->tm_year += 100;
    }
    timep->tm_year -= YEAR_BASE;
    timep->tm_mon -= 1;
    (*current_time) = time_tmp;
    //time_t localtm = mktime(timep);
    //gmtime_r(&localtm, current_time);
}


