#include <peripherals/ps2.h>
#include <stdint.h>
#include <stdbool.h>
#include <asmutils.h>



bool is_special_ps2_code(uint8_t scancode) {
    for (size_t i=0; i<NUM_PS2_SPECIAL_BYTES; i++) {
        if (scancode == ps2_special_bytes[i])
            return true;
    }
    return false;
}



void ps2_wait(uint8_t a_type) {
    int _time_out=100000;
    if (a_type == 0) {
        while (_time_out-- > 0) {  //Data
            if ((inb(PS2_COMMAND_PORT) & 1) == 1) return;
        }
    } else {
        while (_time_out-- > 0) {  //Signal
            if ((inb(PS2_COMMAND_PORT) & 2) == 0) return;
        }
    }
}

void ps2_controller_command_write(uint8_t a_write) {
    //Wait for the final part
    ps2_wait(1);
    //Finally write
    outb(PS2_COMMAND_PORT, a_write);
}

void ps2_first_port_command_write(uint8_t a_write) {
    //Wait for the final part
    ps2_wait(1);
    //Finally write
    outb(PS2_DATA_PORT, a_write);
}

void ps2_second_port_command_write(uint8_t a_write) {
    //Wait to be able to send a command
    ps2_wait(1);
    //Tell the mouse we are sending a command
    outb(PS2_COMMAND_PORT, 0xD4);
    //Wait for the final part
    ps2_wait(1);
    //Finally write
    outb(PS2_DATA_PORT, a_write);
}

uint8_t ps2_read_data() {
    ps2_wait(0);
    return inb(PS2_DATA_PORT);
}


// return nonzero for errors
int get_ps2_scancode(uint8_t *code) {
    uint8_t scancode = 0;
    int time_out=100000;
    while (scancode == 0 && time_out-- > 0) {
        scancode = inb(PS2_DATA_PORT);
    }
    
    if (scancode == 0) return 1;
    
    if (!is_special_ps2_code(scancode)) {
        *code = scancode;
        return 0;
    }
    
    return 2;
}
