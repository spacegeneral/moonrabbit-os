#include <peripherals/ps2.h>
#include <stdint.h>
#include <stdbool.h>
#include <asmutils.h>
#include <kernel/log.h>
#include <stdio.h>



bool ps2_controller_present = true;
bool ps2_controller_dual_channel = true;


void initialize_ps2_controller() {
    ps2_controller_command_write(PS2_CMD_DISABLE_FIRST_DEVICE);
    ps2_controller_command_write(PS2_CMD_DISABLE_SECOND_DEVICE);
    
    int timeout = 1000;
    while (((inb(PS2_COMMAND_PORT) & 1) == 1) && (timeout-- > 0)) {
        inb(PS2_DATA_PORT);  // flush output buffer
    }
    
    //ps2_controller_command_write(PS2_CMD_ENABLE_FIRST_DEVICE);
    
    // disable traslation
    ps2_controller_command_write(PS2_CMD_READ_INTERNAL_RAM(PS2_CONFIG_BYTE));
    ps2_wait(0);
    uint8_t status_orig = inb(PS2_DATA_PORT);
    uint8_t status_modif = (status_orig & 0b11111100);
    ps2_controller_command_write(PS2_CMD_WRITE_INTERNAL_RAM(PS2_CONFIG_BYTE));
    ps2_wait(1);
    outb(PS2_DATA_PORT, status_modif);
    
    
    /*ps2_controller_command_write(PS2_CMD_READ_INTERNAL_RAM(PS2_CONFIG_BYTE));
    ps2_wait(0);
    printf(" [0x%02x] ", inb(PS2_DATA_PORT));*/
    
    if ((status_orig & 0b00100000) == 0) {
        ps2_controller_dual_channel = false;
        klog("PS2 controller is single channel.\n");
    }
    
    ps2_controller_command_write(PS2_CMD_SELF_TEST);
    uint8_t test_result = ps2_read_data();
    if (test_result == 0x55) {
        klog("PS2 controller self test OK.\n");
    } else {
        ps2_controller_present = false;
        klog("PS2 controller self test FAIL.\n");
        return;
    }
    
    ps2_controller_command_write(PS2_CMD_TEST_FIRST_DEVICE);
    uint8_t dev1_test_result = ps2_read_data();
    if (dev1_test_result == 0x00) {
        klog("first PS2 device self test OK.\n");
    } else {
        ps2_controller_present = false;
        klog("first PS2 device self test FAIL.\n");
        return;
    }
    
    if (ps2_controller_dual_channel) {
        ps2_controller_command_write(PS2_CMD_TEST_SECOND_DEVICE);
        uint8_t dev2_test_result = ps2_read_data();
        if (dev2_test_result == 0x00) {
            klog("second PS2 device self test OK.\n");
        } else {
            ps2_controller_dual_channel = false;
            klog("second PS2 device self test FAIL.\n");
            return;
        }
    }
    
}
