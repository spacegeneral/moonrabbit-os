#include <debug.h>


void print_regs(unsigned int addr, unsigned int length) {
    for (unsigned int curr = addr; curr < addr+length; curr++) {
        unsigned char* curr_byte = (unsigned char*) curr;
        printf("%x ", *curr_byte);
    }
}
