#include <string.h>
#include <arch/setup.h>


ucpuint_t kernel_code_selector = KERNEL_CODE_SELECTOR;


void write_tss(GDTEntry *g) {
    // Firstly, let's compute the base and limit of our entry into the GDT.
    uint32_t base = (uint32_t) tss_entry;
    uint32_t limit = sizeof(TSSEntry);

    // Now, add our TSS descriptor's address to the GDT.
    g->limit_low=limit&0xFFFF;
    g->base_low=base&0xFFFFFF; //isolate bottom 24 bits
    g->accessed=1; //This indicates it's a TSS and not a LDT. This is a changed meaning
    g->read_write=0; //This indicates if the TSS is busy or not. 0 for not busy
    g->conforming_expand_down=0; //always 0 for TSS
    g->code=1; //For TSS this is 1 for 32bit usage, or 0 for 16bit.
    g->always_1=0; //indicate it is a TSS
    g->DPL=3; //same meaning
    g->present=1; //same meaning
    g->limit_high=(limit&0xF0000)>>16; //isolate top nibble
    g->available=0;
    g->always_0=0; //same thing
    g->big=0; //should leave zero according to manuals. No effect
    g->gran=0; //so that our computed GDT limit is in bytes, not pages
    g->base_high=(base&0xFF000000)>>24; //isolate top byte.

    // Ensure the TSS is initially zero'd.
    memset(tss_entry, 0, sizeof(TSSEntry));

    tss_entry->ss0  = KERNEL_DATA_SELECTOR;  // Set the kernel stack segment.
    tss_entry->esp0 = (uint32_t) kernel_stack_top; // Set the kernel stack pointer.
    //note that CS is loaded from the IDT entry and should be the regular kernel code segment
}


void set_kernel_stack(ucpuint_t stack) {
   tss_entry->esp0 = stack;
}


void tss_setup() {
    write_tss((GDTEntry*) (gdt_entries_base + TSS_SELECTOR));
}

void enable_sse() {
    asm volatile("mov eax, cr0 \n\
                  and ax, 0xFFFB \n\
                  or ax, 0x2 \n\
                  mov cr0, eax \n\
                  mov eax, cr4 \n\
                  or ax, 3 << 9 \n\
                  mov cr4, eax");
}


void arch_setup() {
    tss_setup();
    enable_sse();
}

void arch_teardown() {}
