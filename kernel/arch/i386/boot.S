# Declare constants for the multiboot header.
.set VBEGRAPHICS_FLAG, 1<<2
.set SCREEN_WIDTH, 1024
.set SCREEN_HEIGHT, 768
.set SCREEN_DEPTH, 32

.set ALIGN,    1<<0             # align loaded modules on page boundaries
.set MEMINFO,  1<<1             # provide memory map
.set FLAGS,    ALIGN | MEMINFO | VBEGRAPHICS_FLAG  # this is the Multiboot 'flag' field
.set MAGIC,    0x1BADB002       # 'magic number' lets bootloader find the header
.set CHECKSUM, -(MAGIC + FLAGS) # checksum of above, to prove we are multiboot

# Declare a header as in the Multiboot Standard.
.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM
.long 0  # header_addr, unused
.long 0  # load_addr, unused
.long 0  # load_end_addr, unused
.long 0  # bss_end_addr, unused
.long 0  # entry_addr, unused
.long 0  # mode type, 0 for linear graphics mode, 1 for EGA-standard text mode
.long SCREEN_WIDTH # width (pixel or characters)
.long SCREEN_HEIGHT # height (pixel or characters)
.long SCREEN_DEPTH  # depth


# Reserve a stack for the initial thread.
.section .bss
.align 16
stack_bottom:
.skip 1024*128 # 128 KiB
stack_top:

# Static data areas, like tables, buffers etc
.section .data
.align 4
.global gdt
.global idt
.global gdt_entries_base
.global idt_entries
.global tss_entry
.global kernel_stack_top
.global floppy_dma_buffer
.global isa_soundcard_dma_buffer

_gdtr:
.word   (6*8)-1
.long   _gdt_entries_base
_idtr:
.word   (256*8)-1
.long   _idt_entries

gdt:
.long _gdtr
idt:
.long _idtr
gdt_entries_base:
.long _gdt_entries_base
idt_entries:
.long _idt_entries
tss_entry:
.long _tss_entry
kernel_stack_top:
.long stack_top

_gdt_entries_base:
.quad   0x0000000000000000 # 0x00 null descriptor
.quad   0x00cf9a000000ffff # 0x08 kernel cs
.quad   0x00cf92000000ffff # 0x10 kernel ds
.quad   0x00dffa000000ffff # 0x18 user cs
.quad   0x00dff2000000ffff # 0x20 user ds
.quad   0x0000000000000000 # 0x28 tss (set at runtime)
_tss_entry:
.skip 104
_idt_entries:
.skip 256*8

# DMA buffers
.align 65536
_floppy_dma_buffer:
#.skip 18432
.skip 20000
floppy_dma_buffer:
.long _floppy_dma_buffer

.align 65536
_isa_soundcard_dma_buffer:
#.skip 18432
.skip 65536
isa_soundcard_dma_buffer:
.long _isa_soundcard_dma_buffer


.align 4

# The kernel entry point.
.section .text
.global _start
.type _start, @function
_start:
        cli
        movl $stack_top, %esp

        push %ebx  # push multiboot data structures
        push %eax

enable_a20:             
        ## The Undocumented PC
        inb     $0x64,  %al     
        testb   $0x2,   %al
        jnz     enable_a20
        movb    $0xdf,  %al
        outb    %al,    $0x64
        
        lgdt _gdtr
        lidt _idtr
        
        ## enter pmode
        movl    %cr0,   %eax
        orl     $0x1,   %eax
        movl    %eax,   %cr0
        
        .intel_syntax noprefix
        jmp 0x08:launch_kernel
        
        
launch_kernel:
	# Transfer control to the main kernel.
	call kernel_main

