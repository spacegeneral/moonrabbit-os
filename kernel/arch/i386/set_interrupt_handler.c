#include <stdint.h>
#include <arch/setup.h>
#include <interrupt/idt.h>


void set_interrupt_handler(unsigned interrupt_code, void *handler) {
    //printf("Set handler for 0x%x, function 0x%x\n", interrupt_code, handler);
    IDTEntry *entry = idt_entries + interrupt_code;
    
    uint32_t offset = (uint32_t) handler;
    
    entry->offset_1 = (uint16_t) (offset & 0x0000FFFF);
    entry->offset_2 = (uint16_t) (offset >> 16);
    entry->selector = KERNEL_CODE_SELECTOR;
    entry->zero = 0;
    entry->type_attr = 0b10001110;
}

void set_syscall_handler(unsigned interrupt_code, void *handler) {
    //printf("Set handler for 0x%x, function 0x%x\n", interrupt_code, handler);
    IDTEntry *entry = idt_entries + interrupt_code;
    
    uint32_t offset = (uint32_t) handler;
    
    entry->offset_1 = (uint16_t) (offset & 0x0000FFFF);
    entry->offset_2 = (uint16_t) (offset >> 16);
    entry->selector = KERNEL_CODE_SELECTOR;
    entry->zero = 0;
    entry->type_attr = 0b11101110;
}

void disable_interrupt_handler(unsigned interrupt_code) {
    IDTEntry *entry = idt_entries + interrupt_code;
    
    entry->selector = KERNEL_CODE_SELECTOR;
    entry->zero = 0;
    entry->type_attr = 0b00001110;
}
