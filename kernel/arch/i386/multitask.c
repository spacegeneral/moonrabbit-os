#include <arch/setup.h>
#include <task/task.h>
#include <stdio.h>


Task* make_task(unsigned int task_id,
                ucpuint_t entry_point, 
                void *main_memarea,
                ucpuint_t stack_top,
                void *stack_memarea,
                ucpuint_t environ,
                ucpuint_t cr3, 
                ucpuint_t flags, 
                unsigned int owner_id,
                unsigned int parent_task,
                int *return_code_here, 
                const char* name, 
                TaskDebuginfo *debug_info) {
    Task *newtask = tasklist + task_id;
    newtask->regs.eax = environ;
    newtask->regs.ebx = 0;
    newtask->regs.ecx = 0;
    newtask->regs.edx = 0;
    newtask->regs.esi = 0;
    newtask->regs.edi = 0;
    newtask->regs.eflags = flags;
    newtask->regs.eip = entry_point;
    newtask->regs.cr3 = cr3;
    newtask->regs.esp = stack_top;
    newtask->main_memarea = main_memarea;
    newtask->stack_memarea = stack_memarea;
    
    newtask->owner_id = owner_id;
    newtask->task_id = task_id;
    newtask->parent_task = parent_task;
    newtask->return_code_here = return_code_here;
    
    snprintf(newtask->name, MAX_TASKNAME_LENGTH, "%s", name);
    newtask->debug_info = debug_info;
    
    newtask->time_counter[TIME_COUNTER_USER] = 0;
    newtask->time_counter[TIME_COUNTER_KERNEL] = 0;
    newtask->time_counter_latch[TIME_COUNTER_USER] = 0;
    newtask->time_counter_latch[TIME_COUNTER_KERNEL] = 0;
    newtask->resource_counter_latch = 1;
    
    newtask->preempt.enabled = false;
    
    newtask->happyness = 0.0;
    
    tasks_make_system_memmap_file(task_id);
    
    return newtask;
}

void change_saved_ip(Registers* regs, addr_t new_ip) {
    regs->eip = new_ip;
}

void change_saved_sp(Registers* regs, addr_t new_sp) {
    regs->esp = new_sp;
}

void change_saved_flags(Registers* regs, ucpuint_t new_flags) {
    regs->eflags = new_flags;
}
