#include <debug.h>
#include <stdio.h>

void print_regs(Registers *regs) {
    printf("    EAX: 0x%08X  EFLAGS: 0x%08X\n", regs->eax, regs->eflags);
    printf("    EBX: 0x%08X  CR3:    0x%08X\n", regs->ebx, regs->cr3);
    printf("    ECX: 0x%08X  ESI:    0x%08X\n", regs->ecx, regs->esi);
    printf("    EDX: 0x%08X  EDI:    0x%08X\n", regs->edx, regs->edi);
    printf("    ESP: 0x%08X  EBP:    0x%08X\n", regs->esp, regs->ebp);
    printf("    EIP: 0x%08X\n", regs->eip);
    debug_printf("    EAX: 0x%08X  EFLAGS: 0x%08X\r\n", regs->eax, regs->eflags);
    debug_printf("    EBX: 0x%08X  CR3:    0x%08X\r\n", regs->ebx, regs->cr3);
    debug_printf("    ECX: 0x%08X  ESI:    0x%08X\r\n", regs->ecx, regs->esi);
    debug_printf("    EDX: 0x%08X  EDI:    0x%08X\r\n", regs->edx, regs->edi);
    debug_printf("    ESP: 0x%08X  EBP:    0x%08X\r\n", regs->esp, regs->ebp);
    debug_printf("    EIP: 0x%08X\r\n", regs->eip);
}
