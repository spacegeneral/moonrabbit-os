#include <arch/interface.h>
#include <builtins.h>
#include <arch/setup.h>


int arch_readylevel_internal = 0;


static int arch_ready_level() {
    return arch_readylevel_internal;
}

static void arch_arch_setup() {
    arch_setup();
    arch_readylevel_internal = 100;
    detect_vm();  // now is a good time to attempt this
}

static void arch_arch_teardown() {
    arch_teardown();
    arch_readylevel_internal = 0;
}



UNIQUE_IMPLEMENTATION_OF(arch, subsystem, { \
    .ready_level = &arch_ready_level, \
    .setup = &arch_arch_setup, \
    .teardown = &arch_arch_teardown \
})
