#!/bin/sh
set -e
. ./headers.sh

cp userspace/crt0.o tinycc/

for PROJECT in $PROJECTS; do
  (cd $PROJECT && DESTDIR="$SYSROOT" $MAKE -j16 $BUILD_TYPE install)
done

. ./doxygen_docs.sh > /dev/null 2>&1
